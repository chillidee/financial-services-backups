<?php

// BEGIN iThemes Security - Do not modify or remove this line
// iThemes Security Config Details: 2
define( 'DISALLOW_FILE_EDIT', true ); // Disable File Editor - Security > Settings > WordPress Tweaks > File Editor
// END iThemes Security - Do not modify or remove this line

/** Enable W3 Total Cache */
define('WP_CACHE', true); // Added by W3 Total Cache

/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'helpdebt_new');

/** MySQL database username */
define('DB_USER', 'helpdebt_new');

/** MySQL database password */
define('DB_PASSWORD', 'y@0wX?oF5pr~');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'Rpg{3dSC,7_!1v0Cq7+mcR4Q(jh?ATuf.]Wk[D$C`+9~:t3^;>YaP!@d@GT?WQ}#');
define('SECURE_AUTH_KEY',  '>wTjVlSeBtF30=ZvPdmjfi*J,INQW<d@.:qU.>;Xiue3<mh!SB; m(yh=V-.*`K$');
define('LOGGED_IN_KEY',    '6kK}e<Y>K<5QUA,K]%k6=qiWI{1J2m{kMC{E^{lj&h,8PBH,|F^O|k7v@CM?#nu7');
define('NONCE_KEY',        'elrB&*Dj3C3d;S0.uR_cO?h7&qB0TY`CWO+3Cb+H0jOpdmL=Bbu;_bf8.Za)#N|A');
define('AUTH_SALT',        ')~h9($~G1#K5Ootb0<2 J@ghM n0+*2[C5K<W-vT=~1U@UFo#*i{y2E<D`?S;V*[');
define('SECURE_AUTH_SALT', 's`7^(q`dn;-A3H!6B`vH}I%U8W$Kndnia@vrh_$c..>@v7^A=)zJw3[nJ%@r+V_{');
define('LOGGED_IN_SALT',   'wd|OD2N9?BE?D:(FD~p!MrMz<SG*lAde%9_t]GuejcqRLp?-J0++3tb`W0%ActVm');
define('NONCE_SALT',       'Ykw**:zLVa%vYeikewk7)K@f7/8&_^#^be1MUMCVU&PIW4t/1GHK_t?e2_kW4/X4');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wk8pgw_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

/** Disable wp-cron */
define('DISABLE_WP_CRON', true);