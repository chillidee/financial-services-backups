CREATE TABLE `wk8pgw_term_taxonomy` (  `term_taxonomy_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,  `term_id` bigint(20) unsigned NOT NULL DEFAULT '0',  `taxonomy` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',  `description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,  `parent` bigint(20) unsigned NOT NULL DEFAULT '0',  `count` bigint(20) NOT NULL DEFAULT '0',  PRIMARY KEY (`term_taxonomy_id`),  UNIQUE KEY `term_id_taxonomy` (`term_id`,`taxonomy`),  KEY `taxonomy` (`taxonomy`)) ENGINE=MyISAM AUTO_INCREMENT=129 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40000 ALTER TABLE `wk8pgw_term_taxonomy` DISABLE KEYS */;
SET FOREIGN_KEY_CHECKS = 0;
SET UNIQUE_CHECKS = 0;
INSERT INTO `wk8pgw_term_taxonomy` VALUES('1', '1', 'category', '', '0', '1');
INSERT INTO `wk8pgw_term_taxonomy` VALUES('69', '64', 'post_tag', '', '0', '3');
INSERT INTO `wk8pgw_term_taxonomy` VALUES('4', '4', 'category', 'A few of our most popular posts', '0', '9');
INSERT INTO `wk8pgw_term_taxonomy` VALUES('5', '5', 'category', 'Current topics', '0', '0');
INSERT INTO `wk8pgw_term_taxonomy` VALUES('6', '6', 'category', 'Ideas and inspiration for professionals', '0', '2');
INSERT INTO `wk8pgw_term_taxonomy` VALUES('7', '7', 'category', 'All you need to know in one place', '0', '7');
INSERT INTO `wk8pgw_term_taxonomy` VALUES('68', '63', 'post_tag', '', '0', '3');
INSERT INTO `wk8pgw_term_taxonomy` VALUES('9', '9', 'category', 'Free resources about debts and budgets', '0', '3');
INSERT INTO `wk8pgw_term_taxonomy` VALUES('13', '13', 'post_tag', '', '0', '0');
INSERT INTO `wk8pgw_term_taxonomy` VALUES('14', '14', 'post_tag', '', '0', '0');
INSERT INTO `wk8pgw_term_taxonomy` VALUES('15', '15', 'post_tag', '', '0', '0');
INSERT INTO `wk8pgw_term_taxonomy` VALUES('16', '16', 'post_tag', '', '0', '0');
INSERT INTO `wk8pgw_term_taxonomy` VALUES('17', '97', 'post_tag', '', '0', '0');
INSERT INTO `wk8pgw_term_taxonomy` VALUES('18', '98', 'post_tag', '', '0', '0');
INSERT INTO `wk8pgw_term_taxonomy` VALUES('19', '17', 'post_tag', '', '0', '0');
INSERT INTO `wk8pgw_term_taxonomy` VALUES('20', '18', 'post_tag', '', '0', '0');
INSERT INTO `wk8pgw_term_taxonomy` VALUES('21', '19', 'post_tag', '', '0', '0');
INSERT INTO `wk8pgw_term_taxonomy` VALUES('22', '20', 'post_tag', '', '0', '0');
INSERT INTO `wk8pgw_term_taxonomy` VALUES('23', '12', 'post_tag', '', '0', '0');
INSERT INTO `wk8pgw_term_taxonomy` VALUES('24', '21', 'post_tag', '', '0', '0');
INSERT INTO `wk8pgw_term_taxonomy` VALUES('25', '22', 'portfolio_category', 'Examples of our multimedia work', '0', '9');
INSERT INTO `wk8pgw_term_taxonomy` VALUES('26', '23', 'portfolio_tag', '', '0', '3');
INSERT INTO `wk8pgw_term_taxonomy` VALUES('27', '24', 'testimonials_category', '', '0', '5');
INSERT INTO `wk8pgw_term_taxonomy` VALUES('28', '25', 'portfolio_tag', '', '0', '5');
INSERT INTO `wk8pgw_term_taxonomy` VALUES('29', '26', 'portfolio_tag', '', '0', '10');
INSERT INTO `wk8pgw_term_taxonomy` VALUES('30', '27', 'portfolio_tag', '', '0', '3');
INSERT INTO `wk8pgw_term_taxonomy` VALUES('31', '28', 'portfolio_tag', '', '0', '9');
INSERT INTO `wk8pgw_term_taxonomy` VALUES('32', '29', 'portfolio_category', 'We can create anything your business needs', '0', '14');
INSERT INTO `wk8pgw_term_taxonomy` VALUES('33', '30', 'portfolio_category', 'Branding and reorganization are just part of what we do', '0', '11');
INSERT INTO `wk8pgw_term_taxonomy` VALUES('34', '31', 'staff_category', '', '0', '5');
INSERT INTO `wk8pgw_term_taxonomy` VALUES('35', '32', 'staff_category', '', '0', '5');
INSERT INTO `wk8pgw_term_taxonomy` VALUES('36', '33', 'portfolio_tag', '', '0', '2');
INSERT INTO `wk8pgw_term_taxonomy` VALUES('37', '34', 'testimonials_category', '', '0', '6');
INSERT INTO `wk8pgw_term_taxonomy` VALUES('38', '35', 'staff_category', '', '0', '5');
INSERT INTO `wk8pgw_term_taxonomy` VALUES('39', '36', 'portfolio_tag', '', '0', '8');
INSERT INTO `wk8pgw_term_taxonomy` VALUES('40', '37', 'portfolio_tag', '', '0', '7');
INSERT INTO `wk8pgw_term_taxonomy` VALUES('41', '99', 'staff_category', '', '0', '5');
INSERT INTO `wk8pgw_term_taxonomy` VALUES('42', '39', 'portfolio_tag', '', '0', '8');
INSERT INTO `wk8pgw_term_taxonomy` VALUES('43', '40', 'staff_category', '', '0', '5');
INSERT INTO `wk8pgw_term_taxonomy` VALUES('44', '41', 'portfolio_tag', '', '0', '6');
INSERT INTO `wk8pgw_term_taxonomy` VALUES('45', '8', 'portfolio_category', 'We work with the best photographers in the industry', '0', '6');
INSERT INTO `wk8pgw_term_taxonomy` VALUES('46', '42', 'testimonials_category', '', '0', '5');
INSERT INTO `wk8pgw_term_taxonomy` VALUES('47', '43', 'portfolio_tag', '', '0', '5');
INSERT INTO `wk8pgw_term_taxonomy` VALUES('48', '44', 'post_series', '', '0', '0');
INSERT INTO `wk8pgw_term_taxonomy` VALUES('49', '45', 'portfolio_tag', '', '0', '8');
INSERT INTO `wk8pgw_term_taxonomy` VALUES('50', '46', 'portfolio_tag', '', '0', '3');
INSERT INTO `wk8pgw_term_taxonomy` VALUES('51', '47', 'nav_menu', '', '0', '13');
INSERT INTO `wk8pgw_term_taxonomy` VALUES('52', '48', 'nav_menu', '', '0', '2');
INSERT INTO `wk8pgw_term_taxonomy` VALUES('53', '49', 'nav_menu', '', '0', '0');
INSERT INTO `wk8pgw_term_taxonomy` VALUES('54', '50', 'nav_menu', '', '0', '2');
INSERT INTO `wk8pgw_term_taxonomy` VALUES('55', '38', 'portfolio_category', '', '0', '9');
INSERT INTO `wk8pgw_term_taxonomy` VALUES('56', '51', 'post_format', '', '0', '0');
INSERT INTO `wk8pgw_term_taxonomy` VALUES('57', '52', 'post_format', '', '0', '0');
INSERT INTO `wk8pgw_term_taxonomy` VALUES('58', '53', 'post_format', '', '0', '0');
INSERT INTO `wk8pgw_term_taxonomy` VALUES('61', '56', 'post_tag', '', '0', '11');
INSERT INTO `wk8pgw_term_taxonomy` VALUES('67', '62', 'post_tag', '', '0', '3');
INSERT INTO `wk8pgw_term_taxonomy` VALUES('62', '57', 'post_tag', '', '0', '5');
INSERT INTO `wk8pgw_term_taxonomy` VALUES('66', '61', 'post_tag', '', '0', '3');
INSERT INTO `wk8pgw_term_taxonomy` VALUES('63', '58', 'category', '', '0', '15');
INSERT INTO `wk8pgw_term_taxonomy` VALUES('65', '60', 'post_tag', '', '0', '2');
INSERT INTO `wk8pgw_term_taxonomy` VALUES('64', '59', 'post_tag', '', '0', '5');
INSERT INTO `wk8pgw_term_taxonomy` VALUES('71', '66', 'post_tag', '', '0', '1');
INSERT INTO `wk8pgw_term_taxonomy` VALUES('70', '65', 'post_tag', '', '0', '1');
INSERT INTO `wk8pgw_term_taxonomy` VALUES('59', '54', 'post_tag', '', '0', '4');
INSERT INTO `wk8pgw_term_taxonomy` VALUES('60', '55', 'post_tag', '', '0', '1');
INSERT INTO `wk8pgw_term_taxonomy` VALUES('72', '67', 'post_tag', '', '0', '2');
INSERT INTO `wk8pgw_term_taxonomy` VALUES('73', '68', 'post_tag', '', '0', '5');
INSERT INTO `wk8pgw_term_taxonomy` VALUES('74', '69', 'post_tag', '', '0', '3');
INSERT INTO `wk8pgw_term_taxonomy` VALUES('75', '70', 'post_tag', '', '0', '1');
INSERT INTO `wk8pgw_term_taxonomy` VALUES('76', '71', 'post_tag', '', '0', '2');
INSERT INTO `wk8pgw_term_taxonomy` VALUES('77', '72', 'post_tag', '', '0', '2');
INSERT INTO `wk8pgw_term_taxonomy` VALUES('78', '73', 'post_tag', '', '0', '1');
INSERT INTO `wk8pgw_term_taxonomy` VALUES('79', '74', 'post_tag', '', '0', '3');
INSERT INTO `wk8pgw_term_taxonomy` VALUES('80', '75', 'post_tag', '', '0', '2');
INSERT INTO `wk8pgw_term_taxonomy` VALUES('81', '76', 'post_tag', '', '0', '2');
INSERT INTO `wk8pgw_term_taxonomy` VALUES('82', '77', 'post_tag', '', '0', '2');
INSERT INTO `wk8pgw_term_taxonomy` VALUES('83', '78', 'post_tag', '', '0', '2');
INSERT INTO `wk8pgw_term_taxonomy` VALUES('84', '79', 'post_tag', '', '0', '2');
INSERT INTO `wk8pgw_term_taxonomy` VALUES('85', '80', 'post_tag', '', '0', '1');
INSERT INTO `wk8pgw_term_taxonomy` VALUES('86', '81', 'post_tag', '', '0', '1');
INSERT INTO `wk8pgw_term_taxonomy` VALUES('87', '82', 'post_tag', '', '0', '2');
INSERT INTO `wk8pgw_term_taxonomy` VALUES('88', '83', 'post_tag', '', '0', '1');
INSERT INTO `wk8pgw_term_taxonomy` VALUES('89', '84', 'post_tag', '', '0', '2');
INSERT INTO `wk8pgw_term_taxonomy` VALUES('90', '85', 'post_tag', '', '0', '2');
INSERT INTO `wk8pgw_term_taxonomy` VALUES('91', '86', 'post_tag', '', '0', '1');
INSERT INTO `wk8pgw_term_taxonomy` VALUES('92', '87', 'post_tag', '', '0', '2');
INSERT INTO `wk8pgw_term_taxonomy` VALUES('93', '88', 'post_tag', '', '0', '2');
INSERT INTO `wk8pgw_term_taxonomy` VALUES('94', '89', 'post_tag', '', '0', '1');
INSERT INTO `wk8pgw_term_taxonomy` VALUES('95', '90', 'post_tag', '', '0', '1');
INSERT INTO `wk8pgw_term_taxonomy` VALUES('96', '91', 'post_tag', '', '0', '1');
INSERT INTO `wk8pgw_term_taxonomy` VALUES('97', '92', 'post_tag', '', '0', '1');
INSERT INTO `wk8pgw_term_taxonomy` VALUES('98', '93', 'post_tag', '', '0', '1');
INSERT INTO `wk8pgw_term_taxonomy` VALUES('99', '94', 'post_tag', '', '0', '1');
INSERT INTO `wk8pgw_term_taxonomy` VALUES('100', '95', 'post_tag', '', '0', '1');
INSERT INTO `wk8pgw_term_taxonomy` VALUES('101', '96', 'post_tag', '', '0', '1');
INSERT INTO `wk8pgw_term_taxonomy` VALUES('102', '100', 'post_tag', '', '0', '1');
INSERT INTO `wk8pgw_term_taxonomy` VALUES('103', '101', 'post_tag', '', '0', '1');
INSERT INTO `wk8pgw_term_taxonomy` VALUES('104', '102', 'post_tag', '', '0', '1');
INSERT INTO `wk8pgw_term_taxonomy` VALUES('105', '103', 'post_tag', '', '0', '1');
INSERT INTO `wk8pgw_term_taxonomy` VALUES('106', '104', 'post_tag', '', '0', '1');
INSERT INTO `wk8pgw_term_taxonomy` VALUES('107', '105', 'post_tag', '', '0', '1');
INSERT INTO `wk8pgw_term_taxonomy` VALUES('108', '106', 'post_tag', '', '0', '1');
INSERT INTO `wk8pgw_term_taxonomy` VALUES('109', '107', 'post_tag', '', '0', '1');
INSERT INTO `wk8pgw_term_taxonomy` VALUES('110', '108', 'post_tag', '', '0', '1');
INSERT INTO `wk8pgw_term_taxonomy` VALUES('111', '109', 'post_tag', '', '0', '3');
INSERT INTO `wk8pgw_term_taxonomy` VALUES('112', '110', 'post_tag', '', '0', '1');
INSERT INTO `wk8pgw_term_taxonomy` VALUES('113', '111', 'post_tag', '', '0', '1');
INSERT INTO `wk8pgw_term_taxonomy` VALUES('114', '112', 'post_tag', '', '0', '1');
INSERT INTO `wk8pgw_term_taxonomy` VALUES('115', '113', 'post_tag', '', '0', '1');
INSERT INTO `wk8pgw_term_taxonomy` VALUES('116', '114', 'post_tag', '', '0', '1');
INSERT INTO `wk8pgw_term_taxonomy` VALUES('117', '115', 'post_tag', '', '0', '1');
INSERT INTO `wk8pgw_term_taxonomy` VALUES('118', '116', 'post_tag', '', '0', '1');
INSERT INTO `wk8pgw_term_taxonomy` VALUES('119', '117', 'post_tag', '', '0', '1');
INSERT INTO `wk8pgw_term_taxonomy` VALUES('120', '118', 'post_tag', '', '0', '1');
INSERT INTO `wk8pgw_term_taxonomy` VALUES('121', '119', 'post_tag', '', '0', '1');
INSERT INTO `wk8pgw_term_taxonomy` VALUES('122', '120', 'post_tag', '', '0', '1');
INSERT INTO `wk8pgw_term_taxonomy` VALUES('123', '121', 'post_tag', '', '0', '1');
INSERT INTO `wk8pgw_term_taxonomy` VALUES('124', '122', 'post_tag', '', '0', '1');
INSERT INTO `wk8pgw_term_taxonomy` VALUES('125', '123', 'post_tag', '', '0', '1');
INSERT INTO `wk8pgw_term_taxonomy` VALUES('126', '124', 'post_tag', '', '0', '1');
INSERT INTO `wk8pgw_term_taxonomy` VALUES('127', '125', 'post_tag', '', '0', '1');
INSERT INTO `wk8pgw_term_taxonomy` VALUES('128', '126', 'post_tag', '', '0', '1');
/*!40000 ALTER TABLE `wk8pgw_term_taxonomy` ENABLE KEYS */;
SET FOREIGN_KEY_CHECKS = 1;
SET UNIQUE_CHECKS = 1;
