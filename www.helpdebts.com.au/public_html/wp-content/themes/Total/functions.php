<?php
/**
 * Total functions and definitions.
 * Text Domain: wpex
 *
 * When using a child theme (see http://codex.wordpress.org/Theme_Development
 * and http://codex.wordpress.org/Child_Themes), you can override certain
 * functions (those wrapped in a function_exists() call) by defining them first
 * in your child theme's functions.php file. The child theme's functions.php
 * file is included before the parent theme's file, so the child theme
 * functions would be used.
 *
 * For more information on hooks, actions, and filters,
 * see http://codex.wordpress.org/Plugin_API
 *
 * Total is a very powerful theme and virtually anything can be customized
 * via a child theme. If you need any help altering a function, just let us know!
 * Customizations aren't included for free but if it's a simple task I'll be sure to help :)
 * 
 * @package	Total
 * @author Alexander Clarke
 * @copyright Copyright (c) 2014, Symple Workz LLC
 * @link http://www.wpexplorer.com
 * @since Total 1.0
 */

/*-----------------------------------------------------------------------------------*/
/*	- Define Constants
/*-----------------------------------------------------------------------------------*/

// Assets
define( 'WPEX_JS_DIR_URI', get_template_directory_uri() .'/js/' );
define( 'WPEX_CSS_DIR_UIR', get_template_directory_uri() .'/css/' );

// Skins
define( 'WPEX_SKIN_DIR', get_template_directory() .'/skins/' );
define( 'WPEX_SKIN_DIR_URI', get_template_directory_uri() .'/skins/' );

// Framework functions & classes
define( 'WPEX_FRAMEWORK_DIR', get_template_directory() .'/framework/' );
define( 'WPEX_FRAMEWORK_DIR_URI', get_template_directory_uri() .'/framework/' );

// Visual Composer Extend
define( 'WPEX_VCEX_DIR', get_template_directory() .'/framework/visual-composer/extend/' );
define( 'WPEX_VCEX_DIR_URI', get_template_directory_uri() .'/framework/visual-composer/extend/' );

// Check if in the admin
$wpex_is_admin = is_admin();


/*-----------------------------------------------------------------------------------*/
/*	- Globals + Theme Setup + Main Skin
/*-----------------------------------------------------------------------------------*/

// Core Functions, these are functions used througout the theme and need to load first
require_once( WPEX_FRAMEWORK_DIR .'core-functions.php' );

// HTML elements outputted with PHP to make theming/RTL easier
require_once( WPEX_FRAMEWORK_DIR .'elements.php' );

// Image overlays - Load first because its needed in the admin
require_once( WPEX_FRAMEWORK_DIR .'loops/overlays.php' );

// Theme setup - load_theme_domain, add_theme_support, register_nav_menus
require_once( WPEX_FRAMEWORK_DIR .'theme-setup.php' );

// Recommend some useful plugins for this theme via TGMA script
require_once( get_template_directory() .'/plugins/class-tgm-plugin-activation.php' );
require_once( get_template_directory() .'/plugins/recommend-plugins.php' );


/*-----------------------------------------------------------------------------------*/
/*	- Branding & Theme Skin
/*-----------------------------------------------------------------------------------*/

// Define branding constant based on theme options
if ( !function_exists( 'wpex_theme_branding') ) {
	function wpex_theme_branding() {
		return wpex_option( 'theme_branding', 'Total' );
	}
}
define( 'WPEX_THEME_BRANDING', wpex_theme_branding() );

// Main Skins functions
require_once( WPEX_SKIN_DIR . 'skins.php' );

// Font Awesome array include at the top for use in admin
require_once( WPEX_FRAMEWORK_DIR .'font-awesome.php' );


/*-----------------------------------------------------------------------------------*/
/*	- ReduxFramework Admin Panel
/*-----------------------------------------------------------------------------------*/

if ( function_exists( 'wpex_supports' ) && wpex_supports( 'primary', 'admin' ) ) {

	// Remove Redux tracking
	require_once( WPEX_FRAMEWORK_DIR .'redux/remove-tracking-class.php' );

	// Include the Redux theme options Framework
	if ( !class_exists( 'ReduxFramework' ) ) {
		require_once( WPEX_FRAMEWORK_DIR .'redux/redux-core/framework.php' );
	}

	// Extensions loader
	require_once( WPEX_FRAMEWORK_DIR. 'redux/extensions-loader.php' );

	// Redux metaboxes
	require_once( WPEX_FRAMEWORK_DIR. 'redux/meta-config.php' );

	// Redux widgets
	require_once( WPEX_FRAMEWORK_DIR. 'redux/widgets-config.php' );

	// Register all the maintheme options
	require_once( WPEX_FRAMEWORK_DIR . 'redux/admin-config.php' );

}


/*-----------------------------------------------------------------------------------*/
/*	- Active Post Types
/*-----------------------------------------------------------------------------------*/

// Return active custom post types
if ( !function_exists( 'wpex_active_post_types' ) ) {
	function wpex_active_post_types() {
		if ( ! function_exists( 'wpex_theme_post_types' ) ) {
			return;
		}
		$theme_post_types = wpex_theme_post_types();
		$active_post_types = array();
		if ( is_array( $theme_post_types ) ) {
			foreach ( $theme_post_types as $post_type ) {
				if ( wpex_option( $post_type .'_enable', '1' ) ) {
					$active_post_types[$post_type ] = $post_type;
				}
			}
		}
		return apply_filters( 'wpex_active_post_types', $active_post_types );
	}
}
// Set global active post types var
$wpex_active_post_types = wpex_active_post_types();


/*-----------------------------------------------------------------------------------*/
/*	- Hooks - VERY IMPORTANT - DONT DELETE EVER!!
/*-----------------------------------------------------------------------------------*/
if ( ! $wpex_is_admin ) {
	require_once( WPEX_FRAMEWORK_DIR .'hooks/hooks.php' );
	require_once( WPEX_FRAMEWORK_DIR .'hooks/actions.php' );
}


/*-----------------------------------------------------------------------------------*/
/*	- CSS / JS
/*	- Loads all the core css and js for this theme
/*-----------------------------------------------------------------------------------*/
require_once( WPEX_FRAMEWORK_DIR .'scripts.php' );


/*-----------------------------------------------------------------------------------*/
/*	- Site Styling, Layout, Custom Fonts
/*-----------------------------------------------------------------------------------*/

// Outputs css for theme panel styling options
require_once( WPEX_FRAMEWORK_DIR .'styling.php' );

// Outputs css for theme panel layout options
require_once( WPEX_FRAMEWORK_DIR .'custom-layout.php' );

// Outputs css for theme panel responsive width options
require_once( WPEX_FRAMEWORK_DIR .'responsive-widths.php' );

// Outputs custom css from theme options
require_once( WPEX_FRAMEWORK_DIR .'custom-css.php' );

// Outputs typography css - fonts
require_once( WPEX_FRAMEWORK_DIR .'typography.php' );

// Used to add CSS to the <head> tag for your custom background settings
require_once( WPEX_FRAMEWORK_DIR .'site-background.php' );

// Custom backgrounds on a per-post basis
require_once( WPEX_FRAMEWORK_DIR .'backgrounds/page-backgrounds.php' );

// Outputs all custom CSS in WP_Head
require_once( WPEX_FRAMEWORK_DIR .'output-css.php' );

// Custom Login screen design
require_once( WPEX_FRAMEWORK_DIR .'custom-login-screen.php' );


/*-----------------------------------------------------------------------------------*/
/*	- Global/Core Functions
/*-----------------------------------------------------------------------------------*/

// A few small optimizations for speed
// clean up the <head>, remove useless jetpack scripts, etc.
require_once( WPEX_FRAMEWORK_DIR .'optimizations.php');

// Adds classes to the body tag
require_once( WPEX_FRAMEWORK_DIR .'body-classes.php' );

// Resize and crop images
require_once( WPEX_FRAMEWORK_DIR .'thumbnails/image-resize.php' );

// Layerslider mods
require_once( WPEX_FRAMEWORK_DIR .'layerslider/layerslider-mods.php' );

// Theme Shortcodes
require_once( WPEX_FRAMEWORK_DIR .'thumbnails/honor-ssl-for-attachments.php' );

// Theme Shortcodes
require_once( WPEX_FRAMEWORK_DIR .'shortcodes/shortcodes.php' );

// Add fields to media items
require_once( WPEX_FRAMEWORK_DIR .'thumbnails/media-fields.php' );

// Load these functions in the backend only
if ( $wpex_is_admin ) {

	// TinyMCE buttons & edits
	require_once( WPEX_FRAMEWORK_DIR .'tinymce/mce-tweaks.php' );

	// Ads meta options for users
	require_once( WPEX_FRAMEWORK_DIR .'users/user-meta.php' );

	// Gallery metabox function used to define images for your gallery post format
	require_once( WPEX_FRAMEWORK_DIR .'meta/gallery-metabox/gmb-admin.php' );

	// Function used to display featured images in your dashboard columns
	require_once( WPEX_FRAMEWORK_DIR .'thumbnails/dashboard-thumbnails.php' );

	// Meta fields for Standard Categories
	require_once( WPEX_FRAMEWORK_DIR .'meta/taxonomies/category-meta.php');

}

// Load these on the front-end only
else {

	// Outputs code in the <head> tag for your favicons
	require_once( WPEX_FRAMEWORK_DIR .'favicon-apple-icons.php' );

	// Outputs your tracking code in the <head> tag
	require_once( WPEX_FRAMEWORK_DIR .'tracking-code.php' );

	// Get the correct Template parts
	require_once( WPEX_FRAMEWORK_DIR .'loops/template-parts.php' );
	
	// Used to tweak the_excerpt() function and also defines the wpex_excerpt() function
	require_once( WPEX_FRAMEWORK_DIR .'excerpts.php' );

	// Returns the correct cropped or non-cropped featured image URLs - Requires that the AquaResizer is called first
	require_once( WPEX_FRAMEWORK_DIR .'thumbnails/featured-images.php');

	// Returns featured image caption
	require_once( WPEX_FRAMEWORK_DIR .'thumbnails/featured-image-caption.php');

	// Returns the correct grid class based on column numbers
	require_once( WPEX_FRAMEWORK_DIR .'grid.php' );
		
	// Retuns the correct post layout class
	require_once( WPEX_FRAMEWORK_DIR .'post-layout.php' );

	// Alter the default output of the WordPress gallery shortcode
	if ( wpex_option( 'custom_wp_gallery', '1' ) ) {
		require_once( WPEX_FRAMEWORK_DIR .'wp-gallery.php');
	}

	// Used to display images defined by the gallery metabox function
	require_once( WPEX_FRAMEWORK_DIR .'meta/gallery-metabox/gmb-display.php' );

}


/*-----------------------------------------------------------------------------------*/
/*	- Top Bar & Toggle Bar
/*-----------------------------------------------------------------------------------*/

// Not needed in the backend
if ( !$wpex_is_admin ) {

	// Displays the site toggle bar
	require_once( WPEX_FRAMEWORK_DIR .'toggle-bar.php' );
	
	// Displays the site top-bar
	require_once( WPEX_FRAMEWORK_DIR .'header/top-bar.php' );

}


/*-----------------------------------------------------------------------------------*/
/*	- Search Functions
/*-----------------------------------------------------------------------------------*/

// Not needed in the backend
if ( !$wpex_is_admin ) {

	// Main search functions
	require_once( WPEX_FRAMEWORK_DIR .'search/search-functions.php' );

	// Adds a search icon at the end of the menu
	require_once( WPEX_FRAMEWORK_DIR .'search/search-menu-icon.php' );

	// Get the correct header search toggle style
	if ( 'overlay' ==  wpex_option( 'main_search_toggle_style', 'overlay' ) ) {
		require_once( WPEX_FRAMEWORK_DIR .'search/search-overlay.php');
	} elseif ( 'drop_down' ==  wpex_option( 'main_search_toggle_style', 'overlay' ) ) {
		require_once( WPEX_FRAMEWORK_DIR .'search/search-dropdown.php');
	} elseif ( 'header_replace' ==  wpex_option( 'main_search_toggle_style', 'overlay' ) ) {
		require_once( WPEX_FRAMEWORK_DIR .'search/search-header-replace.php');
	}

}


/*-----------------------------------------------------------------------------------*/
/*	- Header functions
/*-----------------------------------------------------------------------------------*/

// Not needed in the backend
if ( !$wpex_is_admin ) {

	// Header meta tags
	require_once( WPEX_FRAMEWORK_DIR .'header/meta-tags.php' );
	
	// Header Overlay functions
	require_once( WPEX_FRAMEWORK_DIR .'header/header-overlay.php' );

	// Returns correct header style
	require_once( WPEX_FRAMEWORK_DIR .'header/header-style.php' );
	
	// Adds custom classes to the header container
	require_once( WPEX_FRAMEWORK_DIR .'header/header-classes.php' );
	
	// Outputs the header logo
	require_once( WPEX_FRAMEWORK_DIR .'header/header-logo.php' );

	// Header Output
	require_once( WPEX_FRAMEWORK_DIR .'header/header-output.php' );
	
	// Outputs the header logo
	require_once( WPEX_FRAMEWORK_DIR .'header/header-aside.php' );

}


/*-----------------------------------------------------------------------------------*/
/*	- Menu functions
/*-----------------------------------------------------------------------------------*/

// Load these functions in the backend only
if ( $wpex_is_admin ) {

	// Faster Menu Dashboard
	require_once( WPEX_FRAMEWORK_DIR .'faster-menu-dashboard.php' );
}

// Load these on the front-end only
else {

	// Outputs the header menu
	require_once( WPEX_FRAMEWORK_DIR .'header/menu/menu-output.php' );
	
	// Custom menu walker used to add classes & icons to menus
	require_once( WPEX_FRAMEWORK_DIR .'header/menu/menu-walker.php');
	
	// Outputs HTML and loads scripts for the responsive toggle menu
	require_once( WPEX_FRAMEWORK_DIR .'header/menu/menu-mobile.php' );

}


/*-----------------------------------------------------------------------------------*/
/*	- Widgets & Sidebar
/*-----------------------------------------------------------------------------------*/

// Returns the correct sidebar region depending on the post/page/archive and theme settings
require_once( WPEX_FRAMEWORK_DIR .'widgets/get-sidebar.php' );

// The footer widgets
require_once( WPEX_FRAMEWORK_DIR .'footer/footer-widgets.php' );

// Some cool function functions to tweak widgets
require_once( WPEX_FRAMEWORK_DIR .'widgets/widget-tweaks.php' );

// Define the widget areas for this theme
require_once( WPEX_FRAMEWORK_DIR .'widgets/widget-areas.php' );

// Social widget - image icons
require_once( WPEX_FRAMEWORK_DIR .'widgets/custom-widgets/widget-social.php' );

// Social widget - font awesome
require_once( WPEX_FRAMEWORK_DIR .'widgets/custom-widgets/widget-social-fontawesome.php' );

// Simple menu widget
require_once( WPEX_FRAMEWORK_DIR .'widgets/custom-widgets/widget-simple-menu.php' );

// Flickr widget
require_once( WPEX_FRAMEWORK_DIR .'widgets/custom-widgets/widget-flickr.php' );

// Responsive video widget
require_once( WPEX_FRAMEWORK_DIR .'widgets/custom-widgets/widget-video.php' );

// Post thumbnails widget
require_once( WPEX_FRAMEWORK_DIR .'widgets/custom-widgets/widget-posts-thumbnails.php' );

// Posts grid widget
require_once( WPEX_FRAMEWORK_DIR .'widgets/custom-widgets/widget-recent-posts-thumb-grid.php' );

// Recent posts with format icons
require_once( WPEX_FRAMEWORK_DIR .'widgets/custom-widgets/widget-posts-icons.php' );

// Recent comments with avatar
require_once( WPEX_FRAMEWORK_DIR .'widgets/custom-widgets/widget-comments-avatar.php' );


/*-----------------------------------------------------------------------------------*/
/*	- Blog functions
/*-----------------------------------------------------------------------------------*/

// Not needed in the backend
if ( !$wpex_is_admin ) {

	// Useful blog functions
	require_once( WPEX_FRAMEWORK_DIR .'blog/blog-functions.php' );

	// Displays content for the blog entries
	require_once( WPEX_FRAMEWORK_DIR .'blog/blog-entry.php' );
	
	// Adds custom classes to blog wraps based on blog styles
	require_once( WPEX_FRAMEWORK_DIR .'blog/blog-classes.php' );
	
	// Exclude blog categories from the main blog page / index
	require_once( WPEX_FRAMEWORK_DIR .'blog/blog-exclude-categories.php' );

	// Displays related blog posts
	require_once( WPEX_FRAMEWORK_DIR .'blog/blog-related.php' );

}


/*-----------------------------------------------------------------------------------*/
/*	- Footer functions
/*-----------------------------------------------------------------------------------*/

// Not needed in the backend
if ( !$wpex_is_admin ) {

	// Show/hide the main footer depending on current post meta
	require_once( WPEX_FRAMEWORK_DIR .'footer/footer-display.php' );
	
	// Show/hide the footer callout depending on current post meta
	require_once( WPEX_FRAMEWORK_DIR .'footer/footer-callout.php' );
	
	// Displays the copyright info in the footer
	require_once( WPEX_FRAMEWORK_DIR .'footer/footer-bottom.php' );
	
	// Scroll to top link function
	require_once( WPEX_FRAMEWORK_DIR .'footer/scroll-top-link.php');

}


/*-----------------------------------------------------------------------------------*/
/*	- Other functions
/*-----------------------------------------------------------------------------------*/

// Non Admin functions
if ( ! $wpex_is_admin ) {

	// Responsive videos support
	require_once( WPEX_FRAMEWORK_DIR .'responsive-videos.php' );

	// Increase the quality of resized jpgs
	require_once( WPEX_FRAMEWORK_DIR .'thumbnails/better-jpgs.php' );

	// Displays the number of queries and memory on page load
	require_once( WPEX_FRAMEWORK_DIR .'troubleshooting/display-queries-memory.php' );
	
	// Built-in breadcrumbs function
	require_once( WPEX_FRAMEWORK_DIR .'breadcrumbs.php' );
	
	// The main page title class - displays title/breadcrumbs/title backgrounds/subheading - etc.
	require_once( WPEX_FRAMEWORK_DIR .'page-header.php' );
	
	// Pagination functions - default, infinite scroll and next/prev
	require_once( WPEX_FRAMEWORK_DIR .'pagination.php' );
	
	// Next & Previous single post pagination
	require_once( WPEX_FRAMEWORK_DIR .'next-prev.php' );
	
	// Outputs the post meta for blog posts & entries
	require_once( WPEX_FRAMEWORK_DIR .'post-meta.php' );
	
	// Function used to alter the number of posts displayed for your custom post type archives
	require_once( WPEX_FRAMEWORK_DIR .'posts-per-page.php' );
	
	// Comments callback function
	require_once( WPEX_FRAMEWORK_DIR .'comments-callback.php');
	
	// Used to display your post slider as defined in the wpex_post_slider meta value
	require_once( WPEX_FRAMEWORK_DIR .'post-slider.php' );
	
	// Outputs the social sharing icons for posts and pages
	require_once( WPEX_FRAMEWORK_DIR .'social/social-share.php' );
	
	// Add bbPress post type to search
	require_once( WPEX_FRAMEWORK_DIR .'bbpress/bbpress-search.php' );

	// Check if user has social options
	require_once( WPEX_FRAMEWORK_DIR .'users/user-has-social.php' );
	
} // End else - $wpex_is_admin

// Alter the password protection form
require_once( WPEX_FRAMEWORK_DIR .'password-protection-form.php' );


/*-----------------------------------------------------------------------------------*/
/*	- Add Classes
/*-----------------------------------------------------------------------------------*/

// Non-Admin functions
if ( ! $wpex_is_admin ) {
	// Adds additional classes to the body tag
	require_once( WPEX_FRAMEWORK_DIR .'body-classes.php' );

	// Adds additional classes to your posts
	require_once( WPEX_FRAMEWORK_DIR .'post-classes.php' );
}


/*-----------------------------------------------------------------------------------*/
/*	- Portfolio Post Type
/*-----------------------------------------------------------------------------------*/

if ( in_array( 'portfolio', $wpex_active_post_types ) ) {
	
	// Register the portfolio post type
	require_once( WPEX_FRAMEWORK_DIR .'posttypes-taxonomies/register-portfolio.php' );

	// Useful Portfolio functions
	require_once( WPEX_FRAMEWORK_DIR .'portfolio/portfolio-functions.php' );

	if ( ! $wpex_is_admin ) {

		// Displays portfolio media on single posts
		require_once( WPEX_FRAMEWORK_DIR .'portfolio/portfolio-single-media.php' );
		
		// Displays an array of portfolio categories
		require_once( WPEX_FRAMEWORK_DIR .'portfolio/portfolio-categories.php' );
		
		// Displays related portfolio items
		require_once( WPEX_FRAMEWORK_DIR .'portfolio/portfolio-related.php' );

		// Portfolio Entry Content
		require_once( WPEX_FRAMEWORK_DIR .'portfolio/portfolio-entry.php' );

	}

}


/*-----------------------------------------------------------------------------------*/
/*	- Post Series custom Taxonomy
/*-----------------------------------------------------------------------------------*/

// Registers the post series custom taxonomy for the standard post type
if ( wpex_option( 'post_series', '1' ) ) {
	require_once( WPEX_FRAMEWORK_DIR .'posttypes-taxonomies/register-post-series.php' );
}

// Outputs the post series on posts
require_once( WPEX_FRAMEWORK_DIR .'blog/post-series.php' );


/*-----------------------------------------------------------------------------------*/
/*	- Staff Post Type
/*-----------------------------------------------------------------------------------*/

if ( in_array( 'staff', $wpex_active_post_types ) ) {
	
	// Register the staff custom post type
	require_once( WPEX_FRAMEWORK_DIR .'posttypes-taxonomies/register-staff.php' );
	
}

// Useful staff functions
require_once( WPEX_FRAMEWORK_DIR .'staff/staff-functions.php' );

if ( ! $wpex_is_admin ) {

	// Displays portfolio media on single posts
	require_once( WPEX_FRAMEWORK_DIR .'staff/staff-single-media.php' );
	
	// Related staff posts
	require_once( WPEX_FRAMEWORK_DIR .'staff/staff-related.php' );

	// Staff Entry Details
	require_once( WPEX_FRAMEWORK_DIR .'staff/staff-entry.php' );

}


/*-----------------------------------------------------------------------------------*/
/*	- Testimonials Post Type
/*-----------------------------------------------------------------------------------*/

if ( in_array( 'testimonials', $wpex_active_post_types ) ) {
	
	// Register the testimonials custom post type
	require_once( WPEX_FRAMEWORK_DIR .'posttypes-taxonomies/register-testimonials.php' );
	
}


/*-----------------------------------------------------------------------------------*/
/*	- Custom Post Type & Taxonomy Functions
/*-----------------------------------------------------------------------------------*/

if ( ! empty( $wpex_active_post_types ) ) {

	// Function used to alter your post type labels via theme options
	require_once( WPEX_FRAMEWORK_DIR .'posttypes-taxonomies/post-type-args.php' );

	// Function used to alter your taxonomy labels via theme options
	require_once( WPEX_FRAMEWORK_DIR .'posttypes-taxonomies/taxonomies-args.php' );

	// Tweak custom post types based on theme options
	require_once( WPEX_FRAMEWORK_DIR .'posttypes-taxonomies/remove-slugs.php' );

}


/*-----------------------------------------------------------------------------------*/
/*	- WooCommerce
/*-----------------------------------------------------------------------------------*/

// WooCommerce specific functions
if ( class_exists( 'Woocommerce' ) ) {
	
	if ( ! $wpex_is_admin ) {
		
		// Remove Woo scripts
		require_once( WPEX_FRAMEWORK_DIR .'woocommerce/woo-scripts.php' );
		
		// Alter WooCommerce columns/pagination
		require_once( WPEX_FRAMEWORK_DIR .'woocommerce/woo-columns.php' );
		
		// Change default Woo Image sizes
		require_once( WPEX_FRAMEWORK_DIR .'woocommerce/woo-images.php' );
		
		// Product entry media - featured image / slider / image swap
		require_once( WPEX_FRAMEWORK_DIR .'woocommerce/woo-product-entry-media.php' );
		
		// Overrides the default WooCommerce category image output
		require_once( WPEX_FRAMEWORK_DIR .'woocommerce/woo-category-image.php' );

		// Other Woo Tweaks
		require_once( WPEX_FRAMEWORK_DIR .'woocommerce/woo-other-tweaks.php' );
	
	}

	// Display shopping cart $ in the nav and get header cart functions
	// Must be loaded on backend and front-end due to WooCommerce AJAX
	if ( wpex_option( 'woo_menu_icon', '1' ) ) {
		require_once( WPEX_FRAMEWORK_DIR .'woocommerce/woo-menucart.php' );
		if ( ! $wpex_is_admin ) {
			require_once( WPEX_FRAMEWORK_DIR .'woocommerce/woo-cartwidget-overlay.php' );
			require_once( WPEX_FRAMEWORK_DIR .'woocommerce/woo-cartwidget-dropdown.php' );
		}
	}

}


/*-----------------------------------------------------------------------------------*/
/*	- Visual Composer Extension
/*	- Adds more useful modules to the Visual Composer
/*-----------------------------------------------------------------------------------*/
if( function_exists( 'vc_set_as_theme' ) ) {

	// Useful functions for extending and tweaking the visual composer
	require_once( WPEX_FRAMEWORK_DIR .'visual-composer/arrays.php' );

	// Check if the Visual composer plugin is active and if so display a message
	if ( function_exists( 'visual_composer_extension_run' ) ) {
		function vcex_admin_notice() { ?>
			<div class="error">
				<h4><?php _e( 'IMPORTANT NOTICE', 'wpex' ); ?></h4>
				<p><?php _e( 'The Visual Composer Extension Plugin (not WPBakery VC but the extension created by WPExplorer) for this theme is now built-in, please de-activate and if you want delete the plugin.', 'wpex' ); ?>
				<br /><br />
				<a href="<?php echo admin_url( 'plugins.php?plugin_status=active' ); ?>" class="button button-primary" target="_blank"><?php _e( 'Deactivate', 'wpex' ); ?> &rarr;</a>
				<a href="http://themeforest.net/item/total-responsive-multipurpose-wordpress-theme/6339019/faqs/20545" class="button button-primary" target="_blank"><?php _e( 'Learn More', 'wpex' ); ?> &rarr;</a></p>
				<p></p>
			</div>
		<?php
		}
		add_action( 'admin_notices', 'vcex_admin_notice' );
	}

	// Extend the visual composer
	if ( wpex_option( 'extend_visual_composer_extension', '1' ) ) {
		require_once( WPEX_FRAMEWORK_DIR .'visual-composer/extend/extend.php' );
	}
}


/*-----------------------------------------------------------------------------------*/
/*	- Visual Composer Tweaks
/*-----------------------------------------------------------------------------------*/

// Set composer settings pages as settings page.
if( function_exists( 'vc_set_as_theme' ) ) {
	
	// Set Visual Composer to run in Theme Mode
	function wpex_set_vc_as_theme() {
		if ( ! wpex_option( 'visual_composer_theme_mode', '1' ) ) {
			return;
		}
		vc_set_as_theme( $notifier = false );
	}
	add_action( 'init', 'wpex_set_vc_as_theme' );

	// Edit core visual composer functions
	if ( wpex_supports( 'primary', 'visual_composer_edits' ) ) {
	
		// Remove certain default VC modules
		require_once( WPEX_FRAMEWORK_DIR .'visual-composer/core/remove.php' );
		
		// Add new parameters to VC items
		require_once( WPEX_FRAMEWORK_DIR .'visual-composer/core/add-params.php' );
		
		// Visual Composer Filter tweaks
		require_once( WPEX_FRAMEWORK_DIR .'visual-composer/core/filters.php' );

	}

}


/*-----------------------------------------------------------------------------------*/
/*	- WP-Updates
/*	- If envato license is defined & auto updates enabled load the auto updates class
/*-----------------------------------------------------------------------------------*/
if ( wpex_option( 'envato_license_key' ) && wpex_option( 'enable_auto_updates', '0' ) ) {
	require_once( get_template_directory() .'/wp-updates-theme.php');
	new WPUpdatesThemeUpdater_479( 'http://wp-updates.com/api/2/theme', basename( get_template_directory() ), wpex_option( 'envato_license_key' ) );
}