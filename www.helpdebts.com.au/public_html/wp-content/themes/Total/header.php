<?php
/**
 * The Header for our theme.
 *
 * @package Total
 * @author Alexander Clarke
 * @copyright Copyright (c) 2014, Symple Workz LLC
 * @link  http://www.wpexplorer.com
 * @since Total 1.0
 */ ?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<?php wpex_meta_viewport(); ?>
	<?php wpex_meta_title(); ?>
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
	<?php if ( wpex_option('custom_favicon') ) : ?>
		<link rel="shortcut icon" href="<?php echo wpex_option('custom_favicon'); ?>" />
	<?php endif; ?>
	<?php wp_head(); ?>
</head>

<!-- Begin Body -->
<body <?php body_class(); ?>>

<div id="outer-wrap" class="clr">

	<?php
	// Before wrap hook
	wpex_hook_wrap_before(); ?>

	<div id="wrap" class="clr">

		<?php
		// Top wrap hook
		wpex_hook_wrap_top(); ?>
	
		<?php
		/**
		 * Outputs the header and header hooks
 		 * 
 		 * @since 1.53
 		 * @link framework/header/header-output.php
		 */
		wpex_header_output(); ?>
		
	<?php
	// Main before hook
	wpex_hook_main_before(); ?>
	
	<div id="main" class="site-main clr">
	
		<?php
		// Main top hook
		// Page Header & Page Slider functions are added here by default
		wpex_hook_main_top(); ?>