<?php
/**
 * The template used for single staff posts.
 *
 * @package	Total
 * @author Alexander Clarke
 * @copyright Copyright (c) 2014, Symple Workz LLC
 * @link http://www.wpexplorer.com
 * @since Total 1.0
 */

// Get site header
get_header(); ?>

	<?php
	// Start the standard WP loop
	while ( have_posts() ) : the_post(); ?>
		<div id="content-wrap" class="container clr <?php echo wpex_get_post_layout_class(); ?>">
			<section id="primary" class="content-area clr">
				<div id="content" class="site-content clr" role="main">
					<?php
					// Display staff single media if enabled in the admin
					// Disabled by default
					wpex_staff_single_media(); ?>
					<article class="entry clr">
						<?php the_content(); ?>
					</article><!-- .entry clr -->
					<?php
					// Social Sharing links
					wpex_social_share(); ?>
					<?php
					// Get comments & comment form if enabled for portfoliop posts
					if ( wpex_option( 'staff_comments' ) && comments_open() ) { ?>
					<div id="staff-post-comments" class="clr">
						<?php comments_template(); ?>
					</div><!-- #staff-post-comments -->
					<?php } ?>
					<?php
					// Related Portfolio Items
					// See /functions/staff/staff-related.php
					wpex_staff_related(); ?>
				</div><!-- #content -->
			</section><!-- #primary -->
			<?php
			// Get main sidebar
			get_sidebar(); ?>
			<?php
			// Display next and previous post links if enabled
			// See functions/next-prev.php
			if ( wpex_option( 'staff_next_prev', '1' ) ) {
				wpex_next_prev();
			} ?>
		</div><!-- .container -->
	<?php endwhile; ?>

<?php
// Get site footer
get_footer();?>