<?php
/**
 * Useful functions for the standard posts
 *
 * @package Total
 * @subpackage Blog Functions
 * @author Alexander Clarke
 * @copyright Copyright (c) 2014, Symple Workz LLC
 * @link http://www.wpexplorer.com
 * @since Total 1.0
 */

/**
 * Returns the correct blog style
 *
 * @since Total 1.53
 */
if ( ! function_exists( 'wpex_blog_style' ) ) {
	function wpex_blog_style() {
		$style = wpex_option( 'blog_style' );
		if ( is_category() ) {
			$term = get_query_var( 'cat' );
			$term_data = get_option( "category_$term" );
			if ( isset( $term_data['wpex_term_style'] ) && '' != $term_data['wpex_term_style'] ) {
				$style = $term_data['wpex_term_style'] .'-entry-style';
			}
		}
		return $style;
	}
}

/**
 * Returns the grid style
 *
 * @since Total 1.53
 */
if ( ! function_exists( 'wpex_blog_grid_style' ) ) {
	function wpex_blog_grid_style() {
		$style = wpex_option( 'blog_grid_style', 'fit-rows' );
		if ( is_category() ) {
			$term = get_query_var( 'cat' );
			$term_data = get_option( "category_$term" );
			if ( isset( $term_data['wpex_term_grid_style'] ) && '' != $term_data['wpex_term_grid_style'] ) {
				$style = $term_data['wpex_term_grid_style'];
			}
		}
		return $style;
	}
}

/**
 * Checks if it's a fit-rows style grid
 *
 * @since Total 1.53
 */
if ( ! function_exists( 'wpex_blog_fit_rows' ) ) {
	function wpex_blog_fit_rows() {
		if ( wpex_blog_style() == 'grid-entry-style' ) {
			return true;
		} else {
			return false;
		}
	}
}

/**
 * Returns the correct pagination style
 *
 * @since Total 1.53
 */
if ( ! function_exists( 'wpex_blog_pagination_style' ) ) {
	function wpex_blog_pagination_style() {
		$style = wpex_option( 'blog_pagination_style' );
		if ( is_category() ) {
			$term = get_query_var( 'cat' );
			$term_data = get_option( "category_$term" );
			if ( isset( $term_data['wpex_term_pagination'] ) && '' != $term_data['wpex_term_pagination'] ) {
				$style = $term_data['wpex_term_pagination'];
			}
		}
		return $style;
	}
}

/**
 * Returns correct style for the blog entry based on theme options or category options
 *
 * @since Total 1.53
 */
if ( ! function_exists( 'wpex_blog_entry_style' ) ) {
	function wpex_blog_entry_style() {
		$style = wpex_option( 'blog_style', 'large-image-entry-style' );
		if ( is_category() ) {
			$term = get_query_var( "cat" );
			$term_data = get_option( "category_$term" );
			if ( $term_data ) {
				if ( isset( $term_data['wpex_term_style'] ) && '' != $term_data['wpex_term_style'] ) {
					$term_style = $term_data['wpex_term_style'] .'-entry-style';
				}
			}
			if( isset( $term_style ) && '' != $term_style ) {
				$style = $term_style;
			}
		}
		return apply_filters( 'wpex_blog_entry_style', $style );
	}
}

/**
 * Returns correct columns for the blog entries
 *
 * @since Total 1.53
 */
if ( ! function_exists( 'wpex_blog_entry_columns' ) ) {
	function wpex_blog_entry_columns() {
		$columns = wpex_option( 'blog_grid_columns', '2' );
		if ( is_category() ) {
			$term = get_query_var( 'cat' );
			$term_data = get_option( "category_$term" );
			if ( $term_data ) {
				if ( isset( $term_data['wpex_term_grid_cols'] ) && '' != $term_data['wpex_term_grid_cols'] ) {
					$columns = $term_data['wpex_term_grid_cols'];
				}
			}
		}
		return apply_filters( 'wpex_blog_entry_columns', $columns );
	}
}

/**
 * Check if author avatar is enabled or not for blog entries
 *
 * @since Total 1.0
 * @return bool
 */
if ( ! function_exists( 'wpex_post_entry_author_avatar_enabled' ) ) {
	function wpex_post_entry_author_avatar_enabled() {
		
		// Theme settings
		$style = wpex_option ( 'blog_style' );
		
		if ( !wpex_option( 'blog_entry_author_avatar' ) ) {
			return false;
		}
		
		if ( !is_category() && $style !== 'large-image-entry-style' ) {
			return false;
		}
		
		// Category settings
		if ( is_category() ) {
			$term = get_query_var( 'cat' );
			$term_data = get_option( "category_$term" );
			if ( isset($term_data['wpex_term_style']) ) {
				if ( $term_data['wpex_term_style'] !== '' ) {
					$style = $term_data['wpex_term_style'];
				}
			}
			if ( $style !== 'large-image-entry-style' ) {
				return false;
			}
		}
		
		// Return
		return true;

	}
}

/**
 * Returns post video URL
 *
 * @since Total 1.0
 */
if ( ! function_exists( 'wpex_post_video_url' ) ) {
	function wpex_post_video_url() {
		// Oembed
		if ( '' != get_post_meta( get_the_ID(), 'wpex_post_oembed', true ) ) {
			return esc_url( get_post_meta( get_the_ID(), 'wpex_post_oembed', true ) );
		}
		// Self Hosted redux
		$video = get_post_meta( get_the_ID(), 'wpex_post_self_hosted_shortcode_redux', true );
		if ( is_array( $video ) && !empty( $video['url'] ) ) {
			return $video['url'];
		}
		// Self Hosted old - Thunder theme compatibility
		else {
			return get_post_meta( get_the_ID(), 'wpex_post_self_hosted_shortcode', true );
		}
	}
}

/**
 * Returns post audio URL
 *
 * @since Total 1.0
 */
if ( ! function_exists( 'wpex_post_audio_url' ) ) {
	function wpex_post_audio_url() {
		// Oembed
		if ( get_post_meta( get_the_ID(), 'wpex_post_oembed', true ) ) {
			return esc_url( get_post_meta( get_the_ID(), 'wpex_post_oembed', true ) );
		}
		// Self Hosted redux
		$audio = get_post_meta( get_the_ID(), 'wpex_post_self_hosted_shortcode_redux', true );
		if ( is_array( $audio ) && !empty( $audio['url'] ) ) {
			return $audio['url'];
		}
		// Self Hosted old - Thunder theme compatibility
		else {
			return get_post_meta( get_the_ID(), 'wpex_post_self_hosted_shortcode', true );
		}
	}
}