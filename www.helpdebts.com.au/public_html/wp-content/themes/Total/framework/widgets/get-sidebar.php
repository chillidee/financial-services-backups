<?php
/**
 * Returns the correct sidebar region
 *
 * @package WordPress
 * @subpackage Total
 * @since Total 1.0
*/


if ( ! function_exists( 'wpex_get_sidebar' ) ) {
	function wpex_get_sidebar( $sidebar = 'sidebar' ) {

		// Pages
		if ( is_page() && wpex_option( 'pages_custom_sidebar', '1' ) ) {
			if ( !is_page_template('templates/blog.php') ) {
				$sidebar = 'pages_sidebar';
			}
		}

		// Portfolio
		elseif( is_singular( 'portfolio' ) || is_tax( 'portfolio_category' ) || is_tax( 'portfolio_tag' ) ) {
			if ( wpex_option( 'portfolio_custom_sidebar', '1' ) ) {
				$sidebar = 'portfolio_sidebar';
			}
		}

		// Staff
		elseif( is_singular( 'staff' ) || is_tax( 'staff_category' ) || is_tax( 'staff_tag' ) ) {
			if ( wpex_option( 'staff_custom_sidebar', '1' ) ) {
				$sidebar = 'staff_sidebar';
			}
		}

		// Testimonials
		elseif( is_singular( 'testimonials' ) || is_tax( 'testimonials_category' ) || is_tax( 'testimonials_tag' ) ) {
			if ( wpex_option( 'testimonials_custom_sidebar', '1' ) ) {
				$sidebar = 'testimonials_sidebar';
			}
		}

		// Search
		elseif ( is_search() && wpex_option( 'search_custom_sidebar', '1' ) ) {
			$sidebar = 'search_sidebar';
		}

		// WooCommerce
		elseif ( class_exists( 'Woocommerce' ) ) {
			if ( wpex_option( 'woo_custom_sidebar', '1' ) && is_woocommerce() ) {
				$sidebar = 'woo_sidebar';
			}
		}
		
		// bbPress
		elseif ( function_exists('is_bbpress') ) {
			if ( is_bbpress() && wpex_option( 'bbpress_custom_sidebar', '1' ) ) {
				$sidebar = 'bbpress_sidebar';
			}
		}

		// Check meta option as fallback
		if ( $post_id = wpex_get_the_id() ) {
			$sidebar_meta = get_post_meta( $post_id, 'sidebar', true );
			if ( '' != $sidebar_meta && is_active_sidebar( $sidebar_meta ) ) {
				$sidebar = $sidebar_meta;
			}
		}
		
		// Return the correct sidebar name & add useful hook
		$sidebar = apply_filters( 'wpex_get_sidebar', $sidebar );
		return $sidebar;
		
	}
}