<?php
/**
 * Registers the skillbar shortcode and adds it to the Visual Composer
 *
 * @package Total
 * @subpackage Visual Composer
 * @since Total 1.41
 */

if( !function_exists('vcex_spacing_shortcode') ) {
	function vcex_spacing_shortcode( $atts ) {
		extract( shortcode_atts( array(
			'size'			=> '20px',
			'class'			=> '',
			'visibility'	=> '',
		),
		$atts ) );
		if ( wpex_is_front_end_composer() ) {
			return '<div class="vc-spacing-shortcode vcex-spacing '. $class .' '. $visibility .'" style="height: '. $size .'"></div>';
		} else {
			return '<hr class="vcex-spacing '. $class .' '. $visibility .'" style="height: '. $size .'" />';
		}
	}
}
add_shortcode( 'vcex_spacing', 'vcex_spacing_shortcode' );

if ( ! function_exists( 'vcex_spacing_shortcode_vc_map' ) ) {
	function vcex_spacing_shortcode_vc_map() {
		vc_map( array(
			"name"					=> __( "Spacing", 'vces' ),
			"description"			=> __( "Adds spacing anywhere you need it.", 'wpex' ),
			"base"					=> "vcex_spacing",
			'category'				=> WPEX_THEME_BRANDING,
			"icon"					=> "vcex-spacing",
			"params"				=> array(
				array(
					"type"			=> "textfield",
					"admin_label"	=> true,
					"class"			=> "",
					"heading"		=> __( "Spacing", 'wpex' ),
					"param_name"	=> "size",
					"value"			=> "30px",
				),
				array(
					"type"			=> "textfield",
					"admin_label"	=> true,
					"class"			=> "",
					"heading"		=> __( "Classname", 'wpex' ),
					"param_name"	=> "class",
					"value"			=> "",
				),
				array(
					'type'			=> 'dropdown',
					'heading'		=> __( 'Visibility', 'wpex' ),
					'param_name'	=> "visibility",
					'value'			=> wpex_vc_param_arrays( 'visibility' ),
				),
			)
		) );
	}
}
add_action( 'init', 'vcex_spacing_shortcode_vc_map' );