<?php
/**
 * Exit if the vc_add_param function doesn't exist
 *
 * @since Total 1.0
 */
if ( !function_exists( 'vc_add_param' ) ) {
	return;
}

/**
 * Returns arrays for use with the VC params
 *
 * @since Total 1.53
 */
if ( !function_exists( 'wpex_vc_param_arrays' ) ) {
	function wpex_vc_param_arrays( $array ) {
		// Border Styles
		if ( 'border_styles' == $array ) {
			return array(
				__( 'None', 'wpex' )	=> '',
				__( 'Solid', 'wpex' )	=> 'solid',
				__( 'Dotted', 'wpex' )	=> "dotted",
				__( 'Dashed', 'wpex' )	=> "dashed",
			);
		}
		// Alignmnets
		if ( 'alignments' == $array ) {
			return array(
				__( 'None', 'wpex' )			=> 'none',
				__( 'Align left', 'wpex' )		=> 'left',
				__( 'Align right', 'wpex' )		=> 'right',
				__( 'Align center', 'wpex' )	=> 'center',
			);
		}
		// Visibility
		if ( 'visibility' == $array ) {
			return array(
				__( 'All', 'wpex' )						=> '',
				__( 'Hidden on Phones', 'wpex' )		=> "hidden-phone",
				__( 'Hidden on Tablets', 'wpex' )		=> "hidden-tablet",
				__( 'Hidden on Desktop', 'wpex' )		=> "hidden-desktop",
				__( 'Visible on Desktop Only', 'wpex' )	=> "visible-desktop",
				__( 'Visible on Phones Only', 'wpex' )	=> "visible-phone",
				__( 'Visible on Tablets Only', 'wpex' )	=> "visible-tablet",
			);
		}
		// CSS Animation
		if ( 'css_animation' == $array ) {
			return array(
				__( 'None', 'wpex' )				=> '',
				__( 'Top to bottom', 'wpex' )		=> "top-to-bottom",
				__( 'Bottom to top', 'wpex' )		=> "bottom-to-top",
				__( 'Left to right', 'wpex' )		=> "left-to-right",
				__( 'Right to left', 'wpex' )		=> "right-to-left",
				__( 'Appear from center', 'wpex' )	=> "appear",
			);
		}
	}
}