<?php
/**
 * Outputs the single media on staff pages if enabled
 *
 * @package Total
 * @subpackage Portfolio Functions
 * @author Alexander Clarke
 * @copyright Copyright (c) 2014, Symple Workz LLC
 * @link http://www.wpexplorer.com
 * @since Total 1.1
 */

if ( ! function_exists( 'wpex_staff_single_media' ) ) {
	function wpex_staff_single_media() {
		// Return if disabled
		if ( ! wpex_option( 'staff_single_media' ) ) {
			return;
		} ?>
		<div id="staff-single-media" class="clr">
			<?php
			if( has_post_thumbnail() ) {
				$wpex_image = wpex_image( 'array', '', true ); ?>
					<a href="<?php echo wp_get_attachment_url( get_post_thumbnail_id() ); ?>" title="<?php echo esc_attr( the_title_attribute( 'echo=0' ) ); ?>" class="wpex-lightbox">
						<img src="<?php echo $wpex_image['url']; ?>" alt="<?php the_title(); ?>" class="staff-single-media-img" width="<?php echo $wpex_image['width']; ?>" height="<?php echo $wpex_image['height']; ?>" />
					</a>
			<?php } // End if ?>
		</div><!-- .staff-entry-media -->
	<?php }
}