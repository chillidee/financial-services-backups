<?php
/**
 * Mobile menu functions
 *
 * @package Total
 * @subpackage Menu Functions
 * @author Alexander Clarke
 * @copyright Copyright (c) 2014, Symple Workz LLC
 * @link http://www.wpexplorer.com
 * @since Total 1.0
 */

/**
 * Outputs the responsive/mobile menu (icons) for the header
 *
 * @since Total 1.0
 */
if ( ! function_exists( 'wpex_mobile_menu' ) ) {
	function wpex_mobile_menu( $style = '' ) {
		
		// If responsive is disabled, bail
		if( ! wpex_option( 'responsive', '1' ) ) {
			return;
		}
		
		// Vars
		$mobile_menu_open_button_text = '<span class="fa fa-bars"></span>';
		$mobile_menu_open_button_text = apply_filters( 'wpex_mobile_menu_open_button_text', $mobile_menu_open_button_text ); ?>

		<?php
		// Sidr closing div
		if( 'sidr' == wpex_option( 'mobile_menu_style' ) ) { ?>
			<div id="sidr-close"><a href="#sidr-close" class="toggle-sidr-close"></a></div>
		<?php } ?>

		<div id="mobile-menu" class="clr">
			<a href="#mobile-menu" class="mobile-menu-toggle"><?php echo $mobile_menu_open_button_text; ?></a>
			<?php
			// Output icons if the mobile_menu region has a menu defined
			if ( has_nav_menu( 'mobile_menu' ) ) {
				if ( ( $locations = get_nav_menu_locations() ) && isset( $locations[ 'mobile_menu' ] ) ) {
					$menu = wp_get_nav_menu_object( $locations[ 'mobile_menu' ] );
					if ( !empty( $menu ) ) {
						$menu_items = wp_get_nav_menu_items( $menu->term_id );
						foreach ( $menu_items as $key => $menu_item ) {
							// Make sure it's a font-awesome icon
							if( in_array( $menu_item->title, wpex_get_awesome_icons() ) ) {
								$url = $menu_item->url;
								$attr_title = $menu_item->attr_title; ?>
								<a href="<?php echo $url; ?>" title="<?php echo $attr_title; ?>" class="mobile-menu-extra-icons mobile-menu-<?php echo $menu_item->title; ?>">
									<span class="fa fa-<?php echo $menu_item->title; ?>"></span>
								</a>
						<?php }
						}
					}
				}
			} ?>
		</div><!-- #mobile-menu -->
		
	<?php
	}
}

/**
 * Mobile Menu alternative
 *
 * @since Total 1.3
 */
if ( ! function_exists( 'wpex_mobile_menu_alt' ) ) {
	function wpex_mobile_menu_alt() {
		// If responsive is disabled, bail
		if( ! wpex_option( 'responsive', '1' ) ) {
			return;
		}
		// If mobile_menu_alt menu is defined output the menu
		if ( has_nav_menu( 'mobile_menu_alt' ) ) { ?>
			<div id="mobile-menu-alternative"> 
				<?php wp_nav_menu( array(
					'theme_location'	=> 'mobile_menu_alt',
					'menu_class'		=> 'dropdown-menu',
					'fallback_cb'		=> false,
				) ); ?>
			</div><!-- #mobile-menu-alternative -->
		<?php }	
	}
}
add_action( 'wp_footer', 'wpex_mobile_menu_alt' );