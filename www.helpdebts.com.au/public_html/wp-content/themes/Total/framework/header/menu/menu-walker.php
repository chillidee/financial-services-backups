<?php
/**
 * Custom Menu Walker adds classes to dropdowns and dropdown arrows
 *
 * @package Total
 * @subpackage Menu Functions
 * @author Alexander Clarke
 * @copyright Copyright (c) 2014, Symple Workz LLC
 * @link http://www.wpexplorer.com
 * @since Total 1.0
 */

class WPEX_Dropdown_Walker_Nav_Menu extends Walker_Nav_Menu {
	function display_element( $element, &$children_elements, $max_depth, $depth=0, $args, &$output ) {
		$id_field = $this->db_fields['id'];
		// Down Arrows
		if( !empty( $children_elements[$element->$id_field] ) && ( $depth == 0 ) ) {
			$element->classes[] = 'dropdown';
			if ( wpex_option( 'menu_arrow_down', '0' ) ) {
				$element->title .= ' <span class="nav-arrow fa fa-angle-down"></span>';
			}
		}
		// Right/Left Arrows
		if( !empty( $children_elements[$element->$id_field] ) && ( $depth > 0 ) ) {
			$element->classes[] = 'dropdown';
			if ( wpex_option( 'menu_arrow_side', '1' ) ) {
				if( is_rtl() ) {
					$element->title .= '<span class="nav-arrow fa fa-angle-left"></span>';
				} else {
					$element->title .= '<span class="nav-arrow fa fa-angle-right"></span>';
				}
			}
		}
		Walker_Nav_Menu::display_element( $element, $children_elements, $max_depth, $depth, $args, $output );
	}
}