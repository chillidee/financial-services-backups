<?php
/**
 * This function is used to output the site favicons and apple icons
 * Code is echoed into the wp_head hook
 *
 * @package Total
 * @subpackage functions
 * @author Alexander Clarke
 * @copyright Copyright (c) 2014, Symple Workz LLC
 * @link http://www.wpexplorer.com
 * @since Total 1.0
 */

if ( ! function_exists( 'wpex_favicons_apple_icons' ) ) {
	function wpex_favicons_apple_icons() {
		$output = '';
		// Favicon - Standard
		$icon = wpex_option( 'favicon', '', 'url' );
		if ( $icon ) {
			$output .= '<link rel="shortcut icon" href="'. $icon .'">';
		}
		// Apple iPhone Icon - 57px
		$icon = wpex_option( 'iphone_icon', '', 'url' );
		if ( $icon ) {
			$output .= '<link rel="apple-touch-icon-precomposed" href="'. $icon .'">';
		}
		// Apple iPad Icon - 76px
		$icon = wpex_option( 'ipad_icon', '', 'url' );
		if ( $icon ) {
			$output .= '<link rel="apple-touch-icon-precomposed" sizes="76x76" href="'. $icon .'">';
		}
		// Apple iPhone Retina Icon - 120px
		$icon = wpex_option( 'iphone_icon_retina', '', 'url' );
		if ( $icon ) {
			$output .= '<link rel="apple-touch-icon-precomposed" sizes="120x120" href="'. $icon .'">';
		}
		// Apple iPad Retina Icon - 114px
		$icon = wpex_option( 'ipad_icon_retina', '', 'url' );
		if ( $icon ) {
			$output .= '<link rel="apple-touch-icon-precomposed" sizes="114x114" href="'. $icon .'">';
		}
		echo $output;
	}
}
add_action( 'wp_head', 'wpex_favicons_apple_icons', 1 );