<?php
/**
 * Add post type classes to entries
 *
 * @package Total
 * @subpackage functions
 * @author Alexander Clarke
 * @copyright Copyright (c) 2014, Symple Workz LLC
 * @link http://www.wpexplorer.com
 * @since Total 1.0
 */

/**
 * Hooks into the post_class filter to add extra classes for post entries
 * Used for standard post type entries only
 *
 * @link http://codex.wordpress.org/Function_Reference/post_class
 * @since 1.0
 */
if ( ! function_exists( 'wpex_post_entry_classes' ) ) {
	function wpex_post_entry_classes( $classes ) {

		// Return on search and singular
		if ( is_singular() ) {
			return $classes;
		}

		// Search
		elseif( is_search() && 'default' == wpex_search_results_style() ) {
			return $classes;
		}

		// Get post type
		$get_post_type = get_post_type();
				
		// Custom class for post types
		if ( 'post' != $get_post_type && 'product' != $get_post_type ) {
			$classes[] = 'custom-post-type-entry';
			return $classes;
		}

		/*** STANDARD POSTS ***/
		if( 'post' != $get_post_type ) {
			return $classes;
		}
		
		// Main Classes
		$classes[] = 'blog-entry clr';
		
		// No Featured Image Class
		if ( !has_post_thumbnail() && '' == get_post_meta( get_the_ID(), 'wpex_post_self_hosted_shortcode', true ) && '' == get_post_meta( get_the_ID(), 'wpex_post_oembed', true ) ) {
			$classes[] = 'no-featured-image';
		}

		// Masonry
		if( 'masonry' == wpex_blog_grid_style() ) {
			$classes[] = 'isotope-entry';
		}

		// Blog entry style
		// See framework/blog/blog-functions.php
		$blog_style = wpex_blog_entry_style();

		// Add columns for grid style entries
		if ( $blog_style == 'grid-entry-style' ) {
			$classes[] = 'col';
			$classes[] = wpex_grid_class( wpex_blog_entry_columns() );
		}

		// Blog entry style
		$classes[] = $blog_style;
		
		// Return classes	
		return $classes;
		
	}
}
add_filter( 'post_class', 'wpex_post_entry_classes' );