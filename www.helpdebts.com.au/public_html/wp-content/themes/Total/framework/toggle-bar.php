<?php
/**
 * Toggle Bar Output
 *
 * @package Total
 * @subpackage Functions
 * @author Alexander Clarke
 * @copyright Copyright (c) 2014, Symple Workz LLC
 * @link http://www.wpexplorer.com
 * @since Total 1.0
 */

/**
 * Checks if the toggle bar is enabled
 *
 * @since Total 1.0
 * @return bool
 */
if ( ! function_exists( 'wpex_toggle_bar_active' ) ) {
	function wpex_toggle_bar_active( $return = true ) {
		if ( !wpex_option( 'toggle_bar' ) ) {
			$return = false;
		} elseif ( !wpex_option( 'toggle_bar_page' ) ) {
			$return = false;
		} elseif ( ( $post_id = wpex_get_the_id() ) && 'on' == get_post_meta( $post_id, 'wpex_disable_toggle_bar', true ) ) {
			$return = false;
		} else {
			$return = true;
		}
		$return = apply_filters( 'wpex_toggle_bar_active', $return );
		return $return;
	}
}

/**
 * Outputs the toggle main content
 *
 * @since Total 1.0
 */
if ( ! function_exists( 'wpex_toggle_bar' ) ) {
	function wpex_toggle_bar() {
		// Toggle bar disabled or page not selected, lets bail!
		if ( !wpex_toggle_bar_active() ) {
			return;
		}
		// Get toggle bar page content
		$id = wpex_option( 'toggle_bar_page' );
		if ( $id ) {
			// Fix for WPML
			if ( function_exists( 'icl_object_id' ) ) {
				$id = icl_object_id( $id,'page', false,ICL_LANGUAGE_CODE );
			} ?>
			<div id="toggle-bar-wrap" class="clr toggle-bar-<?php echo wpex_option( 'toggle_bar_animation', 'fade' ); ?> <?php echo wpex_option( 'toggle_bar_visibility' ); ?>">
				<div id="toggle-bar" class="clr container">
					<div class="entry clr">
						<?php echo apply_filters( 'the_content', get_post_field( 'post_content', $id ) ); ?>
					</div><!-- .entry -->
				</div><!-- #toggle-bar -->
			</div><!-- #toggle-bar-wrap -->
		<?php
		} // If $id check
	}
}

/**
 * Outputs the toggle bar button
 *
 * @since Total 1.0
 */
if ( ! function_exists( 'wpex_toggle_bar_btn' ) ) {
	function wpex_toggle_bar_btn() {
		if ( !wpex_toggle_bar_active() ) {
			return;
		}
		echo '<a href="#" class="toggle-bar-btn fade-toggle '. wpex_option( 'toggle_bar_visibility' ) .'"><span class="fa fa-plus"></span></a>';
	}
}