<?php
/**
 * This file is used for all the styling options in the admin
 * All custom color options are output to the <head> tag
 *
 * @package Total
 * @subpackage functions
 * @author Alexander Clarke
 * @copyright Copyright (c) 2014, Symple Workz LLC
 * @link http://www.wpexplorer.com
 * @since Total 1.0
 */


if ( ! function_exists( 'wpex_redux_styling_options' ) ) {
	function wpex_redux_styling_options() {

		// Get redux class
		global $wpex_redux_framework_class;

		// Check that getReduxSections method exists
		if ( ! method_exists( $wpex_redux_framework_class, 'getReduxSections' ) ) {
			return;
		}

		// Declare styling options var
		$styling_options = array();

		// Get theme options sections
		$sections = $wpex_redux_framework_class->getReduxSections();

		// Check if $sections var is isset
		if ( isset ( $sections ) ) {

			// Loop through each sections
			foreach ( $sections as $section ) {

				// Check if $section has fields parameter
				if( isset( $section['fields'] ) ) {

					// Loop through the fields parameter
					foreach( $section['fields'] as $field ) {

						// Colors field type
						if ( isset( $field['type'] ) && $field['type'] == 'color' ) {
							$target_element = isset($field['target_element']) ? $field['target_element'] : '';
							$target_style = isset($field['target_style']) ? $field['target_style'] : '';
							$default = isset($field['default']) ? $field['default'] : '';
							$styling_options[] = array(
								'title'				=> $field['title'],
								'default'			=> $default,
								'type'				=> $field['type'],
								'id'				=> $field['id'],
								'target_element'	=> $target_element,
								'target_style'		=> $target_style,
							);
						}

						// Color gradients field type
						if ( isset( $field['type'] ) && $field['type'] == 'color_gradient' ) {
							$target_element = isset($field['target_element']) ? $field['target_element'] : '';
							$styling_options[] = array(
								'title'				=> $field['title'],
								'default'			=> $field['default'],
								'type'				=> $field['type'],
								'id'				=> $field['id'],
								'target_element'	=> $target_element,
							);
						}

						// Link color field type
						if ( isset( $field['type'] ) && $field['type'] == 'link_color' ) {
							$default = isset( $field['default'] ) ? $field['default'] : '';
							$target_element = isset( $field['target_element'] ) ? $field['target_element'] : '';
							$target_element_hover = isset( $field['target_element_hover'] ) ? $field['target_element_hover'] : '';
							$target_element_active = isset( $field['target_element_active'] ) ? $field['target_element_active'] : '';
							$target_style = isset( $field['target_style'] ) ? $field['target_style'] : '';
							$styling_options[] = array(
								'title'						=> $field['title'],
								'default'					=> $default,
								'type'						=> $field['type'],
								'id'						=> $field['id'],
								'target_element'			=> $target_element,
								'target_element_hover'		=> $target_element_hover,
								'target_element_active'		=> $target_element_active,
								'target_style'				=> $target_style,
							);
						}

					} // End foreach $section['fields']

				} // End isset( $section['fields'] ) check

			} // Foreach $sections

			// Return the $styling options
			return $styling_options;

		} // If isset $sections

	}
}

/**
 * Loops through styling options and create custom CSS
 *
 * @since Total 1.0
 */
if ( !function_exists( 'wpex_styling_css' ) ) {
	function wpex_styling_css() {

		// wpex_redux_styling_options() function returns Null do nothing
		if ( ! wpex_redux_styling_options() ) {
			return;
		}

		// Declare function vars
		$css ='';
		$main_layout_style = wpex_option( 'main_layout_style' );
		
		// Return if custom styling is disabled
		if ( ! wpex_option( 'custom_styling', '1' ) ) {
			return;
		}
		
		// Remove boxed dropshadow if setting enabled
		if ( 'boxed' == $main_layout_style ) {
			if ( ! wpex_option( 'boxed_dropdshadow', '1' ) ) {
				$css .= '.boxed-main-layout #wrap { box-shadow: none; -moz-box-shadow: none; -webkit-box-shadow: none; }';
			}
		}

		// Color Options
		// Loops through all theme options and outputs the correct CSS
		$styling_options = wpex_redux_styling_options();

		foreach( $styling_options as $option ) {

			$value = wpex_option( $option['id'] );
			$type = isset( $option['type'] ) ? $option['type'] : '';
			$target_style = isset( $option['target_style'] ) ? $option['target_style'] : '';
			$target_element = isset( $option['target_element'] ) ? $option['target_element'] : '';
			
			if ( '' != $value ) {

				//Colors
				if ( 'color' == $type ) {
					if ( $target_element && $target_style ) {
						if ( is_array( $target_style ) ) {
							foreach ( $target_style as $key ) {
								$css .= $target_element .'{'. $key .':'. $value .';}';
							}
						} else {
							$css .= $target_element .'{'. $target_style .':'. $value .';}';
						}
					}
				} // End color type

				// Gradients
				if ( 'color_gradient' == $type && !empty($value) ) {
					if ( $target_element ) {
						$from = isset( $value['from'] ) ? $value['from'] : '';
						$to = isset( $value['to'] ) ? $value['to'] : '';
						if ( $from && $to ) {
							$css .= $target_element .'{
								background: '. $to .';
								background: -webkit-linear-gradient('. $from .','. $to .');
								background: -moz-linear-gradient('. $from .','. $to .');
								background: -o-linear-gradient('. $from .','. $to .');
								background: linear-gradient('. $from .','. $to .');
							}';
						} elseif( $from ) {
							$css .= $target_element .'{
								background: '. $from .';
							}';
						}
					}
				} // End color_gradient type

				// Link Color
				if ( 'link_color' == $type ) {
					$style = $target_style;
					$regular = isset( $value['regular'] ) ? $value['regular'] : '';
					$hover = isset( $value['hover'] ) ? $value['hover'] : '';
					$active = isset( $value['active'] ) ? $value['active'] : '';
					if ( $target_element && $regular ) {
						$css .= $target_element .'{'. $style .':'. $regular .';}';
					}
					if ( $option['target_element_hover'] && $hover ) {
						$css .= $option['target_element_hover'] .'{'. $style .':'. $hover .';}';
					}
					if ( $option['target_element_active'] && $active ) {
						$css .= $option['target_element_active'] .'{'. $style .':'. $active .';}';
					}
				} // End link_color type

			} // End if $value

		} // End foreach

		// Dropdown Background Bottom border color
		$color = wpex_option( 'dropdown_menu_background' );
		if ( $color ) {
			$css .= '.navbar-style-one .dropdown-menu ul:after { border-bottom-color: '. $color .'; }';
		}
		
		// Scroll top border radius
		$radius = wpex_option( 'scroll_top_border_radius' );
		if ( $radius && '35px'!= $radius ) {
			$css .= '#site-scroll-top { border-radius:'. $radius .'; }';
		}
		
		// Woo Overlay Margin
		$margin = wpex_option( 'woo_shop_overlay_top_margin' );
		if ( $margin ) {
			$css .= '#current-shop-items { top:'. intval( $margin ) .'px; }';
		}
		
		// Search Overlay
		$margin = wpex_option( 'main_search_overlay_top_margin' );
		if ( $margin ) {
			$css .= '#searchform-overlay { top:'. intval( $margin ) .'px; }';
		}
		
		// Mobile Menu Icon Font Size
		$size = wpex_option( 'mobile_menu_icon_size' );
		if ( '' != $size ) {
			$css .= '#mobile-menu a { font-size:'. $size .'; }';
		}

		// Callout button border radius
		$radius = wpex_option( 'callout_button_border_radius' );
		if ( $radius ) {
			$css .= '.footer-callout-button { border-radius:'. $radius .'; }';
		}

		// Fixed Header opacity
		$opacity = wpex_option('fixed_header_opacity');
		if ( $opacity ) {
			$css .= '.is-sticky #site-header { opacity:'. $opacity .'; }';
		}

		// Sidebar border
		$border = wpex_option( 'sidebar_border' );
		$border_top = isset( $border['border-top'] ) ? $border['border-top'] : '';
		$border_right = isset( $border['border-right'] ) ? $border['border-right'] : '';
		$border_bottom = isset( $border['border-bottom'] ) ? $border['border-bottom'] : '';
		$border_left = isset( $border['border-left'] ) ? $border['border-left'] : '';
		$border_color = isset( $border['border-color'] ) ? $border['border-color'] : 'inherit';
		$border_style = isset( $border['border-style'] ) ? $border['border-style'] : 'inherit';

		if ( 'none' != $border_style ) {
			if ( $border_top ) {
				$css .= '#sidebar { border-top:'. $border_top .' '. $border_style .' '. $border_color .'; }';
			}
			if ( $border_right ) {
				$css .= '#sidebar { border-right:'. $border_right .' '. $border_style .' '. $border_color .'; }';
			}
			if ( $border_bottom ) {
				$css .= '#sidebar { border-bottom:'. $border_bottom .' '. $border_style .' '. $border_color .'; }';
			}
			if ( $border_left ) {
				$css .= '#sidebar { border-left: '. $border_left .' '. $border_style .' '. $border_color .'; }';
			}
		}

		// Footer border
		$border = wpex_option('footer_border');
		$border_top = isset( $border['border-top'] ) ? $border['border-top'] : '';
		$border_right = isset( $border['border-right'] ) ? $border['border-right'] : '';
		$border_bottom = isset( $border['border-bottom'] ) ? $border['border-bottom'] : '';
		$border_left = isset( $border['border-left'] ) ? $border['border-left'] : '';
		$border_color = isset( $border['border-color'] ) ? $border['border-color'] : 'inherit';
		$border_style = isset( $border['border-style'] ) ? $border['border-style'] : 'inherit';

		if ( 'none' != $border_style ) {
			if ( $border_top ) {
				$css .= '#footer { border-top:'. $border_top .' '. $border_style .' '. $border_color .'; }';
			}
			if ( $border_right ) {
				$css .= '#footer { border-right:'. $border_right .' '. $border_style .' '. $border_color .'; }';
			}
			if ( $border_bottom ) {
				$css .= '#footer { border-bottom:'. $border_bottom .' '. $border_style .' '. $border_color .'; }';
			}
			if ( $border_left ) {
				$css .= '#footer { border-left: '. $border_left .' '. $border_style .' '. $border_color .'; }';
			}
		}

		// Footer bottom border
		$border = wpex_option('bottom_footer_border');
		$border_top = isset( $border['border-top'] ) ? $border['border-top'] : '';
		$border_right = isset( $border['border-right'] ) ? $border['border-right'] : '';
		$border_bottom = isset( $border['border-bottom'] ) ? $border['border-bottom'] : '';
		$border_left = isset( $border['border-left'] ) ? $border['border-left'] : '';
		$border_color = isset( $border['border-color'] ) ? $border['border-color'] : 'inherit';
		$border_style = isset( $border['border-style'] ) ? $border['border-style'] : 'inherit';
		
		if ( 'none' != $border_style ) {
			if ( $border_top ) {
				$css .= '#footer-bottom { border-top:'. $border_top .' '. $border_style .' '. $border_color .'; }';
			}
			if ( $border_right ) {
				$css .= '#footer-bottom { border-right:'. $border_right .' '. $border_style .' '. $border_color .'; }';
			}
			if ( $border_bottom ) {
				$css .= '#footer-bottom { border-bottom:'. $border_bottom .' '. $border_style .' '. $border_color .'; }';
			}
			if ( $border_left ) {
				$css .= '#footer-bottom { border-left: '. $border_left .' '. $border_style .' '. $border_color .'; }';
			}
		}
		
		// Return Custom CSS
		if ( '' != $css ) {
			$css = '/*Styling CSS START*/'. $css .'/*Styling CSS END*/';
			return $css;
		} else {
			return '';
		}
		
	}
}