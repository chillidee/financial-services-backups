<?php
/**
 * Used for your staff entries
 *
 * @package	Total
 * @author Alexander Clarke
 * @copyright Copyright (c) 2014, Symple Workz LLC
 * @link http://www.wpexplorer.com
 * @since Total 1.0
 */

// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

// Counter for clearing floats and margins
if ( !isset( $wpex_related_query ) ) {
	global $wpex_count;
	$query = 'archive';
} else {
	$query = 'related';
}

// Add Standard Classes
$wpex_classes = 'staff-entry col';
$wpex_classes .= ' '. wpex_staff_column_class( $query );
$wpex_classes .= ' col-'. $wpex_count;

// Masonry Classes
$wpex_grid_style = wpex_option( 'staff_archive_grid_style', 'fit-rows' );
if( 'masonry' == $wpex_grid_style || 'no-margins' == $wpex_grid_style ) {
	$wpex_classes .= ' isotope-entry';
} ?>

<article id="#post-<?php the_ID(); ?>" class="<?php echo $wpex_classes; ?>">
	<?php
	/**
	 * Displays the entry Media
	 * @link functions/staff/staff-entry.php
	 */
	wpex_staff_entry_media();
	/**
	 * Displays the entry details
	 * @link functions/staff/staff-entry.php
	 */
	wpex_staff_entry_content(); ?>
</article><!-- .staff-entry -->