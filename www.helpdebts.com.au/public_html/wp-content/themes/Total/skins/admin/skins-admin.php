<?php
/**
 * Creates the skins admin dashboard
 *
 * @package Total
 * @subpackage functions
 * @author Alexander Clarke
 * @copyright Copyright (c) 2014, Symple Workz LLC
 * @link http://www.wpexplorer.com
 * @since Total 1.0
 */

// Load CSS for skins admin
function wpex_skins_admin_css( $hook ) {

	// Only for skins admin screen
    if( 'appearance_page_wpex-skins-admin' != $hook ) {
        return;
    }

    // Register scripts
    wp_register_style( 'wpex_skins_admin_css', WPEX_SKIN_DIR_URI . 'admin/assets/skins-admin.css' );
    wp_register_script( 'wpex_skins_admin_js', WPEX_SKIN_DIR_URI . 'admin/assets/skins-admin.js', array( 'jquery' ), '1.0', true );

    // Load scripts
    wp_enqueue_style( 'wpex_skins_admin_css' );
    wp_enqueue_script( 'wpex_skins_admin_js' );
}
add_action( 'admin_enqueue_scripts', 'wpex_skins_admin_css' );

// create custom menu
add_action( 'admin_menu', 'wpex_skins_admin_menu' );

// Create submenu
function wpex_skins_admin_menu() {
	add_submenu_page( 'themes.php', __( 'Skins', 'wpex' ), __( 'Skins', 'wpex' ), 'administrator', 'wpex-skins-admin', 'wpex_skins_admin' );
	add_action( 'admin_init', 'wpex_skins_admin_register_settings' );
}

// Register settings
function wpex_skins_admin_register_settings() {
	register_setting( 'wpex_options', 'theme_skin' );
}

// The settings page output
function wpex_skins_admin() { ?>
<div class="wrap wpex-skins-admin">
<h2><?php _e( 'Theme Skins', 'wpex' ); ?></h2>

<?php
// Current skin from site_theme option
$val = get_option( 'theme_skin' );

// Get fallback from redux
if( !$val ) {
	$data = get_option( 'wpex_options' );
	$val = $data['site_theme'];
} ?>

<form method="post" action="options.php">
	<?php settings_fields( 'wpex_options' ); ?>
	<div class="wpex-skins-select theme-browser" id="theme_skin">
		<?php
		// Get and loop through skins
		$skins = wpex_skins();
		foreach ( $skins as $key => $value ) {
			$checked = $active = '';
			if ( '' != $val && ( $val == $key ) ) {
				$checked = 'checked';
				$active = 'active';
			} ?>
		<div class="wpex-skin <?php echo $active; ?> theme">
			<input type="radio" id="wpex-skin-<?php echo $key; ?>" name="theme_skin" value="<?php echo $key; ?>" <?php echo $checked; ?> class="wpex-skin-radio" />
			<div class="theme-screenshot">
				<img src="<?php echo $value['screenshot'] ?>" alt="<?php _e( 'Screenshot', 'wpex' ); ?>" />
			</div>
			<h3 class="theme-name">
				<?php if( 'active' == $active ) {
					echo '<strong>'. __( 'Active', 'wpex' ). ':</strong> ';
				} ?>
				<?php echo $value[ 'name' ]; ?>
			</h3>
		</div>
		<?php } ?>
	</div>
	<p class="submit">
		<input id="wpex-skins-select-submit" type="submit" class="button-primary" value="<?php _e( 'Save Changes', 'wpex' ) ?>" />
	</p>
</form>
</div>
<?php } ?>