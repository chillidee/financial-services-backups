<?php
/**
 * Plugin Name: ALC API Form
 * Plugin URI: http://www.australianlendingcentre.com.au
 * Description: ALC API Enquire Form for Money Maker
 * Version: 1.0
 * Author: Tim
 * Author URI: http://www.loong.com.au
 * License: ALC Copy Right, All Rights Reserved.
 */


function register_alcapiform_session(){
    if( !session_id() )
        session_start();
        
	if(!empty($_SERVER['HTTP_REFERER'])){
		//if the referer is website itself, then ignore. otherwise, go ahead.
		if (!TimStartsWith($_SERVER['HTTP_REFERER'], 'http://'.$_SERVER['SERVER_NAME']) &&
			!TimStartsWith($_SERVER['HTTP_REFERER'], 'https://'.$_SERVER['SERVER_NAME'])){
			$_SESSION['mm_referalurl'] = $_SERVER['HTTP_REFERER'];
		}
	}
		
	if(isset($_REQUEST['t'])){
		$_SESSION['mm_adwords_t'] = $_REQUEST['t'];
	}
	if(isset($_REQUEST['k'])){
		$_SESSION['mm_adwords_k'] = $_REQUEST['k'];
	}
	if(isset($_REQUEST['a'])){
		$_SESSION['mm_adwords_a'] = $_REQUEST['a'];
	}    
}
add_action('init','register_alcapiform_session');


function alcapiform_shortcode_api_contact_form( $atts, $content = null ) {
 
	//load jquery if not loaded
	if(!wp_script_is('jquery')){
		wp_enqueue_script('jquery'); 
	}
	
	//detect platform
	$platform = 'desktop';
	$useragent=$_SERVER['HTTP_USER_AGENT'];
	if(preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i',$useragent)||preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i',substr($useragent,0,4)))
		$platform = 'mobile';
	
	//echo $useragent.'|'.$platform;
	
	$contact_form = "";
	
	$options = get_option( 'alcapiform_option_name' );
	
	//load company ID - required
	$_cid = isset($atts['cid']) ? $atts['cid'] : $options['cid'];
	if(empty($_cid)) return 'Error: CID is not set. ';
	
	//load API GET URL - required
	$_geturl = isset($atts['geturl']) ? $atts['geturl'] : $options['geturl'];
	if(empty($_geturl)) return 'Error: API GET URL is not set. ';
	if(substr($_geturl, -1) != '/') $_geturl .= '/';
	
	//load API POST URL - required
	$_posturl = isset($atts['posturl']) ? $atts['posturl'] : $options['posturl'];
	if(empty($_posturl)) return 'Error: API POST URL is not set. ';
	
	//load enquiry form css file
	$_css   = isset($atts['css']) ? $atts['css'] : $options['css'];
	if(!empty($_css)) $contact_form .= '<link href="'.$_css.'" rel="stylesheet" type="text/css" />';
	
	//load jquery if specified
	$_jquery   = isset($atts['jquery']) ? $atts['jquery'] : $options['jquery'];
	if(!empty($_jquery)) $contact_form .= '<script src="'.$_jquery.'"></script>';
	
	//do form submission
	if(isset($_POST['alcapiformsumbit'])){
		//check spam bot
		if(!isset($_POST['website_url']) || empty($_POST['website_url'])){
			
			$data = array();
			$data['cid'] 				= strtolower($_cid);
			$data['title'] 				= isset($_POST['title']) ? $_POST['title'] : "";
			$data['firstName'] 			= isset($_POST['firstName']) ? $_POST['firstName'] : null;
			$data['lastName'] 			= isset($_POST['lastName']) ? $_POST['lastName'] : null;
			$data['loanAmount'] 		= isset($_POST['loanAmount']) ? str_replace(array('$', ','), '', $_POST['loanAmount']) : 0;
			$data['typeOfLoan'] 		= $_POST['typeOfLoan'];
			$data['hasProperty'] 		= (isset($_POST['hasProperty']) && $_POST['hasProperty'] == 1) ? "true" : "false";
			$data['haveDeposit'] 		= (isset($_POST['haveDeposit']) && $_POST['haveDeposit'] == 1) ? "true" : "false";;
			$data['realEstateValue'] 	= (isset($_POST['realEstateValue']) && !empty($_POST['realEstateValue'])) ? str_replace(array('$', ','), '', $_POST['realEstateValue']) : 0;
			$data['balanceOwing'] 		= (isset($_POST['balanceOwing']) && !empty($_POST['balanceOwing'])) ? str_replace(array('$', ','), '', $_POST['balanceOwing']) : 0;
			$data['mobileNumber'] 		= $_POST['mobileNumber'];
			$data['landLineNumber'] 	= (isset($_POST['landLineNumber']) && !empty($_POST['landLineNumber'])) ? $_POST['landLineNumberAreaCode'].$_POST['landLineNumber'] : '';
			$data['emailAddress'] 		= $_POST['emailAddress'];
			$data['suburb'] 			= $_POST['suburb'];
			$data['state'] 				= $_POST['state'];
			$data['postCode'] 			= $_POST['postCode'];
			$data['referral'] 			= isset($_POST['referral']) ? $_POST['referral'] : '';
			$data['comments'] 			= isset($_POST['comments']) ? $_POST['comments'] : '';
			$data['referrer'] 			= urlencode($_SESSION['mm_referalurl']);
			$data['t'] 					= $_SESSION['mm_adwords_t'];
			$data['k'] 					= $_SESSION['mm_adwords_k'];
			$data['a'] 					= $_SESSION['mm_adwords_a'];
			$data['platform'] 			= $platform;
			
			//echo "<pre>"; print_r($data); echo "</pre>";
			
			// Setup cURL
			$data_string = json_encode($data);                                                                               
	 
			$ch = curl_init($_posturl);
			curl_setopt($ch, CURLOPT_POST, true);                                                                     
			curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                                  
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
			curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
			    'Content-Type: application/json',                                                                                
			    'Content-Length: ' . strlen($data_string))                                                                       
			); 
	
			// Send the request
			$response = curl_exec($ch);
			
			// Check for errors
			if($response === FALSE){
				if(isset($_SESSION['failureURL']) && $_SESSION['failureURL'] != ''){
					die('<script type="text/javascript">window.location.href="'.$_SESSION['failureURL'].'";</script>');
				}else{
					die(curl_error($ch));
				}
			}
			
			// Decode the response
			$responseData = json_decode($response, TRUE);
			
			//echo "<pre>"; print_r($responseData); echo "</pre>"; 
			//echo "<pre>"; print_r($_POST); echo "</pre>"; 
			
			$responseString = "";
			
			if($responseData['status'] == 'SUCCESS'){
				$responseString = "<p class='api-post-success'>Successful</p>";
				if(isset($_SESSION['sucessURL']) && $_SESSION['sucessURL'] != ''){
					die('<script type="text/javascript">window.location.href="'.$_SESSION['sucessURL'].'";</script>');
				}
			}else{
				$responseString = "<p class='api-post-failed'><ul>";
				foreach($responseData['errorMessage'] as $value){
					$responseString .= "<li>".htmlspecialchars($value)."</li>";
				}
				$responseString .= "</ul></p>";
			}
		}
	}
	
	$jsonurl = $_geturl.$platform.'/'.strtolower($_cid);
	$json = file_get_contents($jsonurl);
	$json = json_decode($json, true);
	
	//echo "<pre>"; print_r($json); echo "</pre>";
	
	$_SESSION['sucessURL'] = $json['sucessURL'];
	$_SESSION['failureURL'] = $json['failureURL'];
	
	$typeOfLoan_options = '<option value="" selected="selected">[Select One]</option>';
	if(isset($json['typeOfLoan'])){
		foreach($json['typeOfLoan'] as $key => $value){
			$typeOfLoan_options .= '<option value="'.$key.'">'.$value.'</option>';
		}
	}
	
	$contact_form .= '<form id="apiEnquiryForm" name="apiEnquiryForm" action="" method="post">';
	
	if(!empty($responseString)) $contact_form .= $responseString;
	
	if(in_array('loanAmount', $json['fields'])){
        $contact_form .= '<p>
			        		<label for="loanAmount">Loan Amount*</label>
			        	    <input id="loanAmount" type="text" name="loanAmount" />
			        	    <span id="loanAmountError" style="color: red; display:none;"></span>
			        	  </p>';
	}
	
	if(in_array('typeOfLoan', $json['fields'])){
		$contact_form .= '<p>
			        		<label for="typeOfLoan">Type of Loan*</label>
			        	    <select id="typeOfLoan" name="typeOfLoan">
			        	      '.$typeOfLoan_options.'
			        	    </select>
			        	    <span id="typeOfLoanError" style="color: red; display:none;"></span>
			        	  </p>';
	}
	
	if(in_array('hasProperty', $json['fields'])){
		$contact_form .= '<p>
			        		<label>Do you currently own or paying off real estate?*</label>
			        	    <input type="radio" id="hasPropertyYes" name="hasProperty" value="1" required></input>
			        	    <label for="hasPropertyYes" class="radio">Yes</label>
			        	    <input type="radio" id="hasPropertyNo" name="hasProperty" value="0" required></input>
			        	    <label for="hasPropertyNo" class="radio">No</label>
			        	    <span id="hasPropertyError" style="color: red; display:none;"></span>
			        	  </p>';
		
		$contact_form .= '<div id="yourPropertySection" class="yourPropertySection" style="display:none;">
							<p>Tell us a little about your property</p>
							<p>
							  <label for="realEstateValue">Total real estate value</label>
				        	  <input id="realEstateValue" type="text" name="realEstateValue" />
				        	  <span id="realEstateValueError" style="color: red; display:none;"></span>
				        	</p>
				        	<p>
							  <label for="balanceOwing">Balance Owing</label>
			        	      <input id="balanceOwing" type="text" name="balanceOwing" />
			        	      <span id="balanceOwingError" style="color: red; display:none;"></span>
			        	    </p>
			        	  </div>';
	}
	
	/*
	if(in_array('haveDeposit', $json['fields'])){
		$contact_form .= '<p>
			        		<label>Do you have a minimum 20% deposit of the purchase price?*</label>
			        	    <input type="radio" id="haveDepositYes" name="haveDeposit" value="1" required></input>
			        	    <label for="haveDepositYes" class="radio">Yes</label>
			        	    <input type="radio" id="haveDepositNo" name="haveDeposit" value="0" required></input>
			        	    <label for="haveDepositNo" class="radio">No</label>
			        	    <span id="haveDepositError" style="color: red; display:none;"></span>
			        	  </p>';
	}*/
	
	if(in_array('title', $json['fields'])){
        $contact_form .= '<p>
			        		<label>Title</label>
			        	    <input type="radio" id="titleMr" name="title" value="Mr"></input>
			        	    <label for="titleMr" class="radio">Mr</label>
			        	    <input type="radio" id="titleMrs" name="title" value="Mrs"></input>
			        	    <label for="titleMrs" class="radio">Mrs</label>
			        	    <input type="radio" id="titleMs" name="title" value="Ms"></input>
			        	    <label for="titleMs" class="radio">Ms</label>
			        	    <input type="radio" id="titleDr" name="title" value="Dr"></input>
			        	    <label for="titleDr" class="radio">Dr</label>
			        	    <span id="titleError" style="color: red; display:none;"></span>
			        	  </p>';
	}
	
	if(in_array('firstName', $json['fields'])){
        $contact_form .= '<p>
			        		<label for="firstName">First Name*</label>
			        	    <input id="firstName" type="text" name="firstName" />
			        	    <span id="firstNameError" style="color: red; display:none;"></span>
			        	  </p>';
	}
	
	if(in_array('lastName', $json['fields'])){
        $contact_form .= '<p>
			        		<label for="lastName">Last Name*</label>
			        	    <input id="lastName" type="text" name="lastName" />
			        	    <span id="lastNameError" style="color: red; display:none;"></span>
			        	  </p>';
	}
	
	if(in_array('mobileNumber', $json['fields'])){
        $contact_form .= '<p>
			        		<label for="mobileNumber">Mobile number*</label>
			        	    <input id="mobileNumber" type="text" name="mobileNumber" />
			        	    <span id="mobileNumberError" style="color: red; display:none;"></span>
			        	  </p>';
	}
	
	if(in_array('landLineNumber', $json['fields'])){
        $contact_form .= '<p>
			        		<label for="landLineNumber">Landline number</label>
			        		<select name="landLineNumberAreaCode" id="landLineNumberAreaCode" class="landLineNumberAreaCode">
								<option selected="selected" value="02">02</option>
								<option value="03">03</option>
								<option value="07">07</option>
								<option value="08">08</option>
							</select>
			        	    <input id="landLineNumber" type="text" name="landLineNumber" class="landLineNumber" />
			        	    <span id="landLineNumberError" style="color: red; display:none;"></span>
			        	  </p>';
	}
	
	if(in_array('emailAddress', $json['fields'])){
        $contact_form .= '<p>
			        		<label for="emailAddress">Email*</label>
			        	    <input id="emailAddress" type="text" name="emailAddress" />
			        	    <span id="emailAddressError" style="color: red; display:none;"></span>
			        	  </p>';
	}
	
	if(in_array('suburb', $json['fields'])){
        $contact_form .= '<p>
			        		<label for="suburb">Suburb*</label>
			        	    <input id="suburb" type="text" name="suburb" />
			        	    <span id="suburbError" style="color: red; display:none;"></span>
			        	  </p>';
	}
	
	if(in_array('state', $json['fields'])){
        $contact_form .= '<p>
			        		<label for="state">State*</label>
			        	    <select id="state" name="state" class="state">
								<option value="ACT">ACT</option>
								<option selected="selected" value="NSW">NSW</option>
								<option value="NT">NT</option>
								<option value="QLD">QLD</option>
								<option value="SA">SA</option>
								<option value="TAS">TAS</option>
								<option value="VIC">VIC</option>
								<option value="WA">WA</option>
			        	    </select>
			        	  </p>';
	}
	
	if(in_array('postCode', $json['fields'])){
        $contact_form .= '<p>
			        		<label for="postCode">Post Code*</label>
			        	    <input id="postCode" type="text" name="postCode" class="postCode" />
			        	    <span id="postCodeError" style="color: red; display:none;"></span>
			        	  </p>';
	}
	
	if(in_array('referral', $json['fields'])){
        $contact_form .= '<p>
			        		<label for="referral">Where did you hear about us?*</label>
			        	    <select name="referral" id="referral" class="referral">
								<option selected="selected" value="">[Select One]</option>
								<option value="Yahoo">Yahoo</option>
								<option value="Radio">Radio</option>
								<option value="Newspaper">Newspaper</option>
								<option value="Google">Google</option>
								<option value="Television">Television</option>
								<option value="Family/Friend">Family/Friend</option>
								<option value="Email">Email</option>
								<option value="Other/Not Sure">Other/Not Sure</option>
							</select>
							<span id="referralError" style="color: red; display:none;"></span>
			        	  </p>';
	}
	
	if(in_array('comments', $json['fields'])){
        $contact_form .= '<p>
			        		<label for="comments">Comments</label>
			        	    <textarea id="comments" name="comments"></textarea>
			        	  </p>';
	}
	
	$contact_form .= '<input type="text" name="website_url" id="website_url" />';
	
	$contact_form .= '<div id="error_message"></div>';
	
	$contact_form .= '<p>* indicates required information.</p>';
	
    $contact_form .= '<input type="hidden" name="alcapiformsumbit" value="1" id="alcapiformsumbit">';
    $contact_form .= '<input type="button" name="apiSubmit" value="Submit" id="apiSubmit">';
            
	$contact_form .= '</form>';
	
	$contact_form .= '
	<script>
      jQuery(document).ready(function () {
      
        var web_url = document.getElementById("website_url");
        web_url.parentNode.removeChild(web_url);
    
        var ERROR_1 = "<li>Please tell us if you currently own or paying off real estate.</li>";
        var ERROR_2 = "<li>Please choose a type of loan you\'re interested in.</li>";
        var ERROR_3 = "<li>Loan amount is required.</li>";
        var ERROR_4 = "<li>Total real estate value required.</li>";
        var ERROR_5 = "<li>Balance owing required.</li>";
        var ERROR_6 = "<li>Mobile is required.</li>";
        var ERROR_7 = "<li>Mobile number is incomplete, or not in the correct format.</li>";
        var ERROR_8 = "<li>Email is required.</li>";
        var ERROR_9 = "<li>Invalid email format.</li>";
        var ERROR_10 = "<li>Your PostCode is required.</li>";
        var ERROR_11 = "<li>Postcode is incomplete or invalid.</li>";
        var ERROR_12 = "<li>First name is required.</li>";
        var ERROR_13 = "<li>Last name is required.</li>";
        var ERROR_14 = "<li>Landline number is incomplete, or not in the correct format.</li>";
        var ERROR_15 = "<li>Suburb is required.</li>";
        var ERROR_16 = "<li>Please choose where did you hear about us?</li>";
        var ERROR_17 = "<li>Please tell us if you have a minimum 20% deposit of the purchase price.</li>";
        
        var error_message = "";
        
        jQuery("#loanAmount, #realEstateValue, #balanceOwing").blur(function() {
          var value= jQuery(this).val().replace(/[^\d\.]/g, "");
          value = value.trim();
          if(value == ""){
            jQuery(this).val("");
          }else{
            value = Math.round(value) + "";
            value = value.replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
		    jQuery(this).val("$"+value);
		    
		    if(jQuery(this).attr("id") == "loanAmount"){
		      jQuery("#loanAmountError").hide();
		      error_message = error_message.replace(ERROR_3, "");
		    }
		    
		    if(jQuery(this).attr("id") == "realEstateValue"){
		      jQuery("#realEstateValueError").hide();
		      error_message = error_message.replace(ERROR_4, "");
		    }
            
		    if(jQuery(this).attr("id") == "balanceOwing"){
		      jQuery("#balanceOwingError").hide();
		      error_message = error_message.replace(ERROR_5, "");
		    }
		    
            showErrorMessages();
		  }
		});
		jQuery("#loanAmount, #realEstateValue, #balanceOwing").keypress(function(event) {
          if ((event.which != 46 || jQuery(this).val().indexOf(".") != -1) && (event.which < 48 || event.which > 57)) {
		    event.preventDefault();
		  }
		});
		jQuery("#loanAmount, #realEstateValue, #balanceOwing").focusin(function(){
		  jQuery(this).val(jQuery(this).val().replace(/[$,]/g, ""));
		});

        
		jQuery("#mobileNumber").focusin(function(){
		  jQuery("#mobileNumberError").hide();
		  error_message = error_message.replace(ERROR_6, "");
		  error_message = error_message.replace(ERROR_7, "");
		  showErrorMessages();
		});
		jQuery("#mobileNumber").blur(function() {
		  var value= jQuery(this).val().replace(/[^\d]/g, "");
		  value= value.replace(/^61/, "0");
		  jQuery(this).val(value);
		  check_mobileNumber();
		  showErrorMessages();
		});
		
		
		jQuery("#emailAddress").focusin(function() {
		  jQuery("#emailAddressError").hide();
		  error_message = error_message.replace(ERROR_8, "");
		  error_message = error_message.replace(ERROR_9, "");
		  showErrorMessages();
		});
		jQuery("#emailAddress").blur(function(){
		  check_emailAddress();
		  showErrorMessages();
		});
		
		
		jQuery("#landLineNumber").focusin(function(){
		  jQuery("#landLineNumberError").hide();
		  error_message = error_message.replace(ERROR_14, "");
		  showErrorMessages();
		});
		jQuery("#landLineNumber").blur(function() {
		  var value= jQuery(this).val().replace(/[^\d]/g, "");
		  jQuery(this).val(value);
		  check_landLineNumber();
		  showErrorMessages();
		});
		
		
		jQuery("#postCode").focusin(function() {
		  jQuery("#postCodeError").hide();
		  error_message = error_message.replace(ERROR_10, "");
		  error_message = error_message.replace(ERROR_11, "");
		  showErrorMessages();
		});
		jQuery("#postCode").blur(function() {
		  var value= jQuery(this).val().replace(/[^\d]/g, "");
		  jQuery(this).val(value);
		  check_postCode();
		  showErrorMessages();
		});
		
		jQuery("#firstName").focusin(function() {
		  jQuery("#firstNameError").hide();
		  error_message = error_message.replace(ERROR_12, "");
		  showErrorMessages();
		});
		jQuery("#firstName").blur(function() {
		  var value= jQuery(this).val().replace(/[^a-z\s,-.\']+/gi, "");
		  jQuery(this).val(value);
		  check_firstName();
		  showErrorMessages();
		});
		
		jQuery("#lastName").focusin(function() {
		  jQuery("#lastNameError").hide();
		  error_message = error_message.replace(ERROR_13, "");
		  showErrorMessages();
		});
		jQuery("#lastName").blur(function() {
		  var value= jQuery(this).val().replace(/[^a-z\s,-.\']+/gi, "");
		  jQuery(this).val(value);
		  check_lastName();
		  showErrorMessages();
		});
		
		jQuery("#suburb").focusin(function() {
		  jQuery("#suburbError").hide();
		  error_message = error_message.replace(ERROR_15, "");
		  showErrorMessages();
		});
		jQuery("#suburb").blur(function() {
		  var value= jQuery(this).val().replace(/[^a-z\s,-.\']+/gi, "");
		  jQuery(this).val(value);
		  check_suburb();
		  showErrorMessages();
		});
		
		
        jQuery("#typeOfLoan").change(function(){
          check_typeOfLoan();
          showErrorMessages();
	    });
	    
	    jQuery("#referral").change(function(){
	      check_referral();
		  showErrorMessages();
	    });
	    
        jQuery("input[type=radio][name=hasProperty]").change(function(){
          jQuery("#hasPropertyError").hide();
          error_message = error_message.replace(ERROR_1, "");
          showErrorMessages();
	      if(this.value == "1"){
	        jQuery("#yourPropertySection").show();
	      }else{
	        jQuery("#yourPropertySection").hide();
	      }
	    });
	    
        jQuery("input[type=radio][name=haveDeposit]").change(function(){
          jQuery("#haveDepositError").hide();
          error_message = error_message.replace(ERROR_17, "");
          showErrorMessages();
	    });
	    
	    
	    jQuery("#apiSubmit").click(function(){
	      
	      if(jQuery("#loanAmount").length && jQuery("#loanAmount").val() == ""){
	        jQuery("#loanAmountError").html("required");
	        jQuery("#loanAmountError").show();
	        error_message = error_message.replace(ERROR_3, "");
	        error_message += ERROR_3;
	      }
	      
	      if(jQuery("input[name=hasProperty]").length && jQuery("input[name=hasProperty]:checked").length == 0){
	        jQuery("#hasPropertyError").html("required");
	        jQuery("#hasPropertyError").show();
	        error_message = error_message.replace(ERROR_1, "");
	        error_message += ERROR_1;
	      }
	      
	      if(jQuery("input[name=haveDeposit]").length && jQuery("input[name=haveDeposit]:checked").length == 0){
	        jQuery("#haveDepositError").html("required");
	        jQuery("#haveDepositError").show();
	        error_message = error_message.replace(ERROR_17, "");
	        error_message += ERROR_17;
	      }
	      
	      if(jQuery("#hasPropertyYes").is(":checked")){ 
	        if(jQuery("#realEstateValue").val() == ""){
	          jQuery("#realEstateValueError").html("required");
	          jQuery("#realEstateValueError").show();
	          error_message = error_message.replace(ERROR_4, "");
	          error_message += ERROR_4;
	        }
	        if(jQuery("#balanceOwing").val() == ""){
	          jQuery("#balanceOwingError").html("required");
	          jQuery("#balanceOwingError").show();
	          error_message = error_message.replace(ERROR_5, "");
	          error_message += ERROR_5;
	        }
	      }
	      
	      
	      if(jQuery("#typeOfLoan").length) check_typeOfLoan();
	      if(jQuery("#mobileNumber").length) check_mobileNumber();
	      if(jQuery("#emailAddress").length) check_emailAddress();
	      if(jQuery("#firstName").length) check_firstName();
	      if(jQuery("#lastName").length) check_lastName();
	      if(jQuery("#suburb").length) check_suburb();
	      if(jQuery("#postCode").length) check_postCode();
	      if(jQuery("#referral").length) check_referral();
	      
	      if(error_message == ""){
	        jQuery("#apiEnquiryForm").submit();
	      }else{
	        showErrorMessages();
	      }
		});
	    
		function check_typeOfLoan(){
          if(jQuery("#typeOfLoan").val() == ""){
            jQuery("#typeOfLoanError").html("required");
	        jQuery("#typeOfLoanError").show();
	        error_message = error_message.replace(ERROR_2, "");
	        error_message += ERROR_2;
	      }else{
            jQuery("#typeOfLoanError").hide();
            error_message = error_message.replace(ERROR_2, "");
            showErrorMessages();
		  }
		}
		
		function check_referral(){
          if(jQuery("#referral").val() == ""){
            jQuery("#referralError").html("required");
	        jQuery("#referralError").show();
	        error_message = error_message.replace(ERROR_16, "");
	        error_message += ERROR_16;
	      }else{
            jQuery("#referralError").hide();
            error_message = error_message.replace(ERROR_16, "");
            showErrorMessages();
		  }
		}
		
		function check_landLineNumber(){
		  if(jQuery("#landLineNumber").val() != "" && jQuery("#landLineNumber").val().length != 8){
	        jQuery("#landLineNumberError").html("invalid");
	        jQuery("#landLineNumberError").show();
	        error_message = error_message.replace(ERROR_14, "");
	        error_message += ERROR_14;
	      }else{
	        jQuery("#landLineNumberError").hide();
		    error_message = error_message.replace(ERROR_14, "");
	      }
		}
		
		function check_mobileNumber(){
		  if(jQuery("#mobileNumber").val() == ""){
	        jQuery("#mobileNumberError").html("required");
	        jQuery("#mobileNumberError").show();
	        error_message = error_message.replace(ERROR_6, "");
	        error_message += ERROR_6;
	      }else if(jQuery("#mobileNumber").val().indexOf("04") != 0 || jQuery("#mobileNumber").val().length != 10){
	        jQuery("#mobileNumberError").html("invalid");
	        jQuery("#mobileNumberError").show();
	        error_message = error_message.replace(ERROR_7, "");
	        error_message += ERROR_7;
	      }else{
	        jQuery("#mobileNumberError").hide();
		    error_message = error_message.replace(ERROR_6, "");
		    error_message = error_message.replace(ERROR_7, "");
	      }
		}
		
		function check_emailAddress(){
		  if(jQuery("#emailAddress").val() == ""){
	        jQuery("#emailAddressError").html("required");
	        jQuery("#emailAddressError").show();
	        error_message = error_message.replace(ERROR_8, "");
	        error_message += ERROR_8;
	      }else if(/^(?:\w+\.?\+?)*\w+@(?:\w+\.)+\w+$/.test(jQuery("#emailAddress").val()) == false){
	        jQuery("#emailAddressError").html("invalid");
	        jQuery("#emailAddressError").show();
	        error_message = error_message.replace(ERROR_9, "");
	        error_message += ERROR_9;
	      }else{
	        jQuery("#emailAddressError").hide();
		    error_message = error_message.replace(ERROR_8, "");
		    error_message = error_message.replace(ERROR_9, "");
	      }
	    }
		
		function check_firstName(){
		  if(jQuery("#firstName").val() == ""){
	        jQuery("#firstNameError").html("required");
	        jQuery("#firstNameError").show();
	        error_message = error_message.replace(ERROR_12, "");
	        error_message += ERROR_12;
	      }else{
	        jQuery("#firstNameError").hide();
		    error_message = error_message.replace(ERROR_12, "");
	      }
		}
		
		function check_lastName(){
		  if(jQuery("#lastName").val() == ""){
	        jQuery("#lastNameError").html("required");
	        jQuery("#lastNameError").show();
	        error_message = error_message.replace(ERROR_13, "");
	        error_message += ERROR_13;
	      }else{
	        jQuery("#lastNameError").hide();
		    error_message = error_message.replace(ERROR_13, "");
	      }
		}
		
		function check_suburb(){
		  if(jQuery("#suburb").val() == ""){
	        jQuery("#suburbError").html("required");
	        jQuery("#suburbError").show();
	        error_message = error_message.replace(ERROR_15, "");
	        error_message += ERROR_15;
	      }else{
	        jQuery("#suburbError").hide();
		    error_message = error_message.replace(ERROR_15, "");
	      }
		}
		
		function check_postCode(){
		  if(jQuery("#postCode").val() == ""){
	        jQuery("#postCodeError").html("required");
	        jQuery("#postCodeError").show();
	        error_message = error_message.replace(ERROR_10, "");
	        error_message += ERROR_10;
	      }else if(jQuery("#postCode").val().length != 4){
	        jQuery("#postCodeError").html("invalid");
	        jQuery("#postCodeError").show();
	        error_message = error_message.replace(ERROR_11, "");
	        error_message += ERROR_11;
	      }else{
	        jQuery("#postCodeError").hide();
		    error_message = error_message.replace(ERROR_10, "");
		    error_message = error_message.replace(ERROR_11, "");
	      }
		}
		
		function showErrorMessages(){
		  if(error_message == ""){
		    jQuery("#error_message").html("");
		  }else{
		    jQuery("#error_message").html("<b>Oops, we\'ve missed something...</b><ul>"+error_message+"</ul>");
		  }
		}
	    
      });
    </script>';
	
	return $contact_form;
}

function register_alcapiform_shortcodes(){
   add_shortcode('alcapi_contact_form', 'alcapiform_shortcode_api_contact_form');
}

add_action( 'init', 'register_alcapiform_shortcodes');
//sample usage: [iframe_contact_form baseurl="http://applicationform.hatpacks.com.au/EnquiryForm/default.aspx" style="width: 90%; height: 900px;" cid="CC" svs="0" css="http://test2.cleancredit.com.au/site/wp-content/themes/cleancredit/styles/enquiry-form.css"]


//create setting page
if( is_admin() ){
	include "settings.php";
    $alcapiform_settings_page = new ALCAPIFormSettingsPage();
}
    
//create setting quick link 
function alcapiform_plugin_action_links($links, $file) {
    static $this_plugin;

    if (!$this_plugin) {
        $this_plugin = plugin_basename(__FILE__);
    }

    if ($file == $this_plugin) {
        $settings_link = '<a href="' . get_bloginfo('wpurl') . '/wp-admin/options-general.php?page=alcapiform-setting-admin">Settings</a>';
        array_unshift($links, $settings_link);
    }

    return $links;
}
add_filter('plugin_action_links', 'alcapiform_plugin_action_links', 10, 2);

//functions
if(!function_exists("TimStartsWith")){
	function TimStartsWith($haystack, $needle){
	    return !strncmp($haystack, $needle, strlen($needle));
	}
}