<?php

// BEGIN iThemes Security - Do not modify or remove this line
// iThemes Security Config Details: 2
define( 'DISALLOW_FILE_EDIT', true ); // Disable File Editor - Security > Settings > WordPress Tweaks > File Editor
// END iThemes Security - Do not modify or remove this line

/** Enable W3 Total Cache */
define('WP_CACHE', true); // Added by W3 Total Cache


/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'mortgage_wp885');

/** MySQL database username */
define('DB_USER', 'mortgage_wp885');

/** MySQL database password */
define('DB_PASSWORD', 'PSe@3t3p3(');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY', '3)@%L0[9u}Ysw?b&?>tnuwYGt<@,Y[Pn7o}f$%$.5zo]xq6wk+JEi>~B]KxLw!Z|');
define('SECURE_AUTH_KEY', ']O.3r~Z-/@`u}CRao[Ew(bY8M-f9U4ZIfV/wsWfpw$Kme]ms#RPVTap;-v0D+l~|');
define('LOGGED_IN_KEY', '79n[stbO,M1 A@sbr7X`7!1P<o**AQ`<m_*Ed)*?uH/VhpWy_f>R&W<!~!R/pn;B');
define('NONCE_KEY', '/~6bTL @*/v[n&k._fN21HByf&wu:nW|+o;E0;eF{zrbdC/]HLQ,*^u=WQfP42FQ');
define('AUTH_SALT', '^x(Do+9cl]dpqbcXNTTsw@d*qiu)~ZxO_e&/}OaaBdpSfsbS!V~>u##L_>sUUQ>D');
define('SECURE_AUTH_SALT', 'U!BKmBLOF1:K(>}SP1N`BnhkhA41%Z;xys8KTf++7&wN@m;p@E+aS<lZt%n.BetZ');
define('LOGGED_IN_SALT', 'x{.^<)210+az/;O`wA~$t-ANYOJ[T3L%RHTLpi?%<?b||Q1n%y{.hpJ64&SjI|++');
define('NONCE_SALT', 'yFxONvqHusxl8+zLknHrD^=<WCd|s{%Qu |8vhbPzz1>k[Fua@MERwx ZA}^p6uK');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'mcr_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
