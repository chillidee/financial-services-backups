CREATE TABLE `a4g1_itsec_lockouts` (  `lockout_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,  `lockout_type` varchar(20) DEFAULT NULL,  `lockout_start` datetime DEFAULT NULL,  `lockout_start_gmt` datetime DEFAULT NULL,  `lockout_expire` datetime DEFAULT NULL,  `lockout_expire_gmt` datetime DEFAULT NULL,  `lockout_host` varchar(40) DEFAULT NULL,  `lockout_user` bigint(20) unsigned DEFAULT NULL,  `lockout_username` varchar(60) DEFAULT NULL,  `lockout_active` int(1) DEFAULT '1',  PRIMARY KEY (`lockout_id`),  KEY `lockout_expire_gmt` (`lockout_expire_gmt`),  KEY `lockout_host` (`lockout_host`),  KEY `lockout_user` (`lockout_user`),  KEY `lockout_username` (`lockout_username`),  KEY `lockout_active` (`lockout_active`)) ENGINE=MyISAM AUTO_INCREMENT=226 DEFAULT CHARSET=latin1;
/*!40000 ALTER TABLE `a4g1_itsec_lockouts` DISABLE KEYS */;
SET FOREIGN_KEY_CHECKS = 0;
SET UNIQUE_CHECKS = 0;
INSERT INTO `a4g1_itsec_lockouts` VALUES('223', 'four_oh_four', '2018-03-28 07:22:45', '2018-03-27 20:22:45', '2018-03-28 07:37:45', '2018-03-27 20:37:45', '124.207.250.89', NULL, NULL, '1');
INSERT INTO `a4g1_itsec_lockouts` VALUES('224', 'four_oh_four', '2018-03-29 13:48:16', '2018-03-29 02:48:16', '2018-03-29 14:03:16', '2018-03-29 03:03:16', '188.225.178.238', NULL, NULL, '1');
INSERT INTO `a4g1_itsec_lockouts` VALUES('225', 'four_oh_four', '2018-04-03 05:23:00', '2018-04-02 19:23:00', '2018-04-03 05:38:00', '2018-04-02 19:38:00', '124.121.226.61', NULL, NULL, '1');
INSERT INTO `a4g1_itsec_lockouts` VALUES('222', 'four_oh_four', '2018-03-28 07:20:39', '2018-03-27 20:20:39', '2018-03-28 07:35:39', '2018-03-27 20:35:39', '218.207.103.168', NULL, NULL, '1');
/*!40000 ALTER TABLE `a4g1_itsec_lockouts` ENABLE KEYS */;
SET FOREIGN_KEY_CHECKS = 1;
SET UNIQUE_CHECKS = 1;
