CREATE TABLE `a4g1_yoast_seo_links` (  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,  `url` varchar(255) DEFAULT NULL,  `post_id` bigint(20) unsigned DEFAULT NULL,  `target_post_id` bigint(20) unsigned DEFAULT NULL,  `type` varchar(8) DEFAULT NULL,  PRIMARY KEY (`id`),  KEY `link_direction` (`post_id`,`type`)) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40000 ALTER TABLE `a4g1_yoast_seo_links` DISABLE KEYS */;
SET FOREIGN_KEY_CHECKS = 0;
SET UNIQUE_CHECKS = 0;
INSERT INTO `a4g1_yoast_seo_links` VALUES('7', 'mailto:info@businessloancalculator.net.au', '3628', '0', 'external');
/*!40000 ALTER TABLE `a4g1_yoast_seo_links` ENABLE KEYS */;
SET FOREIGN_KEY_CHECKS = 1;
SET UNIQUE_CHECKS = 1;
