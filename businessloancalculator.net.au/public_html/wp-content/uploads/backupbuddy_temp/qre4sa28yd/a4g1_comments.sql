CREATE TABLE `a4g1_comments` (  `comment_ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,  `comment_post_ID` bigint(20) unsigned DEFAULT '0',  `comment_author` tinytext,  `comment_author_email` varchar(100) DEFAULT '',  `comment_author_url` varchar(200) DEFAULT '',  `comment_author_IP` varchar(100) DEFAULT '',  `comment_date` datetime DEFAULT '0000-00-00 00:00:00',  `comment_date_gmt` datetime DEFAULT '0000-00-00 00:00:00',  `comment_content` text,  `comment_karma` int(11) DEFAULT '0',  `comment_approved` varchar(20) DEFAULT '1',  `comment_agent` varchar(255) DEFAULT '',  `comment_type` varchar(20) DEFAULT '',  `comment_parent` bigint(20) unsigned DEFAULT '0',  `user_id` bigint(20) unsigned DEFAULT '0',  PRIMARY KEY (`comment_ID`),  KEY `comment_post_ID` (`comment_post_ID`),  KEY `comment_approved_date_gmt` (`comment_approved`,`comment_date_gmt`),  KEY `comment_date_gmt` (`comment_date_gmt`),  KEY `comment_parent` (`comment_parent`),  KEY `comment_author_email` (`comment_author_email`(10))) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40000 ALTER TABLE `a4g1_comments` DISABLE KEYS */;
SET FOREIGN_KEY_CHECKS = 0;
SET UNIQUE_CHECKS = 0;
/*!40000 ALTER TABLE `a4g1_comments` ENABLE KEYS */;
SET FOREIGN_KEY_CHECKS = 1;
SET UNIQUE_CHECKS = 1;
