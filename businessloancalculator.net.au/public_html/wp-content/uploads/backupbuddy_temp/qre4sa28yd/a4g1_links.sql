CREATE TABLE `a4g1_links` (  `link_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,  `link_url` varchar(255) DEFAULT '',  `link_name` varchar(255) DEFAULT '',  `link_image` varchar(255) DEFAULT '',  `link_target` varchar(25) DEFAULT '',  `link_description` varchar(255) DEFAULT '',  `link_visible` varchar(20) DEFAULT 'Y',  `link_owner` bigint(20) unsigned DEFAULT '1',  `link_rating` int(11) DEFAULT '0',  `link_updated` datetime DEFAULT '0000-00-00 00:00:00',  `link_rel` varchar(255) DEFAULT '',  `link_notes` mediumtext,  `link_rss` varchar(255) DEFAULT '',  PRIMARY KEY (`link_id`),  KEY `link_visible` (`link_visible`)) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40000 ALTER TABLE `a4g1_links` DISABLE KEYS */;
SET FOREIGN_KEY_CHECKS = 0;
SET UNIQUE_CHECKS = 0;
/*!40000 ALTER TABLE `a4g1_links` ENABLE KEYS */;
SET FOREIGN_KEY_CHECKS = 1;
SET UNIQUE_CHECKS = 1;
