CREATE TABLE `n026qpix97_itsec_lockouts` (  `lockout_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,  `lockout_type` varchar(20) NOT NULL,  `lockout_start` datetime NOT NULL,  `lockout_start_gmt` datetime NOT NULL,  `lockout_expire` datetime NOT NULL,  `lockout_expire_gmt` datetime NOT NULL,  `lockout_host` varchar(40) DEFAULT NULL,  `lockout_user` bigint(20) unsigned DEFAULT NULL,  `lockout_username` varchar(60) DEFAULT NULL,  `lockout_active` int(1) NOT NULL DEFAULT '1',  PRIMARY KEY (`lockout_id`),  KEY `lockout_expire_gmt` (`lockout_expire_gmt`),  KEY `lockout_host` (`lockout_host`),  KEY `lockout_user` (`lockout_user`),  KEY `lockout_username` (`lockout_username`),  KEY `lockout_active` (`lockout_active`)) ENGINE=MyISAM AUTO_INCREMENT=33 DEFAULT CHARSET=utf8;
/*!40000 ALTER TABLE `n026qpix97_itsec_lockouts` DISABLE KEYS */;
SET FOREIGN_KEY_CHECKS = 0;
SET UNIQUE_CHECKS = 0;
INSERT INTO `n026qpix97_itsec_lockouts` VALUES('31', 'four_oh_four', '2018-03-30 16:10:41', '2018-03-30 05:10:41', '2018-03-30 16:25:41', '2018-03-30 05:25:41', '173.254.205.230', NULL, NULL, '1');
INSERT INTO `n026qpix97_itsec_lockouts` VALUES('32', 'four_oh_four', '2018-04-04 22:43:51', '2018-04-04 12:43:51', '2018-04-04 22:58:51', '2018-04-04 12:58:51', '173.254.205.230', NULL, NULL, '1');
/*!40000 ALTER TABLE `n026qpix97_itsec_lockouts` ENABLE KEYS */;
SET FOREIGN_KEY_CHECKS = 1;
SET UNIQUE_CHECKS = 1;
