<?php
class ALCIframeFormSettingsPage
{
    /**
     * Holds the values to be used in the fields callbacks
     */
    private $options;

    /**
     * Start up
     */
    public function __construct()
    {
        add_action( 'admin_menu', array( $this, 'add_plugin_page' ) );
        add_action( 'admin_init', array( $this, 'page_init' ) );
    }

    /**
     * Add options page
     */
    public function add_plugin_page()
    {
        // This page will be under "Settings"
        add_options_page(
            'Settings Admin', 
            'ALC iFrame Form', 
            'manage_options', 
            'alciframeform-setting-admin', 
            array( $this, 'create_admin_page' )
        );
    }

    /**
     * Options page callback
     */
    public function create_admin_page()
    {
        // Set class property
        $this->options = get_option( 'alciframeform_option_name' );
        ?>
        <div class="wrap">
            <?php screen_icon(); ?>
            <h2>ALC iFrame Form for Money Maker Settings</h2>           
            <form method="post" action="options.php">
            <?php
                // This prints out all hidden setting fields
                settings_fields( 'alciframeform_option_group' );   
                do_settings_sections( 'alciframeform-setting-admin' );
                submit_button(); 
            ?>
            </form>
        </div>
        <?php
    }

    /**
     * Register and add settings
     */
    public function page_init()
    {        
        register_setting(
            'alciframeform_option_group', // Option group
            'alciframeform_option_name', // Option name
            array( $this, 'sanitize' ) // Sanitize
        );

        add_settings_section(
            'setting_section_id', // ID
            'Global Default Settings', // Title
            array( $this, 'print_section_info' ), // Callback
            'alciframeform-setting-admin' // Page
        );  

        add_settings_field(
            'baseurl', 
            'Base URL', 
            array( $this, 'baseurl_callback' ), 
            'alciframeform-setting-admin', 
            'setting_section_id'
        );     

        add_settings_field(
            'style', 
            'iFrame Style', 
            array( $this, 'style_callback' ), 
            'alciframeform-setting-admin', 
            'setting_section_id'
        );   

        add_settings_field(
            'cid', 
            'CID', 
            array( $this, 'cid_callback' ), 
            'alciframeform-setting-admin', 
            'setting_section_id'
        );    

        add_settings_field(
            'svs', 
            'SVS', 
            array( $this, 'svs_callback' ), 
            'alciframeform-setting-admin', 
            'setting_section_id'
        );   

        add_settings_field(
            'mode', 
            'Mode', 
            array( $this, 'mode_callback' ), 
            'alciframeform-setting-admin', 
            'setting_section_id'
        );   

        add_settings_field(
            'css', 
            'Enquiry Form CSS Full URL', 
            array( $this, 'css_callback' ), 
            'alciframeform-setting-admin', 
            'setting_section_id'
        );  

        add_settings_section(
            'setting_section_id_usage', // ID
            'Usage Instruction', // Title
            array( $this, 'print_section_info_usage' ), // Callback
            'alciframeform-setting-admin' // Page
        );  

        add_settings_section(
            'setting_section_id_shortcode', // ID
            'Override Instruction', // Title
            array( $this, 'print_section_info_shortcode' ), // Callback
            'alciframeform-setting-admin' // Page
        );  

    }

    /**
     * Sanitize each setting field as needed
     *
     * @param array $input Contains all settings fields as array keys
     */
    public function sanitize( $input )
    {
        $new_input = array();

        if( isset( $input['baseurl'] ) )
            $new_input['baseurl'] = sanitize_text_field( $input['baseurl'] );

        if( isset( $input['style'] ) )
            $new_input['style'] = sanitize_text_field( $input['style'] );

        if( isset( $input['cid'] ) )
            $new_input['cid'] = sanitize_text_field( $input['cid'] );

        if( isset( $input['svs'] ) )
            $new_input['svs'] = sanitize_text_field( $input['svs'] );

        if( isset( $input['mode'] ) )
            $new_input['mode'] = sanitize_text_field( $input['mode'] );

        if( isset( $input['css'] ) )
            $new_input['css'] = sanitize_text_field( $input['css'] );

        return $new_input;
    }

    /** 
     * Print the Section text
     */
    public function print_section_info()
    {
        print 'Enter your global default settings below, these can be overrode by the values in the shortcode:';
    }
    
    /** 
     * Print the Section text
     */
    public function print_section_info_usage()
    {
        print 'Use shortcode to display ALC iFrame Form, as below: <br/>
        	   <pre>
               [iframe_contact_form]
        	   </pre>
        ';
    }

    /** 
     * Print the Section text
     */
    public function print_section_info_shortcode()
    {
        print 'The global default values above can be overrode by the shortcode usage, as below: <br/>
        	   <pre>
               [iframe_contact_form baseurl="http://applicationform.hatpacks.com.au/EnquiryForm/default.aspx" 
                                    style="width: 90%; height: 900px;" 
                                    cid="CC" 
                                    svs="0" 
                                    css="http://test2.cleancredit.com.au/site/wp-content/themes/cleancredit/styles/enquiry-form.css"]
        	   </pre>
        ';
    }

    /** 
     * Get the settings option array and print one of its values
     */
    public function baseurl_callback()
    {
        printf(
            '<input style="width: 700px;" type="text" id="baseurl" name="alciframeform_option_name[baseurl]" value="%s" />',
            isset( $this->options['baseurl'] ) ? esc_attr( $this->options['baseurl']) : 'http://applicationform.hatpacks.com.au/EnquiryForm/default.aspx'
        );
    }
    public function style_callback()
    {
        printf(
            '<input style="width: 700px;" type="text" id="style" name="alciframeform_option_name[style]" value="%s" />',
            isset( $this->options['style'] ) ? esc_attr( $this->options['style']) : 'width: 90%; height: 900px;'
        );
    }
    public function cid_callback()
    {
        printf(
            '<input style="width: 80px;" type="text" id="cid" name="alciframeform_option_name[cid]" value="%s" />',
            isset( $this->options['cid'] ) ? esc_attr( $this->options['cid']) : 'CC'
        );
    }
    public function svs_callback()
    {
        printf(
            '<input style="width: 80px;" type="text" id="svs" name="alciframeform_option_name[svs]" value="%s" />',
            isset( $this->options['svs'] ) ? esc_attr( $this->options['svs']) : '0'
        );
    }
    public function mode_callback()
    {
        printf(
            '<input style="width: 80px;" type="text" id="mode" name="alciframeform_option_name[mode]" value="%s" />',
            isset( $this->options['mode'] ) ? esc_attr( $this->options['mode']) : ''
        );
    }
    public function css_callback()
    {
        printf(
            '<input style="width: 700px;" type="text" id="css" name="alciframeform_option_name[css]" value="%s" />',
            isset( $this->options['css'] ) ? esc_attr( $this->options['css']) : 'http://test2.cleancredit.com.au/site/wp-content/themes/cleancredit/styles/enquiry-form.css'
        );
    }
}
?>