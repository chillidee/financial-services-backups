<?php
/**
 * Plugin Name: ALC iFrame Form
 * Plugin URI: http://www.australianlendingcentre.com.au
 * Description: ALC iFrame Enquire Form for Money Maker
 * Version: 1.0
 * Author: Tim
 * Author URI: http://www.loong.com.au
 * License: ALC Copy Right, All Rights Reserved.
 */


function register_alciframeform_session(){
    if( !session_id() )
        session_start();
        
	if(!empty($_SERVER['HTTP_REFERER'])){
		//if the referer is website itself, then ignore. otherwise, go ahead.
		if (!TimStartsWith($_SERVER['HTTP_REFERER'], 'http://'.$_SERVER['SERVER_NAME']) &&
			!TimStartsWith($_SERVER['HTTP_REFERER'], 'https://'.$_SERVER['SERVER_NAME'])){
			$_SESSION['mm_referalurl'] = $_SERVER['HTTP_REFERER'];
		}
	}
		
	if(isset($_REQUEST['t'])){
		$_SESSION['mm_adwords_t'] = $_REQUEST['t'];
	}
	if(isset($_REQUEST['k'])){
		$_SESSION['mm_adwords_k'] = $_REQUEST['k'];
	}
	if(isset($_REQUEST['a'])){
		$_SESSION['mm_adwords_a'] = $_REQUEST['a'];
	}    
}
add_action('init','register_alciframeform_session');


function alciframeform_shortcode_iframe_contact_form( $atts, $content = null ) {
 
	$options = get_option( 'alciframeform_option_name' );
	
	$_baseurl = isset($atts['baseurl']) ? $atts['baseurl'] : $options['baseurl'];
	if(empty($_baseurl)) return 'Error: Base URL is not set. ';
	
	$_cid = isset($atts['cid']) ? $atts['cid'] : $options['cid'];
	if(empty($_cid)) return 'Error: CID is not set. ';
	
	$_style = isset($atts['style']) ? $atts['style'] : $options['style'];
	$_svs   = isset($atts['svs']) ? $atts['svs'] : $options['svs'];
	$_mode  = isset($atts['mode']) ? $atts['mode'] : $options['mode'];
	
	$_css   = isset($atts['css']) ? $atts['css'] : $options['css'];
	$_css   = urlencode($_css);
	
	$referurl = urlencode($_SESSION['mm_referalurl']);

	$contact_form.= "".    
		'
		<iframe id="iframe_enquiry_form" style="'.$_style.'" src="'.$_baseurl.'?cid='.$_cid.'&mode='.$_mode.'&svs='.$_svs.'&css='.$_css.'&ref='.$referurl.'&t='.$_SESSION['mm_adwords_t'].'&k='.$_SESSION['mm_adwords_k'].'&a='.$_SESSION['mm_adwords_a'].'" frameborder="0" align="center"></iframe>
		';
	
	return $contact_form;
}

function register_alciframeform_shortcodes(){
   add_shortcode('iframe_contact_form', 'alciframeform_shortcode_iframe_contact_form');
}

add_action( 'init', 'register_alciframeform_shortcodes');
//sample usage: [iframe_contact_form baseurl="http://applicationform.hatpacks.com.au/EnquiryForm/default.aspx" style="width: 90%; height: 900px;" cid="CC" svs="0" css="http://test2.cleancredit.com.au/site/wp-content/themes/cleancredit/styles/enquiry-form.css"]


//create setting page
if( is_admin() ){
	include "settings.php";
    $alciframeform_settings_page = new ALCIframeFormSettingsPage();
}
    
//create setting quick link 
function alciframeform_plugin_action_links($links, $file) {
    static $this_plugin;

    if (!$this_plugin) {
        $this_plugin = plugin_basename(__FILE__);
    }

    if ($file == $this_plugin) {
        $settings_link = '<a href="' . get_bloginfo('wpurl') . '/wp-admin/options-general.php?page=alciframeform-setting-admin">Settings</a>';
        array_unshift($links, $settings_link);
    }

    return $links;
}
add_filter('plugin_action_links', 'alciframeform_plugin_action_links', 10, 2);

//functions
if(!function_exists("TimStartsWith")){
	function TimStartsWith($haystack, $needle){
	    return !strncmp($haystack, $needle, strlen($needle));
	}
}