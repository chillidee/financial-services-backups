<?php

// BEGIN iThemes Security - Do not modify or remove this line
// iThemes Security Config Details: 2
define( 'FORCE_SSL_ADMIN', true ); // Force SSL for Dashboard - Security > Settings > Secure Socket Layers (SSL) > SSL for Dashboard
define( 'DISALLOW_FILE_EDIT', true ); // Disable File Editor - Security > Settings > WordPress Tweaks > File Editor
// END iThemes Security - Do not modify or remove this line

/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('WP_CACHE', true); //Added by WP-Cache Manager
define( 'WPCACHEHOME', '/home/admin/web/justbudget.com.au/public_html/wp-content/plugins/wp-super-cache/' ); //Added by WP-Cache Manager
define( 'DB_NAME', 'admin_jb' );

/** MySQL database username */
define( 'DB_USER', 'admin_jb' );

/** MySQL database password */
define( 'DB_PASSWORD', 'mtPzPJpQXR' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '$=Dla2A8D4zGA4?hxEf[1yh<uU|FWn)F4#chae |M Csgi>9!QWk;;&p_XDm80sd');
define('SECURE_AUTH_KEY',  'uuA6CE)A<Vu.XvLQf4E=xv%zfm&.FA-d4%fpev~ah9SceVI9x$bmhWg}dT]e$dnW');
define('LOGGED_IN_KEY',    'B1kZ|-J3$23BCF7aH`;%yOY/H9Tar@T*h7]~%=KU_W[<HY/&x|QWBEt[,+Hv?^dI');
define('NONCE_KEY',        ',/|Ri[ExAA~@u)4.psD|PV9Qs1kqlQ$+ue+at=bXuuN6jqTS1hA`;[rg4B9FF#kq');
define('AUTH_SALT',        '~q^81)b|Ulm?bB=qAM[rIpy+OgAGAN6&s.!`JtKvGa}|!fk/5Z,7a#>fAQW5hM>P');
define('SECURE_AUTH_SALT', 'l9q|(|_%=r(Y5/0#^@h?hTOc|WNVY) $<lmx=*-B[;Gfai+;}z3fbGWq:lHWymeP');
define('LOGGED_IN_SALT',   'PR5#xk)%bIo<9i6}o,-eY@;F7h){ u43}J),nX2NM_7N/F/xmLriNqA8^o{/2mnl');
define('NONCE_SALT',       '@9rUW*t)QYk[.xZM%tz69PYCK%&:}3Rkr]z.argX;w$g~6SXFh*ZM2mSRr@jFo7k');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'rlzw0js09q_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);
define( 'WP_AUTO_UPDATE_CORE', false );

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
  define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');