<?php

/**
 * The Header for the theme.
 *
 * @package Favea
 * @since Favea 1.0
 */

?>

<?php EfGetOptions::init(); ?>
<?php $ef_data = EfGetOptions::TplConditions(); ?>


<?php 
  // Get Referer URL and ready to push into iFrame form
  if(!empty($_SERVER['HTTPS_REFERER'])){
    //if the referer is website itself, then ignore. otherwise, go ahead.
    if (!startsWith($_SERVER['HTTPS_REFERER'], 'https://'.$_SERVER['SERVER_NAME']) &&
      !startsWith($_SERVER['HTTPS_REFERER'], 'https://'.$_SERVER['SERVER_NAME'])){
      $_SESSION['mm_referalurl'] = $_SERVER['HTTPS_REFERER'];
    }
  }
  if(isset($_REQUEST['t'])){
    $_SESSION['mm_adwords_t'] = $_REQUEST['t'];
  }
  if(isset($_REQUEST['k'])){
    $_SESSION['mm_adwords_k'] = $_REQUEST['k'];
  }
  if(isset($_REQUEST['a'])){
    $_SESSION['mm_adwords_a'] = $_REQUEST['a'];
  }
?>  
  
<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="viewport" content="width=device-width, user-scalable=no">

<!-- Title  -->
<title><?php wp_title( '&#124;', true, 'right' ); ?></title>

<!-- Favicon  -->
 <?php if ( !empty( $ef_data['ef_custom_favicon.'] ) ) { ?>
   <link rel="icon" type="image/png" href="/wp-content/uploads/2014/01/favicon.ico<?php //echo $ef_data['ef_custom_favicon']; ?>" />
 <?php } ?>

<link rel="profile" href="https://gmpg.org/xfn/11" />

<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />

<?php wp_head(); ?>

<!-- for pricing grid -->
    <link rel="stylesheet" href="<?php echo get_bloginfo('template_directory'); ?>/inc/pricing-table/source/pricing.css" type="text/css" media="screen" />
    <script type="text/javascript" src="<?php echo get_bloginfo('template_directory'); ?>/inc/pricing-table/common/js/custom.js"></script>
<!-- end of pricing grid -->

<!-- for popup color box -->
  <link rel="stylesheet" href="<?php echo get_bloginfo('template_directory'); ?>/inc/colorbox/colorbox.css" type="text/css" media="screen" />
    <script type="text/javascript" src="<?php echo get_bloginfo('template_directory'); ?>/inc/colorbox/jquery.colorbox.js"></script>
    <script type="text/javascript">
      jQuery(document).ready(function(){
      jQuery(".inline").colorbox({inline:true, width:"90%", maxWidth:"500px"});
    });
    </script>
<!-- end of popup color box -->
<style>

.action-button
{
        margin-left: 50px !important;
  padding: 10px 40px;
        margin: 0px 10px 10px 0px;
        float: left;
  border-radius: 10px;
  font-family: inherit;
  font-size: 25px;
  text-color: #ffffff !important;
  text-decoration: none;  
}

.animate
{
  transition: all 0.1s;
  -webkit-transition: all 0.1s;
}

.yellow
{
  background-color: #fe7e11;
  border-bottom: 5px solid #d16413;
}

.action-button:active
{
  transform: translate(0px,5px);
        -webkit-transform: translate(0px,5px);
  border-bottom: 1px solid;
}

.home enquire {
  background:-webkit-gradient(linear, left top, left bottom, color-stop(0.05, #b8e356), color-stop(1, #a5cc52));
  background:-moz-linear-gradient(top, #b8e356 5%, #a5cc52 100%);
  background:-webkit-linear-gradient(top, #b8e356 5%, #a5cc52 100%);
  background:-o-linear-gradient(top, #b8e356 5%, #a5cc52 100%);
  background:-ms-linear-gradient(top, #b8e356 5%, #a5cc52 100%);
  background:linear-gradient(to bottom, #b8e356 5%, #a5cc52 100%);
  filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#b8e356', endColorstr='#a5cc52',GradientType=0);
  background-color:#b8e356;
  -moz-border-radius:3px;
  -webkit-border-radius:3px;
  border-radius:3px;
  border:1px solid #83c41a;
  display:inline-block;
  cursor:pointer;
  color:#ffffff;
  font-family:arial;
  font-size:26px;
  font-weight:bold;
  padding:6px 24px;
  text-decoration:none;
  text-shadow:-3px 2px 0px #86ae47;
}
.home enquire:hover {
  background:-webkit-gradient(linear, left top, left bottom, color-stop(0.05, #a5cc52), color-stop(1, #b8e356));
  background:-moz-linear-gradient(top, #a5cc52 5%, #b8e356 100%);
  background:-webkit-linear-gradient(top, #a5cc52 5%, #b8e356 100%);
  background:-o-linear-gradient(top, #a5cc52 5%, #b8e356 100%);
  background:-ms-linear-gradient(top, #a5cc52 5%, #b8e356 100%);
  background:linear-gradient(to bottom, #a5cc52 5%, #b8e356 100%);
  filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#a5cc52', endColorstr='#b8e356',GradientType=0);
  background-color:#a5cc52;
}
.home enquire:active {
  position:relative;
  top:1px;
}

.myButton {
  background:-webkit-gradient(linear, left top, left bottom, color-stop(0.05, #b8e356), color-stop(1, #81ac27));
  background:-moz-linear-gradient(top, #b8e356 5%, #81ac27 100%);
  background:-webkit-linear-gradient(top, #b8e356 5%, #81ac27 100%);
  background:-o-linear-gradient(top, #b8e356 5%, #81ac27 100%);
  background:-ms-linear-gradient(top, #b8e356 5%, #81ac27 100%);
  background:linear-gradient(to bottom, #b8e356 5%, #81ac27 100%);
  filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#b8e356', endColorstr='#a5cc52',GradientType=0);
  background-color:#b8e356;
  -moz-border-radius:3px;
  -webkit-border-radius:3px;
  border-radius:3px;
  border:1px solid #83c41a;
  display:inline-block;
  cursor:pointer;
  color:#ffffff;
  font-family:arial;
  font-size:20px;
  font-weight:bolder;
  padding:6px 24px;
  text-decoration:none;
  text-shadow:-2px 1px 0px #33352f;
}
.myButton:hover {
  background:-webkit-gradient(linear, left top, left bottom, color-stop(0.05, #a5cc52), color-stop(1, #b8e356));
  background:-moz-linear-gradient(top, #a5cc52 5%, #b8e356 100%);
  background:-webkit-linear-gradient(top, #a5cc52 5%, #b8e356 100%);
  background:-o-linear-gradient(top, #a5cc52 5%, #b8e356 100%);
  background:-ms-linear-gradient(top, #a5cc52 5%, #b8e356 100%);
  background:linear-gradient(to bottom, #a5cc52 5%, #b8e356 100%);
  filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#a5cc52', endColorstr='#b8e356',GradientType=0);
  background-color:#a5cc52;
}
.myButton:active {
  position:relative;
  top:1px;
}

.ef-radius ef-bottom-1_5 clearfix { width: 280px; !important
            padding-left:10px;
} 

.large-9 columns {wdth:710px; !important}

. textwidget { height:900px; }

large-9 columns {wdth:710px; !important}

</style>

</head>

<body <?php body_class(); ?>>

<?php if ( EF_DEMO_CHANGER ) ef_demo_changer(); ?>

<div id="ef-container">
  <div class="ef-wrapper">
    <header class="hide-for-print">

      <section class="ef-nav-wrapper">

        <?php if ( has_nav_menu( 'top' ) || isset( $ef_data['to_support'] ) ) { ?>
          <div id="ef-top">
            <div class="row">

              <!-- Top nav -->

              <div class="large-6 small-12 columns">
                <div class="ef-left-wrap text-center clearfix">

                  <?php if ( function_exists( 'icl_get_languages' ) ) { ?>
                    <?php do_action( 'icl_language_selector' ); ?>
                  <?php } ?>

                  <?php if ( function_exists( 'qtrans_generateLanguageSelectCode' ) ) { ?>
                    <?php qtrans_generateLanguageSelectCode( 'image' ); ?>
                  <?php } ?>

                  <nav>
                    <?php ef_top_navigation(); ?>
                  </nav>
                </div><!-- .ef-left-wrap.clearfix -->
              </div><!-- .large-5.small-12.columns -->

              <!-- Social Profiles -->

              <?php if ( isset( $ef_data['to_support'] ) ) { ?>

                <div class="large-6 small-12 columns text-center">

                  <hr class="show-for-small">

                  <ul class="ef-social-block text-left right">

                    <li>

                      <ul class="ef-soc-icons">

                        <?php $n = $l = 1; ?>
                        <?php foreach ( $ef_data['socialize'] as $icon_id => $icon ) : ?>
                          <?php if ( $l <= 10 && isset( $ef_data[$icon_id] ) && !empty( $ef_data[$icon_id] ) ) { ?>
                            <?php echo '<li><a class="'.$icon_id.' ef-tipsy-n" target="_blank" href="'.$ef_data[$icon_id].'" title="'.esc_attr( $icon ).'"></a></li>'; ?>
                            <?php $n = 8; $l++; ?>
                          <?php } else { ?>
                            <?php if ( $n <= 7 ) { ?>
                              <?php echo '<li><a class="'.$icon_id.' ef-tipsy-n" target="_blank" href="#" title="'.esc_attr( $icon ).'"></a></li>'; $n++; ?>
                            <?php } ?>
                          <?php } ?>
                        <?php endforeach; ?>
                      </ul>

                    </li>
                  </ul>
<div style="align: center; font-weight:bold; font-size: 1.3em; padding-top: 5px!important;"> </div>
                </div><!-- .large-7.small-12.columns.text-right -->
 
              <?php } ?>

            </div><!-- .row -->
          </div><!-- #ef-top -->

        <?php } ?>

        <div class="ef-navbar-wrapper">

        <?php if ( ( isset( $ef_data['to_support'] ) && !isset( $ef_data['ef_sticky_head'] ) ) xor !empty( $ef_data['ef_sticky_head'] ) ) { ?>
          <div id="ef-navbar">
        <?php } else { ?>
          <div>
        <?php } ?>
            <div class="row">
              <div class="large-12 column">
                <nav class="top-bar">
                  <ul class="title-area">

                    <!-- Logo / site title Area -->

                    <li class="name">
                      <span  style="width:270px!important; height:63px!important;">
                        <a <?php echo !isset( $ef_data['custom_logo'] ) ? 'class="ef-regular-title"' : ''; ?> href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>">
                          <?php if ( isset( $ef_data['custom_logo'] ) ) { ?>
                            <img src="<?php echo $ef_data['custom_logo']; ?>" alt="<?php echo esc_attr( get_bloginfo( 'name' ) ); ?>" />
                          <?php } else { ?>
                            <strong><?php bloginfo( 'name' ); ?></strong>
                          <?php } ?>
                        </a>
                      </span>

                      <div id="ef-site-description">
                        <?php if ( !empty( $ef_data['ef_tagline'] ) || !isset( $ef_data['to_support'] ) ) { ?>
                          <?php bloginfo( 'description' ); ?>
                        <?php } ?>
                      </div>

                    </li>
                    <li class="toggle-topbar menu-icon"><a href="#"><span>Menu</span></a></li>

                  </ul><!-- .title-area -->

                  <section class="top-bar-section">

                    <?php ef_primary_navigation(); ?>

                    <!-- Search form -->

                    <?php if ( !empty( $ef_data['ef_show_search'] ) ) { ?>

                      <ul class="right has-form">
                        <li><?php get_search_form(); ?></li>
                      </ul>

                    <?php } ?>

                  </section><!-- .top-bar-section -->

                </nav><!-- .top-bar -->

              </div><!-- .large-12.column -->

            </div><!-- .row -->

          </div><!-- #ef-navbar -->

        </div>

        <?php if ( ( !is_page_template( 'templates/home-template.php' ) && !is_front_page() ) xor ( is_home() && !is_front_page() ) ) { ?>
          <div class="ef-breadcrumbs ef-bottom-2_18">
            <div class="row">
              <div class="large-12 columns">
                <div class="ef-bread-inner1">
                  <div class="ef-bread-inner2">
                    <div class="row">

                      <div class="large-7 small-12 columns">
                        <h1 style="font-size:1.2em !important;">
                          <?php ef_page_title( $ef_data ); ?>
                          <?php if ( !empty( $ef_data['page_dsc'] ) && !is_archive() && !is_post_type_archive() && !is_search() && !is_404() ) { ?>
                            <small><?php _e( strip_tags( $ef_data['page_dsc'], '<span>' ) ); ?></small>
                          <?php } ?>
                        </h1>
                      </div><!-- .large-6.small-12.columns -->

                      <?php if ( ( isset( $ef_data['to_support'] ) && !isset( $ef_data['on_sw'] ) ) xor !empty( $ef_data['on_sw'] ) ) { ?>
                        <nav class="large-5 small-12 columns text-right">
                          <?php ef_put_breadcrumbs(); ?>
                        </nav>
                      <?php } ?>
                    </div><!-- .row -->
                  </div><!-- .ef-bread-inner2 -->
                </div><!-- .ef-bread-inner1 -->
              </div><!-- .large-12.columns -->
            </div><!-- .row -->
          </div><!-- .ef-breadcrumbs.ef-bottom-2_18 -->
        <?php }  ?>

      </section>

      <?php if($ef_data['staticimagevideopopup_status'] == 'enable'){ ?>
        <section class="ef-slider-container <?php echo empty( $ef_data['welcome'] ) ? 'ef-bottom-2_18 ' : ''; ?>">
          <div id="staticimagevideopopup">
               <div id="video_area">
              <script>
                   jQuery(document).ready(function(){
                     jQuery(".youtube").colorbox({iframe:true, innerWidth:640, innerHeight:390})
                   });
              </script>
                <?php if(!empty($ef_data['staticimagevideopopup_video'])): ?>
                <a class='youtube' href="http://www.youtube.com/embed/<?php echo $ef_data['staticimagevideopopup_video']; ?>?rel=0&amp;wmode=transparent&amp;autoplay=1">
                <?php endif; ?>  
                  <img style="display: block; margin: 0 auto;" src="<?php echo $ef_data['staticimagevideopopup_image']; ?>"/>
                <?php if(!empty($ef_data['staticimagevideopopup_video'])): ?>
                </a>
                <?php endif; ?>  
               </div>
             </div>
        </section>
      <?php } else if ( function_exists( 'putRevSlider' ) && !empty( $ef_data['slider'] ) && !is_search() ) { ?>
        <section class="ef-slider-container <?php echo empty( $ef_data['welcome'] ) ? 'ef-bottom-2_18 ' : ''; ?>">
          <?php putRevSlider( $ef_data['slider'] ); ?>
        </section><!-- .ef-slider-container.ef-bottom-2_18.hide-for-prin -->
      <?php } ?>

    </header><!-- .hide-for-print (header)  -->

    <!-- Content -->

    <section id="ef-content" class="clearfix">