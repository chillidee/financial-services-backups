<?php
/**
 * The template for displaying all pages.
 *
 * @package Favea
 * @since Favea 1.0
 */

?>

<?php get_header(); ?>

<?php $ef_data = EfGetOptions::TplConditions(); ?>

<?php $layout = $ef_data['layouts']; ?>

		<section class="row">

			<div class="large-8 columns">

				<?php get_template_part( 'loop', 'page' ); ?>

			</div><!-- .large-##.columns -->

			<?php if ( $layout['0'] != 12 ) { ?>
				<div class="ef-sidebars hide-for-print">
					<div class="ef-sidebar large-4 columns">
						<?php get_sidebar(); ?>
				  	</div><!-- .ef-sidebar.large-##.columns -->
			  	</div><!-- .ef-sidebars.hide-for-print -->
			<?php } ?>

		</section><!-- .row -->

<?php get_footer(); ?>