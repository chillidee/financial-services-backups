<?php
/**
 * Template Name: JB Home
 *
 * @package Favea
 * @since Favea 1.0
 */

?>

<?php get_header(); ?>
<div class="mobile new-button"><a href="/contact/" target="_self">Enquire Now</a></div>
<?php $ef_data = EfGetOptions::TplConditions(); ?>

<?php $layout = $ef_data['layouts']; ?>
<span class="home-banner">
		<?php if ( !empty( $ef_data['welcome'] ) ) {?>
			<div id="ef-welcome" class="ef-bottom-2_8">
				<div class="row">
					<div class="large-12 columns text-center">
						<!--<?php _e( $ef_data['welcome'] ); ?>-->
						<a href="http://www.justbudget.com.au/contact/" class="myButton">Enquire Now</a>
					</div>
				</div>
			</div>
		<?php } ?>
</span>
		<section class="row">

			
			<div class="large-8 columns">
			<!-- <div class="large-<?php echo $layout['0'] ?> columns"> -->
                               
				<?php get_template_part( 'loop', 'page' ); ?>

				<?php if ( isset( $ef_data['mb_support'] ) && $ef_data['team_extr'] != 'none' ) { ?>

					<?php if ( isset( $ef_data['mb_support'] ) && $ef_data['team_extr'] == 'show_extras' ) { ?>

						<?php get_template_part( 'loop', 'extras' ); ?>

					<?php } else { ?>

						<?php $args = array(
							'post_type' 	     => $ef_data['cpt_support'] ? EF_CPT_TEAM : '',
							'tax_query' 	     => array(
								array(
									'taxonomy'		=> $ef_data['cpt_support'] ? EF_CPT_TEAM_TAX : '',
									'terms'  		=> $ef_data['terms'],
									'field'      	=> 'id',
									'operator'   	=> 'NOT IN'
								)
							),
							'order'    		      => 'ASC',
							'post_status'	      => 'publish',
							'posts_per_page'      => -1,
							'ignore_sticky_posts' => true,
						); ?>


				    	<?php /* New wp_query object for the the Team */ ?>
				    	<?php $postslist = NULL; $postslist = new WP_Query( $args ); ?>

				    	<?php if ( ! $postslist->have_posts() ) { ?>

			    			<?php ef_get_msg_data( $postslist->query_vars['post_type'], $ef_data ); ?>

				    	<?php } else { ?>

							<div class="row">
								<div class="large-12 columns ef-bottom-1_5">

									<?php if ( ( isset( $ef_data['mb_support'] ) && $ef_data['latest_list'] == 'list' ) || !isset( $ef_data['mb_support'] ) ) { ?>
										<?php if ( $layout['0'] != '12' ) { ?>
											<?php $chunked_posts = array_chunk( $postslist->posts, 3 ); ?>
										<?php } else { ?>
											<?php $chunked_posts = array_chunk( $postslist->posts, 4 ); ?>
										<?php } ?>
									<?php } else { ?>
										<?php $chunked_posts = array( $postslist->posts ); ?>
									<?php } ?>

									<section class="row">
								    	<div class="large-9 small-9 columns">

											<?php if ( !empty( $ef_data['txt_latest'] ) ) { ?>

												<div class="ef-style-title"><?php echo '<span>' . __( strip_tags( $ef_data['txt_latest'], '<span>' ) ) . '</span>'; ?>
							    				</div>

											<?php } ?>

				    					</div><!-- .large-9.small-9.columns  -->

				    					<?php foreach ( $chunked_posts as $number ) : ?>
				    						<?php $post_count = count( $number ); ?>
				    					<?php endforeach; ?>

				    					<?php $col = $layout['0'] != '12' ? 3 : 4; ?>

				    					<?php if ( isset( $ef_data['latest_list'] ) && $ef_data['latest_list'] == 'carousel' && $post_count > $col ) { ?>

					    					<div class="large-3 small-3 columns text-right ef-carousel-nav hide-for-print">
					    						<a id="ef-prev" href="#"></a>
					    						<a id="ef-next" href="#"></a>
				    						</div><!-- .large-3 ... .ef-carousel-nav -->

				    					<?php } ?>

				    				</section><!-- .row  -->

				    				<?php foreach ( $chunked_posts as $chunked_post ) : ?>

				    					<?php $tmp_columns = 12 / count( $chunked_post ); ?>
				    					<?php $columns = count( $chunked_post ) <= 4 ? 'large-'.$tmp_columns.' columns' : ''; ?>

					    				<section class="row">
					    					<div class="large-12 columns">
					    						<div class="ef-team clearfix">

					    							<?php $post_count = count( $chunked_post ); ?>

					    							<div class="<?php echo $columns != '' && !( isset( $ef_data['mb_support'] ) && $ef_data['latest_list'] == 'carousel' ) || !isset( $ef_data['mb_support'] ) ? 'ef-latest-list' : 'ef-carousel'; ?>">

					    								<?php foreach ( $chunked_post as $post ) : ?>

					    									<?php setup_postdata( $post ); ?>

					    										<?php $if_has_more = strpos( $post->post_content, '<!--more-->' ); ?>

					    										<article class="ef-carousel-item <?php echo $columns; ?> <?php echo post_password_required() ? 'ef-protected-post' : ''; ?> ef-radius">

						    										<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

																		<?php if ( has_post_thumbnail() ) { ?>

							    											<?php $ef_excerpt = isset( $ef_data['mb_support'] ) && $ef_data['team_content'] == 'ef-excerpt' ? 1 : NULL; ?>

																			<?php echo $ef_excerpt || $if_has_more ? '<a href="'.esc_url( get_permalink() ).'" class="ef-proj-img">' : '<div class="ef-proj-img">'; ?>

																				<?php echo get_the_post_thumbnail( $post->ID, 'cpt_thumb_large' ); ?>

																				<span class="ef-loader"><span></span></span>

																				<?php if ( post_password_required() ) { ?>
																					<span class="ef-pass"></span>
																				<?php } ?>

																				<?php $member_pos = isset( $ef_data['mb_support'] ) ? efmb_meta( 'ef_member_pos', 'type=text' ) : ''; ?>
																				<?php if ( !empty( $member_pos ) ) { ?>
																					<p class="ef-item-title"><?php _e( strip_tags( $member_pos ) ); ?></p>
																				<?php } ?>
																			<?php echo $ef_excerpt || $if_has_more ? '</a>' : '</div>'; ?>

																		<?php } else { ?>
																			<?php echo '<img src="' . trailingslashit( get_template_directory_uri() ) . 'images/default-team.jpg'.'" alt="" />'; ?>
																		<?php } ?>

																		<div class="ef-team-desc">
																			<h1><?php the_title(); ?></h1>
																			<?php if ( isset( $ef_data['mb_support'] ) && $ef_data['team_content'] == 'ef-full' || $if_has_more ) { ?>
																				<?php global $more; $more = 0; ?>
																				<?php the_content( __( 'Read more &rarr;', EF_TDM ) ); ?>
																				<?php $more = 1; ?>
																			<?php } else { ?>
																				<?php the_excerpt(); ?>
																			<?php } ?>
																		</div>

																		<?php if ( isset( $ef_data['mb_support'] ) ) { ?>

																			<ul class="ef-soc-icons clearfix">
																				<?php foreach ( $ef_data['socialize'] as $icon_id => $icon ) { ?>

																					<?php $ef_team_icn = efmb_meta( $icon_id, 'type=text' ); ?>

																					<?php if ( !empty( $ef_team_icn ) ) { ?>
																						<?php echo '<li><a class="'.$icon_id.' ef-tipsy-n" target="_blank" href="'.esc_url( $ef_team_icn ).'" title="'.esc_attr( $icon ).'"></a></li>'; ?>
																					<?php } ?>
																				<?php } // foreach(){} ?>
																			</ul>
																		<?php } ?>

						    										</div><!-- #post-## -->
						    									</article>

					    								<?php endforeach; ?>

					    							</div><!-- .ef-carousel/.ef-latest-list -->
					    						</div><!-- .ef-latest-works -->
					    					</div><!-- .large-12.columns  -->
					    				</section><!-- .row  -->

					    			<?php endforeach; // have_posts ?>

					    			<?php if ( isset( $ef_data['mb_support'] ) && $ef_data['latest_list'] == 'list' || $post_count <= 4 ) { ?>
				    					<hr class="ef-blank ef-bottom-1_5 hide-for-print">
				    				<?php } ?>

			    				</div><!-- .large-12.columns  -->
			    			</div><!-- .row  -->

			    			<hr class="print-only">

			    		<?php } // have_posts() ?>
					<?php } ?>
				<?php } ?>
			</div><!-- .large-##.columns  -->

			<?php if ( $layout['0'] != 12 ) { ?>
				<div class="ef-sidebars hide-for-print">
				
		                        <div class="ef-sidebar large-4 columns">		
					<!-- <div class="ef-sidebar large-<?php echo $layout['1'] ?> columns"> -->
						<?php get_sidebar(); ?>
				  	</div><!-- .ef-sidebar.large-##.columns -->
			  	</div><!-- .ef-sidebars.hide-for-print -->
			<?php } ?>

		</section><!-- .row  -->

<?php get_footer(); ?>