<?php
/**
 * Template Name: Contact
 *
 * @package Favea
 * @since Favea 1.0
 */

?>

<?php get_header(); ?>
		<!-- Scripts -->
		<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
		<script type="text/javascript" src="/forms/jb/js/iframeResizer.min.js"></script>

<?php $ef_data = EfGetOptions::TplConditions(); ?>

<?php $layout = $ef_data['layouts']; ?>

		<section class="row">
		
						<?php if ( $layout['0'] != 8 ) { ?>
				<div class="ef-sidebars hide-for-print">
		<div class="ef-sidebar large-4 columns">		
					<!-- <div class="ef-sidebar large-<?php echo $layout['1'] ?> columns"> -->
					
					
					<?php get_sidebar( 'Form' ); ?>
					
					<script type="text/javascript">iFrameResize({log:true,enablePublicMethods:true,resizedCallback:function(e){$("p#callback").html("<b>Frame ID:</b> "+e.iframe.id+" <b>Height:</b> "+e.height+" <b>Width:</b> "+e.width+" <b>Event type:</b> "+e.type)},messageCallback:function(e){$("p#callback").html("<b>Frame ID:</b> "+e.iframe.id+" <b>Message:</b> "+e.message);alert(e.message)},closedCallback:function(e){$("p#callback").html("<b>IFrame (</b>"+e+"<b>) removed from page.</b>")}})</script>
					
						<!--<?php get_sidebar(); ?>
						
				  	</div><!-- .ef-sidebar.large-##.columns -->
			  	</div><!-- .ef-sidebars.hide-for-print -->
			<?php } ?>
			
			
			<div class="large-8 columns">
			<!-- <div class="large-<?php echo $layout['0'] ?> columns"> -->

				<?php $lat_lng = isset( $ef_data['mb_support'] ) && ! ( ef_array_empty( $ef_data['map_lat'] ) && ef_array_empty( $ef_data['map_long'] ) ); ?>

				<?php if ( $lat_lng ) { ?>
					<div id="map_canvas" class="ef-map"></div>
				<?php } ?>

				<?php get_template_part( 'loop', 'page' ); ?>

			</div><!-- .large-##.columns -->
			




		</section><!-- .row -->
		
		

<?php get_footer(); ?>