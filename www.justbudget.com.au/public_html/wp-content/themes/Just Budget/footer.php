<?php
/**
 * The template for displaying the footer.
 *
 * @package Favea
 * @since Favea 1.0
 */
?>

<?php $ef_data = EfGetOptions::TplConditions(); ?>

		</section><!-- #ef-content -->

		<?php if ( is_page_template( 'templates/home-template.php' ) && isset( $ef_data['mb_support'] ) && !empty( $ef_data['img_array'] ) ) { ?>
			
			<div class="ef-carousel-mask1">
				<div class="ef-carousel-mask2">
					<div class="ef-carousel-mask3">
						<div class="ef-carousel-mask4">

							<section class="row">
								<div class="large-12 columns">

					    			<div id="ef-clients-carousel" class="ef-radius clearfix">
					    				
					    				<div class="ef-carousel-nav">
					    					<a id="ef-prev1" href="#"></a>
					    					<a id="ef-next1" href="#"></a>
					    				</div>
					    				
					    				<div class="ef-clients-carousel clearfix">
					    					<?php foreach ( $ef_data['img_array'] as $client ) { ?>
					    					    <?php echo "<img src='{$client['full_url']}' height='{$client['height']}' alt='{$client['alt']}' />"; ?>
					    					<?php } ?>						    					
					    				</div><!-- .ef-clients-carousel  -->
					    			</div><!-- #ef-clients-carousel  -->

					    		</div><!-- .large-12.columns  -->
					    	</section><!-- .row  -->

			    		</div><!-- .ef-carousel-mask4  -->
		    		</div><!-- .ef-carousel-mask3  -->			    			
	    		</div><!-- .ef-carousel-mask2  -->
	    	</div><!-- .ef-carousel-mask1  -->
		<?php } ?>

		<footer id="ef-footer" class="hide-for-print">

			<?php get_sidebar( 'footer' ); ?>

			<section id="ef-footer-bottom">
				<div class="row">
					<div class="large-7 columns">

						<?php if ( !empty( $ef_data['ef_footer_text'] ) ) { ?>
							<p><?php echo __( $ef_data['ef_footer_text'] ); ?></p>
						<?php } else { ?>
							<p><a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>"><?php bloginfo( 'name' ); ?></a></p>
						<?php } ?>
					</div><!-- .large-7.columns -->

					<?php if ( has_nav_menu( 'secondary' ) ) { ?>
						<div class="large-5 columns text-right">
							<nav>
								<?php ef_footer_navigation(); ?>
							</nav>
						</div><!-- .large-5.columns.text-right -->
					<?php } ?>

				</div><!-- .row -->
			</section><!-- #ef-footer-bottom -->

		</footer><!-- #ef-footer -->

	</div><!-- .ef-wrapper -->
</div><!-- .ef-container -->

<?php wp_footer(); ?>

<?php echo !empty( $ef_data['ef_google_analytics'] ) ? stripslashes( $ef_data['ef_google_analytics'] ) : ''; ?>
</body>
</html>