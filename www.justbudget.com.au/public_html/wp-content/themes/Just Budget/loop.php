<?php
/**
 * The loop that displays posts.
 *
 * @package Favea
 * @since Favea 1.0
 */

?>

<?php $ef_data = EfGetOptions::TplConditions(); ?>

		<?php /* If there are no posts to display, such as an empty archive page */ ?>

		<?php if ( ! have_posts() ) { ?>
			<?php ef_get_msg_data( $wp_query->query_vars['post_type'], $ef_data ); ?>
		<?php } ?>

		<?php if ( isset( $ef_data['feed_layout'] ) && $ef_data['feed_layout'] == 'ef-blog-4' ) { ?>
			<div class="ef-grid-blog clearfix">
		<?php } elseif ( is_tax() || is_post_type_archive() ) { ?>
			<div class="row">
		<?php } ?>

		<?php $n = 1; ?>

		<?php while ( have_posts() ) : the_post(); ?>

			<?php $if_has_more = strpos( $post->post_content, '<!--more-->' ); ?>

			<?php if ( is_tax() || is_post_type_archive() ) { ?>
				<div class="ef-post large-6 columns">
			<?php } else { ?>
				<div class="ef-post">
			<?php } ?>

				<div id="post-<?php the_ID(); ?>" <?php post_class( 'clearfix' ); ?>>

					<header>

						<div>
							<a class="ef-latest-thumb" href="<?php esc_url( the_permalink() ); ?>" title="<?php the_title_attribute(); ?>">
								<?php the_title(); ?>

								<?php if ( isset( $ef_data['feed_layout'] ) && ( $ef_data['feed_layout'] == 'ef-blog-2' || $ef_data['feed_layout'] == 'ef-blog-4' ) ) { ?>
									<span class="<?php echo !comments_open() ? 'ef-closed-comments ' : '' ?>ef-date-comment text-center">

										<span class="ef-date ef-round">

											<span><?php the_time( 'j' ); ?></span><br />
											<span class="ef-uppercase"><?php the_time( 'M' ); ?></span>

										</span><!-- .ef-date.ef-round  -->

										<?php if ( comments_open() ) { ?>
											<span class="ef-comments hide-for-print"><?php echo get_comments_number(); ?></span>
										<?php } ?>
									
									</span><!-- .ef-date-comment.text-center  -->
								<?php } ?>

								<?php if ( has_post_thumbnail() ) { ?>

									<?php if ( isset( $ef_data['feed_layout'] ) && ( $ef_data['feed_layout'] == 'ef-blog-3' || $ef_data['feed_layout'] == 'ef-blog-4' ) ) { ?>
									<span class="ef-blog-s3-img ef-proj-img">
											<?php if ( $ef_data['feed_layout'] == 'ef-blog-3' ) { ?>
												<?php echo $ef_data['layouts']['0'] != '12' ? get_the_post_thumbnail( $post->ID, 'post_medium_thumb' ) : get_the_post_thumbnail( $post->ID, 'post_large_thumb' ); ?>
											<?php } else { ?>
												<?php echo get_the_post_thumbnail( $post->ID, 'cpt_thumb' ); ?>
											<?php } ?>
									<?php } else { ?>
									<span class="ef-from-blog-img ef-proj-img">
											<?php echo get_the_post_thumbnail( $post->ID, 'small_thumb' ); ?>
									<?php } ?>
										<span class="ef-loader"><span></span></span>
										<?php if ( post_password_required() ) { ?>
											<span class="ef-pass"></span>
										<?php } ?>
									</span><!-- .ef-blog-s3-img/.ef-from-blog-img  -->
									<?php if ( isset( $ef_data['feed_layout'] ) && $ef_data['feed_layout'] == 'ef-blog-4' ) { ?>
										<span class="clear"></span>
									<?php } ?>
								<?php } ?>
							</a>
						</div>

						<?php if ( !is_search() && !is_tax() && !is_post_type_archive() ) { ?>
							<?php ef_theme_posted_on( $ef_data ); ?>
						<?php } ?>

					</header>

					<?php if ( !is_archive() || !is_search() ) { ?>
						<?php if ( !post_password_required() ) { ?>
							<?php if ( isset( $ef_data['extr_content'] ) && $ef_data['extr_content'] =='ef-content' && !is_sticky() || $if_has_more ) { ?>
								<?php global $more; $more = 0; ?>
								<?php the_content( __( 'Read more &rarr;', EF_TDM ) ); ?>
								<?php $more = 1; ?>
							<?php } else { ?>
								<?php the_excerpt(); ?>
							<?php } ?>
						<?php } ?>
					<?php } ?>

				</div><!-- #post-##  -->

			</div><!-- .ef-post.large-6.columns/.ef-post  -->

			<?php if ( is_tax() || is_post_type_archive() ) { ?>
				<?php $n++; ?>
				<?php if ( $n%2 ) { ?>
					<hr class="ef-blank">
				<?php } ?>
			<?php } ?>

		<?php endwhile; // End the loop. ?>

	<?php if ( is_tax() || is_post_type_archive() ) { ?>
		</div><!-- .row -->
	<?php } elseif ( isset( $ef_data['feed_layout'] ) && $ef_data['feed_layout'] == 'ef-blog-4' ) { ?>
		</div><!-- .ef-grid-blog.clearfix -->
	<?php } ?>