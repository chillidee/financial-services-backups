<?php
/**
 * The loop that displays Extra posts.
 *
 * @package Favea
 * @since Favea 1.0
 */
?>

<?php $ef_data = EfGetOptions::TplConditions(); ?>

			<!-- Services -->

			<?php $args = array(
				'post_type'   => $ef_data['cpt_support'] ? EF_CPT_EXTR : '',
				'tax_query'   => array(
					array(
						'taxonomy' => $ef_data['cpt_support'] ? EF_CPT_EXTR_TAX : '',
						'terms'   => $ef_data['terms_extras'],
						'field'   => 'id',
						'operator'  => 'NOT IN',
					)
				),
				'order'    => 'ASC',
				'post_status'  => 'publish',
				'posts_per_page' => -1,
				'ignore_sticky_posts'=> true,
			); ?>

			<?php /* New wp_query for the Extras */ ?>

			<?php $postslist = NULL; $postslist = new WP_Query( $args ); ?>

			<?php if ( ! $postslist->have_posts() ) { ?>

				<?php ef_get_msg_data( $postslist->query_vars['post_type'], $ef_data ); ?>

			<?php } else { ?>

				<div class="row">
					<div class="large-12 columns">

						<?php if ( !empty( $ef_data['txt_extras'] ) ) { ?>
							<div class="ef-style-title"><?php echo '<span>' . __( strip_tags( $ef_data['txt_extras'], '<span>' ) . '</span>' ); ?></div>
						<?php } ?>

						<?php $chunked_posts = array_chunk( $postslist->posts, 4 ); ?>

						<?php foreach ( $chunked_posts as $chunked_post ) : ?>

							<section class="row">
								<div class="ef-services">

									<?php $tmp_columns = 12 / count( $chunked_post ); ?>
									<?php $columns = count( $chunked_post ) <= 4 ? $tmp_columns : ''; ?>

									<?php foreach ( $chunked_post as $post ) : ?>

										<?php setup_postdata( $post ); ?>

										<?php $if_has_more = strpos( $post->post_content, '<!--more-->' ); ?>

										<?php $extr_lnk = isset( $ef_data['mb_support'] ) && $ef_data['extr_content'] == 'ef-content' ? 'ef-no-extr-link' : ''; ?>

										<article class="large-<?php echo $columns; ?> columns text-center <?php echo $ef_data['extra_style']; ?> <?php echo !$if_has_more ?  $extr_lnk : ''; ?>">

											<?php $icon_extra = $ef_data['mb_support'] ? efmb_meta( 'ef_extra_icons', 'type=layout' ) : ''; ?>

											<?php if ( !empty( $icon_extra ) ) { ?>

												<?php if ( $ef_data['extr_content'] !='ef-content' || $if_has_more ) { ?>
													<a href="<?php the_permalink(); ?>" class="ef-icon <?php echo $icon_extra ?>"></a>
												<?php } else { ?>
													<div class="ef-icon <?php echo $icon_extra ?>"></div>
												<?php } ?>

											<?php } ?>

											<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

												<?php if ( isset( $ef_data['mb_support'] ) && $ef_data['extr_content'] !='ef-content' || $if_has_more ) { ?>
													<div class="entry-title"><a href="<?php the_permalink(); ?>" title="<?php the_title_attribute() ?>"><?php the_title(); ?></a></div>
												<?php } else { ?>
													<div class="entry-title"><?php the_title(); ?></div>
												<?php } ?>

												<?php if ( isset( $ef_data['mb_support'] ) && $ef_data['extr_content'] =='ef-content' || $if_has_more ) { ?>
													<?php global $more; $more = 0; ?>
													<?php the_content( '', false ); ?>
													<?php $more = 1; ?>
												<?php } else { ?>
													<?php the_excerpt(); ?>
												<?php } ?>

											</div><!-- #post-##  -->

										</article>

									<?php endforeach;?>

									<?php wp_reset_postdata(); ?>

								</div><!-- .ef-services  -->
							</section><!-- .row  -->

					    <?php endforeach; ?>

					</div><!-- .large-12.columns  -->
		    	</div><!-- .row  -->

		    	<div class="row">
		    		<div class="large-12 columns">
		    			<hr class="ef-blank hide-for-print">
		    		</div><!-- .large-12.columns  -->
		    	</div><!-- .row  -->

		    	<hr class="print-only">

			<?php } // have_posts() ?>
