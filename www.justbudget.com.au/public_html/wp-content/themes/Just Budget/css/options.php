<?php

$logo_padd = $efto_data['ef_logo_padd'];

$main_col = $efto_data['ef_main_col'];
$second_col = $efto_data['ef_second_col'];

$bg_image = $efto_data['ef_custom_bg'];
$bg_color = $efto_data['ef_body_background'];
$bg_pos = $efto_data['ef_bg_pos'];

$body_face = ef_theme_font_family( $efto_data['ef_content_font']['face'] );
$body_color = $efto_data['ef_content_font']['color'];

$heading_face = ef_theme_font_family( $efto_data['ef_heading_font']['face'] );
$heading_style = ef_theme_font_style( $efto_data['ef_heading_font']['style'] );

?>

.top-bar .title-area .name {
	padding-top: <?php echo !empty($logo_padd) ? $logo_padd : '28px' ?>
}

.ef-main-bgcolor, button, .button, button.disabled, button[disabled], .button.disabled, .button[disabled], button.disabled:hover, button.disabled:focus, button[disabled]:hover, button[disabled]:focus, .button.disabled:hover, .button.disabled:focus, .button[disabled]:hover, .button[disabled]:focus, .top-bar-section ul li > a.button, .alert-box, .breadcrumbs li span, .label, .pagination li.current a, .pagination li.current a:hover, .pagination li.current a:focus, .panel.callout, .progress .meter, .sub-nav dt.active a, .ef-team .ef-item-title, 
.sub-nav dd.active a, #ef-top ul.lang-sw li.ef-cur a, .top-bar-section a.button.ef-search-btn, .ef-portfolio-item:hover .ef-item-inner .ef-item-title a, .ef-style-accordion .ui-accordion-header.ui-accordion-header-active .ui-accordion-header-icon, .ef-style-accordion .ui-accordion-header:hover > .ui-accordion-header-icon, .ef-carousel-pag a:hover, .ef-breadcrumbs small:before, .ef-progress-bar span span, div.pp_default .pp_nav .pp_play, div.pp_default .pp_nav .pp_pause, div.pp_default .pp_close, div.pp_default .pp_expand, div.pp_default a.pp_arrow_previous, div.pp_default a.pp_arrow_next, div.pp_default .pp_contract, .post-slider-direct-nav a:hover, #ef-welcome, .tipsy-inner, .wp-caption-text, .top-bar-menu li:hover:after, .top-bar-menu li.current_page_item:after, .top-bar-menu li.current_page_parent:after, .ef-services article:hover > .ef-icon:after, .tagcloud li:hover > a:before, .ef-image-navigation a:hover, .tp-bullets.simplebullets.round .bullet:hover, .tp-bullets.simplebullets.navbar .bullet:hover, .ef-welcome .tp-bullets.simplebullets.navbar, .ef-welcome .tp-bullets.simplebullets.navbar:before, .ef-welcome .tp-bullets.simplebullets.navbar:after, .ef-welcome .tp-bullets.simplebullets.navbar:before, .wpcf7-submit, .tp-leftarrow.default:hover, .tp-rightarrow.default:hover {

	background-color: <?php echo !empty($main_col) ? $main_col : '#ef8e56' ?>;
	
}

/* not for ie8 but for modern browsers */
.top-bar-section li.active a:not(.button) {

	border-color: <?php echo !empty($main_col) ? $main_col : '#ef8e56' ?>;
	
}

.tipsy-s .tipsy-arrow {
	border-top-color: <?php echo !empty($main_col) ? $main_col : '#ef8e56' ?>;
}

.tipsy-n .tipsy-arrow {
	border-bottom-color: <?php echo !empty($main_col) ? $main_col : '#ef8e56' ?>;
}

.tipsy-w .tipsy-arrow {
	border-right-color: <?php echo !empty($main_col) ? $main_col : '#ef8e56' ?>;
}

.tipsy-e .tipsy-arrow {
	border-left-color: <?php echo !empty($main_col) ? $main_col : '#ef8e56' ?>;
}

.top-bar-section .has-form input, .ef-carousel-pag a.selected, .ef-style-accordion .ui-accordion-header.ui-accordion-header-active .ui-accordion-header-icon, #today, .top-bar-section li:hover a:not(.button), .top-bar-section li.active a:not(.button), .top-bar-section li:hover a:not(.button), .ef-portfolio-item:hover .ef-item-inner .ef-item-title, .fixed .top-bar-section li.active a:not(.button), .fixed .top-bar-section li:hover a:not(.button), .ef-portfolio-tags, .ef-icon, .ef-date-comment .ef-comments, .ef-date-comment:before, button, button:hover, button:focus, .button, .button:hover, .button:focus, button.ef-hollow:hover, .button.ef-hollow:hover, input[type="text"]:focus, input[type="password"]:focus, input[type="date"]:focus, input[type="datetime"]:focus, input[type="datetime-local"]:focus, input[type="month"]:focus, input[type="week"]:focus, input[type="email"]:focus, input[type="number"]:focus, input[type="search"]:focus, input[type="tel"]:focus, input[type="time"]:focus, input[type="url"]:focus, textarea:focus, .ef-sidebar .tagcloud li:hover, #ef-footer .tagcloud li:hover, .tagcloud li:hover, .ef-sidebar .tagcloud li:hover:before, #ef-footer .tagcloud li:hover:before, .tagcloud li:hover:before, .ef-carousel-pag a.selected:hover, .ef-carousel-nav a:hover, .tp-bullets.simplebullets.round .bullet:hover, .tp-bullets.simplebullets.round .bullet.selected, .tp-bullets.simplebullets.navbar .bullet:hover, .tp-bullets.simplebullets.navbar .bullet.selected, .tp-bullets.simplebullets.navbar-old .bullet.selected, .wpcf7-submit, div.wpcf7-response-output, .pagination li.current a, .bypostauthor .post-comm {

	border-color: <?php echo !empty($main_col) ? $main_col : '#ef8e56' ?>;
	
}

/* not for ie8 but for modern browsers */
.top-bar-section li li:hover > a:not(.button), .top-bar-section li li.active > a:not(.button), .expanded .top-bar-section li:hover > a:not(.button), .expanded .top-bar-section li.active > a:not(.button) {

	color: <?php echo !empty($second_col) ? $second_col : '#d37919' ?>;
	
}

p a, a:hover, a:focus, .ef-text-color, h1 a, h2 a, h3 a, h4 a, h5 a, .ef-carousel-nav a:hover, .ef-faq.ef-style-accordion .ui-accordion-header:before, .ef-points .ef-map-current, li.ef-currentclass  #today, .ef-tabs ul.ef-tabs-nav li.ui-tabs-active.ui-state-active a, .ef-main-color, a, .side-nav li a, #ef-footer #ef-footer-bottom a:hover, #ef-footer a:hover, .ef-sidebar a:hover, .widget_calendar tbody a, .breadcrumbs > *, #to-top:hover, .breadcrumbs a:hover, .breadcrumbs li.current, button:hover, button:focus, .button:hover, .button:focus, button.ef-hollow:hover, .button.ef-hollow:hover, .ef-icon, .ef-portfolio-filter li a:before, .ef-portfolio-filter li a:after, .wpcf7-submit:hover, .ef-breadcrumbs .ef-icon, .tp-leftarrow.default, .tp-rightarrow.default, .bypostauthor .post-comm:before, article.ef-extr-2 .ef-icon:before, article.ef-extr-2 div.ef-icon:before, .widget_categories li.current-cat a, #ef-footer .widget_categories li.current-cat a, #ef-welcome kbd {

	color: <?php echo !empty($second_col) ? $second_col : '#d37919' ?>;

}

a, p a:hover, .lang-sw a, .lang-sw a, #ef-top ul.lang-sw li a, h1 a, h2 a, h3 a, h4 a, h5 a, .ef-carousel-nav a, #ui-lightbox-title, .ef-map-pag .ef-place, .ef-small-cap-bg p, .top-bar .name h1 a, .panel.callout .ef-date-comment *, .button.ef-hollow, .ef-team .ef-carousel-item:hover .ef-item-title, .breadcrumbs > * a, .ef-latest-thumb:hover > .ef-date-comment .ef-date, .ef-portfolio-filter li a:hover, .ef-portfolio-filter li.ef-currentclass a, form.custom .custom.dropdown .current {
	color: #303030;
}

body.ef-boxed-ver {

<?php if ( !empty($bg_image) && !stristr($bg_image, 'none.png') ) : ?>
background-image: url('<?php echo $bg_image ?>');
background-repeat: repeat;
<?php endif; ?>
background-color: <?php echo !empty($bg_color) ? $bg_color : '#f5f5f5' ?>;
background-attachment: <?php echo !empty( $bg_pos ) ? $bg_pos : 'scroll' ?>;

}

body, #content {

color: <?php echo !empty( $body_color ) ? $body_color : '#707070' ?>;

}

body, h1, h2, h3, h4, h5, h6, small, #yoxview, kbd {

<?php if ( !empty($heading_face) ) { ?>
font-family: <?php echo $heading_face ?>;
<?php } ?>

}

body {

<?php if ( !empty( $body_face ) ) : ?>
font-family: <?php echo $body_face ?>;
<?php endif; ?>

}