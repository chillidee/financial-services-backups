<?php

/**
 * Generate alerts if have no posts (outputs while user is logged in)
 *
 * @since Favea 1.0
 */
function ef_get_msg_data( $get_posttype = '', $ef_data = '' ) {
	if ( is_user_logged_in() ) {

		if ( current_user_can( 'edit_posts' ) ) {
			if ( $ef_data['cpt_support'] ) {
				$tmp_cpt_cat = get_categories( array( 'taxonomy' => EF_CPT_TAX_CAT, 'hide_empty' => 1 ) );
				$tmp_extr_cat = get_categories( array( 'taxonomy' => EF_CPT_EXTR_TAX, 'hide_empty' => 1 ) );
			}

			if ( !$ef_data['cpt_support'] && $get_posttype != 'post' ) {
				$output = __( 'You need to install and activate "Fireform CPT" plugin to display these posts', EF_TDM );
			} elseif ( ( !empty( $ef_data['terms'] ) && ( ( $ef_data['cpt_support'] && $get_posttype == EF_CPT && !empty( $tmp_cpt_cat ) ) || $get_posttype == 'post' ) ) || ( !empty( $ef_data['terms_extras'] ) && !empty( $tmp_extr_cat ) && $ef_data['cpt_support'] && $get_posttype == EF_CPT_EXTR ) ) {
				$output = "";//__( "It seems that all categories are excluded so it's nothing to display.", EF_TDM );
			} else {
				$link = sprintf( __( ' Ready to publish your post? <a href="%s"><abbr title="Add new">Get started here</abbr></a>', EF_TDM ), admin_url( 'post-new.php' ) . '?post_type=' . $get_posttype . '' );
				$get_posttype = $get_posttype == 'post' ? $get_posttype.'s' : $get_posttype;
				$output = sprintf( __( 'Add at least %1s to "%2s".%3s', EF_TDM ), is_page_template( 'templates/home-template.php' ) && ! ( $ef_data['cpt_support'] && $get_posttype == EF_CPT_EXTR ) ? __( 'two posts', EF_TDM ) : __( 'one post', EF_TDM ), ucfirst( $get_posttype ), $link );
			}
		} else {
			$output = __( 'The current user can\'t edit posts. Log in as an appropriate user that have such ability.', EF_TDM );
		}

		if ( !empty( $output ) ) {
			printf( '<div class="row"><div class="large-12 columns"><div class="alert-box radius">%s</div></div></div>', $output );
		}
	}
}