<?php

/*
 * Add custom body classes
 *
 * @since	Favea 1.0
 */
add_filter( 'body_class', 'ef_theme_body_classes_68163' );
function ef_theme_body_classes_68163( $classes ) {

	$ef_data = EfGetOptions::TplConditions();
	$ef_id = get_current_blog_id();

	$classes[] = 'site-id-'.$ef_id;
	$classes[] = !empty( $ef_data['layouts'] ) && $ef_data['layouts'] == 12 ? 'ef-has-no-sidebar' : 'ef-has-sidebar';
	if ( !( !is_page_template( 'templates/home-template.php' ) && !is_front_page() ) xor ( is_home() && !is_front_page() ) ) {
		$classes[] = 'ef-no-breadcrumbs';
	}
	$classes[] = !is_search() && $ef_data['mb_support'] && !empty( $ef_data['slider'] ) && function_exists( 'putRevSlider' ) ? 'ef-has-slider' : 'ef-has-no-slider';
	$classes[] = $ef_data['mb_support'] && !empty( $ef_data['welcome'] ) ? 'ef-welcome' : '';
	$classes[] = !empty( $ef_data['ef_layout_type'] ) ? $ef_data['ef_layout_type'] : 'ef-full-width';
	$classes[] = isset( $ef_data['feed_layout'] ) ? $ef_data['feed_layout'] : '';

	return $classes;
}


/*
 * Add shortcodes support
 *
 * @since	Favea 1.0
 */
add_filter( 'widget_text', 'do_shortcode' );


/*
 * Customizing excerpt
 *
 * @since	Favea 1.0
 */
remove_filter( 'get_the_excerpt', 'wp_trim_excerpt' );
add_filter( 'get_the_excerpt', 'ef_theme_wp_trim_excerpt' );
function ef_theme_wp_trim_excerpt( $text, $excerpt = '' ) {

	global $post;

	/* Excerpt lenght */
	$cpt_excerpt = 6; /* Portfolios */
	$cpt_excerpt_extras = 15; /* Extras */
	$excerpt = 20; /* Other post types */

	$raw_excerpt = $text;
	if ( '' == $text ) {

		$text = get_the_content( '' );
		$text = strip_shortcodes( $text );

		$text = apply_filters(  'the_content', $text );
		$text = str_replace( ']]>', ']]&gt;', $text );
		$text = strip_tags( $text, '<br>,<p>,<a>,<strong>' );

		if ( current_theme_supports( 'ef-custom-post-types' ) && $post->post_type == EF_CPT ) {
			$excerpt_word_count = $cpt_excerpt;
		} elseif ( current_theme_supports( 'ef-custom-post-types' ) && $post->post_type == EF_CPT_EXTR ) {
			$excerpt_word_count = $cpt_excerpt_extras;
		} else {
			$excerpt_word_count = $excerpt;
		}

		$excerpt_length = apply_filters( 'excerpt_length', $excerpt_word_count );
		$excerpt_end = '<a class="ef-read-more" href="'. get_permalink( $post->ID ) . '">' . ' ' . __( 'Read More &rarr;', EF_TDM ) . '</a>';
		$excerpt_more = apply_filters( 'excerpt_more', ' &hellip; ' . $excerpt_end );

		$words = preg_split( "/[\n\r\t ]+/", $text, $excerpt_length + 1, PREG_SPLIT_NO_EMPTY );
		if ( count( $words ) > $excerpt_length ) {
			array_pop( $words );
			$text = implode( ' ', $words );
			$text = !( current_theme_supports( 'ef-custom-post-types' ) && ( $post->post_type == EF_CPT || $post->post_type == EF_CPT_EXTR ) ) ? $text . $excerpt_more : $text . ' &hellip;';
		} else {
			$text = implode( ' ', $words );
		}
	}
	return apply_filters( 'wp_trim_excerpt', $text, $raw_excerpt );
}

/**
 * Output custom document titles
 *
 * @since Favea 1.0
 */
add_filter( 'wp_title', 'ef_theme_filter_wp_title_68163' );
function ef_theme_filter_wp_title_68163( $title = '', $sep = '', $seplocation = '' ) {

	global $paged, $page, $post;
	$ef_data = EfGetOptions::TplConditions();

	if ( is_feed() ) {
		return $title;
	}

	if ( is_search() ) {
		$title = sprintf( __( 'Search results for %s', EF_TDM ), '"' . get_search_query() . '"' );
		$title .= " $sep " . get_bloginfo( 'name', 'display' );
		if ( $paged >= 2 ) {
			$title .= " $sep " . sprintf( __( 'Page %s', EF_TDM ), $paged );
		}
		return $title;
	}

	$title .= get_bloginfo( 'name', 'display' );

	if ( is_home() || is_front_page() ) {
		if ( !empty( $ef_data['tagline'] ) ) {
			$title .= " $sep " . get_bloginfo( 'description', 'display' );
		} else {
			if ( !is_sticky() && !is_front_page() ) {
				$title .= " $sep " . $post->post_title;
			}
		};
	}

	if ( $paged >= 2 || $page >= 2 ) {
		$sep = !is_sticky() && !is_front_page() ? $sep : '';
		$title .= " $sep " . sprintf( __( 'Page %s', EF_TDM ), max( $paged, $page ) );
	}

	return $title;
}


/**
 * Comment form hooks
 *
 * @since Favea 1.0
 */
add_filter( 'comment_form_default_fields', 'ef_comment_form_default_fields_68163' );
function ef_comment_form_default_fields_68163( $fields ) {

	$commenter  = wp_get_current_commenter();
	$req 		= get_option( 'require_name_email' );
	$aria_req 	= ( $req ? " aria-required='true'" : '' );
	if ( isset( $fields['url'] ) ) {
		unset( $fields['url'] );
	}

	$fields['author'] = '<div class="large-6 small-12 columns"><input class="ef-name ef-radius" name="author" type="text" placeholder="' . __( 'Name *', EF_TDM ) . '" size="30" tabindex="1"' . $aria_req . ' value="'.esc_attr( $commenter['comment_author'] ).'" /></div>';

	$fields['email'] = '<div class="large-6 small-12 columns"><input class="ef-email ef-radius" name="email" type="text" placeholder="' . __( 'E-mail *', EF_TDM ) . '" size="30" tabindex="2"' . $aria_req . ' value="'.esc_attr(  $commenter['comment_author_email'] ).'" /></div>';

	return $fields;
}

add_filter( 'comment_form_field_comment', 'ef_comment_form_field_comment_68163' );
function ef_comment_form_field_comment_68163( $comment_field ) {

	$req = get_option( 'require_name_email' );
	$aria_req = ( $req ? " aria-required='true'" : '' );

	$comment_field = '<div class="ef-textarea row"><div class="large-12 columns"><textarea class="ef-message ef-radius" name="comment" type="text" placeholder="' . __( 'Message *', EF_TDM ) . '"  tabindex="3" ' . $aria_req . '></textarea></div></div>';

	return $comment_field;
}

add_action( 'comment_form_before_fields', 'ef_add_fields_before_68163' );
function ef_add_fields_before_68163() {
	echo '<div class="row">';
}

add_action( 'comment_form_after_fields', 'ef_add_fields_after_68163' );
function ef_add_fields_after_68163() {
	echo '</div>';
}


/**
 * Searchbox form
 *
 * @since Favea 1.0
 */
add_filter( 'get_search_form', 'ef_custom_search_form_68163' );
function ef_custom_search_form_68163( $form ) {
	$form = '<form role="search" method="get" id="searchform" class="ef-searchform" action="' . esc_url( home_url( '/' ) ) . '" >
    <div class="row collapse">
	    <div class="large-9 small-9 columns">
	    	<input type="text" class="prefix round" value="' . get_search_query() . '" name="s" id="s" placeholder="'. esc_attr__( 'Search...', EF_TDM ) .'" />
	    </div>
	    <div class="large-3 small-3 columns">
	    	<input type="submit" class="button postfix round" id="searchsubmit" value="'. esc_attr__( 'Go', EF_TDM ) .'" />
	    </div>
    </div>
    </form>';

	return $form;
}


/**
 * Password form
 *
 * @since Favea 1.0
 */
add_filter( 'the_password_form', 'ef_custom_password_form_68163' );
function ef_custom_password_form_68163( $output ) {
	global $post;
	$label = 'pwbox-'.( empty( $post->ID ) ? rand() : $post->ID );
	$output = '<form class="ef-pass-form" action="' . esc_url( site_url( 'wp-login.php?action=postpass', 'login_post' ) ) . '" method="post"> 		<p>' . __( "To view this protected post, enter the password below:", EF_TDM ) . '</p>
    	<div class="row collapse">
    		<div class="large-9 small-9 columns">
		   		<input class="prefix radius" name="post_password" id="' . esc_attr( $label ) . '" type="password" size="20" maxlength="20" />
		   	</div>
		   	<div class="large-3 small-3 columns">
		   		<input class="button postfix radius" type="submit" name="Submit" value="' . esc_attr__( "Submit" ) . '" />
		   	</div>
		</div>
    </form>';
	return $output;
}


/**
 * Replacing the no excerpt text with a password form
 *
 * @since Favea 1.0
 */
add_filter( 'the_excerpt', 'ef_excerpt_post_protected_68163' );
function ef_excerpt_post_protected_68163( $content ) {
	if ( post_password_required() ) {
		$content = get_the_password_form();
	}
	return $content;
}


/**
 * Allow SVG uploads
 *
 * @since Favea 1.0
 */
add_filter( 'upload_mimes', 'ef_custom_upload_mimes_68163' );
function ef_custom_upload_mimes_68163( $existing_mimes = array() ) {
	$existing_mimes['svg'] = 'mime/type';
	return $existing_mimes;
}


/**
 * Customizing RSS feed (adding images)
 *
 * @since Favea 1.0
 */
add_filter( 'the_excerpt_rss', 'ef_custom_rss_view_68163' );
add_filter( 'the_content_feed', 'ef_custom_rss_view_68163' );
function ef_custom_rss_view_68163( $content ) {
	global $post;
	if ( has_post_thumbnail( $post->ID ) ) {
		$content = '<p>' . get_the_post_thumbnail( $post->ID ) . '</p>' . get_the_excerpt();
	}
	return $content;
}


/**
 * Disable comments on WordPress media attachments
 *
 * @since Favea 1.0
 */
add_filter( 'comments_open', 'ef_attachment_comment_status_68163' );
function ef_attachment_comment_status_68163( $open ) {
	global $post;
	$post = get_post( $post->ID );
	if ( $post->post_type == 'attachment' ) {
		return false;
	}
	return $open;
}

/**
 * Add default title if title isn't specified
 *
 * @since Favea 1.0
 */
add_filter( 'the_title', 'ef_intitled_post_68163' );
function ef_intitled_post_68163( $title ) {
	if ( $title == '' ) {
		return apply_filters( 'ef_default_post_title', __( 'Untitled', EF_TDM ) );
	} else {
		return $title;
	}
}


/**
 * Custom reply link
 *
 * @since Favea 1.0
 */
add_filter( 'comment_reply_link', 'ef_custom_reply_link_68163' );
function ef_custom_reply_link_68163( $link ) {
	if ( !empty( $comment->comment_type ) ) {
		return '';
	}
	$link = str_replace( "comment-reply-link", "comment-reply-link button small radius", $link );

	return $link;
}

/**
 * get_comment_author_link
 *
 * @since Favea 1.0
 */
add_filter( "get_comment_author_link", "ef_modifiy_comment_author_anchor_68163" );
function ef_modifiy_comment_author_anchor_68163( $author_link ){
    return str_replace( "<a", "<a target='_blank'", $author_link );
}


/**
 * Enqueue comment-reply script
 *
 * @since Favea 1.0
 */
add_action( 'comment_form_before', 'ef_theme_enqueue_comment_reply_68163' );
function ef_theme_enqueue_comment_reply_68163() {
	if ( is_singular() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}


/**
 * Fixing empty search
 *
 * @since Favea 1.0
 */
function ef_fix_blank_search( $query ) {
	global $wp_query;
	if ( isset( $_GET['s'] ) && ( $_GET['s'] == '' ) ) {
		$wp_query->set( 's', ' ' );
		$wp_query->is_search = true;
	}
	return $query;
}

add_action( 'pre_get_posts', 'ef_fix_blank_search' );


/**
 * Widget Tagcloud
 *
 * @since Favea 1.0
 */
function ef_custom_tag_cloud_widget( $args ) {
	$args['format'] = 'list';
	return $args;
}
add_filter( 'widget_tag_cloud_args', 'ef_custom_tag_cloud_widget' );


/**
 * Contact form 7 dropdowns
 *
 * @since Favea 1.0
 */
if ( has_filter( 'wpcf7_form_class_attr' ) ) {
	add_filter( 'wpcf7_form_class_attr', 'ef_wpcf7_form_class_attr' );
	function ef_wpcf7_form_class_attr( $content ) { 
		$rl_formfind = '/wpcf7-form/';
		$rl_formreplace = 'wpcf7-form custom';
		$content = preg_replace( $rl_formfind, $rl_formreplace, $content );
		return $content;
	}
}


/**
 * SSL Links
 *
 * @since Favea 1.0
 */
function filter_content_match_protocols( $content ) {
	$search = $replace = home_url();
	
	if ( ! preg_match( '|/$|', $search ) )
		$search = $replace = "$search/";
	
	if ( is_ssl() ) {
		$search = str_replace( 'https://', 'http://', $search );
		$replace = str_replace( 'http://', 'https://', $replace );
	}
	else {
		$search = str_replace( 'http://', 'https://', $search );
		$replace = str_replace( 'https://', 'http://', $replace );
	}
	
	$content = str_replace( $search, $replace, $content );
	
	return $content;
}
ob_start( 'filter_content_match_protocols' );

function filter_content_match_protocols_end() {
	ob_end_flush();
}
add_action( 'shutdown', 'filter_content_match_protocols_end', -10 );