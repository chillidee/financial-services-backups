<?php

// Main functions
require_once ( 'functions/functions.php' );

// TGM Plugin Activation class
require_once ( 'classes/class-tgm-plugin-activation.php' );

// Load options class
require_once( 'classes/class.conditions.php' );

// WP filters
require_once ( 'functions/wp-hooks.php' );

// Helpers
require_once ( 'functions/messages.php' );

// Stylesheets + scripts
require_once ( 'main/enqueue.php' );

// Sidebars
require_once ( 'main/sidebars.php' );

// Menus
require_once ( 'main/menus.php' );

// Demo
if ( file_exists( get_template_directory() . '/demo/changer.php' ) ) {
	require_once get_template_directory() . '/demo/changer.php';
}
if ( !defined('EF_DEMO_CHANGER' ) ) {
	define( 'EF_DEMO_CHANGER', FALSE );
}