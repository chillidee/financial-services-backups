<?php

/**
 * Register custom sidebars.
 *
 * @since Favea 1.0
 * @uses register_sidebar
 */
function ef_register_default_sidebars() {

	/**
	 * Register custom sidebars.
	 */
	
	global $efto_data;
	
	if ( isset( $efto_data ) && !empty( $efto_data['ef_sidebar_manager'] ) ) {
	
		$options = $efto_data['ef_sidebar_manager'];
		
		if ( !empty( $options ) ) {
		
		    foreach( $options as $sideb ) {
		    	$name = $sideb['title'];
		    	$order = $sideb['order'];
		    	$desc = $sideb['link'];
		    	$uniqueId = sanitize_title( $name );
		    	
		        register_sidebar( array(  
		            'name'			=> !empty($name) ? $name : "Sidebar $order",
		            'description'	=> !empty($desc) ? $desc : '',
		            'id'			=> !empty($name) ? $uniqueId : "sidebar".-$order,  
		            'before_widget'	=> '<section id="%1$s" class="large-3 columns widget %2$s">',  
		            'after_widget'	=> "</section>",  
		            'before_title'	=> '<div>',  
		            'after_title'	=> '</div>',  
		        ) );
		    }
		}
	}
	
	
	/**
	 * Register default sidebar and footer widget areas.
	 */
	register_sidebar( array(
		'name'			=> __( 'Default sidebar widget area', EF_TDM ),
		'id'			=> 'primary-widget-area',
		'description'	=> __( 'The primary widget area', EF_TDM ),
		'before_widget'	=> '<section id="%1$s" class="widget %2$s">',
		'after_widget'	=> "</section>",
		'before_title'	=> '<span>',
		'after_title'	=> '</span>',
	) );

	register_sidebar( array(
		'name'			=> __( 'Default footer widget area', EF_TDM ),
		'id'			=> 'footer-widget-area',
		'description'	=> __( 'The secondary widget area', EF_TDM ),
		'before_widget' => '<section id="%1$s" class="large-3 columns widget %2$s">',
		'after_widget'	=> "</section>",
		'before_title'	=> '<span>',
		'after_title'	=> '</span>',
	) );
	
}

add_action( 'widgets_init', 'ef_register_default_sidebars' );