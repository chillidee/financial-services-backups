<?php
/* 
* rt-theme home page 
*/
get_header(); 
?>

	<!-- slider -->
	<?php if(is_home()) get_template_part( 'slider', 'home_slider' );  ?>
	<!-- / slider -->

	<div class="diognal-pattern-dummy"></div>
	
	<!-- home page contents -->
	<div class="border"><div class="content_background">
	<div class="homepage-bigtext">
		<?php
			if(function_exists('icl_register_string')){
			    $homepage_bigtext = icl_t('rt_theme', 'Homepage Big Text', get_option('rttheme_homepage_bigtext'));
			}else{
			    $homepage_bigtext = get_option('rttheme_homepage_bigtext');
			}
			
			if($homepage_bigtext)   echo "<h1>" . do_shortcode($homepage_bigtext) . "</h1>";
			?>
	</div>
	
	
	<?php if (function_exists (FadeIn)) { ?>
	<div class="homepage-news">
		<?php FadeIn('Media'); ?>
	</div>
	<?php }?>
	
	
	<div class="content home_page"> 
		<?php
		//home pae content
		$home_page=array(
		    'post_type'=> 'home_page',
		    'post_status'=> 'publish',
		    'ignore_sticky_posts'=>1,
		    'showposts' => 1000,
		    'orderby'=> 'date',
		    'order' => 'ASC',
		    'cat' => -0,
		);
	 
	    get_template_part( 'home_content_loop', 'home_page' ); 
	    ?> 
		
		<div class="clear"></div>
		
		<div class="homepage-extra">
			<?php 
				$page = get_posts(
				    array(
				        'name'      => 'home',
				        'post_type' => 'page'
				    )
				);	
				if ( $page )
				{
				    echo $page[0]->post_content;
				}
			?>	
		</div>
   
	    <div class="clear"></div>

		<!-- widgetized home page area -->
		<?php if (function_exists('dynamic_sidebar')){  dynamic_sidebar('home-page-contents'); } ?>
		<div class="clear"></div>
		<!-- / widgetized home page area -->
		
	</div>
	</div></div>
	<!-- / home papge contents  -->    
    
<?php get_footer();?>