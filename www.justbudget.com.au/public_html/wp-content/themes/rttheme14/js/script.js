/*
    File Name: script.js
    by Tolga Can
    RT-Theme 14
*/
var rttheme_template_dir = jQuery("meta[name=rttheme_template_dir]").attr('content');
var rttheme_slider_timeout=jQuery("meta[name=rttheme_slider_timeout]").attr('content')*1000;
var rttheme_slider_effect=jQuery("meta[name=rttheme_slider_effect]").attr('content');
var rttheme_slider_buttons=jQuery("meta[name=rttheme_slider_buttons]").attr('content');
 
//home page slider  
jQuery(document).ready(function(){
   
	//Slide Backgrounds
	jQuery('.slider_backgrounds li').each(function(){
		var background_file = jQuery(this).find('img').attr("src");//background image
		var link =  jQuery(this).find('a').attr("href");//slide link
		
		image_class = jQuery(this).attr("class");
		var slide_image = '#fff url('+ background_file + ') no-repeat';	
		var this_slide = jQuery('.background_image.'+image_class) ;
	
		if(link){
			this_slide.css({'background' : slide_image, 'cursor' : 'pointer'});
			this_slide.click(function(){
			    window.location = link;  
			});
		}else{
			this_slide.css('background',slide_image);
		}
	});
    
	var slider_area;
	var slider_buttons;

	// Which slider
	if (jQuery('#slider_area').length>0){
		
		// Home Page Slider
		slider_area="#slider_area";	
		
		if(rttheme_slider_buttons){
			slider_buttons="#numbers";
		}
	
		jQuery(slider_area).cycle({ 
			fx:     rttheme_slider_effect,  // Effect 
			timeout:  rttheme_slider_timeout,  // Timeout value (ms) = 4 seconds
			easing: 'backout', 
			pager:  slider_buttons, 
			cleartype:  1,
			pause:           true,     // true to enable "pause on hover"
			pauseOnPagerHover: true,   // true to pause when hovering over pager link					
			pagerAnchorBuilder: function(idx) { 
				return '<a href="#" title=""><img src="'+rttheme_template_dir+'/images/pixel.gif" width="14" heigth="14"></a>'; 
			}
		});
		
	}   
		
	
});


//Product Detail Page - Image Slider
jQuery(document).ready(function(){ 

	jQuery('.product_slider').cycle({ 
	       fx:     rttheme_slider_effect,  // Effect 
	       timeout:  4000,  // Timeout value (ms) = 4 seconds
	       easing: 'backout',
	       pager:  '.slider_buttons',
	       cleartype:  1,
	       pause:    true,     // true to enable "pause on hover"
	       pauseOnPagerHover: true,   // true to pause when hovering over pager link					
	       pagerAnchorBuilder: function(idx) { 
	       return '<a href="#" title=""><img src="'+rttheme_template_dir+'/images/pixel.gif" width="14" heigth="14"></a>'; 
	       }	    
	});
});


//Photo Slider
jQuery(document).ready(function(){ 
    if (jQuery('.photo_gallery_cycle ul').length>0){
        jQuery(".photo_gallery_cycle ul").cycle({ 
            fx:     'scrollUp',  //fade
            timeout:  rttheme_slider_timeout,
            pager:  '.slider_buttons', 
            cleartype:  1,
            pause:           true,     // true to enable "pause on hover"
            pauseOnPagerHover: true,   // true to pause when hovering over pager link						
                pagerAnchorBuilder: function(idx) { 
                    return '<a href="#" title=""><img src="'+rttheme_template_dir+'/images/pixel.gif" width="14" heigth="14"></a>'; 
                }
        });
    }
});
 
//pretty photo
jQuery(document).ready(function(){
        jQuery("a[rel^='prettyPhoto']").prettyPhoto({animationSpeed:'slow',theme:'light_rounded',slideshow:false,overlay_gallery: false,social_tools:''});
});


//buttons hover effect
jQuery(document).ready(function(){		
	if(!jQuery.browser.msie){     
	jQuery('.small_button,.medium_button,.big_button').hover(function(){
		jQuery(this).stop().animate({opacity:'0.7'},{duration:500}  );
			  
	},function(){
		jQuery(this).stop().css('opacity','1');
	});
	}
});


//Categories
jQuery(document).ready(function(){		
	jQuery('.portfolio_categories').click(function(){
		jQuery('.portfolio_categories').hide(); 
		jQuery('.portfolio_categories_open').stop().css({display:'block'});  
	});
	
	jQuery('.portfolio_categories_open').click(function(){
		jQuery('.portfolio_categories_open').hide(); 
		jQuery('.portfolio_categories').stop().css({display:'block'});         
	}); 
}); 

//drop down menu
jQuery(document).ready(function() {
	jQuery("#navigation li").each(function()
	{
		jQuery(this).hover(function()
		{
			jQuery(this).find('ul:first').stop().css({
			      paddingTop:"8px",                              
			      height:"auto",
			      overflow:"hidden",
			      display:"none"
			      }).slideDown(200, function()
			{
			jQuery(this).css({
			      height:"auto",
			      overflow:"visible"
			});
		});
		       
		},
		    
		function()
		{	
			jQuery(this).find('ul:first').stop().slideUp(200, function()
			{	
			       jQuery(this).css({
			       display:"none",
			       overflow:"hidden"
			       });
			});
		});	
	});
        
        jQuery("#navigation ul ").css({
            display: "none"}
        ); 
});
 

// TABS - perform JavaScript after the document is scriptable.
jQuery(function() {
	jQuery("ul.tabs").tabs("> .pane", {effect: 'fade'});
	
	jQuery(".accordion").tabs(".pane", {tabs: 'h3', effect: 'slide'});
	jQuery(".scrollable").scrollable();
	
	
	jQuery(".items.big_image img").click(function() {
	
		// see if same thumb is being clicked
		if (jQuery(this).hasClass("active")) { return; }
		
		// calclulate large image's URL based on the thumbnail URL (flickr specific)
		var url = jQuery(this).attr("src").replace("_t", "");
		
		// get handle to element that wraps the image and make it semi-transparent
		var wrap = jQuery("#image_wrap").fadeTo("medium", 0.5);
		
		// the large image from www.flickr.com
		var img = new Image();
		
		
		// call this function after it's loaded
		img.onload = function() {
		
		// make wrapper fully visible
		wrap.fadeTo("fast", 1);
		
		// change the image
		wrap.find("img").attr("src", url);
		
		};
		
		// begin loading the image from www.flickr.com
		img.src = url;
		
		// activate item
		jQuery(".items img").removeClass("active");
		jQuery(this).addClass("active");
	
	// when page loads simulate a "click" on the first image
	}).filter(":first").click();

}); 


//search field function
jQuery(document).ready(function() {
var search_text=jQuery(".search_bar .search_text").val();

	jQuery(".search_bar .search_text").focus(function() {
		jQuery(".search_bar .search_text").val('');
	});
});



//RT Portfolio Effect
jQuery(document).ready(function() {


	jQuery(window).load(function() {
		var portfolio_item=jQuery("a.imgeffect");
		
		portfolio_item.each(function(){
		
		var img_width = jQuery(this).find('img').width();  
		var img_height = jQuery(this).find('img').innerHeight();
		var imageClass = jQuery(this).attr("class");
		jQuery(this).prepend('<span class="imagemask '+imageClass+'"></span>');
		
		if(jQuery.browser.msie){
			var p = jQuery(this).find('img');
			var position = p.position();
			var PosTop= parseInt(p.css("margin-top"))+position.top;
			var PosLeft= parseInt(p.css("margin-left"))+position.left;
			if (!PosLeft) {PosLeft= position.left}; 
				jQuery(this).find('.imagemask').css({top: PosTop+2});
				jQuery(this).find('.imagemask').css({left: PosLeft+2});
			}
		
			jQuery('.imagemask', this).css({width:img_width,height:img_height,backgroundPosition:'right bottom'});
		
			if(jQuery.browser.msie){ jQuery('.imagemask', this).css({display:'none'});}
		});
	});
	
	var image_e= jQuery("a.imgeffect");
	
	if(jQuery.browser.msie){//ignore the shadow effect if browser IE
		jQuery(this).find('.imagemask').css({display:"none"});
		
		image_e.mouseover(function(){
			jQuery(this).find('.imagemask').stop().css({
			display:"block"
		}); 
		
		}).mouseout(function(){
			jQuery(this).find('.imagemask').stop().css({
			display:"none"
			} );
		});
		
	}else{//real browsers :)
		image_e.mouseover(function(){
			jQuery(this).find('.imagemask').stop().animate({
			display:"block", 
			opacity:1
		}, 0); 
		
		}).mouseout(function(){
			jQuery(this).find('.imagemask').stop().animate({
			display:"none",
			opacity:0
			}, 0 );
		});                  
	}

});



 
 
//validate contact form
jQuery(document).ready(function(){

	// show a simple loading indicator
	var loader = jQuery('<img src="'+rttheme_template_dir+'/images/loading.gif" alt="..." />')
		.appendTo(".loading")
		.hide(); 
      
    jQuery.validator.messages.required = "";

	var options = { 
		target:     '#result', 
		success:    function() { 
		   // use javascript to redirect to Enquiry is Complete page, so not hide loading icon, not reset form here.
		   //loader.hide();
		   //v.resetForm();
		   document.location.href = '/?page_id=171';
		} 
	};
	
	var v = jQuery("#validate_form").validate({
		rules: {
			homenumber: {
		    	required: false
		    },
		    worknumber: {
		    	required: false
		    },
		    mobilenumber: {
		    	required: false
		    },
		    email: {
		    	required: false
		    },
		    homenumber_areacode: {
		    	required: function(){
		    		return jQuery("#homenumber").val() != "";
		    	}
		    },
		    worknumber_areacode: {
		    	required: function(){
		    		return jQuery("#worknumber").val() != ""
		    	}
		    }
		},
		submitHandler: function(form) {
			//check if at least 2 contacts submitted
			var required_count = 0;
			if(jQuery("#homenumber").val() != "") required_count++;
			if(jQuery("#worknumber").val() != "") required_count++;
			if(jQuery("#mobilenumber").val() != "") required_count++;
			if(jQuery("#email").val() != "") required_count++;
			if(required_count < 2) {
				jQuery("#contact_restriction").css('color', '#FAA890'); 
				alert("Please enter at least 2 forms of contact below (e.g. mobile + email)");
				return false;
			}
			else {
				jQuery("#contact_restriction").css('color', '#024A76');
			}
			
			loader.show();
			jQuery(form).ajaxSubmit(options);
		}
	});
      
	/*
	jQuery("#homenumber").blur(function() {
		jQuery("#worknumber").valid();
		jQuery("#mobilenumber").valid();
		jQuery("#email").valid();
		jQuery("#homenumber_areacode").valid();
	});
	jQuery("#worknumber").blur(function() {
		jQuery("#homenumber").valid();
		jQuery("#mobilenumber").valid();
		jQuery("#email").valid();
		jQuery("#worknumber_areacode").valid();
	});
	jQuery("#mobilenumber").blur(function() {
		jQuery("#homenumber").valid();
		jQuery("#worknumber").valid();
		jQuery("#email").valid();
	});
	jQuery("#email").blur(function() {
		jQuery("#homenumber").valid();
		jQuery("#mobilenumber").valid();
		jQuery("#worknumber").valid();
	});
	*/
	
	$("#worknumber,#mobilenumber,#homenumber,#postcode").keydown(function(event) {
		// Allow only backspace and delete
		if ( event.keyCode == 46 || event.keyCode == 8 ) {
			// let it happen, don't do anything
		}
		else {
			// Ensure that it is a number and stop the keypress
			if (event.keyCode < 48 || (event.keyCode > 57 && event.keyCode < 96) || event.keyCode > 105) {
				event.preventDefault();	
			}		
		}
	});
	
	jQuery("#reset").click(function() {
		v.resetForm();
	});
	
	//add credit issue box button
	jQuery("#addCreditIssue").click(function() { 
		if(!jQuery("#creditissue2_li").is(":visible")){
			jQuery("#creditissue2_li").show();
		}else if(!jQuery("#creditissue3_li").is(":visible")){
			jQuery("#creditissue3_li").show();
		}else{
			jQuery("#creditissue4_li").show();
			jQuery("#addCreditIssue").hide();
		}
		return false;
    });
	
	//show/hide property info
	jQuery("tr.youown").click(function() { 
		if(jQuery('#youown1').is(':checked')) { jQuery("tr.propertyvalue").fadeIn(); jQuery("tr.balanceowing").fadeIn(); }
		if(jQuery('#youown2').is(':checked')) { jQuery("tr.propertyvalue").fadeOut(); jQuery("tr.balanceowing").fadeOut(); }
		return true;
    });
	
	
	//free report form
	var options_free = { 
			target:     '#result', 
			success:    function() { 
			   loader.hide();
			   x.resetForm();
			   jQuery("#free_report_form").hide();
			} 
		};
		
	var x = jQuery("#free_report_form").validate({
		submitHandler: function(form) {
			loader.show();
			jQuery(form).ajaxSubmit(options_free);
		}
	});
	
});


//Slide to top
jQuery(document).ready(function(){
    jQuery(".line span.top").click(function() {
        jQuery('html, body').animate( { scrollTop: 0 }, 'slow' );
    });
}); 


//Social Media Icons
if(!jQuery.browser.msie){
jQuery(function() {
	jQuery('.social_media_icons img').each(function() {
		jQuery(this).hover(
			function() {
				jQuery(this).stop().animate({ opacity: 1.0 }, 400);
			},
			function() {
				jQuery(this).stop().animate({ opacity: 0.8 }, 400);
			})
		});
	});
}

//rounded images
if(!jQuery.browser.msie){
	jQuery(document).ready(function(){ 
		jQuery(".frame img").imgr({radius:"6px 6px 6px 6px"});  
		jQuery(".page_banner .frame img").imgr({radius:"0px 0px 0px 0px"});  
	});
}

//scroll 
jQuery(document).ready(function(){
if (jQuery('div.taps_wrap').length>0){	
	var settings = { 
		autoReinitialise: true,
		maintainPosition:false
	};
	var pane = jQuery('div.taps_wrap .pane');
	pane.jScrollPane(settings);
}	
});  
