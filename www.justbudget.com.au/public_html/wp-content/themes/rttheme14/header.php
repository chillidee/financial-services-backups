<?php // do cookie for tplayers
	/*
	if(!isset($_COOKIE["tplayer_home"]) && (is_home() || is_front_page())){
		$show_tplayer_home = true; 
		$expire=time()+60*60*24*3;
		setcookie("tplayer_home", "true", $expire);
	}else{
		$show_tplayer_home = false;
	} 
	
	if(!isset($_COOKIE["tplayer_affiliate"]) && $post->ID  == wpml_page_id(AFFILIATEPROGRAMPAGE)){
		$show_tplayer_affiliate = true; 
		$expire=time()+60*60*24*3;
		setcookie("tplayer_affiliate", "true", $expire);
	}else{
		$show_tplayer_affiliate = false;
	} */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">  
<head>
<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
<meta http-equiv="X-UA-Compatible" content="IE=Edge"/>
<meta name="rttheme_template_dir" content="<?php echo THEMEURI; ?>" />
<meta name="rttheme_slider_timeout" content="<?php echo get_option('rttheme_slider_timeout');?>" />
<meta name="rttheme_slider_effect" content="<?php echo get_option('rttheme_slider_effect');?>" />
<meta name="rttheme_slider_buttons" content="<?php echo get_option('rttheme_slider_buttons');?>" />

<?php if (get_option( 'rttheme_favicon_url')):?><link rel="icon" type="image/png" href="<?php echo get_option( 'rttheme_favicon_url');?>"><?php endif;?>

<title><?php if (is_home() || is_front_page() ): bloginfo('name'); else: wp_title('');endif; ?></title>
<link rel="alternate" type="application/rss+xml" title="<?php bloginfo('name'); ?> RSS Feed" href="<?php bloginfo('rss2_url'); ?>" />
<link rel="alternate" type="application/atom+xml" title="<?php bloginfo('name'); ?> Atom Feed" href="<?php bloginfo('atom_url'); ?>" />
<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" /> 
<?php if ( is_singular() && get_option( 'thread_comments' ) ) wp_enqueue_script( 'comment-reply' ); //thread comments?>		

<?php wp_head(); ?>  

<?php

#
#   Top bar OFF
#

if(!get_option(THEMESLUG.'_top_bar')){
	echo '<style type="text/css">#wrapper{ background-position:center 0px!important; }</style>';
}

	
#
#   Custom Heading Colors
#
$rttheme_heading_color=get_option('rttheme_heading_color');
if($rttheme_heading_color){
	echo '<style type="text/css">h1,h2,h3,h4,h5,h6, h1 a, h2 a, h3, h4,h5,h6, h3 a, h4 a, h5 a, h6 a, h1 a:hover,h2 a:hover,h3 a:hover,h4 a:hover,h5 a:hover,h6 a:hover,.slide .desc b.title,.slide .desc b.title a{color:'.$rttheme_heading_color.' !important;}</style>';
}

#
#   Custom Body Font Color
#
$rttheme_body_font_color=get_option('rttheme_body_font_color');
if($rttheme_body_font_color){
	echo '<style type="text/css">	body {color:'.$rttheme_body_font_color.';}</style>';
}

#
#   Custom Body Font Size
#
$rttheme_body_font_size=get_option('rttheme_body_font_size');
if($rttheme_body_font_size){
	echo '<style type="text/css">	body {font-size:'.$rttheme_body_font_size.'px;line-height:160%;}</style>';
}

#
#   Custom Body Font Family
#
$rttheme_body_font_family=get_option('rttheme_body_font_family');
if($rttheme_body_font_family){
	echo '<style type="text/css">	body {font-family:'.$rttheme_body_font_family.';}</style>';
}

#
#   Custom Menu Font Size
#
$rttheme_menu_font_size=get_option('rttheme_menu_font_size');
if($rttheme_menu_font_size){
	echo '<style type="text/css">	#navigation li {font-size:'.$rttheme_menu_font_size.'px;}</style>';
}

#
#   Custom Menu Font Color
#
$rttheme_menu_font_color=get_option('rttheme_menu_font_color');
$rttheme_menu_font_color_hover=get_option('rttheme_menu_font_color_hover');
if($rttheme_menu_font_size){
	echo '<style type="text/css">';
	echo '#navigation li a, #navigation li.current_page_item li a {color:'.$rttheme_menu_font_color.';}';
	if($rttheme_menu_font_size){
		echo '#navigation li a:hover, #navigation li.current_page_item a, #navigation li.current_page_item li a:hover {color:'.$rttheme_menu_font_color_hover.';}';	
	}
	echo '</style>';
}

#
#   Custom Slider Height
#
$rttheme_slider_height=get_option('rttheme_slider_height');
$rttheme_slider_desc_height=$rttheme_slider_height-20;
if (THEMESTYLE == '0') {
	$rttheme_slider_height = 300;
	$rttheme_slider_desc_height = 300;
}
if($rttheme_slider_height){
	echo '<style type="text/css">';
	echo '#slider, #slider_area{ height:'.$rttheme_slider_height.'px; } .slide,.background_image,#slider_area .desc{ height:'.$rttheme_slider_desc_height.'px; }';
	echo '</style>';
}

#
#   Custom Heading Sizes
#
for ($i = 1; $i <= 6; $i++) {
	$this_heading=get_option('rttheme_h'.$i.'_font_size');
	if($this_heading){
		echo '<style type="text/css">';
		echo 'h'.$i.'{ font-size:'.$this_heading.'px;line-height:140%; }';
		echo '</style>';
	}
}

#
#   Custom Content Box Font Size
#
$rttheme_box_title_font_size=get_option('rttheme_box_title_font_size');
if($rttheme_menu_font_size){
	echo '<style type="text/css">	.title h4{font-size:'.$rttheme_box_title_font_size.'px;line-height:140%;}</style>';
}


#
#  Call Google Fonts
#
call_user_func(array('RTThemeSite', 'google_fonts'));


#
#  Call Cufon Fonts
#
call_user_func(array('RTThemeSite', 'cufon_fonts'));

#
#  Wordpress Background
#

$background 		= get_background_image();
$background_color 	= get_background_color();
 
if($background_color){
	echo '<style type="text/css">	body {background-image:none;}</style>';
}

#
#  Get Search Keywords from Referer URL and ready to push into MoneyMaker database
#
/*if(!empty($_SERVER['HTTP_REFERER'])){
	//if the referer is website itself, then ignore. otherwise, go ahead.
	if(stristr($_SERVER['HTTP_REFERER'], $_SERVER['SERVER_NAME']) === FALSE){
		$mm_referalurl = $_SERVER['HTTP_REFERER'];
		$mm_keywords = bwp_get_search_keywords();
		
		$parsed_url = parse_url($referrer);
		$mm_referalhost = $parsed_url['host'];
		
		//put these into session for enquiry form to pick up
		$_SESSION['mm_referalurl'] = $mm_referalurl;
		$_SESSION['mm_keywords'] = $mm_keywords;
		$_SESSION['mm_referalhost'] = $mm_referalhost;
	}
}*/

   #
	#  Get Referer URL and ready to push into iFrame form
	#
	if(!empty($_SERVER['HTTP_REFERER'])){
		//if the referer is website itself, then ignore. otherwise, go ahead.
		if (!startsWith($_SERVER['HTTP_REFERER'], 'http://'.$_SERVER['SERVER_NAME']) &&
			!startsWith($_SERVER['HTTP_REFERER'], 'https://'.$_SERVER['SERVER_NAME'])){
			$_SESSION['mm_referalurl'] = $_SERVER['HTTP_REFERER'];
		}
	}
	
	if(isset($_REQUEST['t'])){
		$_SESSION['mm_adwords_t'] = $_REQUEST['t'];
	}
	if(isset($_REQUEST['k'])){
		$_SESSION['mm_adwords_k'] = $_REQUEST['k'];
	}
	if(isset($_REQUEST['a'])){
		$_SESSION['mm_adwords_a'] = $_REQUEST['a'];
	}
?>

<!-- add transparent player support -->
<link href="<?php bloginfo('template_url'); ?>/tplayers/home/css/ADPMask.css" rel="stylesheet" type="text/css" />
<?php if ($show_tplayer_home){ ?>
		<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/tplayers/home/scripts/tplayer.js?uid1=tplayervideo1;"></script>
<?php }else if($show_tplayer_affiliate) { ?>
		<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/tplayers/affiliate/scripts/tplayer.js?uid1=tplayervideo1;"></script>
<?php } //echo "<p>";  print_r($_COOKIE); echo "</p>";?>

</head>
<body <?php body_class(); ?>>

<!-- wrapper -->
<div id="wrapper">
<?php if(get_option(THEMESLUG.'_top_bar')):?>	
	<!-- Top Bar -->	
	<div id="top_bar">
		<div class="top_content">                

				<?php if(get_option(THEMESLUG.'_show_search')):?>
				<!-- search -->
				<div class="search_bar">
					<form action="<?php echo BLOGURL;?>" method="get" class="showtextback">
						<fieldset>
							<input type="text" class="search_text" name="s" id="s" value="<?php _e('search','rt_theme');?>" />
							<input type="image" src="<?php echo THEMEURI; ?>/images/pixel.gif" class="searchsubmit" alt="" />
						</fieldset>
					</form>
				</div>
				<!-- / search-->
				<?php endif;?>
				
				<?php            
				//call the top menu
				$position ="";
				if(!get_option(THEMESLUG.'_show_search')) $position="position_2";
				
				$topmenuVars = array(
					'menu'            => 'RT Theme Top Navigation Menu',
					'depth'		   => 1,
					'menu_id'         => '',
					'menu_class'      => 'top_links '.$position.'', 
					'echo'            => false,
					'container'       => '', 
					'container_class' => '', 
					'container_id'    => '',
					'theme_location'  => 'rt-theme-top-menu' 
				);
				 
				$top_menu = wp_nav_menu($topmenuVars); 
				echo add_class_first_item($top_menu);
				?>  			


				<?php if(get_option(THEMESLUG.'_show_flags')):?>
					<!-- / flags -->	
					    <?php if(function_exists('icl_get_languages')){ languages_list(); } ?>		  
					<!-- / flags -->
				<?php endif;?>
        
		</div>
	</div>
	<!-- / Top Bar -->
	<?php endif;?>
	
<div id="container">	    
    
	<!-- header -->
	<div id="header">
		
		<!-- logo -->
		<div id="logo">
			<?php if(get_option('rttheme_logo_url')):?>
				<a href="<?php echo BLOGURL; ?>" title="<?php echo bloginfo('name');?>"><img src="<?php echo get_option('rttheme_logo_url'); ?>" alt="<?php echo bloginfo('name');?>" class="png" /></a>
			<?php else:?>
				<h1 class="cufon logo"><a href="<?php echo BLOGURL; ?>" title="<?php echo bloginfo('name');?>"><?php echo bloginfo('name');?></a></h1>
			<?php endif;?>
		</div>
		<!-- /logo -->
 
 		<!-- social 
 		<div id="social">
 			<?php
			//social media icons
			global $social_media_icons;
			$social_media_output ='';

			foreach ($social_media_icons as $key => $value){
				if($value != 'cra'){
					//social media
					if(get_option( THEMESLUG.'_'.$key )){
						$social_media_output .= '<li>';
						$social_media_output .= '<a href="'. get_option( THEMESLUG.'_'.$key ) .'" class="j_ttip" title="'.$key.'">';
						$social_media_output .= '<img src="'.THEMEURI.'/images/social_media/icon-'.$value.'.png" alt="" />';
						$social_media_output .= '</a>';
						$social_media_output .= '</li>';
					}
				}
			}
			if($social_media_output) echo '<ul class="social_media_icons">'.$social_media_output.'</ul>';
			?>			     	
 		</div> -->
 		<!-- /social -->

		<!-- header slogan -->
		<div class="top_slogan">
			<?php
			if(function_exists('icl_register_string')){
			    $header_text = icl_t('rt_theme', 'Header Text', get_option('rttheme_header_text'));
			}else{
			    $header_text = get_option('rttheme_header_text');
			}
			
			if($header_text)   echo do_shortcode($header_text);
			?>
		</div>
		<!-- /header slogan -->
	 
		<div class="clear"></div>
	    
	</div>   
	<!-- /header -->
	
	<!--  navigation bar -->
	<div class="border">
 
		<!-- navigation --> 
		
		<?php            
		//call the main menu
		$menuVars = array(
			'menu'            => 'RT Theme Main Navigation Menu',  
			'menu_id'         => 'navigation',
			'menu_class'         => '',
			'echo'            => false,
			'container'       => 'div', 
			'container_class' => '', 
			'container_id'    => 'navigation_bar',
			'theme_location'  => 'rt-theme-main-menu' 
		);
		
		$main_menu=wp_nav_menu($menuVars);
		echo add_class_first_item($main_menu);
		?>
     
	</div>
	<div class="clear"></div>
	<!-- / navigation bar -->
	