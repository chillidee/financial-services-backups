<?php
/* 
* rt-theme slider
*/
?>
    <?php if(!get_option("rttheme_remove_slider")): ?>
    <!-- slider-->
    <div class="border">
	    <div id="slider">
	    
		   <!-- slider wrapper -->
		   <div id="slider_area">
			 
			 <?php
				$slides=array(
				'post_type'=> 'slider',
				'post_status'=> 'publish',
				'ignore_sticky_posts'=>1,
				'showposts' => 1000,
				'cat' => -0,
			 );

			 query_posts($slides); 
			 
			 $background_images="";
			 
			 if ( have_posts() ) : while ( have_posts() ) : the_post(); 
			 
			 $hide_title_and_text = get_post_meta($post->ID, THEMESLUG.'hide_titles', true); 
			 $custom_link = get_post_meta($post->ID, THEMESLUG.'custom_link', true);
			 $custom_link_text = get_post_meta($post->ID, THEMESLUG.'custom_link_text', true);
			 $title = get_the_title();
			 $slide_text = get_post_meta($post->ID, THEMESLUG.'slide_text', true);
			 $thumb = get_post_thumbnail_id(); 
			 $image = vt_resize( $thumb, '', 970, 300, true );			  
			 ?>
			    <!-- slide -->
			    <div class="slide">
				    <div class="background_image <?php echo $post->ID;?>"><!-- set background image -->
				    <?php if($hide_title_and_text):?>
					    <!-- description & title -->
					    <div class="desc">
						    <!-- slide title -->
						    <b class="title">
						    <?php if($custom_link):?><a href="<?php echo $custom_link; ?>" title="<?php echo $title; ?>"><?php endif;?><?php echo $title; ?><?php if($custom_link):?></a><?php endif;?></b>
						    
						    <!-- slide text -->
						    <?php echo $slide_text; ?> 
						    
						    <!-- button-->
						    <?php if($custom_link_text):?><br /><a href="<?php echo $custom_link; ?>" title="<?php $title; ?>" class="medium_button"><span><?php echo $custom_link_text; ?></span></a><?php endif;?>
					    </div>
					    <!-- / description & title  -->
				    <?php endif;?>
				    </div>
			    </div>
			    <!--/ slide  -->
			 <?php
			 //background images
			 if($image){
				$background_images.='<li class="'.$post->ID.'">';
				if($custom_link) $background_images.='<a href="'.$custom_link.'" title="'.$title.'">';
				$background_images.='<img src="'.$image["url"].'" alt="" />';
				if($custom_link) $background_images.='</a>';
				$background_images.='</li>'; 
			 }
			 
			 endwhile;endif;?>  
			  
		   </div>
		   <!-- / slide wrapper -->
		   
		   <!-- slider navigation buttons -->
		   <div id="numbers"></div>
		   
		    <!-- slider background images -->
		    <ul class="slider_backgrounds">
			 <?php echo $background_images;?>
		    </ul>
		    <!-- / slider background images -->
	    
	    </div>
    </div>
    <!-- / slider -->
    <?php endif;wp_reset_query();?>