<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head> 

<title>
<?php
# LAYOUTS
if($_GET['section']=='layouts'){
	echo 'RT-THEME LAYOUTS';
}elseif($_GET['section']=='shortcodes'){
	echo 'RT-THEME SHORTCODES';
}elseif($_GET['section']=='buttons'){
	echo 'RT-THEME BUTTONS';	
}else{
	echo 'RT-THEME QUICK STYLING';
}
?>
</title>

<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>  
<script type="text/javascript" src="../js/tiny_mce_popup.js?ver=3223"></script>
 
<style type="text/css">

	#wphead {
		font-size: 80%;
		border-top: 0;
		color: #555;
		background-color: #f1f1f1;
	}
	#wphead h1 {
		font-size: 24px;
		color: #555;
		margin: 0;
		padding: 10px;
	}
	#tabs {
		padding: 15px 15px 3px;
		background-color: #f1f1f1;
		border-bottom: 1px solid #dfdfdf;
	}
	#tabs li {
		display: inline;
	}
	#tabs a.current {
		background-color: #fff;
		border-color: #dfdfdf;
		border-bottom-color: #fff;
		color: #d54e21;
	}
	#tabs a {
		color: #2583AD;
		padding: 6px;
		border-width: 1px 1px 0;
		border-style: solid solid none;
		border-color: #f1f1f1;
		text-decoration: none;
	}
	#tabs a:hover {
		color: #d54e21;
	}
	.wrap h2 {
		border-bottom-color: #dfdfdf;
		color: #555;
		margin: 5px 0;
		padding: 0;
		font-size: 18px;
	}
	#user_info {
		right: 5%;
		top: 5px;
	}
	h3 {
		font-size: 1.1em;
		margin-top: 10px;
		margin-bottom: 0px;
	}
	#flipper {
		margin: 0;
		padding: 5px 20px 10px;
		background-color: #fff;
		border-left: 1px solid #dfdfdf;
		border-bottom: 1px solid #dfdfdf;
	}
	* html {
        overflow-x: hidden;
        overflow-y: scroll;
    }
	#flipper div p {
		margin-top: 0.4em;
		margin-bottom: 0.8em;
		text-align: justify;
	}
	th {
		text-align: center;
	}
	.top th {
		text-decoration: underline;
	}
	.top .key {
		text-align: center;
		width: 5em;
	}
	.top .action {
		text-align: left;
	}
	.align {
		border-left: 3px double #333;
		border-right: 3px double #333;
	}
	.keys {
		margin-bottom: 15px;
	}
	.keys p {
		display: inline-block;
		margin: 0px;
		padding: 0px;
	}
	.keys .left { text-align: left; }
	.keys .center { text-align: center; }
	.keys .right { text-align: right; }
	td b {
		font-family: "Times New Roman" Times serif;
	}
	#buttoncontainer {
		text-align: center;
		margin-bottom: 20px;
	}
	#buttoncontainer a, #buttoncontainer a:hover {
		border-bottom: 0px;
	}
     
     .rt_button{
        padding:3px;
        -moz-border-radius:6px;
        -webkit-border-radius:6px;
        border-radius:6px;  
        border:1px solid #B7B7B7;
        background:#EBEBEB;
        display:inline-block;
        position:relative;
        margin-left:2px;
        text-shadow: 1px 1px 0px #fff;
        cursor: pointer;
     }
     
     td{
        padding:4px 0;
     }
     
     td p {
        font-style: italic;
        color: #989898;
        font-size:10px;
     }

	
	table.layouts td{
		padding-right:20px;
		padding-bottom:20px;
		text-align:center;
	}
	
	table.layouts label{
		font-size:10px;
		color: #979797; 
	}

	table.layouts td img:hover{
		border-color:#71AEC6;
	}
	
	#values{
		display:none;
	}

	.content_wrapper{
		background:#fff;
		padding:20px;
	}


/* ----------------------------------------------------
	BUTTONS
------------------------------------------------------- */

	/* small button */
	a.small_button{ 
		height:22px  !important;
		display:inline-block;
		-moz-border-radius: 6px;
		-webkit-border-radius: 6px;
		border-radius: 6px;
		margin:0;
		position:relative;
		z-index:10;
		line-height:20px;
		text-decoration:none  !important; 
		padding:1px 15px 0 15px !important;
		font-size:11px  !important; 
	}
	
	.small_button span{
		position:relative;
		z-index:12;
	}
	
	/* big button */
	a.big_button{ 
		height:48px  !important;
		display:inline-block;
		-moz-border-radius: 8px;
		-webkit-border-radius: 8px;
		border-radius: 8px;
		margin:0   !important;
		position:relative;
		z-index:10;
		line-height:46px;
		text-decoration:none  !important; 
		padding:1px 22px 0 22px !important;
		font-size:18px  !important; 
	}  
	
	a.big_button span{
		position:relative;
		z-index:11; 
	}  
	
	/* medium button */
	a.medium_button{ 
		height:30px  !important;
		display:inline-block;
		-moz-border-radius: 8px;
		-webkit-border-radius: 8px;
		border-radius: 8px;
		margin:10px 0 0 0   !important;
		position:relative;
		z-index:10;
		text-decoration:none  !important;
		font-size:12px  !important; 
		padding:1px 15px 0 15px !important; 
		line-height:28px;
		font-style: normal !important;		
	}     
	
	.medium_button span{
		position:relative;
		z-index:12;
	} 
	
	/* link button */    
	a.link_button{
		font-style: italic; 
		text-decoration:none;
		padding:0;
		display:inline-block;
	}
	
	a.link_button:hover{
		text-decoration:underline;
	}	 
	
	/* order button */
	a.order_button{
		display:block;
		outline:none;
		text-align:center; 
		height:50px;
		width:298px;
		display:inline-block; 
		line-height:50px;
		text-decoration:none; 
		margin:0 0 20px 0;
		font-size:18px; 
	}
	
	a.order_button:hover{ 
	}


/* ----------------------------------------------------
	BUTTONS
------------------------------------------------------- */

	/*--------- BLUE -------- */
	
	/* small button */
	a.small_button.blue{
		background: url(../../../images/style1/button_background.png);
		color:#fff !important;
		text-shadow:0 1px #424242 !important;
	}
	
	
	/* big button */
	a.big_button.blue{
		background: url(../../../images/style1/big_button_background.png);
		color:#fff !important;
		text-shadow:0 1px #424242 !important;
		border:1px solid #6099ac;
	}  
	
	/* medium button */
	a.medium_button.blue{
		background: url(../../../images/style1/medium_button_background.png);
		color:#fff !important;
		text-shadow:0 1px #424242 !important;
	}     
	
	/* order button */
	a.order_button.blue{
		background: url(../../../images/style1/order-button.png) center -2px no-repeat;
		color:#fff;
		text-shadow:0 1px 1px #424242;
	}
	
	a.order_button.blue:hover{
		background: url(../../../images/style1/order-button.png) center -54px no-repeat;
		text-shadow:0 1px 1px #303030;
	}
	

	/*--------- RED -------- */

	/* small button */
	a.small_button.red{
		background: url(../../../images/style2/button_background.png);
		color:#fff !important;
		text-shadow:0 1px #424242 !important;
	}
	
	
	/* big button */
	a.big_button.red{
		background: url(../../../images/style2/big_button_background.png);
		color:#fff !important;
		text-shadow:0 1px #424242 !important;
		border:1px solid #7A0F1C;
	}  
	
	/* medium button */
	a.medium_button.red{
		background: url(../../../images/style2/medium_button_background.png);
		color:#fff !important;
		text-shadow:0 1px #424242 !important;
	}     
	
	/* order button */
	a.order_button.red{
		background: url(../../../images/style2/order-button.png) center -2px no-repeat;
		color:#fff;
		text-shadow:0 1px 1px #424242;
	}
	
	a.order_button.red:hover{
		background: url(../../../images/style2/order-button.png) center -54px no-repeat;
		text-shadow:0 1px 1px #303030;
	}

	
	/*--------- SILVER -------- */

	/* small button */
	a.small_button.silver{
		background: url(../../../images/style3/button_background.png);
		color:#fff !important;
		text-shadow:0 1px #424242 !important;
	}
	
	
	/* big button */
	a.big_button.silver{
		background: url(../../../images/style3/big_button_background.png);
		color:#fff !important;
		text-shadow:0 1px #424242 !important;
		border:1px solid #7A7A7A;
	}  
	
	/* medium button */
	a.medium_button.silver{
		background: url(../../../images/style3/medium_button_background.png);
		color:#fff !important;
		text-shadow:0 1px #424242 !important;
	}     
	
	/* order button */
	a.order_button.silver{
		background: url(../../../images/style3/order-button.png) center -2px no-repeat;
		color:#fff;
		text-shadow:0 1px 1px #424242;
	}
	
	a.order_button.silver:hover{
		background: url(../../../images/style3/order-button.png) center -54px no-repeat;
		text-shadow:0 1px 1px #303030;
	}	 

	/*--------- GREEN -------- */

	/* small button */
	a.small_button.green{
		background: url(../../../images/style4/button_background.png);
		color:#fff !important;
		text-shadow:0 1px #424242 !important;
	}
	
	/* big button */
	a.big_button.green{
		background: url(../../../images/style4/big_button_background.png);
		color:#fff !important;
		text-shadow:0 1px #424242 !important;
		border:1px solid #6F7A6E;
	}  
	
	/* medium button */
	a.medium_button.green{
		background: url(../../../images/style4/medium_button_background.png);
		color:#fff !important;
		text-shadow:0 1px #424242 !important;
	}     
	
	/* order button */
	a.order_button.green{
		background: url(../../../images/style4/order-button.png) center -2px no-repeat;
		color:#fff;
		text-shadow:0 1px 1px #424242;
	}
	
	a.order_button.green:hover{
		background: url(../../../images/style4/order-button.png) center -54px no-repeat;
		text-shadow:0 1px 1px #303030;
	}

	/*--------- GOLDEN -------- */

	/* small button */
	a.small_button.golden{
		background: url(../../../images/style5/button_background.png);
		color:#fff !important;
		text-shadow:0 1px #424242 !important;
	}

	/* big button */
	a.big_button.golden{
		background: url(../../../images/style5/big_button_background.png);
		color:#fff !important;
		text-shadow:0 1px #424242 !important;
		border:1px solid #7A6A52;
	}  
	
	/* medium button */
	a.medium_button.golden{
		background: url(../../../images/style5/medium_button_background.png);
		color:#fff !important;
		text-shadow:0 1px #424242 !important;
	}     
	
	/* order button */
	a.order_button.golden{
		background: url(../../../images/style5/order-button.png) center -2px no-repeat;
		color:#fff;
		text-shadow:0 1px 1px #424242;
	}
	
	a.order_button.golden:hover{
		background: url(../../../images/style5/order-button.png) center -54px no-repeat;
		text-shadow:0 1px 1px #303030;
	}    			
	
</style>

<script type="text/javascript">
	function rt_send_shortcode(shortcode) {
	    
	    var shortcode_value = jQuery('#'+shortcode).html();
	    
	    window.tinyMCE.execInstanceCommand(window.tinyMCE.activeEditor.editorId, 'mceInsertContent', false, shortcode_value);
	    window.tinyMCE.activeEditor.execCommand('mceRepaint');
	    tinyMCEPopup.close();
	}
		
	function d(id) { return document.getElementById(id); }

	function flipTab(n) {
		for (i=1;i<=6;i++) {
			c = d('content'+i.toString());
			t = d('tab'+i.toString());
			if ( n == i ) {
				c.className = '';
				t.className = 'current';
			} else {
				c.className = 'hidden';
				t.className = '';
			}
		}
	}

    function init() {
        document.getElementById('version').innerHTML = tinymce.majorVersion + "." + tinymce.minorVersion;
        document.getElementById('date').innerHTML = tinymce.releaseDate;
    }
    
    tinyMCEPopup.onInit.add(init);
    
</script>

</head>

<?php
$dummy_text="Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.";
$dummy_text_short="Lorem ipsum dolor sit amet.";
$button_text= "Button Text";
?>
<div id="values">
	
	<!-- two cols -->
	<div id="two-cols">
		<br class="clear" /><div class="boxes"><div class="box two first"><?php echo $dummy_text;?></div><div class="box two last"><?php echo $dummy_text;?></div></div><br class="clear" />
	</div>	

	<!-- three cols -->
	<div id="three-cols">
		<br class="clear" /><div class="boxes"><div class="box three first"><?php echo $dummy_text;?></div><div class="box three"><?php echo $dummy_text;?></div><div class="box three last"><?php echo $dummy_text;?></div></div><br class="clear" />
	</div>

	<!-- four cols -->
	<div id="four-cols">
		<br class="clear" /><div class="boxes"><div class="box four first"><?php echo $dummy_text;?></div><div class="box four"><?php echo $dummy_text;?></div><div class="box four"><?php echo $dummy_text;?></div><div class="box four last"><?php echo $dummy_text;?></div></div><br class="clear" />
	</div>

	<!-- five cols -->
	<div id="five-cols">
		<br class="clear" /><div class="boxes"><div class="box five first"><?php echo $dummy_text;?></div><div class="box five"><?php echo $dummy_text;?></div><div class="box five"><?php echo $dummy_text;?></div><div class="box five"><?php echo $dummy_text;?></div><div class="box five last"><?php echo $dummy_text;?></div></div><br class="clear" />
	</div>

	<!-- two-three cols -->
	<div id="two-three-cols">
		<br class="clear" /><div class="boxes"><div class="box two-three first"><?php echo $dummy_text;?></div><div class="box three last"><?php echo $dummy_text;?></div></div><br class="clear" />
	</div>

	<!-- three-three cols -->
	<div id="three-four-cols">
		<br class="clear" /><div class="boxes"><div class="box three-four first"><?php echo $dummy_text;?></div><div class="box four last"><?php echo $dummy_text;?></div></div><br class="clear" />
	</div>
	
	<!-- four-five cols -->
	<div id="four-five-cols">
		<br class="clear" /><div class="boxes"><div class="box four-five first"><?php echo $dummy_text;?></div><div class="box five last"><?php echo $dummy_text;?></div></div><br class="clear" />
	</div>
	
	

	<!--
	******************************************************
	*
	*
	*         		Pullquotes
	*
	*
	******************************************************
	-->
			    
	
	<!-- pullquote left -->
	<div id="pullquote-left">
		<blockquote class="pullquote alignleft"><p><?php echo $dummy_text;?></p></blockquote>
	</div>

	<!-- pullquote right -->
	<div id="pullquote-right">
		<blockquote class="pullquote alignright"><p><?php echo $dummy_text;?></p></blockquote>
	</div>
	

	<!--
	******************************************************
	*
	*
	*         		Highlights
	*
	*
	******************************************************
	-->
	<div id="htext">
		<span class="htext"><?php echo $dummy_text_short;?></span>
	</div>

	<div id="red">
		<span class="red"><?php echo $dummy_text_short;?></span>
	</div>

	<div id="yellow">
		<span class="yellow"><?php echo $dummy_text_short;?></span>
	</div>	
	
	<div id="black">
		<span class="black"><?php echo $dummy_text_short;?></span>
	</div>

	<!--
	******************************************************
	*
	*
	*         		Dropcaps
	*
	*
	******************************************************
	-->

	<div id="dropcap1">
		<span class="dropcap style1 cufon">A</span>
	</div>
	
	<div id="dropcap2">
		<span class="dropcap style2 cufon">A</span>
	</div>

	<!--
	******************************************************
	*
	*
	*         		Lines
	*
	*
	******************************************************
	-->

	<div id="line">
		<div class="line"><span class="line"> </span></div><br />
	</div>
	
	<div id="line_with_top">
		<div class="line"><span class="top">[top]</span></div><br />
	</div> 


	<!--
	******************************************************
	*
	*
	*         		Lists
	*
	*
	******************************************************
	-->

	<div id="star">
		<br />
			<ul class="star">
				<li>Unordered list test</li>
				<li>Another list element.</li>
				<li>Yet another element in the list.</li>
				<li>Some long text. Lorem ipsum dolor sit amet, consectetur adipisicing elit.</li>
			</ul>
		<br />
	</div> 

	<div id="check">
		<br />
			<ul class="check">
				<li>Unordered list test</li>
				<li>Another list element.</li>
				<li>Yet another element in the list.</li>
				<li>Some long text. Lorem ipsum dolor sit amet, consectetur adipisicing elit.</li>
			</ul>
		<br />
	</div> 		    


	<!--
	******************************************************
	*
	*
	*         		Buttons
	*
	*
	******************************************************
	-->
	
	<div id="button-1-1">
		<a class="small_button blue" title="" href="http://type-your-url"><?php echo $button_text;?></a>
	</div>

	<div id="button-1-2">
		<a class="small_button red" title="" href="http://type-your-url"><?php echo $button_text;?></a>
	</div>
	
	<div id="button-1-3">
		<a class="small_button silver" title="" href="http://type-your-url"><?php echo $button_text;?></a>
	</div>

	<div id="button-1-4">
		<a class="small_button green" title="" href="http://type-your-url"><?php echo $button_text;?></a>
	</div>

	<div id="button-1-5">
		<a class="small_button golden" title="" href="http://type-your-url"><?php echo $button_text;?></a>
	</div>
	
	

	<div id="button-2-1">
		<a class="medium_button blue" title="" href="http://type-your-url"><?php echo $button_text;?></a>
	</div>

	<div id="button-2-2">
		<a class="medium_button red" title="" href="http://type-your-url"><?php echo $button_text;?></a>
	</div>
	
	<div id="button-2-3">
		<a class="medium_button silver" title="" href="http://type-your-url"><?php echo $button_text;?></a>
	</div>

	<div id="button-2-4">
		<a class="medium_button green" title="" href="http://type-your-url"><?php echo $button_text;?></a>
	</div>

	<div id="button-2-5">
		<a class="medium_button golden" title="" href="http://type-your-url"><?php echo $button_text;?></a>
	</div>
	


	<div id="button-3-1">
		<a class="big_button blue" title="" href="http://type-your-url"><?php echo $button_text;?></a>
	</div>

	<div id="button-3-2">
		<a class="big_button red" title="" href="http://type-your-url"><?php echo $button_text;?></a>
	</div>
	
	<div id="button-3-3">
		<a class="big_button silver" title="" href="http://type-your-url"><?php echo $button_text;?></a>
	</div>

	<div id="button-3-4">
		<a class="big_button green" title="" href="http://type-your-url"><?php echo $button_text;?></a>
	</div>

	<div id="button-3-5">
		<a class="big_button golden" title="" href="http://type-your-url"><?php echo $button_text;?></a>
	</div>
	


	<div id="button-4-1">
		<a class="order_button blue" title="" href="http://type-your-url"><?php echo $button_text;?></a>
	</div>

	<div id="button-4-2">
		<a class="order_button red" title="" href="http://type-your-url"><?php echo $button_text;?></a>
	</div>
	
	<div id="button-4-3">
		<a class="order_button silver" title="" href="http://type-your-url"><?php echo $button_text;?></a>
	</div>

	<div id="button-4-4">
		<a class="order_button green" title="" href="http://type-your-url"><?php echo $button_text;?></a>
	</div>

	<div id="button-4-5">
		<a class="order_button golden" title="" href="http://type-your-url"><?php echo $button_text;?></a>
	</div>			 

	
</div>



<?php
#
# 	LAYOUTS
#

if($_GET['section']=='layouts'):
?>

<div class="content_wrapper">
 <table class="layouts">
   <tr>
       <td>		
		<img src="../images/2-col.png" onclick="rt_send_shortcode('two-cols')" class="rt_button" /><br />
		<label>Two Columns</label>
	  </td>

       <td>		
		<img src="../images/3-col.png" onclick="rt_send_shortcode('three-cols')" class="rt_button" /><br />
		<label>Three Columns</label>
	  </td>

       <td>		
		<img src="../images/4-col.png" onclick="rt_send_shortcode('four-cols')" class="rt_button" /><br />
		<label>Four Columns</label>
	  </td>

       <td>		
		<img src="../images/5-col.png" onclick="rt_send_shortcode('five-cols')" class="rt_button" /><br />
		<label>Five Columns</label>
	  </td>       
   </tr>

   <tr>
       <td>		
		<img src="../images/3-1-col.png" onclick="rt_send_shortcode('two-three-cols')" class="rt_button" /><br />
		<label>2:3 and 1:3 Columns </label>
	  </td>

       <td>		
		<img src="../images/4-1-col.png" onclick="rt_send_shortcode('three-four-cols')" class="rt_button" /><br />
		<label>3:4 and 1:4 Columns</label>
	  </td>

       <td>		
		<img src="../images/5-1-col.png" onclick="rt_send_shortcode('four-five-cols')" class="rt_button" /><br />
		<label>4:5 and 1:5 Columns</label>
	  </td>
   </tr>
</table>
</div>
<?php endif;?> 



<?php
#
# 	QUICK STYLES
#

if($_GET['section']=='styling'):
?>
 <div class="content_wrapper">           
       <table>
           
       <tr>
           <td><label>Pullquotes: </label></td>
           <td>
            <input type="button" value="+ insert left" onclick="rt_send_shortcode('pullquote-left')" id="rt_button_8" class="rt_button">
           <input type="button" value="+ insert right" onclick="rt_send_shortcode('pullquote-right')" id="rt_button_8" class="rt_button">
           </td>
       </tr>
       
    
       <tr>
           <td><label>Lists: </label></td>
            <td>
           <input type="button" value="+ insert star icon list" onclick='rt_send_shortcode("star")' id="rt_button_8" class="rt_button">
           <input type="button" value="+ insert check icon list" onclick='rt_send_shortcode("check")' id="rt_button_8" class="rt_button">
           </td>
       </tr>
  
       <tr>
           <td><label for="rt_button_8">Line: </label></td>
           <td><input type="button" value="+ insert" onclick='rt_send_shortcode("line")' id="rt_button_8" class="rt_button"></td>
       </tr>
       
       <tr>
           <td width="150"><label for="rt_button_8">Line With Top Link: </label></td>
           <td><input type="button" value="+ insert" onclick='rt_send_shortcode("line_with_top")' id="rt_button_8" class="rt_button"></td>
       </tr>   
    
       <tr>
           <td><label>Highlights: </label></td>
           <td>
           <input type="button" value="+ insert blue" onclick="rt_send_shortcode('htext')"' id="rt_button_8" class="rt_button">
           <input type="button" value="+ insert red" onclick="rt_send_shortcode('red')" id="rt_button_8" class="rt_button">
           <input type="button" value="+ insert yellow" onclick="rt_send_shortcode('yellow')" id="rt_button_8" class="rt_button">
           <input type="button" value="+ insert black" onclick="rt_send_shortcode('black')" id="rt_button_8" class="rt_button">
           </td>
       </tr>   
     
    
       <tr>
           <td><label>Dropcaps: </label></td>
           <td>
           <input type="button" value="+ insert style 1" onclick="rt_send_shortcode('dropcap1')"' id="rt_button_8" class="rt_button">
           <input type="button" value="+ insert style 2" onclick="rt_send_shortcode('dropcap2')"' id="rt_button_8" class="rt_button">
           </td>
       </tr>
    
       </table>     
</div>     
<?php endif;?>


<?php
#
# 	BUTTONS
#

if($_GET['section']=='buttons'):
?>
<div class="content_wrapper">         
<table>
<tr>
	<td>
		<a class="small_button blue" title="" href="#" onclick='rt_send_shortcode("button-1-1")' ><?php echo $button_text;?></a>
		
		<a class="medium_button blue" title="" href="#" onclick='rt_send_shortcode("button-2-1")' ><?php echo $button_text;?></a>
		
		<a class="big_button blue" title="" href="#" onclick='rt_send_shortcode("button-3-1")' ><?php echo $button_text;?></a>
		
		<a class="order_button blue" title="" href="#" onclick='rt_send_shortcode("button-4-1")' ><?php echo $button_text;?></a>
	</td>
</tr>

<tr>
	<td>
		<a class="small_button red" title="" href="#" onclick='rt_send_shortcode("button-1-2")' ><?php echo $button_text;?></a>
		
		<a class="medium_button red" title="" href="#" onclick='rt_send_shortcode("button-2-2")' ><?php echo $button_text;?></a>
		
		<a class="big_button red" title="" href="#" onclick='rt_send_shortcode("button-3-2")' ><?php echo $button_text;?></a>
		
		<a class="order_button red" title="" href="#" onclick='rt_send_shortcode("button-4-2")' ><?php echo $button_text;?></a>
	</td>
</tr>

<tr>
	<td>
		<a class="small_button silver" title="" href="#" onclick='rt_send_shortcode("button-1-3")' ><?php echo $button_text;?></a>
		
		<a class="medium_button silver" title="" href="#" onclick='rt_send_shortcode("button-2-3")' ><?php echo $button_text;?></a>
		
		<a class="big_button silver" title="" href="#" onclick='rt_send_shortcode("button-3-3")' ><?php echo $button_text;?></a>
		
		<a class="order_button silver" title="" href="#" onclick='rt_send_shortcode("button-4-3")' ><?php echo $button_text;?></a>
	</td>
</tr>
	
<tr>
	<td>
		<a class="small_button green" title="" href="#" onclick='rt_send_shortcode("button-1-4")' ><?php echo $button_text;?></a>
		
		<a class="medium_button green" title="" href="#" onclick='rt_send_shortcode("button-2-4")' ><?php echo $button_text;?></a>
		
		<a class="big_button green" title="" href="#" onclick='rt_send_shortcode("button-3-4")' ><?php echo $button_text;?></a>
		
		<a class="order_button green" title="" href="#" onclick='rt_send_shortcode("button-4-4")' ><?php echo $button_text;?></a>
	</td>
</tr>

<tr>
	<td>
		<a class="small_button golden" title="" href="#" onclick='rt_send_shortcode("button-1-5")' ><?php echo $button_text;?></a>
		
		<a class="medium_button golden" title="" href="#" onclick='rt_send_shortcode("button-2-5")' ><?php echo $button_text;?></a>
		
		<a class="big_button golden" title="" href="#" onclick='rt_send_shortcode("button-3-5")' ><?php echo $button_text;?></a>
		
		<a class="order_button golden" title="" href="#" onclick='rt_send_shortcode("button-4-5")' ><?php echo $button_text;?></a>
	</td>
</tr>
</table>
</div>	
<?php endif;?>