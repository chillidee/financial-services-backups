<?php

$options = array (); 

//initial allows 7 partners logo to be set in footer area

for($i = 1; $i < 8; $i ++){
	$options[] = array(
					"name" => __("Partner {$i} Logo",'rt_theme_admin'),
					"desc" => __("Please choose an image file or write url of your Partner logo. Recommended height 60px. ",'rt_theme_admin'),
					"id" => THEMESLUG."_partner_logo_{$i}",
					"type" => "upload");
	
	$options[] = array(
					"name" => __("Partner {$i} Link",'rt_theme_admin'),
					"desc" => __("Please choose a link for your Partner logo.",'rt_theme_admin'),
					"id" => THEMESLUG."_partner_link_{$i}",
					"hr" => "true",
					"type" => "text");
}


?>