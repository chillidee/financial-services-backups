<?php

$options = array (
			
			array(
					"name"	=> __("Select Your Credit Restoration List Page",'rt_theme_admin'),
					"id" 	=> THEMESLUG."_credit_restoration_list",
					"options" => RTTheme::rt_get_pages(),				
					"hr"		=> true,
					"select" 	=> __("Select a page",'rt_theme_admin'),
					"type" 	=> "select"
				),

			array(
					"name" 	=> __("Select Credit Restoration List Page Box Category",'rt_theme_admin'),
					"id"		=> THEMESLUG."_credit_restoration_cat",
					"options" => RTTheme::rt_get_box_categories(),
					"select" 	=> __("Select a category",'rt_theme_admin'),
					"hr"		=> true,
					"type" 	=> "select"
				), 
				
			
			array(
					"name"	=> __("Select Your Useful Information List Page",'rt_theme_admin'),
					"id" 	=> THEMESLUG."_useful_information_list",
					"options" => RTTheme::rt_get_pages(),				
					"hr"		=> true,
					"select" 	=> __("Select a page",'rt_theme_admin'),
					"type" 	=> "select"
				),

			array(
					"name" 	=> __("Select Useful Information List Page Box Category",'rt_theme_admin'),
					"id"		=> THEMESLUG."_useful_information_cat",
					"options" => RTTheme::rt_get_box_categories(),
					"select" 	=> __("Select a category",'rt_theme_admin'),
					"hr"		=> true,
					"type" 	=> "select"
				), 

			
			array(
					"name"	=> __("Select Your Affiliate Centre List Page",'rt_theme_admin'),
					"id" 	=> THEMESLUG."_affiliate_program_list",
					"options" => RTTheme::rt_get_pages(),				
					"hr"		=> true,
					"select" 	=> __("Select a page",'rt_theme_admin'),
					"type" 	=> "select"
				),

			array(
					"name" 	=> __("Select Affiliate Program List Page Box Category",'rt_theme_admin'),
					"id"		=> THEMESLUG."_affiliate_program_cat",
					"options" => RTTheme::rt_get_box_categories(),
					"select" 	=> __("Select a category",'rt_theme_admin'),
					"hr"		=> true,
					"type" 	=> "select"
				), 	

			
			array(
					"name"	=> __("Select Why Choose Us List Page",'rt_theme_admin'),
					"id" 	=> THEMESLUG."_why_choose_us_list",
					"options" => RTTheme::rt_get_pages(),				
					"hr"		=> true,
					"select" 	=> __("Select a page",'rt_theme_admin'),
					"type" 	=> "select"
				),

			array(
					"name" 	=> __("Select Why Choose Us List Page Box Category",'rt_theme_admin'),
					"id"		=> THEMESLUG."_why_choose_us_cat",
					"options" => RTTheme::rt_get_box_categories(),
					"select" 	=> __("Select a category",'rt_theme_admin'),
					"hr"		=> true,
					"type" 	=> "select"
				), 	

			
			array(
					"name"	=> __("Select Your Affiliate Program Page",'rt_theme_admin'),
					"id" 	=> THEMESLUG."_affiliate_program",
					"options" => RTTheme::rt_get_pages(),				
					"hr"		=> true,
					"select" 	=> __("Select a page",'rt_theme_admin'),
					"type" 	=> "select"
				),

			array(
					"name" 	=> __("Amount of boxes per page",'rt_theme_admin'),
					"desc"	=> __("How many boxes do you want to display per page?",'rt_theme_admin'),
					"id" 	=> THEMESLUG."_box_list_pager",
					"min"	=> "1",
					"max"	=> "200",
					"default"	=> "9",
					"hr"		=> true,
					"type" 	=> "rangeinput"
				),
	 
			array(
					"name" 	=> __("OrderBy Parameter",'rt_theme_admin'),
					"desc" 	=> __("sort the boxes by this parameter",'rt_theme_admin'),
					"id" 	=> THEMESLUG."_box_list_orderby",
					"options" => array('author'=>'Author','date'=>'Date','title'=>'Title','modified'=>'Modified','ID'=>'ID','rand'=>'Randomized'),
					"default"	=> "date",
					"hr"		=> true,
					"type" 	=> "select"
				),
	
			array(
					"name" 	=> __("Order",'rt_theme_admin'),
					"desc" 	=> __("Designates the ascending or descending order of the ORDERBY parameter",'rt_theme_admin'),
					"id" 	=> THEMESLUG."_box_list_order",
					"options" => array('ASC'=>'Ascending','DESC'=>'Descending'),
					"default"	=> "DESC",
					"hr"		=> true,
					"type" 	=> "select"
				),

			array(
					"name" => __("BOX IMAGES",'rt_theme_admin'), 
					"type" => "heading"
				),
			
			array(
					"name" 	=> __("Crop Box Images",'rt_theme_admin'),
					"id" 	=> THEMESLUG."_box_image_crop",
					"default" => "on",
					"hr"		=> true,
					"type" 	=> "checkbox"
				),
			
			array(
				   "name" 	=> __("Maximum Image Heght",'rt_theme_admin'),
				   "id" 		=> THEMESLUG."_box_image_height",
				   "desc"		=> __('You can use this option if the "Crop Box Images" feature is on','rt_theme_admin'),
				   "min"		=>"60",
				   "max"		=>"400",
				   "default"	=>"120",
				   "type" 	=> "rangeinput"
				),
				
			array(
				"name" 	=> __("PERMALINKS",'rt_theme_admin'), 
				"type" 	=> "heading"
			),

			array(
					"name" 	=> __("Category Slug",'rt_theme_admin'),
					"desc" 	=> __("Change the slug for box categories",'rt_theme_admin'),
					"id" 	=> THEMESLUG."_box_category_slug",
					"default"	=> "boxes",
					"help" 	=> "true",
					"type" 	=> "text",
					"hr" 	=> "true"
				),

			array(
					"name" 	=> __("Single Box Slug",'rt_theme_admin'),
					"desc" 	=> __("IMPORTANT! Bring your cursor over the help icon to read the detailed instructions ",'rt_theme_admin'),
					"id" 	=> THEMESLUG."_box_single_slug",
					"help" 	=> "true",
					"default"	=> "box",
					"type" 	=> "text",
					"hr" 	=> "true"
				)
			
		); 
?>