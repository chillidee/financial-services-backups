<?php

$options = array (

			array(
					"name" => __("Logo",'rt_theme_admin'),
					"desc" => __("Please choose an image file or write url of your logo.",'rt_theme_admin'),
					"id" => THEMESLUG."_logo_url",
					"hr" => "true",
					"type" => "upload"),
			
			array(
					"name" => __("Custom Favicon",'rt_theme_admin'),
					"desc" => __("You can put url of a ico image that will represent your website's favicon (16px x 16px) ",'rt_theme_admin'),
					"id" => THEMESLUG."_favicon_url",
					"type" => "text"),	  

			array(
					"name" => __("WIDGETIZED PART OF HOME PAGE",'rt_theme_admin'),
					"type" => "heading"),
	 
			array(
					"name" => __("Layout",'rt_theme_admin'),
					"desc" => __("Select the layout of widgetized home page content area.",'rt_theme_admin'),
					"id" => THEMESLUG."_home_box_width",
					"options" =>  array(
								5 => "1/5", 
								4 => "1/4",
								3 => "1/3",
								2 => "1/2",
								1 => "1/1"
							  ),
					"default" => "3",
					"help"=> "true",
					"type" => "select"),


			array(
					"name" => __("TOP BAR",'rt_theme_admin'), 
					"type" => "heading"),

			array(
					"name" => __("Show Top Bar",'rt_theme_admin'),
					"desc" => __("Show the bar on top of the page (includes search and top links)",'rt_theme_admin'),
					"id" => THEMESLUG."_top_bar",
					"type" => "checkbox",
					"default" => "checked"),
		
			array(
					"name" => __("SEARCH FIELD and FLAGS",'rt_theme_admin'), 
					"type" => "heading"),

			array(
					"name" => __("Show Search",'rt_theme_admin'),
					"id" => THEMESLUG."_show_search",
					"desc" => __('Show search form field on top of the page.','rt_theme_admin'),	
					"type" => "checkbox",
					"default" => "checked",
					"help"=> "true",
					"hr" => "true"),

			array(
					"name" => __("Show Flags",'rt_theme_admin'),
					"desc" => __("Show WPML plugin's language flags on top of the page",'rt_theme_admin'),				
					"id" => THEMESLUG."_show_flags",
					"default" => "checked",
					"type" => "checkbox",
					"hr" => "true"
					), 

			array(
					"name" => __("Header Text",'rt_theme_admin'),
					"desc" => __("Edit the text on right side of the header. If you don't want to use it leave the field blank",'rt_theme_admin'),
					"id" => THEMESLUG."_header_text",
					"default"	=> "Call Us Free: +01 555 55 55",
					"type" => "text"),

			array(
					"name" => __("Homepage Big Text",'rt_theme_admin'),
					"desc" => __("Edit the big text on center of home page. If you don't want to use it leave the field blank",'rt_theme_admin'),
					"id" => THEMESLUG."_homepage_bigtext",
					"default"	=> "Clean Up Your Credit File & Improve Your Credit Score with Credit Repair",
					"type" => "text"),
		
			array(
					"name" => __("FOOTER RELATED FIELDS",'rt_theme_admin'), 
					"type" => "heading"), 
			
			array(
					"name" => __("Footer Copyright Text",'rt_theme_admin'),
					"desc" => __("The copyright text area on right-sider footer",'rt_theme_admin'),
					"id" => THEMESLUG."_footer_copy",
					"default"	=> "Copyright &copy; 2011 Company Name, Inc.",
					"type" => "text",
					"hr" => "true"),

			array(
					"name" => __("Footer Widgets",'rt_theme_admin'),
					"desc" => __("Show footer widgets and use 'Sidebar for Footer' sidebar.",'rt_theme_admin'),				
					"id" => THEMESLUG."_show_footer_widgets",
					"type" => "checkbox",
					"hr" => "true"
					),
			
			array(
					"name" => __("Footer Widget Layout",'rt_theme_admin'),
					"desc" => __("Select the layout of the footer widgets.",'rt_theme_admin'),
					"id" => THEMESLUG."_footer_box_width",
					"options" =>  array(
								5 => "1/5", 
								4 => "1/4",
								3 => "1/3",
								2 => "1/2",
								1 => "1/1"
							  ),
					"default" => "3",
					"help"=> "true",
					"type" => "select"),
			
			array(
					"name" => __("TWITTER BAR",'rt_theme_admin'), 
					"type" => "heading"), 
			

			array(
					"name" => __("Turn on/off the Twitter Bar",'rt_theme_admin'),
					"id" => THEMESLUG."_twitter_bar",
					"type" => "checkbox",
					"hr" => "true"),
			array(
					"name" => __("Twitter Username",'rt_theme_admin'), 
					"id" => THEMESLUG."_twitter_username", 
					"type" => "text"),			
			
			array(
					"name" => __("BREADCRUMB MENU",'rt_theme_admin'), 
					"type" => "heading"),

			array(
					"name" => __("Breadcrumb Menus",'rt_theme_admin'),
					"desc" => __("You can turn on/off bredcrumb menus",'rt_theme_admin'),
					"id" => THEMESLUG."_breadcrumb_menus",
					"hr" => "true",
					"default" => "checked",
					"type" => "checkbox"),

			array(
					"name" => __("Breadcrumb Menu Text",'rt_theme_admin'),
					"desc" => __("The text before the breadcrumb menu",'rt_theme_admin'),
					"id" => THEMESLUG."_breadcrumb_text",
					"default" => __("You are here:",'rt_theme_admin'), 
					"type" => "text"),

			array(
					"name" =>  __("SIDEBAR MENU FOR PAGES",'rt_theme_admin'),  
					"type" => "heading"),
	
			array(
					"name" => __("Show sub pages on page sidebar",'rt_theme_admin'),  
					"id" => THEMESLUG."_show_sub_pages",
					"default" => "checked",
					"hr" => "true",					
					"type" => "checkbox"),

			array(
					"name" => __("Same Lavel Sub Pages",'rt_theme_admin'),   
					"desc" => __("Show same lavel pages on sub page sidebar menu.",'rt_theme_admin'),  
					"id" => THEMESLUG."_same_lavel",
					"type" => "checkbox",			
					"std" => "false"),
			array(
					"name" => __("GOOGLE ANALYTICS",'rt_theme_admin'), 
					"type" => "heading"), 
			
			array(
					"name" => __("Analytics Code",'rt_theme_admin'),
					"desc" => __("Paste your google analytics code",'rt_theme_admin'),
					"id" => THEMESLUG."_google_analytics",
					"type" => "textarea",					
					"hr" => "true",		
					),			
); 
?>