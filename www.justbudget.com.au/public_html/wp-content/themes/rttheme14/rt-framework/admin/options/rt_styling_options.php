<?php
$options = array (

			array(
				   "name" => __("Theme Style",'rt_theme_admin'),
				   "desc" => __("Please choose a style for your theme. If you choose style 1-5, please choose the Cufon for Menu (Aller Light) in Typography Options. Otherwise, don't choose any Cufon for Menu!!!!!",'rt_theme_admin'),
				   "id" => THEMESLUG."_style",
				   "options" =>	array(
									  0 => "Default",
									  1 => "Style 1",
									  2 => "Style 2",
									  3 => "Style 3",
									  4 => "Style 4",
									  5 => "Style 5",
							    ),
				   "default"=> 0,
				   "hr" => "true",
				   "type" => "select"),
			array(
				   "name" => __("Custom Heading Color",'rt_theme_admin'),
				   "desc" => __("You can turn on/off cufon font replacement plugin. Leave blank if you want to use default colors.",'rt_theme_admin'),
				   "id" => THEMESLUG."_heading_color",
				   "hr" => "true", 
				   "type" => "colorpicker"),

			array(
				   "name" => __("Body Font Color",'rt_theme_admin'),
				   "id" => THEMESLUG."_body_font_color",
				   "hr" => "true",
				   "default"=>"#5B5B5B", 
				   "dont_save"=>"true",
				   "type" => "colorpicker"),
 
			array(
				   "name" => __("Custom Menu Font Color",'rt_theme_admin'),
				   "id" => THEMESLUG."_menu_font_color",
				   "hr" => "true",
				   "type" => "colorpicker"),

			array(
				   "name" => __("Custom Menu Font Color (:hover states)",'rt_theme_admin'),
				   "id" => THEMESLUG."_menu_font_color_hover",
				   "hr" => "true",
				   "type" => "colorpicker"),
			 
		
);

 
 
?>