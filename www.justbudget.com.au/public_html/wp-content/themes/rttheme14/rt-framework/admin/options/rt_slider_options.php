<?php
$options = array (


			array(
				   "name" => __("Transition Timeout (seconds)",'rt_theme_admin'),
				   "id" => THEMESLUG."_slider_timeout",
				   "hr" => "true",
				   "min"=>"1",
				   "max"=>"120",
				   "default"=>"4",
				   "type" => "rangeinput"),


			array(
				   "name" => __("Slider Height (px)",'rt_theme_admin'),
				   "id" => THEMESLUG."_slider_height",
				   "hr" => "true",
				   "min"=>"100",
				   "max"=>"1000",
				   "default"=>"320",
				   "type" => "rangeinput"),

	  
			array(
				   "name" => __("Transition Effect",'rt_theme_admin'),
				   "desc" => __("Please choose an effect for main page slider",'rt_theme_admin'),
				   "id" => THEMESLUG."_slider_effect",
				   "options" =>	array(
								"blindX"	=>	    "blindX",
								"blindY"	=>	    "blindY",
								"blindZ"	=>	    "blindZ",
								"cover"	=>	    "cover",
								"fade"	=>	    "fade",
								"none"	=>	    "none",
								"scrollUp"	=>	    "scrollUp",
								"scrollDown"	=>	    "scrollDown",
								"scrollLeft"	=>	    "scrollLeft",
								"scrollRight"	=>	    "scrollRight",
								"scrollHorz"	=>	    "scrollHorz",
								"scrollVert"	=>	    "scrollVert",
								"slideX"	=>	    "slideX",
								"slideY"	=>	    "slideY",
								"turnUp"	=>	    "turnUp",
								"turnDown"	=>	    "turnDown",
								"turnLeft"	=>	    "turnLeft",
								"turnRight"	=>	    "turnRight"
							    ),
				   "default"=>"fade",
				   "hr" => "true",
				   "type" => "select"),
			array(
				   "name" => __("Slider Buttons",'rt_theme_admin'),
				   "desc" => __("You can turn on/off the paging buttons of the slider",'rt_theme_admin'),
				   "id" => THEMESLUG."_slider_buttons",
				   "hr" => "true",
				   "default" => "checked",
				   "type" => "checkbox"),             
		
); 
?>