/* Adapted from http://brettterpstra.com/adding-a-tinymce-button/ */

(function() {
	tinymce.create('tinymce.plugins.rt_theme_shortcodes', {
		init : function(ed, url) {

			ed.addButton('rt_themeshortcode', {
				title : 'RT-Theme Layouts',
				image : url+'/../images/layout-shortcodes.png', 
				onclick : function() {
					ed.windowManager.open({
						file : url + '/../pages/rt_shortcodes_popup.php?section=layouts',
						width : 540,
						height : 330,
                              title: 'RT-Theme Layouts',
						inline : 1	
					});
				}
			});

			ed.addButton('rt_themeshortcode_2', {
				title : 'RT-Theme Quick Styling',
				image : url+'/../images/styling-shortcodes.png', 
				onclick : function() {
					ed.windowManager.open({
						file : url + '/../pages/rt_shortcodes_popup.php?section=styling',
						width : 640,
						height : 320,
                              title: 'RT-Theme Quick Styling',
						inline : 1
					});
				}
			});			


			ed.addButton('rt_themeshortcode_4', {
				title : 'RT-Theme Buttons ',
				image : url+'/../images/button-shortcodes.png', 				
				onclick : function() {
					ed.windowManager.open({
						file : url + '/../pages/rt_shortcodes_popup.php?section=buttons',
						width : 720,
						height : 460,
                              title: 'RT-Theme Buttons',
						inline : 1
					});
				}
			});

			ed.addButton('rt_themeshortcode_5', {
				title : 'RT-Theme Contact Form Shortcode ',
				image : url+'/../images/mail-open.png',
				onclick : function() {
					window.tinyMCE.execInstanceCommand(window.tinyMCE.activeEditor.editorId, 'mceInsertContent', false, '[contact_form title=\"Form Title\" email=\"youremail@yoursite.com\" text=\"Form description\"] ');
					window.tinyMCE.activeEditor.execCommand('mceRepaint');
					
						jQuery(".rt-message-contact-form").remove();
						jQuery("#poststuff").prepend('<div class="rt-message-contact-form"></div>');

						jQuery(".rt-message-contact-form").hide(function() {
							jQuery(".rt-message-contact-form").html('<div class="updated"><div class="rt-message">X</div>'
											+ '	<h2 class="rt-message-h2">Shortcode Tips</h2> '
											+ '	<hr class="rt-message-hr" /> Please Note: You can also use this shortcode with a text widget in sidebars.'
											+ '	<h4>Parameters of this shortcode</h4> '
											+ '	<ul>	'
											+ '	<li> <b>title:</b> Form title</li>	'
											+ '	<li> <b>email:</b> Write an email which you want to send the form</li>	'
											+ '	<li> <b>text:</b> The text before the form</li>	'											
											+ '	</ul></div>');						
						});
						jQuery(".rt-message-contact-form").fadeIn('slow');
				
				}
				
			});

			ed.addButton('rt_themeshortcode_6', {
				title : 'RT-Theme Slider Shortcode',
				image : url+'/../images/slider-shortcodes.png',
				onclick : function() {
					window.tinyMCE.execInstanceCommand(window.tinyMCE.activeEditor.editorId, 'mceInsertContent', false, '[slider]<br />[slide image_width=\"650\" image_height=\"300\" link=\"your_link\" alt_text="check it out" auto_resize="true"] full url of your image [/slide] <br />[slide image_width=\"650\" image_height=\"300\" link="your_link\" alt_text="check it out" auto_resize="true"] full url of your image [/slide] <br />[/slider] <br /> <br /> ');
					window.tinyMCE.activeEditor.execCommand('mceRepaint');


						jQuery(".rt-message-contact-form").remove();
						jQuery("#poststuff").prepend('<div class="rt-message-contact-form"></div>');

						jQuery(".rt-message-contact-form").hide(function() {
							jQuery(".rt-message-contact-form").html('<div class="updated"><div class="rt-message">X</div>'
									+ '	<h2 class="rt-message-h2">Shortcode Tips</h2> '
									+ '	<hr class="rt-message-hr" /> You can enter unlimited [slide ]..[/slide] line to add new items to your gallery.'
									+ '	<h4>Parameters of this shortcode</h4> '
									+ '	<ul>	'
									+ '	<li> <b>image_width:</b> Image width</li>	'
									+ '	<li> <b>image_height:</b> Image height</li>	'
									+ '	<li> <b>auto_resize:</b> If it\'s "true" a new image will be created automatically. Default is "true", set "false" if you want to use your orginal image.</li>	'
									+ '	<li> <b>link:</b> Write the link for the slide or leave blank.</li>	'
									+ '	<li> <b>alt_text:</b> The text for the "alt" tag.</li>	'
									+ '	</ul></div>');						
						});
						jQuery(".rt-message-contact-form").fadeIn('slow'); 
				}
			});

			ed.addButton('rt_themeshortcode_7', {
				title : 'RT-Theme Photo Gallery Shortcode',
				image : url+'/../images/photo-gallery-shortcodes.png',
				onclick : function() {
					window.tinyMCE.execInstanceCommand(window.tinyMCE.activeEditor.editorId, 'mceInsertContent', false, '			[photo_gallery] <br />[image thumb_width="135" thumb_height="135" lightbox="true" custom_link="" title="sample image"] full url of your image [/image] <br />[image thumb_width="135" thumb_height="135" lightbox="true" custom_link="" title="sample image"] full url of your image [/image] <br />[image thumb_width="135" thumb_height="135" lightbox="true" custom_link="" title="sample image"] full url of your image [/image] <br />[/photo_gallery] <br /> <br /> ');
					window.tinyMCE.activeEditor.execCommand('mceRepaint');

						jQuery(".rt-message-contact-form").remove();
						jQuery("#poststuff").prepend('<div class="rt-message-contact-form"></div>');

						jQuery(".rt-message-contact-form").hide(function() {
							jQuery(".rt-message-contact-form").html('<div class="updated"><div class="rt-message">X</div>'
									+ '	<h2 class="rt-message-h2">Shortcode Tips</h2> '
									+ '	<hr class="rt-message-hr" /> You can enter unlimited [image ]..[/image] line to add new items to your gallery.'
									+ '	<h4>Parameters of this shortcode</h4> '
									+ '	<ul>	'
									+ '	<li> <b>thumb_width:</b> thumbnail width</li>	'
									+ '	<li> <b>thumb_height:</b> thumbnail height</li>	'
									+ '	<li> <b>lightbox:</b> opens the big image in a lightbox</li>	'
									+ '	<li> <b>custom_link:</b> you can define another link different then the big version of the thumbnail.</li>	'
									+ '	<li> <b>title:</b> title text.</li>	'
									+ '	</ul></div>');						
						});
						jQuery(".rt-message-contact-form").fadeIn('slow'); 
				}
			});

			ed.addButton('rt_themeshortcode_8', {
				title : 'RT-Theme Auto Thumbnail and Lightbox Shortcode',
				image : url+'/../images/thumbnail-shortcodes.png',
				onclick : function() {
					window.tinyMCE.execInstanceCommand(window.tinyMCE.activeEditor.editorId, 'mceInsertContent', false, '			[auto_thumb width="150" height="150" link="" lightbox="true" align="left" title="" alt="" iframe="false" frame="true" crop="true"] full url of your image [/auto_thumb] <br /> <br /> ');
					window.tinyMCE.activeEditor.execCommand('mceRepaint');

						jQuery(".rt-message-contact-form").remove();
						jQuery("#poststuff").prepend('<div class="rt-message-contact-form"></div>');

						jQuery(".rt-message-contact-form").hide(function() {
							jQuery(".rt-message-contact-form").html('<div class="updated"><div class="rt-message">X</div>'
									+ '	<h2 class="rt-message-h2">Shortcode Tips</h2> '
									+ '	<hr class="rt-message-hr" /> '
									+ '	<h4>Parameters of this shortcode</h4> '
									+ '	<ul>	'
									+ '	<li> <b>link:</b> you can enter custom url. If you leave blank it will be linked to the bigger version of the image. </li>	'
									+ '	<li> <b>width:</b> thumbnail width</li>	'
									+ '	<li> <b>height:</b> thumbnail height</li>	'
									+ '	<li> <b>lightbox:</b> (true/false) default is true, enter no to disable lightbox feature</li>	'
									+ '	<li> <b>title:</b> link title text.</li>	'
									+ '	<li> <b>align:</b> (left/right/center) default is left, image alignment</li>	'
									+ '	<li> <b>alt:</b> alt tag for image</li>	'
									+ '	<li> <b>iframe:</b> (true/false) default is false. Use this paramater if you want to open a page or an external url in a lightbox.</li>	'
									+ '	<li> <b>frame:</b> (true/false) default is true.  Use this paramater if you want to add a frame to the thubmnail.</li>	'
									+ '	<li> <b>crop:</b> (true/false) default is true. Crops images with the width and height values that you defined.</li>	'
									+ '	</ul></div>');						
						});
						jQuery(".rt-message-contact-form").fadeIn('slow');
						 
				}
			});				
			
		},
		createControl : function(n, cm) {
			return null;
		},
		getInfo : function() {
			return {
				longname : "Shortcodes",
				author : 'RT-Theme',
				version : "1.0"
			};
		}
	});
	tinymce.PluginManager.add('rt_themeshortcode', tinymce.plugins.rt_theme_shortcodes);
})();