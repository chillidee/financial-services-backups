<?php
#
# RT-Theme Contact Info
#

class Contact_Info extends WP_Widget {

	function Contact_Info() {
		$opts =array(
					'classname' 	=> 'widget_contact_info',
					'description' 	=> __( 'Use this widget to display your contact details with icons.', 'rt_theme_admin' )
				);

		$this-> WP_Widget('contact_info', '['. THEMENAME.']   '.__('Contact Info', 'rt_theme_admin'), $opts);
	}
	

	function widget( $args, $instance ) {
		extract( $args );
		
		$title			=	apply_filters('widget_title', $instance['title']) ;		 
		$address			=	wpml_t( THEMESLUG , 'Address', $instance['address'] );
		$phone_1			=	wpml_t( THEMESLUG , 'Phone 1', $instance['phone_1'] ); 
		$phone_2			=	wpml_t( THEMESLUG , 'Phone 2', $instance['phone_2'] );
		$mail_1			=	wpml_t( THEMESLUG , 'Email 1', $instance['mail_1'] );
		$mail_2			=	wpml_t( THEMESLUG , 'Email 2', $instance['mail_2'] );
		$support_mail_1	=	wpml_t( THEMESLUG , 'Support Email 1', $instance['support_mail_1'] );
		$support_mail_2	=	wpml_t( THEMESLUG , 'Support Email 2', $instance['support_mail_2'] ); 
 
		//Contact Info
 		$contactInfo = '<ul class="contact_list">';
		
		if(!empty($address)) 			$contactInfo .= '<li class="home">'.$address.'</li>';
		if(!empty($phone_1)) 			$contactInfo .= '<li class="phone">'.$phone_1.'</li>';
		if(!empty($phone_2))			$contactInfo .= '<li class="phone">'.$phone_2.'</li>';
		if(!empty($mail_1)) 			$contactInfo .= '<li class="mail">'.$mail_1.'</li>';
		if(!empty($mail_2)) 			$contactInfo .= '<li class="mail">'.$mail_2.'</li>'; 
		if(!empty($support_mail_1)) 		$contactInfo .= '<li class="support">'.$support_mail_1.'</li>';
		if(!empty($support_mail_2)) 		$contactInfo .= '<li class="support">'.$support_mail_2.'</li>';
		
		$contactInfo .= '</ul>';
		 

		echo $before_widget;
		if ($title) echo $before_title . $title . $after_title;
		echo $contactInfo;
		echo $after_widget;
	}

	function update( $new_instance, $old_instance ) {
		 
		$instance = $old_instance;
		$instance['title']			= strip_tags($new_instance['title']);  
		$instance['address']		= strip_tags($new_instance['address']);	
		$instance['phone_1']		= strip_tags($new_instance['phone_1']);
		$instance['phone_2']		= strip_tags($new_instance['phone_2']);
		$instance['mail_1']			= strip_tags($new_instance['mail_1']);
		$instance['mail_2']			= strip_tags($new_instance['mail_2']);
		$instance['support_mail_1']	= strip_tags($new_instance['support_mail_1']);
		$instance['support_mail_2']	= strip_tags($new_instance['support_mail_2']);
		
		wpml_register_string( THEMESLUG , 'Address', strip_tags($new_instance['address']) ) ;
		wpml_register_string( THEMESLUG , 'Phone 1', strip_tags($new_instance['phone_1']) ) ;
		wpml_register_string( THEMESLUG , 'Phone 2', strip_tags($new_instance['phone_2']) ) ;
		wpml_register_string( THEMESLUG , 'Email 1', strip_tags($new_instance['mail_1']) ) ;
		wpml_register_string( THEMESLUG , 'Email 2', strip_tags($new_instance['mail_2']) ) ;
		wpml_register_string( THEMESLUG , 'Support Email 1', strip_tags($new_instance['support_mail_1']) ) ;
		wpml_register_string( THEMESLUG , 'Support Email 2', strip_tags($new_instance['support_mail_2']) ) ;		

		return $instance;
	}

	function form( $instance ) {
		$title 			= 	isset($instance['title']) ? esc_attr($instance['title']) : '';
		$address 			= 	isset($instance['address']) ? esc_attr($instance['address']) : '';
		$phone_1 			= 	isset($instance['phone_1']) ? esc_attr($instance['phone_1']) : '';
		$phone_2 			= 	isset($instance['phone_2']) ? esc_attr($instance['phone_2']) : '';
		$mail_1 			= 	isset($instance['mail_1']) ? esc_attr($instance['mail_1']) : '';
		$mail_2 			= 	isset($instance['mail_2']) ? esc_attr($instance['mail_2']) : '';
		$support_mail_1 	= 	isset($instance['support_mail_1']) ? esc_attr($instance['support_mail_1']) : '';
		$support_mail_2 	= 	isset($instance['support_mail_2']) ? esc_attr($instance['support_mail_2']) : '';
		
		// Categories
		$rt_getcat = RTTheme::rt_get_categories();
		

?>
		<p><label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:', 'rt_theme_admin'); ?></label>
		<input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo empty($title) ? __('Contact Details','rt_theme_admin') : $title; ?>" /></p>
 
		<p><label for="<?php echo $this->get_field_id('address'); ?>"><?php _e('Address:', 'rt_theme_admin'); ?></label>
		<input class="widefat" id="<?php echo $this->get_field_id('address'); ?>" name="<?php echo $this->get_field_name('address'); ?>" type="text" value="<?php echo $address; ?>" /></p>

		<p><label for="<?php echo $this->get_field_id('phone_1'); ?>"><?php _e('Phone 1:', 'rt_theme_admin'); ?></label>
		<input class="widefat" id="<?php echo $this->get_field_id('phone_1'); ?>" name="<?php echo $this->get_field_name('phone_1'); ?>" type="text" value="<?php echo $phone_1; ?>" /></p>

		<p><label for="<?php echo $this->get_field_id('phone_2'); ?>"><?php _e('Phone 2:', 'rt_theme_admin'); ?></label>
		<input class="widefat" id="<?php echo $this->get_field_id('phone_2'); ?>" name="<?php echo $this->get_field_name('phone_2'); ?>" type="text" value="<?php echo $phone_2; ?>" /></p>

		<p><label for="<?php echo $this->get_field_id('mail_1'); ?>"><?php _e('Email 1:', 'rt_theme_admin'); ?></label>
		<input class="widefat" id="<?php echo $this->get_field_id('mail_1'); ?>" name="<?php echo $this->get_field_name('mail_1'); ?>" type="text" value="<?php echo $mail_1; ?>" /></p>		

		<p><label for="<?php echo $this->get_field_id('mail_2'); ?>"><?php _e('Email 2:', 'rt_theme_admin'); ?></label>
		<input class="widefat" id="<?php echo $this->get_field_id('mail_2'); ?>" name="<?php echo $this->get_field_name('mail_2'); ?>" type="text" value="<?php echo $mail_2; ?>" /></p>		

		<p><label for="<?php echo $this->get_field_id('support_mail_1'); ?>"><?php _e('Support Email 1:', 'rt_theme_admin'); ?></label>
		<input class="widefat" id="<?php echo $this->get_field_id('support_mail_1'); ?>" name="<?php echo $this->get_field_name('support_mail_1'); ?>" type="text" value="<?php echo $support_mail_1; ?>" /></p>		
 	
		<p><label for="<?php echo $this->get_field_id('support_mail_2'); ?>"><?php _e('Support Email 2:', 'rt_theme_admin'); ?></label>
		<input class="widefat" id="<?php echo $this->get_field_id('support_mail_2'); ?>" name="<?php echo $this->get_field_name('support_mail_2'); ?>" type="text" value="<?php echo $support_mail_2; ?>" /></p>				
<?php } } ?>