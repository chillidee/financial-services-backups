<?php
/* RT-Theme Shortcodes */ 


/*using shortcodes in widgets*/

add_filter( 'widget_text', 'shortcode_unautop');
add_filter( 'widget_text', 'do_shortcode');

//shortcodes 



/*
* ------------------------------------------------- *
*		Widget Caller
* ------------------------------------------------- *
*/
function rt_widget_caller($atts, $content = null){
//[widget_caller id="sidebarid_37036"]

 	//defaults
	extract(shortcode_atts(array(  
		"id" => ''
	), $atts));
	
     //check id
	if(!empty($id)){
	    dynamic_sidebar($id);
	}
	
	return $content;
 
}

add_shortcode('widget_caller', 'rt_widget_caller');

/*
* ------------------------------------------------- *
*		Fix shortcodes
* ------------------------------------------------- *
*/

function fixshortcode($content){

     //fix

	//remove invalid p
	$content = preg_replace('#^<\/p>|<p>$#', '', trim($content));
	
	//fix line shortcode
     $content = preg_replace('#<p>\n<div class="line top #', '<div class="line top ', trim($content));
     $content = preg_replace('#<p>\n<div class="line"></div>\n</p>#', '<div class="line"></div>', trim($content)); 
     $content = preg_replace('#<p>\n<div class="line">#', '<div class="line">', trim($content));
    
	return $content;
}

/*
* ------------------------------------------------- *
*		PHOTO GALLERY		
* ------------------------------------------------- *		
*/ 
function rt_photo_gallery( $atts, $content = null ) {
	//[photo_gallery]
	$rt_photo_gallery='<div class="photo_gallery"><ul>';
	$rt_photo_gallery .= do_shortcode(strip_tags($content));
	$rt_photo_gallery.='</ul><div class="clear"></div></div>';
	return $rt_photo_gallery;
}

function rt_photo_gallery_lines( $atts, $content = null ) {
	//[image thumb_width="135" thumb_height="135" lightbox="true" custom_link="" title="photo title"]
	
	//defaults
	extract(shortcode_atts(array(  
		"thumb_width" 		=> '135',
		"thumb_height"		=> '135',
		"lightbox" 		=> 'true',
		"custom_link" 		=> '',
		"title"			=> '',	   
	), $atts)); 
	
	$photo=trim($content);

	//icon
	if ($lightbox!="true" && !empty($custom_link)) {
		$icon="link";
	} else {
		$icon="plus";
	}
	
	//width and height
	if($thumb_width=="")  $thumb_width = "135";
	if($thumb_height=="") $thumb_height = "135";
	
	// Resize Portfolio Image
	if($content) $image = @vt_resize( '', $photo, $thumb_width, $thumb_height, 'true' );
	
	//lightbox = default is true
	if($lightbox != "false" ) $lightbox='rel="prettyPhoto[rt_photo_gallery]"';
	
	//link - default is image 
	if (!$custom_link) $custom_link=trim($content);
	 
	$rt_photo_gallery_lines.='<li>';
	$rt_photo_gallery_lines.='<span class="frame">';
	$rt_photo_gallery_lines.='<a href="'.$custom_link.' " title="'.$title.'"  '.$lightbox.' class="imgeffect '.$icon.'">';
	$rt_photo_gallery_lines.='<img src="'. $image['url'] .'" />';
	$rt_photo_gallery_lines.='</a></span></li>'; 
	
	return $rt_photo_gallery_lines;
}	

add_shortcode('photo_gallery', 'rt_photo_gallery');
add_shortcode('image', 'rt_photo_gallery_lines');



/*
* ------------------------------------------------- *
*	Auto Thumbnails & Lightboxes	
* ------------------------------------------------- *		
*/ 
function rt_auto_thumb( $atts, $content = null ) {
	//[auto_thumb width="" height="" link="" lightbox="" align="" title="" alt="" iframe="" frame=""]
 
 	//defaults
	extract(shortcode_atts(array(  
		"width" 			=> '135',
		"height"			=> '135',
		"link" 			=> '',
		"lightbox" 		=> 'true',
		"align"			=> 'left',
		"title"			=> '',
		"alt"			=> '',
		"iframe"			=> 'false',
		"frame"			=> 'true',
		"crop"			=> 'true',
	), $atts));
	

	//width and height
	if($width=="")  $width = "135";
	if($height=="") $height = "135";
	
	//clear p and br tags
	$content = preg_replace('#^<\/p>|<p>$#', '', trim($content));
	$content = preg_replace('#^<p>|<\/p>$#', '', trim($content));
	$content = preg_replace('#^<br />$#', '', trim($content));	
     
     
	//lightbox
	if($lightbox!="false") $lightbox='rel="prettyPhoto[rt_theme_thumb]"';
	
 	//if it's not a video
	if($link=="") $link=$content;
	
	/* icon */
	if (preg_match("/(png|jpg|gif)/",  trim($link) )) {
		$icon="plus";
	} elseif($lightbox=="false" && !empty($link)) {
		$icon="link";
	} else {
		$icon="play";
	}
    
     //frame
	if($frame=="true"){
        
		if($align=="left")		:  	$border_open='<span class="frame alignleft">';  				$border_close='</span>'; 		endif;
		if($align=="right")		: 	$border_open='<span class="frame alignright">';  				$border_close='</span>'; 		endif;
		if($align=="center")	:  	$border_open='<span class="aligncenter"><span class="frame">';  	$border_close='</span></span>'; 	endif;
	    
		$align="";
     }	 
	
	
	//iframe
	if ($iframe=='true') $iframe= '?iframe=true&width=90%&height=100%';  else  $iframe = '';	
	if (preg_match("/(mov|avi|swf|vimeo|youtube|screenr)/",  trim($link))): $iframe= ""; else: if($iframe && trim($link) ) $icon=""; endif;
	
	
	//crop
	if($crop=="false") $height = 0;
	
	// Resize Portfolio Image
	if($content) $image = @vt_resize( '', trim($content), $width, $height, $crop ); 
	
 
	//result
  
	if (trim($content)): 
	$rt_auto_thumb ='<a href="'.$link.''.$iframe.'" title="'.$title.'"  '.$lightbox.' class="imgeffect '.$icon.'"><img src="'.$image['url'].'" alt="'.$alt.'"  class="align'.$align.'" /></a>';	
	else:
	$rt_auto_thumb ='<a href="'.$link.''.$iframe.'" title="'.trim($atts["title"]).'"  '.$lightbox.' >'.trim($atts["title"]).'</a>';
	endif;
     $rt_auto_thumb = $border_open . $rt_auto_thumb . $border_close;
 
	
	return $rt_auto_thumb;
}

add_shortcode('auto_thumb', 'rt_auto_thumb'); 


/*
* ------------------------------------------------- *
*		Contact Form Pages
* ------------------------------------------------- *
*/
function rt_shortcode_contact_form( $atts, $content = null ) {
 
if(isset($atts['title'])) $contact_form= '<div class="clear"></div><h3>'.$atts['title'].'</h3>';
if(isset($atts['text'])) $contact_form.= '<p><i>'.$atts['text'].'</i></p>';

if(isset($atts['email'])){


$contact_form.= "".    
	'<!-- contact form -->'.
	'<div class="clear"></div>'.
	'<div class="panel_home"><div class="mid"><div class="bottom">'.
	'<div id="contact_form">'.
	'	<div style="text-align:center; height: 22px;"><img alt="" src="'.get_bloginfo('template_directory').'/images/style0/express_enquiry.gif"></div>'.
	'	<div class="headtop">Quick, Easy & Confidential</div>'.
	'	<form action="'.get_bloginfo('template_directory').'/contact_form.php" name="contact_form" id="validate_form" method="post">'.
	'		<table><tbody>'.
	'			<tr><td class="col1"><label for="firstname">First Name: <span class="redtext">*</span></label></td>'.
	'				<td class="col2"><input id="firstname" type="text" name="firstname" value="" class="required" /></td></tr>'.
	'			<tr><td class="col1"><label for="lastname">Last Name: <span class="redtext">*</span></label></td>'.
	'				<td class="col2"><input id="lastname" type="text" name="lastname" value="" class="required" /></td></tr>'.
	'			<tr><td class="arow" colspan="2" style="text-align: left;"><span id="contact_restriction" class="instructions">'.__('Please enter 2 forms of contact below (e.g. mobile + email)','rt_theme').'</span></td></tr>'.
	'			<tr><td class="col1"><label for="homenumber">'.__('Home Number:','rt_theme').'</label></td>'.
	'				<td class="col2"><select id="homenumber_areacode" name="homenumber_areacode" class="required"><option value="">-</option><option value="02">02</option><option value="03">03</option><option value="07">07</option><option value="08">08</option></select>&nbsp;<input style="width: 61px !important" id="homenumber" type="text" name="homenumber" value="" minlength="8" maxlength="8" class="digits" /></td></tr>'.
	'			<tr><td class="col1"><label for="worknumber">'.__('Work Number:','rt_theme').'</label></td>'.
	'				<td class="col2"><select id="worknumber_areacode" name="worknumber_areacode" class="required"><option value="">-</option><option value="02">02</option><option value="03">03</option><option value="07">07</option><option value="08">08</option></select>&nbsp;<input style="width: 61px !important" id="worknumber" type="text" name="worknumber" value="" minlength="8" maxlength="8" class="digits" /></td></tr>'.
	'			<tr><td class="col1"><label for="mobilenumber">'.__('Mobile Number:','rt_theme').'</label></td>'.
	'				<td class="col2"><input id="mobilenumber" type="text" name="mobilenumber" value="" minlength="10" maxlength="10" class="digits" /></td></tr>'.
	'			<tr><td class="col1"><label for="email">'.__('Email:','rt_theme').'</label></td>'.
	'				<td class="col2"><input id="email" type="text" name="email" value="" class="email" /></td></tr>'.
	'			<tr><td class="col1"><label for="suburb">'.__('Suburb:','rt_theme').' <span class="redtext">*</span></label></td>'.
	'				<td class="col2"><input id="suburb" type="text" name="suburb" value="" class="required"/></td></tr>'.
	'			<tr><td class="col1"><label id="state_label" for="state">'.__('State:','rt_theme').'<span class="redtext">*</span></label>'.
	'					<select id="state" name="state" class="required">'.
	'						<option value="">-</option>'.
	'						<option value="NSW">NSW</option>'.
	'						<option value="ACT">ACT</option>'.
	'						<option value="NT">NT</option>'.
	'						<option value="QLD">QLD</option>'.
	'						<option value="SA">SA</option>'.
	'						<option value="TAS">TAS</option>'.
	'						<option value="VIC">VIC</option>'.
	'						<option value="WA">WA</option>'.
	'					</select></td>'.
	'				<td class="col2"><label id="postcode_label" for="postcode">'.__('Postcode:','rt_theme').'<span class="redtext">*</span></label>'.
	'					<input id="postcode" type="text" name="postcode" value="" style="width: 50px !important" class="required digits" minlength="4" maxlength="4"/></td></tr>'.

	'			<tr><td class="col1" colspan="2"><label id="youown_label" for="youown">Do you own or are you paying off</label></td></tr>'.
	'			<tr class="youown"><td class="col1">real estate?<span class="redtext">*</span></td><td class="col2"><label for="youown1" class="inline">Yes</label><input id="youown1" type="radio" name="youown" value="1"> <label for="youown2" class="inline">No</label><input id="youown2" type="radio" name="youown" value="0"></td></tr>'.
	'			<tr class="propertyvalue"><td class="col1"><label for="propertyvalue">'.__('Real Estate Property value:','rt_theme').'</label></td>'.
	'				<td class="col2"><input id="propertyvalue" type="text" name="propertyvalue" value="" minlength="1" maxlength="10" class="digits" /></td></tr>'.
	'			<tr class="balanceowing"><td class="col1"><label for="balanceowing">'.__('Balance Owing:','rt_theme').'</label></td>'.
	'				<td class="col2"><input id="balanceowing" type="text" name="balanceowing" value="" minlength="1" maxlength="10" class="digits" /></td></tr>'.

	'			<tr><td class="col1"><label id="hearaboutus_label" for="hearaboutus">Where did you hear about us?<span class="redtext">*</span></label></td>'.
	'				<td class="col2"><select name="hearaboutus" id="hearaboutus" class="select2" style="width:115px !important;">
					<option selected="selected" value="0">-Select One-</option>
					<option value="1">Yahoo!</option>
					<option value="2">Radio</option>
					<option value="3">Newspaper</option>
					<option value="4">Google</option>
					<option value="5">Television</option>
					<option value="6">Family/Friend</option>
					<option value="7">Email</option>
					<option value="7">Other / Not Sure</option>
				</select></td></tr>'.

	'			<tr><td class="col1"><label id="comment_label" for="comment"><b>Comments</b><span class="redtext">*</span></label></td>'.
	'				<td class="col2"><textarea  id="comment" name="comment" rows="6" cols="20" class="required"></textarea></td></tr>'.
	'			<tr><td class="arow" colspan="2">'.
	'			<input type="hidden" name="your_email" value="'.trim($atts['email']).'">'.
	'			<input type="hidden" name="your_web_site_name" value="'.get_bloginfo('name').'">'.
	'			<input type="hidden" name="text_1" value="'.__('Thanks','rt_theme').'">'.	
	'			<input type="hidden" name="text_2" value="'.__('Thank you for making an enquiring to NSW Mortgage Corp.  One of our professionals will be in contact with you shortly to discuss how we can assist you.','rt_theme').'">'.	
	'			<input type="hidden" name="text_3" value="'.__('There was an error submitting the form.','rt_theme').'">'.	
	'			<input type="hidden" name="text_4" value="'.__('Please enter a valid email address!','rt_theme').'">'.
	'			<input type="submit" class="button" value="Send Enquiry"  /><span class="loading"></span></td></tr>'.
	'		</tbody></table>'.
	'	</form>'.
	'</div></div></div></div><div id="result"></div><div class="clear"></div>'.
	'<!-- /contact form -->'; 
}else{
	$contact_form="ERROR: This shortcode is not contains an email attribute!";
}

return $contact_form;
}
add_shortcode('contact_form', 'rt_shortcode_contact_form');


/*
* ------------------------------------------------- *
*		Contact Form for iFrame
* ------------------------------------------------- *
*/
function rt_shortcode_iframe_contact_form( $atts, $content = null ) {
 
	if(!isset($atts['baseurl'])) return 'Error: Base URL is not set. ';
	if(!isset($atts['cid'])) return 'Error: CID is not set. ';
	$cssurl = get_bloginfo('template_directory')."/css/style0/enquiryForm.css";
	$cssurl = urlencode($cssurl);
	$referurl = urlencode($_SESSION['mm_referalurl']);

	$contact_form.= "".    
		'<!-- contact form -->'.
		'<div class="clear"></div>'.
		'<div id="contact_form">'.
		'	<iframe id="iframe_enquiry_form" frameBorder="0" src="'.$atts['baseurl'].'?cid='.$atts['cid'].'&mode='.$atts['mode'].'&svs='.$atts['svs'].'&css='.$cssurl.'&ref='.$referurl.'&t='.$_SESSION['mm_adwords_t'].'&k='.$_SESSION['mm_adwords_k'].'&a='.$_SESSION['mm_adwords_a'].'"></iframe>'.
		'</div>'.
		'<div class="clear"></div>'.
		'<!-- /contact form -->'.
		'
		<style> 
			.sidebar_back2 { padding-bottom: 0 !important; }
			.sidebar_back {padding-top: 0 !important; }
		</style>
		<script type="text/javascript">
		//#sidebar needs to have style: position: relative for this to work.
		jQuery(document).ready(function($) 
		{ 
		    originalSidebarTop = jQuery("#sidebar").offset().top;
		    jQuery(window).scroll(function () {
		    	var scrollTop = jQuery(document).scrollTop();
		        var contentHeight = jQuery("#left").height();
		        var sidebarHeight = jQuery("#sidebar").height();
		        
		        if(scrollTop > originalSidebarTop){
		        	offset = scrollTop - originalSidebarTop;
		        }else{
		        	offset = -30;
		        }
		        
		        if(offset > (contentHeight - sidebarHeight)){
		        	offset = contentHeight - sidebarHeight; //can not go beyond the content length
		        	if(offset < 0){ //just in case content length is shorter than sidebar
		        		offset = -30;
		        	}
		        }
		        
		        jQuery("#sidebar").animate({top:offset+"px"},{duration:500,queue:false});
		        
		        console.log("originalSidebarTop:"+originalSidebarTop+" contentHeight:"+contentHeight+" sidebarHeight:"+sidebarHeight+" scrollTop:"+jQuery(document).scrollTop()+" offset:"+offset);
		    }); 
		});
		</script>
		';
	
	return $contact_form;
}
add_shortcode('iframe_contact_form', 'rt_shortcode_iframe_contact_form');


/*
* ------------------------------------------------- *
*		Get Free Report Form
* ------------------------------------------------- *
*/
function rt_shortcode_free_report_form( $atts, $content = null ) {
 
if(isset($atts['email'])){
$free_report_form = "".    
	'<!-- free report form -->'.
	'<div class="clear"></div><div id="result"></div>'.
	'<div>'.
	'	<form action="'.get_bloginfo('template_directory').'/free_report_form.php" name="free_report_form" id="free_report_form" method="post">'.
	'		<table><tbody>'.
	'			<tr><td><label for="fullname">Name: <span class="redtext">*</span></label></td><td><input id="fullname" type="text" name="fullname" value="" class="required" /></td></tr>'.
	'			<tr><td><label for="freeemail">Email: <span class="redtext">*</span></label></td><td><input id="freeemail" type="text" name="freeemail" value="" class="email required" /></td></tr>'.
	'			<tr><td colspan="2">'.
	'			<input type="hidden" name="your_email" value="'.trim($atts['email']).'">'.
	'			<input type="hidden" name="your_web_site_name" value="'.get_bloginfo('name').'">'.
	'			<input type="hidden" name="text_1" value="'.__('Thanks','rt_theme').'">'.	
	'			<input type="hidden" name="text_2" value="'.__('Thank you. Free report has been sent to your mailbox.','rt_theme').'">'.	
	'			<input type="hidden" name="text_3" value="'.__('There was an error submitting the form.','rt_theme').'">'.	
	'			<input type="hidden" name="text_4" value="'.__('Please enter a valid email address!','rt_theme').'">'.
	'			<input type="submit" class="button" value="get report"  /><span class="loading"></span></td></tr>'.
	'		</tbody></table>'.
	'	</form>'.
	'</div><div class="clear"></div>'.
	'<!-- /free report form -->'; 
}else{
	$free_report_form = "ERROR: This shortcode is not contains an email attribute!";
}

return $free_report_form;
}
add_shortcode('free_report_form', 'rt_shortcode_free_report_form');


/*
* ------------------------------------------------- *
*		Image Slider
* ------------------------------------------------- *
*/
    	


function rt_shortcode_slider( $atts, $content = null ) {
	//[slider][/slider]

	//fix content
	$content = preg_replace('#<br \/>#', "",trim($content));
	$content = preg_replace('#<p>#', "",trim($content));
	$content = preg_replace('#<\/p>#', "",trim($content));
	
 	$content = wpautop(do_shortcode($content));
	$content = fixshortcode($content);
	
	return '<div class="frame"><div class="photo_gallery_cycle">	<ul>' . trim($content) . '</ul>	<div class="clear"></div></div></div><div class="slider_buttons"></div>';
}

function rt_shortcode_slider_slides( $atts, $content = null ) {
 	//[slide image_width="" image_height="" link="" alt_text="" auto_resize=""]

	//defaults
	extract(shortcode_atts(array(  
       "image_width" => '650',
	   "image_height" => '300',
	   "link" => '',
	   "alt_text" => '',
	   "auto_resize" => 'true',
	   "class" => ''	   
	), $atts));

	//width and height
	if($image_width=="")  $image_width = "650";
	if($image_height=="") $image_height = "300";

	//fix content
	$content = preg_replace('#<br \/>#', "",trim($content));
	$content = preg_replace('#<p>#', "",trim($content));
	$content = preg_replace('#<\/p>#', "",trim($content));		
 
	if($link){
		$link1='<a href="'.$link.'">';
		$link2='</a>';
	}

	if(!empty($class)) $class = ' class="'.$class.'"';
	
	$slide='<li'.$class.'>';	
	
	// Resize Portfolio Image
	if($content) $image = @vt_resize( '', $content, $image_width, $image_height, 'true' );
	
	if($auto_resize=="true"){
	$slide.=$link1.'<img src="'.$image['url'].'" width="'.$image_width.'" height="'.$image_height.'" alt="'.$alt_text.'" />'.$link2;
	}else{
	$slide.=$link1.'<img src="'.$content.'"  alt="'.$alt_text.'" />'.$link2;
	}
	$slide.='</li>';
	
	return $slide;
}


add_shortcode('slider', 'rt_shortcode_slider');
add_shortcode('slide', 'rt_shortcode_slider_slides');
 

/*
* ------------------------------------------------- *
*		show shortcode 
* ------------------------------------------------- *
*/

function rt_shortcode_show_shortcode( $atts, $content = null ) {
 
	//convert html [] spacial chars  

	//fix shortcode
	$content = fixshortcode($content);
	$content = preg_replace('#<br \/>#', "",trim($content));
	$content = preg_replace('#<p>#', "",trim($content));
	$content = preg_replace('#<\/p>#', "",trim($content));
	$content = preg_replace('#\[\/braket_close\]#', "[/show_shortcode]",trim($content));
	
	return '<code>' . htmlspecialchars($content) . '</code>';
}

add_shortcode('show_shortcode', 'rt_shortcode_show_shortcode');


?>