<?php
/* 
* rt-theme home page content loop
*/

global $home_page,$which_theme,$row;

query_posts($home_page);
 
		$box_counter=1;
		$total_width=0;
		
		if (have_posts() ) : while ( have_posts() ) : the_post();
		
		#
		#	Values 
		#
		
		$box_title=get_the_title();
		$box_sub_title=get_post_meta($post->ID, THEMESLUG.'sub_title', true);
		$custom_link = get_post_meta($post->ID, THEMESLUG.'custom_link', true);
		$custom_link_text = get_post_meta($post->ID, THEMESLUG.'custom_link_text', true);
	 
		$box_layout= get_post_meta($post->ID, THEMESLUG.'layout_options', true);

		#
		#	Layout Options
		#
		$home_layout_names=array("9"=>"four-five","8"=>"three-four","7"=>"two-three","6"=>"full-box","5"=>"five","4"=>"four","3"=>"three","2"=>"two");
		if($box_layout==0 || !$box_layout) $box_layout=3;
		
		#
		# Thumbnail sizes
		#
		
		switch ($box_layout) {
			case 9:
			    $w=700;
			break;                       
			case 8:
			    $w=700;
			break;                       
			case 7:
			    $w=620;
			break;                    
			case 6:
			    $w=960;
			break;     
			case 5:
			    $w=172;
			break;                
			case 4:
			    $w=220;
			break;
			case 3:
			    $w=300;
			break;
			case 2:
			    $w=460;
			break;		  
			default:
			    $w=300;
			break;		  
		}

		#
		#	featured image 
		#
		
		$f_thumb = get_post_thumbnail_id(); 
		$f_image = @vt_resize( $f_thumb, '', $w, 1000, false );		  
		
		?>
		
		<?php
		
		#
		#	Calculation total width
		#
		
		$total_width=$total_width+$w+20; 
		
		if($box_counter==1):
		    $box_class="first";
		elseif($total_width>799):
		    $box_class="last"; 
		else:
		    $box_class="";
		endif;
		 
		if($total_width>960){
			echo "<div class=\"clear\"></div>";
			//echo "<div class=\"space\"></div>";
			$box_class="first";
			$total_width=$w;
			$first_line = "end";
		}
		
		#
		#	Extra space for no-freatured image
		#		
		if((!isset($first_line)) && !@$f_image["url"]){
			$box_class .= " noimage"; 
		}
		?>
	    

		<!-- box -->
		<!-- customize here: according chosen style -->
		<div class="box <?php echo $home_layout_names[$box_layout];?> <?php echo $box_class;?>">
			
			<?php if (THEMESTYLE == '0') { ?>
				<div class="panel_home">
					<div class="mid">
						<div class="bottom">
							<div class="panel_content">
									<!-- Title -->
									<h2><?php if($custom_link):?><a href="<?php echo $custom_link;?>" title="<?php echo $box_title;?>"><?php endif;?><?php echo $box_title;?><?php if($custom_link):?></a><?php endif;?></h2>					    
									<?php if($f_thumb):?>
									<!-- featured image -->
									<div class="featured_image">
										<?php if($custom_link):?><a href="<?php echo $custom_link;?>" title="<?php echo $box_title;?>"><?php endif;?>
											<img src="<?php echo @$f_image["url"];?>" class="featured_image" alt="" />
										<?php if($custom_link):?></a><?php endif;?>
									</div>
									<?php endif;?>
									<!-- text-->
									<!-- for the third box, we put officeautopilot form in -->
									<?php if($post->ID == 32){
										echo '<div class="moonraybox">';
										echo the_content();
										echo '<script type="text/javascript" src="https://forms.moon-ray.com/v2.4/include/minify/?g=moonrayJS"></script>';
										echo '<link rel="stylesheet" type="text/css" href="http://forms.moon-ray.com/v2.4/include/minify/?g=moonrayCSS">';
										echo '<script type="text/javascript" src="https://www1.moon-ray.com/v2.4/analytics/genf.php?f=p2c5208f5"></script>';
										echo '</div>';
									} else {
										echo the_content();
										// learn more button 
										if ($custom_link && $custom_link_text):
										    echo "<a href=\"". $custom_link ."\" title=\"". $box_title ."\" class=\"small_button\">". $custom_link_text ."</a>";
										endif;
									} ?>
							</div>
						</div>
					</div>
				</div>
			<?php } else { ?>
				<?php if($f_thumb):?>
				<!-- featured image -->
				<img src="<?php echo @$f_image["url"];?>" class="featured_image" alt="" />
				<?php endif;?>
				
				<div class="featured">
					<div class="title">
						<!-- box title-->
						<h4><?php if($custom_link):?><a href="<?php echo $custom_link;?>" title="<?php echo $box_title;?>"><?php endif;?><?php echo $box_title;?><?php if($custom_link):?></a><?php endif;?></h4>					    
						<!-- box subtitle-->
						<?php if($custom_link):?><span class="sub_title"><?php echo $box_sub_title;?></span><?php endif;?>
					</div>
					
					<!-- text-->
					<?php echo the_content();?>
					
					<?php
					if ($custom_link && $custom_link_text):
					    echo "<a href=\"". $custom_link ."\" title=\"". $box_title ."\" class=\"small_button\">". $custom_link_text ."</a>";
					endif;
					?>
				</div>
			<?php } ?>
		    
		</div>
		<!-- /box -->
    
		<?php
		$box_counter++; 
		?>
    
		<?php endwhile;endif;?>
            
<?php wp_reset_query();?>
<div class="clear"></div>