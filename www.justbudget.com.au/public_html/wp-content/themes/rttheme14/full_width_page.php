<?php
/*
Template Name: Full Width Page - Enquiry Complete
*/
get_header();
?>
	<!-- slider -->
	<?php if(is_front_page()) get_template_part( 'slider', 'home_slider' );  ?>
	<!-- / slider -->

	<?php 
		//show page banner slides when it is not blog page
		//if($post->ID != wpml_page_id(BLOGPAGE)){
		
		$z_page_banners_options_slugs = array("z_page_banners_option_1", "z_page_banners_option_2", "z_page_banners_option_3","z_page_banners_option_4");
		shuffle($z_page_banners_options_slugs); 
		foreach($z_page_banners_options_slugs as $z_page_slug){
			$z_page = get_page_by_path($z_page_slug);
		    if (!$z_page) { continue; }
		    $z_page_id = $z_page->ID;
		    $thumb 		=	get_post_thumbnail_id($z_page_id); 
		    $thumb2 = null; $thumb3 = null;
		    if (class_exists('MultiPostThumbnails') && MultiPostThumbnails::has_post_thumbnail('page', 'secondary-image',$z_page_id)){
				$thumb2 = MultiPostThumbnails::get_post_thumbnail_id('page', 'secondary-image', $z_page_id);
		    }
		    if (class_exists('MultiPostThumbnails') && MultiPostThumbnails::has_post_thumbnail('page', 'third-image',$z_page_id)){
				$thumb3 = MultiPostThumbnails::get_post_thumbnail_id('page', 'third-image', $z_page_id);
		    }
		    if(!empty($thumb) || !empty($thumb2) || !empty($thumb3)){
		    	$shortcode_text = '[slider]';
		    	if(!empty($thumb)){
		    		$image 		=	@vt_resize( $thumb, '', 970, 150, true );
					$shortcode_text .= '
						[slide class="first" image_width="970" image_height="150" link="" alt_text="check it out" auto_resize="true"]'.$image['url'].'[/slide]';
		    	}
				if (!empty($thumb2)){
					$image2 =	@vt_resize( $thumb2, '', 970, 150, true );
					$shortcode_text .= '
						[slide image_width="970" image_height="150" link="" alt_text="check it out" auto_resize="true"]'.$image2['url'].'[/slide]';
				}
				if (!empty($thumb3)){
					$image3 =	@vt_resize( $thumb3, '', 970, 150, true );
					$shortcode_text .= '
						[slide image_width="970" image_height="150" link="" alt_text="check it out" auto_resize="true"]'.$image3['url'].'[/slide]';
				}
		    	$shortcode_text .= '
		    		[/slider]'; 
		    	
				echo '<div class="page_banner">';
				echo do_shortcode($shortcode_text);
				echo '</div>';
				break;
		    }
		    
		}
	//}
	?>

	<div class="diognal-pattern-dummy"></div>

<!-- contents -->
<div class="border">

	<!-- page title --> 
	<?php if(!is_front_page()):?>
		<br/>
		<div class="head_text"><h1 style="text-align: center"><?php the_title(); ?></h1></div>
		<!-- / page title --> 
	
		<!-- Page navigation-->
		<?php //rt_breadcrumb(); ?>
		<!-- /Page navigation-->
	<?php endif;?>


	<div class="content_background sub">
		<div class="content sub full">
			<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
			    <?php the_content(); ?>
			    <br/>
			    <br/>
			    <br/>
			    <br/>
			    <br/>
			    <br/>
			<?php endwhile;?>
			<?php else: ?>
				<p><?php _e( 'Sorry, no page found.', 'rt_theme' ); ?></p>
			<?php endif; ?>       			 
		<div class="clear"></div>
		</div>
	</div>
	
	<!-- custom google script -->
	<!-- Google Code for lead Conversion Page -->
	<script type="text/javascript">
	/* <![CDATA[ */
	var google_conversion_id = 1011844785;
	var google_conversion_language = "en";
	var google_conversion_format = "2";
	var google_conversion_color = "ffffff";
	var google_conversion_label = "I3WuCJ-ayQIQsY2-4gM";
	var google_conversion_value = 0;
	/* ]]> */
	</script>
	<script type="text/javascript" src="http://www.googleadservices.com/pagead/conversion.js">
	</script>
	<noscript>
	<div style="display:inline;">
	<img height="1" width="1" style="border-style:none;" alt="" src="http://www.googleadservices.com/pagead/conversion/1011844785/?label=I3WuCJ-ayQIQsY2-4gM&amp;guid=ON&amp;script=0"/>
	</div>
	</noscript>
	
</div>
<!-- / contents  -->
	
<?php get_footer();?>