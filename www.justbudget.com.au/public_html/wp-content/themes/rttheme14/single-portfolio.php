<?php
# 
# rt-theme single portfolio page
#

//taxonomy
$taxonomy = 'portfolio_categories';

//page link
$link_page=get_permalink(get_option('rttheme_portf_page'));

//category link
$terms = get_the_terms($post->ID, $taxonomy);
$i=0;
if($terms){
    foreach ($terms as $taxindex => $taxitem) {
    if($i==0){
		$link_cat		= get_term_link($taxitem->slug,$taxonomy);
		$term_slug 	= $taxitem->slug;
		$term_id 		= $taxitem->term_id;    
		}
    $i++;
    }
}

// portfolio crop image
$crop 	= 	get_option('rttheme_portfolio_image_crop');

get_header();
 
?>

<!-- contents -->
<div class="border">

	<!-- page title --> 
	<div class="head_text"><h2><?php the_title(); ?></h2></div>
	<!-- / page title --> 
    
	<!-- Page navigation-->
	<?php rt_breadcrumb(); ?>
	<!-- /Page navigation-->

	<?php
	//Portfolio Categories Popup Menu
	if(get_option('rttheme_portfolio_popup_categories')){
		get_template_part( 'portfolio_categories_menu', 'portfolio_categories_menu_file' );
	}
	?>		
 
	<div class="content_background sub">
	<div class="content sub">
	
		<!-- page contents  -->
		<div id="left" class="left">
 
			<?php if (have_posts()) : while (have_posts()) : the_post(); ?> 
				<?php
				
				// Values
				$image 			=	get_post_meta($post->ID, 'rttheme_portfolio_image', true);
				$title 			=	get_the_title();
				$video 			=	get_post_meta($post->ID, 'rttheme_portfolio_video', true);
				$video_thumbnail 	=	get_post_meta($post->ID, 'rttheme_portfolio_video_thumbnail', true); 
				$desc 			=	get_post_meta($post->ID, 'rttheme_portfolio_desc', true);
				$permalink	 	=	get_permalink();
				$remove_link	 	= 	get_post_meta($post->ID, 'rttheme_portf_no_detail', true);
				$custom_thumb		= 	get_post_meta($post->ID, 'rttheme_portfolio_thumb_image', true);	
				$w=640;
				$h=400;
				
				// Crop
				if($crop) $crop="true"; else $h=10000;
				
				// Resize Portfolio Image
				if($image) $image_thumb = @vt_resize( '', $image, $w, $h, ''.$crop.'' );
				
				// Resize Video Image
				if($video_thumbnail) $video_thumbnail = @vt_resize( '', $video_thumbnail, $w, 999, '' );
				
				
				// Getting image type
				if ($video) {
					$button="play";
					$media_link= $video;
				} else {
					$media_link= $image;
					$button="plus";
				}
				?>

				<?php if ($image || $video_thumbnail || $custom_thumb):?>
				<!-- portfolio image -->
				<span class="frame block">
					
					<?php if($media_link):?><a href="<?php echo $media_link;?>" title="<?php echo $title; ?>" rel="prettyPhoto[rt_theme_portfolio]" class="imgeffect <?php echo $button;?>"><?php endif;?>
		
						  <?php if($custom_thumb)://auto resize not active?>
							 <img src="<?php echo $custom_thumb;?>" alt="<?php echo $title; ?>"  />
						  <?php elseif($video_thumbnail):?>
							 <img src="<?php echo $video_thumbnail["url"];?>" alt="<?php echo $title;?>" />	    
						  <?php else:?>
							 <img src="<?php echo $image_thumb["url"];?>" alt="<?php echo $title;?>" />
						  <?php endif;?>
		
					<?php if($media_link):?></a><?php endif;?>
				</span>
				<div class="space"></div>
				<!-- / portfolio image -->		
				<?php endif;?>
		 
				<?php  the_content(); ?>
				
			<?php endwhile;?>

			<div class='entry commententry'>
			    <?php comments_template(); ?>
			</div>
		  
			<?php else: ?>
				<p><?php _e( 'Sorry, no page found.', 'rt_theme'); ?></p>
			<?php endif; ?>
			
		</div>
		<!-- / page contents  -->
		
		<!-- side bar --> 
		<div id="sidebar" class="sidebar"><div class="sidebar_back"><div class="sidebar_back2">
		<?php get_template_part( 'sidebar', 'sidebar_file' );?>
		<div class="clear"></div></div></div></div>
		<!-- / side bar -->
    
	<div class="clear"></div>
	</div>
	</div>
</div>
<!-- / contents  -->

<?php get_footer();?> 