<?php
/* 
* rt-theme 404 
*/
get_header();  
?>

<!-- contents -->
<div class="border">


	<!-- page title --> 
	<div class="head_text"><h2>404 <?php wp_title(''); ?></h2></div>
	<!-- / page title --> 
    
	<!-- Page navigation-->
	<?php rt_breadcrumb(); ?>
	<!-- /Page navigation-->
 
	<div class="content_background sub">
	<div class="content sub">
	
		<!-- page content -->
		<div id="left" class="left">
			<h1 style="font-size:120px;">404</h1>
			<h6><?php _e( 'Apologies, but the page you requested could not be found. Perhaps searching will help.', 'rt_theme'); ?></h6><br>
		</div>
		<!-- / page contents  -->
 
		<!-- side bar --> 
		<div id="sidebar" class="sidebar"><div class="sidebar_back"><div class="sidebar_back2">
		<?php get_template_part( 'sidebar', 'sidebar_file' );?>
		<div class="clear"></div></div></div></div>
		<!-- / side bar -->
 
    
	<div class="clear"></div>
	</div>
	</div>
</div>
<!-- / contents  -->
    
<?php get_footer();?>