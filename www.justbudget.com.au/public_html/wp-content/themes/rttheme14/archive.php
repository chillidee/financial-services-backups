<?php
/* 
* rt-theme archive 
*/
get_header();
$category = get_category_by_slug(get_query_var("category_name"));  
?>

	<?php 
	//show page banner slides when it is not blog page
	//if($post->ID != wpml_page_id(BLOGPAGE)){
		
		$z_page_banners_options_slugs = array("z_page_banners_option_1", "z_page_banners_option_2", "z_page_banners_option_3","z_page_banners_option_4");
		shuffle($z_page_banners_options_slugs); 
		foreach($z_page_banners_options_slugs as $z_page_slug){
			$z_page = get_page_by_path($z_page_slug);
		    if (!$z_page) { continue; }
		    $z_page_id = $z_page->ID;
		    $thumb 		=	get_post_thumbnail_id($z_page_id); 
		    $thumb2 = null; $thumb3 = null;
		    if (class_exists('MultiPostThumbnails') && MultiPostThumbnails::has_post_thumbnail('page', 'secondary-image',$z_page_id)){
				$thumb2 = MultiPostThumbnails::get_post_thumbnail_id('page', 'secondary-image', $z_page_id);
		    }
		    if (class_exists('MultiPostThumbnails') && MultiPostThumbnails::has_post_thumbnail('page', 'third-image',$z_page_id)){
				$thumb3 = MultiPostThumbnails::get_post_thumbnail_id('page', 'third-image', $z_page_id);
		    }
		    if(!empty($thumb) || !empty($thumb2) || !empty($thumb3)){
		    	$shortcode_text = '[slider]';
		    	if(!empty($thumb)){
		    		$image 		=	@vt_resize( $thumb, '', 970, 150, true );
					$shortcode_text .= '
						[slide class="first" image_width="970" image_height="150" link="" alt_text="check it out" auto_resize="true"]'.$image['url'].'[/slide]';
		    	}
				if (!empty($thumb2)){
					$image2 =	@vt_resize( $thumb2, '', 970, 150, true );
					$shortcode_text .= '
						[slide image_width="970" image_height="150" link="" alt_text="check it out" auto_resize="true"]'.$image2['url'].'[/slide]';
				}
				if (!empty($thumb3)){
					$image3 =	@vt_resize( $thumb3, '', 970, 150, true );
					$shortcode_text .= '
						[slide image_width="970" image_height="150" link="" alt_text="check it out" auto_resize="true"]'.$image3['url'].'[/slide]';
				}
		    	$shortcode_text .= '
		    		[/slider]'; 
		    	
				echo '<div class="page_banner">';
				echo do_shortcode($shortcode_text);
				echo '</div>';
				break;
		    }
		    
		}
	//}
	?>

	<div class="diognal-pattern-dummy"></div>
	
<!-- contents -->
<div class="border">


	<!-- page title --> 
	<div class="head_text"><h1><?php echo $category->name;?></h1></div>
	<!-- / page title --> 
    
	<!-- Page navigation-->
	<?php rt_breadcrumb(); ?>
	<!-- /Page navigation-->
 
	<div class="content_background sub">
	<div class="content sub">
	
		<!-- page content -->
		<div id="left" class="left">
     

        
            <?php get_template_part( 'loop', 'archive' );?>
        
            <div class="clear"></div>
        
    
    
		</div>
		<!-- / page contents  -->

 
		<!-- side bar --> 
		<div id="sidebar" class="sidebar"><div class="sidebar_back"><div class="sidebar_back2">
		<?php get_template_part( 'sidebar', 'sidebar_file' );?>
		<div class="clear"></div></div></div></div>
		<!-- / side bar -->
 
    
	<div class="clear"></div>
	</div>
	</div>
</div>
<!-- / contents  -->
    
<?php get_footer();?>