<?php
# 
# rt-theme search 
#
get_header();  
?>

<!-- contents -->
<div class="border">


	<!-- page title --> 
	<div class="head_text"><h2><?php _e('Search Results:','rt_theme');?> <?php echo get_query_var('s'); ?></h2></div>
	<!-- / page title --> 
    
	<!-- Page navigation-->
	<?php rt_breadcrumb(); ?>
	<!-- /Page navigation-->
 
	<div class="content_background sub">
	<div class="content sub">
	
		<!-- page content -->
		<div id="left" class="left">
      
            <?php get_template_part( 'loop', 'archive' );?> 
            <div class="clear"></div> 
    
		</div>
		<!-- / page contents  -->

 
		<!-- side bar --> 
		<div id="sidebar" class="sidebar"><div class="sidebar_back"><div class="sidebar_back2">
		<?php get_template_part( 'sidebar', 'sidebar_file' );?>
		<div class="clear"></div></div></div></div>
		<!-- / side bar -->
 
    
	<div class="clear"></div>
	</div>
	</div>
</div>
<!-- / contents  -->
    
<?php get_footer();?>