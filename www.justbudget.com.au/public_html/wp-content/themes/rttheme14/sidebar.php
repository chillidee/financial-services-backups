<?php
global $term_id,$taxonomy;

#
# displaying sub pages
#

if (get_option('rttheme_show_sub_pages') && is_theme_page()):
    
    if($post){
    if($post->post_parent){
	   $show_sub=true;  
    }  
    
    if(!get_option('rttheme_same_lavel')){
	   $current_post=$post->ID;
	   $depth=1;
    }else{
	   $current_post=$post->post_parent; 
	   $depth=2;
    }

    $children = wp_list_pages("title_li=&child_of=".$current_post."&echo=0&depth=".$depth."");
    }
    
    if (!empty($children)) : 
?>

    <!-- sub navigation -->	
    <div class="sub_navigation">
	   <!-- title-->
	   <h5><?php _e('Sub Menu','rt_theme');?></h5>
	   <!-- sub link-->
	   <ul class="sub_navigation">
		  <?php echo $children; ?>
	   </ul>			
	   <!-- /sub link-->
    </div>
    <!-- / sub navigation -->

    <?php endif;?> 
<?php endif;?>

<?php
#
#	displaying blog categories
#
if (is_blog_page()):?>
    <div class="sub_navigation">
	   <!-- box title-->
	   <h5><?php _e('Blog Categories','rt_theme');?></h5>				
	   <!-- sub link-->
	   <ul class="sub_navigation">
		  <?php wp_list_categories('include='.get_option(THEMESLUG."_blog_ex_cat[]").'&title_li=&orderby=slug');?>			
	   </ul>			
	   <!-- /sub link-->
    </div> 
<?php endif;?>


<?php
#
# call sidebars
#
$createSidebars = new RT_Create_Sidebars();
$createSidebars -> rt_get_sidebar();
?>
