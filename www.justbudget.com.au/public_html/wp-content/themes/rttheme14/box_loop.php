<?php
# 
# rt-theme box loop
#
global $args,$wp_query;

//column
if(is_tax() || is_page()){
	$column 			= 3;
	$box_layout_class	= "three";
	$w				= 188;
}else{
	$column 			= 5;
	$box_layout_class	= "five";
	$w				= 154;
}

$box_counter = 0;

if(is_tax()) $args = array_merge( $wp_query->query, $args);
query_posts($args); 

if ( have_posts() ) : while ( have_posts() ) : the_post();

//get page and post counts
$page_count=get_page_count();  

//product options
$crop	= get_option('rttheme_box_image_crop');	 	// image crop
$h		= get_option('rttheme_box_image_height'); 	// image max height

?>

	<?php
	//box class
	if (fmod($box_counter,$column)==0) {
		$box_class="first";
	}elseif (fmod($box_counter,$column)==$column-1){
		$box_class="last";
	}else{
		$box_class="";
	}
	
	// Crop
	if($crop) $crop="true"; else $h=10000;	
	
	//values
	$title 		=	get_the_title();
	$thumb 		=	get_post_thumbnail_id(); 
	$image 		=	@vt_resize( $thumb, '', $w, $h, ''.$crop.'' );
	$short_desc	=	get_the_content();
	$custom_link = get_post_meta($post->ID, THEMESLUG.'custom_link', true);
	$custom_link_text = get_post_meta($post->ID, THEMESLUG.'custom_link_text', true);
	$permalink	= 	$custom_link;
	
	?>

	<!-- product -->
	<div class="box <?php echo $box_layout_class;?> <?php echo $box_class;?>">
			<?php if($thumb):?>
			<!-- product image -->
			<span class="frame block"><a href="<?php echo $permalink;?>" class="imgeffect link"><img src="<?php echo $image['url'];?>"  alt="" /></a></span>
			<?php endif;?>
			
			<div class="product_info">
				<div class="box_title_desc">
					<!-- title-->
					<h2><a href="<?php echo $permalink;?>" title="<?php echo $title;?>"><?php echo $title;?></a></h2> 				
					<!-- text-->
					<p><?php echo $short_desc;?></p>
				</div>	
			
			<?php
			if ($custom_link && $custom_link_text):
			    echo "<a href=\"". $custom_link ."\" title=\"". $box_title ."\" class=\"small_button\">". $custom_link_text ."</a>";
			endif;
			?>					
		</div>
	</div>
	<!-- / product -->


<?php
	//reset row
	$box_counter++;
	if (fmod($box_counter,$column)==0 || $box_counter==$page_count['post_count']){
		echo "<div class=\"space\"></div>"; 
	}
?>

<?php endwhile?>

<?php
//show pagination if page count bigger then 1
if ($page_count['page_count']>1):
?>
    
	<!-- paging-->
	<div class="paging_wrapper">
		<ul class="paging">
			<?php get_pagination(); ?>
		</ul>
	</div>			
	<!-- / paging-->
    
<?php endif;?>	
<?php endif; wp_reset_query();?>
	 