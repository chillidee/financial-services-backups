<?php get_header();?>

	<!-- slider -->
	<?php if(is_front_page()) get_template_part( 'slider', 'home_slider' );  ?>
	<!-- / slider -->
	
	
	<?php 
	//show page banner slides when it is not blog page
	//if($post->ID != wpml_page_id(BLOGPAGE)){
	
		$thumb 		=	get_post_thumbnail_id(); 
		if(!empty($thumb) && $post->ID != wpml_page_id(BLOGPAGE)){  // - ignore blog posts' featured image
			$image 		=	@vt_resize( $thumb, '', 970, 150, true );
			$shortcode_text = '
				[slider]
				[slide class="first" image_width="970" image_height="150" link="" alt_text="check it out" auto_resize="true"]'.$image['url'].'[/slide]
				';
			
			if (class_exists('MultiPostThumbnails') && MultiPostThumbnails::has_post_thumbnail('page', 'secondary-image')){
				$thumb2 = MultiPostThumbnails::get_post_thumbnail_id('page', 'secondary-image', $post->ID);
				$image2 =	@vt_resize( $thumb2, '', 970, 150, true );
				$shortcode_text .= '[slide image_width="970" image_height="150" link="" alt_text="check it out" auto_resize="true"]'.$image2['url'].'[/slide]';
			}
			if (class_exists('MultiPostThumbnails') && MultiPostThumbnails::has_post_thumbnail('page', 'third-image')){
				$thumb3 = MultiPostThumbnails::get_post_thumbnail_id('page', 'third-image', $post->ID);
				$image3 =	@vt_resize( $thumb3, '', 970, 150, true );
				$shortcode_text .= '
					[slide image_width="970" image_height="150" link="" alt_text="check it out" auto_resize="true"]'.$image3['url'].'[/slide]';
			}
        
			$shortcode_text .= '[/slider]'; 
			
			echo '<div class="page_banner">';
			echo do_shortcode($shortcode_text);
			echo '</div>';
		}else{
			
			$z_page_banners_options_slugs = array("z_page_banners_option_1", "z_page_banners_option_2", "z_page_banners_option_3","z_page_banners_option_4");
			shuffle($z_page_banners_options_slugs); 
			foreach($z_page_banners_options_slugs as $z_page_slug){
				$z_page = get_page_by_path($z_page_slug);
			    if (!$z_page) { continue; }
			    $z_page_id = $z_page->ID;
			    $thumb 		=	get_post_thumbnail_id($z_page_id); 
			    $thumb2 = null; $thumb3 = null;
			    if (class_exists('MultiPostThumbnails') && MultiPostThumbnails::has_post_thumbnail('page', 'secondary-image',$z_page_id)){
					$thumb2 = MultiPostThumbnails::get_post_thumbnail_id('page', 'secondary-image', $z_page_id);
			    }
			    if (class_exists('MultiPostThumbnails') && MultiPostThumbnails::has_post_thumbnail('page', 'third-image',$z_page_id)){
					$thumb3 = MultiPostThumbnails::get_post_thumbnail_id('page', 'third-image', $z_page_id);
			    }
			    if(!empty($thumb) || !empty($thumb2) || !empty($thumb3)){
			    	$shortcode_text = '[slider]';
			    	if(!empty($thumb)){
			    		$image 		=	@vt_resize( $thumb, '', 970, 150, true );
						$shortcode_text .= '
							[slide class="first" image_width="970" image_height="150" link="" alt_text="check it out" auto_resize="true"]'.$image['url'].'[/slide]';
			    	}
					if (!empty($thumb2)){
						$image2 =	@vt_resize( $thumb2, '', 970, 150, true );
						$shortcode_text .= '
							[slide image_width="970" image_height="150" link="" alt_text="check it out" auto_resize="true"]'.$image2['url'].'[/slide]';
					}
					if (!empty($thumb3)){
						$image3 =	@vt_resize( $thumb3, '', 970, 150, true );
						$shortcode_text .= '
							[slide image_width="970" image_height="150" link="" alt_text="check it out" auto_resize="true"]'.$image3['url'].'[/slide]';
					}
			    	$shortcode_text .= '
			    		[/slider]'; 
			    	
					echo '<div class="page_banner">';
					echo do_shortcode($shortcode_text);
					echo '</div>';
					break;
			    }
			    
			}
		}
	//}
	?>

	<div class="diognal-pattern-dummy"></div>
	
<!-- contents -->
<div class="border">

	<!-- page title --> 
	<?php if(!is_front_page()):?>
		<div class="head_text"><h1><?php the_title(); ?></h1></div>
		<!-- / page title --> 
	
		<!-- Page navigation-->
		<?php rt_breadcrumb(); ?>
		<!-- /Page navigation-->
	<?php endif;?>

	<?php
	//Portfolio Categories Popup Menu
	if($post->ID == wpml_page_id(PORTFOLIOTPAGE) && get_option('rttheme_portfolio_popup_categories')){
		$taxonomy 	=	'portfolio_categories';
		$term_slug	=	get_query_var('term');
		$term 		=	get_term_by( 'slug', $term_slug, $taxonomy, 'true', '' );
		$term_id		= 	($term) ? $term->term_id : "" ;
		get_template_part( 'portfolio_categories_menu', 'portfolio_categories_menu_file' );
	}
	?>		

	<?php
	//Portfolio Categories Popup Menu
	if($post->ID == wpml_page_id(PRODUCTPAGE) && get_option('rttheme_product_popup_categories')){
		$taxonomy 	=	'product_categories';
		$term_slug	=	get_query_var('term');
		$term 		=	get_term_by( 'slug', $term_slug, $taxonomy, 'true', '' );
		$term_id		= 	($term) ? $term->term_id : "" ;	
		get_template_part( 'product_categories_menu', 'product_categories_menu_file' );
	}
	?>
	
	<div class="content_background sub">
	<div class="content <?php if($post->ID == wpml_page_id(PORTFOLIOTPAGE)):?>full<?php else:?>sub<?php endif;?>">
	
		<!-- page content -->
		<?php if($post->ID != wpml_page_id(PORTFOLIOTPAGE)):?>
		<div id="left" class="left"> 
		<?php endif;?>
			
		<?php
		#
		#  Regular Page     
		#
		?>
 
			<?php if (have_posts()) : while (have_posts()) : the_post(); ?> 
				<?php the_content(); ?>
				<?php wp_link_pages(); ?>
			<?php endwhile;?>
	    
			<?php else: ?>
				<p><?php _e( 'Sorry, no page found.', 'rt_theme'); ?></p>
			<?php endif; ?>
			

		<?php
		#
		#  Blog Start Page     
		#
		if($post->ID == wpml_page_id(BLOGPAGE)){
			//blog cats
			if(get_option('rttheme_blog_ex_cat[]')){
				$blog_cats=implode(unserialize(get_option('rttheme_blog_ex_cat[]')),",");
			}else{
				$blog_cats="";
			}
			
			
			//Match WPML Categories
			$blog_cats= wpml_lang_object_ids(array($blog_cats),'category');
			if(is_array($blog_cats)) $blog_cats  = implode($blog_cats,',');
		
			if (get_query_var('paged') ) {$paged = get_query_var('paged');} elseif ( get_query_var('page') ) {$paged = get_query_var('page');} else {$paged = 1;}
			$args=array(
				'post_status'	=> 	'publish',
				'orderby'		=> 	'date',
				'order'		=> 	'DESC',
				'paged'		=>	$paged,
				'cat'		=>	$blog_cats 
			);
		
			get_template_part( 'loop', 'archive' );
		}
		?>


		<?php
		#
		#  Portfolio Start Page     
		#
		 
		if($post->ID == wpml_page_id(PORTFOLIOTPAGE) && get_option(THEMESLUG.'_portf_first_page_hide')){ 
	
			if (get_query_var('paged') ) {$paged = get_query_var('paged');} elseif ( get_query_var('page') ) {$paged = get_query_var('page');} else {$paged = 1;}
			$args=array(
			    'post_status'		=>	'publish',
			    'post_type'		=>	'portfolio',
			    'orderby'			=>	get_option('rttheme_portf_list_orderby'),
			    'order'			=>	get_option('rttheme_portf_list_order'),
			    'posts_per_page'	=>	get_option('rttheme_portf_pager'), 
			    'paged'=>$paged
			);

			if(get_option('rttheme_portf_start_cat')){
				
				//Match WPML Categories
				$rttheme_portf_start_cat= wpml_lang_object_ids(array(get_option('rttheme_portf_start_cat')),'portfolio_categories');
				if(is_array($rttheme_portf_start_cat)) $rttheme_portf_start_cat  = implode($rttheme_portf_start_cat,',');
				
				$args2=array( 
					'tax_query' => array(
						array(
							'taxonomy'	=>	'portfolio_categories',
							'field' 		=>	'id',
							'terms' 		=>	$rttheme_portf_start_cat
						)
					)
				);
				$args = array_merge($args, $args2);
			}
	
			get_template_part( 'portfolio_loop', 'portfolio_categories');
		}
		?>


		<?php
		#
		#  Product Start Page     
		#
		
		if($post->ID == wpml_page_id(PRODUCTPAGE) && get_option(THEMESLUG.'_products_first_page_hide')){ 
	
			if (get_query_var('paged') ) {$paged = get_query_var('paged');} elseif ( get_query_var('page') ) {$paged = get_query_var('page');} else {$paged = 1;}
			$args=array(
			    'post_status'		=>	'publish',
			    'post_type'		=>	'products',
			    'orderby'			=>	get_option('rttheme_product_list_orderby'),
			    'order'			=>	get_option('rttheme_product_list_order'),
			    'posts_per_page'	=>	get_option('rttheme_product_list_pager'), 
			    'paged'=>$paged
			);

			if(get_option('rttheme_product_start_cat')){
			
				//Match WPML Categories
				$rttheme_product_start_cat= wpml_lang_object_ids(array(get_option('rttheme_product_start_cat')),'product_categories');
				if(is_array($rttheme_product_start_cat)) $rttheme_product_start_cat  = implode($rttheme_product_start_cat,',');
				
				$args2=array( 
					'tax_query' => array(
						array(
							'taxonomy'	=>	'product_categories',
							'field' 		=>	'id',
							'terms' 		=>	$rttheme_product_start_cat
						)
					)
				);
				$args = array_merge($args, $args2);
			}
	
			get_template_part( 'product_loop', 'product_categories');
		}
		?>
		
		<?php
		#
		#  Credit Restoration List Page     
		#
		
		if($post->ID == wpml_page_id(CREDITRESTORATIONPAGE)){ 
	
			if (get_query_var('paged') ) {$paged = get_query_var('paged');} elseif ( get_query_var('page') ) {$paged = get_query_var('page');} else {$paged = 1;}
			$args=array(
			    'post_status'		=>	'publish',
			    'post_type'		=>	'box',
			    'orderby'			=>	get_option('rttheme_box_list_orderby'),
			    'order'			=>	get_option('rttheme_box_list_order'),
			    'posts_per_page'	=>	get_option('rttheme_box_list_pager'), 
			    'paged'=>$paged
			);

			if(get_option('rttheme_credit_restoration_cat')){
			
				//Match WPML Categories
				$rttheme_credit_restoration_cat = wpml_lang_object_ids(array(get_option('rttheme_credit_restoration_cat')),'box_categories');
				if(is_array($rttheme_credit_restoration_cat)) $rttheme_credit_restoration_cat  = implode($rttheme_credit_restoration_cat,',');
				
				$args2=array( 
					'tax_query' => array(
						array(
							'taxonomy'	=>	'box_categories',
							'field' 		=>	'id',
							'terms' 		=>	$rttheme_credit_restoration_cat
						)
					)
				);
				$args = array_merge($args, $args2); 
			}
	
			get_template_part( 'box_loop', 'box_categories');
		}
		?>
		
		<?php
		#
		#  Useful Information List Page     
		#
		
		if($post->ID == wpml_page_id(USEFULINFORMATIONPAGE)){ 
	
			if (get_query_var('paged') ) {$paged = get_query_var('paged');} elseif ( get_query_var('page') ) {$paged = get_query_var('page');} else {$paged = 1;}
			$args=array(
			    'post_status'		=>	'publish',
			    'post_type'		=>	'box',
			    'orderby'			=>	get_option('rttheme_box_list_orderby'),
			    'order'			=>	get_option('rttheme_box_list_order'),
			    'posts_per_page'	=>	get_option('rttheme_box_list_pager'), 
			    'paged'=>$paged
			);

			if(get_option('rttheme_useful_information_cat')){
			
				//Match WPML Categories
				$rttheme_useful_information_cat = wpml_lang_object_ids(array(get_option('rttheme_useful_information_cat')),'box_categories');
				if(is_array($rttheme_useful_information_cat)) $rttheme_useful_information_cat  = implode($rttheme_useful_information_cat,',');
				
				$args2=array( 
					'tax_query' => array(
						array(
							'taxonomy'	=>	'box_categories',
							'field' 		=>	'id',
							'terms' 		=>	$rttheme_useful_information_cat
						)
					)
				);
				$args = array_merge($args, $args2); 
			}
	
			get_template_part( 'box_loop', 'box_categories');
		}
		?>
		
		<?php
		#
		#  Affiliate Centre List Page     
		#
		
		if($post->ID == wpml_page_id(AFFILIATECENTREPAGE)){ 
	
			if (get_query_var('paged') ) {$paged = get_query_var('paged');} elseif ( get_query_var('page') ) {$paged = get_query_var('page');} else {$paged = 1;}
			$args=array(
			    'post_status'		=>	'publish',
			    'post_type'		=>	'box',
			    'orderby'			=>	get_option('rttheme_box_list_orderby'),
			    'order'			=>	get_option('rttheme_box_list_order'),
			    'posts_per_page'	=>	get_option('rttheme_box_list_pager'), 
			    'paged'=>$paged
			);

			if(get_option('rttheme_affiliate_program_cat')){
			
				//Match WPML Categories
				$rttheme_affiliate_program_cat = wpml_lang_object_ids(array(get_option('rttheme_affiliate_program_cat')),'box_categories');
				if(is_array($rttheme_affiliate_program_cat)) $rttheme_affiliate_program_cat  = implode($rttheme_affiliate_program_cat,',');
				
				$args2=array( 
					'tax_query' => array(
						array(
							'taxonomy'	=>	'box_categories',
							'field' 		=>	'id',
							'terms' 		=>	$rttheme_affiliate_program_cat
						)
					)
				);
				$args = array_merge($args, $args2); 
			}
	
			get_template_part( 'box_loop', 'box_categories');
		}
		?>
		
		<?php
		#
		#  Why Choose Us List Page     
		#
		
		if($post->ID == wpml_page_id(WHYCHOOSEUSPAGE)){ 
	
			if (get_query_var('paged') ) {$paged = get_query_var('paged');} elseif ( get_query_var('page') ) {$paged = get_query_var('page');} else {$paged = 1;}
			$args=array(
			    'post_status'		=>	'publish',
			    'post_type'		=>	'box',
			    'orderby'			=>	get_option('rttheme_box_list_orderby'),
			    'order'			=>	get_option('rttheme_box_list_order'),
			    'posts_per_page'	=>	get_option('rttheme_box_list_pager'), 
			    'paged'=>$paged
			);

			if(get_option('rttheme_why_choose_us_cat')){
			
				//Match WPML Categories
				$rttheme_why_choose_us_cat = wpml_lang_object_ids(array(get_option('rttheme_why_choose_us_cat')),'box_categories');
				if(is_array($rttheme_why_choose_us_cat)) $rttheme_why_choose_us_cat = implode($rttheme_why_choose_us_cat,',');
				
				$args2=array( 
					'tax_query' => array(
						array(
							'taxonomy'	=>	'box_categories',
							'field' 		=>	'id',
							'terms' 		=>	$rttheme_why_choose_us_cat
						)
					)
				);
				$args = array_merge($args, $args2); 
			}
	
			get_template_part( 'box_loop', 'box_categories');
		}
		?>
		
		<?php
		#
		#  Affiliate Program Page 
		#
		
		if($post->ID == wpml_page_id(AFFILIATEPROGRAMPAGE)){ 
			echo '<script type="text/javascript" src="https://forms.moon-ray.com/v2.4/include/minify/?g=moonrayJS"></script>';
			echo '<link rel="stylesheet" type="text/css" href="https://forms.moon-ray.com/v2.4/include/minify/?g=moonrayCSS">';
			echo '<script type="text/javascript" src="https://www1.moon-ray.com/v2.4/analytics/genf.php?f=p2c5208f3"></script>';
		}
		?>
		
		<!-- AddThis Button BEGIN -->
		<div class="addthis_toolbox addthis_default_style ">
		<a class="addthis_button_facebook_like" fb:like:layout="button_count"></a>
		<a class="addthis_button_tweet"></a>
		<a class="addthis_button_pinterest_pinit"></a>
		<a class="addthis_counter addthis_pill_style"></a>
		</div>
		<script type="text/javascript">var addthis_config = {"data_track_addressbar":true};</script>
		<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5202f1f67cede230"></script>
		<!-- AddThis Button END -->
		
		<?php if($post->ID != wpml_page_id(PORTFOLIOTPAGE)):?></div><?php endif;?>
		<!-- / page contents  -->

		<?php if($post->ID != wpml_page_id(PORTFOLIOTPAGE)):?>
		<!-- side bar --> 
		<div id="sidebar" class="sidebar"><div class="sidebar_back"><div class="sidebar_back2">
		<?php get_template_part( 'sidebar', 'sidebar_file' );?>
		<div class="clear"></div></div></div></div>
		<!-- / side bar -->
		<?php endif;?>

    
	<div class="clear"></div>
	</div>
	</div>
</div>
<!-- / contents  -->
		
    
<?php get_footer();?>