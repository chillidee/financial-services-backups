<?php
# 
# rt-theme portfolio categories drop down menu
#

global $term_id,$taxonomy;

$args = array(
    'orderby'            => 'name',
    'order'              => 'ASC',
    'show_last_update'   => 0,
    'style'              => 'list',
    'show_count'         => 0,
    'hide_empty'         => 0,
    'use_desc_for_title' => 0,
    'child_of'           => 0, 
    'hierarchical'       => true,
    'title_li'           => __( '' ,'rt_theme'),
    'number'             => NULL,
    'echo'               => 1,
    'depth'              => 0,
    'current_category'   => $term_id,
    'pad_counts'         => 0,
    'taxonomy'           => $taxonomy
); 
?>

<!-- Categories -->
	<!-- close button -->
	<div class="portfolio_categories">
		<span class="m_title"><?php _e("Product Categories",'rt_theme');?></span><!-- button name -->
	</div>
	<!-- / close button -->
	
	<!-- open button -->
	<div class="portfolio_categories_open">
		<img src="<?php echo THEMEURI;?>/images/categories_top.png" alt="" /><div class="back">
		<span class="m_title"><?php _e("Product Categories",'rt_theme');?></span><!-- button name -->
			<ul>
				<?php
				    wp_list_categories( $args );
				?>	
			</ul>
		</div><img src="<?php echo THEMEURI;?>/images/categories_bottom.png" alt="" />
	</div>
	<!-- / open button -->
<!-- / Categories -->		