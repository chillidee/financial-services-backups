<?php
# enable session
if (!session_id())
    session_start();

# check referer URL
/*if(stristr($_SERVER['HTTP_REFERER'], 'ccdr') === FALSE){
	die('You are not allowed to use this form directly. Please use Enquiry Form on the NSWMC website.');   
} */

$your_email=trim($_POST['your_email']);
$your_web_site_name=trim($_POST['your_web_site_name']);
?>

<?php 
//If the form is submitted
if(isset($_POST['firstname'])) {

	$errorMessage = "";

	//Check to make sure that the name field is not empty
	if(trim($_POST['firstname']) === '' || trim($_POST['lastname']) === '') {
		$hasError = true;
		$errorMessage .= "First Name and Last Name can't be empty. ";
	} else {
		$firstname = trim($_POST['firstname']);
		$lastname = trim($_POST['lastname']);
	}

	//contact
	if(isset($_POST['homenumber'])) $homenumber = trim($_POST['homenumber_areacode'])." ".trim($_POST['homenumber']);
	if(isset($_POST['worknumber'])) $worknumber = trim($_POST['worknumber_areacode'])." ".trim($_POST['worknumber']);
	if(isset($_POST['mobilenumber'])) $mobilenumber = trim($_POST['mobilenumber']);
	if(isset($_POST['email'])) $email = trim($_POST['email']);

	//suburb
	if(isset($_POST['suburb'])) $suburb = trim($_POST['suburb']);

	//state
	if(isset($_POST['state'])) $state = trim($_POST['state']);		

	//postcode
	if(isset($_POST['postcode'])) $postcode = trim($_POST['postcode']);	

	//type of loan
	if(isset($_POST['typeofloan'])) $typeofloan = trim($_POST['typeofloan']); else $typeofloan = "";		 
	if(isset($_POST['loanamount'])) $loanamount = trim($_POST['loanamount']); else $loanamount = 0;	
	if(isset($_POST['youown'])) $youown = trim($_POST['youown']);	
	if(isset($_POST['propertyvalue'])) $propertyvalue = trim($_POST['propertyvalue']);
	if(isset($_POST['balanceowing'])) $balanceowing = trim($_POST['balanceowing']);
	
	//how hear about us
	if(isset($_POST['hearaboutus'])) $hearaboutus = trim($_POST['hearaboutus']);	
	
	//Check to make sure comments were entered	
	if(trim($_POST['comment']) === '') {
		$hasError = true;
		$errorMessage .= "Comment can't be empty. ";
	} else {
		if(function_exists('stripslashes')) {
			$comment = stripslashes(trim($_POST['comment']));
		} else {
			$comment = trim($_POST['comment']);
		}
	}

	//If there is no error, save into MoneyMaker database or send the email
	if(!isset($hasError)) {
		$saved_in_moneymaker = false;
		$saved_in_momeymaker_error_message = "";
		
		//save to Money Maker database first
		require './moneymaker/config.php';
		//open connection to moneymaker database
		$conn = mssql_connect($mssql_server, $mssql_user, $mssql_pwd);

		if( $conn === false ){
			$saved_in_momeymaker_error_message .= "Could not connect to MoneyMaker database. Please check server connections.\n";
			$saved_in_momeymaker_error_message .= mssql_get_last_message();
		}else{
			mssql_select_db('australianlendingcentre', $conn);
			
			$mm_appDate = date('Y-m-d H:i:s');
			$mm_loanAmmount = $loanamount;
			$mm_debtConsolidation = 0;
			$mm_clientIP = $_SERVER['REMOTE_ADDR'];
			$mm_Asset = $youown;
			$mm_comments = str_replace("'", "''", $comment);
			$mm_url = isset($_SESSION['mm_referalurl']) ? $_SESSION['mm_referalurl'] : "";
			$mm_isCompleted = 0;
			$mm_LoanType = $typeofloan;
			$mm_searchTerm = isset($_SESSION['mm_keywords']) ? str_replace("'", "''", $_SESSION['mm_keywords']) : "";
			$mm_referData = '';
			$mm_referralType = $hearaboutus;
			$mm_Is8am11am = 0;
			$mm_Contact8am11amAt = 'Contact8am11amAt';
			$mm_Is11am2pm = 0;
			$mm_Contact11am2pmAt = 'Contact11am2pmAt';
			$mm_Is2pm5pm = 0;
			$mm_Contact2pm5pmAt = 'Contact2pm5pmAt';
			$mm_Is5pm8pm = 0;
			$mm_Contact5pm8pmAt = 'Contact5pm8pmAt';
			$mm_middlename = '654321';
			$mm_adwordskeyword = isset($_SESSION['mm_keywords']) ? str_replace("'", "''", $_SESSION['mm_keywords']) : "";
			$mm_debtadgroup = '';
			$mm_homeNum = $homenumber;
			$mm_workNum = $worknumber;
			$mm_mobileNum = $mobilenumber;
			$mm_emailAddr = str_replace("'", "''", $email);
			$mm_postcode = $postcode;
			$mm_suburb = str_replace("'", "''", $suburb);
			$mm_state = $state;
			$mm_firstName = str_replace("'", "''", $firstname);
			$mm_lastName = str_replace("'", "''", $lastname);
			$mm_balanceOwing = isset($balanceowing) ? $balanceowing : '0';
			$mm_realEstate = isset($propertyvalue) ? $propertyvalue : '0'; // ($youown == 1) ? 'Yes' : 'No';
			$mm_website = 'CCDR';
			
			$sql = "execute addEnquiry 
					@appDate =  '{$mm_appDate}',
					@loanAmmount =  '{$mm_loanAmmount}',
					@debtConsolidation =  '{$mm_debtConsolidation}',
					@clientIP =  '{$mm_clientIP}',
					@Asset =  '{$mm_Asset}',
					@comments =  '{$mm_comments}',
					@url =  '{$mm_url}',
					@isCompleted =  '{$mm_isCompleted}',
					@LoanType =  '{$mm_LoanType}',
					@searchTerm =  '{$mm_searchTerm}',
					@referData =  '{$mm_referData}',
					@referralType =  '{$mm_referralType}',
					@Is8am11am =  '{$mm_Is8am11am}',
					@Contact8am11amAt =  '{$mm_Contact8am11amAt}',
					@Is11am2pm =  '{$mm_Is11am2pm}',
					@Contact11am2pmAt =  '{$mm_Contact11am2pmAt}',
					@Is2pm5pm =  '{$mm_Is2pm5pm}',
					@Contact2pm5pmAt =  '{$mm_Contact2pm5pmAt}',
					@Is5pm8pm =  '{$mm_Is5pm8pm}',
					@Contact5pm8pmAt =  '{$mm_Contact5pm8pmAt}',
					@middlename =  '{$mm_middlename}',
					@adwordskeyword =  '{$mm_adwordskeyword}',
					@debtadgroup =  '{$mm_debtadgroup}',
					@homeNum =  '{$mm_homeNum}',
					@workNum =  '{$mm_workNum}',
					@mobileNum =  '{$mm_mobileNum}',
					@emailAddr =  '{$mm_emailAddr}',
					@postcode =  '{$mm_postcode}',
					@suburb =  '{$mm_suburb}',
					@state =  '{$mm_state}',
					@firstName =  '{$mm_firstName}',
					@lastName =  '{$mm_lastName}',
					@balanceOwing =  '{$mm_balanceOwing}',
					@realEstate =  '{$mm_realEstate}',
					@website = '{$mm_website}'
					";

			//echo $sql; 
				
			$outcome = mssql_query($sql, $conn);
			
			if($outcome){
				// going ok
				$saved_in_moneymaker = true;
				$emailSent = true;
			
				/*
				$subject = "NSWMC Form Successful Debug";
				$body = print_r($outcome,true)."\n\n";
				while($results = mssql_fetch_array($outcome)){
					$body .= print_r($results, true)."\n\n";
				}
				mail('xchen@opq.com.au', $subject, $body);
				*/
			}else{
				$saved_in_momeymaker_error_message .= "SQL Statement could not be executed.Check SQL matching with SQL Table fields. \n";
				$saved_in_momeymaker_error_message .= mssql_get_last_message();

				//more debug messages
				$saved_in_momeymaker_error_message .= "\nMore debug messages:\n";
				$saved_in_momeymaker_error_message .= "\n  mm_appDate: ".$mm_appDate;
				$saved_in_momeymaker_error_message .= "\n  mm_loanAmmount: ".$mm_loanAmmount;
				$saved_in_momeymaker_error_message .= "\n  mm_debtConsolidation: ".$mm_debtConsolidation;
				$saved_in_momeymaker_error_message .= "\n  mm_clientIP: ".$mm_clientIP;
				$saved_in_momeymaker_error_message .= "\n  mm_Asset: ".$mm_Asset;
				$saved_in_momeymaker_error_message .= "\n  mm_comments: ".$mm_comments;
				$saved_in_momeymaker_error_message .= "\n  mm_url: ".$mm_url;
				$saved_in_momeymaker_error_message .= "\n  mm_isCompleted: ".$mm_isCompleted;
				$saved_in_momeymaker_error_message .= "\n  mm_LoanType: ".$mm_LoanType;
				$saved_in_momeymaker_error_message .= "\n  mm_searchTerm: ".$mm_searchTerm;
				$saved_in_momeymaker_error_message .= "\n  mm_referData: ".$mm_referData;
				$saved_in_momeymaker_error_message .= "\n  mm_referralType: ".$mm_referralType;
				$saved_in_momeymaker_error_message .= "\n  mm_Is8am11am: ".$mm_Is8am11am;
				$saved_in_momeymaker_error_message .= "\n  mm_Contact8am11amAt: ".$mm_Contact8am11amAt;
				$saved_in_momeymaker_error_message .= "\n  mm_Is11am2pm: ".$mm_Is11am2pm;
				$saved_in_momeymaker_error_message .= "\n  mm_Contact11am2pmAt: ".$mm_Contact11am2pmAt;
				$saved_in_momeymaker_error_message .= "\n  mm_Is2pm5pm: ".$mm_Is2pm5pm;
				$saved_in_momeymaker_error_message .= "\n  mm_Contact2pm5pmAt: ".$mm_Contact2pm5pmAt;
				$saved_in_momeymaker_error_message .= "\n  mm_Is5pm8pm: ".$mm_Is5pm8pm;
				$saved_in_momeymaker_error_message .= "\n  mm_Contact5pm8pmAt: ".$mm_Contact5pm8pmAt;
				$saved_in_momeymaker_error_message .= "\n  mm_middlename: ".$mm_middlename;
				$saved_in_momeymaker_error_message .= "\n  mm_adwordskeyword: ".$mm_adwordskeyword;
				$saved_in_momeymaker_error_message .= "\n  mm_debtadgroup: ".$$mm_debtadgroup;
				$saved_in_momeymaker_error_message .= "\n  mm_homeNum: ".$mm_homeNum;
				$saved_in_momeymaker_error_message .= "\n  mm_workNum: ".$mm_workNum;
				$saved_in_momeymaker_error_message .= "\n  mm_mobileNum: ".$mm_mobileNum;
				$saved_in_momeymaker_error_message .= "\n  mm_emailAddr: ".$mm_emailAddr;
				$saved_in_momeymaker_error_message .= "\n  mm_postcode: ".$mm_postcode;
				$saved_in_momeymaker_error_message .= "\n  mm_suburb: ".$mm_suburb;
				$saved_in_momeymaker_error_message .= "\n  mm_state: ".$mm_state;
				$saved_in_momeymaker_error_message .= "\n  mm_firstName: ".$mm_firstName;
				$saved_in_momeymaker_error_message .= "\n  mm_lastName: ".$mm_lastName;
				$saved_in_momeymaker_error_message .= "\n  mm_balanceOwing: ".$mm_balanceOwing;
				$saved_in_momeymaker_error_message .= "\n  mm_realEstate: ".$mm_realEstate;
				$saved_in_momeymaker_error_message .= "\n  mm_website: ".$mm_website;
				$saved_in_momeymaker_error_message .= "\n\n";
			}
		}
			
		//if didn't save into MoneyMaker database, we send an email instead with error messages
		if(!$saved_in_moneymaker){
			$emailTo = $your_email;
			$subject = 'Express Enquiry Form Submission from '.$firstname.' '.$lastname;

			//message body 
			$body = "I am having trouble saving the enquiry data into MoneyMaker database. Error as below. \n\n";
			$body .= $saved_in_momeymaker_error_message."\n\n";

			$headers = 'From: CCDR <'.$emailTo.'>' . "\r\n" . 'Reply-To: ' . $email;

			mail($emailTo, $subject, $body, $headers); 

			$emailSent = true;
		}
	}

} 

?>



<?php if(isset($emailSent) == true) { ?>
	<!-- use Ajax javascript to redirect to Enquiry is Complete page, so not output OK result here 
	<div class="ok_box">
		<h3><?php //echo $_POST['text_1'];?>, <?php //echo $firstname;?></h3>
		<p><?php //echo $_POST['text_2'];?></p>
	</div> -->
<?php } ?>

<?php if(isset($hasError) ) { ?>
	<div class="error_box">
		<?php echo $_POST['text_3'];?>
		<br />
		<?php echo $errorMessage;?>
	</div>
<?php } ?> 