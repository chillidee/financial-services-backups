<?php
$your_email=trim($_POST['your_email']);
$your_web_site_name=trim($_POST['your_web_site_name']);
?>

<?php 
//If the form is submitted
if(isset($_POST['fullname'])) {
 	
		$errorMessage = "";
		
		//Check to make sure that the name & email field is not empty
		if(trim($_POST['fullname']) === '' || trim($_POST['freeemail']) === '') {
			$hasError = true;
			$errorMessage .= "Please fill your full name and your email.";
		} else {
			$fullname = trim($_POST['fullname']);
			$email = trim($_POST['freeemail']);
		}
		
		//If there is no error, send the email
		if(!isset($hasError)) {

			$emailTo = $email;
			$subject = 'Free Report from Clean Credit';
			
			//message body 
			$body  ="Dear $fullname,\r\n\r\n";
			$body .="Your Email: $email \r\n\r\n";
			$body .="As requested, we have delivery you the free report as below. \r\n\r\n";
			
			//load wordpress so we can use wordpress functions
			require_once("../../../wp-load.php");
			
			//get content from page - free_sample_report
			$free_sample_report_slug = "z_free_sample_report";
			$free_sample_report_page = get_page_by_path($free_sample_report_slug);
		    if (!$free_sample_report_page) { 
				$hasError = true;
				$errorMessage .= "Sample Report Missing..Please make sure there is a page with slug: z_free_sample_report";
		    }else{
		    	$free_sample_report_page_id = $free_sample_report_page->ID;
		    	$page_data = get_page( $free_sample_report_page_id );
		    	$content = apply_filters('the_content', $page_data->post_content);
		    	$body .= $content;
		    	
		   		//show the pdf or word download button for this report
				$attachments = get_children( array(
				        'post_type' => 'attachment',
				        'post_mime_type' => array('application/doc','application/pdf'),
				        'numberposts' => 1,
				        'post_status' => null,
				        'post_parent' => $free_sample_report_page_id
				));
				foreach($attachments as $attachment){
					$pdf_file_path = get_attached_file( $attachment->ID );
					break;
				}
		    	
		    	require_once 'rt-framework/swift/lib/swift_required.php';
				//Create the message
				$message = Swift_Message::newInstance()
				  			->setSubject($subject)
							->setFrom(array($your_email => $your_web_site_name))
							->setTo(array($emailTo))
							->setBcc(array($your_email))
							->setBody($body)
				  			//And optionally an alternative body
				  			->addPart(str_replace("\r\n", "<br/>", $body), 'text/html');
				
				if(!empty($pdf_file_path)){
					$message->attach(Swift_Attachment::fromPath($pdf_file_path));
				}
				
				$transport = Swift_MailTransport::newInstance();
				$mailer = Swift_Mailer::newInstance($transport);
				$emailSent = $mailer->send($message);
		    }
		}
} 
?>

<?php if(isset($emailSent) == true) { ?>
	<div class="ok_box">
		<p><?php echo $_POST['text_2'];?></p>
	</div>
<?php } ?>

<?php if(isset($hasError) ) { ?>
	<div class="error_box">
		<?php //echo $_POST['text_3'];?>
		<?php echo $errorMessage;?>
	</div>
<?php } ?> 