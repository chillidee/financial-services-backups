<?php
# 
# rt-theme product detail page
#

//flush rewrite rules
add_action('init', 'flush_rewrite_rules');

//taxonomy
$taxonomy = 'product_categories';

//page link
$link_page = get_permalink(get_option('rttheme_product_list'));

//category link
$terms = get_the_terms($post->ID, $taxonomy);
$i=0;
if($terms){
	foreach ($terms as $taxindex => $taxitem) {
	if($i==0){
		$link_cat=get_term_link($taxitem->slug,$taxonomy);
		$term_slug = $taxitem->slug;
		$term_id = $taxitem->term_id;
		}
	$i++;
	}
}

//product options
$crop = get_option('rttheme_product_image_crop');	 	// image crop

get_header();
?>


<!-- contents -->
<div class="border">

	<!-- page title --> 
	<div class="head_text"><h2><?php the_title(); ?></h2></div>
	<!-- / page title --> 
    
	<!-- Page navigation-->
	<?php rt_breadcrumb(); ?>
	<!-- /Page navigation-->


	<?php
	//Portfolio Categories Popup Menu
	if(get_option('rttheme_product_popup_categories')){	
		get_template_part( 'product_categories_menu', 'product_categories_menu_file' );
	}
	?>
	
		<div class="content_background sub">
		<div class="content full">


			<?php
			if (have_posts()) : while (have_posts()) : the_post();
			
				//values
				$rt_other_images 	= trim(get_post_meta($post->ID, 'rtthemeother_images', true));
				$rt_chart_file_url  = get_post_meta($post->ID, 'rtthemechart_file_url', true);
				$rt_excel_file_url  = get_post_meta($post->ID, 'rtthemeexcel_file_url', true);
				$rt_pdf_file_url  	= get_post_meta($post->ID, 'rtthemepdf_file_url', true);
				$rt_word_file_url  	= get_post_meta($post->ID, 'rtthemeword_file_url', true);
				$content			= wpautop(do_shortcode(get_the_content()));
				$title			= get_the_title();
				$order_button		= get_post_meta($post->ID, THEMESLUG.'order_button', true);
				$order_button_text	= get_post_meta($post->ID, THEMESLUG.'order_button_text', true);
				$order_button_link	= get_post_meta($post->ID, THEMESLUG.'order_button_link', true);
				$related_products	= get_post_meta($post->ID, THEMESLUG.'related_products[]', true);
 
				//free tabs count
				$tab_count=3;
				for($i=0; $i<$tab_count+1; $i++){
				    if (trim(get_post_meta($post->ID, THEMESLUG.'free_tab_'.$i.'_title', true)))  $tabbed_page="yes";
				}
			?>
 

			<!-- PRODUCT TABS COLUMN -->
			<div class="box two-three first">

				<!-- TABS WRAP -->				
				<?php if(@$tabbed_page):?>
				<div class="taps_wrap">
				    <!-- the tabs -->
				    <ul class="tabs">
						<?php if($content):?><li><a href="#"><?php _e('General Details','rt_theme');?></a></li><?php endif;?>
						<?php
						#
						#	Free Tabs
						#	
						for($i=0; $i<$tab_count+1; $i++){ 
							if (trim(get_post_meta($post->ID, THEMESLUG.'free_tab_'.$i.'_title', true))){
								echo '<li><a href="#">'.get_post_meta($post->ID, THEMESLUG.'free_tab_'.$i.'_title', true).'</a></li>';
							}
						}
						?>
				    </ul>
				<?php endif;?>
				
				<?php if($content):?>								
				<!-- General Details -->
				
				<?php if(@$tabbed_page):?><div class="pane"><?php else:?><div class="box"><?php endif;?> 
					<div>
					<?php
					$content  = str_replace("two-three", "two_three", $content);
					$content  = str_replace("three-four", "three_four", $content);
					$content  = str_replace("four-five", "four_five", $content);					
					echo $content;
					?></div>
					<div class="clear"></div>
				</div>
				<?php endif;?>

				<?php
				#
				#	Free Tabs' Content
				#	
				for($i=0; $i<$tab_count+1; $i++){ 
					if (trim(get_post_meta($post->ID, THEMESLUG.'free_tab_'.$i.'_title', true))){
						echo '<div class="pane">'.wpautop(do_shortcode(get_post_meta($post->ID, THEMESLUG.'free_tab_'.$i.'_content', true))).'<div class="clear"></div></div>';
					}
				}
				?>
				
				<?php if(@$tabbed_page):?>        
				</div><div class="clear"></div>
				<?php endif;?>
				
			</div>
			<!-- / PRODUCT TABS COLUMN -->
			
			
			
			<!-- RIGHT COLUMN -->
			<div class="box three last">
			
				<?php
				//photos
				
				//default photo
				if(get_post_meta($post->ID, THEMESLUG.'product_image_url', true)):
				    $default_photo  = get_post_meta($post->ID, THEMESLUG.'product_image_url', true);
				    $total_photo 	= 1;
				endif;
				
				
				//other photos
				if(trim($rt_other_images)):
				    $other_photos 	= trim(preg_replace("/(^[\r\n]*|[\r\n]+)[\s\t]*[\r\n]+/", "\n", $rt_other_images));  
				    $total_photo	= $total_photo + count( explode("\n", $other_photos) );
				endif;
				
				//merge all
				$product_photos=@$default_photo ."\n".@$other_photos;
							
				?>
				
				<?php if (trim($product_photos)):?>
					<!-- product image slider -->
					<div class="product_sliderss">
						<div class="frame block"> 
							<div class="product_slider"> 
								<?php  
								//Product Photos 
								$product_photos_split=explode("\n", $product_photos);
							
								foreach ($product_photos_split as &$photo_url) {
								
									if($photo_url){
										//resize the photo 
										$w	= 285;
										$h	= 200;
										
										// Crop
										if($crop) $crop="true"; else $h=10000;
										
										$image_thumb 	= @vt_resize( '', $photo_url, $w, $h, ''.$crop.'' );
										?>
										<div><a href="<?php echo $photo_url; ?>" title="" rel="prettyPhoto[product]" class="imgeffect plus"><img src="<?php echo $image_thumb['url'];?>" width="<?php echo $image_thumb['width'];?>" height="<?php echo $image_thumb['height'];?>" alt="" /></a> </div>
										<?php
										
										@$photo_count++;
									}
								}
								?>
							</div>
						</div>
					</div>
					<!-- slider navigation buttons -->
					<div class="slider_buttons"></div>
					<!-- / product image slider -->
				<?php endif;?>
				
				<?php if($order_button):?>
				<!-- order button -->
				<a href="<?php echo $order_button_link;?>" class="order_button"><?php if($order_button_text): echo $order_button_text; else: _e('order enquiry form →','rt_theme'); endif; ?></a>
				<!-- / order button -->
				<?php endif;?>
				
				<?php if($rt_chart_file_url || $rt_excel_file_url || $rt_pdf_file_url || $rt_word_file_url ):?>
				<!-- document icons -->
				<ul class="doc_icons frame">
					<?php if($rt_chart_file_url):?><li><a href="<?php echo $rt_chart_file_url; ?>"><img src="<?php echo THEMEURI; ?>/images/assets/icons/Chart_1.png" alt="<?php _e('Donwload Charts','rt_theme');?>" class="png" /></a></li><?php endif;?>
					<?php if($rt_excel_file_url):?><li><a href="<?php echo $rt_excel_file_url; ?>"><img src="<?php echo THEMEURI; ?>/images/assets/icons/File_Excel.png" alt="<?php _e('Download Excel File','rt_theme');?>" class="png" /></a></li><?php endif;?>
					<?php if($rt_pdf_file_url):?><li><a href="<?php echo $rt_pdf_file_url; ?>"><img src="<?php echo THEMEURI; ?>/images/assets/icons/File_Pdf.png" alt="<?php _e('Download PDF File','rt_theme');?>" class="png" /></a></li><?php endif;?>
					<?php if($rt_word_file_url):?><li><a href="<?php echo $rt_word_file_url; ?>"><img src="<?php echo THEMEURI; ?>/images/assets/icons/Word.png" alt="<?php _e('Download Word File','rt_theme');?>" class="png" /></a></li><?php endif;?>
				</ul>
				<!-- document icons -->
				<?php endif;?>
				
				<div class="clear"></div>
				
				<!-- side bar -->
				<?php get_template_part( 'sidebar', 'sidebar_file' );?>
				<!-- / side bar -->
				
				<div class="clear"></div>
			</div>
			<!-- / RIGHT COLUMN -->
		
			<div class="clear"></div>
			<!-- / content --> 		
			
			<!-- RELATED PRODUCTS -->
	
				<?php
				if($related_products){
					echo '<div class="line"></div><div class="related_products cufon">'.__("Related Products",'rt_theme').'</div>';		
					//taxonomy 
					$args=array(
					'post_type'=> 'products', 
					'post_status'=> 'publish',
					'orderby'=> 'menu_order', 
					'ignore_sticky_posts'=>1,
					'posts_per_page'	=>	1000, 
					'post__in' =>	$related_products
					);
				    get_template_part( 'product_loop', 'product_categories' );
				}
				?>
	
			<!-- / RELATED PRODUCTS -->


			<?php endwhile;?>
			
			<?php else: ?>
				<p><?php _e( 'Sorry, no page found.', 'rt_theme'); ?></p>
			<?php endif; ?>
	
	
		<div class="clear"></div>		
	</div>
	</div>
</div>
<!-- / contents  --> 
  
<?php get_footer();?>