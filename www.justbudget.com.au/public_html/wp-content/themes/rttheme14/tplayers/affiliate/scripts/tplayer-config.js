//****** The Transparent Player config file version 3.24.00 ******

//start do not edit
(function(){
var f=DFS.adv;
f.configReady=true;
f.hostname = window.location.hostname;
//end do not edit



//Global:
f.path_to_templates='';
					//optional path to the templates folder when remote hosting or using in Nanacast
					//if defined this will be used instead of default in templates folder
					//form: 'http://'+f.hostname+'/PATH_TO_TEMPLATES_FOLDER/'
f.titleColor = '#119889';//title color
f.titleTextColor = '#fd9500';//title text color
f.videoScrollRate = 10;//set the rate of scroll from one anchor to the next, 1-10 where 1 is slowest
f.closeOneCloseAll = false;//closing one layer with close button or autoClose will close all other layers
f.borderDisplay = false;	//Specify if a border is shown around the video
f.initialVolume = .6;	//set the initial volume 0-1 for tplayer instances
f.initialAudio = true;		//set audio on or off initially
f.pauseImage = '../images/pause3.png';	//path to pause image, if none specified none is used
f.bufferImage = '../images/buffer1.gif';	//path to buffering image, if none specified none is used
f.totalOverlay = false;	//when false any start image used will be allow the control bar to be visible
f.useMetaData = true;	//when true the video dimensions will be as encoded, when false the video will be as dimensions in config



//template tvideo1 with system unique-id tplayervideo1 local vars:
f.tvideo1=[];f.tvideo1[0]={};   //do not edit
f.tvideo1[0].flvmp4 = '/wp-content/videos/Affiliate_Blue.flv';   //flv source (if present will be used for flash instead of mp4)
f.tvideo1[0].mp4 = '';   //mp4 source (for flash flv takes precendance, will be used for i-devices)
f.tvideo1[0].ogv = '';   //ogv source (for future use only: for firefox iff mp4 null)
f.tvideo1[0].stream = "rtmp://s1fc6c106j2ahq.cloudfront.net/cfx/st";	//rtmp only - of form rtmp://path_to_myVideosFolder
f.tvideo1[0].protocol = "http";		//either http or rtmp
f.tvideo1[0].secondsIn = 0;		//rtmp only - n start n seconds into the video
f.tvideo1[0].playDuration = -1;		//rtmp only - play duration n seconds, -1 for whole video
f.tvideo1[0].rtmpTimeout = 150;		//rtmp only - if the server timesout on lengthy non-play state specify approx timeout here (secs)

f.tvideo1[1] = '../videos/ADVplayer.swf';   //player - path is relative to the templates folder 
f.tvideo1[2] = true;   //autoLoad
f.tvideo1[3] = true;   //isContrVisible
f.tvideo1[4] = 1;   //bufferInt
f.tvideo1[5] = true;   //vPreview
f.tvideo1[6] = false;   //stopLastFrame
f.tvideo1[7] = false;   //runOnStreamEnd
f.tvideo1[8] = '_blank';   //specify new window location for info (iLink) button (_blank, _parent, _self, _top)  
f.tvideo1[9] = true;   //redOnStreamEnd
//f.tvideo1[10] = 'http://nanacast.com/vp/88490/11087/10896/';   //vLink
f.tvideo1[10] = '';   //vLink
f.tvideo1[11] = 'http://www.cleancredit.com.au';   //iLink
f.tvideo1[12] = 250;   //width
f.tvideo1[13] = 400;   //height 
f.tvideo1[14] = true;   //autoPlay
f.tvideo1[15] = false;   //autoClose
f.tvideo1[16] = '';   //onStreamEnd
f.tvideo1[17] = false;   //loop video 
f.tvideo1[18] = true;   //isContrVisibleStay (isContrVisible must be true)
f.tvideo1[19] = true;   //wideCtrl (stage must be >= 240 in width)
f.tvideo1[20] = '';   //Affiliates of the Transparent Player enter affiliate id here of form vp/12345/12345/12345/
f.tvideo1[21] = '';   //Affiliates of Digital Flow Software enter affiliate id here of form 42eb1234
f.tvideo1[22] = '_top';   //specify new window location for video onclick & redirect (vLink) (_blank, _parent, _self, _top)
f.tvideo1[23] = '_blank';   //specify new window location for context menu links (_blank, _parent, _self, _top)
f.tvideo1[24] = 3;   //advanced: previewTimeMax - if vPreview & autoLoad true & autoPlay false cancel all buffering after this time (secs) 
f.tvideo1[25] = true;   //use a start image (set this up in the layer variables below)
f.tvideo1[26] = true;   //display the timer 
f.tvideo1[30] = true;   //follow page scroll
f.tvideo1[31] = 7;   //placement: 1: Center 2: Bottom center 3: Left center 4: Right center 5: Top center 6: Top left 7: Bottom left 8: Bottom right 9: Top right
f.tvideo1[32] = false;   //modal
f.tvideo1[33] = 'Title text';   //titleText
f.tvideo1[34] = false;   //title
f.tvideo1[35] = false;   //close
f.tvideo1[36] = false;   //anchored
f.tvideo1[37] = 'adpLayerAnchor1';   //layer anchor ID where the ID must correspond to the video number 
f.tvideo1[38] = 0;   //Offset from layer anchor in x
f.tvideo1[39] = 0;   //Offset from layer anchor in y
f.tvideo1[40] = -1;   //cookie: set to -1 for no cookie, 0 for session cookie, integer n for n days cookie
f.tvideo1[41] = false;   //on exit: if true & all other onload videos false then all videos appear on exit (single exit)
f.tvideo1[42] = 0;   //delay (secs) for this video  (this works in addition to any startVideo() delay)
f.tvideo1[43] = false;   //directly embedded (relies on html element with id advDirectAnchorn where n is the video number)
f.tvideo1[44] = '';   //internal use only: onStreamEnd wizard settings
f.tvideo1[45] = '';   //runOnClose command set
f.tvideo1[46] = '../images/play2.png';   //start image
f.tvideo1[47] = 'tplayervideo1.DFS.adv.advPlay("tplayervideo1");';   //start image command set
f.tvideo1[48] = '';   //internal use only: display mode wizard settings
f.tvideo1[49] = '';   //internal use only: onClose wizard settings
f.tvideo1[50] = '';   //internal use only: onClose wizard settings (custom)




//start do not edit
})();
DFS.adv.advOnSubmit = function(uid){
	var f=DFS.adv;
	var n = uid.substring(13);
	var tmp = 'f.tcustom' + n;
	f.customn = eval(tmp);
	eval(f.customn[19]);
}
function adpOnHide(uid){
 var f=DFS.adv;
 switch(uid.substring(0,8)){
	case 'tplayerv':	
		var n = uid.substring(12);
		var tmp = 'f.tvideo' + n;
		f.tvideon=eval(tmp);
		eval(f.tvideon[45]);
		break;	
	case 'tplayerc':
		var n = uid.substring(13);
		var tmp = 'f.tcustom' + n;
		f.tcustomn=eval(tmp);
		eval(f.tcustomn[11]);
		break;
	case 'tplayery':
		var n = uid.substring(14);
		var tmp = 'f.tyoutube' + n;
		f.tyoutuben=eval(tmp);
		eval(f.tyoutuben[25]);	
		break;
	default:
 }	

  if(f.closeOneCloseAll){  
    for (var i=1;i<f.j;i++){
      if(de7(f.uids[i]+'_adpC') && de7(f.uids[i]+'_adpC').style.visibility=='visible'){
        soh(f.uids[i],nbl,nvi);
        if(mie || f.safchr)adpContent(f.uids[i],'');
      }
    }
  }  
}
DFS.adv.runOverImageCmdSet=function(uid){
	var n = uid.substring(12);
	var tmp = 'DFS.adv.tvideo' + n;
	DFS.adv.tvideon=eval(tmp);
	eval(DFS.adv.tvideon[47]);
}
//end do not edit