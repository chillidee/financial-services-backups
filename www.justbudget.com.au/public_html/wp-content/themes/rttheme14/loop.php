<?php
# 
# rt-theme loop
#

global $args;
add_filter('excerpt_more', 'no_excerpt_more');

if ($args) query_posts($args);

if ( have_posts() ) : while ( have_posts() ) : the_post(); 
?>



    <!-- blog box-->
    <div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <div class="box first blog_box">
	   
	   <?php
	   //featured image	   
	   $thumb 	= get_post_thumbnail_id();
	   $crop 		= get_option('rttheme_image_crop'); if($crop) $crop="true";
	   $width 	= get_option('rttheme_blog_image_width');
	   $height 	= get_option('rttheme_blog_image_height'); if($height==0 ) $height=9999;
	   
	   if($thumb) $image = @vt_resize( $thumb, '', $width, $height, ''.$crop.'' );
	   ?>
	   
	   <?php if ($thumb):?>
	   <!-- blog image-->
	   <div class="blog_image">
		  <span class="frame alignleft"><a href="<?php echo get_permalink() ?>" title="<?php the_title(); ?>" class="imgeffect link">
			 <img src="<?php echo $image["url"];?>" alt="" />
		  </a></span>
	   </div>
	   <!-- / blog image -->
	   <?php endif;?>
	   
	   <!-- blog headline-->
	   <h3><a href="<?php echo get_permalink() ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h3>
	   <!-- / blog headline-->					
	   
        <!-- date and cathegory bar -->
		  <div class="dateandcategories">
			 <?php the_time(get_option('rttheme_date_format')) ?> | <?php _e('posted in:','rt_theme'); ?> <i><?php the_category(', ') ?></i> | <?php _e('by','rt_theme'); ?> <i><?php the_author_posts_link(); ?></i>
			 <?php
			 //Comments
			 $comment_count = get_comment_count($post->ID);
			 if($comment_count['approved'] > 0):
			 ?>			
			 <span class="comment"> → <?php comments_popup_link( __('0 Comment','rt_theme'), __('1 Comment','rt_theme'), __('% Comments','rt_theme') ); ?></span>
			 <?php endif;?>
		  </div>
        <!-- / date and cathegory bar -->

        <?php if(get_the_excerpt()):?>
        <!-- blog text-->
		  <?php
		  echo "<p>".do_shortcode(get_the_excerpt())."</p>";
		  if(!empty($post->post_content)): echo ' <a href="'. get_permalink($post->ID) . ' " class="link_button" >'.__('read more →','rt_theme').'</a>';endif;
		  ?> 
        <!-- /blog text-->
        <?php endif;?>
	   
	   <div class="tags">
        <!-- tags -->
        <?php echo the_tags( '<span>', '</span>, <span>', '</span>');?>  
        <!-- / tags -->
        </div>
	   
    </div>
    </div>
    <!-- blog box-->
    
    <div class="line"></div>
    
<?php endwhile; ?> 

<div class="clear"></div>
		
    <?php
    //get page and post counts
    $page_count=get_page_count();
    
    //show pagination if page count bigger then 1
    if ($page_count['page_count']>1):
    ?>  

    <!-- paging-->
    <div class="paging_wrapper">
	   <ul class="paging">
		  <?php get_pagination(); ?>
	   </ul>
    </div>			
    <!-- / paging-->
    
    <?php endif;?>

<?php wp_reset_query();?>

<?php else: ?>
<p><?php _e( 'Sorry, no posts matched your criteria.', 'rt_theme'); ?></p> 
<?php endif; ?>
