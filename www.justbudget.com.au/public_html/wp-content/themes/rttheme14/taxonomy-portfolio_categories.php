<?php
# 
# rt-theme portfolio list
#

$taxonomy		= 'portfolio_categories';
$term_slug	= get_query_var('term');
$term 		= get_term_by( 'slug', $term_slug, $taxonomy, 'true', '' );
$term_id 		= $term->term_id; 

get_header();
?>

<!-- contents -->
<div class="border">

	<!-- page title --> 
	<div class="head_text"><h2><?php echo $term->name;?></h2></div>
	<!-- / page title --> 
    
	<!-- Page navigation-->
	<?php rt_breadcrumb(); ?>
	<!-- /Page navigation-->

	<?php
	//Portfolio Categories Popup Menu
	if(get_option('rttheme_portfolio_popup_categories')){
		get_template_part( 'portfolio_categories_menu', 'portfolio_categories_menu_file' );
	}
	?>	
 
	<div class="content_background sub">
	<div class="content full">


		<?php if($term->description):?>
		<!-- Category Description -->
		<div class="box">
		<?php echo do_shortcode($term->description);?> 
		</div><div class="clear"></div>
		<?php endif;?>		

		<!-- Start Porfolio Items -->
		<?php
			//page
			if (get_query_var('paged') ) {$paged = get_query_var('paged');} elseif ( get_query_var('page') ) {$paged = get_query_var('page');} else {$paged = 1;}
			//taxonomy

			$args=array( 
				'post_status'=> 'publish',
				'orderby'=> get_option('rttheme_portf_list_orderby'),
				'order'=> get_option('rttheme_portf_list_order')
			);		
		?>
		<?php get_template_part( 'portfolio_loop', 'portfolio_categories' );?>
		<!-- End Porfolio Items -->

     <div class="clear"></div> 
	</div>

	</div>
</div>
<!-- / contents  -->
<?php get_footer();?>