	<?php
	if(get_option( THEMESLUG.'_twitter_username') && get_option( THEMESLUG.'_twitter_bar') ):
	?>
	<!-- Tweet Bar  -->
	<div class="border"> 
		<div class="banner tweetbar">
			<script type="text/javascript">jQuery(".banner.tweetbar").tweet({ count: '1', query: "from:<?php echo get_option( THEMESLUG.'_twitter_username'); ?>", loading_text: "..." });</script>
		</div>
	 </div>
	<!-- / Tweet Bar  -->
	<?php endif;?>
	
</div>
</div>
<!-- / wrapper -->

<!-- footer wrapper -->
<div class="footer_background"> 
	<div class="border">

	<?php
	if(get_option( THEMESLUG.'_show_footer_widgets') ):
	?>
	<!-- footer widgets -->
	<div class="content_background">
	<div class="content home_page content_footer"> 

		<!-- widgetized home page area -->
		<?php if (function_exists('dynamic_sidebar')){  dynamic_sidebar('sidebar-for-footer'); } ?>
		<div class="clear"></div>
		<!-- / widgetized home page area -->
		
	</div>
	</div>
	<!-- / footer widgets contents  -->
	<div class="clear"></div><div class="space half"></div>
	<?php endif;?>
	
		<!-- footer -->
		<div id="footer">
			<div class="part1">
			 
				<!-- footer links -->
				<div class="footer_menu_block">
				    <?php 
				    
				    //call the footer menu
				    $topmenuVars = array(
					   'menu'            => 'RT Theme Footer Navigation Menu',
					   'depth'		 => 1,
					   'menu_id'         => '',
					   'menu_class'      => 'footer_links', 
					   'echo'            => false,
					   'container'       => '', 
					   'container_class' => '', 
					   'container_id'    => '',
					   'theme_location'  => 'rt-theme-footer-menu' 
				    );
				    
				    $footer_menu=wp_nav_menu($topmenuVars);
				    echo $footer_menu;
				    ?>
		  		</div>
				<!-- / footer links -->
				
				<!-- copyright text -->
				<div class="copyright"><?php echo wpml_t(THEMESLUG, 'Footer Copyright Text', get_option(THEMESLUG.'_footer_copy'));?></div>
				<!-- / copyright text -->
			</div>
			
			<div class="part2">
				<ul class="partners_links">
					<?php for($i = 1; $i < 10; $i ++){
							$_partner_logo = get_option(THEMESLUG.'_partner_logo_'.$i); 
							$_partner_link = get_option(THEMESLUG.'_partner_link_'.$i); 
							if(empty($_partner_logo)){
								break;
							}else{
						?>
								<li><a href="<?php echo $_partner_link; ?>" target="_blank"><img src="<?php echo $_partner_logo; ?>"/></a></li>
					<?php 	
							}
						  } 
					?>
				</ul>
			</div>
		  
			<?php
			//social media icons
			global $social_media_icons;
			$social_media_output ='';

			foreach ($social_media_icons as $key => $value){
				if($value == 'cra'){
					//show CRA logo
					if(get_option( THEMESLUG.'_'.$key )){
						$titlestring = "For your peace of mind only ever deal with an Accredited CRIA member. ";
						echo '<div class="cria">';
						echo '<a href="http://www.cria.net.au" target="_blank" title="'.$titlestring.'">';
						echo '<img src="'.get_option( THEMESLUG.'_'.$key ).'" alt="'.$titlestring.'" />';
						echo '</a>';
						echo '</div>';
					}	
				}else{
					//social media
					if(get_option( THEMESLUG.'_'.$key )){
						$social_media_output .= '<li>';
						$social_media_output .= '<a href="'. get_option( THEMESLUG.'_'.$key ) .'" class="j_ttip" title="'.$key.'">';
						$social_media_output .= '<img src="'.THEMEURI.'/images/social_media/icon-'.$value.'.png" alt="" />';
						$social_media_output .= '</a>';
						$social_media_output .= '</li>';
					}
				}
			}
			if($social_media_output) echo '<ul class="social_media_icons">'.$social_media_output.'</ul>';
			?>			     			
		</div>
		<!-- /footer -->
	</div>
</div>
<!-- /footer wrapper -->

<?php echo get_option( THEMESLUG.'_google_analytics');?> 

<!-- for officeautopilot tracking 
<script src='http://moonraymarketing.com/tracking.js' type='text/javascript'></script>
<script>
_mri = "5208_1_2";
mrtracking();
</script>
-->

</body>
</html>

<?php wp_footer();?>