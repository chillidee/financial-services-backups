<?php
//page link
$link_page=get_permalink(get_option('rttheme_blog_page'));

//category link
$category_id = get_the_category($post->ID);
$category_id = $category_id[0]->cat_ID;//only one category can be show in the list  - the first one
$link_cat=get_category_link($category_id); 

//redirect to home page if user tries to view slider or home page contents by clicking the view link on admin
if (get_query_var('home_page') || get_query_var('slider')){ header( 'Location: '.BLOGURL.'/ ' ) ;}


get_header();

?>

	<?php 
		//show page banner slides when it is not blog page
		//if($post->ID != wpml_page_id(BLOGPAGE)){
		
		$z_page_banners_options_slugs = array("z_page_banners_option_1", "z_page_banners_option_2", "z_page_banners_option_3","z_page_banners_option_4");
		shuffle($z_page_banners_options_slugs); 
		foreach($z_page_banners_options_slugs as $z_page_slug){
			$z_page = get_page_by_path($z_page_slug);
		    if (!$z_page) { continue; }
		    $z_page_id = $z_page->ID;
		    $thumb 		=	get_post_thumbnail_id($z_page_id); 
		    $thumb2 = null; $thumb3 = null;
		    if (class_exists('MultiPostThumbnails') && MultiPostThumbnails::has_post_thumbnail('page', 'secondary-image',$z_page_id)){
				$thumb2 = MultiPostThumbnails::get_post_thumbnail_id('page', 'secondary-image', $z_page_id);
		    }
		    if (class_exists('MultiPostThumbnails') && MultiPostThumbnails::has_post_thumbnail('page', 'third-image',$z_page_id)){
				$thumb3 = MultiPostThumbnails::get_post_thumbnail_id('page', 'third-image', $z_page_id);
		    }
		    if(!empty($thumb) || !empty($thumb2) || !empty($thumb3)){
		    	$shortcode_text = '[slider]';
		    	if(!empty($thumb)){
		    		$image 		=	@vt_resize( $thumb, '', 970, 150, true );
					$shortcode_text .= '
						[slide class="first" image_width="970" image_height="150" link="" alt_text="check it out" auto_resize="true"]'.$image['url'].'[/slide]';
		    	}
				if (!empty($thumb2)){
					$image2 =	@vt_resize( $thumb2, '', 970, 150, true );
					$shortcode_text .= '
						[slide image_width="970" image_height="150" link="" alt_text="check it out" auto_resize="true"]'.$image2['url'].'[/slide]';
				}
				if (!empty($thumb3)){
					$image3 =	@vt_resize( $thumb3, '', 970, 150, true );
					$shortcode_text .= '
						[slide image_width="970" image_height="150" link="" alt_text="check it out" auto_resize="true"]'.$image3['url'].'[/slide]';
				}
		    	$shortcode_text .= '
		    		[/slider]'; 
		    	
				echo '<div class="page_banner">';
				echo do_shortcode($shortcode_text);
				echo '</div>';
				break;
		    }
		    
		}
	//}
	?>

	<div class="diognal-pattern-dummy"></div>
	
<!-- contents -->
<div class="border">

   
    <!-- Page navigation-->
    <?php rt_breadcrumb(); ?>
    <!-- /Page navigation-->

    <div class="content_background sub">
    <div class="content sub">
    
        <!-- page content -->
        <div id="left" class="left">


            <?php if (have_posts()) : while (have_posts()) : the_post(); ?>            
            
            <!-- blog box-->
            <div class="box first blog_box">
                
                <?php
                //featured image	   
                $thumb = get_post_thumbnail_id();
                $crop = get_option('rttheme_image_crop'); if($crop) $crop="true";
                $image_url = wp_get_attachment_image_src($thumb,'false', true);
                $width = get_option('rttheme_blog_image_width');
                $height = get_option('rttheme_blog_image_height'); if($height==0 ) $height=9999;
                
                if($thumb) $image = vt_resize( $thumb, '', $width, $height, ''.$crop.'' );
                ?>
                
                <?php if ($thumb):?>
                <!-- blog image-->
                <div class="blog_image">
                    <span class="frame alignleft"><a href="<?php echo $image_url[0]; ?>" title="<?php the_title(); ?>" rel="prettyPhoto[rt_theme_blog_<?php echo $post->ID;?>]" class="imgeffect plus">
                        <img src="<?php echo $image["url"];?>" alt="" />
                    </a></span>
                </div>
                <!-- / blog image -->
                <?php endif;?>
                
                <!-- blog headline-->
                <h2><?php the_title(); ?></h2>
                <!-- / blog headline-->					
                
                <!-- date and cathegory bar -->
                    <div class="dateandcategories">
                        <?php the_time(get_option('rttheme_date_format')) ?> | <?php _e('posted in:','rt_theme'); ?> <i><?php the_category(', ') ?></i> | <?php _e('by','rt_theme'); ?> <i><?php the_author_posts_link(); ?></i>
                        <?php
                        //Comments
                        $comment_count = get_comment_count($post->ID);
                        if($comment_count['approved'] > 0):
                        ?>			
                        <span class="comment"> → <?php comments_popup_link( __('0 Comment','rt_theme'), __('1 Comment','rt_theme'), __('% Comments','rt_theme') ); ?></span>
                        <?php endif;?>
                    </div>
                <!-- / date and cathegory bar -->
            
                <!-- blog text-->
                <?php the_content(); ?> 
                <!-- /blog text-->

		<!-- AddThis Button BEGIN -->
		<div class="addthis_toolbox addthis_default_style ">
		<a class="addthis_button_facebook_like" fb:like:layout="button_count"></a>
		<a class="addthis_button_tweet"></a>
		<a class="addthis_button_pinterest_pinit"></a>
		<a class="addthis_counter addthis_pill_style"></a>
		</div>
		<script type="text/javascript">var addthis_config = {"data_track_addressbar":true};</script>
		<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5202f1f67cede230"></script>
		<!-- AddThis Button END -->
                
                <div class="tags">
                    <!-- tags -->
                    <?php echo the_tags( '<span>', '</span><span>', '</span>');?>  
                    <!-- / tags -->
                </div>
                
            </div>    
            <!-- blog box-->            

            <?php endwhile;?>

            <div class="clear"></div>
            
            <?php if(get_option(THEMESLUG."_hide_author_info")):?>
            <!-- Info Box -->
            <div class="line"></div>
                <h5><?php _e( 'About the Author', 'rt_theme' ); ?></h5>
                <div class="info_box_content">
                        <span class="alignleft frame"><?php if (function_exists('get_avatar')) { echo get_avatar( get_the_author_meta('email'), '60' ); }?>  </span>
                           <p>
                            <strong><?php the_author_posts_link(); ?></strong><br />
                              <?php the_author_meta('description'); ?>
                           </p>
                    <div class="clear"></div>       
                </div>
            <?php endif;?>            

            <div class='entry commententry'>
                <?php comments_template(); ?>
            </div>
            
            <?php else: ?>
                <p><?php _e( 'Sorry, no page found.', 'rt_theme' ); ?></p>
            <?php endif; ?>                

        </div>
        <!-- / page contents  -->


        <!-- side bar --> 
        <div id="sidebar" class="sidebar"><div class="sidebar_back"><div class="sidebar_back2">
        <?php get_template_part( 'sidebar', 'sidebar_file' );?>
        <div class="clear"></div></div></div></div>
        <!-- / side bar -->
        
    <div class="clear"></div>
    </div>
    </div>
</div>
<!-- / contents  -->

   
<?php get_footer();?>