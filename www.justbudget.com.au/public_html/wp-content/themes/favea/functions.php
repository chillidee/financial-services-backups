<?php

//Textdomain
define( 'EF_TDM', 'favea' );

require_once 'admin/init.php';

if ( current_theme_supports( 'ef-custom-post-types' ) ) {
	require_once 'inc/ajax-posts.php';
}


/*
* ------------------------------------------------- *
*		Contact Form for iFrame
* ------------------------------------------------- *
*/
// enable session
if (!session_id())
    session_start();
    
function startsWith($haystack, $needle){
    return !strncmp($haystack, $needle, strlen($needle));
}

function rt_shortcode_iframe_contact_form( $atts, $content = null ) {
 
	if(!isset($atts['baseurl'])) return 'Error: Base URL is not set. ';
	if(!isset($atts['cid'])) return 'Error: CID is not set. ';
	$cssurl = get_bloginfo('template_directory')."/css/enquiryForm.css";
	$cssurl = urlencode($cssurl);
	$referurl = urlencode($_SESSION['mm_referalurl']);
	$adwords_t = urlencode($_SESSION['mm_adwords_t']);
	$adwords_k = urlencode($_SESSION['mm_adwords_k']);
	$adwords_a = urlencode($_SESSION['mm_adwords_a']);

	$contact_form.= "".    
		'<!-- contact form -->'.
		'<div class="clear"></div>'.
		'<div id="contact_form">'.
		'	<iframe id="iframe_enquiry_form" frameBorder="0" src="'.$atts['baseurl'].'?cid='.$atts['cid'].'&mode='.$atts['mode'].'&svs='.$atts['svs'].'&css='.$cssurl.'&ref='.$referurl.'&t='.$adwords_t.'&k='.$adwords_k.'&a='.$adwords_a.'"></iframe>'.
		'</div>'.
		'<div class="clear"></div>'.
		'<!-- /contact form -->'.
		'<!-- comment out floating function for now
		<style> 
			.sidebar_back2 { padding-bottom: 0 !important; }
			.sidebar_back {padding-top: 0 !important; }
		</style>
		<script type="text/javascript">
		//#sidebar needs to have style: position: relative for this to work.
		jQuery(document).ready(function($) 
		{ 
		    originalSidebarTop = jQuery("#sidebar").offset().top;
		    jQuery(window).scroll(function () {
		    	var scrollTop = jQuery(document).scrollTop();
		        var contentHeight = jQuery("#left").height();
		        var sidebarHeight = jQuery("#sidebar").height();
		        
		        if(scrollTop > originalSidebarTop){
		        	offset = scrollTop - originalSidebarTop;
		        }else{
		        	offset = -30;
		        }
		        
		        if(offset > (contentHeight - sidebarHeight)){
		        	offset = contentHeight - sidebarHeight; //can not go beyond the content length
		        	if(offset < 0){ //just in case content length is shorter than sidebar
		        		offset = -30;
		        	}
		        }
		        
		        jQuery("#sidebar").animate({top:offset+"px"},{duration:500,queue:false});
		        
		        console.log("originalSidebarTop:"+originalSidebarTop+" contentHeight:"+contentHeight+" sidebarHeight:"+sidebarHeight+" scrollTop:"+jQuery(document).scrollTop()+" offset:"+offset);
		    }); 
		});
		</script>-->
		';
	
	return $contact_form;
}
add_shortcode('iframe_contact_form', 'rt_shortcode_iframe_contact_form');
