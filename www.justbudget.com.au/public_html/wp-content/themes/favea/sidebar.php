<?php

/**
 * The sidebar template.
 *
 * @package Favea
 * @since Favea 1.0
 */

?>

<?php $ef_data = EfGetOptions::TplConditions(); ?>

<?php $sidebar = $ef_data['sidebar']; ?>

<?php if ( !empty( $sidebar ) ) { ?>
	<?php if ( $sidebar && is_active_sidebar( $sidebar ) ) { ?>

		<aside class="ef-radius ef-bottom-1_5 clearfix">
			<?php dynamic_sidebar( $sidebar ); ?>
		</aside><!-- .ef-radius.ef-bottom-1_5.clearfix -->

	<?php } elseif ( is_user_logged_in() ) { ?>

		<aside class="ef-radius clearfix">
			<div class="alert-box radius">
				<?php $link = sprintf( __( ' Ready to add widgets? <a href="%s"><abbr title="Add new">Get started here</abbr></a>', EF_TDM ), admin_url( 'widgets.php' ) ); ?>
				<p><?php printf( __( 'Please, add widgets to "Default sidebar widget area". %s', EF_TDM ), $link ); ?></p>
			</div><!-- .alert-box.radius -->
		</aside><!-- .ef-radius.clearfix -->
		
	<?php } ?>
<?php } ?>