<?php
/**
 * The template for displaying 404 pages (Not Found).
 *
 * @package Favea
 * @since Favea 1.0
 */

?>

<?php get_header(); ?>

<?php $ef_data = EfGetOptions::TplConditions(); ?>

<?php $layout = $ef_data['layouts']; ?>

	<section class="row">
		<div class="large-<?php echo $layout['0'] ?> columns">

			<div id="post-0" class="post error404 not-found">
				<div class="row">
					<div class="small-6 large-centered small-centered columns">

						<p class="is-404 text-center ef-no-margin">404</p>

						<p class="text-center"><?php _e( 'Apologies, but the page you requested could not be found. Perhaps searching will help.', EF_TDM ); ?></p>
						<?php get_search_form(); ?>

					</div><!-- .small-6.large-centered.small-centered.columns -->
				</div><!-- .row -->
			</div><!-- #post-0 -->

		</div><!-- large-##.columns -->

		<?php if ( $layout['0'] != 12 ) { ?>
			<div class="ef-sidebars hide-for-print">
				<div class="ef-sidebar large-<?php echo $layout['1'] ?> columns">
					<?php get_sidebar(); ?>
			  	</div><!-- .ef-sidebar.large-##.columns -->
		  	</div><!-- .ef-sidebars.hide-for-print -->
		<?php } ?>

	</section><!-- .row -->

<?php get_footer(); ?>