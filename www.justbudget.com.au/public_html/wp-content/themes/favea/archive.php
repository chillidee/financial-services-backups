<?php
/**
 * The template for displaying Archive pages.
 *
 * @package Favea
 * @since Favea 1.0
 */

?>

<?php get_header(); ?>

<?php $ef_data = EfGetOptions::TplConditions(); ?>

<?php $layout = $ef_data['layouts']; ?>

		<section class="row">

			<div class="large-<?php echo $layout['0'] ?> columns">

				<?php if ( term_description() ) { ?>
					<div class="ef-archive-meta"><?php echo term_description(); ?></div>
				<?php } ?>

				<?php get_template_part( 'loop', 'archive' ); ?>

				<?php /* Pagination */ ?>
				<?php ef_theme_pagination( $wp_query->max_num_pages, $range = 2, $ef_data ); ?>

			</div><!-- .large-##.columns -->

			<?php if ( $layout['0'] != 12 ) { ?>
				<div class="ef-sidebars hide-for-print">
					<div class="ef-sidebar large-<?php echo $layout['1'] ?> columns">
						<?php get_sidebar(); ?>
				  	</div><!-- .ef-sidebar.large-##.columns -->
			  	</div><!-- .ef-sidebars.hide-for-print -->
			<?php } ?>

		</section><!-- .row -->

<?php get_footer(); ?>