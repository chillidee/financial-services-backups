<?php
/**
 * The template for displaying Author's archive.
 *
 * @package Favea
 * @since Favea 1.0
 */

?>

<?php get_header(); ?>

<?php $ef_data = EfGetOptions::TplConditions(); ?>
<?php $layout = $ef_data['layouts']; ?>

		<section class="row">
			<div class="large-<?php echo $layout['0'] ?> columns">

				<?php ef_theme_author_info(); ?>

				<?php if ( have_posts() ) { ?>
					<?php the_post(); ?>
				<?php } ?>
				<?php rewind_posts(); ?>
				<?php get_template_part( 'loop', 'archive' ); ?>

				<?php /* Pagination */ ?>
				<?php ef_theme_pagination( $wp_query->max_num_pages, $range = 2, $ef_data ); ?>

			</div><!-- .large-##.columns -->

			<?php if ( $layout['0'] != 12 ) { ?>
				<div class="ef-sidebars hide-for-print">
					<div class="ef-sidebar large-<?php echo $layout['1'] ?> columns">
						<?php get_sidebar(); ?>
				  	</div><!-- .ef-sidebar.large-##.columns -->
			  	</div><!-- .ef-sidebars.hide-for-print -->
			<?php } ?>

		</section><!-- .row -->

<?php get_footer(); ?>
