<?php
/**
 * Template Name: Blog
 *
 * @package Favea
 * @since Favea 1.0
 */

?>

<?php get_header(); ?>

<?php global $paged ?>

<?php $ef_data = EfGetOptions::TplConditions(); ?>

<?php $layout = $ef_data['layouts']; ?>

<?php $wp_query_tmp = $wp_query; ?>

		<section class="row">
			
			<div class="large-<?php echo $layout['0'] ?> columns">
			
				<?php get_template_part( 'loop', 'page' ); ?>
				
				<?php $args = array(
					'post_type'				=> 'post',
					'category__not_in'		=> $ef_data['terms'],
					'posts_per_page'		=> isset( $ef_data['posts_per'] ) ? $ef_data['posts_per'] : '',
					'post_status'			=> 'publish',
					'paged'					=> $paged,
					'ignore_sticky_posts'	=> 1,
				); ?>
				
				<?php $wp_query = NULL; $wp_query = new WP_Query( $args ); ?>
				
				<?php get_template_part( 'loop', 'blog' ); ?>
				
				<?php /* Pagination */ ?>
				<?php ef_theme_pagination($wp_query->max_num_pages, $range = 2, $ef_data); ?>
				
				<?php $wp_query = $wp_query_tmp; ?>
		
			</div>
		
			<?php if ( $layout['0'] != 12 ) { ?>
				<div class="ef-sidebars hide-for-print">
					<div class="ef-sidebar large-<?php echo $layout['1'] ?> columns">
						<?php get_sidebar(); ?>
				  	</div><!-- .ef-sidebar.large-##.columns -->
			  	</div><!-- .ef-sidebars.hide-for-print -->
			<?php } ?>
		
		</section><!-- .row -->

<?php get_footer(); ?>
