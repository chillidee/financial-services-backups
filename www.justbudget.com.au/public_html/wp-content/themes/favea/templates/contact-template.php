<?php
/**
 * Template Name: Contact
 *
 * @package Favea
 * @since Favea 1.0
 */

?>

<?php get_header(); ?>

<?php $ef_data = EfGetOptions::TplConditions(); ?>

<?php $layout = $ef_data['layouts']; ?>

		<section class="row">

			<div class="large-<?php echo $layout['0'] ?> columns">

				<?php $lat_lng = isset( $ef_data['mb_support'] ) && ! ( ef_array_empty( $ef_data['map_lat'] ) && ef_array_empty( $ef_data['map_long'] ) ); ?>

				<?php if ( $lat_lng ) { ?>
					<div id="map_canvas" class="ef-map"></div>
				<?php } ?>

				<?php get_template_part( 'loop', 'page' ); ?>

			</div><!-- .large-##.columns -->

			<?php if ( $layout['0'] != 12 ) { ?>
				<div class="ef-sidebars hide-for-print">
					<div class="ef-sidebar large-<?php echo $layout['1'] ?> columns">
						<?php get_sidebar(); ?>
				  	</div><!-- .ef-sidebar.large-##.columns -->
			  	</div><!-- .ef-sidebars.hide-for-print -->
			<?php } ?>

		</section><!-- .row -->

<?php get_footer(); ?>
