<?php
/**
 * Template Name: Portfolio
 *
 * @package Favea
 * @since Favea 1.0
 */

?>

<?php get_header(); ?>

<?php $ef_data = EfGetOptions::TplConditions(); ?>

<?php $layout = $ef_data['layouts']; ?>

		<section class="row">
		
			<div class="large-<?php echo $layout['0'] ?> columns">
			
				<?php get_template_part( 'loop', 'page' ); ?>
				
				<section class="row">
					<div class="large-12 columns">
						
						<?php global $paged; ?>
						
						<?php $args = array(
							'post_type' => $ef_data['cpt_support'] ? EF_CPT : '',
							'tax_query' => array(
								array('taxonomy'=> $ef_data['cpt_support'] ? EF_CPT_TAX_CAT : '',
									'terms'		=> $ef_data['terms'],
									'field'		=> 'id',
									'operator'	=> 'NOT IN'
								)
							),
							'posts_per_page'	  => isset( $ef_data['posts_per'] ) ? $ef_data['posts_per'] : -1,
							'post_status'		  => 'publish',
							'ignore_sticky_posts' => true,
							'meta_key' 			  => '_thumbnail_id',
							'paged'				  => $paged
						); ?>
						
						<?php $postslist = NULL; $postslist = new WP_Query( $args ); ?>
						
						<?php if ( ! $postslist->have_posts() ) { ?>
						
							<?php ef_get_msg_data( $postslist->query_vars['post_type'], $ef_data ); ?>
							
						<?php } elseif ( !post_password_required() ) { ?>
						
							<?php if ( isset( $ef_data['mb_support'] ) && $ef_data['cpt_support'] && $ef_data['portf_type'] == 'filtered' ) { ?>
							
								<?php $terms = get_terms( EF_CPT_TAX_CAT ); ?>
								
								<?php $cats = array(); ?>
								<?php foreach ( $terms as $term ) { ?>							
									<?php if ( !in_array( $term->term_id, $ef_data['terms'] ) ) { ?>
										<?php $cats[$term->term_id] = array( 'name' => $term->name, 'slug' => $term->slug ); ?>
									<?php } ?>
								<?php } ?>
		
								<?php if ( count( $cats ) != 0 ) { ?>
								
									<ul class="ef-portfolio-filter ef-bottom-2_18 text-center hide-for-print">

										<li class="ef-currentclass ef-all-works"><a data-option-value="*" href="#"><?php _e( 'All categories', EF_TDM ) ?></a></li>

										<?php foreach ( $cats as $id => $cat ) { ?>
											<?php echo '<li><a data-option-value=".' . $cat['slug'] . '" href="' . esc_url( get_term_link( $id, EF_CPT_TAX_CAT ) ) . '">' . $cat['name'] . '</a></li>'; ?>										
										<?php } ?>
									</ul>

									<div class="clear"></div>
									
								<?php } ?>
							
							<?php } ?>
						
							<div id="ef-portfolio">								
								<section class="ef-portfolio-wrap clearfix">
									
									<?php while( $postslist->have_posts()) : // Loop ?>
									
										<?php $postslist->the_post(); ?>
										
										<?php if ( has_post_thumbnail() ) { ?>
											
											<?php $tags = get_the_terms( $post->ID, EF_CPT_TAX_TAG ); ?>
											<?php $cats = get_the_terms( $post->ID, EF_CPT_TAX_CAT ); ?>
											<?php $cats = !empty( $cats ) ? $cats : $cats = array(); ?>
																					
											<article class="<?php foreach ( $cats as $cat ) echo esc_attr( $cat->slug ) . " "; ?>ef-portfolio-item text-center<?php echo $ef_data['mb_support'] && $ef_data['lb_lnk'] == "img" ? ' ef-has-lightbox' : ''; ?>">
												
												<div class="ef-portfolio-item-inner">
																		    	
													<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
														
														<?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'post_large_thumb' ); ?>
														
														<?php $thumb_small = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'cpt_thumb' ); ?>
														
														<?php if ( isset( $ef_data['mb_support'] ) && $ef_data['lb_lnk'] == "img" ) { ?>
															<a class="ef-proj-img" <?php if ( !post_password_required() ) { echo 'data-gal="lb[gal]"'; } ?> title="<?php the_title_attribute(); ?>" href="<?php echo !post_password_required() ? $thumb['0'] : the_permalink(); ?>">

																<div class="ef-img-overlay"><span class="ef-link-marker ef-radius"></span></div>
																
														<?php } else { ?>
															<a class="ef-proj-img" title="<?php the_title_attribute(); ?>" href="<?php the_permalink(); ?>">									
														<?php } ?>
																<?php echo get_the_post_thumbnail( $post->ID, 'cpt_thumb' ); ?>
																<span class="ef-loader"><span></span></span>
																<?php if ( post_password_required() ) { ?>
																	<span class="ef-pass"></span>
																<?php } ?>
															</a>
														
														<?php $ef_expd = isset( $ef_data['mb_support'] ) && !empty( $ef_data['thumb_tip'] ) && $ef_data['thumb_tip'] == 'excerpt' && $post->post_content != "" ? 1 : NULL; ?>
														
														<?php $ef_expd2 = !empty( $tags ) ? 1 : NULL; ?>
														
														<div class="ef-item-inner<?php echo !( $ef_expd || $ef_expd2 ) ? ' ef-no-expd' : ''; ?>">
														
															<h5 class="ef-no-margin ef-item-title">
																<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?>
																</a>
															</h5>
															
															<?php if ( $ef_expd ) { ?>
															
																<div class="ef-portfolio-tags text-center hide-for-print">
																	<?php the_excerpt();  ?>
																</div><!-- .ef-portfolio-tags.text-center.hide-for-print -->
																		
															<?php } elseif ( $ef_expd2 ) { ?>
															
																<div class="ef-portfolio-tags text-center hide-for-print">
																	<?php $links = array(); $v = 0; ?>
																	<?php foreach( $tags as $tag ) { ?>
																		<?php $v++; ?>
																		<?php if ( $v <= 4 ) { ?>
																			<?php $links[] = '<a href="'.get_term_link($tag->slug, $tag->taxonomy) .'">'.$tag->name.'</a>'; ?>	
																		<?php } ?>		
																	<?php } ?>
																	<?php $links = implode( ', ', $links ); ?>
																	<?php echo( $links ); ?>
																</div><!-- .ef-portfolio-tags.text-center.hide-for-print -->
																	
															<?php } ?>
															
														</div><!-- .ef-item-inner -->
															    											
													</div><!-- #post-## -->
												</div><!-- .ef-portfolio-item-inner -->
											</article>
											
										<?php } ?>
									
									<?php endwhile; // end of the loop. ?>
								
								</section><!-- .ef-portfolio-wrap.clearfix -->
								
								<?php /* Pagination */ ?>
								<?php ef_theme_pagination( $postslist->max_num_pages, $range = 2, $ef_data ); ?>
								
							</div><!-- #ef-portfolio -->
							
						<?php } // have_posts() ?>
						
					</div><!-- .large-12.columns -->
				</section><!-- .row -->
			</div><!-- .large-##.columns -->
		
			<?php if ( $layout['0'] != 12 ) { ?>
				<div class="ef-sidebars hide-for-print">
					<div class="ef-sidebar large-<?php echo $layout['1'] ?> columns">
						<?php get_sidebar(); ?>
				  	</div><!-- .ef-sidebar.large-##.columns -->
			  	</div><!-- .ef-sidebars.hide-for-print -->
			<?php } ?>
		
		</section><!-- .row -->
		
<?php get_footer(); ?>
