<?php
/**
 * Template Name: Home 
 *
 * @package Favea
 * @since Favea 1.0
 */

?>

<?php get_header(); ?>

<?php $ef_data = EfGetOptions::TplConditions(); ?>

<?php $layout = $ef_data['layouts']; ?>

		<?php if ( !empty( $ef_data['welcome'] ) ) { ?>
			<div id="ef-welcome" class="ef-bottom-2_8">
				<div class="row">
					<div class="large-12 columns text-center">
						<?php _e( $ef_data['welcome'] ); ?>
					</div>
				</div>
			</div>
		<?php } ?>

		<section class="row">
			<div class="large-<?php echo $layout['0'] ?> columns">
			
				<?php if ( !empty( $ef_data['layout'] ) ) { ?>
					
					<?php foreach ( $ef_data['layout'] as $key_cndn => $each_val ) { ?>
					    
					    <?php switch( $key_cndn ) {
					
							case 'ef_extras': ?>
							
								<?php get_template_part( 'loop', 'extras' ); ?>
						    	
						    <?php break; ?>
						    
						    <?php case 'ef_latest': ?>
						    
						    	<!-- Recent Works -->
					    				
	    						<?php $args = array(
						    		'post_type' => $ef_data['cpt_support'] ? EF_CPT : '',
						    		'tax_query' => array(
						    			array('taxonomy'	=> $ef_data['cpt_support'] ? EF_CPT_TAX_CAT : '',
						    			 	  'terms'		=> $ef_data['terms'],
						    			 	  'field'		=> 'id',
						    			 	  'operator'	=> 'NOT IN',
						    			)
						    		),
						    		'posts_per_page'		=> isset( $ef_data['mb_support'] ) ? $ef_data['num_latest'] : -1,
						    		'meta_key' 				=> '_thumbnail_id',
						    		'ignore_sticky_posts' 	=> true,
						    	); ?>
	
	
						    	<?php /* New wp_query object for the the Recent Works */ ?>					    	
						    	<?php $postslist = NULL; $postslist = new WP_Query( $args ); ?>
						    	
						    	<?php if ( ! $postslist->have_posts() || count( $postslist->posts ) < 2 ) { ?>
						    			    							    		
					    			<?php ef_get_msg_data( $postslist->query_vars['post_type'], $ef_data ); ?>	
					    							    	
						    	<?php } else { ?>
								    	
									<div class="row">
										<div class="large-12 columns">
											
											<?php if ( ( isset( $ef_data['mb_support'] ) && $ef_data['latest_list'] == 'list' ) || !isset( $ef_data['mb_support'] ) ) { ?>
												<?php if ( $layout['0'] != '12' ) { ?>
													<?php $chunked_posts = array_chunk( $postslist->posts, 3 ); ?>
												<?php } else { ?>
													<?php $chunked_posts = array_chunk( $postslist->posts, 4 ); ?>
												<?php } ?>
											<?php } else { ?>
												<?php $chunked_posts = array( $postslist->posts ); ?>
											<?php } ?>
											
											<section class="row">
										    	<div class="large-9 small-9 columns">
													
													<?php if ( !empty( $ef_data['txt_latest'] ) || !empty( $ef_data['latest_lnk'] ) ) { ?>
													
														<h4 class="ef-style-title"><?php echo !empty( $ef_data['txt_latest'] ) ? '<span>' . __( strip_tags( $ef_data['txt_latest'], '<span>' ) . '</span>' ) : ''; ?>
										    				<?php if ( !empty( $ef_data['latest_lnk'] ) ) { ?>
										    					<a title="<?php echo esc_attr( __( $ef_data['latest_lnk_title'] ) ); ?>" class="ef-portfolio-lnk ef-tipsy-w hide-for-print ef-radius" href="<?php echo $ef_data['latest_lnk']; ?>"></a>
										    				<?php } ?>
									    				</h4>
									    				
													<?php } ?>   			
											    	
						    					</div><!-- .large-9.small-9.columns  -->
						    					
						    					<?php foreach( $chunked_posts as $number ) { ?>
						    						<?php $work_count = count( $number ); ?>
						    					<?php } ?>
						    					
						    					<?php $col = $layout['0'] != '12' ? 3 : 4; ?>

						    					<?php if ( isset( $ef_data['latest_list'] ) && $ef_data['latest_list'] == 'carousel' && $work_count > $col ) { ?>
						    					
							    					<div class="large-3 small-3 columns text-right ef-carousel-nav hide-for-print">
							    						<a id="ef-prev" href="#"></a>
							    						<a id="ef-next" href="#"></a>
						    						</div><!-- .large-3 ... .ef-carousel-nav -->
						    						
						    					<?php } ?>
						    	
						    				</section><!-- .row  -->
						    				
						    				<?php foreach( $chunked_posts as $chunked_post ) : ?>
						    					
						    					<?php $num_posts = count( $chunked_post ); ?>
						    					
						    					<?php $tmp_columns = 12 / $num_posts; ?>
						    					<?php $columns = ( $num_posts <= 4 && ( isset( $ef_data['mb_support'] ) && $ef_data['latest_list'] != 'carousel' ) ) || !isset( $ef_data['mb_support'] ) ? 'large-'.$tmp_columns.' columns' : ''; ?>
						    					
							    				<section class="row">
							    					<div class="large-12 columns">
							    						<div class="ef-latest-works" class="clearfix">							    						
							    							<div class="<?php echo $columns != '' ? 'ef-latest-list' : 'ef-carousel'; ?>">
							    								
							    								<?php foreach( $chunked_post as $post ) : ?>
							    									
							    									<?php setup_postdata( $post ); ?>
							    									
							    									<?php if ( has_post_thumbnail()) { ?>
							    									
							    										<?php $tags = get_the_terms( $post->ID, EF_CPT_TAX_TAG ); ?>
							    										
								    									<article class="ef-portfolio-item <?php echo $columns; ?> <?php echo $ef_data['mb_support'] && $ef_data['lb_lnk'] == "img" ? 'ef-has-lightbox' : ''; ?> text-center ef-radius">						    	
								    										<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
								    																						
																				<?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'post_large_thumb' ); ?>
																				
																				<?php $thumb_small = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'cpt_thumb_large' ); ?>											
																				
																				<?php if ( isset( $ef_data['mb_support'] ) && $ef_data['lb_lnk'] == "img" ) { ?>
																					<a class="ef-proj-img" <?php if ( !post_password_required() ) { echo 'data-gal="lb[gal]"'; } ?> title="<?php the_title_attribute(); ?>" href="<?php echo !post_password_required() ? $thumb['0'] : the_permalink(); ?>">
																						
																						<div class="ef-img-overlay"><span class="ef-link-marker ef-radius"></span></div>

																				<?php } else { ?>
																					<a class="ef-proj-img" title="<?php the_title_attribute(); ?>" href="<?php the_permalink(); ?>">									
																				<?php } ?>
																						<?php echo get_the_post_thumbnail( $post->ID, 'cpt_thumb_large' ); ?>
																						<span class="ef-loader"><span></span></span>
																						<?php if ( post_password_required() ) { ?>
																							<span class="ef-pass"></span>
																						<?php } ?>
																					</a>					
																				
								    											
								    											<?php $ef_expd = !empty( $ef_data['thumb_tip'] ) && $ef_data['thumb_tip'] == 'excerpt' && $post->post_content != "" ? 1 : NULL; ?>
								    											
								    											<?php $ef_expd2 = !empty( $tags ) ? 1 : NULL; ?>
								    											
								    											<div class="ef-item-inner <?php echo !( $ef_expd || $ef_expd2 ) ? 'ef-no-expd' : ''; ?>">
								    											
								    												<h5 class="ef-no-margin ef-item-title">
								    													<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute() ?>"><?php the_title(); ?>
								    													</a>
								    												</h5>
								    												
								    												<?php if ( $ef_expd ) { ?>
								    												
								    													<div class="ef-portfolio-tags text-center hide-for-print">
								    														<?php the_excerpt();  ?>
								    													</div>
								    															
								    												<?php } elseif ( $ef_expd2 ) { ?>
								    												
								    													<div class="ef-portfolio-tags text-center hide-for-print">
								    														<?php $links = array(); $v = 0; ?>
								    														<?php foreach( $tags as $tag ) { ?>
								    															<?php $v++; ?>
								    															<?php if ( $v <= 4 ) { ?>
								    																<?php $links[] = '<a href="'.get_term_link($tag->slug, $tag->taxonomy) .'">'.$tag->name.'</a>'; ?>	
								    															<?php } ?>		
								    														<?php } ?>
								    														<?php $links = implode( ', ', $links ); ?>
								    														<?php echo( $links ); ?>
								    													</div>
								    														
								    												<?php } ?>
								    												
								    											</div><!-- .ef-item-inner -->					
								    										</div><!-- #post-## -->
								    									</article>
								    									
							    									<?php } // has_post_thumbnail() ?>
							    									
							    								<?php endforeach; ?>
							    								
							    								<?php wp_reset_postdata(); ?>
							    								
							    							</div><!-- .ef-carousel/.ef-latest-list -->	
							    						</div><!-- .ef-latest-works -->				
							    					</div><!-- .large-12.columns  -->
							    				</section><!-- .row  -->
							    				
							    			<?php endforeach; // have_posts ?>
							    			
							    			<?php if ( ( isset( $ef_data['mb_support'] ) && $ef_data['latest_list'] != 'carousel' ) || !isset( $ef_data['mb_support'] ) ) { ?>
						    					<hr class="ef-blank ef-bottom-2_18 hide-for-print">
						    				<?php } ?>
						    				
					    				</div><!-- .large-12.columns  -->
					    			</div><!-- .row  -->
					    			
					    			<hr class="print-only">
					    			
					    		<?php } // have_posts() ?>
					    						    
						    <?php break; ?>
	
						    <?php case 'ef_content': ?>
								
						    	<?php get_template_part( 'loop', 'page' ); ?>
	
						    <?php break; ?>

					    <?php } ?>
					<?php } ?>
				
				<?php } else { ?>
					
					<?php get_template_part( 'loop', 'page' ); ?>
			    	
				<?php } ?>
			
			</div><!-- .large-## columns -->
			
			<?php if ( $layout['0'] != 12 ) { ?>
				<div class="ef-sidebars hide-for-print">
					<div class="ef-sidebar large-<?php echo $layout['1'] ?> columns">
						<?php get_sidebar(); ?>
				  	</div><!-- .ef-sidebar.large-##.columns -->
			  	</div><!-- .ef-sidebars.hide-for-print -->
			<?php } ?>
			
		</section><!-- .row -->

<?php get_footer(); ?>
