<?php

/**
 * The Footer widget areas.
 *
 * @package Favea
 * @since Favea 1.0
 */
?>

<?php $ef_data = EfGetOptions::TplConditions(); ?>

<?php $footer = $ef_data['footer']; ?>

<?php if ( !isset( $ef_data['show_footer'] ) xor !empty( $ef_data['show_footer'] ) ) { ?>
	
	<div class="ef-footer-wrapper">
	
		<?php if ( $footer && is_active_sidebar( $footer ) ) { ?>
			
			<div class="row ef-widgets">
				<div class="large-12 columns">
					<div class="ef-widgets-inner">
						<div class="row">
							<?php dynamic_sidebar( $footer ); ?>
						</div><!-- .row -->
					</div><!-- .ef-widgets-inner -->
				</div><!-- .large-##.columns -->
			</div><!-- .row.ef-widgets -->

		<?php } elseif ( is_user_logged_in() ) { ?>

			<div class="row">
				<div class="large-12 columns">
					<div class="alert-box radius">
						<?php $link = sprintf( __( ' Ready to add widgets? <a href="%s"><abbr title="Add new">Get started here</abbr></a>', EF_TDM ), admin_url( 'widgets.php' ) ); ?>
						<p><?php printf( __( 'Please, add widgets to "Default footer widget area". %s', EF_TDM ), $link ); ?></p>
					</div><!-- .alert-box.radius -->
				</div><!-- .large-12.columns -->
			</div><!-- .row -->

		<?php } ?>
	</div><!-- .ef-footer-wrapper -->

<?php } ?>
