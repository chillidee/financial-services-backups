<?php

/**
 * The template for displaying Search Results pages.
 *
 * @package Favea
 * @since Favea 1.0
 */

?>

<?php get_header(); ?>

<?php $ef_data = EfGetOptions::TplConditions(); ?>
<?php $layout = $ef_data['layouts']; ?>

		<section class="row">
			<div class="large-<?php echo $layout['0'] ?> columns">

				<?php if ( have_posts() ) { ?>
					<?php get_template_part( 'loop', 'search' ); ?>

					<?php /* Pagination */ ?>
					<?php ef_theme_pagination( $wp_query->max_num_pages, $range = 2, $ef_data ); ?>

				<?php } else { ?>
					<div id="post-0" class="post no-results not-found">
						<h2 class="entry-title"><?php _e( 'Nothing Found', EF_TDM ); ?></h2>
						<div class="ef-entry-content">
							<p><?php _e( 'Sorry, but nothing matched your search criteria. Please try again with some different keywords.', EF_TDM ); ?></p>
							<div class="row">
								<div class="large-6 columns">
									<?php get_search_form(); ?>
								</div><!-- .large-6.columns -->
							</div><!-- .row -->
						</div><!-- .ef-entry-content -->
					</div><!-- #post-0 -->
				<?php } ?>
			
			</div><!-- .large-##.columns -->

			<?php if ( $layout['0'] != 12 ) { ?>
				<div class="ef-sidebars hide-for-print">
					<div class="ef-sidebar large-<?php echo $layout['1'] ?> columns">
						<?php get_sidebar(); ?>
				  	</div><!-- .ef-sidebar.large-##.columns -->
			  	</div><!-- .ef-sidebars.hide-for-print -->
			<?php } ?>

		</section><!-- .row -->

<?php get_footer(); ?>
