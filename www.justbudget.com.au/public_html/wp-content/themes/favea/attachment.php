<?php
/**
 * The template for displaying attachments.
 *
 * @package Favea
 * @since Favea 1.0
 */

?>

<?php get_header(); ?>

<?php $ef_data = EfGetOptions::TplConditions(); ?>
<?php $layout = $ef_data['layouts']; ?>

		<section class="row">

			<div class="large-<?php echo $layout['0'] ?> columns">

				<?php while ( have_posts() ) { ?>

					<?php the_post(); ?>

					<article id="post-<?php the_ID(); ?>" <?php post_class( 'image-attachment' ); ?>>

						<?php $attachments = array_values(
								get_children( array(
									'post_parent'	=> $post->post_parent,
									'post_status'	=> 'inherit',
									'post_type'		=> 'attachment',
									'post_mime_type'=> 'image',
									'order' 		=> 'ASC',
									'orderby'		=> 'menu_order ID'
								)
							)
						); ?>

						<?php foreach ( $attachments as $n => $attachment ) { ?>
							<?php if ( $attachment->ID == $post->ID ) { ?>
								<?php break; ?>
							<?php } ?>
						<?php } ?>

						<?php $n++; ?>
						<?php // If there is more than 1 attachment in a gallery ?>
						<?php if ( count( $attachments ) > 1 ) { ?>
							<?php if ( isset( $attachments[ $n ] ) ) { ?>
								<?php $next_attachment_url = get_attachment_link( $attachments[ $n ]->ID ); ?>
							<?php } else { $next_attachment_url = get_attachment_link( $attachments[ 0 ]->ID ); ?>
							<?php } ?>
						<?php } else { ?>
							<?php $next_attachment_url = wp_get_attachment_url(); ?>
						<?php } ?>

						<div class="ef-latest-thumb ef-proj-img">
							<a href="<?php echo esc_url( $next_attachment_url ); ?>" title="<?php the_title_attribute(); ?>">
								<?php echo wp_get_attachment_image( $post->ID, 'full' ); ?>
							</a>
							<?php edit_post_link( __( 'Edit Media', EF_TDM ), '<div class="ef-edit-attachment-link">', '</div>' ); ?>

							<nav class="ef-image-navigation">
								<span class="ef-previous-image"><?php previous_image_link( false ); ?></span>
								<span class="ef-next-image"><?php next_image_link( false ); ?></span>
							</nav><!-- .ef-image-navigation -->

						</div><!-- .ef-latest-thumb.ef-proj-img -->

						<header>
							<?php ef_theme_posted_on( $ef_data ); ?>
						</header>

						<?php if ( ! empty( $post->post_excerpt ) ) { ?>
							<div class="entry-caption">
								<?php the_excerpt(); ?>
							</div>
						<?php } ?>

						<?php the_content(); ?>
						<?php wp_link_pages( array( 'before' => '<div class="page-links">' . __( 'Pages:', EF_TDM ), 'after' => '</div>' ) ); ?>

					</article><!-- #post-## -->

					<?php comments_template(); ?>

				<?php } // end of the loop. ?>

			</div><!-- .large-## columns -->

			<?php if ( $layout['0'] != 12 ) { ?>
				<div class="ef-sidebars hide-for-print">
					<div class="ef-sidebar large-<?php echo $layout['1'] ?> columns">
						<?php get_sidebar(); ?>
				  	</div><!-- .ef-sidebar.large-##.columns -->
			  	</div><!-- .ef-sidebars.hide-for-print -->
			<?php } ?>

		</section><!-- .row -->

<?php get_footer(); ?>
