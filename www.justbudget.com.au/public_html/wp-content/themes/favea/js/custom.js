var ef = jQuery;
ef.noConflict();

ef(document).foundation();

ef(document).ready(function() {


	/* Add styles to WP's 'input#submit' */

	ef('input#submit').addClass('send button small ef-radius');



	/* Placeholders for IE8-9 */

	ef('input, textarea').placeholder();



	/* Prevent empty search */

	ef('.ef-searchform').submit(function(a) {
		var s = ef(this).find("#s");
		if (!s.val()) {
			a.preventDefault();
			s.focus();
		}
	});


	/* Hover for social icons */

	ef('.ef-soc-icons').find('a').append('<span></span>');



	/* Replace *.svg to *.png in browsers that don't support svg */

	if (!Modernizr.svg) {
		ef('img[src*="svg"]').attr('src', function() {
			return ef(this).attr('src').replace('.svg', '.png');
		});
	}


	/* Sticky Footer */

	function changeHeight() {
		ef('#ef-content').css('min-height', '');
		var sigma = ef(window).height() - ef('body').height();
		if (sigma > 0) ef('#ef-content').css('min-height', ef('#ef-content').outerHeight() + sigma - ef('html').offset().top - ef('#wpadminbar').height());
	}

	function SGaddEvent(obj, type, fn) {
		if (obj.addEventListener) {
			obj.addEventListener(type, fn, false);
		} else if (obj.attachEvent) {
			obj["e" + type + fn] = fn;
			obj[type + fn] = function() {
				obj["e" + type + fn](window.event);
			}

			obj.attachEvent("on" + type, obj[type + fn]);
		}
	}

	if (!ef('body').hasClass('ef-boxed-ver')) {
		SGaddEvent(window, 'load', changeHeight);
		SGaddEvent(window, 'resize', changeHeight);
	}


	/* Sticky post */

	function ef_fluid_width($target) {
		$target.css('width', $target.parent().width() + 'px');
	}


	/* Sticky main navigation */

	ef("#ef-navbar").parent().css({
		minHeight: ef("#ef-navbar").height()
	});

	if (ef('#ef-navbar').length > 0 && ef(window).width() > 940) {

		ef_fluid_width(ef('#ef-navbar'));

		var $PadMar = ef('.top-bar-section').css('paddingTop'),
			$LogoPadd = ef('.title-area .name').css('paddingTop'),
			$offTop = ef("body").hasClass("admin-bar") ? ef('#ef-navbar').offset().top - ef("#wpadminbar").height() : ef('#ef-navbar').offset().top;

		/* Scrollspy */
		ef('#ef-navbar').scrollspy({
			min: $offTop,
			onEnter: function(element, position) {
				ef("#ef-navbar").addClass('fixed');
				ef(".top-bar-section, .title-area .name").animate({
					paddingTop: '17px'
				});
				ef(".top-bar").animate({
					marginBottom: '17px'
				});
			},
			onLeave: function(element, position) {
				ef("#ef-navbar").removeClass('fixed');
				ef(".top-bar-section").animate({
					paddingTop: $PadMar
				});
				ef(".title-area .name").animate({
					paddingTop: $LogoPadd
				});
				ef(".top-bar").animate({
					marginBottom: $PadMar
				});
			},
			max: 99999
		});

		ef(window).resize(function() {
			ef_fluid_width(ef('#ef-navbar'));

			ef("#ef-navbar").parent().css({
				minHeight: ef("#ef-navbar").height()
			});
		});
	}

});

ef(window).load(function() {

	/* Wait for images */

	ef('.ef-proj-img').waitForImages(function() {
		ef(this).find('.ef-loader').fadeOut(1000);
	});



	/*Equal height*/
	ef.fn.equalHeight = function() {
		var group = this;
		ef(window).bind('resize', function() {
			var tallest = 0;
			ef(group).height('auto').each(function() {
				tallest = Math.max(tallest, ef(this).height());
			}).height(tallest);
		}).trigger('resize');

	};

	ef(".ef-grid-blog > .ef-post").equalHeight();
});