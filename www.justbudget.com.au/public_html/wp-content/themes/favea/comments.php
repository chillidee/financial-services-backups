<?php
/**
 * The template for displaying Comments.
 *
 * @package Favea
 * @since Favea 1.0
 */

?>
		<?php if ( have_comments() ) { ?>

			<div id="ef-comments">

				<?php if ( post_password_required() ) { ?>
					</div><!-- #ef-comments -->
				<?php return; ?>
				<?php } ?>

				<h3 class="ef-bottom-1_5">
					<?php printf( _n( '1 Comment to %2$s', '%1$s Comments to %2$s', get_comments_number(), EF_TDM ), number_format_i18n( get_comments_number() ), '"' . get_the_title() . '"' );?>
				</h3>

				<ul class="comments-list">
					<?php wp_list_comments( array( 'callback' => 'ef_theme_comments' ) ); ?>
				</ul>

				<?php if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) { ?>

					<nav class="ef-post-single-nav clearfix ef-bottom-1_5">
						<div class="ef-nav-previous left">
							<?php previous_comments_link( __( '<span class="meta-nav">&larr;</span> Older Comments', EF_TDM ) ); ?>
						</div><!-- .ef-nav-previous.left -->

						<div class="ef-nav-next right">
							<?php next_comments_link( __( 'Newer Comments <span class="meta-nav">&rarr;</span>', EF_TDM ) ); ?>
						</div><!-- .ef-nav-next.right -->

					</nav><!-- .ef-post-single-nav.clearfix.ef-bottom-1_5 -->
				<?php } ?>

			</div><!-- #ef-comments -->

		<?php } // end have_comments() ?>

		<?php comment_form(); ?>