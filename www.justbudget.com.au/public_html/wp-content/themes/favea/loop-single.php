<?php
/**
 * The loop that displays a single post.
 *
 * @package Favea
 * @since Favea 1.0
 */

?>

<?php $ef_data = EfGetOptions::TplConditions(); ?>

<?php if ( have_posts() ) { ?>

	<div class="ef-post">
		<div class="row">
			<div class="large-12 columns">

				<?php while ( have_posts() ) : the_post(); ?>

					<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

						<?php if ( ( !empty( $ef_data['ef_post_nav']['pp'] ) && $ef_data['cpt_support'] && is_singular( EF_CPT ) ) || ( !empty( $ef_data['ef_post_nav']['ot'] ) && $ef_data['cpt_support'] && is_singular( EF_CPT_TEAM ) ) || ( !empty( $ef_data['ef_post_nav']['ex'] ) && $ef_data['cpt_support'] && is_singular( EF_CPT_EXTR ) ) || ( !empty( $ef_data['ef_post_nav']['bp'] ) && is_singular( 'post' ) ) ) { ?>

							<?php ef_theme_post_nav(); ?>
							
						<?php } ?>

						<header>

							<?php if ( !post_password_required() ) { ?>

								<?php if ( $ef_data['mb_support'] && !empty( $ef_data['img_array'] ) ) { ?>

									<div class="ef-latest-thumb">

										<div class="ef-post-carousel">

											<?php $featured = $ef_data['mb_support'] && has_post_thumbnail() ? efmb_meta( 'ef_inc_featured_img', 'type=checkbox' ) : NULL; ?>
											<?php if ( !empty( $featured ) ) { ?>
												<div class="ef-slide">
													<div class="ef-blog-s3-img ef-proj-img">
														<?php echo $ef_data['layouts']['0'] != '12' ? get_the_post_thumbnail( $post->ID, 'post_medium_thumb' ) : get_the_post_thumbnail( $post->ID, 'post_large_thumb' ); ?>
														<span class="ef-loader"><span></span></span>
													</div><!-- .ef-blog-s3-img.ef-proj-img -->
												</div><!-- .ef-slide -->
											<?php } ?>
											<?php foreach ( $ef_data['img_array'] as $img ) { ?>
												<div class="ef-slide">
													<div class="ef-blog-s3-img ef-proj-img">
											   			<?php echo "<img src='{$img['full_url']}' alt='{$img['alt']}' />"; ?>
											   			<span class="ef-loader"><span></span></span>
											   		</div><!-- .ef-blog-s3-img.ef-proj-img -->
											    </div><!-- .ef-slide -->
											<?php } ?>

										</div><!-- .ef-post-carousel -->

										<div class="post-slider-direct-nav">
											<a href="#" class="post-slider-prev"></a>
											<a href="#" class="post-slider-next"></a>
										</div><!-- .post-slider-direct-nav -->

									</div><!-- .ef-latest-thumb -->

								<?php } elseif ( has_post_thumbnail() ) { ?>

									<div class="ef-latest-thumb ef-proj-img">
										<?php echo $ef_data['layouts']['0'] != '12' ? get_the_post_thumbnail( $post->ID, 'post_medium_thumb' ) : get_the_post_thumbnail( $post->ID, 'post_large_thumb' ); ?>
									</div><!-- .ef-latest-thumb.ef-proj-img -->

								<?php } ?>

							<?php } ?>

							<?php if ( !( $ef_data['cpt_support'] && ( $post->post_type == EF_CPT_EXTR || $post->post_type == EF_CPT_TEAM ) ) ) { ?>
								<?php ef_theme_posted_on( $ef_data ); ?>
							<?php } ?>

						</header>

						<?php the_content(); ?>
						<?php wp_link_pages( array( 'before' => '<div class="page-link">' . __( 'Pages:', EF_TDM ), 'after' => '</div>' ) ); ?>

						<?php ef_theme_author_info(); ?>

						<div class="ef-bottom-1_5 clearfix">
							<?php if ( !( $ef_data['cpt_support'] && ( $post->post_type == EF_CPT_EXTR || $post->post_type == EF_CPT_TEAM ) ) ) { ?>
								<?php ef_theme_posted_in( $ef_data ); ?>
							<?php } ?>
							<?php edit_post_link( __( 'Edit', EF_TDM ), '<span class="ef-edit-link right">', '</span>' ); ?>
						</div><!-- .ef-bottom-1_5.clearfix -->

					</div><!-- #post-## -->

					<?php comments_template( '', true ); ?>

				<?php endwhile; // end of the loop. ?>

			</div><!-- .large-12.columns -->
		</div><!-- .row -->
	</div><!-- .ef-post -->

<?php } ?>