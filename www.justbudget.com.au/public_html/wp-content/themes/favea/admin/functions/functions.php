<?php

/*
 * Performing basic setup
 *
 * @since	Favea 1.0
 */
add_action( 'after_setup_theme', 'ef_theme_setup_68163' );
function ef_theme_setup_68163() {

	// Thumbnails
	add_theme_support( 'post-thumbnails', current_theme_supports( 'ef-custom-post-types' ) ? array( 'post', EF_CPT, EF_CPT_TEAM ) : array( 'post' ) );

	// Add new image sizes
	add_image_size( 'cpt_thumb', 455, 250, true );
	add_image_size( 'cpt_thumb_large', 730, 401, true );
	add_image_size( 'post_large_thumb', 1172, 9999 );
	add_image_size( 'post_medium_thumb', 858, 9999 );
	add_image_size( 'small_thumb', 183, 101, true );

	load_theme_textdomain( EF_TDM, get_template_directory() . '/languages' );

	$locale = get_locale();
	$locale_file = get_template_directory() . "/languages/$locale.php";
	if ( is_readable( $locale_file ) ) {
		require_once $locale_file;
	}
	add_editor_style();

	add_theme_support( 'automatic-feed-links' );

	add_theme_support( 'menus' );
	register_nav_menus( array(
			'top'		=> __( 'Top Navigation', EF_TDM ),
			'primary'	=> __( 'Header Navigation', EF_TDM ),
			'secondary'	=> __( 'Footer Navigation', EF_TDM ),
		) );
}

add_filter( 'image_size_names_choose', 'ef_show_image_sizes' );
function ef_show_image_sizes( $sizes ) {
	$sizes['small_thumb']		= __( 'Custom Thumb 1', EF_TDM );
	$sizes['cpt_thumb']			= __( 'Custom Thumb 2', EF_TDM );
	$sizes['cpt_thumb_large']	= __( 'Custom Thumb 3', EF_TDM );
	$sizes['post_medium_thumb'] = __( 'Custom Thumb 4', EF_TDM );
	$sizes['post_large_thumb']	= __( 'Custom Thumb 5', EF_TDM );

	return $sizes;
}


/*
 * Max content width
 *
 * @since	Favea 1.0
 */
if ( !isset( $content_width ) ) {
	$content_width = 1201;
}



/*
 * Plugins
 *
 * @since	Favea 1.0
 */
add_action( 'tgmpa_register', 'ef_register_required_plugins_68163' );
function ef_register_required_plugins_68163() {

	$plugins_dir = get_template_directory() . '/inc/plugins/';

	$plugins = array(

		// Plugins pre-packaged with a theme
		array(
			'name'         		=> 'Fireform Theme Options',
			'slug'         		=> 'ef-theme-options',
			'source'       		=> $plugins_dir . 'ef-theme-options.zip',
			'required'     		=> true,
			'version'     		=> '1.0',
			'force_activation'  => false,
			'force_deactivation'  => true,
			'external_url'		=> '',
		),

		array(
			'name'				=> 'Fireform CPT',
			'slug'				=> 'ef-custom-posts',
			'source'			=> $plugins_dir . 'ef-custom-posts.zip',
			'required'			=> true,
			'version'			=> '1.0',
			'force_activation'  => false,
			'force_deactivation'  => true,
			'external_url'		=> '',
		),

		array(
			'name'				=> 'Fireform Meta Box',
			'slug'				=> 'ef-meta-box',
			'source'			=> $plugins_dir . 'ef-meta-box.zip',
			'required'			=> true,
			'version'			=> '1.0',
			'force_activation'  => false,
			'force_deactivation'  => true,
			'external_url'		=> '',
		),

		array(
			'name'				=> 'Fireform Shortcodes ',
			'slug'				=> 'ef-shortcodes',
			'source'			=> $plugins_dir . 'ef-shortcodes.zip',
			'required'			=> true,
			'version'			=> '1.0',
			'force_activation'  => false,
			'force_deactivation'  => false,
			'external_url'		=> '',
		),

		array(
			'name'				=> 'Revolution Slider',
			'slug'				=> 'revslider',
			'source'			=> $plugins_dir . 'revslider.zip',
			'required'			=> true,
			'version'			=> '3.0.95',
			'force_activation'  => false,
			'force_deactivation'  => false,
			'external_url'		=> '',
		),

		// Plugins from the WordPress Plugin Repository
		array(
			'name'				=> 'qTranslate',
			'slug'				=> 'qtranslate',
			'version'			=> '2.5.36',
			'required'			=> false,
		),

		array(
			'name'				=> 'Contact Form 7',
			'slug'				=> 'contact-form-7',
			'version'			=> '3.5.2',
			'required'			=> false,
		),

		array(
			'name'				=> 'Really Simple CAPTCHA',
			'slug'				=> 'really-simple-captcha',
			'version'			=> '1.7',
			'required'			=> false,
		),

		array(
			'name'				=> 'Intuitive Custom Post Order',
			'slug'				=> 'intuitive-custom-post-order',
			'version'			=> '2.0.6',
			'required'			=> false,
		),

		array(
			'name'				=> 'Rotating Tweets (Twitter widget and shortcode)',
			'slug'				=> 'rotatingtweets',
			'version'			=> '1.5.1',
			'required'			=> false,
		),
	);

	$config = array(
		'domain'				=> EF_TDM,
		'default_path'			=> '',
		'parent_menu_slug'		=> 'themes.php',
		'parent_url_slug'		=> 'themes.php',
		'menu'					=> 'install-required-plugins',
		'has_notices'			=> true,
		'is_automatic'			=> true,
		'message'				=> '',
	);

	tgmpa( $plugins, $config );

}


/*
 * Advanced Pagination. Ignore previous_posts_link() and next_posts_link() Theme Check's nug.
 * Credits: http://www.kriesi.at/archives/how-to-build-a-wordpress-post-pagination-without-plugin
 *
 * @since Favea 1.0
 */
function ef_theme_pagination( $pages = '', $range = 2, $ef_data = '' ) {

	global $paged;

	$showitems = ( $range * 2 )+1;
	if ( empty( $paged ) ) {
		$paged = 1;
	}
	if ( $pages == '' ) {
		global $wp_query;
		$pages = $wp_query->max_num_pages;
		if ( !$pages ) {
			$pages = 1;
		}
	}
	if ( 1 != $pages ) {

		if ( $ef_data['mb_support'] && isset( $ef_data['portf_type'] ) && $ef_data['portf_type'] == 'filtered' ) {
			echo '<div class="row"><div class="columns text-center"><a id="ef-addnewitems" class="button tiny ef-hollow radius hide-for-print" href="#">'.__( 'Load More Projects', EF_TDM ).'<span class="ef-loader"><span></span></span></a></div></div>';
		} else {
			echo "<div class='clear'></div>";
			echo "<ul class='pagination ef-pagination hide-for-print'>";
			if ( $paged > 2 && $paged > $range+1 && $showitems < $pages ) {
				echo "<li class='ef-prev-page'><a href='".get_pagenum_link( 1 )."'>&laquo;</a></li>";
			}
			if ( $paged > 1 && $showitems < $pages ) {
				echo "<li class='ef-prev-page'><a href='".get_pagenum_link( $paged - 1 )."'>&lsaquo;</a></li>";
			}

			for ( $i=1; $i <= $pages; $i++ ) {
				if ( 1 != $pages &&( !( $i >= $paged+$range+1 || $i <= $paged-$range-1 ) || $pages <= $showitems ) ) {
					echo ( $paged == $i ) ? '<li class="current"><a href="">'.$i.'</a></li>':"<li><a href='".get_pagenum_link( $i )."' class='inactive' >".$i."</a></li>";
				}
			}

			if ( $paged < $pages && $showitems < $pages ) {
				echo "<li class='ef-next-page'><a href='".get_pagenum_link( $paged + 1 )."'>&rsaquo;</a></li>";
			}
			if ( $paged < $pages-1 &&  $paged+$range-1 < $pages && $showitems < $pages ) {
				echo "<li class='ef-next-page'><a href='".get_pagenum_link( $pages )."'>&raquo;</a></li>";
			}
			echo "</ul>\n";
		}
	}
}

/*
 * Post navigation.
 *
 * @since Favea 1.0
 */
function ef_theme_post_nav() {
	global $post;

	$previous = ( is_attachment() ) ? get_post( $post->post_parent ) : get_adjacent_post( false, '', true );
	$next     = get_adjacent_post( false, '', false );

	if ( $next || $previous ) { ?>

		<nav class="ef-post-single-nav clearfix ef-bottom-1_5">

			<div class="ef-nav-previous left">
				<?php previous_post_link( '%link', _x( '<span class="meta-nav">&larr;</span> %title', 'Previous post link', EF_TDM ) ); ?>
			</div>

			<div class="ef-nav-next right">
				<?php next_post_link( '%link', _x( '%title <span class="meta-nav">&rarr;</span>', 'Next post link', EF_TDM ) ); ?>
			</div><!-- .ef-nav-next.right -->

		</nav><!-- .ef-post-single-nav.clearfix.ef-bottom-1_5 -->
		<?php
	}
}


/**
 * Template for comments and pingbacks/trackbacks.
 *
 * @since Favea 1.0
 */
function ef_theme_comments( $comment, $args, $depth ) {
	$GLOBALS['comment'] = $comment;

	if ( $comment->user_id == '0' ) {
		if ( !empty ( $comment->comment_author_url ) ) {
			$auth_url = $comment->comment_author_url;
		} else {
			$auth_url = '#';
		}
	} else {
		$auth_url = get_author_posts_url( $comment->user_id );
	}

	switch ( $comment->comment_type ) :
	case '' :

?>
			<li <?php comment_class(); ?> id="li-comment-<?php comment_ID(); ?>">
				<div id="comment-<?php comment_ID(); ?>" class="ef-comment ef-radius">
					<a href="<?php echo esc_url( $auth_url ); ?>" class="ef-avatar alignleft">
						<?php echo get_avatar( $comment, 60 ); ?>
					</a><!-- .ef-avatar -->
					<div class="comment post-comm"><a href="<?php echo esc_url( get_comment_link( $comment->comment_ID ) ); ?>">
						<div class="auth">
							<?php printf( __( '<strong>%1$s</strong> says:', EF_TDM ), get_comment_author( $comment->comment_ID ) ); ?>
								<span>
									<?php printf( __( 'On %1$s at %2$s', EF_TDM ), get_comment_date( 'F j, Y' ),  get_comment_time( 'g:i A' ) ); ?>

									<?php if ( $comment->comment_approved == '0' ) { ?>
										<span class="ef-text-color"><strong><?php _e( '(Your comment is awaiting moderation.)', EF_TDM ); ?></strong></span>
									<?php } ?>

								</span>
							</a>
							<?php edit_comment_link( __( 'Edit Comment', EF_TDM ), ' ' ); ?>
						</div>
						<?php comment_text(); ?>
						<?php comment_reply_link( array_merge( $args, array( 'depth' => $depth, 'max_depth' => $args['max_depth'] ) ) ); ?>
					</div><!-- .comment .post-comm -->
				</div><!-- #comment-##  -->
			</li>

		<?php break;
case 'pingback'  :
case 'trackback' :
	echo '<li class="post pingback"><p>'.__( 'Pingback:', EF_TDM ).' '.get_comment_author_link().' '.get_edit_comment_link( __( 'Edit Comment', EF_TDM ), ' ' ).'</p></li>';
	break;
	endswitch;
}


/**
 * Output custom page/post titles
 *
 * @since Favea 1.0
 */
function ef_page_title( $ef_data = '' ) {

	global $post;

	if ( $ef_data['mb_support'] && $ef_data['cpt_support'] && is_singular( EF_CPT_EXTR ) ) {
		$icon_extra = efmb_meta( 'ef_extra_icons', 'type=layout' );
		$title = sprintf( '<span class="ef-icon %1$s"><span></span></span>%2$s', $icon_extra, get_the_title() );
	} elseif ( is_search() ) {
		$title = sprintf( __( 'Search Results for: %s', EF_TDM ), '<span>' . get_search_query() . '</span>' );
	} elseif ( is_404() ) {
		$title = __( '404 - Not Found', EF_TDM );
	} elseif ( is_archive() ) {
		if ( is_day() ) {
			$title = sprintf( __( 'Daily Archives: <span>%s</span>', EF_TDM ), esc_html( get_the_date( 'F j, Y' ) ) );
		} elseif ( is_month() ) {
			$title = sprintf( __( 'Monthly Archives: <span>%s</span>', EF_TDM ), esc_html( get_the_date( _x( 'F Y', 'monthly archives date format', EF_TDM ) ) ) );
		} elseif ( is_year() ) {
			$title = sprintf( __( 'Yearly Archives: <span>%s</span>', EF_TDM ), esc_html( get_the_date( _x( 'Y', 'yearly archives date format', EF_TDM ) ) ) );
		} elseif ( is_author() ) {
			global $author; $userdata = get_userdata( $author );
			$title = sprintf( __( 'Author Archives: %s', EF_TDM ), "<span>" . $userdata->display_name . "</span>" );
		} elseif ( is_tax() || is_category() || is_tag() ) {
			$title = sprintf( __( '%1s Archives: %2s', EF_TDM ), is_category() || ( $ef_data['cpt_support'] && is_tax( EF_CPT_TAX_CAT ) ) || ( $ef_data['cpt_support'] && is_tax( EF_CPT_EXTR_TAX ) ) ? __( 'Category', EF_TDM ) : __( 'Tag', EF_TDM ), '<span>' . single_term_title( '', false ) . '</span>' );
		} else {
			$title = $ef_data['cpt_support'] && is_post_type_archive( EF_CPT ) ? __( 'Portfolio Archives', EF_TDM ) : __( 'Blog Archives', EF_TDM );
		}
	} else {
		$title = get_the_title();
	}

	echo $title;
}


/**
 * Get an author info (when it's specified)
 *
 * @since Favea 1.0
 */
function ef_theme_author_info() {

	global $post, $author;

	$author_id = get_post( $post->id )->post_author;

	if ( get_the_author_meta( 'description', $author_id ) ) {

		if ( is_single() ) {
			$infotmp = sprintf( '<a title="%1$s" href="%2$s">%3$s</a>',
				esc_attr( sprintf( __( 'View all posts by %s', EF_TDM ), get_the_author_meta( 'display_name', $author_id ) ) ),
				esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ),
				get_avatar( get_the_author_meta( 'user_email', $author_id ), apply_filters( 'favea_author_bio_avatar_size', 60 ) )
			);
		} else {
			$infotmp = sprintf( '%1$s',
				get_avatar( get_the_author_meta( 'user_email', $author_id ), apply_filters( 'favea_author_bio_avatar_size', 60 ) )
			);
		}

		if ( $post->post_type == 'post' ) {

			echo '<div class="ef-author-info ef-radius">
					<div class="ef-avatar alignleft">'.$infotmp.'</div>
					<div class="post-comm">
						<h4>'.sprintf( __( 'About %s:', EF_TDM ), get_the_author_meta( 'display_name', $author_id ) ).'</h4>
						<p class="ef-no-margin">'.get_the_author_meta( 'description', $author_id ).'</p>
					</div>
				</div>';

		}
	}
}


/**
 * Prints HTML with meta information for the current post-date/time and author.
 *
 * @since Favea 1.0
 */
function ef_theme_posted_on( $ef_data = '' ) {

	global $post, $author;

	$output = '<div class="ef-bloginfo"><div class="ef-bloginfo-inner">';

	$output .= '<ul class="inline-list ef-uppercase">';

	if ( ! ( isset( $ef_data['feed_layout'] ) && ( $ef_data['feed_layout'] == 'ef-blog-2' || $ef_data['feed_layout'] == 'ef-blog-4' ) ) ) {
		$output .= sprintf( '<li class="ef-bloginfo-date"><a href="%1$s" title="%2$s"><time class="entry-date" datetime="%3$s">%4$s</time></a></li>',
			esc_url( get_permalink() ),
			esc_attr( get_the_time( 'g:i A' ) ),
			esc_attr( get_the_date( 'c' ) ),
			esc_html( get_the_date( 'F j, Y' ) )
		);
	}

	if ( $post->post_type == 'post' ) {
		if ( is_author() ) {
			$output .= sprintf( '<li class="ef-bloginfo-author"> '.__( 'by', EF_TDM ).' %1$s</li>',
				get_the_author_meta( 'display_name' )
			);
		} else {
			$output .= sprintf( '<li class="ef-bloginfo-author"> '.__( 'by', EF_TDM ).' <a href="%1$s" title="%2$s">%3$s</a></li>',
				esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ),
				esc_attr( sprintf( __( 'View all posts by %s', EF_TDM ), get_the_author_meta( 'display_name' ) ) ),
				get_the_author_meta( 'display_name' )
			);
		}
	}

	if ( $ef_data['cpt_support'] && is_singular( EF_CPT ) ) {
		$termslist = get_the_term_list( $post->ID, EF_CPT_TAX_CAT, '', ', ', '' );
	} else {
		$termslist = get_the_term_list( $post->ID, 'category', '', ', ', '' );
	}

	if ( !empty( $termslist ) ) {
		$output .= sprintf( '<li class="ef-bloginfo-cat">In %1$s</li>', strip_tags( $termslist, '<a>' ) );
	}

	if ( $post->post_type == 'attachment' ) {
		$metadata = wp_get_attachment_metadata();
		$output .= sprintf( '<li class="ef-bloginfo-size">'.__( 'To', EF_TDM ).' <a href="%1$s" title="'.esc_attr( __( 'Link to full-size image', EF_TDM ) ).'">%2$s &times; %3$s</a></li>',
			esc_url( wp_get_attachment_url() ),
			$metadata['width'],
			$metadata['height']
		);
		$output .= sprintf( '<li class="ef-bloginfo-post">'.__( 'In the post', EF_TDM ).' <a href="%1$s" title="'.esc_attr( __( 'Return to %2$s', EF_TDM ) ).'">"%3$s"</a></li>',
			esc_url( get_permalink( $post->post_parent ) ),
			esc_attr( get_the_title( $post->post_parent ) ),
			get_the_title( $post->post_parent )
		);
	}

	if ( ! ( isset( $ef_data['feed_layout'] ) && ( $ef_data['feed_layout'] == 'ef-blog-2' || $ef_data['feed_layout'] == 'ef-blog-4' ) ) ) {
		$num_comments = get_comments_number();
		if ( comments_open() ) {
			if ( $num_comments == 0 ) {
				$comments = __( '0 Comments', EF_TDM );
			} elseif ( $num_comments > 1 ) {
				$comments = $num_comments . __( ' Comments', EF_TDM );
			} else {
				$comments = __( '1 Comment', EF_TDM );
			}
			$comments_num = $comments;
		} else {
			$comments_num =  $post->post_type == 'post' ? __( 'Comments are off for this post', EF_TDM ) : '';
		}

		$output .= sprintf( '<li class="ef-bloginfo-comment right">%1$s</li>', $comments_num );
	}

	$output .= '</ul>';
	$output .= '</div></div>';

	echo $output;
}


/**
 * Prints HTML with meta information for the current post (tags).
 *
 * @since Favea 1.0
 */
function ef_theme_posted_in( $ef_data = '' ) {

	global $post;

	if ( !is_attachment() ) {
		$tagged =  __( 'Tagged:', EF_TDM );
		$posted_in = '<div class="tagcloud"><ul class="wp-tag-cloud ef-posted-in"><li class="ef-taghead">%1$s</li> %2$s</ul></div>';

		if ( is_tax() || $ef_data['cpt_support'] && is_singular( EF_CPT ) ) {
			$tag_list = get_the_term_list( $post->ID, EF_CPT_TAX_TAG, '<li>', '</li><li>', '</li>' );
		} else {
			$tag_list = get_the_tag_list( '<li>', '</li><li>', '</li>' );
		}

		if ( !empty( $tag_list ) ) {
			printf( $posted_in, $tagged, $tag_list );
		}
	}
}


/**
 * Check if an array is empty.
 *
 * @since Favea 1.0
 */
function ef_array_empty( $mixed ) {
	if ( is_array( $mixed ) ) {
		foreach ( $mixed as $value ) {
			if ( !ef_array_empty( $value ) ) {
				return false;
			}
		}
	}
	elseif ( !empty( $mixed ) ) {
		return false;
	}
	return true;
}