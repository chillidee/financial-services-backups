<?php

class EfGetOptions {

    private static $data_ = array();

    public static function TplConditions() {
        return self::$data_;
    }

    public static function init() {

        // Retrive Theme Options data
        global $efto_data;

        // Redefine
        self::$data_ = $efto_data;

        // Theme support features
        self::$data_['mb_support']  = current_theme_supports( 'ef-meta-boxes' ) ? 1 : NULL;
        self::$data_['cpt_support'] = current_theme_supports( 'ef-custom-post-types' ) ? 1 : NULL;
        self::$data_['to_support']  = current_theme_supports( 'ef-theme-options' ) ? 1 : NULL;

        $cpt = self::$data_['cpt_support'];
        $mb = self::$data_['mb_support'];
        $to = self::$data_['to_support'];

        if ( $to ) {
            if ( !isset( self::$data_['ef_custom_logo'] ) && !isset( self::$data_['ef_custom_logo_svg'] ) ) {
                self::$data_['custom_logo'] = get_template_directory_uri() . '/images/logo.png';
            } else {
                self::$data_['custom_logo'] = !empty( self::$data_['ef_custom_logo_svg'] ) ? self::$data_['ef_custom_logo_svg'] : self::$data_['ef_custom_logo'];
            }            
        }

        // Get post meta
        if ( $mb ) {
            self::$data_['welcome']   = efmb_meta( 'ef_site_welcome', 'type=textarea' );
            self::$data_['posts_per'] = efmb_meta( 'ef_range', 'type=slider' );
            self::$data_['slider']    = efmb_meta( 'ef_slider', 'type=sliders' );
            self::$data_['lb_lnk']    = efmb_meta( 'ef_lb_link', 'type=radio' );
            self::$data_['thumb_tip'] = efmb_meta( 'ef_thumb_tip', 'type=select' );
            self::$data_['map_lat']   = efmb_meta( 'ef_map_lat', 'type=input' );
            self::$data_['map_long']  = efmb_meta( 'ef_map_long', 'type=input' );
            self::$data_['map_desc']  = efmb_meta( 'ef_map_dsc', 'type=input' );
            
            self::$data_['staticimagevideopopup_status'] = efmb_meta( 'ef_staticimagevideopopup_status', 'type=select' );
            self::$data_['staticimagevideopopup_image']  = efmb_meta( 'ef_staticimagevideopopup_image', 'type=input' );
            self::$data_['staticimagevideopopup_video']  = efmb_meta( 'ef_staticimagevideopopup_video', 'type=input' );
            
            $page_dsc                 = efmb_meta( 'ef_text', 'type=text' );
            $layouts                  = !is_archive() ? efmb_meta( 'ef_layout', 'type=layout' ) : NULL;
            $sidebar                  = efmb_meta( 'ef_sidebar', 'type=sidebars' );
            $footer                   = efmb_meta( 'ef_footer', 'type=sidebars' );

            if ( is_page_template() ) {
                self::$data_['extra_style']      = efmb_meta( 'ef_extras_style', 'type=radio' );
                self::$data_['txt_extras']       = efmb_meta( 'ef_text_extras', 'type=text' );
                self::$data_['extr_content']     = efmb_meta( 'ef_extr_content', 'type=radio' );
                self::$data_['num_latest']       = efmb_meta( 'ef_select_latest', 'type=slider' );
                self::$data_['txt_latest']       = efmb_meta( 'ef_text_latest', 'type=text' );
                self::$data_['latest_lnk']       = efmb_meta( 'ef_latest_lnk', 'type=cpt_link' );
                self::$data_['latest_lnk_title'] = efmb_meta( 'ef_latest_lnk_title', 'type=text' );
                self::$data_['latest_list']      = efmb_meta( 'ef_radio_latest', 'type=radio' );
                self::$data_['team_extr']        = efmb_meta( 'ef_team_extr', 'type=radio' );
                self::$data_['team_content']     = efmb_meta( 'ef_team_content', 'type=radio' );
            }

            self::$data_['img_array'] = efmb_meta( 'ef_upload_img', 'type=image_advanced' );

            if ( $cpt ) {
                $terms_extr = efmb_meta( 'ef_cat_extras', 'type=taxonomy&taxonomy='.EF_CPT_EXTR_TAX.'' );
                $terms      = efmb_meta( 'ef_cat_latest', 'type=taxonomy&taxonomy='.EF_CPT_TAX_CAT.'' );

                if ( is_page_template( 'templates/portfolio-template.php' ) ) {
                    self::$data_['portf_type'] = efmb_meta( 'ef_portf_type', 'type=radio' );
                    $terms                     = efmb_meta( 'ef_cat', 'type=taxonomy&taxonomy='.EF_CPT_TAX_CAT.'' );
                }
            }

            if ( is_page_template( 'templates/blog-template.php' ) ) {
                $terms = efmb_meta( 'ef_cat', 'type=taxonomy&taxonomy=category' );
            }

            self::$data_['feed_layout'] = efmb_meta( 'ef_feed_layout', 'type=layout' );
        }

        $cat_ids_extr = array();
        if ( !empty( $terms_extr ) ) {
            foreach ( $terms_extr as $term ) {
                $cat_ids_extr[] = $term->term_id;
            }
        }

        $cat_ids = array();
        if ( !empty( $terms ) ) {
            foreach ( $terms as $term ) {
                $cat_ids[] = $term->term_id;
            }
        }

        self::$data_['terms_extras'] = $cat_ids_extr;
        self::$data_['terms'] = $cat_ids;

        if ( is_page_template( 'templates/home-template.php' ) ) {
            if ( !isset( self::$data_['page_build']['enabled'] ) ) {
                $layout = array(
                    'ef_extras'     => 'Extras',
                    'ef_latest'     => 'Recent Works',
                    'ef_content'    => 'Page Content'
                );
            } else {
                $layout = self::$data_['page_build']['enabled'];
            }
        } else {
            $layout = array();
        }

        self::$data_['layout'] = $layout;


        // Page description
        if ( empty( $page_dsc ) ) {
            if ( $to ) {
                if ( isset( self::$data_['ef_page_dsc'] ) ) {
                    self::$data_['page_dsc'] = self::$data_['ef_page_dsc'];
                } else {
                    self::$data_['page_dsc'] = "A description of the entire page";
                }
            }
        } else {
            self::$data_['page_dsc'] = $page_dsc;
        }


        // Social icons
        self::$data_['socialize'] = array(
            'ef_twt'        => 'Twitter',
            'ef_fb'         => 'Facebook',
            'ef_in'         => 'LinkedIn',
            'ef_pin'        => 'Pinterest',
            'ef_drb'        => 'Dribbble',
            'ef_tumb'       => 'Tumblr',
            'ef_flick'      => 'Flickr',
            'ef_vim'        => 'Vimeo',
            'ef_delic'      => 'Delicious',
            'ef_goog'       => 'Google Plus',
            'ef_forr'       => 'Forrst',
            'ef_hi5'        => 'Hi5!',
            'ef_last'       => 'Last.fm',
            'ef_space'      => 'Myspace',
            'ef_newsv'      => 'Newsvine',
            'ef_pica'       => 'Picasa',
            'ef_tech'       => 'Technorati',
            'ef_rss'        => 'RSS',
            'ef_rdio'       => 'Rdio',
            'ef_share'      => 'ShareThis',
            'ef_skyp'       => 'Skype',
            'ef_slid'       => 'SlideShare',
            'ef_squid'      => 'Squidoo',
            'ef_stum'       => 'StumbleUpon',
            'ef_what'       => 'WhatsApp',
            'ef_wp'         => 'Wordpress',
            'ef_ytb'        => 'Youtube',
            'ef_digg'       => 'Digg',
            'ef_beh'        => 'Behance',
            'ef_yah'        => 'Yahoo',
            'ef_blogg'      => 'Blogger',
            'ef_hype'       => 'Hype Machine',
            'ef_groove'     => 'Grooveshark',
            'ef_sound'      => 'SoundCloud',
            'ef_insta'      => 'Instagram',
            'ef_vk'         => 'Vkontakte',
            'ef_mail'       => "Say 'Hello!'",
            'ef_phone'      => 'Make a call'
        );


        // Page layout
        if ( empty( $layouts ) ) {
            if ( $cpt && is_tax( EF_CPT_TAX_CAT ) ) {
                $layouts = !empty( self::$data_['ef_layout_cpt_archive'] ) ? self::$data_['ef_layout_cpt_archive'] : '93';
            } elseif ( $cpt && is_tax( EF_CPT_TAX_TAG ) ) {
                $layouts = !empty( self::$data_['ef_layout_cpt_tag_archive'] ) ? self::$data_['ef_layout_cpt_tag_archive'] : '93';
            } elseif ( is_archive() || is_search() ) {
                $layouts = !empty( self::$data_['ef_layout_archive'] ) ? self::$data_['ef_layout_archive'] : '93';
            } elseif ( is_single() ) {
                if ( ( $cpt && is_singular( EF_CPT ) ) && !empty( self::$data_['ef_layout_cpt_post'] ) ) {
                    $layouts = self::$data_['ef_layout_cpt_post'];
                } elseif ( !empty( self::$data_['ef_layout_post'] ) ) {
                    $layouts = self::$data_['ef_layout_post'];
                } else {
                    $layouts = '93';
                }
            } elseif ( is_page_template( 'templates/blog-template.php' ) ) {
                if ( !empty( self::$data_['ef_blog_layout'] ) ) {
                    $layouts = self::$data_['ef_blog_layout'];
                } else {
                    $layouts = '93';
                }
            } elseif ( !empty( self::$data_['ef_layout'] ) ) {
                $layouts = self::$data_['ef_layout'];
            } else {
                $layouts = '12';
            }
        }

        if ( $layouts == '12' ) {
            $layouts = array( $layouts, $layouts );
        } elseif ( $layouts == '39' ) {
            $layouts = array( '9 right', '3' );
        } else {
            $layouts = str_split( $layouts );
        }

        self::$data_['layouts'] = $layouts;


        // Widget areas
        if ( empty( $sidebar ) ) {
            if ( is_single() ) {
                if ( ( $cpt && is_singular( EF_CPT ) ) && !empty( self::$data_['ef_post_cpt_sidebar'] ) ) {
                    $sidebar = self::$data_['ef_post_cpt_sidebar'];
                } elseif ( !empty( self::$data_['ef_post_sidebar'] ) ) {
                    $sidebar =  self::$data_['ef_post_sidebar'];
                } else {
                    $sidebar = 'primary-widget-area';
                }
            } elseif ( $cpt && is_tax( EF_CPT_TAX_CAT ) && !empty( self::$data_['ef_archive_cpt_sidebar'] ) ) {
                $sidebar =  self::$data_['ef_archive_cpt_sidebar'];
            } elseif ( $cpt && is_tax( EF_CPT_TAX_TAG ) && !empty( self::$data_['ef_archive_cpt_tag_sidebar'] ) ) {
                $sidebar =  self::$data_['ef_archive_cpt_tag_sidebar'];
            } elseif ( !empty( self::$data_['ef_blog_sidebar'] ) ) {
                $sidebar = self::$data_['ef_blog_sidebar'];
            } elseif ( !empty( self::$data_['ef_general_sidebar'] ) ) {
                $sidebar = self::$data_['ef_general_sidebar'];
            } else {
                $sidebar = 'primary-widget-area';
            }
        }

        if ( empty( $footer ) ) {
            if ( is_single() ) {
                if ( ( $cpt && is_singular( EF_CPT ) ) && !empty( self::$data_['ef_post_cpt_footer'] ) ) {
                    $footer = self::$data_['ef_post_cpt_footer'];
                } elseif ( !empty( self::$data_['ef_post_footer'] ) ) {
                    $footer =  self::$data_['ef_post_footer'];
                } else {
                    $footer = 'footer-widget-area';
                }
            } elseif ( $cpt && is_tax( EF_CPT_TAX_CAT ) && !empty( self::$data_['ef_archive_cpt_footer'] ) ) {
                $footer =  self::$data_['ef_archive_cpt_footer'];
            } elseif ( $cpt && is_tax( EF_CPT_TAX_TAG ) && !empty( self::$data_['ef_archive_cpt_tag_footer'] ) ) {
                $footer =  self::$data_['ef_archive_cpt_tag_footer'];
            } elseif ( !empty( self::$data_['ef_blog_footer'] ) ) {
                $footer = self::$data_['ef_blog_footer'];
            } elseif ( !empty( self::$data_['ef_general_footer'] ) ) {
                $footer = self::$data_['ef_general_footer'];
            } else {
                $footer = 'footer-widget-area';
            }
        }

        self::$data_['sidebar'] = $sidebar;
        self::$data_['footer'] = $footer;
    }
}