<?php

/*
 * Theme Stylesheets
 */
if ( !is_admin() ) {
	add_action( 'wp_print_styles', 'ef_theme_enqueue_css' );
}
function ef_theme_enqueue_css() {

	$template_url = get_template_directory_uri();

	wp_enqueue_style( 'ef-normalize', $template_url . '/css/normalize.min.css', array(), '', 'all' );

	$ef_data = EfGetOptions::TplConditions();

	/*--- Enqueue Google fonts stylesheets ---*/
	if ( isset( $ef_data['ef_content_font'] ) || isset( $ef_data['ef_heading_font'] ) ) {

		$body_face_tmp = $ef_data['ef_content_font']['face'];
		$heading_face_tmp = $ef_data['ef_heading_font']['face'];

		$default_fonts = array( "arial", "verdana", "trebuchet", "georgia", "times", "tahoma", "palatino", "helvetica" );

		$body_face = !in_array( $body_face_tmp, $default_fonts ) && !empty( $body_face_tmp ) ? $body_face_tmp : NULL;
		$heading_face = !in_array( $heading_face_tmp, $default_fonts ) && !empty( $heading_face_tmp ) ? $heading_face_tmp : NULL;

		$body_face_opt = ef_theme_font_options( $body_face );
		$face_b = !empty( $body_face ) ? str_replace( ' ', '+', trim( $body_face ) ) . $body_face_opt : NULL;
		$heading_face_opt = ef_theme_font_options( $heading_face );
		$face_h = !empty( $heading_face ) ? str_replace( ' ', '+', trim( $heading_face ) ) . $heading_face_opt : NULL;

		if ( $body_face != $heading_face && ( !empty( $body_face ) && !empty( $heading_face ) ) ) {
			wp_enqueue_style( 'ef-googleFonts-head', '//fonts.googleapis.com/css?family='.$face_h.'', false, null, 'screen' );
			wp_enqueue_style( 'ef-googleFonts-content', '//fonts.googleapis.com/css?family='.$face_b.'', false, null, 'screen' );
		} elseif ( !empty( $body_face ) ) {
			wp_enqueue_style( 'ef-googleFonts-content', '//fonts.googleapis.com/css?family='.$face_b.'', false, null, 'screen' );
		} elseif ( !empty( $heading_face ) ) {
			wp_enqueue_style( 'ef-googleFonts-head', '//fonts.googleapis.com/css?family='.$face_h.'', false, null, 'screen' );
		} else {
			$face = 'Lato:400,300,300italic,400italic,700,700italic,900,900italic';
		}
	}

	wp_enqueue_style( 'ef-print', $template_url . '/css/print.css', array(), '', 'print' );
	wp_enqueue_style( 'ef-foundation', $template_url . '/css/foundation.min.css', array(), '', 'screen' );
	wp_enqueue_style( 'ef-general', $template_url . '/css/style.css', array(), '', 'screen' );
	wp_enqueue_style( 'ef-responsive', $template_url . '/css/responsive.css', array(), '', 'screen' );
	wp_enqueue_style( 'ef-retina', $template_url . '/css/retina.css', array(), '', 'screen' );
	wp_enqueue_style( 'ef-entypo1', $template_url . '/inc/entypo/stylesheet.css', array(), '', 'screen' );
	wp_enqueue_style( 'ef-style', $template_url . '/style.css', array(), '', 'screen' );

	if ( class_exists( 'RevSlider' ) ) {
		wp_enqueue_style( 'ef-revo', $template_url . '/inc/revolution/stylesheet.css', array(), '', 'screen' );
	}

	global $wp_styles;
	wp_enqueue_style( 'ef-ie8', $template_url . '/css/ie8.css', array(), '' );
	$wp_styles->add_data( 'ef-ie8', 'conditional', 'lt IE 9' );

	if ( is_multisite() ) {
		$uploads = wp_upload_dir();
		wp_enqueue_style( 'ef-options', $uploads['baseurl'] . '/options.css', array(), '', 'screen' );
	} else {
		wp_enqueue_style( 'ef-options', $template_url . '/css/options.css', array(), '', 'screen' );
	}
	
	if ( EF_DEMO_CHANGER ) {
		wp_enqueue_style( 'demo-styles', $template_url . '/demo/demo-styles.php', array(), '', 'screen' );
	}
	
}


/*
 * Enqueue scripts
 */
if ( !is_admin() ) {
	add_action( 'init', 'ef_theme_enqueue_modernizr', -1 );
	add_action( 'wp_enqueue_scripts', 'ef_theme_enqueue_scripts' );
}
function ef_theme_enqueue_modernizr() {
	$template_url = get_template_directory_uri();
	wp_enqueue_script( 'modernizr', $template_url . '/js/custom.modernizr.js', array(), '', false );
}

function ef_theme_enqueue_scripts() {
	
	$template_url = get_template_directory_uri();

	global $wp_scripts;

	if ( !isset( $wp_scripts->registered[ 'jquery' ] ) ) {
		wp_enqueue_script( 'jquery' );
	}
	
	wp_enqueue_script( 'ef-html5', $template_url . '/js/ie8/html5.js', array(), '' );
	wp_enqueue_script( 'selectivizr', $template_url . '/js/ie8/selectivizr-min.js', array(), '' );
	$wp_scripts->add_data( 'ef-html5', 'conditional', 'lt IE 9' );
	$wp_scripts->add_data( 'selectivizr', 'conditional', 'lt IE 9' );

	wp_enqueue_script( 'ef-foundation', $template_url . '/js/foundation.min.js', array( 'jquery' ), '', true );
	wp_enqueue_script( 'ef-scrollspy', $template_url . '/js/jquery-scrollspy.min.js', array( 'jquery' ), '', true );

	if ( !class_exists( 'RevSlider' ) ) {
		wp_enqueue_script( 'ef-wait-for-images', $template_url . '/js/jquery.waitforimages.min.js', array( 'jquery' ), '', true );
		wp_enqueue_script( 'ef-easing', $template_url . '/js/jquery.easing.1.3.min.js', array( 'jquery' ), '', true );
	}

	wp_enqueue_script( 'ef-custom', $template_url . '/js/custom.js', array( 'jquery' ), '', true );
}