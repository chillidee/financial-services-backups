<?php

/* Ajax Post */

add_action( 'wp_ajax_ef_ajax_posts', 'ef_ajax_posts' );
add_action( 'wp_ajax_nopriv_ef_ajax_posts', 'ef_ajax_posts' );

function ef_ajax_posts() {

	$nonce = $_POST['postCommentNonce'];
	if ( !wp_verify_nonce( $nonce, 'ef_ajax-post-comment-nonce' ) ) {
		die ( 'Busted!' );
	} elseif ( 'POST' == $_SERVER['REQUEST_METHOD'] && !empty( $_POST['action'] ) ) {

		global $post;

		$args = array(
			'post_type' 		=> EF_CPT,
			'tax_query' 		=> array(
				array(
					'taxonomy'		=> EF_CPT_TAX_CAT,
					'terms'			=> !empty( $_POST['terms'] ) ? $_POST['terms'] : array(),
					'field'			=> 'id',
					'operator'		=> 'NOT IN'
				)
			),
			'posts_per_page'	=> $_POST['offset'],
			'offset'			=> $_POST['offset'],
			'meta_key'			=> '_thumbnail_id',
			'ignore_sticky_posts' => true,
		);

		$new_query = new WP_Query( $args ); ?>

		<?php while ( $new_query->have_posts() ) : $new_query->the_post(); ?>

			<?php $cats = get_the_terms( $new_query->post->ID, EF_CPT_TAX_CAT ); ?>
			<?php $cats = !empty( $cats ) ? $cats : $cats = array(); ?>
			<?php $tags = get_the_terms( $new_query->post->ID, EF_CPT_TAX_TAG ); ?>

			<article class="<?php foreach ( $cats as $cat ) echo esc_attr( $cat->slug ) . " "; ?>ef-portfolio-item text-center ef-radius <?php echo post_password_required() ? 'ef-protected-post' : ''; ?> <?php echo $_POST['lb_lnk'] == "img" ? 'ef-has-lightbox' : ''; ?>">

				<div class="ef-portfolio-item-inner">

					<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

						<?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id( $new_query->post->ID ), 'full' ); ?>

						<?php $thumb_small = wp_get_attachment_image_src( get_post_thumbnail_id( $new_query->post->ID ), 'cpt_thumb' ); ?>

						<?php if ( $_POST['lb_lnk'] == "img" ) { ?>
							<a class="ef-proj-img" <?php if ( !post_password_required() ) { echo 'data-gal="lb[gal]"'; } ?> title="<?php the_title_attribute(); ?>" href="<?php echo !post_password_required() ? $thumb['0'] : the_permalink(); ?>">
							
							<div class="ef-img-overlay"><span class="ef-link-marker ef-radius"></span></div>								
						<?php } else { ?>
							<a class="ef-proj-img" title="<?php the_title_attribute(); ?>" href="<?php the_permalink(); ?>">
						<?php } ?>
								<?php echo get_the_post_thumbnail( $new_query->post->ID, 'cpt_thumb' ); ?>
								<span class="ef-loader"><span></span></span>
								<?php if ( post_password_required() ) { ?>
									<span class="ef-pass"></span>
								<?php } ?>
							</a>

						<?php $ef_expd = !empty( $_POST['thumb_tip'] ) && $_POST['thumb_tip'] == 'excerpt' && $new_query->post->post_content != "" ? 1 : NULL; ?>

						<?php $ef_expd2 = !empty( $tags ) ? 1 : NULL; ?>

						<div class="ef-item-inner <?php echo !( $ef_expd || $ef_expd2 ) ? 'ef-no-expd' : ''; ?>">

							<h5 class="ef-no-margin ef-item-title">
								<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?>
								</a>
							</h5>

							<?php if ( $ef_expd ) { ?>

								<div class="ef-portfolio-tags text-center hide-for-print">
									<?php the_excerpt();  ?>
								<div>

							<?php } elseif ( $ef_expd2 ) { ?>

								<div class="ef-portfolio-tags text-center hide-for-print">
									<?php $links = array(); $v = 0; ?>
									<?php foreach ( $tags as $tag ) { ?>
										<?php $v++; ?>
										<?php if ( $v <= 4 ) { ?>
											<?php $links[] = '<a href="'.get_term_link( $tag->slug, $tag->taxonomy ) .'">'.$tag->name.'</a>'; ?>
										<?php } ?>
									<?php } ?>
									<?php $links = implode( ', ', $links ); ?>
									<?php echo $links; ?>
								<div>

							<?php } ?>

						</div><!-- .ef-item-inner -->

					</div>

				</div>

			</article>
		  <?php endwhile; ?>

	 <?php } ?>

  <?php exit;
}
