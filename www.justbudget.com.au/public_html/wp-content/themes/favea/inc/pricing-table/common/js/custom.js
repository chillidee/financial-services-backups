jQuery(document).ready(function(){

    var jQuerypricingContainer = jQuery('.pricing_container');

    jQuery('.ul_colors li a').on('click', function(){
        var jQueryparent = jQuery(this).parent();
        var cls = jQueryparent.data('class');

        jQuery('.ul_colors li').removeClass('selected');
        jQueryparent.addClass('selected');

        jQuerypricingContainer
            .removeClass('ochre pink red violet carmin cobalt green blue turquoise asparagus emerald olive brown orange yellow')
            .addClass(cls);

        return false;
    });

    jQuery('#selOpt .toggler').on('click', function(){

        var selOpt = jQuery(this).parent();

        if (!selOpt.hasClass('opened')){
            selOpt.animate({left: '0px'}, 500).addClass('opened');
        }
        else {
            selOpt.animate({left: '-250px'}, 500).removeClass('opened');
        }

    });

    if ( jQuery(window).outerWidth() < 1670 ) {
        jQuery('#selOpt').delay(1000).animate({left:'-250px'}, 500).removeClass('opened');
    }

    jQuery('#selOpt input.option').on('change', function(e){
        var _this = jQuery(this);
        var cls = _this.data('class');
        if (_this.prop('checked')) {
            jQuery('.pricing_container').addClass(cls);
        }
        else {
            jQuery('.pricing_container').removeClass(cls);
        }
    });


});