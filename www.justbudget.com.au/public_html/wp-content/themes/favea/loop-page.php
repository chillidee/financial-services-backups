<?php
/**
 * The loop that displays a page.
 *
 * @package Favea
 * @since Favea 1.0
 */
?>

<?php if ( have_posts() ) { ?>
	<?php while ( have_posts() ) : the_post(); ?>

		<?php if ( $post->post_content != "" ) { ?>

			<div class="row">
				<section class="large-12 columns">

					<div id="post-<?php the_ID(); ?>" <?php post_class( 'ef-bottom-1_25' ); ?>>
				    	<?php the_content(); ?>
					</div><!-- #post-## -->

				</section><!-- .large-12.columns -->
			</div><!-- .row -->

		<?php } ?>

	<?php endwhile; // end of the loop. ?>
<?php } ?>