<?php
/**
 * The main template file.
 *
 * @package Favea
 * @since Favea 1.0
 */

?>

<?php get_header(); ?>

<?php $ef_data = EfGetOptions::TplConditions(); ?>

	<section class="row">
		<div class="large-9 columns">
			<?php get_template_part( 'loop', 'index' ); ?>

			<?php /* Pagination */ ?>
			<?php ef_theme_pagination( $wp_query->max_num_pages, $range = 2, $ef_data ); ?>
		</div><!-- .large-9.columns -->

		<div class="ef-sidebars hide-for-print">
			<div class="ef-sidebar large-3 columns">
				<?php get_sidebar( 'primary-widget-area' ); ?>
			</div><!-- .ef-sidebar.large-3.columns -->
	  	</div><!-- .ef-sidebars.hide-for-print -->
	 </section><!-- .row -->

<?php get_footer(); ?>