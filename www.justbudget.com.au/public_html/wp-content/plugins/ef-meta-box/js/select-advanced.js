/**
 * Update select2
 * Used for static & dynamic added elements (when clone)
 */
jQuery( document ).ready( function ( $ )
{	
	$( ':input.efmb-select-advanced' ).each( efmb_update_select_advanced );
	$( '.efmb-input' ).on( 'clone', ':input.efmb-select-advanced', efmb_update_select_advanced );
	
	function efmb_update_select_advanced()
	{
		var $this = $( this ),
			options = $this.data( 'options' );
		$this.siblings('.select2-container').remove();
		$this.select2( options );	
	}
} );