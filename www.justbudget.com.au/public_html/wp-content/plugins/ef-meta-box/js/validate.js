jQuery( document ).ready( function( $ )
{
	var $form = $( '#post' );

	// Required field styling
	$.each( efmb.validationOptions.rules, function( k, v )
	{
		if ( v['required'] )
			$( '#' + k ).parent().siblings( '.efmb-label' ).addClass( 'required' ).append( '<span>*</span>' );
	} );

	efmb.validationOptions.invalidHandler = function( form, validator )
	{
		// Re-enable the submit ( publish/update ) button and hide the ajax indicator
		$( '#publish' ).removeClass( 'button-primary-disabled' );
		$( '#ajax-loading' ).attr( 'style', '' );
		$form.siblings( '#message' ).remove();
		$form.before( '<div id="message" class="error"><p>' + efmb.summaryMessage + '</p></div>' );
	};

	$form.validate( efmb.validationOptions );
} );