/**
 * Update datetime picker element
 * Used for static & dynamic added elements (when clone)
 */
jQuery( document ).ready( function( $ )
{
	$( ':input.efmb-datetime' ).each( efmb_update_datetime_picker );
	$( '.efmb-input' ).on( 'clone', ':input.efmb-datetime', efmb_update_datetime_picker );
	
	function efmb_update_datetime_picker()
	{
		var $this = $( this ),
			options = $this.data( 'options' );
	
		$this.siblings( '.ui-datepicker-append' ).remove();         // Remove appended text
		$this.removeClass( 'hasDatepicker' ).attr( 'id', '' ).datetimepicker( options );
	
	}
} );
