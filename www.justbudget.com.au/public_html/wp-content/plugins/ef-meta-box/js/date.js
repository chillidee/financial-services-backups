/**
 * Update date picker element
 * Used for static & dynamic added elements (when clone)
 */
jQuery( document ).ready( function( $ )
{
	$( ':input.efmb-date' ).each( efmb_update_date_picker );
	$( '.efmb-input' ).on( 'clone', ':input.efmb-date', efmb_update_date_picker );
	
	function efmb_update_date_picker()
	{
		var $this = $( this ),
			options = $this.data( 'options' );
	
		$this.siblings( '.ui-datepicker-append' ).remove();         // Remove appended text
		$this.removeClass( 'hasDatepicker' ).attr( 'id', '' ).datepicker( options );
	}

} );
