var ef = jQuery;
ef.noConflict();

/*-------------------------------------------------------*/
/*------------------- Main Variables --------------------*/
/*-------------------------------------------------------*/

/* Lightbox Class */
var $lightB = ef("a[data-gal^='lb']");

/* Setting Up The Map */

var $zoomLevel = 15;

/*-------------------------------------------------------*/
/*-------------------------------------------------------*/
/*-------------------------------------------------------*/


ef(document).ready(function() {

	/*prettyPhoto*/
	$lightB.prettyPhoto({
		hook: 'data-gal',
		counter_separator_label: ' of ',
		overlay_gallery: false,
		social_tools: false
	});

	/* Portfolio lightbox */
	if (ef('.ef-portfolio-item').find('.ef-link-marker').length > 0) {
		ef('.ef-portfolio-item').addClass('ef-has-lightbox');
	}

	if (typeof(ef_map_vars) !== 'undefined') {

		var markers = ef_map_vars;

		/*gMap Settings*/
		if (markers.length > 1) {
			ef.extend(ef.ui.gmap.prototype, {
				pagination: function(prop) {
					var $el = ef("<div id='ef-map-pag' class='ef-map-pag'><div class='btn back-btn'></div><div class='ef-place'></div><div class='btn fwd-btn'></div></div>");
					var self = this,
						i = 0;
					prop = prop || 'title';
					self.set('pagination', function(a, b) {
						if (a) {
							i = i + b;
							$el.find('.ef-place').text(self.get('markers')[i][prop]);
							self.get('map').panTo(self.get('markers')[i].getPosition());
						}
					});
					self.get('pagination')(true, 0);
					$el.find('.back-btn').click(function() {
						self.get('pagination')((i > 0), -1, this);
					});
					$el.find('.fwd-btn').click(function() {
						self.get('pagination')((i < self.get('markers').length - 1), 1, this);
					});
					self.addControl($el, google.maps.ControlPosition.BOTTOM_LEFT);
				}
			});
		}

		ef('#map_canvas').gmap({
			'zoom': $zoomLevel,
			'scrollwheel': false,

			/* Delete these 7 lines if you need default styling */
			'styles': [{
				stylers: [{
					lightness: 3
				}, {
					saturation: -100
				}]
			}],
			/*--*/

			'callback': function() {
				var self = this;
				ef.each(markers, function(i, marker) {
					self.addMarker(marker).click(function() {
						if (markers.length == 1) {
							self.openInfoWindow({
								'content': this.title
							}, this);
						}
					});
				});
			}
		}).gmap('pagination', 'title');
	}

});