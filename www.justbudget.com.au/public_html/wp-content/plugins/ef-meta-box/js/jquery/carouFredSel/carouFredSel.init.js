var ef = jQuery;
ef.noConflict();

/*-------------------------------------------------------*/
/*------------------- Main Variables --------------------*/
/*-------------------------------------------------------*/

/* Latest Works Carousel class*/
var $worksCarousel = ef(".ef-carousel");

/* Clients Carousel class */
var $clientsCarousel = ef(".ef-clients-carousel");

/* Post carousel */
var $postCarousel = ef(".ef-post-carousel");

/*-------------------------------------------------------*/
/*-------------------------------------------------------*/
/*-------------------------------------------------------*/

ef(document).ready(function() {

	/*Blog Post carousel*/

	if (typeof(ef_slider_vars2) !== 'undefined' && $postCarousel.length > 0) {

		$auto = ef_slider_vars2.autoplay && ef_slider_vars2.autoplay == '1' ? true : false;
		$infinite = ef_slider_vars2.infinite && ef_slider_vars2.infinite == '1' ? true : false;
		$duration = ef_slider_vars2.duration ? parseInt(ef_slider_vars2.duration, null) : 2000;
		$fx = ef_slider_vars2.fx ? ef_slider_vars2.fx : 'slide';
		$direction = ef_slider_vars2.direction ? ef_slider_vars2.direction : 'left';

		$postCarousel.carouFredSel({
			responsive: true,
			transition: true,
			direction: $direction,
			prev: '.post-slider-prev',
			next: '.post-slider-next',
			scroll: {
				fx: $fx,
				pauseOnHover: true,
				duration: 1000,
				timeoutDuration: $duration,
				easing: 'easeInOutExpo'
			},
			circular: $infinite,
			auto: $auto,
			swipe: {
				onTouch: true
			},
			items: {
				height: 'variable'
			}
		});

	}

	/*Clients carousel*/

	if (typeof(ef_slider_vars2) !== 'undefined' && $clientsCarousel.length > 0) {

		$auto1 = ef_slider_vars2.autoplay_cl && ef_slider_vars2.autoplay_cl == '1' ? true : false;

		$clientsCarousel.carouFredSel({
			width: '100%',
			transition: true,
			height: 'variable',
			scroll: {
				duration: 1000,
				pauseOnHover: true,
				timeoutDuration: 2500,
				easing: 'easeInOutExpo'
			},
			auto: $auto1,
			prev: "#ef-prev1",
			next: "#ef-next1",
			swipe: {
				onTouch: true
			}
		});
	}

});

ef(window).load(function() {

	/*Latest Works carousel*/

	if ($worksCarousel.length > 0) {

		$worksCarousel.carouFredSel({
			responsive: true,
			transition: true,
			circular: false,
			scroll: {
				items: 1,
				duration: 800,
				timeoutDuration: 600,
				easing: 'easeInOutExpo'
			},
			prev: {
				button: '#ef-prev',
				key: "left"
			},
			next: {
				button: '#ef-next',
				key: "right"
			},
			auto: false,
			items: {
				height: 'variable',
				visible: {
					min: 1,
					max: 4
				}
			},
			swipe: {
				onTouch: true
			}
		});
	}

});