/**
 * Update datetime picker element
 * Used for static & dynamic added elements (when clone)
 */
jQuery( document ).ready( function( $ )
{
	$( ':input.efmb-time' ).each( efmb_update_time_picker );
	$( '.efmb-input' ).on( 'clone', ':input.efmb-time', efmb_update_time_picker );
	
	function efmb_update_time_picker()
	{
		var $this = $( this ),
			options = $this.data( 'options' );
	
		$this.siblings( '.ui-datepicker-append' ).remove();         // Remove appended text
		$this.removeClass( 'hasDatepicker' ).attr( 'id', '' ).timepicker( options );
	
	}
} );
