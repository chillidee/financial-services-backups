<?php

/* CPTs */

$cpt = 'portfolios';
$cpt_slug = 'project';
$cpt_tax_cat = 'portfolio-category';

$cpt_extr = 'extras';
$cpt_tax_cat_extr = 'extra-category';

$cpt_team = 'team';
$cpt_tax_cat_team = 'team-category';

/* STD fix */
function ef_set_default_meta_new_post( $post_ID ) {

	if ( get_post_type( $post_ID ) == 'page' ) {

		$prefix = "ef_";

		$stds = array(
			"templates/home-template.php" 	=> array(
				"{$prefix}site_welcome"  	=> "<h1>Hi There! I'm Favea - A Premium Multipurpose Wordpress Theme</h1>",
				"{$prefix}text_latest"  	=> "Recent Works",
				"{$prefix}extras_style"  	=> "ef-extr-1",
				"{$prefix}extr_content"  	=> "ef-excerpt",
				"{$prefix}select_latest" 	=> 4,
				"{$prefix}radio_latest"  	=> "list",
				"{$prefix}lb_link"   		=> "post",
				"{$prefix}thumb_tip"  		=> "tags",
				"{$prefix}latest_lnk_title" => "Go to Portfolio",
			),
			"templates/portfolio-template.php" => array(
				"{$prefix}lb_link"			=> "post",
				"{$prefix}portf_type"  		=> "regular",
				"{$prefix}feed_layout"  	=> "ef-4-col-portfolio",
				"{$prefix}thumb_tip"  		=> "tags",
				"{$prefix}range"   			=> 8,
			),
			"templates/regular-template.php" => array(
				"{$prefix}team_extr"  		=> "none",
				"{$prefix}text_extras"  	=> "Our services",
				"{$prefix}extras_style"  	=> "ef-extr-1",
				"{$prefix}extr_content"  	=> "ef-excerpt",
				"{$prefix}text_latest"  	=> "Meet our team",
				"{$prefix}radio_latest"  	=> "list",
				"{$prefix}team_content"  	=> "ef-excerpt",
			),
			"templates/blog-template.php" 	=> array(
				"{$prefix}feed_layout"  	=> "ef-blog-1",
				"{$prefix}range"   			=> 5,
				"{$prefix}extr_content"  	=> "ef-excerpt",
			),
		);

		foreach ( $stds as $tpl => $param ) {

			$template = get_post_meta( $post_ID, '_wp_page_template' );
			if ( in_array( $tpl, $template ) ) {

				foreach ( $param as $std => $val ) {
					$current_field_value = get_post_meta( $post_ID, $std );
					if ( isset( $current_field_value ) && !wp_is_post_revision( $post_ID ) ) {
						$default_meta = $val;
						update_post_meta( $post_ID, $std, $default_meta );
					}
				}
			}
		}
	}
}

add_action( 'save_post', 'ef_set_default_meta_new_post' );


/**
 * Registering meta boxes
 */

/********************* META BOX DEFINITIONS ***********************/

$prefix = 'ef_';

global $meta_boxes;
$meta_boxes = array();

// Check if plugins are active
function ef_is_plugin_active( $plugin ) {
	return in_array( $plugin, (array) get_option( 'active_plugins', array() ) );
}
$efto = ef_is_plugin_active( 'ef-theme-options/ef-theme-options.php' ) ? 1 : NULL;
$ef_cpt = ef_is_plugin_active( 'ef-custom-posts/post-types.php' ) ? 1 : NULL;

// Extra icons
$extra_icn = array();
for ( $n = 1; $n <= 223; $n++ ) {
	$extra_icn["ef-icn-$n"] = "ef-icn-$n";
}

// Header settings
$meta_boxes[] = array(

	'id' 			=> 'header_mb',
	'title' 		=> __( 'Header Settings', 'efmb' ),
	'pages' 		=> array( 'page', 'post', $cpt, $cpt_extr, $cpt_team ),
	'context' 		=> 'normal',
	'priority' 		=> 'high',
	'autosave' 		=> true,
	'fields' 		=> array(
		// TEXT
		array(
			'name'  => __( 'Page Description', 'efmb' ),
			'id'    => "{$prefix}text",
			'desc'  => __( 'Description of the entire page. Applies after the name of a page. It overrides "Global page description" that specified in Theme Options.', 'efmb' ),
			'type'  => 'text',
		),
	),
	'exclude_on'    => array(
		'template'	=> array( 'templates/home-template.php' )
	)
);

// Slider meta box
$meta_boxes[] = array(

	'id'			=> 'slider_mb',
	'title'			=> __( 'Slider Settings', 'efmb' ),
	'pages'			=> array( 'page' ),
	'context'		=> 'normal',
	'priority'		=> 'high',
	'autosave'		=> true,
	'fields'		=> array(
		array(
			'name'    => ' ',
			'desc'    => __( 'Check documentation if you see "Revolution Slider is not installed". Create at least one Slider if you see "No sliders available".', 'efmb' ),
			'id'      => "{$prefix}slider",
			'type'    => 'sliders',
		),
	)
);

// Static Image Video Popup Settings meta box
$meta_boxes[] = array(

	'id'			=> 'staticimagevideopopup_mb',
	'title'			=> __( 'Static Image Video Popup Settings', 'efmb' ),
	'pages'			=> array( 'page' ),
	'context'		=> 'normal',
	'priority'		=> 'high',
	'autosave'		=> true,
	'fields'		=> array(
		array(
				'name'    => __( 'Static Image Video Enable/Disable', 'efmb' ),
				'desc'    => __( 'Display a static image with a video popup (Enable this will disable Slider!).', 'efmb' ),
				'id'      => "{$prefix}staticimagevideopopup_status",
				'options' => array(
					"enable"   => "Enable",
				),
				'type'    => 'select',
				'std'		=> 'Disable',
			),
		array(
				'name'  => __( 'Static Image URL', 'efmb' ),
				'id'    => "{$prefix}staticimagevideopopup_image",
				'desc'  => __( 'Specify the Static Image URL to show up to replace slider.', 'efmb' ),
				'type'  => 'text',
			),
		array(
				'name'  => __( 'Youtube Video URL', 'efmb' ),
				'id'    => "{$prefix}staticimagevideopopup_video",
				'desc'  => __( 'Specify the Youtube Video Code here, eg: IjL7dNGM-Uc.', 'efmb' ),
				'type'  => 'text',
			),		
	)
);

// Welcome message meta box
$meta_boxes[] = array(

	'id' 		=> 'welcome_mb',
	'title' 	=> __( 'Welcome message', 'efmb' ),
	'pages' 	=> array( 'page' ),
	'context' 	=> 'normal',
	'priority' 	=> 'high',
	'autosave' 	=> true,
	'fields' 	=> array(
		array(
			'name'    => ' ',
			'desc'    => __( 'This colourful block is located below the header (slider). You are able to use different html tags such as &lt;h1&gt;...&lt;h6&gt;, &lt;kbd&gt;, &lt;cite&gt;, etc.', 'efmb' ),
			'id'      => "{$prefix}site_welcome",
			'type'    => 'textarea',
			'std'	  => "<h1>Hi There! I'm Favea - A Premium Multipurpose Wordpress Theme</h1>",
		),
	),
	'only_on'    	=> array(
		'template' 	=> array( 'templates/home-template.php' )
	)
);


if ( isset( $ef_cpt ) ) {

	// Meta boxes for portfolio
	$meta_boxes[] = array(
		'id' 		=> 'portfolio_mb',
		'title' 	=> __( 'Content Settings', 'efmb' ),
		'pages' 	=> array( 'page' ),
		'context' 	=> 'normal',
		'priority' 	=> 'high',
		'autosave' 	=> true,
		'fields' 	=> array(

			array(
				'name'	=> __( 'Layout', 'efmb' ),
				'desc'	=> __( 'Select a layout of the portfolio grid. Choose between 3, 4 and 5 columns.', 'efmb' ),
				'id'    => "{$prefix}feed_layout",
				'type'  => 'layout',
				'class' => 'ef-layout-check',
				'options' => array(
					"ef-3-col-portfolio" => efmb_IMG_URL . "3-col-portfolio.png",
					"ef-4-col-portfolio" => efmb_IMG_URL . "4-col-portfolio.png",
					"ef-5-col-portfolio" => efmb_IMG_URL . "5-col-portfolio.png",
				),
			),

			array(
				'name'    => __( 'Type', 'efmb' ),
				'desc'    => __( '"Regular" portfolio has a usual paginated layout. "Filtered" portfolio has no pagination and other pages are loading via Ajax.', 'efmb' ),
				'id'      => "{$prefix}portf_type",
				'options' => array(
					"regular"   => "Regular",
					"filtered"  => "Filtered",
				),
				'type'    => 'radio',
			),

			array(
				'name'    	=> __( 'Thumbnail link', 'efmb' ),
				'desc'    	=> __( 'Choose a link to the post or to a full size image of the thumbnail.', 'efmb' ),
				'id'      	=> "{$prefix}lb_link",
				'options' 	=> array(
					"post"  	=> "Post",
					"img"  		=> "Image",
				),
				'type'    	=> 'radio',
			),

			array(
				'name'    	=> __( 'Expandable area', 'efmb' ),
				'desc'    	=> __( 'Select what should be displayed in an expandable area or hide it.', 'efmb' ),
				'id'      	=> "{$prefix}thumb_tip",
				'options' 	=> array(
					"tags"  => "Tags",
					"excerpt"  	=> "Excerpt",
				),
				'type'    	=> 'select',
				'std'		=> '- Hide -',
			),

			array(
				'name' 	=> __( 'Posts Per Page', 'efmb' ),
				'id'   	=> "{$prefix}range",
				'type' 	=> 'slider',
				'js_options' 	=> array(
					'min'   	=> 3,
					'max'   	=> 99,
					'step'  	=> 1,
				),
			),

			array(
				'name'    	=> __( 'Exclude category posts', 'efmb' ),
				'desc'    	=> __( "Select categories you want to exclude. Empty categories are unavailable.", 'efmb' ),
				'id'      	=> "{$prefix}cat",
				'type'    	=> 'taxonomy',
				'options' 	=> array(
					'taxonomy' 	=> $cpt_tax_cat,
					'type' 		=> 'select_tree',
					'args' 		=> array( 'hide_empty' => true )
				),
				'multiple' 	=> true,
			),

		),

		'only_on'    => array(
			'template' => array( 'templates/portfolio-template.php' )
		)
	);

	// Meta boxes for regular page
	$meta_boxes[] = array(
		'id' 		=> 'page_mb',
		'title' 	=> __( 'Content Settings', 'efmb' ),
		'pages' 	=> array( 'page' ),
		'context' 	=> 'normal',
		'priority' 	=> 'high',
		'autosave' 	=> true,
		'fields' 	=> array(

			array(
				'name'    => __( 'Extras/Team', 'efmb' ),
				'desc'    => __( 'Select "Extra" or "Team" to display on the page below the main content or hide both.', 'efmb' ),
				'id'      => "{$prefix}team_extr",
				'type'    => 'radio',
				'options' => array(
					"none"   		=> __( "Hide", 'efmb' ),
					"show_team"  	=> __( "Team", 'efmb' ),
					"show_extras" 	=> __( "Extras", 'efmb' )
				),
			),

			array(
				'name'  => __( 'Extras', 'efmb' ),
				'type'	=> 'heading',
				'id'   	=> "fake_id",
			),

			array(
				'name'  => __( 'Title', 'efmb' ),
				'id'    => "{$prefix}text_extras",
				'desc'  => __( 'Specify a title of this content block on your site or leave blank to hide title.', 'efmb' ),
				'type'  => 'text',
			),

			array(
				'name'     => __( 'Extras style', 'efmb' ),
				'id'       => "{$prefix}extras_style",
				'desc'     => __( 'Choose a style of Extras. "Vertical" is default.', 'efmb' ),
				'type'     => 'radio',
				'options'  => array(
					"ef-extr-1" => __( "Style 1 (Vertical)", 'efmb' ),
					"ef-extr-2" => __( "Style 2 (Horizontal)", 'efmb' ),
				),
			),

			array(
				'name'    => __( 'Exclude category posts', 'efmb' ),
				'desc'    => __( "Select categories you want to exclude. Empty categories are unavailable.", 'efmb' ),
				'id'      => "{$prefix}cat_extras",
				'type'    => 'taxonomy',
				'options' => array(
					'taxonomy'	=> $cpt_tax_cat_extr,
					'type' 		=> 'select_tree',
					'args' 		=> array( 'hide_empty' => true )
				),
				'multiple' => true,
			),

			array(
				'name'    => __( 'Content', 'efmb' ),
				'id'      => "{$prefix}extr_content",
				'type'    => 'radio',
				'desc'    => __( 'Choose "content" if you want to display the entire post. "Excerpt" provides the first 40 words of the post\'s content. In both types you can use Wordpress "more" tag to split content manually.', 'efmb' ),
				'class'   => 'ef-radio-check',
				'options' => array(
					'ef-content' => __( 'Content', 'efmb' ),
					'ef-excerpt' => __( 'Excerpt', 'efmb' ),
				),
			),

			array(
				'name'	 => __( 'Team', 'efmb' ),
				'type'   => 'heading',
				'id'     => "fake_id",
			),

			array(
				'name'  => __( 'Title', 'efmb' ),
				'id'    => "{$prefix}text_latest",
				'desc'  => __( 'Specify a title of this content block on your site or leave blank to hide title.', 'efmb' ),
				'type'  => 'text',
			),

			array(
				'name'    => __( 'Type', 'efmb' ),
				'id'      => "{$prefix}radio_latest",
				'type'    => 'radio',
				'desc'    => __( "Select a type of the team layout.", 'efmb' ),
				'class'   => 'ef-radio-check',
				'options' => array(
					'carousel' 	=> __( 'Carousel', 'efmb' ),
					'list'  	=> __( 'List', 'efmb' ),
				),
			),

			array(
				'name'     => __( 'Content', 'efmb' ),
				'id'       => "{$prefix}team_content",
				'desc'     => __( 'Choose "content" if you want to display the entire post. "Excerpt" provides the first 40 words of the post\'s content. In both types you can use Wordpress "more" tag to split content manually.', 'efmb' ),
				'type'     => 'radio',
				'options'  => array(
					"ef-full"  		=> __( "Content", 'efmb' ),
					"ef-excerpt" 	=> __( "Excerpt", 'efmb' ),
				),
			),

			array(
				'name'    => __( 'Exclude category posts', 'efmb' ),
				'desc'    => __( "Select categories you want to exclude. Empty categories are not available.", 'efmb' ),
				'id'      => "{$prefix}cat_latest",
				'type'    => 'taxonomy',
				'options' => array(
					'taxonomy'	=> $cpt_tax_cat_team,
					'type' 		=> 'select_tree',
					'args'		=> array( 'hide_empty' => true )
				),
				'multiple' => true,
			),

		),

		'only_on'    => array(
			'template' => array( 'templates/regular-template.php' )
		)
	);

}

if ( isset( $ef_cpt ) && isset( $efto ) ) {

	// Meta boxes for the Homepage
	$meta_boxes[] = array(
		'id' 		=> 'homepage_mb',
		'title' 	=> __( 'Content Settings', 'efmb' ),
		'pages' 	=> array( 'page' ),
		'context' 	=> 'normal',
		'priority' 	=> 'high',
		'autosave' 	=> true,
		'fields' 	=> array(

			array(
				'name'  => __( 'Extras', 'efmb' ),
				'type'  => 'heading',
				'id'   	=> "fake_id",
			),

			array(
				'name'  => __( 'Title', 'efmb' ),
				'id'    => "{$prefix}text_extras",
				'desc'  => __( 'Specify a title of this content block on your site or leave blank to hide title.', 'efmb' ),
				'type'  => 'text',
			),

			array(
				'name'     => __( 'Extras style', 'efmb' ),
				'id'       => "{$prefix}extras_style",
				'desc'     => __( 'Choose a style of Extras. "Vertical" is default.', 'efmb' ),
				'type'     => 'radio',
				'options'  => array(
					"ef-extr-1" => __( "Style 1 (Vertical)", 'efmb' ),
					"ef-extr-2" => __( "Style 2 (Horizontal)", 'efmb' ),
				),
			),

			array(
				'name'    => __( 'Exclude category posts', 'efmb' ),
				'desc'    => __( "Select categories you want to exclude. Empty categories are unavailable.", 'efmb' ),
				'id'      => "{$prefix}cat_extras",
				'type'    => 'taxonomy',
				'options' => array(
					'taxonomy' => $cpt_tax_cat_extr,
					'type' => 'select_tree',
					'args' => array( 'hide_empty' => true )
				),
				'multiple' => true,
			),

			array(
				'name'    => __( 'Content', 'efmb' ),
				'id'      => "{$prefix}extr_content",
				'type'    => 'radio',
				'desc'    => __( 'Choose "content" if you want to display the entire post. "Excerpt" provides the first 40 words of the post\'s content. In both types you can use Wordpress "more" tag to split content manually.', 'efmb' ),
				'class'   => 'ef-radio-check',
				'options' => array(
					'ef-content' => __( 'Content', 'efmb' ),
					'ef-excerpt' => __( 'Excerpt', 'efmb' ),
				),
			),

			array(
				'name' 	=> __( 'Recent Projects', 'efmb' ),
				'type'	=> 'heading',
				'id'	=> "fake_id",
			),

			array(
				'name'  => __( 'Title', 'efmb' ),
				'id'    => "{$prefix}text_latest",
				'desc'  => __( 'Specify a title of this content block on your site or leave blank to hide title.', 'efmb' ),
				'type'  => 'text',
			),

			array(
				'name'	=> __( 'Projects per page', 'efmb' ),
				'id'  	=> "{$prefix}select_latest",
				'desc'  => __( 'Select a number of the latest projects you want to display.', 'efmb' ),
				'type'	=> 'slider',
				'js_options' => array(
					'min'  	 => 2,
					'max'  	 => 99,
					'step' 	 => 1,
				),
			),

			array(
				'name'    => __( 'Type', 'efmb' ),
				'id'      => "{$prefix}radio_latest",
				'type'    => 'radio',
				'desc'    => __( 'Select a type of the recent projects.', 'efmb' ),
				'class'   => 'ef-radio-check',
				'options' => array(
					'carousel'	=> __( 'Carousel', 'efmb' ),
					'list'		=> __( 'List', 'efmb' ),
				),
			),

			array(
				'name'    => __( 'Thumbnail link', 'efmb' ),
				'desc'    => __( 'Choose a link to the post or to a full size image of the thumbnail.', 'efmb' ),
				'id'      => "{$prefix}lb_link",
				'options' => array(
					"post"  => __( "Post", 'efmb' ),
					"img"	=> __( "Image", 'efmb' ),
				),
				'type'    => 'radio',
			),

			array(
				'name'		=> __( 'Expandable area', 'efmb' ),
				'desc'		=> __( 'Select what should be displayed in an expandable area or hide it.', 'efmb' ),
				'id'		=> "{$prefix}thumb_tip",
				'options'	=> array(
					"tags"  	=> __( "Tags", 'efmb' ),
					"excerpt"  	=> __( "Excerpt", 'efmb' ),
				),
				'type'		=> 'select',
				'std'		=> __( '- Hide -', 'efmb' ),
			),

			array(
				'name'    => __( 'Go to Portfolio', 'efmb' ),
				'id'      => "{$prefix}latest_lnk",
				'type'    => 'cpt_link',
				'desc'    => __( 'Show or hide link to the portfolio page.', 'efmb' ),
			),

			array(
				'name'    => __( 'Permalink title', 'efmb' ),
				'id'      => "{$prefix}latest_lnk_title",
				'type'    => 'text',
				'desc'    => __( 'Desc.', 'efmb' ),
			),

			array(
				'name'    => __( 'Exclude category posts', 'efmb' ),
				'desc'    => __( "Select categories you want to exclude. Empty categories are unavailable.", 'efmb' ),
				'id'      => "{$prefix}cat_latest",
				'type'    => 'taxonomy',
				'options' => array(
					'taxonomy'	=> $cpt_tax_cat,
					'type' 		=> 'select_tree',
					'args'		=> array( 'hide_empty' => true )
				),
				'multiple' => true,
			),

			array(
				'name' 	=> __( 'Clients carousel', 'efmb' ),
				'type'	=> 'heading',
				'id' 	=> "fake_id",
			),

			array(
				'name'	=> __( 'Client logos', 'efmb' ),
				'id'	=> "{$prefix}upload_img",
				'desc'	=> __( "Select images from the Wordpress Gallery or upload new ones. Use drag'n'drop to order images.", 'efmb' ),
				'type'	=> 'image_advanced',
				'max_file_uploads'	=> 99,
			),

			array(
				'name'    => __( 'Autoplay', 'efmb' ),
				'desc'    => __( 'Check this checkbox to enable autoplay.', 'efmb' ),
				'id'      => "{$prefix}clients_auto",
				'type'    => 'checkbox',
				'std'	  => 0,
			),

		),

		'only_on'    => array(
			'template' => array( 'templates/home-template.php' )
		)
	);

}


// Meta boxes for the Portfolio/blog post
$meta_boxes[] = array(
	'id'		=> 'page_mb',
	'title'		=> __( 'Slider Settings', 'efmb' ),
	'pages'		=> array( $cpt, 'post' ),
	'context'	=> 'normal',
	'priority'	=> 'high',
	'autosave'	=> true,
	'fields'	=> array(

		array(
			'name' => __( 'Autoplay', 'efmb' ),
			'id'   => "{$prefix}slider_autoplay",
			'desc' => __( 'Check this checkbox to enable autoplay.', 'efmb' ),
			'type' => 'checkbox',
			'std'  => 0,
		),

		array(
			'name' => __( 'Infinite', 'efmb' ),
			'id'   => "{$prefix}slider_infinite",
			'desc' => __( 'Check this checkbox to apply "infinite" scrolling.', 'efmb' ),
			'type' => 'checkbox',
			'std'  => 0,
		),

		array(
			'name' => __( 'Duration', 'efmb' ),
			'id'   => "{$prefix}slider_duration",
			'desc' => __( 'Slide display timing in milliseconds. Applies if autoplay was activated.', 'efmb' ),
			'type' => 'slider',
			'js_options'	=> array(
				'min'		=> 500,
				'max'		=> 20000,
				'step'		=> 100,
			),
			'std' => 2000,
		),

		array(
			'name'    => __( 'Transition', 'efmb' ),
			'id'      => "{$prefix}slider_fx",
			'desc'    => __( 'Choose a slider transition.', 'efmb' ),
			'type'    => 'select',
			'options' => array(
				'crossfade' 	=> __( 'Crossfade', 'efmb' ),
				'cover'  		=> __( 'Cover', 'efmb' ),
				'cover-fade' 	=> __( 'Cover-fade', 'efmb' ),
				'uncover' 		=> __( 'Uncover', 'efmb' ),
				'uncover-fade' 	=> __( 'Uncover-fade', 'efmb' ),
			),
			'std'   => __( 'Slide', 'efmb' ),
		),

		array(
			'name'		=> __( 'Direction', 'efmb' ),
			'id'		=> "{$prefix}slider_direction",
			'desc'		=> __( 'Choose a direction of the slider animations. It will provide visual effect in pare with each "slide-like" transition.', 'efmb' ),
			'type'		=> 'select',
			'options'	=> array(
				'right'		=> __( 'Right', 'efmb' ),
			),
			'std'		=> __( 'Left', 'efmb' ),
		),

		array(
			'name'          => __( 'Slider images', 'efmb' ),
			'id'            => "{$prefix}upload_img",
			'desc'          => __( "Select images from the Wordpress Gallery or upload new ones. Use drag'n'drop to order images.", 'efmb' ),
			'type'             => 'image_advanced',
			'max_file_uploads' => 30,
		),

		array(
			'name' => __( 'Include featured image', 'efmb' ),
			'id'   => "{$prefix}inc_featured_img",
			'desc' => __( 'Include featured image in the slider.', 'efmb' ),
			'type' => 'checkbox',
			'std'  => 1,
		),
	)
);


// Meta boxes for the Extras post
$meta_boxes[] = array(
	'id'		=> 'extras_mb',
	'title'		=> __( 'Extra Settings', 'efmb' ),
	'pages'		=> array( $cpt_extr ),
	'context'	=> 'normal',
	'priority'	=> 'high',
	'autosave'	=> true,
	'fields'	=> array(

		array(
			'name'		=> 'Icon',
			'id'		=> "{$prefix}extra_icons",
			'class'		=> "{$prefix}extra-icons",
			'type'		=> 'extras',
			'options'	=> $extra_icn,
			'std'   	=> "ef-icn-1",
		),

	)
);


// Meta boxes for the Team post
$meta_boxes[] = array(
	'id'		=> 'team_mb',
	'title'		=> __( 'Team Settings', 'efmb' ),
	'pages'		=> array( $cpt_team ),
	'context'	=> 'normal',
	'priority'	=> 'high',
	'autosave'	=> true,
	'fields'	=> array(

		// TEXT
		array(
			'name'  => __( 'Position', 'efmb' ),
			'id'    => "{$prefix}member_pos",
			'desc'  => __( 'Enter a team member position.', 'efmb' ),
			'type'  => 'text',
		),

		// Title
		array(
			'name'	=> __( 'Socialize', 'efmb' ),
			'type'	=> "heading",
			'id'	=> "fake_id",
		),

		// Social icons
		array(
			'name'	=> ' ',
			'desc'	=> "Twitter",
			'id'	=> "{$prefix}twt",
			'type'	=> "text",
			'std'	=> "#",
		),

		array(
			'name'	=> ' ',
			'desc'	=> "Facebook",
			'id'	=> "{$prefix}fb",
			'type'	=> "text",
			'std'	=> "#",
		),

		array(
			'name'	=> ' ',
			'desc'	=> "LinkedIn",
			'id'	=> "{$prefix}in",
			'type'	=> "text",
			'std'	=> "#",
		),

		array(
			'name'	=> ' ',
			'desc'	=> "Pinterest",
			'id'	=> "{$prefix}pin",
			'type'	=> "text",
			'std'	=> "#",
		),

		array(
			'name'	=> ' ',
			'desc'	=> "Dribbble",
			'id'	=> "{$prefix}drb",
			'type'	=> "text",
			'std'	=> "#",
		),

		array(
			'name'	=> ' ',
			'desc'	=> "Tumblr",
			'id'	=> "{$prefix}tumb",
			'type'	=> "text",
			'std'	=> "",
		),

		array(
			'name'	=> ' ',
			'desc'	=> "Flickr",
			'id'	=> "{$prefix}flick",
			'type'	=> "text",
			'std'	=> "",
		),

		array(
			'name'	=> ' ',
			'desc'	=> "Vimeo",
			'id'	=> "{$prefix}vim",
			'type'	=> "text",
			'std'	=> "",
		),

		array(
			'name'	=> ' ',
			'desc'	=> "Delicious",
			'id'	=> "{$prefix}delic",
			'type'	=> "text",
			'std'	=> "",
		),

		array(
			'name'	=> ' ',
			'desc'	=> "Google Plus",
			'id'	=> "{$prefix}goog",
			'type'	=> "text",
			'std'	=> "",
		),

		array(
			'name'	=> ' ',
			'desc'	=> "Forrst",
			'id'	=> "{$prefix}forr",
			'type'	=> "text",
			'std'	=> "",
		),

		array(
			'name'	=> ' ',
			'desc'	=> "Hi5!",
			'id'	=> "{$prefix}hi5",
			'type'	=> "text",
			'std'	=> "",
		),

		array(
			'name'	=> ' ',
			'desc'	=> "Last.fm",
			'id'	=> "{$prefix}last",
			'type'	=> "text",
			'std'	=> "",
		),

		array(
			'name'	=> ' ',
			'desc'	=> "Myspace",
			'id'	=> "{$prefix}space",
			'type'	=> "text",
			'std'	=> "",
		),

		array(
			'name'	=> ' ',
			'desc'	=> "Newsvine",
			'id'	=> "{$prefix}newsv",
			'type'	=> "text",
			'std'	=> "",
		),

		array(
			'name'	=> ' ',
			'desc'	=> "Picasa",
			'id'    => "{$prefix}pica",
			'type'  => "text",
			'std'   => "",
		),

		array(
			'name'	=> ' ',
			'desc'	=> "Technorati",
			'id'	=> "{$prefix}tech",
			'type'	=> "text",
			'std'	=> "",
		),

		array(
			'name'	=> ' ',
			'desc'	=> "RSS",
			'id'	=> "{$prefix}rss",
			'type'	=> "text",
			'std'	=> "",
		),

		array(
			'name'	=> ' ',
			'desc'	=> "Rdio",
			'id'	=> "{$prefix}rdio",
			'type'	=> "text",
			'std'	=> "",
		),

		array(
			'name'	=> ' ',
			'desc'	=> "ShareThis",
			'id'	=> "{$prefix}share",
			'type'	=> "text",
			'std'	=> "",
		),

		array(
			'name'	=> ' ',
			'desc'	=> "Skype",
			'id'	=> "{$prefix}skyp",
			'type'	=> "text",
			'std'	=> "",
		),

		array(
			'name'	=> ' ',
			'desc'	=> "SlideShare",
			'id'	=> "{$prefix}slid",
			'type'	=> "text",
			'std'   => "",
		),

		array(
			'name'	=> ' ',
			'desc'	=> "Squidoo",
			'id'	=> "{$prefix}squid",
			'type'	=> "text",
			'std'	=> "",
		),

		array(
			'name'	=> ' ',
			'desc'	=> "StumbleUpon",
			'id'	=> "{$prefix}stum",
			'type'	=> "text",
			'std'	=> "",
		),

		array(
			'name'	=> ' ',
			'desc'	=> "WhatsApp",
			'id'	=> "{$prefix}what",
			'type'	=> "text",
			'std'   => "",
		),

		array(
			'name'	=> ' ',
			'desc'	=> "Wordpress",
			'id'	=> "{$prefix}wp",
			'type'	=> "text",
			'std'   => "",
		),

		array(
			'name'	=> ' ',
			'desc'	=> "Youtube",
			'id'	=> "{$prefix}ytb",
			'type'	=> "text",
			'std'   => "",
		),

		array(
			'name'	=> ' ',
			'desc'	=> "Digg",
			'id'	=> "{$prefix}digg",
			'type'	=> "text",
			'std'   => "",
		),

		array(
			'name'	=> ' ',
			'desc'	=> "Behance",
			'id'	=> "{$prefix}beh",
			'type'	=> "text",
			'std'   => "",
		),

		array(
			'name'	=> ' ',
			'desc'	=> "Yahoo",
			'id'	=> "{$prefix}yah",
			'type'	=> "text",
			'std'   => "",
		),

		array(
			'name'	=> ' ',
			'desc'	=> "Blogger",
			'id'	=> "{$prefix}blogg",
			'type'	=> "text",
			'std'   => "",
		),

		array(
			'name'	=> ' ',
			'desc'	=> "Hype Machine",
			'id'	=> "{$prefix}hype",
			'type'	=> "text",
			'std'   => "",
		),

		array(
			'name'	=> ' ',
			'desc'	=> "Grooveshark",
			'id'	=> "{$prefix}groove",
			'type'	=> "text",
			'std'   => "",
		),

		array(
			'name'	=> ' ',
			'desc'	=> "SoundCloud",
			'id'	=> "{$prefix}sound",
			'type'	=> "text",
			'std'   => "",
		),

		array(
			'name'	=> ' ',
			'desc'	=> "Instagram",
			'id'	=> "{$prefix}insta",
			'type'	=> "text",
			'std'   => "",
		),

		array(
			'name'	=> ' ',
			'desc'	=> "Vkontakte",
			'id'	=> "{$prefix}vk",
			'type'	=> "text",
			'std'   => "",
		),

		array(
			'name'	=> ' ',
			'desc'	=> __( "Say 'Hello!'", 'efmb' ),
			'id'	=> "{$prefix}mail",
			'type'	=> "text",
			'std'   => "",
		),

		array(
			'name'	=> ' ',
			'desc'	=> __( "Make a call", 'efmb' ),
			'id'	=> "{$prefix}phone",
			'type'	=> "text",
			'std'   => "",
		),
	)
);


// Meta boxes for Blog
$meta_boxes[] = array(
	'id'		=> 'blog_mb',
	'title'		=> __( 'Content Settings', 'efmb' ),
	'pages'		=> array( 'page' ),
	'context'	=> 'normal',
	'priority'	=> 'high',
	'autosave'	=> true,
	'fields'	=> array(

		array(
			'name'    => __( 'Blog Layout', 'efmb' ),
			'desc'	  => __( 'Select a layout of the blog feed. Choose between 4 types.', 'efmb' ),
			'id'      => "{$prefix}feed_layout",
			'type'    => 'layout',
			'class'   => 'ef-layout-check',
			'options' => array(
				"ef-blog-1" => efmb_IMG_URL . "blog-1.png",
				"ef-blog-2" => efmb_IMG_URL . "blog-2.png",
				"ef-blog-3" => efmb_IMG_URL . "blog-3.png",
				"ef-blog-4" => efmb_IMG_URL . "blog-4.png",
			),
		),

		array(
			'name' => __( 'Posts Per Page', 'efmb' ),
			'id'   => "{$prefix}range",
			'type' => 'slider',
			'js_options' => array(
				'min'		=> 3,
				'max'		=> 99,
				'step'		=> 1,
			),
		),

		array(
			'name'    => __( 'Exclude category posts', 'efmb' ),
			'desc'    => __( "Select categories you want to exclude. Empty categories are unavailable.", 'efmb' ),
			'id'      => "{$prefix}cat",
			'type'    => 'taxonomy',
			'options' => array(
				'taxonomy' => 'category',
				'type' => 'select',
				'args' => array()
			),
			'multiple' => true,
		),

		array(
			'name'    => __( 'Post content', 'efmb' ),
			'id'      => "{$prefix}extr_content",
			'type'    => 'radio',
			'desc'     => __( 'Choose "content" if you want to display the entire post. "Excerpt" provides the first 40 words of the post\'s content. In both types you can use Wordpress "more" tag to split content manually.', 'efmb' ),
			'class'   => 'ef-radio-check',
			'options' => array(
				'ef-content' => __( 'Content', 'efmb' ),
				'ef-excerpt' => __( 'Excerpt', 'efmb' ),
			),
		),

	),

	'only_on'    => array(
		'template' => array( 'templates/blog-template.php' )
	),
);

// Meta boxes for contact
$meta_boxes[] = array(
	'id'		=> 'contact_mb',
	'title'		=> __( 'Content Settings', 'efmb' ),
	'pages'		=> array( 'page' ),
	'context'	=> 'normal',
	'priority'	=> 'high',
	'autosave'	=> true,
	'fields'	=> array(

		array(
			'name'	=> __( 'Map settings', 'efmb' ),
			'type'	=> "heading",
			'id'	=> "fake_id",
		),

		array(
			'name'  => __( 'Latitude', 'efmb' ),
			'id'    => "{$prefix}map_lat",
			'desc'  => __( 'Use Google Maps to get the latitude of your location.', 'efmb' ),
			'type'  => 'text',
			'std'   => '',
			'clone' => true,
			'clone-group' => 'ef-clone-group'
		),

		array(
			'name'  => __( 'Longitude', 'efmb' ),
			'id'    => "{$prefix}map_long",
			'desc'  => __( 'Use Google Maps to get the longitude of your location.', 'efmb' ),
			'type'  => 'text',
			'std'   => '',
			'clone' => true,
			'clone-group' => 'ef-clone-group'
		),

		array(
			'name' => __( 'Description', 'efmb' ),
			'desc' => __( 'Specify any description that will be displayed in the bubble (or in the navigation if multiple places were set up).', 'efmb' ),
			'id'   => "{$prefix}map_dsc",
			'type' => 'textarea',
			'cols' => 20,
			'rows' => 3,
			'clone' => true,
			'clone-group' => 'ef-clone-group'
		),

	),

	'only_on'    => array(
		'template' => array( 'templates/contact-template.php' )
	)
);


// Meta box for page layouts
$meta_boxes[] = array(
	'id'		=> 'layout_mb',
	'title'		=> __( 'Page layout', 'efmb' ),
	'pages'		=> array( 'page', 'post', $cpt, $cpt_extr, $cpt_team  ),
	'context'	=> 'normal',
	'priority'	=> 'high',
	'autosave'	=> true,
	'fields'	=> array(

		array(
			'name'    => ' ',
			'desc'	  => __( 'Select a page layout.', 'efmb' ),
			'id'      => "{$prefix}layout",
			'type'    => 'layout',
			'class'   => 'ef-layout-check',
			'options' => array(
				'0'		=> efmb_IMG_URL . "global.png",
				'12'	=> efmb_IMG_URL . "1col.png",
				'93'	=> efmb_IMG_URL . "2cr.png",
				'39'	=> efmb_IMG_URL . "2cl.png"
			),
			'std'   => '0'
		),
	)
);

if ( isset( $efto ) ) {

	$meta_boxes[] = array(
		'id'		=> 'widgets_mb',
		'title'		=> __( 'Widget areas', 'efmb' ),
		'pages'		=> array( 'page', 'post', $cpt, $cpt_extr, $cpt_team  ),
		'context'	=> 'normal',
		'priority'	=> 'high',
		'autosave'	=> true,
		'fields'	=> array(

			array(
				'name'    => __( 'Sidebar', 'efmb' ),
				'desc'    => __( 'Select a widget area. Widget areas that have widgets are available only. If it isn\'t specified then "Default sidebar widget area" will be assigned. This option overrides global sidebar option that was set up in Theme Options.', 'efmb' ),
				'id'      => "{$prefix}sidebar",
				'type'    => 'sidebars',
			),

			array(
				'name'    => __( 'Footer', 'efmb' ),
				'desc'    => __( 'Select a widget area. Widget areas that have widgets are available only. If it isn\'t specified then "Default footer widget area" will be assigned. This option overrides global footer option that was set up in Theme Options.', 'efmb' ),
				'id'      => "{$prefix}footer",
				'type'    => 'sidebars',
			)
		)
	);

}


/********************* META BOX REGISTERING ***********************/

/**
 * Register meta boxes
 *
 * @return void
 */
function rw_register_meta_boxes() {
	global $meta_boxes;

	// Make sure there's no errors when the plugin is deactivated or during upgrade
	if ( class_exists( 'EF_Meta_Box' ) ) {
		foreach ( $meta_boxes as $meta_box ) {
			if ( isset( $meta_box['only_on'] ) && ! rw_maybe_include( $meta_box['only_on'] ) ) {
				continue;
			} elseif ( isset( $meta_box['exclude_on'] ) && rw_maybe_exlude( $meta_box['exclude_on'] ) ) {
				continue;
			}

			new EF_Meta_Box( $meta_box );
		}
	}

}

add_action( 'admin_init', 'rw_register_meta_boxes' );

/**
 * Check if meta boxes is included
 *
 * @return bool
 */
function rw_maybe_include( $conditions ) {


	// Include in back-end only
	if ( ! defined( 'WP_ADMIN' ) || ! WP_ADMIN ) {
		return false;
	}

	// Always include for ajax
	if ( defined( 'DOING_AJAX' ) && DOING_AJAX ) {
		return true;
	}

	if ( isset( $_GET['post'] ) ) {
		$post_id = $_GET['post'];
	}
	elseif ( isset( $_POST['post_ID'] ) ) {
		$post_id = $_POST['post_ID'];
	}
	else {
		$post_id = false;
	}

	$post_id = (int) $post_id;
	$post    = get_post( $post_id );


	foreach ( $conditions as $cond => $v ) {
		// Catch non-arrays too
		if ( ! is_array( $v ) ) {
			$v = array( $v );
		}

		switch ( $cond ) {
		case 'id':
			if ( in_array( $post_id, $v ) ) {
				return true;
			}
			break;
		case 'parent':
			$post_parent = $post->post_parent;
			if ( in_array( $post_parent, $v ) ) {
				return true;
			}
			break;
		case 'slug':
			$post_slug = $post->post_name;
			if ( in_array( $post_slug, $v ) ) {
				return true;
			}
			break;
		case 'template':
			$template = get_post_meta( $post_id, '_wp_page_template', true );
			if ( in_array( $template, $v ) ) {
				return true;
			}
			break;
		}
	}

	// If no condition matched
	return false;

}

/**
 * Check if meta boxes is included
 *
 * @return bool
 */
function rw_maybe_exlude( $conditions ) {
	// Include in back-end only
	if ( ! defined( 'WP_ADMIN' ) || ! WP_ADMIN ) {
		return true;
	}

	// Always include for ajax
	if ( defined( 'DOING_AJAX' ) && DOING_AJAX ) {
		return false;
	}

	if ( isset( $_GET['post'] ) ) {
		$post_id = $_GET['post'];
	}
	elseif ( isset( $_POST['post_ID'] ) ) {
		$post_id = $_POST['post_ID'];
	}
	else {
		$post_id = false;
	}

	$post_id = (int) $post_id;
	$post    = get_post( $post_id );

	foreach ( $conditions as $cond => $v ) {
		// Catch non-arrays too
		if ( ! is_array( $v ) ) {
			$v = array( $v );
		}

		switch ( $cond ) {
		case 'id':
			if ( in_array( $post_id, $v ) ) {
				return true;
			}
			break;
		case 'parent':
			$post_parent = $post->post_parent;
			if ( in_array( $post_parent, $v ) ) {
				return true;
			}
			break;
		case 'slug':
			$post_slug = $post->post_name;
			if ( in_array( $post_slug, $v ) ) {
				return true;
			}
			break;
		case 'template':
			$template = get_post_meta( $post_id, '_wp_page_template', true );
			if ( in_array( $template, $v ) ) {
				return true;
			}
			break;
		}
	}

	// If no condition matched
	return false;
}
