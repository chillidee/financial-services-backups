<?php
// Prevent loading this file directly
defined( 'ABSPATH' ) || exit;

if ( ! class_exists( 'efmb_sidebars_Field' ) )
{
	class efmb_sidebars_Field {
		/**
		 * Get field HTML
		 *
		 * @param $html
		 * @param $field
		 * @param $meta
		 *
		 * @return string
		 */
		 
		static function html( $html, $meta, $field ) {
			
			$field['options'] = self::get_options();
			
			$field['std'] = __( 'Default widget area', 'efmb' );
			
			$html = efmb_Select_Field::html( $html, $meta, $field );
			
			return $html;
		}

		/**
		 * Get options for select
		 *
		 * @param $options array
		 */
		static function get_options()
		{		
			$wp_sidebars_obj = $GLOBALS['wp_registered_sidebars'];
			$term =  array();
			
			// Access the WordPress Sidebars via an Array
			foreach ( $wp_sidebars_obj as $sidebar ) {		
				if ( is_active_sidebar($sidebar['id']) && $sidebar['id'] != 'footer-widget-area' && $sidebar['id'] != 'primary-widget-area' ) {
					$term[$sidebar['id']] = $sidebar['name'];
				}
			}	
			return $term;
		}
	}
}
