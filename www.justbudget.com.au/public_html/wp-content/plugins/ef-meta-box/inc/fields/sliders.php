<?php
// Prevent loading this file directly
defined( 'ABSPATH' ) || exit;

if ( ! class_exists( 'efmb_Sliders_Field' ) )
{
	class efmb_Sliders_Field
	{		
		static function html( $html, $meta, $field ) {
			
			$field['options'] = self::get_options();
			
			if ( !class_exists( 'RevSlider' ) ) :				
				$field['std'] = __( 'Revolution Slider is not installed', 'efmb' );			
			elseif ( count( self::get_options() ) == 0 ) :
				$field['std'] = __( 'No Sliders available', 'efmb' );
			else :
				$field['std'] = __( '- Hide Slider -', 'efmb' );
			endif;

			$html = efmb_Select_Field::html( $html, $meta, $field );			

			return $html;
		}
	
		/**
		 * Get options for select
		 *
		 * @param $options array
		 */
		static function get_options()
		{	
			$arrSliders = array();
			$sliders = array();
			
			if ( class_exists( 'RevSlider' ) ) :
			
				$slider = new RevSlider();
				$arrSliders = $slider->getArrSliders();
				
				foreach($arrSliders as $slider) {
					$sliders[ $slider->getAlias() ] = $slider->getTitle();
				}
				
			endif;
			
			return $sliders;
		}
	}
}