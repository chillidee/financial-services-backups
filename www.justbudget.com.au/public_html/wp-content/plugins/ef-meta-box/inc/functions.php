<?php

/**
 * Required scripts/css
 *
 * @since 1.0.0
 */

add_action( 'wp_print_styles', 'ef_efmb_enqueue_style' );
function ef_efmb_enqueue_style() {
	wp_enqueue_style( 'ef-entypo', efmb_ENTYPO_URL . 'stylesheet.css', array(), '', 'screen' );
	wp_enqueue_style( 'prettyPhoto', efmb_JS_URL . 'jquery/prettyPhoto/css/prettyPhoto.css', array(), '', 'screen' );
}


if ( !is_admin() ) {
	add_action( 'init', 'ef_theme_enqueue_maps', -1 );
}
function ef_theme_enqueue_maps() {
	wp_enqueue_script( 'gmaps', '//maps.google.com/maps/api/js?sensor=true', array(), '' );
}

add_action( 'wp_enqueue_scripts', 'ef_efmb_enqueue_script' );
function ef_efmb_enqueue_script() {

	global $wp_scripts;

	if ( !isset( $wp_scripts->registered[ 'jquery' ] ) ) {
		wp_enqueue_script( 'jquery' );
	}

	wp_enqueue_script( 'prettyPhoto', efmb_JS_URL . 'jquery/prettyPhoto/jquery.prettyPhoto.js', array( 'jquery' ), '', true );
	wp_enqueue_script( 'gMap', efmb_JS_URL . 'jquery/gMap/jquery.ui.map.min.js', array( 'jquery', 'gmaps' ), '', true );
	wp_enqueue_script( 'gMap-ext', efmb_JS_URL . 'jquery/gMap/jquery.ui.map.extensions.js', array( 'jquery', 'gmaps', 'gMap' ), '', true );

	if ( !isset( $wp_scripts->registered[ 'carouFredSel' ] ) ) {
		wp_enqueue_script( 'carouFredSel', efmb_JS_URL . 'jquery/carouFredSel/jquery.carouFredSel-6.2.1-packed.js', array( 'jquery' ), '', true );
	}
	wp_enqueue_script( 'efmb-carouFredSel-init', efmb_JS_URL . 'jquery/carouFredSel/carouFredSel.init.js', array( 'jquery', 'carouFredSel' ), '', true );

	$autoplay_cl = efmb_meta( 'ef_clients_auto', 'type=checkbox' );
	$autoplay = efmb_meta( 'ef_slider_autoplay', 'type=checkbox' );
	$infinite = efmb_meta( 'ef_slider_infinite', 'type=checkbox' );
	$duration = efmb_meta( 'ef_slider_duration', 'type=slider' );
	$fx = efmb_meta( 'ef_slider_fx', 'type=select' );
	$direction = efmb_meta( 'ef_slider_direction', 'type=select' );

	$map_lat = efmb_meta( 'ef_map_lat', 'type=input' );
	$map_long = efmb_meta( 'ef_map_long', 'type=input' );
	$map_desc = efmb_meta( 'ef_map_dsc', 'type=input' );

	$slider_vars1 = array(
		'autoplay_cl'   => !empty( $autoplay_cl ) ? $autoplay_cl : "0",
		'autoplay'    => !empty( $autoplay ) ? $autoplay : "0",
		'infinite'    => !empty( $infinite ) ? $infinite : "0",
		'duration'    => !empty( $duration ) ? $duration : "2000",
		'fx'          => !empty( $fx ) ? $fx : 'slide',
		'direction'   => !empty( $direction ) ? $direction : 'left',
	);

	wp_localize_script( 'efmb-carouFredSel-init', 'ef_slider_vars2', $slider_vars1 );

	wp_enqueue_script( 'efmb-custom', efmb_JS_URL . 'jquery/init.js', array( 'jquery' ), '', true );

	$markers = array();

	for ( $i = 0; $i < count( $map_lat ); $i++ ) {
		if ( !empty( $map_lat[$i] ) && !empty( $map_long[$i] ) ) {
			$markers[$i] = array(
				"position" => $map_lat[$i].','.$map_long[$i],
				"title"  => !empty( $map_desc[$i] ) ? $map_desc[$i] : __( 'Untitled', 'efmb' ),
			);
		}
	}

	wp_localize_script( 'efmb-custom', 'ef_map_vars', $markers );
}
