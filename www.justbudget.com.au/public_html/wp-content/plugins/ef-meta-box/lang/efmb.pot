msgid ""
msgstr ""
"Project-Id-Version: Fireform Meta Box\n"
"POT-Creation-Date: 2013-09-03 19:52+0300\n"
"PO-Revision-Date: 2013-09-03 19:53+0300\n"
"Last-Translator: fireform <twm2003@gmail.com>\n"
"Language-Team: Fireform\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.5.7\n"
"X-Poedit-KeywordsList: __;_e;_x;_n\n"
"X-Poedit-Basepath: .\n"
"X-Poedit-SearchPath-0: /Users/fireform/wordpress/wp-content/plugins/ef-meta-"
"box\n"

#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/meta-boxes-options.php:105
msgid "Header Settings"
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/meta-boxes-options.php:113
msgid "Page Description"
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/meta-boxes-options.php:115
msgid ""
"Description of the entire page. Applies after the name of a page. It "
"overrides \"Global page description\" that specified in Theme Options."
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/meta-boxes-options.php:128
#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/meta-boxes-options.php:570
msgid "Slider Settings"
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/meta-boxes-options.php:136
msgid ""
"Check documentation if you see \"Revolution Slider is not installed\". "
"Create at least one Slider if you see \"No sliders available\"."
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/meta-boxes-options.php:148
msgid "Welcome message"
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/meta-boxes-options.php:156
msgid ""
"This colourful block is located below the header (slider). You are able to "
"use different html tags such as &lt;h1&gt;...&lt;h6&gt;, &lt;kbd&gt;, &lt;"
"cite&gt;, etc."
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/meta-boxes-options.php:173
#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/meta-boxes-options.php:261
#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/meta-boxes-options.php:392
#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/meta-boxes-options.php:1019
#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/meta-boxes-options.php:1086
msgid "Content Settings"
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/meta-boxes-options.php:181
msgid "Layout"
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/meta-boxes-options.php:182
msgid ""
"Select a layout of the portfolio grid. Choose between 3, 4 and 5 columns."
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/meta-boxes-options.php:194
#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/meta-boxes-options.php:343
#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/meta-boxes-options.php:474
msgid "Type"
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/meta-boxes-options.php:195
msgid ""
"\"Regular\" portfolio has a usual paginated layout. \"Filtered\" portfolio "
"has no pagination and other pages are loading via Ajax."
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/meta-boxes-options.php:205
#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/meta-boxes-options.php:486
msgid "Thumbnail link"
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/meta-boxes-options.php:206
#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/meta-boxes-options.php:487
msgid "Choose a link to the post or to a full size image of the thumbnail."
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/meta-boxes-options.php:216
#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/meta-boxes-options.php:497
msgid "Expandable area"
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/meta-boxes-options.php:217
#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/meta-boxes-options.php:498
msgid "Select what should be displayed in an expandable area or hide it."
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/meta-boxes-options.php:228
#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/meta-boxes-options.php:1041
msgid "Posts Per Page"
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/meta-boxes-options.php:239
#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/meta-boxes-options.php:305
#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/meta-boxes-options.php:366
#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/meta-boxes-options.php:424
#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/meta-boxes-options.php:523
#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/meta-boxes-options.php:1052
msgid "Exclude category posts"
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/meta-boxes-options.php:240
#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/meta-boxes-options.php:306
#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/meta-boxes-options.php:425
#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/meta-boxes-options.php:524
#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/meta-boxes-options.php:1053
msgid ""
"Select categories you want to exclude. Empty categories are unavailable."
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/meta-boxes-options.php:269
msgid "Extras/Team"
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/meta-boxes-options.php:270
msgid ""
"Select \"Extra\" or \"Team\" to display on the page below the main content "
"or hide both."
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/meta-boxes-options.php:274
msgid "Hide"
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/meta-boxes-options.php:275
#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/meta-boxes-options.php:330
msgid "Team"
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/meta-boxes-options.php:276
#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/meta-boxes-options.php:281
#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/meta-boxes-options.php:400
msgid "Extras"
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/meta-boxes-options.php:287
#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/meta-boxes-options.php:336
#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/meta-boxes-options.php:406
#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/meta-boxes-options.php:455
msgid "Title"
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/meta-boxes-options.php:289
#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/meta-boxes-options.php:338
#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/meta-boxes-options.php:408
#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/meta-boxes-options.php:457
msgid ""
"Specify a title of this content block on your site or leave blank to hide "
"title."
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/meta-boxes-options.php:294
#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/meta-boxes-options.php:413
msgid "Extras style"
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/meta-boxes-options.php:296
#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/meta-boxes-options.php:415
msgid "Choose a style of Extras. \"Vertical\" is default."
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/meta-boxes-options.php:299
#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/meta-boxes-options.php:418
msgid "Style 1 (Vertical)"
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/meta-boxes-options.php:300
#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/meta-boxes-options.php:419
msgid "Style 2 (Horizontal)"
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/meta-boxes-options.php:318
#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/meta-boxes-options.php:324
#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/meta-boxes-options.php:355
#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/meta-boxes-options.php:360
#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/meta-boxes-options.php:437
#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/meta-boxes-options.php:443
#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/meta-boxes-options.php:1071
msgid "Content"
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/meta-boxes-options.php:321
#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/meta-boxes-options.php:357
#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/meta-boxes-options.php:440
#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/meta-boxes-options.php:1068
msgid ""
"Choose \"content\" if you want to display the entire post. \"Excerpt\" "
"provides the first 40 words of the post's content. In both types you can use "
"Wordpress \"more\" tag to split content manually."
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/meta-boxes-options.php:325
#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/meta-boxes-options.php:361
#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/meta-boxes-options.php:444
#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/meta-boxes-options.php:502
#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/meta-boxes-options.php:1072
msgid "Excerpt"
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/meta-boxes-options.php:346
msgid "Select a type of the team layout."
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/meta-boxes-options.php:349
#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/meta-boxes-options.php:480
msgid "Carousel"
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/meta-boxes-options.php:350
#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/meta-boxes-options.php:481
msgid "List"
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/meta-boxes-options.php:367
msgid ""
"Select categories you want to exclude. Empty categories are not available."
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/meta-boxes-options.php:449
msgid "Recent Projects"
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/meta-boxes-options.php:462
msgid "Projects per page"
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/meta-boxes-options.php:464
msgid "Select a number of the latest projects you want to display."
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/meta-boxes-options.php:477
msgid "Select a type of the recent projects."
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/meta-boxes-options.php:490
#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/inc/fields/post.php:54
msgid "Post"
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/meta-boxes-options.php:491
msgid "Image"
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/meta-boxes-options.php:501
msgid "Tags"
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/meta-boxes-options.php:505
msgid "- Hide -"
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/meta-boxes-options.php:509
msgid "Go to Portfolio"
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/meta-boxes-options.php:512
msgid "Show or hide link to the portfolio page."
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/meta-boxes-options.php:516
msgid "Permalink title"
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/meta-boxes-options.php:519
msgid "Desc."
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/meta-boxes-options.php:536
msgid "Clients carousel"
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/meta-boxes-options.php:542
msgid "Client logos"
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/meta-boxes-options.php:544
#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/meta-boxes-options.php:637
msgid ""
"Select images from the Wordpress Gallery or upload new ones. Use drag'n'drop "
"to order images."
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/meta-boxes-options.php:550
#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/meta-boxes-options.php:578
msgid "Autoplay"
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/meta-boxes-options.php:551
#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/meta-boxes-options.php:580
msgid "Check this checkbox to enable autoplay."
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/meta-boxes-options.php:586
msgid "Infinite"
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/meta-boxes-options.php:588
msgid "Check this checkbox to apply \"infinite\" scrolling."
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/meta-boxes-options.php:594
msgid "Duration"
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/meta-boxes-options.php:596
msgid ""
"Slide display timing in milliseconds. Applies if autoplay was activated."
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/meta-boxes-options.php:607
msgid "Transition"
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/meta-boxes-options.php:609
msgid "Choose a slider transition."
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/meta-boxes-options.php:612
msgid "Crossfade"
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/meta-boxes-options.php:613
msgid "Cover"
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/meta-boxes-options.php:614
msgid "Cover-fade"
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/meta-boxes-options.php:615
msgid "Uncover"
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/meta-boxes-options.php:616
msgid "Uncover-fade"
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/meta-boxes-options.php:618
msgid "Slide"
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/meta-boxes-options.php:622
msgid "Direction"
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/meta-boxes-options.php:624
msgid ""
"Choose a direction of the slider animations. It will provide visual effect "
"in pare with each \"slide-like\" transition. Note that you need to upload "
"images that have the same size if you choose \"up\" or \"down\" direction."
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/meta-boxes-options.php:627
msgid "Right"
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/meta-boxes-options.php:628
msgid "Up"
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/meta-boxes-options.php:629
msgid "Down"
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/meta-boxes-options.php:631
msgid "Left"
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/meta-boxes-options.php:635
msgid "Slider images"
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/meta-boxes-options.php:643
msgid "Include featured image"
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/meta-boxes-options.php:645
msgid "Include featured image in the slider."
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/meta-boxes-options.php:656
msgid "Extra Settings"
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/meta-boxes-options.php:679
msgid "Team Settings"
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/meta-boxes-options.php:688
msgid "Member"
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/meta-boxes-options.php:695
msgid "Position"
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/meta-boxes-options.php:697
msgid "Enter a team member position."
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/meta-boxes-options.php:703
msgid "Socialize"
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/meta-boxes-options.php:999
msgid "Say 'Hello!'"
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/meta-boxes-options.php:1007
msgid "Make a call"
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/meta-boxes-options.php:1027
msgid "Blog Layout"
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/meta-boxes-options.php:1028
msgid "Select a layout of the blog feed. Choose between 4 types."
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/meta-boxes-options.php:1065
msgid "Post content"
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/meta-boxes-options.php:1094
msgid "Map settings"
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/meta-boxes-options.php:1100
msgid "Latitude"
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/meta-boxes-options.php:1102
msgid "Use Google Maps to get the latitude of your location."
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/meta-boxes-options.php:1110
msgid "Longitude"
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/meta-boxes-options.php:1112
msgid "Use Google Maps to get the longitude of your location."
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/meta-boxes-options.php:1120
msgid "Description"
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/meta-boxes-options.php:1121
msgid ""
"Specify any description that will be displayed in the bubble (or in the "
"navigation if multiple places were set up)."
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/meta-boxes-options.php:1141
msgid "Page layout"
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/meta-boxes-options.php:1150
msgid "Select a page layout."
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/meta-boxes-options.php:1169
msgid "Widget areas"
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/meta-boxes-options.php:1177
msgid "Sidebar"
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/meta-boxes-options.php:1178
msgid ""
"Select a widget area. Widget areas that have widgets are available only. If "
"it isn't specified then \"Default sidebar widget area\" will be assigned. "
"This option overrides global sidebar option that was set up in Theme Options."
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/meta-boxes-options.php:1184
msgid "Footer"
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/meta-boxes-options.php:1185
msgid ""
"Select a widget area. Widget areas that have widgets are available only. If "
"it isn't specified then \"Default footer widget area\" will be assigned. "
"This option overrides global footer option that was set up in Theme Options."
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/inc/functions.php:66
msgid "Untitled"
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/inc/classes/meta-box.php:252
msgid "Please correct the errors highlighted below and try again."
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/inc/classes/meta-box.php:426
msgid "+"
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/inc/classes/meta-box.php:448
msgid "&#8211;"
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/inc/fields/button.php:36
msgid "Click me"
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/inc/fields/cpt-link.php:21
msgid "No Portfolio pages available"
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/inc/fields/cpt-link.php:21
msgid "- Hide link -"
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/inc/fields/file-advanced.php:23
#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/inc/fields/plupload-image.php:106
msgid "Select Files"
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/inc/fields/file-advanced.php:66
msgid "Select or Upload Files"
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/inc/fields/file-advanced.php:102
#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/inc/fields/file.php:138
#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/inc/fields/image-advanced.php:107
#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/inc/fields/image.php:144
msgid "Delete"
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/inc/fields/file-advanced.php:103
#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/inc/fields/file.php:139
#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/inc/fields/image-advanced.php:108
#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/inc/fields/image.php:145
msgid "Edit"
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/inc/fields/file.php:19
#, php-format
msgid "You may only upload maximum %d file"
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/inc/fields/file.php:20
#, php-format
msgid "You may only upload maximum %d files"
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/inc/fields/file.php:70
msgid "Error: Cannot delete file"
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/inc/fields/file.php:84
msgid "Upload Files"
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/inc/fields/file.php:85
msgid "+ Add new file"
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/inc/fields/image-advanced.php:22
msgid "Select Images"
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/inc/fields/image-advanced.php:71
msgid "Select or Upload Images"
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/inc/fields/image.php:64
msgid "Order saved"
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/inc/fields/image.php:78
#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/inc/fields/thickbox-image.php:35
msgid "Upload Images"
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/inc/fields/image.php:79
msgid "+ Add new image"
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/inc/fields/map.php:48
msgid "Find Address"
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/inc/fields/plupload-image.php:104
msgid "Drop images here"
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/inc/fields/plupload-image.php:105
msgid "or"
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/inc/fields/plupload-image.php:181
msgid "Allowed Image Files"
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/inc/fields/post.php:68
#, php-format
msgid "Select a %s"
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/inc/fields/select-advanced.php:70
msgid "Select a value"
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/inc/fields/sidebars.php:22
msgid "Default widget area"
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/inc/fields/sliders.php:14
msgid "Revolution Slider is not installed"
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/inc/fields/sliders.php:16
msgid "No Sliders available"
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/inc/fields/sliders.php:18
msgid "- Hide Slider -"
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-meta-box/inc/fields/taxonomy.php:40
#, php-format
msgid "Select a %s:"
msgstr ""
