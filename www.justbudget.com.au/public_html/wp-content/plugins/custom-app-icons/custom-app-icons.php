<?php
/*
Plugin Name: Custom app icons
Plugin URI: http://irama.org/web/cms/wordpress/custom-app-icons/
Description: Allows you to set a custom app icons for website shortcuts saved to Home Screen on iphone and ipod touch. Also allows you to specify whether author avatars should be used as the application icon for author pages.
Version: 1.1.0
Author: Andrew Ramsden
Author URI: http://irama.org/

Version History

1.0.0 - 2010-03-18 Initial release version
1.1.0 - 2013-07-07 Updated to support larger icons and precomposed versions

*/

function cai_admin() {
?><div class="wrap">
<style type="text/css">
	
	#app-icon-form {
		padding: 1em;
		width: 77em;
	}
	
	#app-icon-form .question,
	#app-icon-form .actions {
		clear: both;
		margin: 1em 0;
		overflow: hidden;
	}
	#app-icon-form label,
	#app-icon-form .checkbox h3 {
		float: left;
		width: 13em;
		padding-right: 1em;
		font-weight: normal;
		font-size: 100%;
		margin: 0;
	}
	#app-icon-form .question.checkbox {
		background: transparent;
	}
	#app-icon-form .question.checkbox label {
		width: 30em;
		/*padding-left: 14em;*/
	}
	#app-icon-form .input {
		float: left;
		width: 30em;
	}
	#app-icon-form .comment {
		float: left;
		width: 30em;
		padding: .5em 1em;
		margin-left: 1em;
		background: #ddd;
	}
	#app-icon-form .actions {
		background: #fff;
		padding: .5em 1em .5em 13em;
	}
	#app-icon-form legend {
		font-weight: bold;
		font-size: larger;
	}
</style>
<?php
    if( $_POST ) {
        update_option('default-app-icon', stripslashes($_POST['default-app-icon']));
		
		update_option('ipad-app-icon', stripslashes($_POST['ipad-app-icon']));
		update_option('ipad-retina-app-icon', stripslashes($_POST['ipad-retina-app-icon']));
		update_option('iphone-retina-app-icon', stripslashes($_POST['iphone-retina-app-icon']));
		update_option('iphone-old-retina-app-icon', stripslashes($_POST['iphone-old-retina-app-icon']));
        update_option('icons-are-precomposed', stripslashes($_POST['icons-are-precomposed']));
		
        update_option('use-author-app-icons', stripslashes($_POST['use-author-app-icons']));
		
		
	?>
    <div class="updated">
    	<p>Custom app icons settings updated!</p>
    </div>
    <?php
    }
?>
<form method="post" action="" id="app-icon-form">

    <input type="hidden" name="action" value="update" />
    <input type="hidden" name="page_options" value="default-app-icon" />
    <?php wp_nonce_field('update-options'); ?>
    
    
    <h2>Custom iPhone/Touch app icons</h2>
    <div class="introduction">
    	<p>Use this plugin to specify the paths to icons you wish to be used as your app icon
        	when iPhone and iPod Touch users save a shortcut to your site onto their Home Screens.</p>
    </div>
    
    <div class="question">
        <label for="default-app-icon">Default app icon (57x57 pixels)</label>
        <div class="input"><input type="text" name="default-app-icon" id="default-app-icon" value="<?php echo get_option('default-app-icon'); ?>" size="60" /></div>
        <div class="comment">
        	<p>The path to a png file you have uploaded to the server (<strong>57x57 pixels</strong>). Can contain:</p>
            <ul>
            	<li><samp>{theme}</samp> to indicate the path to the current theme (for example: <samp>{theme}/app-icon.png</samp>)</li>
            </ul>
        </div>
    </div>
    
    
    <div class="question">
        <label for="ipad-app-icon">iPad app icon (72x72 pixels)</label>
        <div class="input"><input type="text" name="ipad-app-icon" id="ipad-app-icon" value="<?php echo get_option('ipad-app-icon'); ?>" size="60" /></div>
        <div class="comment">
        	<p>The path to a png file you have uploaded to the server (<strong>72x72 pixels</strong>). Can contain:</p>
            <ul>
            	<li><samp>{theme}</samp> to indicate the path to the current theme (for example: <samp>{theme}/ipad-app-icon.png</samp>)</li>
            </ul>
        </div>
    </div>
    
    
    <div class="question">
        <label for="iphone-old-retina-app-icon">iPhone (old retina) app icon (114x114 pixels)</label>
        <div class="input"><input type="text" name="iphone-old-retina-app-icon" id="iphone-old-retina-app-icon" value="<?php echo get_option('iphone-old-retina-app-icon'); ?>" size="60" /></div>
        <div class="comment">
        	<p>The path to a png file you have uploaded to the server (<strong>114x114 pixels</strong>). Can contain:</p>
            <ul>
            	<li><samp>{theme}</samp> to indicate the path to the current theme (for example: <samp>{theme}/iphone-old-retina-app-icon.png</samp>)</li>
            </ul>
        </div>
    </div>
    
    <div class="question">
        <label for="iphone-retina-app-icon">iPhone (retina) app icon (120x120 pixels)</label>
        <div class="input"><input type="text" name="iphone-retina-app-icon" id="iphone-retina-app-icon" value="<?php echo get_option('iphone-retina-app-icon'); ?>" size="60" /></div>
        <div class="comment">
        	<p>The path to a png file you have uploaded to the server (<strong><a href="http://mathiasbynens.be/notes/touch-icons#sizes">120x120 pixels</a></strong>). Can contain:</p>
            <ul>
            	<li><samp>{theme}</samp> to indicate the path to the current theme (for example: <samp>{theme}/iphone-retina-app-icon.png</samp>)</li>
            </ul>
        </div>
    </div>
    
    <div class="question">
        <label for="ipad-retina-app-icon">iPad (retina) app icon (144x144 pixels)</label>
        <div class="input"><input type="text" name="ipad-retina-app-icon" id="ipad-retina-app-icon" value="<?php echo get_option('ipad-retina-app-icon'); ?>" size="60" /></div>
        <div class="comment">
        	<p>The path to a png file you have uploaded to the server (<strong>144x144 pixels</strong>). Can contain:</p>
            <ul>
            	<li><samp>{theme}</samp> to indicate the path to the current theme (for example: <samp>{theme}/ipad-retina-app-icon.png</samp>)</li>
            </ul>
        </div>
    </div>
    
    <div class="question checkbox">
    	<h3>Pre-composed?</h3>
        <div class="input">
        <label for="icons-are-precomposed">
        	<input type="checkbox" name="icons-are-precomposed" id="icons-are-precomposed" value="true" <?php echo get_option('icons-are-precomposed') == 'true' ? ' checked="checked"' : '' ; ?> />
            My icons above are 'pre-composed' (don't mess with 'em)
        </label>
        </div>
        <div class="comment">
        	<p>Check this box if you don't want Apple to add the 'glossy' effect to your icons.</p>
        </div>
    </div>
    
    
    <hr />
    
    <fieldset><legend>Author avatar app icons</legend>
	<div class="question checkbox">
        <h3>Use avatars?</h3>
        <div class="input"><label for="use-author-app-icons">
        	<input type="checkbox" name="use-author-app-icons" id="use-author-app-icons" value="true" <?php echo get_option('use-author-app-icons') == 'true' ? ' checked="checked"' : '' ; ?> />
            Use avatars as app icons for author pages
        </label></div>
    </div>
    
    </fieldset>
    
    <div class="actions">
        <input type="submit" class="button-primary" value="<?php _e('Save Changes') ?>" />
    </div>

</form></div>
<?php
}

function cai_add_admin() {
    add_submenu_page('options-general.php', 'Custom app icons', 'Custom app icons', 10, __FILE__, 'cai_admin');
}

function cai_add_icons () {
	
	if (is_author() && get_option('use-author-app-icons') == 'true') {
		global $author;
		$curauth = get_userdata(intval($author));
		//global $defaultAvatar;
		
		
		echo cai_format_icon_link(cai_get_gravatar_url ($curauth->user_email, $size=57), null, false);
		echo cai_format_icon_link(cai_get_gravatar_url ($curauth->user_email, $size=72), 72, false);
		echo cai_format_icon_link(cai_get_gravatar_url ($curauth->user_email, $size=114), 114, false);
		echo cai_format_icon_link(cai_get_gravatar_url ($curauth->user_email, $size=120), 120, false);
		echo cai_format_icon_link(cai_get_gravatar_url ($curauth->user_email, $size=144), 144, false);
		
		
	} else {
				
		$search = array(
			'{theme}'
		);
		$replace = array(
			cai_get_theme_path()
		);
		
		$defaultIconPath = str_replace($search, $replace, get_option('default-app-icon'));
		$ipadIconPath = str_replace($search, $replace, get_option('ipad-app-icon'));
		$ipadRetinaIconPath = str_replace($search, $replace, get_option('ipad-retina-app-icon'));
		$iphoneRetinaIconPath = str_replace($search, $replace, get_option('iphone-retina-app-icon'));
		$iphoneOldRetinaIconPath = str_replace($search, $replace, get_option('iphone-old-retina-app-icon'));
		
		
		echo "\n\n";
		
		if ($defaultIconPath !== '') {
			echo cai_format_icon_link($defaultIconPath, null, get_option('icons-are-precomposed'));
		}
		if ($ipadIconPath !== '') {
			echo cai_format_icon_link($ipadIconPath, 72, get_option('icons-are-precomposed'));
		}
		if ($iphoneOldRetinaIconPath !== '') {
			echo cai_format_icon_link($iphoneOldRetinaIconPath, 114, get_option('icons-are-precomposed'));
		}
		if ($iphoneRetinaIconPath !== '') {
			echo cai_format_icon_link($iphoneRetinaIconPath, 120, get_option('icons-are-precomposed'));
		}
		if ($ipadRetinaIconPath !== '') {
			echo cai_format_icon_link($ipadRetinaIconPath, 144, get_option('icons-are-precomposed'));
		}
		
		echo "\n\n";
	}
}

function cai_format_icon_link ($imgPath, $size=null, $isPreComp=false) {
	
	$rel = ($isPreComp)?'apple-touch-icon-precomposed':'apple-touch-icon';
	$sizes = ($size===null)?'':' sizes="'.$size.'x'.$size.'"';
	
	return '<link rel="'.$rel.'"'.$sizes.' href="'.$imgPath.'" />'."\n";
}

/**
 * @author irama.org
 * @since 16/03/2010
 */
function cai_get_gravatar_url ($email, $size=64, $defaultAvatar=null) {
	
	ob_start();
	if ($defaultAvatar !== null) {
		echo get_avatar( $email, $size, $defaultAvatar );
	} else {
		echo get_avatar( $email, $size );
	}
	$avatar = ob_get_contents();
	ob_end_clean();
	
	//echo '<!-- '.$avatar.' -->';
	
	//$links_regex = "/src=\'(?P<url>[^\']*?)\'/Ui";
	$links_regex = "/src=[\'\\\"](?P<url>[^\'\\\"]*?)[\'\\\"]/Ui";
	
	preg_match_all($links_regex, $avatar, $matches);
	/*
	echo '<pre>';
	var_dump($matches['url'][0]);
	echo '</pre>';
	*/
	return $matches['url'][0];
}

function cai_get_theme_path () {
    ob_start();
	bloginfo('stylesheet_directory');
	$path = ob_get_contents();
	ob_end_clean();
	return $path;
}


add_action('wp_head', 'cai_add_icons');

add_action('admin_menu', 'cai_add_admin');

?>