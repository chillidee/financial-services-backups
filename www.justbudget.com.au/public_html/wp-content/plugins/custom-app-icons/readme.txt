=== Custom App Icons ===
Contributors: andrew@irama.org
Tags: icon
Requires at least: 2.7
Tested up to: 3.5.1
Stable tag: 1.1.0

Allows you to set a custom app icons for website shortcuts saved to homescreen on iphone and ipod touch.

== Description ==

Allows you to set a custom app icons for website shortcuts saved to Home Screen on iphone and ipod touch. Also allows you to specify whether author avatars should be used as the application icon for author pages.


== Installation ==

1. Upload the Custom Author Base plugin to your 'wp-content/plugins' directory
2. activate the plugin
3. Upload a 45x45 pixel app icon (tested with .png files)
4. Copy the path of the app icon into the configuration page for Custom App Icon plugin.
5. Save your changes


== Version History ==

* 1.0.0 - 2010-03-18 - Initial release version
* 1.1.0 - 2013-07-07 - Initial release version