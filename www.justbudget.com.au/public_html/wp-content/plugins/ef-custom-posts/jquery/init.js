var ef = jQuery;
ef.noConflict();

/*-------------------------------------------------------*/
/*------------------- Main Variables --------------------*/
/*-------------------------------------------------------*/

var $icontainer = ef('.ef-portfolio-wrap'),
	$portfolioFilteritem = ef('.ef-portfolio-filter a'),
	$portfolioFilterli = ef('.ef-portfolio-filter li'),
	$currentClass = ef('.ef-currentclass'),
	$addItems = ef('#ef-addnewitems');

/*-------------------------------------------------------*/
/*-------------------------------------------------------*/
/*-------------------------------------------------------*/
ef(document).ready(function() {

	function enlarge_class() {
		ef('.ef-has-lightbox .ef-proj-img').hover(function() {
			ef(this).parent().find('.ef-link-marker').toggleClass('ef-enlarge');
		});
	}

	enlarge_class();

	ef('.ef-link-marker').html('<span><span><span></span></span></span>');

	$portfolioFilteritem.each(function() {
		if (!ef(this).parent().hasClass('ef-all-works')) {
			var _class = ef(this).attr('data-option-value').slice(1);
			if (!ef('.ef-portfolio-item').hasClass(_class)) {
				ef(this).parent('li').hide().addClass('ef-no-posts');
			}
		}
	});

	$portfolioFilteritem.click(function(e) {
		e.preventDefault();
		var selector = ef(this).attr('data-option-value');
		$icontainer.isotope({
			filter: selector
		});
		$portfolioFilterli.removeClass();
		ef(this).parent('li').addClass('ef-currentclass');
	});


	var post_per = ef_ajax.offset,
		offset = ef_ajax.offset,
		total = ef_ajax.postscount;

	$addItems.click(function(e) {
		$self = ef(this);
		e.preventDefault();

		ef.ajax({
			type: "post",
			cache: false,
			timeout: 8000,
			url: ef_ajax.ajaxurl,
			data: ({
				action: 'ef_ajax_posts',
				offset: offset,
				postCommentNonce: ef_ajax.postCommentNonce,
				thumb_tip: ef_ajax.thumb_tip,
				lb_lnk: ef_ajax.lb_lnk,
				terms: ef_ajax.terms
			}),

			beforeSend: function() {
				$self.find('.ef-loader').css({
					display: 'block'
				});
			},

			success: function(data, textStatus, jqXHR) {
				var $newElems = ef(data);
				$newElems.imagesLoaded(function() {
					$icontainer.isotope('insert', $newElems);
					ef('.ef-proj-img').find('.ef-loader').hide();

					ef("a[data-gal^='lb']").prettyPhoto({
						hook: 'data-gal',
						counter_separator_label: ' of ',
						overlay_gallery: false,
						social_tools: false
					});

					$portfolioFilterli.each(function() {
						var _class = ef(this).children('a').attr('data-option-value').slice(1);
						if (ef('.ef-portfolio-item').hasClass(_class)) {
							ef(this).show().removeClass('ef-no-posts');
						}
					});

					enlarge_class();

					ef('.ef-link-marker').html('<span><span><span></span></span></span>');
				});
			},

			complete: function(jqXHR, textStatus) {

				$self.find('.ef-loader').fadeOut();
				offset = parseInt(offset, null) + parseInt(post_per, null);
				if (offset >= total) {
					$self.fadeOut();
				}

				enlarge_class();
			}
		});

	});

});

ef(window).load(function() {

	/* Isotope Portfolio */
	$icontainer.isotope();

});