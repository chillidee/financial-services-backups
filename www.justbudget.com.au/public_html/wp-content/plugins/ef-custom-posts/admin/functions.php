<?php

/**
 * Required scripts/stylesheets
 *
 * @since 1.0.0
 */
function ef_cpt_enqueue_scripts() {
	
	if ( class_exists( 'EfGetOptions' ) ) {
	
		$ef_data = EfGetOptions::TplConditions();
	
		$args = array(
			'post_type' => EF_CPT,
			'tax_query' => array(
				array('taxonomy'=> EF_CPT_TAX_CAT,
					'terms'		=> $ef_data['terms'],
					'field'		=> 'id',
					'operator'	=> 'NOT IN'
				)
			),
			'posts_per_page' 	=> -1,
			'meta_key' 			=> '_thumbnail_id'
		);
		$postslist = new WP_Query( $args );
		$postscount = count( $postslist->posts );
		

		if ( !isset( $GLOBALS['wp_scripts']->registered[ 'jquery' ] ) ) {
			wp_enqueue_script( 'jquery' );
		}
		
		wp_register_script( 'isotope', EF_CPT_JS_URL . 'jquery.isotope.min.js',array('jquery'),'',true );
		wp_enqueue_script( 'isotope' );
		
		wp_register_script( 'cpt-custom', EF_CPT_JS_URL . 'init.js',array('jquery'),'',true );
		wp_enqueue_script( 'cpt-custom' );
		
		$vars_array = array(
			'postCommentNonce' 	=> wp_create_nonce( 'ef_ajax-post-comment-nonce' ),
			'ajaxurl' 			=> admin_url( 'admin-ajax.php' ),
			'offset' 			=> isset( $ef_data['posts_per'] ) ? $ef_data['posts_per'] : '',
			'postscount' 		=> $postscount,
			'thumb_tip'			=> isset( $ef_data['thumb_tip'] ) ? $ef_data['thumb_tip'] : '',
			'lb_lnk'			=> isset( $ef_data['lb_lnk'] ) ? $ef_data['lb_lnk'] : '',
			'terms'				=> $ef_data['terms'],
		);
		
		wp_localize_script( 'cpt-custom', 'ef_ajax', $vars_array );	
	}
	
}
add_action( 'wp_enqueue_scripts', 'ef_cpt_enqueue_scripts' );

/**
 * Include custom post types in search results
 *
 * @since Favea 1.0
 */
function ef_include_cpt_search($query) {
  if ( !is_admin() && $query->is_main_query() ) {
    if ($query->is_search) {
      $query->set( EF_CPT, EF_CPT_EXTR, EF_CPT_TEAM, 'post' );
    }
  }
}
add_action('pre_get_posts','ef_include_cpt_search');