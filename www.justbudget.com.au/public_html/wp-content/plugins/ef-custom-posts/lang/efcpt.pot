msgid ""
msgstr ""
"Project-Id-Version: Fireform CPT\n"
"POT-Creation-Date: 2013-09-03 19:48+0300\n"
"PO-Revision-Date: 2013-09-03 19:49+0300\n"
"Last-Translator: fireform <twm2003@gmail.com>\n"
"Language-Team: Fireform\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.5.7\n"
"X-Poedit-KeywordsList: __;_e;_x;_n\n"
"X-Poedit-Basepath: /Users/fireform/wordpress/wp-content/plugins/ef-custom-"
"posts\n"
"X-Poedit-SearchPath-0: /Users/fireform/wordpress/wp-content/plugins/ef-"
"custom-posts\n"

#: /Users/fireform/wordpress/wp-content/plugins/ef-custom-posts/admin/register.php:13
#: /Users/fireform/wordpress/wp-content/plugins/ef-custom-posts/admin/register.php:25
msgid "Portfolios"
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-custom-posts/admin/register.php:14
msgid "Portfolio"
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-custom-posts/admin/register.php:15
#: /Users/fireform/wordpress/wp-content/plugins/ef-custom-posts/admin/register.php:94
#: /Users/fireform/wordpress/wp-content/plugins/ef-custom-posts/admin/register.php:151
msgid "Add New"
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-custom-posts/admin/register.php:16
msgid "Add New Portfolio post"
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-custom-posts/admin/register.php:17
msgid "Edit Portfolio post"
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-custom-posts/admin/register.php:18
msgid "New Portfolio post"
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-custom-posts/admin/register.php:19
#: /Users/fireform/wordpress/wp-content/plugins/ef-custom-posts/admin/register.php:98
#: /Users/fireform/wordpress/wp-content/plugins/ef-custom-posts/admin/register.php:155
msgid "All Posts"
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-custom-posts/admin/register.php:20
msgid "View Portfolio post"
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-custom-posts/admin/register.php:21
msgid "Search Portfolio posts"
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-custom-posts/admin/register.php:22
msgid "No Portfolio posts found"
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-custom-posts/admin/register.php:23
msgid "No Portfolio posts found in the Trash"
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-custom-posts/admin/register.php:30
msgid "Holds Portfolio specific data"
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-custom-posts/admin/register.php:44
msgid "Portfolio categories"
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-custom-posts/admin/register.php:45
msgid "Portfolio category"
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-custom-posts/admin/register.php:46
msgid "Search Portfolio categories"
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-custom-posts/admin/register.php:47
msgid "All Portfolio categories"
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-custom-posts/admin/register.php:48
msgid "Parent Portfolio category"
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-custom-posts/admin/register.php:49
msgid "Parent Portfolio category:"
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-custom-posts/admin/register.php:50
msgid "Edit Portfolio category"
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-custom-posts/admin/register.php:51
msgid "Update Portfolio category"
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-custom-posts/admin/register.php:52
msgid "Add New Portfolio category"
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-custom-posts/admin/register.php:53
msgid "New Portfolio Category"
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-custom-posts/admin/register.php:54
#: /Users/fireform/wordpress/wp-content/plugins/ef-custom-posts/admin/register.php:133
#: /Users/fireform/wordpress/wp-content/plugins/ef-custom-posts/admin/register.php:190
msgid "Categories"
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-custom-posts/admin/register.php:69
msgid "Portfolio tags"
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-custom-posts/admin/register.php:70
msgid "Portfolio tag"
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-custom-posts/admin/register.php:71
msgid "Search Portfolio tags"
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-custom-posts/admin/register.php:72
msgid "All Portfolio tags"
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-custom-posts/admin/register.php:73
msgid "Parent Portfolio tag"
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-custom-posts/admin/register.php:74
msgid "Parent Portfolio tag:"
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-custom-posts/admin/register.php:75
msgid "Edit Portfolio tag"
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-custom-posts/admin/register.php:76
msgid "Update Portfolio tag"
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-custom-posts/admin/register.php:77
msgid "Add New Portfolio tag"
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-custom-posts/admin/register.php:78
msgid "New Portfolio tag"
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-custom-posts/admin/register.php:79
msgid "Tags"
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-custom-posts/admin/register.php:92
#: /Users/fireform/wordpress/wp-content/plugins/ef-custom-posts/admin/register.php:104
msgid "Extras"
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-custom-posts/admin/register.php:93
msgid "Extra"
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-custom-posts/admin/register.php:95
msgid "Add New Extra post"
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-custom-posts/admin/register.php:96
msgid "Edit Extra post"
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-custom-posts/admin/register.php:97
msgid "New Extra post"
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-custom-posts/admin/register.php:99
msgid "View Extra post"
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-custom-posts/admin/register.php:100
msgid "Search Extra posts"
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-custom-posts/admin/register.php:101
msgid "No Extra posts found"
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-custom-posts/admin/register.php:102
msgid "No Extra posts found in the Trash"
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-custom-posts/admin/register.php:109
msgid "Holds Extras specific data"
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-custom-posts/admin/register.php:123
msgid "Extra categories"
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-custom-posts/admin/register.php:124
msgid "Extra category"
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-custom-posts/admin/register.php:125
#: /Users/fireform/wordpress/wp-content/plugins/ef-custom-posts/admin/register.php:182
msgid "Search Extra categories"
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-custom-posts/admin/register.php:126
msgid "All Extra categories"
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-custom-posts/admin/register.php:127
msgid "Parent Extra category"
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-custom-posts/admin/register.php:128
msgid "Parent Extra category:"
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-custom-posts/admin/register.php:129
msgid "Edit Extra category"
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-custom-posts/admin/register.php:130
msgid "Update Extra category"
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-custom-posts/admin/register.php:131
msgid "Add New Extra category"
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-custom-posts/admin/register.php:132
msgid "New Extra category"
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-custom-posts/admin/register.php:149
#: /Users/fireform/wordpress/wp-content/plugins/ef-custom-posts/admin/register.php:150
#: /Users/fireform/wordpress/wp-content/plugins/ef-custom-posts/admin/register.php:161
msgid "Team"
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-custom-posts/admin/register.php:152
msgid "Add New Team post"
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-custom-posts/admin/register.php:153
msgid "Edit Team post"
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-custom-posts/admin/register.php:154
msgid "New Team post"
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-custom-posts/admin/register.php:156
msgid "View Team post"
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-custom-posts/admin/register.php:157
msgid "Search Team posts"
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-custom-posts/admin/register.php:158
msgid "No Team posts found"
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-custom-posts/admin/register.php:159
msgid "No Team posts in the Trash"
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-custom-posts/admin/register.php:166
msgid "Holds Team specific data"
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-custom-posts/admin/register.php:180
msgid "Team categories"
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-custom-posts/admin/register.php:181
msgid "Team category"
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-custom-posts/admin/register.php:183
msgid "All Team categories"
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-custom-posts/admin/register.php:184
msgid "Parent Team category"
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-custom-posts/admin/register.php:185
msgid "Parent Team category:"
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-custom-posts/admin/register.php:186
msgid "Edit Team category"
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-custom-posts/admin/register.php:187
msgid "Update Team category"
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-custom-posts/admin/register.php:188
msgid "Add New Team category"
msgstr ""

#: /Users/fireform/wordpress/wp-content/plugins/ef-custom-posts/admin/register.php:189
msgid "New Team category"
msgstr ""
