var ef = jQuery;
ef.noConflict();

/*-------------------------------------------------------*/
/*------------------- Main Variables --------------------*/
/*-------------------------------------------------------*/

/* Accordion Class */
var $accClass = ef(".ef-uiaccordion");
/* Toggle Panels Class */
var $toggleClass = ef(".ef-toggle");
/* Progressbars class */
var $progressB = ef('.ef-progress-bar div');

/*-------------------------------------------------------*/
/*-------------------------------------------------------*/
/*-------------------------------------------------------*/

ef(document).ready(function() {

	/*Skill graphs*/
	$progressB.each(function() {
		var pc = ef(this).attr('data-id') + '%';

		ef(this).append('<span><span></span></span>');
		ef(this).prev().append(' - ' + pc);
		ef(this).children().animate({
			'width': pc
		}, 1500, 'easeOutExpo');
	});


	/*jQuery UI Accordion*/
	$accClass.accordion({
		heightStyle: "content"
	});


	/*Toggles*/
	$toggleClass.addClass("ui-accordion ui-accordion-icons ui-widget ui-helper-reset")
		.find("h4")
		.addClass("ui-accordion-header ui-helper-reset ui-state-default ui-corner-top ui-corner-bottom")
		.prepend('<span class="ui-accordion-header-icon ui-icon ui-icon-triangle-1-e"></span>')
		.click(function() {
			ef(this)
				.toggleClass("ui-accordion-header-active ui-state-active ui-state-default ui-corner-bottom")
				.find("> .ui-icon").toggleClass("ui-icon-triangle-1-e ui-icon-triangle-1-s").end()
				.next().toggleClass("ui-accordion-content-active").slideToggle();
			return false;
		})
		.next()
		.addClass("ui-accordion-content  ui-helper-reset ui-widget-content ui-corner-bottom")
		.hide();


	/*jQuery UI Tabs*/
	ef('.ef-tabs').tabs({
		heightStyle: 'content',
		active: 0
	});

});