jQuery.validator.setDefaults({
	debug: true
});

jQuery('#insert').click(function() {
	jQuery("#myshortcode-form").validate({
		rules: {
			numberfield: {
				required: true,
				digits: true,
				min: 1
			},
			duration: {
				required: true,
				digits: true,
				min: 500
			},
			numberslidefield: {
				required: true,
				digits: true,
				min: 1
			}
		},
		submitHandler: function() {
			var numberfield = jQuery('#numberfield').val(),
				checkfield = jQuery('#checkfield:checkbox').prop("checked") ? 'yes' : 'no',
				numberslidefield = jQuery('#numberslidefield').val(),
				show_lnk = jQuery('select.ef-select-lnk > option:selected').val(),
				permtitle = jQuery('#permtitle').val(),
				titlefield = jQuery('#titlefield').val(),
				autoplay = jQuery('#autoplay:checkbox').prop("checked") ? 'yes' : 'no',
				infinite = jQuery('#infinite:checkbox').prop("checked") ? 'yes' : 'no',
				duration = jQuery('#duration').val(),
				transition = jQuery('#trans-select > option:selected').val(),
				categoryfield = [],
				shortcode;

			jQuery('select.ef-select-cats > option:selected').each(function(i) {
				categoryfield[i] = jQuery(this).attr("data-id");
			});

			shortcode = '[ef-recent-posts number="' + numberfield + '" image="' + checkfield + '" in_slide="' + numberslidefield + '" exclude="' + categoryfield + '" link="' + show_lnk + '" link_title="' + permtitle + '" autoplay="' + autoplay + '" infinite="' + infinite + '" duration="' + duration + '" fx="' + transition + '"]' + titlefield + '[/ef-recent-posts]';

			tinyMCEPopup.execCommand('mceInsertContent', 0, shortcode);
			tinyMCEPopup.editor.execCommand('mceRepaint');
			tinyMCEPopup.close();
		}
	});
});