jQuery.validator.setDefaults({
	debug: true
});

jQuery('#insert').click(function() {
	jQuery("#myshortcode-form").validate({
		rules: {
			titlefield: "required"
		},
		submitHandler: function() {
			var titleField = jQuery('#titlefield').val(),
				paneltype = jQuery('#panel-type > option:selected').attr('data-id'),
				panelstyle = jQuery('#panel-style > option:selected').attr('data-id'),
				checkfield = jQuery('#checkfield:checkbox').prop("checked") ? 'yes' : 'no',
				shortcode;
				
			shortcode = '[ef-alert-box type="' + paneltype + '" style="' + panelstyle + '" close="' + checkfield + '"]' + titleField + '[/ef-alert-box]';
			
			tinyMCEPopup.execCommand('mceInsertContent', 0, shortcode);
			tinyMCEPopup.editor.execCommand('mceRepaint');
			tinyMCEPopup.close();
		}
	});
});