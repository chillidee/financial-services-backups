jQuery.validator.setDefaults({
	debug: true
});

jQuery('#insert').click(function() {
	jQuery("#myshortcode-form").validate({
		rules: {
			duration: {
				required: true,
				digits: true,
				min: 500
			}
		},
		submitHandler: function() {
			var slider = '',
				oldslider = '',
				titlefield = jQuery('#titlefield').val(),
				slidertype = jQuery('#slidertype > option:selected').val(),
				slide = jQuery('#numberslidefield > option:selected').val(),
				autoplay = jQuery('#autoplay:checkbox').prop("checked") ? 'yes' : 'no',
				infinite = jQuery('#infinite:checkbox').prop("checked") ? 'yes' : 'no',
				duration = jQuery('#duration').val(),
				transition = jQuery('#trans-select > option:selected').val(),
				textImg = slidertype == 'carousel' ? '<p>Any content goes here</p>' : '<img style="display: block" src="' + jQuery('#slidertype > option:selected').attr('data-url') + 'img/placeholder.jpg" alt="" />',
				shortcode;

			for (var i = 0; i < slide; i++) {
				oldslider = slider;
				slider = '[ef-slide]' + textImg + '[/ef-slide]<br />';
				slider = oldslider + slider;
			}

			shortcode = '[ef-slider title="' + titlefield + '" type="' + slidertype + '" autoplay="' + autoplay + '" infinite="' + infinite + '" duration="' + duration + '" fx="' + transition + '"]<br />' + slider + '[/ef-slider]';

			tinyMCEPopup.execCommand('mceInsertContent', 0, shortcode);
			tinyMCEPopup.editor.execCommand('mceRepaint');
			tinyMCEPopup.close();
		}
	});
});