jQuery(document).ready(function() {

	jQuery.validator.setDefaults({
		debug: true
	});

	$i = 1;
	jQuery('#add-fields').click(function() {
		$i++;

		jQuery('.grouped:last-child')
			.clone().css({
				display: 'none'
			})
			.insertAfter('.grouped:last-child')
			.fadeIn()
			.find('.titlefield').attr('name', 'titlefield' + $i);

		if ($i == 10) {
			jQuery('#add-fields').fadeOut();
		}
	});

	jQuery('#insert').click(function() {

		jQuery('#myshortcode-form').validate({
			rules: {
				titlefield: "required",
				titlefield2: "required",
				titlefield3: "required",
				titlefield4: "required",
				titlefield5: "required",
				titlefield6: "required",
				titlefield7: "required",
				titlefield8: "required",
				titlefield9: "required",
				titlefield10: "required",
			},
			submitHandler: function() {

				var pane = '',
					oldpane = '',
					val1_ = '',
					val2_ = '';

				jQuery('.grouped').each(function() {

					oldpane = pane;

					val1_ = jQuery(this).find('.titlefield').val(),
					val2_ = jQuery(this).find('.percents > option:selected').val(),
					pane = '[ef-progress-pane percent="' + val2_ + '"]' + val1_ + '[/ef-progress-pane]<br />';
					pane = oldpane + pane;

				});

				var shortcode;

				shortcode = '[ef-progress-bar]<br />' + pane + '[/ef-progress-bar]';

				tinyMCEPopup.execCommand('mceInsertContent', 0, shortcode);
				tinyMCEPopup.editor.execCommand('mceRepaint');
				tinyMCEPopup.close();
			}
		});

	});
});