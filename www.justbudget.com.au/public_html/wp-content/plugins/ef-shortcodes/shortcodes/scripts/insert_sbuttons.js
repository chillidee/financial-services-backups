jQuery('#insert').click(function() {

	var but = '',
		oldBut = '',
		targetField = jQuery('#targetfield').val();

	jQuery('.soc-icn').each(function() {

		if (jQuery(this).val() !== '') {

			oldBut = but;
			var urlField = jQuery(this).val(),
				title = jQuery(this).parent().parent().find('label').attr('data-id'),
				_class = jQuery(this).attr('data-id');

			but = '[ef-soc-button class="' + _class + '" href="' + urlField + '" title="' + title + '" target="' + targetField + '"][/ef-soc-button]<br />';
			but = oldBut + but;
		}

	});

	var shortcode;

	shortcode = '[ef-soc-button-list]<br />' + but + '[/ef-soc-button-list]';

	tinyMCEPopup.execCommand('mceInsertContent', 0, shortcode);
	tinyMCEPopup.editor.execCommand('mceRepaint');
	tinyMCEPopup.close();
});