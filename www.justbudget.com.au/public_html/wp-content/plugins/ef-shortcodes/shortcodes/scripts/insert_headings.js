jQuery.validator.setDefaults({
	debug: true
});

jQuery('#insert').click(function() {
	jQuery("#myshortcode-form").validate({
		rules: {
			titlefield: "required"
		},
		submitHandler: function() {
			var titleField = jQuery('#titlefield').val(),
				headClass = jQuery('#head-class > option:selected').attr('data-id'),
				headType = jQuery('#head-type > option:selected').attr('data-id'),
				headSize = jQuery('#head-size > option:selected').attr('data-id'),
				shortcode;

			shortcode = '[ef-heading class="' + headClass + '" type="' + headType + '" size="' + headSize + '"]' + titleField + '[/ef-heading]';

			tinyMCEPopup.execCommand('mceInsertContent', 0, shortcode);
			tinyMCEPopup.editor.execCommand('mceRepaint');
			tinyMCEPopup.close();
		}
	});
});