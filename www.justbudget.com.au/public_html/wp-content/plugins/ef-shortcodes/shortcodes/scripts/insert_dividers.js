jQuery('#insert').click(function() {

	var headType = jQuery('#hr-type > option:selected').val(),
		headClass = jQuery('#hr-class > option:selected').val(),
		shortcode;

	shortcode = '[ef-hr type="' + headType + '" class="' + headClass + '"][/ef-hr]';

	tinyMCEPopup.execCommand('mceInsertContent', 0, shortcode);
	tinyMCEPopup.editor.execCommand('mceRepaint');
	tinyMCEPopup.close();

});