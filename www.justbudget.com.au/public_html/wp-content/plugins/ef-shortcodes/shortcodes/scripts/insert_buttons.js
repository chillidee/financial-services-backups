jQuery.validator.setDefaults({
	debug: true
});

jQuery('#insert').click(function() {
	jQuery("#myshortcode-form").validate({
		rules: {
			urlfield: {
				required: true,
				url: true
			},
			titlefield: "required"
		},
		submitHandler: function() {
			var urlField = jQuery('#urlfield').val(),
				titleField = jQuery('#titlefield').val(),
				buttonsize = jQuery('#button-size > option:selected').attr('data-id'),
				buttontype = jQuery('#button-type > option:selected').attr('data-id'),
				targetField = jQuery('#targetfield').val(),
				shortcode;
				
			shortcode = '[ef-button class="button' + ' ' + buttonsize + ' ' + buttontype + '" href="' + urlField + '" title="' + titleField + '" target="' + targetField + '"]' + titleField + '[/ef-button]';
			
			tinyMCEPopup.execCommand('mceInsertContent', 0, shortcode);
			tinyMCEPopup.editor.execCommand('mceRepaint');
			tinyMCEPopup.close();
		}
	});
});