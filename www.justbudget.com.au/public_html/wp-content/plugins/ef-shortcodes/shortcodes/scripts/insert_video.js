jQuery.validator.setDefaults({
	debug: true
});

jQuery('#insert').click(function() {
	jQuery("#myshortcode-form").validate({
		rules: {
			urlfield: {
				required: true,
				url: true
			},
			titlefield: "required"
		},
		submitHandler: function() {
			var urlField = jQuery('#urlfield').val(),
				videoType = jQuery('option:selected').val(),
				shortcode;

			shortcode = '[ef-video type="' + videoType + '" href="' + urlField + '"][/ef-video]';

			tinyMCEPopup.execCommand('mceInsertContent', 0, shortcode);
			tinyMCEPopup.editor.execCommand('mceRepaint');
			tinyMCEPopup.close();
		}
	});
});