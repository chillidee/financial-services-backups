jQuery.validator.setDefaults({
	debug: true
});

$i = 1;
jQuery('#add-fields').click(function() {
	$i++;

	jQuery('.grouped:last-child')
		.clone().css({
			display: 'none'
		})
		.insertAfter('.grouped:last-child')
		.fadeIn()
		.find('.ui-header').attr('value', 'Header ' + $i).attr('name', 'head' + $i)
		.parent().parent().parent().find('.pane-content').attr('name', 'pane' + $i);

	if ($i == 10) {
		jQuery('#add-fields').fadeOut();
	}
});

jQuery('#insert').click(function() {

	jQuery('#myshortcode-form').validate({
		rules: {
			head: "required",
			pane: "required",
			head2: "required",
			pane2: "required",
			head3: "required",
			pane3: "required",
			head4: "required",
			pane4: "required",
			head5: "required",
			pane5: "required",
			head6: "required",
			pane6: "required",
			head7: "required",
			pane7: "required",
			head8: "required",
			pane8: "required",
			head9: "required",
			pane9: "required",
			head10: "required",
			pane10: "required",
		},
		submitHandler: function() {

			var val_ = '',
				head = '',
				oldhead = '',
				content = '',
				oldcontent = '',
				headContent = '',
				oldHeadContent = '';
			$typo = jQuery('#elem-select').val();

			$n = 1;
			jQuery('.grouped').each(function() {

				if ($typo == 'tabs') {

					oldhead = head;
					oldcontent = content;

					val_ = jQuery(this).find('input').val();
					head = '[ef-ui-header id="#ef-tab-' + $n + '" for="' + $typo + '"]' + val_ + '[/ef-ui-header]<br />';
					val_ = jQuery(this).find('textarea').val();
					content = '[ef-ui-pane id="ef-tab-' + $n + '" for="' + $typo + '"]' + val_ + '[/ef-ui-pane]<br />';

					head = oldhead + head;
					content = oldcontent + content;

					headContent = '[ef-ui-tabs-nav]<br />' + head + '[/ef-ui-tabs-nav]<br />' + content;

					$n++;

				} else {

					oldHeadContent = headContent;

					val_ = jQuery(this).find('input').val();
					head = '[ef-ui-header for="' + $typo + '"]' + val_ + '[/ef-ui-header]<br />';
					val_ = jQuery(this).find('textarea').val();
					content = '[ef-ui-pane for="' + $typo + '"]' + val_ + '[/ef-ui-pane]<br />';

					headContent = head + content;
					headContent = oldHeadContent + headContent;
				}

			});

			var shortcode;

			shortcode = '[ef-ui-element type="' + $typo + '"]<br />' + headContent + '[/ef-ui-element]';

			tinyMCEPopup.execCommand('mceInsertContent', 0, shortcode);
			tinyMCEPopup.editor.execCommand('mceRepaint');
			tinyMCEPopup.close();
		}
	});

});