<?php

/* -------- Headings ------- */

add_shortcode( 'ef-heading', 'ef_headings' );
function ef_headings( $atts, $content = '' ) {

	if ( empty( $atts ) || empty( $content ) ) return;

	extract( $atts );

	$size = !empty( $size ) ? $size : 'h4';
	$class = !empty( $class ) ? esc_attr( $class ) . ' ' : '';
	$type = !empty( $type ) ? esc_attr( $type ) : '';
	$classType = !empty( $class ) || !empty( $type ) ? ' class="' . $class . $type . '"' : '';

	return '<' . $size . $classType . '>' . strip_tags( $content, '<span>' ) . '</' . $size . '>';
}

/* -------- Dividers ------- */

add_shortcode( 'ef-hr', 'ef_dividers' );
function ef_dividers( $atts, $content = '' ) {

	if ( empty( $atts ) ) return;

	extract( $atts );

	$type = !empty( $type ) ? $type . ' ': '';
	$class = !empty( $class ) ? $class : '';
	$class_type = $type . $class;

	return '<hr class="' . $class_type . '">';
}

/* -------- Columns ------- */

add_shortcode( 'ef-row', 'ef_rows' );
add_shortcode( 'ef-columns', 'ef_columns' );
function ef_rows( $atts, $content = null ) {

	return '<div class="row">' . do_shortcode( $content ) . '</div>';
}

function ef_columns( $atts, $content = null ) {

	if ( empty( $atts ) || empty( $content ) ) return;

	extract( $atts );

	if ( empty( $part ) ) return;

	$cols = '<div class="large-'.$part.' columns">' . do_shortcode( $content ) . '</div>';

	return $cols;
}

/* -------- Buttons ------- */

add_shortcode( 'ef-button', 'ef_buttons' );
function ef_buttons( $atts, $content = NULL ) {

	if ( empty( $atts ) || empty( $content ) ) return;

	extract( $atts );

	$class = !empty( $class ) ? esc_attr( $class ) : 'button';
	$href = !empty( $href ) ? esc_url( $href ) : '#';
	$title = !empty( $title ) ? esc_attr( $title ) : '';
	$target = !empty( $target ) ? esc_attr( $target ) : '';

	return '<a class="'. $class . '" href="'. $href . '" title="'. $title . '" target="'. $target . '">'. strip_tags( $content, '<span>' ) . '</a>';
}


/* -------- Social buttons ------- */

add_shortcode( 'ef-soc-button-list', 'ef_sbuttons_list' );
add_shortcode( 'ef-soc-button', 'ef_sbuttons' );
function ef_sbuttons_list( $atts, $content = NULL ) {

	return '<ul class="ef-soc-icons hide-for-print clearfix">'. do_shortcode( $content ) . '</ul>';
}

function ef_sbuttons( $atts, $content = NULL ) {

	if ( empty( $atts ) ) return;

	extract( $atts );

	if ( empty( $href ) || empty( $class ) || empty( $title ) ) return;

	$href = !empty( $href ) ? esc_url( $href ) : '#';
	$target = !empty( $target ) ? esc_attr( $target ) : '';

	return '<li><a class="'. esc_attr( $class ) . ' ef-tipsy-n" href="'. $href . '" title="'. esc_attr( $title ) . '" target="'. $target . '"></a></li>';
}


/* -------- Panels ------- */

add_shortcode( 'ef-panel', 'ef_panels' );
function ef_panels( $atts, $content = NULL ) {

	if ( empty( $atts ) || empty( $content ) ) return;

	extract( $atts );
	$style = !empty( $style ) ? ' '.esc_attr( $style ) : ' radius';
	$type = !empty( $type ) ? ' '.esc_attr( $type ) : '';

	return '<div class="panel'. $type . $style . '">'. do_shortcode( $content ) . '</div>';
}

/* -------- Alerts ------- */

add_shortcode( 'ef-alert-box', 'ef_alerts' );
function ef_alerts( $atts, $content = NULL ) {

	if ( empty( $atts ) || empty( $content ) ) return;

	extract( $atts );
	$style = !empty( $style ) ? ' '.esc_attr( $style ) : ' radius';
	$type = !empty( $type ) ? ' '.esc_attr( $type ) : '';
	if ( !empty( $close ) && $close == 'yes' ) {
		$close = '<a href="#" class="close">&times;</a>';
		$alert_attr = ' data-alert';
	} else {
		$close = $alert_attr = '';
	}

	return '<div' . $alert_attr . ' class="alert-box'. $type . $style . '">' . strip_tags( $content, '<span>,<p>' ) . $close . '</div>';
}

/* -------- Progressbars ------- */

add_shortcode( 'ef-progress-bar', 'ef_progress_bar' );
add_shortcode( 'ef-progress-pane', 'ef_progress_pane' );
function ef_progress_bar( $atts, $content = NULL ) {
	return '<div class="ef-progress-bar">'. do_shortcode( $content ) . '</div>';
}

function ef_progress_pane( $atts = NULL, $content = NULL ) {

	if ( empty( $atts ) || empty( $content ) ) return;

	extract( $atts );

	if ( empty( $percent ) ) return;

	return '<div class="ef-progress-title">' . strip_tags( $content, '<span>' ) . '</div><div data-id="' . esc_attr( $percent ) . '"></div>';
}


/* ----- Recent posts ----- */

add_shortcode( 'ef-recent-posts', 'ef_recent_posts', 9999 );
function ef_recent_posts( $atts, $content = '' ) {

	if ( empty( $atts ) ) return;

	extract( $atts );

	$vars_array = array(
		'autoplay'   => !empty( $autoplay ) ? $autoplay : "no",
		'infinite'   => !empty( $infinite ) ? $infinite : "yes",
		'duration'  => !empty( $duration ) ? $duration : "2000",
		'fx'   => !empty( $fx ) ? $fx : 'slide',
	);

	wp_localize_script( 'shtd-carouFredSel-init', 'ef_slider_vars1', $vars_array );

	if ( empty( $in_slide ) ) return;

	global $post;
	$cats = array();
	$excludes = isset( $exclude ) ? explode( ',', $exclude ) : array();
	foreach ( $excludes as $cat ) {
		$cats[] = $cat;
	}
	$args = array(
		'post_type'				=> 'post',
		'posts_per_page'		=> isset( $number ) ? $number : '2',
		'category__not_in'		=> $cats,
		'ignore_sticky_posts'	=> 1,
	);

	$rposts = new WP_Query( $args );
	$post_count = count( $rposts->posts );

	$chunked_posts = array_chunk( $rposts->posts, $in_slide );

	if ( !$rposts->have_posts() ) return;

	$link = !empty( $link ) ? '<a title="' .  esc_attr( isset( $link_title ) ? strip_tags( $link_title ) : '' ) . '" class="ef-portfolio-lnk ef-tipsy-w hide-for-print ef-radius" href="' . esc_url( !empty( $link ) ? $link : '' ) . '"></a>' : '';

	$paging = $post_count > $in_slide ? '<div class="large-4 small-4 columns text-right hide-for-print"><div class="carousel-pagintation ef-carousel-pag text-right"></div></div>' : '';

	$Loop = '';

	foreach ( $chunked_posts as $chunked_post ) {
		$Loop .= '<div class="ef-slide">';

		foreach ( $chunked_post as $post ) {
			setup_postdata( $post );

			$pass = post_password_required() ? '<span class="ef-pass"></span>' : '';

			$thumb = has_post_thumbnail() && !empty( $image ) && $image == 'yes' ? '<span class="ef-from-blog-img ef-proj-img">' . get_the_post_thumbnail( $post->ID, 'small_thumb' ) . '<span class="ef-loader"><span></span></span>'.$pass.'</span>' : '';

			$Loop .= '<article class="clearfix"><header><div><a href="' . esc_url( get_permalink() ) . '" class="ef-latest-thumb" title="' . the_title_attribute( 'echo=0' ) . '">' . get_the_title() . '<span class="ef-date-comment text-center"><time datetime="' . esc_html( get_the_date( 'Y-m-d' ) ) . '"><span class="ef-date ef-round"><span>' . esc_html( get_the_time( 'j' ) ) . '</span><br /><span class="ef-uppercase">' . esc_html( get_the_time( 'M' ) ) . '</span></span></time><span class="ef-comments hide-for-print">' . get_comments_number() . '</span></span>' . $thumb . '</a></div></header>' . get_the_excerpt() . '</article>';
		}

		wp_reset_postdata();
		$Loop .= '</div>';
	}


	return '<section class="ef-slider-carousel ef-from-blog"><div class="row"><div class="large-8 small-8 columns"><h4 class="ef-style-title"><span>' . strip_tags( $content, '<span>' ) . '</span>' . $link . '</h4></div>' . $paging . '</div><div class="row"><div class="columns"><div class="ef-simple-carousel">' . $Loop . '</div></div></div></section>';
}


/* ---------- UI elements ---------- */

add_shortcode( "ef-ui-element", "ef_add_ui_element" );
add_shortcode( "ef-ui-tabs-nav", "ef_add_ui_tabs_nav" );
add_shortcode( "ef-ui-header", "ef_add_ui_header" );
add_shortcode( "ef-ui-pane", "ef_add_ui_pane" );
function ef_add_ui_element( $atts, $content = NULL ) {

	if ( empty( $atts ) ) return;

	extract( $atts );

	if ( empty( $type ) ) return;

	switch ( $type ) {
	case 'accordion':
		return '<div class="ef-uiaccordion ef-style-accordion">' . do_shortcode( $content ) . '</div>';
		break;
	case 'toggles':
		return '<div class="ef-style-accordion ef-toggle">' . do_shortcode( $content ) . '</div>';
		break;
	case 'faqs':
		return '<div class="ef-style-accordion ef-toggle ef-faq">' . do_shortcode( $content ) . '</div>';
		break;
	default:
		return '<div class="ef-tabs">' . do_shortcode( $content ) . '</div>';
		break;
	}
}

function ef_add_ui_tabs_nav( $atts = NULL, $content = NULL ) {
	return '<ul class="ef-tabs-nav">' . do_shortcode( $content ) . '</ul>';
}

function ef_add_ui_header( $atts, $content = NULL ) {

	if ( empty( $atts ) ) return;

	extract( $atts );

	if ( empty( $content ) || empty( $for ) ) return;

	if ( $for == 'tabs' ) {
		if ( empty( $id ) ) return;
		return '<li><a href="' . esc_url( $id ) . '" title="' . esc_attr( strip_tags( $content ) ) . '">' . strip_tags( $content, '<span>' ) . '</a></li>';
	} else {
		return '<div><span class="ef-uiaccordion-head-inner">' . strip_tags( $content, '<span>' ) . '</span></div>';
	}
}

function ef_add_ui_pane( $atts, $content = NULL ) {

	if ( empty( $atts ) || empty( $content ) ) return;

	extract( $atts );

	if ( empty( $for ) ) return;

	if ( $for == 'tabs' ) {
		if ( empty( $id ) ) return;
		return '<div id="'.esc_attr( $id ).'" class="tab">' . $content . '</div>';
	} else {
		return '<div class="show-for-print"><div class="ef-uiaccordion-content-inner">' . $content . '</div></div>';
	}
}


/* ---------- Slider shortcode ---------- */

add_shortcode( "ef-slider", "ef_add_slider" );
add_shortcode( "ef-slide", "ef_add_slides" );

global $ef_count_index;
global $ef_slider_vars;
$ef_slider_vars = array();
$ef_count_index = 1;

function ef_add_slider( $atts, $content = NULL ) {

	global $ef_count_index;

	if ( empty( $atts ) ) return;

	extract( $atts );

	$type = !empty( $type ) ? $type : 'slider';
	$title = !empty( $title ) ? $title : '';
	$autoplay = !empty( $autoplay ) ? $autoplay : 'no';
	$infinite = !empty( $infinite ) ? $infinite : 'yes';
	$duration = !empty( $duration ) ? $duration : '2000';
	$fx = !empty( $fx ) ? $fx : '2000';

	ef_get_slider_vars( $type, $autoplay, $infinite, $duration, $fx, $ef_count_index );

	if ( empty( $type ) ) return;

	$ind = $ef_count_index;

	$ef_count_index++;

	$s_title = !empty( $title ) ? '<div class="row"><div class="large-12 columns"><h4 class="ef-style-title"><span>' . strip_tags( $title, '<span>' ) . '</span></h4></div></div>' : '';

	$c_title_nav = !empty( $title ) ? '<div class="row"><div class="large-8 small-8 columns"><div class="ef-style-title"><span>' . $title . '</span></div></div><div class="large-4 small-4 columns text-right hide-for-print"><div class="carousel-pagintation-' . $ind . ' ef-carousel-pag text-right"></div></div></div>' : '<div class="row"><div class="large-12 columns text-right hide-for-print"><div class="carousel-pagintation-'.$ind.' ef-carousel-pag ef-bottom-1_25 text-right"></div></div></div>';

	switch ( $type ) {
	case 'slider':
		return $s_title . '<div class="ef-slider-carousel"><div class="ef-simple-carousel-' . $ind . '">' . do_shortcode( $content ) . '</div><div class="post-slider-direct-nav"><a class="post-slider-prev index-' . $ind . '" href="#"></a><a class="post-slider-next index-' . $ind . '" href="#"></a></div></div>';
		break;
	case 'carousel':
		return $c_title_nav . '<div class="ef-slider-carousel"><div class="ef-simple-carousel-' . $ind . '">' . do_shortcode( $content ) . '</div></div>';
		break;
	}
}

function ef_add_slides( $atts, $content = NULL ) {

	if ( empty( $content ) ) return;

	return '<div class="ef-slide">' . do_shortcode( $content ) . '</div>';
}

/* ---------- Video ---------- */

add_shortcode( "ef-video", "ef_add_video" );
function ef_add_video( $atts, $content = NULL ) {

	if ( empty( $atts ) ) return;

	extract( $atts );

	if ( $type == 'youtube' ) {

		parse_str( parse_url( $href, PHP_URL_QUERY ), $id );

		$id = $id['v'];

		return "<div class=\"flex-video\"><iframe src=\"http://www.youtube.com/embed/$id?wmode=transparent\" width=\"320\" height=\"180\" frameborder=\"0\"  wmode=\"opaque\" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe></div>";

	} elseif ( $type == 'vimeo' ) {

		sscanf( parse_url( $href, PHP_URL_PATH ), '/%d', $id );

		return "<div class=\"flex-video\"><iframe src=\"http://player.vimeo.com/video/$id\" width=\"320\" height=\"180\" frameborder=\"0\" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe></div>";
	}
}

/* --------- End --------- */

function ef_get_slider_vars( $type, $autoplay, $infinite, $duration, $fx, $ef_count_index ) {

	global $ef_slider_vars;

	$ef_slider_vars['s_type'.$ef_count_index.'']     = $type;
	$ef_slider_vars['s_autoplay'.$ef_count_index.''] = $autoplay;
	$ef_slider_vars['s_infinite'.$ef_count_index.''] = $infinite;
	$ef_slider_vars['s_duration'.$ef_count_index.''] = $duration;
	$ef_slider_vars['s_fx'.$ef_count_index.'']   	 = $fx;
	$ef_slider_vars['s_ind']						 = $ef_count_index;

	wp_localize_script( 'shtd-carouFredSel-init', 'ef_slider_vars', $ef_slider_vars );
}

/* Filter the content */
function ef_fix_shortcodes( $content ) {
	$matches = array (
		'<p>[' => '[',
		']</p>' => ']',
		']<br />' => ']',
		'<br />[' => '['
	);

	$content = strtr( $content, $matches );
	return $content;
}
add_filter( 'the_content', 'ef_fix_shortcodes' );
