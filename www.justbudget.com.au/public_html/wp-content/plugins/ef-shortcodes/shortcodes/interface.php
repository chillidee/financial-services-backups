<?php

if ( is_admin() ) {
	add_action( 'wp_ajax_get_ef_form_headings', 'get_ef_form_headings' );
	add_action( 'wp_ajax_get_ef_form_dividers', 'get_ef_form_dividers' );
	add_action( 'wp_ajax_get_ef_form_columns', 'get_ef_form_columns' );
	add_action( 'wp_ajax_get_ef_form_buttons', 'get_ef_form_buttons' );
	add_action( 'wp_ajax_get_ef_form_sbuttons', 'get_ef_form_sbuttons' );
	add_action( 'wp_ajax_get_ef_form_panels', 'get_ef_form_panels' );
	add_action( 'wp_ajax_get_ef_form_alerts', 'get_ef_form_alerts' );
	add_action( 'wp_ajax_get_ef_form_progressbars', 'get_ef_form_progressbars' );
	add_action( 'wp_ajax_get_ef_form_recentposts', 'get_ef_form_recentposts' );
	add_action( 'wp_ajax_get_ef_form_elements', 'get_ef_form_elements' );
	add_action( 'wp_ajax_get_ef_form_carousel', 'get_ef_form_carousel' );
	add_action( 'wp_ajax_get_ef_form_video', 'get_ef_form_video' );
	add_action( 'wp_ajax_get_html_last_part', 'get_html_last_part' );
}

function get_html_last_part() { ?>
				<tr>
					<td><input class="button-secondary" type="button" id="cancel" name="cancel" value="<?php _e( 'Cancel', EF_SHTD ) ?>" onClick="tinyMCEPopup.close();" /></td>

					<td><input class="button button-primary" type="submit" id="insert" name="insert" value="<?php _e( 'Insert shortcode', EF_SHTD ) ?>" /></td>
				</tr>
			</table>
		</form>

		<span class="ef-loader"><span></span></span>

		<script type="text/javascript" src="<?php echo get_option( 'siteurl' ) . '/wp-includes/js/jquery/jquery.js' ?>"></script>
		<script type="text/javascript" src="<?php echo SHTD_JS_URL . 'jquery/jquery.validate.min.js' ?>"></script>
		<script type="text/javascript" src="<?php echo get_option( 'siteurl' ) . '/wp-includes/js/tinymce/tiny_mce_popup.js' ?>"></script>
		<script type="text/javascript" src="<?php echo SHTD_JS_URL . 'loader.js' ?>"></script>
	<?php
}

/* Headings shortcode front */
function get_ef_form_headings() { ?>
    <title><?php _e( 'Heading shortcode', EF_SHTD ) ?></title>
		<link rel="stylesheet" href="<?php echo get_option( 'siteurl' ) . '/wp-includes/css/buttons.min.css' ?>" type="text/css" rel="stylesheet" />
		<form id="myshortcode-form">
			<table class="form-table wp-core-ui">

				<tr>
					<th valign="top" align="left"><label><?php _e( 'Text', EF_SHTD ) ?></label></th>
					<td>
						<input type="text" id="titlefield" name="titlefield" value="This is heading" />
						<br />
						<em><?php _e( 'Enter your text.', EF_SHTD ) ?></em>
					</td>
				</tr>

				<tr>
					<th valign="top" align="left"><label><?php _e( 'Class', EF_SHTD ) ?></label></th>
					<td>
						<select id="head-class" name="head-class">
							<option data-id=""><?php _e( 'Primary', EF_SHTD ) ?></option>
							<option data-id="ef-secondary"><?php _e( 'Secondary', EF_SHTD ) ?></option>
						</select>
						<br />
						<em><?php _e( 'Choose a class.', EF_SHTD ) ?></em>
					</td>
				</tr>

				<tr>
					<th valign="top" align="left"><label><?php _e( 'Type', EF_SHTD ) ?></label></th>
					<td>
						<select id="head-type" name="head-type">
							<option data-id=""><?php _e( 'Header', EF_SHTD ) ?></option>
							<option data-id="subheader"><?php _e( 'Subheader', EF_SHTD ) ?></option>
						</select>
						<br />
						<em><?php _e( 'Choose a type.', EF_SHTD ) ?></em>
					</td>
				</tr>

				<tr>
					<th valign="top" align="left"><label><?php _e( 'Size', EF_SHTD ) ?></label></th>
					<td>
						<select id="head-size" name="head-size">
							<option data-id="h1"><?php _e( 'H1', EF_SHTD ) ?></option>
							<option data-id="h2"><?php _e( 'H2', EF_SHTD ) ?></option>
							<option data-id="h3"><?php _e( 'H3', EF_SHTD ) ?></option>
							<option data-id="h4"><?php _e( 'H4', EF_SHTD ) ?></option>
							<option data-id="h5"><?php _e( 'H5', EF_SHTD ) ?></option>
							<option data-id="h6"><?php _e( 'H6', EF_SHTD ) ?></option>
						</select>
						<br />
						<em><?php _e( 'Choose a size.', EF_SHTD ) ?></em>
					</td>
				</tr>

    <?php
	echo get_html_last_part(); ?>
	    <script type="text/javascript" src="<?php echo SHTD_JS_URL . 'insert_headings.js' ?>"></script>
	<?php die;
}

/* Divider shortcode front */
function get_ef_form_dividers() { ?>
    <title><?php _e( 'Divider shortcode', EF_SHTD ) ?></title>
		<link rel="stylesheet" href="<?php echo get_option( 'siteurl' ) . '/wp-includes/css/buttons.min.css' ?>" type="text/css" rel="stylesheet" />
		<form id="myshortcode-form">
			<table class="form-table wp-core-ui">

				<tr>
					<th valign="top" align="left"><label><?php _e( 'Type', EF_SHTD ) ?></label></th>
					<td>
						<select id="hr-type" name="hr-type">
							<option value=""><?php _e( 'Default', EF_SHTD ) ?></option>
							<option value="ef-blank"><?php _e( 'Blank divider', EF_SHTD ) ?></option>
						</select>
						<br />
						<em><?php _e( 'Choose a type.', EF_SHTD ) ?></em>
					</td>
				</tr>

				<tr>
					<th valign="top" align="left"><label><?php _e( 'Class', EF_SHTD ) ?></label></th>
					<td>
						<select id="hr-class" name="hr-class">
							<option value=""><?php _e( 'Default', EF_SHTD ) ?></option>
							<option value="ef-bottom-1_25"><?php _e( '1.25em', EF_SHTD ) ?></option>
							<option value="ef-bottom-1_5"><?php _e( '1.5em', EF_SHTD ) ?></option>
							<option value="ef-bottom-2_18"><?php _e( '2.18em', EF_SHTD ) ?></option>
							<option value="ef-bottom-2_8"><?php _e( '2.8em', EF_SHTD ) ?></option>
						</select>
						<br />
						<em><?php _e( 'Choose a bottom indent class.', EF_SHTD ) ?></em>
					</td>
				</tr>

    <?php
	echo get_html_last_part(); ?>
	    <script type="text/javascript" src="<?php echo SHTD_JS_URL . 'insert_dividers.js' ?>"></script>
	<?php die;
}

/* Columns shortcode front */
function get_ef_form_columns() { ?>
    <title><?php _e( 'Columns shortcode', EF_SHTD ) ?></title>
		<link rel="stylesheet" href="<?php echo get_option( 'siteurl' ) . '/wp-includes/css/buttons.min.css' ?>" type="text/css" rel="stylesheet" />
		<form id="myshortcode-form">
			<table class="form-table wp-core-ui">

				<tr>
					<th valign="top" align="left"><label><?php _e( 'Columns', EF_SHTD ) ?></label></th>
					<td>
						<select id="cols" name="cols">
							<option value="6"><?php _e( '[1/2] + [1/2]', EF_SHTD ) ?></option>
							<option value="39"><?php _e( '[1/4] + [3/4]', EF_SHTD ) ?></option>
							<option value="93"><?php _e( '[3/4] + [1/4]', EF_SHTD ) ?></option>
							<option value="4"><?php _e( '[1/3] + [1/3] + [1/3]', EF_SHTD ) ?></option>
							<option value="336"><?php _e( '[1/4] + [1/4] + [1/2]', EF_SHTD ) ?></option>
							<option value="633"><?php _e( '[1/2] + [1/4] + [1/4]', EF_SHTD ) ?></option>
							<option value="363"><?php _e( '[1/4] + [1/2] + [1/4]', EF_SHTD ) ?></option>
							<option value="3"><?php _e( '[1/4] + [1/4] + [1/4] + [1/4]', EF_SHTD ) ?></option>
							<option value="2"><?php _e( '[1/6] + [1/6] + [1/6] + [1/6] + [1/6] + [1/6]', EF_SHTD ) ?></option>
						</select>
						<br />
						<em><?php _e( 'Choose a grid type.', EF_SHTD ) ?></em>
					</td>
				</tr>

    <?php
	echo get_html_last_part(); ?>
	    <script type="text/javascript" src="<?php echo SHTD_JS_URL . 'insert_columns.js' ?>"></script>
	<?php die;
}

/* Buttons shortcode front */
function get_ef_form_buttons() { ?>
    <title><?php _e( 'Button shortcode', EF_SHTD ) ?></title>
		<link rel="stylesheet" href="<?php echo get_option( 'siteurl' ) . '/wp-includes/css/buttons.min.css' ?>" type="text/css" rel="stylesheet" />
		<form id="myshortcode-form">
			<table class="form-table wp-core-ui">
				<tr>
					<th valign="top" align="left"><label><?php _e( 'Url', EF_SHTD ) ?></label></th>
					<td><input type="text" id="urlfield" name="urlfield" value="http://" />
						<br />
						<em><?php _e( 'Specify a URL.', EF_SHTD ) ?></em>
					</td>
				</tr>

				<tr>
					<th valign="top" align="left"><label><?php _e( 'Text', EF_SHTD ) ?></label></th>
					<td>
						<input type="text" id="titlefield" name="titlefield" value="Button text" />
						<br />
						<em><?php _e( 'Enter a text.', EF_SHTD ) ?></em>
					</td>
				</tr>

				<tr>
					<th valign="top" align="left"><label><?php _e( 'Size', EF_SHTD ) ?></label></th>
					<td>
						<select id="button-size" name="button-select">
							<option data-id="tiny"><?php _e( 'Tiny', EF_SHTD ) ?></option>
							<option data-id="small"><?php _e( 'Small', EF_SHTD ) ?></option>
							<option data-id="large"><?php _e( 'Large', EF_SHTD ) ?></option>
						</select>
						<br />
						<em><?php _e( 'Choose a size of the button.', EF_SHTD ) ?></em>
					</td>
				</tr>

				<tr>
					<th valign="top" align="left"><label><?php _e( 'Style', EF_SHTD ) ?></label></th>
					<td>
						<select id="button-type" name="button-select">
							<option data-id="square"><?php _e( 'Square', EF_SHTD ) ?></option>
							<option data-id="radius"><?php _e( 'Radius', EF_SHTD ) ?></option>
							<option data-id="ef-hollow radius"><?php _e( 'Hollow', EF_SHTD ) ?></option>
							<option data-id="round"><?php _e( 'Round', EF_SHTD ) ?></option>
						</select>
						<br />
						<em><?php _e( 'Choose a type of the button.', EF_SHTD ) ?></em>
					</td>
				</tr>

				<tr>
					<th valign="top" align="left"><label><?php _e( 'Target', EF_SHTD ) ?></label></th>
					<td>
						<input type="text" id="targetfield" name="target" value="_blank" />
						<br />
						<em><?php _e( 'Specify target or leave this field blank if you want to open page in the same window.', EF_SHTD ) ?></em>
					</td>
				</tr>
    <?php
	echo get_html_last_part(); ?>
	    <script type="text/javascript" src="<?php echo SHTD_JS_URL . 'insert_buttons.js' ?>"></script>
	<?php die;
}


/* Buttons shortcode front */
function get_ef_form_sbuttons() { ?>

	<?php $social_icons = array(
		'ef_twt'  => 'Twitter',
		'ef_fb'   => 'Facebook',
		'ef_in'   => 'LinkedIn',
		'ef_pin'  => 'Pinterest',
		'ef_drb'  => 'Dribbble',
		'ef_tumb'  => 'Tumblr',
		'ef_flick'  => 'Flickr',
		'ef_vim'  => 'Vimeo',
		'ef_delic'  => 'Delicious',
		'ef_goog'  => 'Google Plus',
		'ef_forr'  => 'Forrst',
		'ef_hi5'  => 'Hi5!',
		'ef_last'   => 'Last.fm',
		'ef_space'  => 'Myspace',
		'ef_newsv'  => 'Newsvine',
		'ef_pica'  => 'Picasa',
		'ef_tech'  => 'Technorati',
		'ef_rss'  => 'RSS',
		'ef_rdio'  => 'Rdio',
		'ef_share'  => 'ShareThis',
		'ef_skyp'  => 'Skype',
		'ef_slid'  => 'SlideShare',
		'ef_squid'  => 'Squidoo',
		'ef_stum'  => 'StumbleUpon',
		'ef_what'  => 'WhatsApp',
		'ef_wp'   => 'Wordpress',
		'ef_ytb'  => 'Youtube',
		'ef_digg'  => 'Digg',
		'ef_beh'  => 'Behance',
		'ef_yah'  => 'Yahoo',
		'ef_blogg'  => 'Blogger',
		'ef_hype'  => 'Hype Machine',
		'ef_groove'  => 'Grooveshark',
		'ef_sound'  => 'SoundCloud',
		'ef_insta'  => 'Instagram',
		'ef_vk'   => 'Vkontakte',
		'ef_mail'  => "Say 'Hello!'",
		'ef_phone'  => 'Make a call'
	); ?>

    <title><?php _e( 'Social buttons shortcode', EF_SHTD ) ?></title>
		<link rel="stylesheet" href="<?php echo get_option( 'siteurl' ) . '/wp-includes/css/buttons.min.css' ?>" type="text/css" rel="stylesheet" />
		<form id="myshortcode-form">
			<table class="form-table wp-core-ui">

				<?php foreach ( $social_icons as $class => $title ) { ?>
					<tr>
						<th valign="top" align="left"><label data-id="<?php echo esc_attr( $title ); ?>"><?php echo $title; ?></label></th>
						<td>
							<input type="text" class="soc-icn" data-id="<?php echo $class; ?>" value="" />
						</td>
					</tr>
				<?php } ?>

				<tr>
					<th valign="top" align="left"><label><?php _e( 'Target', EF_SHTD ) ?></label></th>
					<td>
						<input type="text" id="targetfield" name="target" value="_blank" />
						<br />
						<em><?php _e( 'Specify target or leave this field blank if you want to open page in the same window.', EF_SHTD ) ?></em>
					</td>
				</tr>
    <?php
	echo get_html_last_part(); ?>
	    <script type="text/javascript" src="<?php echo SHTD_JS_URL . 'insert_sbuttons.js' ?>"></script>
	<?php die;
}

/* Panels shortcode front */
function get_ef_form_panels() { ?>
    <title><?php _e( 'Panel shortcode', EF_SHTD ) ?></title>
		<link rel="stylesheet" href="<?php echo get_option( 'siteurl' ) . '/wp-includes/css/buttons.min.css' ?>" type="text/css" rel="stylesheet" />
		<form id="myshortcode-form">
			<table class="form-table wp-core-ui">

				<tr>
					<th valign="top" align="left"><label><?php _e( 'Text', EF_SHTD ) ?></label></th>
					<td>
						<textarea id="titlefield" rows="3" cols="57" name="titlefield">&#60;h3&#62;This is Panel&#60;/h3&#62;&#60;p&#62;Any content goes here&#60;/p&#62;</textarea>
						<br />
						<em><?php _e( 'Enter any text, paste html or shortcodes here.', EF_SHTD ) ?></em>
					</td>
				</tr>

				<tr>
					<th valign="top" align="left"><label><?php _e( 'Type', EF_SHTD ) ?></label></th>
					<td>
						<select id="panel-type" name="select">
							<option data-id=""><?php _e( 'Regular', EF_SHTD ) ?></option>
							<option data-id="callout"><?php _e( 'Callout', EF_SHTD ) ?></option>
						</select>
						<br />
						<em><?php _e( 'Choose a style of the panel.', EF_SHTD ) ?></em>
					</td>
				</tr>

				<tr>
					<th valign="top" align="left"><label><?php _e( 'Style', EF_SHTD ) ?></label></th>
					<td>
						<select id="panel-style" name="select">
							<option data-id="square"><?php _e( 'Square', EF_SHTD ) ?></option>
							<option data-id="radius"><?php _e( 'Radius', EF_SHTD ) ?></option>
							<option data-id="round"><?php _e( 'Round', EF_SHTD ) ?></option>
						</select>
						<br />
						<em><?php _e( 'Choose a type of the panel.', EF_SHTD ) ?></em>
					</td>
				</tr>

    <?php
	echo get_html_last_part(); ?>
	    <script type="text/javascript" src="<?php echo SHTD_JS_URL . 'insert_panels.js' ?>"></script>
	<?php die;
}

/* Alerts shortcode front */
function get_ef_form_alerts() { ?>
    <title><?php _e( 'Alert shortcode', EF_SHTD ) ?></title>
		<link rel="stylesheet" href="<?php echo get_option( 'siteurl' ) . '/wp-includes/css/buttons.min.css' ?>" type="text/css" rel="stylesheet" />
		<form id="myshortcode-form">
			<table class="form-table wp-core-ui">

				<tr>
					<th valign="top" align="left"><label><?php _e( 'Text', EF_SHTD ) ?></label></th>
					<td>
						<input id="titlefield" name="titlefield" value="This is Alert" />
						<br />
						<em><?php _e( 'Enter text.', EF_SHTD ) ?></em>
					</td>
				</tr>

				<tr>
					<th valign="top" align="left"><label><?php _e( 'Type', EF_SHTD ) ?></label></th>
					<td>
						<select id="panel-type" name="select">
							<option data-id=""><?php _e( 'Regular', EF_SHTD ) ?></option>
							<option data-id="alert"><?php _e( 'Alert', EF_SHTD ) ?></option>
							<option data-id="success"><?php _e( 'Success', EF_SHTD ) ?></option>
						</select>
						<br />
						<em><?php _e( 'Choose a type of the panel.', EF_SHTD ) ?></em>
					</td>
				</tr>

				<tr>
					<th valign="top" align="left"><label><?php _e( 'Style', EF_SHTD ) ?></label></th>
					<td>
						<select id="panel-style" name="select">
							<option data-id="square"><?php _e( 'Square', EF_SHTD ) ?></option>
							<option data-id="radius"><?php _e( 'Radius', EF_SHTD ) ?></option>
							<option data-id="round"><?php _e( 'Round', EF_SHTD ) ?></option>
						</select>
						<br />
						<em><?php _e( 'Choose a style of the panel.', EF_SHTD ) ?></em>
					</td>
				</tr>

				<tr>
					<th valign="top" align="left"><label><?php _e( '"Close" button', EF_SHTD ) ?></label></th>
					<td>
						<input type="checkbox" checked id="checkfield" name="checkfield" value="" />
						<br />
						<em><?php _e( 'Select this checkbox to display "close" button.', EF_SHTD ) ?></em>
					</td>
				</tr>

    <?php
	echo get_html_last_part(); ?>
	    <script type="text/javascript" src="<?php echo SHTD_JS_URL . 'insert_alerts.js' ?>"></script>
	<?php die;
}

/* Progressbars shortcode front */
function get_ef_form_progressbars() { ?>
    <title><?php _e( 'Progressbars shortcode', EF_SHTD ) ?></title>

    	<?php $siteUrl = get_option( 'siteurl' ); ?>

		<link rel="stylesheet" href="<?php echo $siteUrl . '/wp-includes/css/buttons.min.css' ?>" type="text/css" rel="stylesheet" />
		<form id="myshortcode-form">
			<table class="form-table wp-core-ui">

				<tr>
					<th valign="top" align="left"><label><?php _e( 'Fill all fields. Add new fields to create some progressbars.', EF_SHTD ) ?></label></th>
					<td>
						<table class="grouped">

							<tr>
								<th valign="top" align="left"><label><?php _e( 'Skill name', EF_SHTD ) ?></label></th>
								<td>
									<input class="titlefield" type="text" name="titlefield" value="Skill name" />
									<br />
									<em><?php _e( 'Enter a skill name.', EF_SHTD ) ?></em>
								</td>
							</tr>

							<tr>
								<th valign="top" align="left"><label><?php _e( 'Percents', EF_SHTD ) ?></label></th>
								<td>
									<select class="percents" name="percents">
										<?php for ( $i = 1; $i <= 100; $i++ ) { ?>
											<option><?php echo $i; ?></option>
										<?php } ?>
									</select>
									<br />
									<em><?php _e( 'Choose a value in percents.', EF_SHTD ) ?></em>
								</td>
							</tr>

						</table>
					</td>
				<tr>
					<th valign="top" align="left"><label></label></th>
					<td>
						<input class="button-secondary" id="add-fields" type="button" value="<?php _e( 'Add fileds &#43;', EF_SHTD ) ?>" name="add">
					</td>
				</tr>

    <?php
	echo get_html_last_part(); ?>
	    <script type="text/javascript" src="<?php echo SHTD_JS_URL . 'insert_progressbars.js' ?>"></script>
	<?php die;
}

/* Recent Posts shortcode front */
function get_ef_form_recentposts() { ?>
    <title><?php _e( 'Recent posts shortcode', EF_SHTD ) ?></title>
		<link rel="stylesheet" href="<?php echo get_option( 'siteurl' ) . '/wp-includes/css/buttons.min.css' ?>" type="text/css" rel="stylesheet" />
		<form id="myshortcode-form">
			<table class="form-table wp-core-ui">
				<tr>
					<th valign="top" align="left"><label><?php _e( 'Title', EF_SHTD ) ?></label></th>
					<td>
						<input type="text" id="titlefield" name="titlefield" value="Latest From The Blog" />
						<br />
						<em><?php _e( "Enter a shortcode's heading.", EF_SHTD )?></em>
					</td>
				</tr>

				<tr>
					<th valign="top" align="left"><label><?php _e( 'Featured image', EF_SHTD ) ?></label></th>
					<td>
						<input type="checkbox" checked id="checkfield" name="checkfield" value="" />
						<br />
						<em><?php _e( 'Select this checkbox to display featured images.', EF_SHTD ) ?></em>
					</td>
				</tr>

				<tr>
					<th valign="top" align="left"><label><?php _e( 'Number of posts', EF_SHTD ) ?></label></th>
					<td>
						<input type="text" id="numberfield" name="numberfield" value="4" />
						<br />
						<em><?php _e( 'Specify a number of posts you want to display.', EF_SHTD ) ?></em>
					</td>
				</tr>

				<tr>
					<th valign="top" align="left"><label><?php _e( 'Number of posts per slide', EF_SHTD ) ?></label></th>
					<td>
						<input type="text" id="numberslidefield" name="numberslidefield" value="2" />
						<br />
						<em><?php _e( 'Specify a number of posts per one slide of the carousel.', EF_SHTD ) ?></em>
					</td>
				</tr>

				<?php $pages = array(); ?>
				<?php $pages = get_pages( array(
					'meta_key' => '_wp_page_template',
					'meta_value' => 'templates/blog-template.php'
				) ); ?>

				<tr>
					<th valign="top" align="left"><label><?php _e( '"Go to Blog" link', EF_SHTD ) ?></label></th>
					<td>
						<select class="ef-select-lnk" name="blog-lnk-select">
							<option value="" selected><?php _e( '- Hide link -', EF_SHTD ) ?></option>
							<?php foreach ( $pages as $page ) { ?>
								<?php echo '<option value="'.get_permalink( $page->ID ).'">'.$page->post_title.'</option>'; ?>
							<?php } ?>
						</select>
						<br />
						<em><?php _e( 'Choose a blog page.', EF_SHTD ) ?></em>
					</td>
				</tr>

				<tr>
					<th valign="top" align="left"><label><?php _e( 'Permalink title', EF_SHTD ) ?></label></th>
					<td>
						<input type="text" id="permtitle" name="permtitle" value="Go to Blog" />
						<br />
						<em><?php _e( 'Specify a title (will be displayed in Tooltip).', EF_SHTD ) ?></em>
					</td>
				</tr>

				<tr>
					<th valign="top" align="left"><label><?php _e( 'Exclude categories', EF_SHTD )?></label></th>
					<td>
						<select class="ef-select-cats" name="button-select" multiple="multiple">
							<option selected><?php _e( 'Show all categories', EF_SHTD ) ?></option>
							<?php $categories = get_categories(); ?>
								<?php foreach ( $categories as $category ) { ?>
									<?php echo '<option data-id="'.$category->cat_ID.'">'.$category->name.'</option>'; ?>
							<?php } ?>
						</select>
						<br />
						<em><?php _e( 'Choose categories to exclude posts or leave this box as default to display posts from all categories.', EF_SHTD ) ?></em>
					</td>
				</tr>

				<tr>
					<th valign="top" align="left"><label><?php _e( 'Autoplay', EF_SHTD ) ?></label></th>
					<td>
						<input type="checkbox" id="autoplay" name="autoplay" value="" />
						<br />
						<em><?php _e( 'Select this checkbox for auto rotation.', EF_SHTD ) ?></em>
					</td>
				</tr>

				<tr>
					<th valign="top" align="left"><label><?php _e( 'Infinite', EF_SHTD ) ?></label></th>
					<td>
						<input type="checkbox" checked id="infinite" name="infinite" value="" />
						<br />
						<em><?php _e( 'Select this checkbox to apply "infinite" scrolling.', EF_SHTD ) ?></em>
					</td>
				</tr>

				<tr>
					<th valign="top" align="left"><label><?php _e( 'Duration', EF_SHTD ) ?></label></th>
					<td>
						<input type="text" id="duration" name="duration" value="2000" />
						<br />
						<em><?php _e( 'Slide display timing in milliseconds. Applies if autoplay was activated.', EF_SHTD ) ?></em>
					</td>
				</tr>

				<tr>
					<th valign="top" align="left"><label><?php _e( 'Transition', EF_SHTD ) ?></label></th>
					<td>
						<select id="trans-select" name="trans-select">
							<option value="slide"><?php _e( 'Slide', EF_SHTD ) ?></option>
							<option value="crossfade"><?php _e( 'Crossfade', EF_SHTD ) ?></option>
							<option value="cover"><?php _e( 'Cover', EF_SHTD ) ?></option>
							<option value="cover-fade"><?php _e( 'Cover-fade', EF_SHTD ) ?></option>
							<option value="uncover"><?php _e( 'Uncover', EF_SHTD ) ?></option>
							<option value="uncover-fade"><?php _e( 'Uncover-fade', EF_SHTD ) ?></option>
						</select>
						<br />
						<em><?php _e( 'Choose a slider transition.', EF_SHTD ) ?></em>
					</td>
				</tr>

    <?php
	echo get_html_last_part(); ?>
	    <script type="text/javascript" src="<?php echo SHTD_JS_URL . 'insert_recent_posts.js' ?>"></script>
    <?php die;
}


/* UI Elements front */
function get_ef_form_elements() { ?>
    <title><?php _e( 'UI Elements shortcode', EF_SHTD ) ?></title>
    <link rel="stylesheet" href="<?php echo get_option( 'siteurl' ) . '/wp-includes/css/buttons.min.css' ?>" type="text/css" rel="stylesheet" />
		<form id="myshortcode-form">
			<table class="form-table wp-core-ui">

				<tr>
					<th valign="top" align="left"><label><?php _e( 'UI element', EF_SHTD ) ?></label></th>
					<td>
						<select name="elem-select" id="elem-select">
							<option value="accordion"><?php _e( 'Accordion', EF_SHTD ) ?></option>
							<option value="toggles"><?php _e( 'Toggle boxes', EF_SHTD ) ?></option>
							<option value="faqs"><?php _e( 'Toggle boxes (FAQs)', EF_SHTD ) ?></option>
							<option value="tabs"><?php _e( 'Tabs', EF_SHTD ) ?></option>
						</select>
						<br />
						<em><?php _e( 'Choose an UI element.', EF_SHTD ) ?></em>
					</td>
				</tr>

				<tr>
					<th></th>
					<td>
						<table class="grouped">

							<tr>
								<th valign="top" align="left"><label><?php _e( 'Header', EF_SHTD ) ?></label></th>
								<td>
									<input type="text" class="ui-header" name="head" value="Header 1" />
									<br />
									<em><?php _e( 'Enter a header text.', EF_SHTD ) ?></em>
								</td>
							</tr>

							<tr>
								<th valign="top" align="left"><label><?php _e( 'Content', EF_SHTD ) ?></label></th>
								<td>
									<textarea class="pane-content" rows="3" cols="57" name="pane">&#60;p&#62;Any content goes here&#60;p&#62;</textarea>
									<br />
									<em><?php _e( 'Paste your content in the box above.', EF_SHTD ) ?></em>
								</td>
							</tr>

						</table>
					</td>
				</tr>

				<tr>
					<th></th>
					<td>
						<input class="button-secondary" id="add-fields" type="button" value="<?php _e( 'Add fileds &#43;', EF_SHTD ) ?>" name="add">
					</td>
				</tr>


    <?php
	echo get_html_last_part(); ?>
	    <script type="text/javascript" src="<?php echo SHTD_JS_URL . 'insert_elems.js' ?>"></script>
    <?php die;
}


/* Carousel front */
function get_ef_form_carousel() { ?>
    <title><?php _e( 'Slider shortcode', EF_SHTD ) ?></title>
    <link rel="stylesheet" href="<?php echo get_option( 'siteurl' ) . '/wp-includes/css/buttons.min.css' ?>" type="text/css" rel="stylesheet" />
		<form id="myshortcode-form">
			<table class="form-table wp-core-ui">
				<tr>
					<th valign="top" align="left"><label><?php _e( 'Title', EF_SHTD ) ?></label></th>
					<td>
						<input type="text" id="titlefield" name="titlefield" value="Any title goes here" />
						<br />
						<em><?php _e( "Enter a shortcode's heading.", EF_SHTD )?></em>
					</td>
				</tr>

				<tr>
					<th valign="top" align="left"><label><?php _e( 'Slider type', EF_SHTD ) ?></label></th>
					<td>
						<select name="slidertype" id="slidertype">
							<option data-url="<?php echo SHTD_ADM_URL; ?>" value="slider"><?php _e( 'Slider', EF_SHTD ) ?></option>
							<option value="carousel"><?php _e( 'Carousel', EF_SHTD ) ?></option>
						</select>
						<br />
						<em><?php _e( 'Choose a slider type.', EF_SHTD ) ?></em>
					</td>
				</tr>

				<tr>
					<th valign="top" align="left"><label><?php _e( 'Slides number', EF_SHTD ) ?></label></th>
					<td>
						<select name="numberslidefield" id="numberslidefield">
							<?php for ( $i = 2; $i <= 10 ; $i++ ) { ?>
								<option value="<?php echo $i;?>"><?php echo $i;?></option>
							<?php } ?>
						</select>
						<br />
						<em><?php _e( 'Choose a number of slides.', EF_SHTD ) ?></em>
					</td>
				</tr>

				<tr>
					<th valign="top" align="left"><label><?php _e( 'Autoplay', EF_SHTD ) ?></label></th>
					<td>
						<input type="checkbox" id="autoplay" name="autoplay" value="" />
						<br />
						<em><?php _e( 'Select this checkbox for auto rotation.', EF_SHTD ) ?></em>
					</td>
				</tr>

				<tr>
					<th valign="top" align="left"><label><?php _e( 'Infinite', EF_SHTD ) ?></label></th>
					<td>
						<input type="checkbox" checked id="infinite" name="infinite" value="" />
						<br />
						<em><?php _e( 'Select this checkbox to apply "infinite" scrolling.', EF_SHTD ) ?></em>
					</td>
				</tr>

				<tr>
					<th valign="top" align="left"><label><?php _e( 'Duration', EF_SHTD ) ?></label></th>
					<td>
						<input type="text" id="duration" name="duration" value="2000" />
						<br />
						<em><?php _e( 'Slide display timing in milliseconds. Applies if autoplay was activated.', EF_SHTD ) ?></em>
					</td>
				</tr>

				<tr>
					<th valign="top" align="left"><label><?php _e( 'Transition', EF_SHTD ) ?></label></th>
					<td>
						<select id="trans-select" name="trans-select">
							<option value="slide"><?php _e( 'Slide', EF_SHTD ) ?></option>
							<option value="crossfade"><?php _e( 'Crossfade', EF_SHTD ) ?></option>
							<option value="cover"><?php _e( 'Cover', EF_SHTD ) ?></option>
							<option value="cover-fade"><?php _e( 'Cover-fade', EF_SHTD ) ?></option>
							<option value="uncover"><?php _e( 'Uncover', EF_SHTD ) ?></option>
							<option value="uncover-fade"><?php _e( 'Uncover-fade', EF_SHTD ) ?></option>
						</select>
						<br />
						<em><?php _e( 'Choose a slider transition.', EF_SHTD ) ?></em>
					</td>
				</tr>


    <?php
	echo get_html_last_part(); ?>
	    <script type="text/javascript" src="<?php echo SHTD_JS_URL . 'insert_slider.js' ?>"></script>
    <?php die;
}


/* Youtube front */
function get_ef_form_video() { ?>
    <title><?php _e( 'Video shortcode', EF_SHTD ) ?></title>
		<link rel="stylesheet" href="<?php echo get_option( 'siteurl' ) . '/wp-includes/css/buttons.min.css' ?>" type="text/css" rel="stylesheet" />
		<form id="myshortcode-form">
			<table class="form-table wp-core-ui">

				<tr>
					<th valign="top" align="left"><label><?php _e( 'Video-sharing website', EF_SHTD ) ?></label></th>
					<td>
						<select name="video-select">
							<option value="youtube"><?php _e( 'Youtube', EF_SHTD ) ?></option>
							<option value="vimeo"><?php _e( 'Vimeo', EF_SHTD ) ?></option>
						</select>
						<br />
						<em><?php _e( 'Choose a video-sharing website.', EF_SHTD ) ?></em>
					</td>
				</tr>

				<tr>
					<th valign="top" align="left"><label><?php _e( 'Video url', EF_SHTD ) ?></label></th>
					<td>
						<input type="text" id="urlfield" name="urlfield" value="http://" />
						<br />
						<em><?php _e( "Specify direct link to video (copy from browser's address line).", EF_SHTD ) ?></em>
					</td>
				</tr>
    <?php
	echo get_html_last_part(); ?>
	    <script type="text/javascript" src="<?php echo SHTD_JS_URL . 'insert_video.js' ?>"></script>
    <?php die;
}