(function() {

    tinymce.create('tinymce.plugin.ef_shortcodes', {
        init: function(ed, url) {

            /* Headings ----------------------------------------------------------*/

            ed.addCommand('headings', function() {

                ed.windowManager.open({
                    file: ajaxurl + '/interface.php?action=get_ef_form_headings',
                    width: 800 + parseInt(ed.getLang('advanced.image_delta_width', 0), null),
                    height: 500 + parseInt(ed.getLang('advanced.image_delta_height', 0), null),
                    inline: 1,
                    popup_css: url + "/css/interface.css"
                }, {
                    plugin_url: url
                });
            });

            ed.addButton('headings', {
                title: 'Heading shortcode',
                cmd: 'headings',
                image: url + '/img/headings.png'
            });

            /* Dividers ----------------------------------------------------------*/

            ed.addCommand('dividers', function() {

                ed.windowManager.open({
                    file: ajaxurl + '/interface.php?action=get_ef_form_dividers',
                    width: 800 + parseInt(ed.getLang('advanced.image_delta_width', 0), null),
                    height: 500 + parseInt(ed.getLang('advanced.image_delta_height', 0), null),
                    inline: 1,
                    popup_css: url + "/css/interface.css"
                }, {
                    plugin_url: url
                });
            });

            ed.addButton('dividers', {
                title: 'Divider shortcode',
                cmd: 'dividers',
                image: url + '/img/dividers.png'
            });

            /* Columns ----------------------------------------------------------*/

            ed.addCommand('columns', function() {

                ed.windowManager.open({
                    file: ajaxurl + '/interface.php?action=get_ef_form_columns',
                    width: 800 + parseInt(ed.getLang('advanced.image_delta_width', 0), null),
                    height: 500 + parseInt(ed.getLang('advanced.image_delta_height', 0), null),
                    inline: 1,
                    popup_css: url + "/css/interface.css"
                }, {
                    plugin_url: url
                });
            });

            ed.addButton('columns', {
                title: 'Columns shortcode',
                cmd: 'columns',
                image: url + '/img/columns.png'
            });

            /* Buttons ----------------------------------------------------------*/

            ed.addCommand('buttons', function() {

                ed.windowManager.open({
                    file: ajaxurl + '/interface.php?action=get_ef_form_buttons',
                    width: 800 + parseInt(ed.getLang('advanced.image_delta_width', 0), null),
                    height: 500 + parseInt(ed.getLang('advanced.image_delta_height', 0), null),
                    inline: 1,
                    popup_css: url + "/css/interface.css"
                }, {
                    plugin_url: url
                });
            });

            ed.addButton('buttons', {
                title: 'Button shortcode',
                cmd: 'buttons',
                image: url + '/img/buttons.png'
            });

            /* Social buttons ----------------------------------------------------------*/

            ed.addCommand('soc-buttons', function() {

                ed.windowManager.open({
                    file: ajaxurl + '/interface.php?action=get_ef_form_sbuttons',
                    width: 800 + parseInt(ed.getLang('advanced.image_delta_width', 0), null),
                    height: 500 + parseInt(ed.getLang('advanced.image_delta_height', 0), null),
                    inline: 1,
                    popup_css: url + "/css/interface.css"
                }, {
                    plugin_url: url
                });
            });

            ed.addButton('soc-buttons', {
                title: 'Social buttons shortcode',
                cmd: 'soc-buttons',
                image: url + '/img/sbuttons.png'
            });

            /* Panels ----------------------------------------------------------*/

            ed.addCommand('panels', function() {

                ed.windowManager.open({
                    file: ajaxurl + '/interface.php?action=get_ef_form_panels',
                    width: 800 + parseInt(ed.getLang('advanced.image_delta_width', 0), null),
                    height: 500 + parseInt(ed.getLang('advanced.image_delta_height', 0), null),
                    inline: 1,
                    popup_css: url + "/css/interface.css"
                }, {
                    plugin_url: url
                });
            });

            ed.addButton('panels', {
                title: 'Panel shortcode',
                cmd: 'panels',
                image: url + '/img/panels.png'
            });

            /* Alerts ----------------------------------------------------------*/

            ed.addCommand('alerts', function() {

                ed.windowManager.open({
                    file: ajaxurl + '/interface.php?action=get_ef_form_alerts',
                    width: 800 + parseInt(ed.getLang('advanced.image_delta_width', 0), null),
                    height: 500 + parseInt(ed.getLang('advanced.image_delta_height', 0), null),
                    inline: 1,
                    popup_css: url + "/css/interface.css"
                }, {
                    plugin_url: url
                });
            });

            ed.addButton('alerts', {
                title: 'Alert shortcode',
                cmd: 'alerts',
                image: url + '/img/alerts.png'
            });

            /* Progressbars ----------------------------------------------------------*/

            ed.addCommand('progressbars', function() {

                ed.windowManager.open({
                    file: ajaxurl + '/interface.php?action=get_ef_form_progressbars',
                    width: 800 + parseInt(ed.getLang('advanced.image_delta_width', 0), null),
                    height: 500 + parseInt(ed.getLang('advanced.image_delta_height', 0), null),
                    inline: 1,
                    popup_css: url + "/css/interface.css"
                }, {
                    plugin_url: url
                });
            });

            ed.addButton('progressbars', {
                title: 'Progressbars shortcode',
                cmd: 'progressbars',
                image: url + '/img/progress.png'
            });

            /* Recent posts -----------------------------------------------------*/

            ed.addCommand('showrecent', function() {

                ed.windowManager.open({
                    file: ajaxurl + '/interface.php?action=get_ef_form_recentposts',
                    width: 800 + parseInt(ed.getLang('advanced.image_delta_width', 0), null),
                    height: 400 + parseInt(ed.getLang('advanced.image_delta_height', 0), null),
                    inline: 1,
                    popup_css: url + "/css/interface.css"
                }, {
                    plugin_url: url
                });
            });

            ed.addButton('showrecent', {
                title: 'Recent posts shortcode',
                cmd: 'showrecent',
                image: url + '/img/recent.png'
            });

            /* UI Elements -----------------------------------------------------*/

            ed.addCommand('ui-elements', function() {

                ed.windowManager.open({
                    file: ajaxurl + '/interface.php?action=get_ef_form_elements',
                    width: 800 + parseInt(ed.getLang('advanced.image_delta_width', 0), null),
                    height: 400 + parseInt(ed.getLang('advanced.image_delta_height', 0), null),
                    inline: 1,
                    popup_css: url + "/css/interface.css"
                }, {
                    plugin_url: url
                });
            });

            ed.addButton('ui-elements', {
                title: 'UI Elements shortcode',
                cmd: 'ui-elements',
                image: url + '/img/elems.png'
            });

            /* Sliders -----------------------------------------------------*/

            ed.addCommand('carousel', function() {

                ed.windowManager.open({
                    file: ajaxurl + '/interface.php?action=get_ef_form_carousel',
                    width: 800 + parseInt(ed.getLang('advanced.image_delta_width', 0), null),
                    height: 400 + parseInt(ed.getLang('advanced.image_delta_height', 0), null),
                    inline: 1,
                    popup_css: url + "/css/interface.css"
                }, {
                    plugin_url: url
                });
            });

            ed.addButton('carousel', {
                title: 'Slider shortcode',
                cmd: 'carousel',
                image: url + '/img/sliders.png'
            });

            /* Video --------------------------------------------------------------*/

            ed.addCommand('video', function() {

                ed.windowManager.open({
                    file: ajaxurl + '/interface.php?action=get_ef_form_video',
                    width: 800 + parseInt(ed.getLang('advanced.image_delta_width', 0), null),
                    height: 400 + parseInt(ed.getLang('advanced.image_delta_height', 0), null),
                    inline: 1,
                    popup_css: url + "/css/interface.css"
                }, {
                    plugin_url: url
                });
            });

            ed.addButton('video', {
                title: 'Video shortcode',
                cmd: 'video',
                image: url + '/img/video.png'
            });

            /*----------------------------------------------------------------------*/

        },

        createControl: function(n, cm) {
            return null;
        },

        getInfo: function() {
            return {
                longname: 'Fireform Shortcodes',
                author: 'fireform',
                authorurl: 'http://themeforest.net/user/fireform',
                infourl: 'http://themeforest.net/user/fireform',
                version: "1.0"
            };
        }

    });

    // Register plugin
    tinymce.PluginManager.add('ef_shortcodes', tinymce.plugin.ef_shortcodes);
})();