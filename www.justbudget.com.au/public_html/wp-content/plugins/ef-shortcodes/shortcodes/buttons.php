<?php

add_action( 'init', 'ef_shortcode_button' );
function ef_shortcode_button() {
	if ( ( current_user_can('edit_posts') || current_user_can('edit_pages') ) && get_user_option('rich_editing') ) {
		add_filter("mce_external_plugins", "ef_shortcode_add_button");
	    add_filter('mce_buttons_3', 'ef_shortcode_register_button');
	}
}

function ef_shortcode_add_button($plugin_args) {
	$plugin_args['ef_shortcodes'] = SHTD_ADM_URL . 'buttons.js';
	return $plugin_args;
}

function ef_shortcode_register_button($buttons) {
	array_push( $buttons, 'headings', 'dividers', 'columns', 'buttons', 'soc-buttons', 'panels', 'alerts', 'progressbars', 'showrecent', 'ui-elements', 'carousel', 'video' );
	return $buttons;
}