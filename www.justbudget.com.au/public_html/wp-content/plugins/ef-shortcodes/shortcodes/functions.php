<?php

/**
 * Required scripts/css
 *
 * @since 1.0.0
 */
function ef_shtd_enqueue_style() {
	wp_enqueue_style( 'jQueryUI', SHTD_JS_URL . 'jquery/jQueryUI/jquery-ui-1.10.2.custom.min.css', array(), '', 'screen' );
	wp_enqueue_style( 'shtd-style', SHTD_ADM_URL . 'css/style.css', array(), '', 'screen' );

    if ( !isset( $GLOBALS['wp_styles']->registered[ 'ef-foundation' ] ) ) {
    	wp_enqueue_style( 'ef-foundation', SHTD_ADM_URL . 'css/foundation.min.css', array(), '', 'screen' );
    }
}
add_action('wp_print_styles', 'ef_shtd_enqueue_style');

function ef_shtd_enqueue_script() {

	global $wp_scripts;

	if ( !isset( $wp_scripts->registered[ 'jquery' ] ) ) {
		wp_enqueue_script( 'jquery' );
	}

	if ( !isset( $wp_scripts->registered[ 'ef-foundation' ] ) ) {
		wp_enqueue_script( 'ef-foundation', SHTD_JS_URL . 'jquery/foundation.min.js',array('jquery'),'',true );
	}
	
	if ( !isset( $wp_scripts->registered[ 'carouFredSel' ] ) ) {
		wp_enqueue_script( 'carouFredSel', SHTD_JS_URL . 'jquery/carouFredSel/jquery.carouFredSel-6.2.1-packed.js',array('jquery'),'',true );
	}

	wp_enqueue_script( 'shtd-carouFredSel-init', SHTD_JS_URL . 'jquery/carouFredSel/carouFredSel.init.js',array('jquery', 'carouFredSel'),'',true );
	wp_enqueue_script( 'jquery-ui-tabs' );
	wp_enqueue_script( 'jquery-ui-accordion' );
    
    if ( !class_exists( 'RevSlider' ) || !isset( $GLOBALS['wp_scripts']->registered[ 'ef-easing' ] ) ) {
		wp_register_script( 'ef-easing', SHTD_JS_URL . 'jquery/jquery.easing.1.3.min.js',array('jquery'),'',true );
		wp_enqueue_script( 'ef-easing' );
    }
	
	wp_enqueue_script( 'shtd-custom', SHTD_JS_URL . 'jquery/init.js','','',true );
}
add_action( 'wp_enqueue_scripts', 'ef_shtd_enqueue_script' );