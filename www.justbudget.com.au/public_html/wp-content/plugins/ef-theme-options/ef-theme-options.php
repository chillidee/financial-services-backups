<?php
/*
Plugin Name: Fireform Theme Options
Plugin URI: http://themeforest.net/users/fireform
Description: This is a required plugin for "Favea" theme. This plugin helps to adjust Favea theme by providing necessary options.
Author: Evgeny Fireform
Author URI: http://themeforest.net/users/fireform
Version: 1.0
License: Regular License
License URI: http://themeforest.net/licenses
*/

/*  Copyright 2013 Evgeny Fireform. Feel free to contact me via my profile page contact form.

    The Regular License allows use of the item in one single end product which end users are not charged to access or use. You can do this directly or, if you're a freelancer, you can create the end product for one client to distribute free to its end users. You can charge your client to produce the single end product. Distribution of source files is not permitted.    

	Important information !!!

	"Fireform Theme Options" plugin is based on "Slightly Modded Options Framework" by Sy4mil. Direct link to SMOF documentation: http://aquagraphite.com/2011/09/slightly-modded-options-framework/
	Do not use "Fireform Theme Options" plugin in your themes (to Themeforest authors). Create your own mods using source code from Sy4mil. Get more information about copyrights and using at Aquagraphite.com.
*/

defined( 'ABSPATH' ) || exit;

add_theme_support( 'ef-theme-options' );

/**
 * Definitions
 *
 * @since 1.0
 */
$theme_version = '';
$efto_output = '';

if( function_exists( 'wp_get_theme' ) ) {	
	$temp_obj = wp_get_theme();
	$theme_obj = wp_get_theme( $temp_obj->get('Template') );
	$theme_version = $theme_obj->get('Version');
	$theme_name = $theme_obj->get('Name');
	$theme_uri = $theme_obj->get('ThemeURI');
	$author_uri = $theme_obj->get('AuthorURI');
} else {
	$theme_data = get_theme_data( get_template_directory().'/style.css' );
	$theme_version = $theme_data['Version'];
	$theme_name = $theme_data['Name'];
	$theme_uri = $theme_data['ThemeURI'];
	$author_uri = $theme_data['AuthorURI'];
}


define( 'efto_VERSION', '1.0' );

if( !defined('efto_ADMIN_PATH') ) {
	define( 'efto_ADMIN_PATH', plugin_dir_url( __FILE__ ) );
	define( 'efto_ASSETS_DIR_URL', trailingslashit( efto_ADMIN_PATH . 'assets' ) );
	define( 'efto_IMAGES_DIR_URL', trailingslashit( efto_ASSETS_DIR_URL . 'images' ) );
	define( 'efto_CSS_DIR_URL', trailingslashit( efto_ASSETS_DIR_URL . 'css' ) );
	define( 'efto_JS_DIR_URL', trailingslashit( efto_ASSETS_DIR_URL . 'js' ) );
	define( 'efto_ADM_DIR_URL', trailingslashit( efto_ADMIN_PATH . 'admin' ) );
}

if( !defined('efto_ADMIN_DIR') ) {
	define( 'efto_ADMIN_DIR', plugin_dir_path( __FILE__ ) );
	define( 'efto_CLASSES_DIR', trailingslashit( efto_ADMIN_DIR . 'classes' ) );
	define( 'efto_FUNCTIONS_DIR', trailingslashit( efto_ADMIN_DIR . 'functions' ) );
	define( 'efto_FRONTEND_DIR', trailingslashit( efto_ADMIN_DIR . 'front-end' ) );
	define( 'efto_ADM_DIR', trailingslashit( efto_ADMIN_DIR . 'admin' ) );
}

if ( !defined('efto_BASENAME') ) {
	define( 'efto_BASENAME', plugin_basename(__FILE__) );	
}

define( 'efto_THEMENAME', $theme_name );
define( 'efto_THEMEVERSION', $theme_version );
define( 'efto_THEMEURI', $theme_uri );
define( 'efto_THEMEAUTHORURI', $author_uri );

define( 'efto_BACKUPS','backups' );


/**
 * Textdomain
 *
 * @uses load_plugin_textdomain()
 *
 * @since 1.0
 */

define( 'EF_TO', 'ef-theme-options' );

load_plugin_textdomain( EF_TO, false, basename(dirname(__FILE__)) . '/lang' );


/**
 * Required action filters
 *
 * @uses add_action()
 *
 * @since 1.0
 */

add_action('admin_head', 'optionsframework_admin_message');
add_action('admin_init','optionsframework_admin_init');
add_action('admin_menu', 'optionsframework_add_admin');


/**
 * Required Files
 *
 * @since 1.0
 */
 
require_once efto_FUNCTIONS_DIR . 'functions.filters.php';
require_once efto_FUNCTIONS_DIR . 'functions.interface.php';
require_once efto_FUNCTIONS_DIR . 'functions.options.php';
require_once efto_FUNCTIONS_DIR . 'functions.admin.php';
require_once efto_FUNCTIONS_DIR . 'functions.fonts.php';
require_once efto_ADM_DIR . 'breadcrumbs.php';

if ( is_admin() ) {
	require_once efto_CLASSES_DIR . 'class.options_machine.php';
}

require_once efto_ADM_DIR . 'functions.php';


/**
 * AJAX Saving Options
 *
 * @since 1.0
 */
add_action('wp_ajax_of_ajax_post_action', 'of_ajax_callback');