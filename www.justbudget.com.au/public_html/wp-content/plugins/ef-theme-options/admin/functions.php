<?php

/**
 * Enqueue scripts/stylesheets
 *
 * @since 1.0.0
 */
function ef_efto_enqueue_style() {
	wp_enqueue_style( 'tipsy', efto_ADM_DIR_URL . 'css/colorful-tipsy/tipsy.min.css', array(), '', 'screen' );
}
add_action('wp_print_styles', 'ef_efto_enqueue_style');

function ef_efto_enqueue_script() {
	
	wp_enqueue_script( 'tipsy', efto_ADM_DIR_URL . 'js/colorful-tipsy/jquery.tipsy.custom.min.js','','',true );	
	wp_enqueue_script( 'efto-custom', efto_ADM_DIR_URL . 'js/init.js','','',true );

	global $efto_data;
	$totop = isset( $efto_data ) && !empty( $efto_data['ef_show_totop'] ) ? $efto_data['ef_show_totop'] : '0';

	wp_localize_script( 'efto-custom', 'ef_totop', $totop );
}
add_action( 'wp_enqueue_scripts', 'ef_efto_enqueue_script' );


/**
 * Filters/hooks
 *
 * @since 1.0.0
 */

// Plugin Settings Link
add_filter( 'plugin_action_links', 'of_plugin_action_links', 10, 2 );
function of_plugin_action_links( $links, $file ) {
	static $this_plugin;

	if ( !$this_plugin ) {
		$this_plugin = efto_BASENAME;
	}

	if ( $file == $this_plugin ) {
		$settings_link = '<a href="' . get_bloginfo('url') . '/wp-admin/themes.php?page=optionsframework">Settings</a>';
		array_unshift($links, $settings_link);
	}

	return $links;
}

 
// Reset Posts page option (Front page displays screen)
if ( get_option( 'page_for_posts' ) != 0 ) {
	update_option( 'page_for_posts', 0 );
}


// Remove "Settings > Media" item from Wordpress menu
add_action( 'admin_menu', 'ef_remove_options_media' );
function ef_remove_options_media() {
	remove_submenu_page( 'options-general.php', 'options-media.php' );
}