var ef = jQuery;
ef.noConflict();

/*-------------------------------------------------------*/
/*------------------- Main Variables --------------------*/
/*-------------------------------------------------------*/

/* Tipsy (tooltips) in the Social Bar on top */
var $tipsySocial = ef('a.ef-tipsy-n');
var $tipsy_w = ef('a.ef-tipsy-w');

/*-------------------------------------------------------*/
/*-------------------------------------------------------*/
/*-------------------------------------------------------*/

ef(document).ready(function() {

	/*Tipsy for the Social profiles on top*/
	$tipsySocial.tipsy({
		fade: true,
		opacity: 1,
		gravity: 'n',
		delayIn: 0,
		delayOut: 0,
		offset: -4
	});

	$tipsy_w.tipsy({
		fade: true,
		opacity: 1,
		gravity: 'w',
		delayIn: 0,
		delayOut: 0,
		offset: 10
	});

	/* To top script*/

	if (typeof(ef_totop) !== 'undefined' && parseInt(ef_totop, null) !== 0) {
		ef(function() {
			ef('body').append('<div id="to-top"></div>');
			ef(window).scroll(function() {
				if (ef(this).scrollTop() > 200) {
					ef('#to-top').fadeIn();
				} else {
					ef('#to-top').fadeOut();
				}
			});

			ef('#to-top').click(function() {
				ef('body,html').animate({
					scrollTop: 0
				}, 1200);
				return false;
			});
		});
	}
});