<?php

/**
 * Breadcrumbs navigation.
 *
 * @since Favea 1.0
 */
function ef_put_breadcrumbs() {

	global $efto_data;

	$text['home']     = __( 'Home', EF_TO );
	$text['category'] = __( 'Category Archives: "%s"', EF_TO );
	$text['search']   = __( 'Search Results for: "%s"', EF_TO );
	$text['tag']      = __( 'Tag Archives: "%s"', EF_TO );
	$text['author']   = __( 'Author Archives: "%s"', EF_TO );
	$text['404']      = __( 'Error 404', EF_TO );

	$show_title = isset( $efto_data['ef_title_sw'] ) ? $efto_data['ef_title_sw'] : 0;

	if ( isset( $efto_data['ef_sep'] ) ) {
		if ( "sep_4" == $efto_data['ef_sep'] ) $delimiter = "&#124;";
		elseif ( "sep_2" == $efto_data['ef_sep'] ) $delimiter = "&raquo;";
		elseif ( "sep_3" == $efto_data['ef_sep'] ) $delimiter = "&#47;";
		elseif ( "sep_1" == $efto_data['ef_sep'] ) $delimiter = "&#8594;";
	} else {
		$delimiter = "&#8594;";
	}

	$delimiter = '<li class="ef-delimiter">'.$delimiter.'</li>';

	$before         = '<li class="current">'; // current menu item
	$after          = '</li>';

	global $post;
	$home_link    = home_url( '/' );
	$link_before  = '<li>';
	$link_after   = '</li>';
	$link_attr    = ' rel="v:url" property="v:title"';
	$link         = $link_before . '<a' . $link_attr . ' href="%1$s">%2$s</a>' . $link_after;
	$parent_id    = $parent_id_2 = $post ? $post->post_parent : '';
	$frontpage_id = get_option( 'page_on_front' );

	if ( !is_home() && !is_front_page() && !is_page_template( 'home-template.php' ) ) {

		echo '<ul class="breadcrumbs">';

		echo sprintf( $link, $home_link, $text['home'] );
		if ( $frontpage_id == 0 || $parent_id != $frontpage_id ) echo $delimiter;


		if ( is_category() || ( current_theme_supports( 'ef-custom-post-types' ) && is_tax( EF_CPT_TAX_CAT ) ) ) {

			if ( is_tax() ) {
				foreach ( ( get_the_category() ) as $cat ) {
					$this_cat = $cat->cat_name;
				}
			} else {
				$this_cat = get_category( get_query_var( 'cat' ), false );
			}

			if ( !empty( $this_cat ) && $this_cat->parent != 0 ) {
				$cats = get_category_parents( $this_cat->parent, TRUE, $delimiter );
				$cats = str_replace( '<a', $link_before . '<a' . $link_attr, $cats );
				$cats = str_replace( '</a>', '</a>' . $link_after, $cats );
				if ( $show_title == 0 ) $cats = preg_replace( '/ title="(.*?)"/', '', $cats );
				echo $cats;
			}
			echo $before . sprintf( $text['category'], single_cat_title( '', false ) ) . $after;

		} elseif ( is_tag() || ( current_theme_supports( 'ef-custom-post-types' ) && is_tax( EF_CPT_TAX_TAG ) ) ) {
			echo $before . sprintf( $text['tag'], single_tag_title( '', false ) ) . $after;

		} elseif ( is_search() ) {
			echo $before . sprintf( $text['search'], get_search_query() ) . $after;

		} elseif ( is_day() ) {
			echo sprintf( $link, get_year_link( get_the_time( 'Y' ) ), get_the_time( 'Y' ) ) . $delimiter;
			echo sprintf( $link, get_month_link( get_the_time( 'Y' ), get_the_time( 'm' ) ), get_the_time( 'F' ) ) . $delimiter;
			echo $before . get_the_time( 'd' ) . $after;

		} elseif ( is_month() ) {
			echo sprintf( $link, get_year_link( get_the_time( 'Y' ) ), get_the_time( 'Y' ) ) . $delimiter;
			echo $before . get_the_time( 'F' ) . $after;

		} elseif ( is_year() ) {
			echo $before . get_the_time( 'Y' ) . $after;

		} elseif ( is_single() && !is_attachment() ) {

			$post_type = get_post_type();

			if ( current_theme_supports( 'ef-custom-post-types' ) && ( $post_type == EF_CPT || $post_type == EF_CPT_EXTR || $post_type == EF_CPT_TEAM ) ) {
				if ( $post_type == EF_CPT ) {
					$post_type = get_post_type_object( get_post_type() );
					printf( $link, get_post_type_archive_link( EF_CPT ), $post_type->labels->singular_name );
					echo $delimiter . $before . get_the_title() . $after;
				} else {
					echo $before . get_the_title() . $after;
				}
			} else {
				$cat = get_the_category(); $cat = $cat[0];
				$cats = get_category_parents( $cat, TRUE, $delimiter );
				$cats = str_replace( '<a', $link_before . '<a' . $link_attr, $cats );
				$cats = str_replace( '</a>', '</a>' . $link_after, $cats );
				if ( $show_title == 0 ) $cats = preg_replace( '/ title="(.*?)"/', '', $cats );
				echo $cats;
				echo $before . get_the_title() . $after;
			}

		} elseif ( !is_single() && !is_page() && get_post_type() != 'post' && !is_404() ) {
			$post_type = get_post_type_object( get_post_type() );
			echo $before . $post_type->labels->singular_name . $after;

		} elseif ( is_attachment() ) {

			$parent = get_post( $parent_id );
			if ( $parent->post_type == 'post' ) {
				$cat = get_the_category( $parent->ID ); $cat = $cat[0];
				$cats = get_category_parents( $cat, TRUE, $delimiter );
				$cats = str_replace( '<a', $link_before . '<a' . $link_attr, $cats );
				$cats = str_replace( '</a>', '</a>' . $link_after, $cats );
				if ( $show_title == 0 ) {
					$cats = preg_replace( '/ title="(.*?)"/', '', $cats );
				}
				echo $cats;
				printf( $link, get_permalink( $parent ), $parent->post_title );
				echo $delimiter;
			} elseif ( current_theme_supports( 'ef-custom-post-types' ) ) {
				printf( $link, get_permalink( $parent ), $parent->post_title );
				echo $delimiter;
			}

			echo $before . get_the_title() . $after;

		} elseif ( is_page() && !$parent_id ) {
			echo $before . get_the_title() . $after;

		} elseif ( is_page() && $parent_id ) {
			if ( $parent_id != $frontpage_id ) {
				$breadcrumbs = array();
				while ( $parent_id ) {
					$page = get_page( $parent_id );
					if ( $parent_id != $frontpage_id ) {
						$breadcrumbs[] = sprintf( $link, get_permalink( $page->ID ), get_the_title( $page->ID ) );
					}
					$parent_id = $page->post_parent;
				}
				$breadcrumbs = array_reverse( $breadcrumbs );
				for ( $i = 0; $i < count( $breadcrumbs ); $i++ ) {
					echo $breadcrumbs[$i];
					if ( $i != count( $breadcrumbs ) -1 ) {
						echo $delimiter;
					}
				}
			}

			if ( $parent_id_2 != 0 && $parent_id_2 != $frontpage_id ) echo $delimiter;
			echo $before . get_the_title() . $after;

		} elseif ( is_author() ) {
			global $author;
			$userdata = get_userdata( $author );
			echo $before . sprintf( $text['author'], $userdata->display_name ) . $after;

		} elseif ( is_404() ) {
			echo $before . $text['404'] . $after;
		}

		if ( get_query_var( 'paged' ) ) {
			echo $delimiter . $before . __( 'page', EF_TO ) . get_query_var( 'paged' ). $after;
		}

		echo '</ul>';

	}
}