<?php

/*
 * Fonts
 */
function ef_theme_font_family( $font ) {
	$case = '';

	switch ( $font ) {

	case 'arial':
		$case .= 'Arial, sans-serif';
		break;
	case 'verdana':
		$case .= 'Verdana, "Verdana Ref", sans-serif';
		break;
	case 'trebuchet':
		$case .= '"Trebuchet MS", Verdana, "Verdana Ref", sans-serif';
		break;
	case 'georgia':
		$case .= 'Georgia, serif';
		break;
	case 'times':
		$case .= 'Times, "Times New Roman", serif';
		break;
	case 'tahoma':
		$case .= 'Tahoma,Geneva,Verdana,sans-serif';
		break;
	case 'palatino':
		$case .= '"Palatino Linotype", Palatino, Palladio, "URW Palladio L", "Book Antiqua", Baskerville, "Bookman Old Style", "Bitstream Charter", "Nimbus Roman No9 L", Garamond, "Apple Garamond", "ITC Garamond Narrow", "New Century Schoolbook", "Century Schoolbook", "Century Schoolbook L", Georgia, serif';
		break;
	case 'helvetica':
		$case .= '"Helvetica Neue", Helvetica, Arial, sans-serif';
		break;
	case 'Open Sans':
		$case .= '"Open Sans", sans-serif';
		break;
	case 'Arvo':
		$case .= '"Arvo", serif';
		break;
	case 'Fauna One':
		$case .= '"Fauna One", serif';
		break;
	case 'Lato':
		$case .= '"Lato", sans-serif';
		break;
	case 'Vollkorn':
		$case .= '"Vollkorn", serif';
		break;
	case 'Ubuntu':
		$case .= '"Ubuntu", sans-serif';
		break;
	case 'PT Sans':
		$case .= '"PT Sans", sans-serif';
		break;
	case 'Droid Sans':
		$case .= '"Droid Sans", sans-serif';
		break;
	case 'Prociono':
		$case .= '"Prociono", serif';
		break;
	case 'Muli':
		$case .= '"Muli", sans-serif';
		break;
	case 'Exo':
		$case .= '"Exo", sans-serif';
		break;
	case 'Yanone Kaffeesatz':
		$case .= '"Yanone Kaffeesatz", sans-serif';
		break;
	case 'Armata':
		$case .= '"Armata", sans-serif';
		break;
	case 'Cabin':
		$case .= '"Cabin", sans-serif';
		break;
	case 'Nobile':
		$case .= '"Nobile", sans-serif';
		break;
	case 'Goudy Bookletter 1911':
		$case .= '"Goudy Bookletter 1911", serif';
		break;
	case 'Merriweather':
		$case .= '"Merriweather", serif';
		break;
	}
	return $case;
}

function ef_theme_font_options( $gfont_opt ) {
	$case = '';

	switch ( $gfont_opt ) {

	case 'Open Sans':
		$case .= ':400,300,300italic,400italic,600,600italic,700,700italic,800,800italic&subset=latin,cyrillic-ext,cyrillic,latin-ext,greek,greek-ext,vietnamese';
		break;
	case 'Arvo':
		$case .= ':400,400italic,700,700italic';
		break;
	case 'Fauna One':
		$case .= '&subset=latin,latin-ext';
		break;
	case 'Lato':
		$case .= ':400,300,300italic,400italic,700,700italic,900,900italic';
		break;
	case 'Vollkorn':
		$case .= ':400,400italic,700,700italic';
		break;
	case 'Ubuntu':
		$case .= ':400,300,300italic,400italic,500italic,500,700,700italic&subset=latin,cyrillic-ext,greek-ext,greek,latin-ext,cyrillic';
		break;
	case 'PT Sans':
		$case .= ':400,400italic,700,700italic&subset=latin,cyrillic-ext,latin-ext,cyrillic';
		break;
	case 'Droid Sans':
		$case .= ':400,700';
		break;
	case 'Prociono':
		$case .= '';
		break;
	case 'Muli':
		$case .= ':400,300,300italic,400italic';
		break;
	case 'Exo':
		$case .= ':400,200,200italic,300,300italic,400italic,500,500italic,600,600italic,700,700italic,800,800italic&subset=latin,latin-ext';
		break;
	case 'Yanone Kaffeesatz':
		$case .= ':400,300,200,700&subset=latin,latin-ext';
		break;
	case 'Armata':
		$case .= '&subset=latin,latin-ext';
		break;
	case 'Cabin':
		$case .= ':400,400italic,500,500italic,600,600italic,700,700italic';
		break;
	case 'Nobile':
		$case .= ':400,400italic,700,700italic';
		break;
	case 'Goudy Bookletter 1911':
		$case .= '';
		break;
	case 'Merriweather':
		$case .= ':400,300,300italic,400italic,700,700italic,900,900italic&subset=latin,latin-ext';
		break;
	}
	return $case;
}

function ef_theme_font_style( $font_styles ) {
	$font_style = '';
	switch ( $font_styles ) {
	case 'normal':
		$font_style = 'font-weight: normal;';
		break;
	case 'bold':
		$font_style = 'font-weight: bold;';
		break;
	case 'italic':
		$font_style = 'font-weight: normal; font-style: italic;';
		break;
	case 'bold italic':
		$font_style = 'font-weight: bold; font-style: italic;';
		break;
	}
	return $font_style;
}