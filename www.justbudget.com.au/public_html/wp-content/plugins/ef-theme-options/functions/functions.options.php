<?php

add_action('wp_loaded','of_options');

if (!function_exists('of_options'))
{
	function of_options()
	{
	
		$prefix = "ef_";
		
		
		$of_sidebars = array();
		foreach ( $GLOBALS['wp_registered_sidebars'] as $sidebar ) {
			if ( is_active_sidebar($sidebar['id']) && !($sidebar['id'] == 'footer-widget-area' || $sidebar['id'] == 'primary-widget-area' ) ) :
				$of_sidebars[$sidebar['id']] = $sidebar['name'];				
			endif;
		}
		$sidebars_tmp = array_unshift( $of_sidebars, __("Default widget area", EF_TO) );
		
		
		$of_options_homepage_blocks = array
		( 
			"disabled" => array (
				"placebo" 	 => "placebo", //REQUIRED!
			), 
			"enabled" => array (
				"placebo" 	 => "placebo", //REQUIRED!
				"ef_extras"	 => "Extras",
				"ef_latest"	 => "Recent Works",
				"ef_content" => "Page Content",
				'before_title'	=> '<div>',  
		                'after_title'	=> '</div>',
			),
		);
		
		// Slider Revolution chooser
		$of_sliders = array();		
		$arrSliders = array();
		if ( class_exists( 'RevSlider' ) ) {		
			$slider = new RevSlider();
			$arrSliders = $slider->getArrSliders();			
			foreach($arrSliders as $slider) {
				$of_sliders[ $slider->getAlias() ] = $slider->getTitle();
			}
		}
		
		if ( !class_exists( 'RevSlider' ) ) {
			$of_sliders_tmp = array_unshift( $of_sliders, __( "Revolution Slider is not installed", EF_TO ) );
		} elseif ( count( $of_sliders ) == 0 ) {
			$of_sliders_tmp = array_unshift( $of_sliders, __( "No Sliders available", EF_TO ) );
		} else {
			$of_sliders_tmp = array_unshift( $of_sliders, __( "Choose a Slider:", EF_TO ) );
		}


		//Background Images Reader
		$bg_images_path = get_template_directory() . '/images/bg/';
		$bg_images_url = get_template_directory_uri().'/images/bg/';
		$bg_images = array('none' => get_template_directory_uri() . '/images/none.png');
		
		if ( is_dir($bg_images_path) ) {
		    if ($bg_images_dir = opendir($bg_images_path) ) { 
		        while ( ($bg_images_file = readdir($bg_images_dir)) !== false ) {
					$img_formats = array(".png", ".jpg", ".gif");
					foreach($img_formats as $format) {
						if(stristr($bg_images_file, $format) !== false) {
							$bg_images[] = $bg_images_url . $bg_images_file;
						}
					}
		        }    
		    }
		}


/*-----------------------------------------------------------------------------------*/
/* The Options Array */
/*-----------------------------------------------------------------------------------*/

// Set the Options Array
global $of_options;
$of_options = array();

$sidebar_dsc = __( 'Select a widget area. Widget areas that have widgets are available only. If it isn\'t specified then "Default sidebar widget area" will be assigned.', EF_TO );
$footer_dsc = __( 'Select a widget area. Widget areas that have widgets are available only. If this option isn\'t specified then "Default footer widget area" will be assigned.', EF_TO );
$layout_dsc = __( "Select content and sidebar alignment.", EF_TO );

/*-- General settings --*/

$of_options[] = array( 	"name" 		=> __( "General", EF_TO ),
						"type" 		=> "heading"
				);
	
	$of_options[] = array( 	"name" 		=> __( "Header", EF_TO ),
							"id" 		=> "{$prefix}divider",
							"type"		=> "divider"
					);
		

		$of_options[] = array( 	"name" 		=> __( "Logo", EF_TO ),
								"desc" 		=> __( "Default logo is 123px x 28px. Upload any png/jpg/gif image that should be your website's logo.", EF_TO ),
								"id" 		=> "{$prefix}custom_logo",
								"std"		=> efto_IMAGES_DIR_URL . 'logo.png',
								"mod"		=> "min",
								"type"		=> "media"
					);

		$of_options[] = array( 	"name" 		=> __( "Logo (vector)", EF_TO ),
								"desc" 		=> __( "Upload svg image (retina-ready and resolution-independed scallable vector graphic format) that should be applied instead your raster logo image, uploaded above. Works in modern browsers that support SVG.", EF_TO ),
								"id" 		=> "{$prefix}custom_logo_svg",
								"std"		=> efto_IMAGES_DIR_URL . 'logo.svg',
								"mod"		=> "min",
								"type"		=> "media"
					);

		$of_options[] = array( 	"name" 		=> __( "Logo adjustment", EF_TO ),
								"desc" 		=> __( 'Enter a less "padding-top" value if your logo is too large. Do not forget to specify "px" dimension.', EF_TO ),
								"id" 		=> "{$prefix}logo_padd",
								"std" 		=> "28px",
								"type" 		=> "text"
						);
		
		$of_options[] = array( 	"name" 		=> __( "Favicon", EF_TO ),
								"desc" 		=> __( "Upload a 16px x 16px ico/png/gif image that will represent your website's favicon or paste URL in the field.", EF_TO ),
								"id" 		=> "{$prefix}custom_favicon.",
								"std" 		=> efto_IMAGES_DIR_URL . 'favicon.ico',
								"type" 		=> "upload"
					);
	
						
		$of_options[] = array( 	"name" 		=> __( "Tagline", EF_TO ),
								"desc" 		=> __( 'Show tagline under the logo (if it was filled in the "Dashboard > Settings" > "General").', EF_TO ),
								"id" 		=> "{$prefix}tagline",
								"std" 		=> 0,
								"on" 		=> __( "Enable", EF_TO ),
								"off" 		=> __( "Disable", EF_TO ),
								"type" 		=> "switch"
						);
						
		$of_options[] = array( 	"name"		=> __( "Show/hide search", EF_TO ),
								"desc" 		=> __( 'Show or hide search in the header.', EF_TO ),
								"id" 		=> "{$prefix}show_search",								
								"on" 		=> __( "Show", EF_TO ),
								"off" 		=> __( "Hide", EF_TO ),
								"type" 		=> "switch",
								"std" 		=> "0",
							);
		
		$of_options[] = array( 	"name" 		=> __( "Global page description", EF_TO ),
								"desc" 		=> __( 'General page description. Applies after the name of a page. Remove it if you want to hide it.', EF_TO ),
								"id" 		=> "{$prefix}page_dsc",
								"std" 		=> "A description of the entire page",
								"type" 		=> "text"
						);
	
	$of_options[] = array( 	"name" 		=> __( "Profiles/Social Network Links", EF_TO ),
							"id" 		=> "{$prefix}divider",
							"desc" 		=> __( "Enter URLs of your social profile pages. While field is blank the icon is not displayed.", EF_TO ),
							"type"		=> "divider"
					);		
		
		$of_social_icons = array(
			'twt'		=> 'Twitter',
			'fb'		=> 'Facebook',
			'in'		=> 'LinkedIn',
			'pin'		=> 'Pinterest',
			'drb'		=> 'Dribbble',
			'tumb'		=> 'Tumblr',
			'flick'		=> 'Flickr',
			'vim'		=> 'Vimeo',
			'delic'		=> 'Delicious',
			'goog'		=> 'Google Plus',
			'forr'		=> 'Forrst',
			'hi5'		=> 'Hi5!',
			'last' 		=> 'Last.fm',
			'space'		=> 'Myspace',
			'newsv'		=> 'Newsvine',
			'pica'		=> 'Picasa',
			'tech'		=> 'Technorati',
			'rss'		=> 'RSS',
			'rdio'		=> 'Rdio',
			'share'		=> 'ShareThis',
			'skyp'		=> 'Skype',
			'slid'		=> 'SlideShare',
			'squid'		=> 'Squidoo',
			'stum'		=> 'StumbleUpon',
			'what'		=> 'WhatsApp',
			'wp'		=> 'Wordpress',
			'ytb'		=> 'Youtube',
			'digg'		=> 'Digg',
			'beh'		=> 'Behance',
			'yah'		=> 'Yahoo',
			'blogg'		=> 'Blogger',
			'hype'		=> 'Hype Machine',
			'groove'	=> 'Grooveshark',
			'sound'		=> 'SoundCloud',
			'insta'		=> 'Instagram',
			'vk'		=> 'Vkontakte',
			'mail'		=> __( 'Say "Hello!"', EF_TO ),
			'phone'		=> __( 'Make a call', EF_TO ),
		);
		
		$i = 0;
		foreach( $of_social_icons as $icon_id => $social ) {
			$i++;
			$std = $i <= 7 ? '#' : '';
			$of_options[] = array(
				'name' => '',
				'desc' => $social,
				'id' => $prefix . $icon_id,
				'std' => $std,
				'type' => 'text'
			);
			
		}
						
	$of_options[] = array( 	"name" 		=> __( "Additional Settings", EF_TO ),
							"id" 		=> "{$prefix}divider",
							"type"		=> "divider"
					);

		$of_options[] = array( 	"name"		=> __( 'Show/hide "To top"', EF_TO ),
								"desc" 		=> __( 'Show or hide "To top" tab.', EF_TO ),
								"id" 		=> "{$prefix}show_totop",								
								"on" 		=> __( "Show", EF_TO ),
								"off" 		=> __( "Hide", EF_TO ),
								"type" 		=> "switch",
								"std" 		=> "0",
							);
					
		$of_options[] = array( 	"name" 		=> __( "Tracking Code", EF_TO ),
								"desc" 		=> __( "Paste your Google Analytics (or other) tracking code here. This will be added into the footer template.", EF_TO ),
								"id" 		=> "{$prefix}google_analytics",
								"std" 		=> "",
								"type" 		=> "textarea"
						);
						
		$of_options[] = array( 	"name" 		=> __( "Footer Text", EF_TO ),
								"desc" 		=> __( "Paste your copyrights here. You are able to use html tags and attributes.", EF_TO ),
								"id" 		=> "{$prefix}footer_text",
								"std" 		=> '&copy; 2013 FAVEA - A Premium WordPress Theme. Developed by <a target="_blank" href="http://themeforest.net/user/fireform">Fireform</a> for <a target="_blank" href="http://themeforest.net">ThemeForest.net</a>',
								"type" 		=> "textarea"
						);
				
				
/*-- Home settings --*/

$of_options[] = array( 	"name" 		=> __( "Home", EF_TO ),
						"type" 		=> "heading"
				);

	$of_options[] = array( 	"name" 		=> __( "Page Builder", EF_TO ),
							"desc" 		=> __( "Organize how you want the layout to appear on the homepage.", EF_TO ),
							"id" 		=> "page_build",
							"std" 		=> $of_options_homepage_blocks,
							"type" 		=> "sorter"
					);
	
	
$of_options[] = array( 	"name" 		=> __( "Post", EF_TO ),
						"type" 		=> "heading"
				);
	
	$of_options[] = array( 	"name" 		=> __( "Navigation", EF_TO ),
							"desc" 		=> __( "Navigation through single posts. Applies above each post.", EF_TO ),
							"id" 		=> "{$prefix}post_nav",
							"std" 		=> array('pp',"bp"),
							"type" 		=> "multicheck",
							"options" 	=> array(
								'pp' => __( "Portfolio post", EF_TO ),
								'bp' => __( "Blog post", EF_TO ),
								'ot' => __( "Team post", EF_TO ),
								'ex' => __( "Extra post", EF_TO ),
							)
					);
						
/*-- Layout Settings --*/

$of_options[] = array( 	"name" 		=> __( "Layout", EF_TO ),
						"type" 		=> "heading"
				);
				
	$of_options[] = array( 	"name" 		=> __( "Global layout settings", EF_TO ),
							"id" 		=> "{$prefix}divider",
							"type"		=> "divider"
					);
					
		$of_options[] = array( 	"name"		=> __( "General layout type", EF_TO ),
								"desc" 		=> __( 'Site\'s general layout type.', EF_TO ),
								"id" 		=> "{$prefix}layout_type",
								"type" 		=> "select",
								"options" 	=> array(
									'ef-full-width' => 'Full-width',
									'ef-boxed-ver' 	=> 'Boxed',
								),
								"std" 		=> "ef-full-width"
						);
	
		$of_options[] = array( 	"name" 		=> __( "General Page Layout", EF_TO ),
								"desc" 		=> __( "Select main content and sidebar alignment.", EF_TO ),
								"id" 		=> "{$prefix}layout",
								"std" 		=> "12",
								"type" 		=> "images",
								"options" 	=> array(
									'12' 		=> efto_IMAGES_DIR_URL . '1col.png',
									'93' 		=> efto_IMAGES_DIR_URL . '2cr.png',
									'39' 		=> efto_IMAGES_DIR_URL . '2cl.png'
								)
						);
		
		$of_options[] = array( 	"name" 		=> __( "General Widget Area (Sidebar)", EF_TO ),
								"desc" 		=> $sidebar_dsc,
								"id" 		=> "{$prefix}general_sidebar",
								"std" 		=> "",
								"type" 		=> "select_val_id",
								"options" 	=> $of_sidebars
						);
		
		$of_options[] = array( 	"name"		=> __( "Footer visibility", EF_TO ),
								"desc" 		=> __( 'Show/hide Footer.', EF_TO ),
								"id" 		=> "show_footer",								
								"on" 		=> __( "Show", EF_TO ),
								"off" 		=> __( "Hide", EF_TO ),
								"folds"		=> 1,
								"type" 		=> "switch",
								"std" 		=> "1",
						);
		
		$of_options[] = array( 	"name" 		=> __( "General Widget Area (Footer)", EF_TO ),
								"desc" 		=> $footer_dsc,
								"id" 		=> "{$prefix}general_footer",
								"std" 		=> "",
								"type" 		=> "select_val_id",
								"options" 	=> $of_sidebars,
								"fold"		=> "show_footer"
						);
						
	$of_options[] = array( 	"name" 		=> __( "Blog Settings", EF_TO ),
							"id" 		=> "{$prefix}divider",
							"type"		=> "divider"
					);
		
		$of_options[] = array( 	"name" 		=> __( "Page Layout", EF_TO ),
								"desc" 		=> $layout_dsc,
								"id" 		=> "{$prefix}layout_blog",
								"std" 		=> "93",
								"type" 		=> "images",
								"options" 	=> array(
									'12'		=> efto_IMAGES_DIR_URL . '1col.png',
									'93' 		=> efto_IMAGES_DIR_URL . '2cr.png',
									'39' 		=> efto_IMAGES_DIR_URL . '2cl.png',
								)
						);
						
		$of_options[] = array( 	"name" 		=> __( "Widget Area (Sidebar)", EF_TO ),
								"desc" 		=> $sidebar_dsc,
								"id" 		=> "{$prefix}blog_sidebar",
								"std" 		=> "",
								"type" 		=> "select_val_id",
								"options" 	=> $of_sidebars
						);
		
		$of_options[] = array( 	"name" 		=> __( "Widget Area (Footer)", EF_TO ),
								"desc" 		=> $footer_dsc,
								"id" 		=> "{$prefix}blog_footer",
								"std" 		=> "",
								"type" 		=> "select_val_id",
								"options" 	=> $of_sidebars
						);
	
	$of_options[] = array( 	"name" 		=> __( "Post Settings", EF_TO ),
							"id" 		=> "{$prefix}divider",
							"type"		=> "divider"
					);
				
		$of_options[] = array( 	"name" 		=> __( "Page Layout", EF_TO ),
								"desc" 		=> $layout_dsc,
								"id" 		=> "{$prefix}layout_post",
								"std" 		=> "93",
								"type" 		=> "images",
								"options" 	=> array(
									'12'		=> efto_IMAGES_DIR_URL . '1col.png',
									'93' 		=> efto_IMAGES_DIR_URL . '2cr.png',
									'39' 		=> efto_IMAGES_DIR_URL . '2cl.png',
								)
						);
		
		$of_options[] = array( 	"name" 		=> __( "Widget Area (Sidebar)", EF_TO ),
								"desc" 		=> $sidebar_dsc,
								"id" 		=> "{$prefix}post_sidebar",
								"std" 		=> "",
								"type" 		=> "select_val_id",
								"options" 	=> $of_sidebars
						);
		
		$of_options[] = array( 	"name" 		=> __( "Widget Area (Footer)", EF_TO ),
								"desc" 		=> $footer_dsc,
								"id" 		=> "{$prefix}post_footer",
								"std" 		=> "",
								"type" 		=> "select_val_id",
								"options" 	=> $of_sidebars
						);

	$of_options[] = array( 	"name" 		=> __( "Portfolio Post Settings", EF_TO ),
							"id" 		=> "{$prefix}divider",
							"type"		=> "divider"
					);
				
		$of_options[] = array( 	"name" 		=> __( "Page Layout", EF_TO ),
								"desc" 		=> $layout_dsc,
								"id" 		=> "{$prefix}layout_cpt_post",
								"std" 		=> "93",
								"type" 		=> "images",
								"options" 	=> array(
									'12' 		=> efto_IMAGES_DIR_URL . '1col.png',
									'93' 		=> efto_IMAGES_DIR_URL . '2cr.png',
									'39' 		=> efto_IMAGES_DIR_URL . '2cl.png'
								)
						);	
		
		$of_options[] = array( 	"name" 		=> __( "Widget Area (Sidebar)", EF_TO ),
								"desc" 		=> $sidebar_dsc,
								"id" 		=> "{$prefix}post_cpt_sidebar",
								"std" 		=> "",
								"type" 		=> "select_val_id",
								"options" 	=> $of_sidebars
						);
		
		$of_options[] = array( 	"name" 		=> __( "Widget Area (Footer)", EF_TO ),
								"desc" 		=> $footer_dsc,
								"id" 		=> "{$prefix}post_cpt_footer",
								"std" 		=> "",
								"type" 		=> "select_val_id",
								"options" 	=> $of_sidebars
						);

				

/*-- Archive Settings --*/

$of_options[] = array( 	"name" 		=> __( "Archive", EF_TO ),
						"type" 		=> "heading"
				);
				
	$of_options[] = array( 	"name" 		=> __( "Global Archive Settings", EF_TO ),
							"id" 		=> "{$prefix}divider",
							"type"		=> "divider"
					);
	
	$of_options[] = array( 	"name" 		=> __( "Page Layout", EF_TO ),
							"desc" 		=> $layout_dsc,
							"id" 		=> "{$prefix}layout_archive",
							"std" 		=> "93",
							"type" 		=> "images",
							"options" 	=> array(
								'12'		=> efto_IMAGES_DIR_URL . '1col.png',
								'93' 		=> efto_IMAGES_DIR_URL . '2cr.png',
								'39' 		=> efto_IMAGES_DIR_URL . '2cl.png',
							)
					);	
	
	$of_options[] = array( 	"name" 		=> __( "Widget Area (Sidebar)", EF_TO ),
							"desc" 		=> $sidebar_dsc,
							"id" 		=> "{$prefix}archive_sidebar",
							"std" 		=> "",
							"type" 		=> "select_val_id",
							"options" 	=> $of_sidebars
					);
	
	$of_options[] = array( 	"name" 		=> __( "Widget Area (Footer)", EF_TO ),
							"desc" 		=> $footer_dsc,
							"id" 		=> "{$prefix}archive_footer",
							"std" 		=> "",
							"type" 		=> "select_val_id",
							"options" 	=> $of_sidebars
					);
					
	$of_options[] = array( 	"name" 		=> __( "Portfolio Category Archive", EF_TO ),
							"id" 		=> "{$prefix}divider",
							"type"		=> "divider"
					);
					
	$of_options[] = array( 	"name" 		=> __( "Page Layout", EF_TO ),
							"desc" 		=> $layout_dsc,
							"id" 		=> "{$prefix}layout_cpt_archive",
							"std" 		=> "93",
							"type" 		=> "images",
							"options" 	=> array(
								'12'		=> efto_IMAGES_DIR_URL . '1col.png',
								'93' 		=> efto_IMAGES_DIR_URL . '2cr.png',
								'39' 		=> efto_IMAGES_DIR_URL . '2cl.png'
							)
					);	
				
	$of_options[] = array( 	"name" 		=> __( "Widget Area (Sidebar)", EF_TO ),
							"desc" 		=> $sidebar_dsc,
							"id" 		=> "{$prefix}archive_cpt_sidebar",
							"std" 		=> "",
							"type" 		=> "select_val_id",
							"options" 	=> $of_sidebars
					);
	
	$of_options[] = array( 	"name" 		=> __( "Widget Area (Footer)", EF_TO ),
							"desc" 		=> $footer_dsc,
							"id" 		=> "{$prefix}archive_cpt_footer",
							"std" 		=> "",
							"type" 		=> "select_val_id",
							"options" 	=> $of_sidebars
					);
	
	$of_options[] = array( 	"name" 		=> __( "Portfolio Tag Archive", EF_TO ),
							"id" 		=> "{$prefix}divider",
							"type"		=> "divider"
					);
	
	$of_options[] = array( 	"name" 		=> __( "Page Layout", EF_TO ),
							"desc" 		=> $layout_dsc,
							"id" 		=> "{$prefix}layout_cpt_tag_archive",
							"std" 		=> "93",
							"type" 		=> "images",
							"options" 	=> array(
								'12'		=> efto_IMAGES_DIR_URL . '1col.png',
								'93' 		=> efto_IMAGES_DIR_URL . '2cr.png',
								'39' 		=> efto_IMAGES_DIR_URL . '2cl.png'
							)
					);
				
	$of_options[] = array( 	"name" 		=> __( "Widget Area (Sidebar)", EF_TO ),
							"desc" 		=> $sidebar_dsc,
							"id" 		=> "{$prefix}archive_cpt_tag_sidebar",
							"std" 		=> "",
							"type" 		=> "select_val_id",
							"options" 	=> $of_sidebars
					);
	
	$of_options[] = array( 	"name" 		=> __( "Widget Area (Footer)", EF_TO ),
							"desc" 		=> $footer_dsc,
							"id" 		=> "{$prefix}archive_cpt_tag_footer",
							"std" 		=> "",
							"type" 		=> "select_val_id",
							"options" 	=> $of_sidebars
					);
	

/*-- Sidebars --*/

$of_options[] = array( 	"name" 		=> __( "Sidebar Generator", EF_TO ),
						"type" 		=> "heading"
				);

	$of_options[] = array( 	"name" 		=> __( "Widget Areas Manager", EF_TO ),
							"desc" 		=> __( "Create new widget areas if you need to display different widgets on several pages.", EF_TO ),
							"id" 		=> "{$prefix}sidebar_manager",
							"std" 		=> "",
							"type" 		=> "sidebar"
					);
					
/*-- Breadcrumbs --*/

$of_options[] = array( 	"name" 		=> __( "Navigation", EF_TO ),
						"type" 		=> "heading"
				);
				
	$of_options[] = array( 	"name" 		=> __( '"Sticky" navigation', EF_TO ),
							"desc" 		=> __( 'Enable or disable "Sticky" navigation on the site.', EF_TO ),
							"id" 		=> "{$prefix}sticky_head",
							"std" 		=> "1",
							"on" 		=> __( "Enable", EF_TO ),
							"off" 		=> __( "Disable", EF_TO ),
							"type" 		=> "switch",
					);
				
	
	$of_options[] = array( 	"name" 		=> __( "Breadcrumbs navigation", EF_TO ),
							"id" 		=> "{$prefix}divider",
							"type"		=> "divider"
					);

	$of_options[] = array( 	"name" 		=> __( "Enable/disable navigation", EF_TO ),
							"desc" 		=> __( 'Show or hide "Breadcrumbs" navigation on pages.', EF_TO ),
							"id" 		=> "on_sw",
							"std" 		=> "1",
							"on" 		=> __( "Enable", EF_TO ),
							"off" 		=> __( "Disable", EF_TO ),
							"type" 		=> "switch",
							"folds"		=> 1,
					);
	
	$of_options[] = array( 	"name" 		=> __( "Link titles", EF_TO ),
							"desc" 		=> __( 'Show or hide "title" attribute for links in the "Breadcrumbs".', EF_TO ),
							"id" 		=> "{$prefix}title_sw",
							"std" 		=> "0",
							"on" 		=> __( "Show", EF_TO ),
							"off" 		=> __( "Hide", EF_TO ),
							"type" 		=> "switch",
							"fold"		=> "on_sw"
					);
					
	$of_options[] = array( 	"name" 		=> __( "Separator", EF_TO ),
							"desc" 		=> __( 'Separator for the links in the "Breadcrumbs".', EF_TO ),
							"id" 		=> "{$prefix}sep",
							"type" 		=> "radio",
							"std" 		=> "sep_1",
							"options"	=> array(
								"sep_1"	=> "&#8594;",								
								"sep_2"	=> "&raquo;",
								"sep_3"	=> "&#47;",
								"sep_4"	=> "&#124;",
							),
							"fold"		=> "on_sw"
							
					);
					

/*-- Styling Options --*/

$of_options[] = array( 	"name" 		=> __( "Styling Options", EF_TO ),
						"type" 		=> "heading"
				);
	
	$of_options[] = array( 	"name" 		=> __( "Typography", EF_TO ),
							"id" 		=> "{$prefix}divider",
							"type"		=> "divider"
					);
					
		$of_options[] = array( 	"name" 		=> __( "Heading Font", EF_TO ),
								"desc" 		=> __( "Specify heading font properties.", EF_TO ),
								"id" 		=> "{$prefix}heading_font",
								"std" 		=> array('face' => 'Lato','style' => 'normal'),
								"type" 		=> "typography"
						);
						
		$of_options[] = array( 	"name" 		=> __( "Content Font", EF_TO ),
								"desc" 		=> __( "Specify content font properties.", EF_TO ),
								"id" 		=> "{$prefix}content_font",
								"std" 		=> array('face' => 'Lato','color' => '#707070'),
								"type" 		=> "typography"
						);
	
	$of_options[] = array( 	"name" 		=> __( "Colors/backgrounds", EF_TO ),
							"id" 		=> "{$prefix}divider",
							"type"		=> "divider"
					);
		
		$of_options[] = array( 	"name" 		=> __( "Primary color", EF_TO ),
								"desc" 		=> __( 'Pick a primary color for the theme.', EF_TO ),
								"id" 		=> "{$prefix}main_col",
								"std" 		=> "#ef8e56",
								"type" 		=> "color"
						);
						
		$of_options[] = array( 	"name" 		=> __( "Secondary color", EF_TO ),
								"desc" 		=> __( 'Pick a secondary color for the theme. Usually it\'s much the same as original color, but darker a bit. It\'s necessary for better readability.', EF_TO ),
								"id" 		=> "{$prefix}second_col",
								"std" 		=> "#d37919",
								"type" 		=> "color"
						);
		
		$of_options[] = array( 	"name" 		=> __( "Body Background Color", EF_TO ),
								"desc" 		=> __( 'Pick a background color for the body (applies if "Boxed" general layout type was choosed).', EF_TO ),
								"id" 		=> "{$prefix}body_background",
								"std" 		=> "#f5f5f5",
								"type" 		=> "color"
						);
		
		$of_options[] = array( 	"name" 		=> __( "Background Images", EF_TO ),
								"desc" 		=> __( 'Select a background pattern (applies if "Boxed" general layout type was choosed).', EF_TO ),
								"id" 		=> "{$prefix}custom_bg",
								"std" 		=> $bg_images_url."bg10.png",
								"type" 		=> "tiles",
								"options" 	=> $bg_images,
						);
						
		$of_options[] = array( 	"desc" 		=> __( 'Select a "background-attachment" attribute.', EF_TO ),
								"id" 		=> "{$prefix}bg_pos",
								"std" 		=> "scroll",
								"type" 		=> "select",
								"options" 	=> array(
									"scroll"	=> "Scroll",
									"fixed"		=> "Fixed"
								)
						);
	
	$of_options[] = array( 	"name" 		=> __( "Custom CSS", EF_TO ),
							"id" 		=> "{$prefix}divider",
							"type"		=> "divider"
					);
						
		$of_options[] = array( 	"desc" 		=> __( "Quickly add some CSS to the theme by adding it to this block.", EF_TO ),
								"id" 		=> "{$prefix}custom_css",
								"std" 		=> "",
								"type" 		=> "textarea"
						);
				
			
/*-- Backup Options --*/

$of_options[] = array( 	"name" 		=> __( "Backup Options", EF_TO ),
						"type" 		=> "heading"
				);
				
$of_options[] = array( 	"name" 		=> __( "Backup and Restore Options", EF_TO ),
						"id" 		=> "{$prefix}of_backup",
						"std" 		=> "",
						"type" 		=> "backup",
						"desc" 		=> __( 'You can use the two buttons below to backup your current options, and then restore it back at a later time. This is useful if you want to experiment on the options but would like to keep the old settings in case you need it back.', EF_TO ),
				);
				
$of_options[] = array( 	"name" 		=> __( "Transfer Theme Options Data", EF_TO ),
						"id" 		=> "{$prefix}of_transfer",
						"std" 		=> "",
						"type" 		=> "transfer",
						"desc" 		=> __( 'You can tranfer the saved options data between different installs by copying the text inside the text box. To import data from another install, replace the data in the text box with the one from another install and click "Import Options".', EF_TO ),
				);
				
	}//End function: of_options()
}//End check if function exists: of_options()
?>