CREATE TABLE `chq_28_usermeta` (  `umeta_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,  `user_id` bigint(20) unsigned NOT NULL DEFAULT '0',  `meta_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,  `meta_value` longtext COLLATE utf8mb4_unicode_ci,  PRIMARY KEY (`umeta_id`),  KEY `user_id` (`user_id`),  KEY `meta_key` (`meta_key`(191))) ENGINE=InnoDB AUTO_INCREMENT=73 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40000 ALTER TABLE `chq_28_usermeta` DISABLE KEYS */;
INSERT INTO `chq_28_usermeta` VALUES('1', '45', 'chq_28_capabilities', 'a:1:{s:13:\"administrator\";s:1:\"1\";}');
INSERT INTO `chq_28_usermeta` VALUES('2', '45', 'chq_28_user_level', '10');
INSERT INTO `chq_28_usermeta` VALUES('6', '45', 'chq_28_dashboard_quick_press_last_post_id', '2844');
INSERT INTO `chq_28_usermeta` VALUES('7', '45', 'wporg_favorites', '');
INSERT INTO `chq_28_usermeta` VALUES('9', '45', 'wpseo_ignore_tour', '1');
INSERT INTO `chq_28_usermeta` VALUES('10', '45', 'wpseo_seen_about_version', '3.1.1');
INSERT INTO `chq_28_usermeta` VALUES('11', '45', 'nav_menu_recently_edited', '24');
INSERT INTO `chq_28_usermeta` VALUES('12', '45', 'managenav-menuscolumnshidden', 'a:5:{i:0;s:11:\"link-target\";i:1;s:11:\"css-classes\";i:2;s:3:\"xfn\";i:3;s:11:\"description\";i:4;s:15:\"title-attribute\";}');
INSERT INTO `chq_28_usermeta` VALUES('13', '45', 'metaboxhidden_nav-menus', 'a:5:{i:0;s:21:\"add-post-type-gallery\";i:1;s:17:\"add-post-type-faq\";i:2;s:20:\"add-post-type-slider\";i:3;s:12:\"add-post_tag\";i:4;s:20:\"add-gallery-category\";}');
INSERT INTO `chq_28_usermeta` VALUES('14', '45', 'tgmpa_dismissed_notice', '1');
INSERT INTO `chq_28_usermeta` VALUES('15', '46', '_yoast_wpseo_profile_updated', '1458540035');
INSERT INTO `chq_28_usermeta` VALUES('16', '46', 'wpseo_title', '');
INSERT INTO `chq_28_usermeta` VALUES('17', '46', 'wpseo_metadesc', '');
INSERT INTO `chq_28_usermeta` VALUES('18', '46', 'wpseo_metakey', '');
INSERT INTO `chq_28_usermeta` VALUES('19', '46', 'wpseo_noindex_author', '');
INSERT INTO `chq_28_usermeta` VALUES('20', '46', 'nickname', 'Che');
INSERT INTO `chq_28_usermeta` VALUES('21', '46', 'first_name', '');
INSERT INTO `chq_28_usermeta` VALUES('22', '46', 'last_name', '');
INSERT INTO `chq_28_usermeta` VALUES('23', '46', 'description', '');
INSERT INTO `chq_28_usermeta` VALUES('24', '46', 'rich_editing', 'true');
INSERT INTO `chq_28_usermeta` VALUES('25', '46', 'comment_shortcuts', 'false');
INSERT INTO `chq_28_usermeta` VALUES('26', '46', 'admin_color', 'fresh');
INSERT INTO `chq_28_usermeta` VALUES('27', '46', 'use_ssl', '0');
INSERT INTO `chq_28_usermeta` VALUES('28', '46', 'show_admin_bar_front', 'true');
INSERT INTO `chq_28_usermeta` VALUES('29', '46', 'googleplus', '');
INSERT INTO `chq_28_usermeta` VALUES('30', '46', 'twitter', '');
INSERT INTO `chq_28_usermeta` VALUES('31', '46', 'facebook', '');
INSERT INTO `chq_28_usermeta` VALUES('32', '46', 'chq_28_capabilities', 'a:1:{s:13:\"administrator\";b:1;}');
INSERT INTO `chq_28_usermeta` VALUES('33', '46', 'chq_28_user_level', '10');
INSERT INTO `chq_28_usermeta` VALUES('36', '45', 'session_tokens', 'a:1:{s:64:\"1f08dcd217c051c401e6f1d19937a0e9a921b90d3f057ff353e439d49df6f279\";a:4:{s:10:\"expiration\";i:1522391585;s:2:\"ip\";s:14:\"139.162.61.192\";s:2:\"ua\";s:114:\"Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36\";s:5:\"login\";i:1522218785;}}');
INSERT INTO `chq_28_usermeta` VALUES('37', '45', 'itsec-settings-view', 'list');
INSERT INTO `chq_28_usermeta` VALUES('38', '45', 'chq_28_yoast_notifications', 'a:3:{i:0;a:2:{s:7:\"message\";s:559:\"To make sure all the links in your texts are counted, we need to analyse all your texts.\n					All you have to do is press the following button and we\'ll go through all your texts for you.\n					\n					<button type=\"button\" id=\"noticeRunLinkIndex\" class=\"button\">Count links</button>\n					\n					The Text link counter feature provides insights in how many links are found in your text and how many links are referring to your text. This is very helpful when you are improving your <a href=\"https://yoa.st/15m?utm_content=5.1\" target=\"_blank\">internal linking</a>.\";s:7:\"options\";a:8:{s:4:\"type\";s:7:\"warning\";s:2:\"id\";s:19:\"wpseo-reindex-links\";s:5:\"nonce\";N;s:8:\"priority\";d:0.80000000000000004;s:9:\"data_json\";a:0:{}s:13:\"dismissal_key\";N;s:12:\"capabilities\";s:14:\"manage_options\";s:16:\"capability_check\";s:3:\"all\";}}i:1;a:2:{s:7:\"message\";s:793:\"We\'ve noticed you\'ve been using Yoast SEO for some time now; we hope you love it! We\'d be thrilled if you could <a href=\"https://yoa.st/rate-yoast-seo?utm_content=7.1\">give us a 5 stars rating on WordPress.org</a>!\n\nIf you are experiencing issues, <a href=\"https://yoa.st/bugreport?utm_content=7.1\">please file a bug report</a> and we\'ll do our best to help you out.\n\nBy the way, did you know we also have a <a href=\'https://yoa.st/premium-notification?utm_content=7.1\'>Premium plugin</a>? It offers advanced features, like a redirect manager and support for multiple keywords. It also comes with 24/7 personal support.\n\n<a class=\"button\" href=\"http://debtconsolidationpersonalloans.com.au/wp-admin/?page=wpseo_dashboard&yoast_dismiss=upsell\">Please don\'t show me this notification anymore</a>\";s:7:\"options\";a:8:{s:4:\"type\";s:7:\"warning\";s:2:\"id\";s:19:\"wpseo-upsell-notice\";s:5:\"nonce\";N;s:8:\"priority\";d:0.80000000000000004;s:9:\"data_json\";a:0:{}s:13:\"dismissal_key\";N;s:12:\"capabilities\";s:20:\"wpseo_manage_options\";s:16:\"capability_check\";s:3:\"all\";}}i:2;a:2:{s:7:\"message\";s:187:\"Don\'t miss your crawl errors: <a href=\"http://debtconsolidationpersonalloans.com.au/wp-admin/admin.php?page=wpseo_search_console&tab=settings\">connect with Google Search Console here</a>.\";s:7:\"options\";a:8:{s:4:\"type\";s:7:\"warning\";s:2:\"id\";s:17:\"wpseo-dismiss-gsc\";s:5:\"nonce\";N;s:8:\"priority\";d:0.5;s:9:\"data_json\";a:0:{}s:13:\"dismissal_key\";N;s:12:\"capabilities\";s:20:\"wpseo_manage_options\";s:16:\"capability_check\";s:3:\"all\";}}}');
INSERT INTO `chq_28_usermeta` VALUES('40', '45', 'wpseo-dismiss-gsc', 'seen');
INSERT INTO `chq_28_usermeta` VALUES('41', '45', 'itsec_user_activity_last_seen', '1522218787');
INSERT INTO `chq_28_usermeta` VALUES('42', '45', 'meta-box-order_dashboard', 'a:4:{s:6:\"normal\";s:63:\"dashboard_right_now,dashboard_activity,wpseo-dashboard-overview\";s:4:\"side\";s:39:\"dashboard_quick_press,dashboard_primary\";s:7:\"column3\";s:20:\"pb_backupbuddy_stats\";s:7:\"column4\";s:0:\"\";}');
INSERT INTO `chq_28_usermeta` VALUES('43', '45', 'community-events-location', 'a:1:{s:2:\"ip\";s:12:\"203.27.178.0\";}');
INSERT INTO `chq_28_usermeta` VALUES('44', '47', 'nickname', 'Shelia Taylor');
INSERT INTO `chq_28_usermeta` VALUES('45', '47', 'first_name', 'Shelia');
INSERT INTO `chq_28_usermeta` VALUES('46', '47', 'last_name', 'Taylor');
INSERT INTO `chq_28_usermeta` VALUES('47', '47', 'description', '');
INSERT INTO `chq_28_usermeta` VALUES('48', '47', 'rich_editing', 'true');
INSERT INTO `chq_28_usermeta` VALUES('49', '47', 'comment_shortcuts', 'false');
INSERT INTO `chq_28_usermeta` VALUES('50', '47', 'admin_color', 'fresh');
INSERT INTO `chq_28_usermeta` VALUES('51', '47', 'use_ssl', '0');
INSERT INTO `chq_28_usermeta` VALUES('52', '47', 'show_admin_bar_front', 'true');
INSERT INTO `chq_28_usermeta` VALUES('53', '47', 'locale', '');
INSERT INTO `chq_28_usermeta` VALUES('54', '47', 'chq_28_capabilities', 'a:1:{s:13:\"administrator\";b:1;}');
INSERT INTO `chq_28_usermeta` VALUES('55', '47', 'chq_28_user_level', '10');
INSERT INTO `chq_28_usermeta` VALUES('56', '47', '_yoast_wpseo_profile_updated', '1507077050');
INSERT INTO `chq_28_usermeta` VALUES('57', '47', 'dismissed_wp_pointers', '');
INSERT INTO `chq_28_usermeta` VALUES('58', '48', 'nickname', 'Jun Li');
INSERT INTO `chq_28_usermeta` VALUES('59', '48', 'first_name', 'Jun');
INSERT INTO `chq_28_usermeta` VALUES('60', '48', 'last_name', 'Li');
INSERT INTO `chq_28_usermeta` VALUES('61', '48', 'description', '');
INSERT INTO `chq_28_usermeta` VALUES('62', '48', 'rich_editing', 'true');
INSERT INTO `chq_28_usermeta` VALUES('63', '48', 'comment_shortcuts', 'false');
INSERT INTO `chq_28_usermeta` VALUES('64', '48', 'admin_color', 'fresh');
INSERT INTO `chq_28_usermeta` VALUES('65', '48', 'use_ssl', '0');
INSERT INTO `chq_28_usermeta` VALUES('66', '48', 'show_admin_bar_front', 'true');
INSERT INTO `chq_28_usermeta` VALUES('67', '48', 'locale', '');
INSERT INTO `chq_28_usermeta` VALUES('68', '48', 'chq_28_capabilities', 'a:1:{s:13:\"administrator\";b:1;}');
INSERT INTO `chq_28_usermeta` VALUES('69', '48', 'chq_28_user_level', '10');
INSERT INTO `chq_28_usermeta` VALUES('70', '48', '_yoast_wpseo_profile_updated', '1507077070');
INSERT INTO `chq_28_usermeta` VALUES('71', '48', 'dismissed_wp_pointers', '');
INSERT INTO `chq_28_usermeta` VALUES('72', '45', '_yoast_wpseo_profile_updated', '1522254979');
/*!40000 ALTER TABLE `chq_28_usermeta` ENABLE KEYS */;
