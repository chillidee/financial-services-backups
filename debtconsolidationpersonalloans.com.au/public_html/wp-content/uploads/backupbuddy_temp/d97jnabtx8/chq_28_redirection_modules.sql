CREATE TABLE `chq_28_redirection_modules` (  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,  `type` varchar(20) NOT NULL DEFAULT '',  `name` varchar(50) NOT NULL DEFAULT '',  `options` mediumtext,  PRIMARY KEY (`id`),  KEY `name` (`name`),  KEY `type` (`type`)) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40000 ALTER TABLE `chq_28_redirection_modules` DISABLE KEYS */;
INSERT INTO `chq_28_redirection_modules` VALUES('1', 'wp', 'WordPress', '');
INSERT INTO `chq_28_redirection_modules` VALUES('2', 'apache', 'Apache', '');
/*!40000 ALTER TABLE `chq_28_redirection_modules` ENABLE KEYS */;
