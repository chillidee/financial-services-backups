CREATE TABLE `chq_28_redirection_groups` (  `id` int(11) NOT NULL AUTO_INCREMENT,  `name` varchar(50) NOT NULL,  `tracking` int(11) NOT NULL DEFAULT '1',  `module_id` int(11) unsigned NOT NULL DEFAULT '0',  `status` enum('enabled','disabled') NOT NULL DEFAULT 'enabled',  `position` int(11) unsigned NOT NULL DEFAULT '0',  PRIMARY KEY (`id`),  KEY `module_id` (`module_id`),  KEY `status` (`status`)) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40000 ALTER TABLE `chq_28_redirection_groups` DISABLE KEYS */;
INSERT INTO `chq_28_redirection_groups` VALUES('1', 'Redirections', '1', '1', 'enabled', '0');
INSERT INTO `chq_28_redirection_groups` VALUES('2', 'Modified Posts', '1', '1', 'enabled', '1');
/*!40000 ALTER TABLE `chq_28_redirection_groups` ENABLE KEYS */;
