<?php

// BEGIN iThemes Security - Do not modify or remove this line
// iThemes Security Config Details: 2
define( 'DISALLOW_FILE_EDIT', true ); // Disable File Editor - Security > Settings > WordPress Tweaks > File Editor
// END iThemes Security - Do not modify or remove this line

/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'mor5797_newdb');

/** MySQL database username */
define('DB_USER', 'mor5797_user');

/** MySQL database password */
define('DB_PASSWORD', 'U^,R5VpH~sfi');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY', 'kX10rB6$UHYHx&G87$Y4VunKDoiE5Od$/4Bp;>;MCQ(Zunu{{&TK?6)C)|6ME9nC');
define('SECURE_AUTH_KEY', 'APSrKJ4!+KygXS>MwN-vf4X[Uqz+[Nnm}6Ezb>?]Fw<{#{B/CksI#%vu:Uf&>t;_');
define('LOGGED_IN_KEY', '+vi]qg#VC8KdX~b+)utV.P%},)8}bZ![))#/]OSqj{u6ZvSHP!{Mn5BjM, OV!BC');
define('NONCE_KEY', 'TbzHQ^YZ6&sra~ZQKN{Xfp7Rl6:7Et,=uvr~?$w5UP_VnI/X)+h[b#EnGx(~Q$V~');
define('AUTH_SALT', '.@I,P50,hJQu^O^Zz+MAZ^O;au3R.ko:_X{yoPuv=~Q2luoLf1!4EmWFGl_Cw?vg');
define('SECURE_AUTH_SALT', 'VkF`Z0;KxI J2n(7#Tyf5)L?:uBpsWw@61JsnB2L`1(y$1:PgrV!?:`;m/gFUCfn');
define('LOGGED_IN_SALT', '|PFd%>PNWv~x_4@|)u2*nz`Ax^5&g~;.mx }sPC#` ?U4~KL: qgz]GW4?A`9s6)');
define('NONCE_SALT', 'Q6`8h?J&?Hz2f` ?9qe>eKR8I&Fe.?+M5Wh@VQRT=^i}Ni]iy;dCCU/~yD$vB18:');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_rsnm_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

/**
 * Include tweaks requested by hosting providers.  You can safely
 * remove either the file or comment out the lines below to get
 * to a vanilla state.
 */
if (file_exists(ABSPATH . 'hosting_provider_filters.php')) {
	include('hosting_provider_filters.php');
}
