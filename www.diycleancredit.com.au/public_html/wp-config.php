<?php

// BEGIN iThemes Security - Do not modify or remove this line
// iThemes Security Config Details: 2
define( 'DISALLOW_FILE_EDIT', true ); // Disable File Editor - Security > Settings > WordPress Tweaks > File Editor
// END iThemes Security - Do not modify or remove this line

/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, and ABSPATH. You can find more information by visiting
 * {@link https://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'admin_diy');

/** MySQL database username */
define('DB_USER', 'admin_diy');

/** MySQL database password */
define('DB_PASSWORD', 'JKtNbHKsXc');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'h.jA|2g_#JJW{yf,q,@#eB%):r>!lSu,#u,|W4#9>*+ibL{7$m{^eiQ/3|uMvjhj');
define('SECURE_AUTH_KEY',  'f-(@5w_;MP]CbwT2_FQTs:-PL_JK8/BWe,/lC:(,,gZ0Ue[v(i0N12s=9m7q tN*');
define('LOGGED_IN_KEY',    'vb/pPA7X*/`9mNMiB/aAV^TxjH{C.VH]|cVDx^6w=B@,pX6G4Y;P6w(4hdZ`cUt$');
define('NONCE_KEY',        'O(l0#[$cjY<h!oRuJ%-M&?%~IU?t-hQdPyny(ZD~uk)dMmqspgzuK`bXwn(iRDt[');
define('AUTH_SALT',        'Y %|CZ?XzR3TuvxU@K:^FmbrG`g_SP@7ejj!1sZA#D(x:rePVX4x[M*>j;beeh+^');
define('SECURE_AUTH_SALT', 'ufZ0&.F+$!w3+&i#S5NqbnN._c{sg$&l;-DNw#U5 N^G>]6NM_0V EckeR62&Q7p');
define('LOGGED_IN_SALT',   ':x$x*Ot:7W}khUqh$&EVM4.ex/:(J6`=25c9w7?u8N/3&|uP={xmb%r*e2m$G#|:');
define('NONCE_SALT',       'us0Ik<lgSoTae}47gYDM3^5gqN<X@Bm7)A]u1NsITq5=P_Makil2>AP>t,d$]:R|');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'diy';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG',false);
define( 'WP_AUTO_UPDATE_CORE', false );

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
  define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');