/*---
 * Custom scripts for the CC DIY website--
 *-----*/
 //Add missing indexof functionality for ie
 if (!Array.prototype.indexOf)
{
  Array.prototype.indexOf = function(elt /*, from*/)
  {
    var len = this.length >>> 0;

    var from = Number(arguments[1]) || 0;
    from = (from < 0)
         ? Math.ceil(from)
         : Math.floor(from);
    if (from < 0)
      from += len;

    for (; from < len; from++)
    {
      if (from in this &&
          this[from] === elt)
        return from;
    }
    return -1;
  };
}
jQuery( document ).ready(function($) {
$('body').scrollTop(0);
function getParameterByName(name) {
    var match = RegExp('[?&]' + name + '=([^&]*)').exec(window.location.search);
    return match && decodeURIComponent(match[1].replace(/\+/g, ' '));
}
if(  getParameterByName('purchase') == 'packages' ) {
    $( '#please_purchase' ).show();
    $( 'html' ).on( 'click', function(e) { 
         $( '#please_purchase' ).hide();
         window.history.pushState('',document.title,'https://www.diy.cleancredit.com.au/packages');
    }); 
    } 
    $( '#loading-back' ).fadeOut();
    $( '#loader' ).fadeOut();
open = 0;
$( '#hamburger' ).on( 'click', function() {
    if ( open == 0 ) {
        open = 1;
        $( '#menu-header-menu' ).css({ 
             height: '280px',
             overflow: 'visible'
 });
        $( '.login a' ).show();
}
else {
     open = 0;
     $( '#menu-header-menu' ).css({ 
             height: '0px',
             overflow: 'hidden'
 });
     $( '.login a' ).hide();
        }   
});
    
//Simple sticky header
fading =0;
 function bindScroll(){
   if ( fading == 0 ){
       fading = 1;
       if($(window).scrollTop() > 100) {
           $( '#sticky-header' ).fadeIn();
       }
   if($(window).scrollTop() < 10) {
       $( '#sticky-header' ).fadeOut();
   }
   fading = 0
   }
}
$(window).bind('scroll', bindScroll);
var tab_id,
    str,
    total_price;
    //Tab functions for packages page
$( '.tab_link' ).on( 'click', function(e) {
    if (! $( this).hasClass( 'unclick') ) {
       $( '.active' ).removeClass( 'active' );
       $( this ).parent().addClass( 'active' );
       str = $( this ).attr( 'id' );
       tab_id = str.substring(0, str.length - 5);
       $( '#' + tab_id ).addClass( 'active');
    }
});
$( '.kit_tab_link' ).on( 'click', function(e) {
       $( '.kit-box.active' ).removeClass( 'active' );
       $( '.active_kittab' ).removeClass( 'active_kittab' );
       $( this ).parent().addClass( 'active_kittab' );
       str = $( this ).attr( 'class' );
       tab_id = str.slice(-1);
       var kit_box = '.kit_box_' + tab_id;
       $( '.active_tab' ).removeClass( 'active_tab' );
       $( '.kit_box_' + tab_id ).addClass( 'active_tab');
});
$( '.close' ).on( 'click',function() {   
     $( this ).parent().hide();
     $( '#loading-back' ).hide();
     $( 'html' ).removeClass( 'no-scroll' );
     });
$( '.back' ).on( 'click',function() {
      $('body').scrollTop(0);
     $( '.kit').hide();
     $( '#loading-back' ).hide();
     $( 'html' ).removeClass( 'no-scroll' );
     });
$( '.product_button' ).on( 'click',function() {
       ga('send','event','View Product',$( this ).parent().children('h3').html(),'click');
       str = $( this ).attr( 'id' );
       kit_id = str.slice(-1);
       $( '#kit_' + kit_id ).show();
       $( '#loading-back' ).show();
       $( 'html' ).addClass( 'no-scroll' );
       });
$( '.product_inner' ).on( 'click',function() {
       if ( $(this).hasClass( 'purchased_product' ) ) {
           $( '#loading-back' ).show();
           $( '#loader' ).show();
           window.location.href = "/getting-started";
           }
       else if ( $(this).hasClass( 'expired' ) ) {
       str = $( this ).find('.renew_button' ).attr( 'id' );
       kit_id = str.slice(-1);
       $( '#kit_' + kit_id ).show();
       $( '#loading-back' ).show();
       $( 'html' ).addClass( 'no-scroll' );
       }
       else {
       str = $( this ).find('.product_button' ).attr( 'id' );
       kit_id = str.slice(-1);
       $( '#kit_' + kit_id ).show();
       $( '#loading-back' ).show();
       $( 'html' ).addClass( 'no-scroll' );
       }
}); 
$( '.add-to-cart' ).on( 'click',function() {
   ga('send','event','Add to cart',$( this ).parent('#packages').children('h2').html(),'click');
   $('body').scrollTop(0);
   $( '#loading-back-2' ).show();
   $( '#loader' ).show();
   var data = {
		'action': 'add_product_to_cart',
		'id' : $( this ).data( 'id' )
	};
	jQuery.post(ajaxurl, data, function(response) {
	        $( 'html' ).removeClass( 'no-scroll' );
		$( '.kit' ).hide();
		$( '#loading-back-2' ).hide();
		$( '#loading-back' ).hide();
		$( '#loader' ).hide();
		$( '#loading-back' ).hide();
		$( '#inner-' + data.id ).addClass( 'added_product' );
		products_array;
		products_array.push(data.id);     
		$( '.fix_my' ).css("display", "block");
		$( '#cart_link' ).removeClass( 'unclick' );
		$( '#checkout_link' ).removeClass( 'unclick' );	
		obj = JSON.parse(response);
		$( '.cart_container' ).before( obj.html );
		//price += parseInt(obj.price);
		products ++;
		price = 0;
		if ( products > 0 ) {
		    price = price_1 + (( products - 1 ) * price_2 );
		}
                 $( '#cart_total_cart' ).html( '' ); 
                     var newData = {'action': 'fs_get_cart_total'};
	             jQuery.post(ajaxurl, newData, function(response) {
                     $( '#cart_total_cart' ).html( response ); 
	        });     
                $('.kit[data-kit-number=' + data.id +']').addClass( 'in_cart_kit' );
                $( '.remove_item' ).off();
                $( '.remove_item' ).on( 'click',function(e) {
                e.stopPropagation();
                   remove_item( $( this ).data( 'id' ) );
                });
	});
});
function remove_item( id ){
        $( '#loading-back' ).show();
        $( '#loader' ).show();
var data = {
		'action': 'remove_product_from_cart',
		'id' : id
	};
	jQuery.post(ajaxurl, data, function(response) {
	        $('body').scrollTop(0);
	        $( 'html' ).removeClass( 'no-scroll' );
	        $( '#loading-back' ).hide();
	        $( '#loading-back' ).hide();
                $( '#loader' ).hide();
		$( '#product-' + data.id ).remove();
		$( '#inner-' + data.id ).removeClass( 'added_product' );
		//price = parseInt(response);
		products --;
		price = 0;
		if ( products > 0 ) {
		    price = price_1 + (( products - 1 ) * price_2 );
		}
		$( 'span.price.cart_span:first' ).html( '$' + price_1 );
		if ( products == 0 ) {
                    $( '#packages_link' ).click();
                    $( '#cart_link' ).addClass( 'unclick' );
                    $( '#checkout_link' ).addClass( 'unclick' );
                    $( '.fix_my' ).hide();
                    }
	        $( '#cart_total_cart' ).html( '' ); 
                     var newData = {'action': 'fs_get_cart_total'};
	             jQuery.post(ajaxurl, newData, function(response) {
                     $( '#cart_total_cart' ).html( response ); 
	        }); 
		var index = products_array.indexOf(5);
		if (index > -1) {
                  products_array.splice(index, 1);
                }
                $( '.kit').hide();
                $('.kit[data-kit-number=' + data.id +']').removeClass( 'in_cart_kit' );
	});
	}
$( '.remove_item' ).on( 'click',function(e) {
    ga('send','event','Revove item',$( this ).parent().children('h3').html(),'click');
    e.stopPropagation();
    remove_item($( this ).data( 'id' ));
});
$( '#place_order' ).on( 'click', function() {
     ga('send','event','Place order','Go to paypal','click');
     });
$( '.fix_my' ).on( 'click', function(e) {
    ga('send','event','Proceed to checkout','First Page','click');
    e.stopPropagation();
    $('body').scrollTop(0);
    $( '#cart_link' ).click();
    $( '#loader' ).hide();
    $( '#loading-back' ).hide();
    }); 
$( '#purchase' ).on( 'click', function() {
        ga('send','event','Proceed to checkout','Second Page','click');
        $('body').scrollTop(0);
        $( '#loading-back' ).show();
        $( '#loader' ).show();
var data = {
		'action': 'get_checkout'
	};
    jQuery.post(ajaxurl, data, function(response) {
           $( '#checkout_link' ).click();
           $( '#checkout_backer' ).html( '<div class ="col-md-10 col-md-offset-1" id ="checkout_backer">' + response + '</div>' );
           $( '#payment_method_paypal' ).prop('checked', true);
           $( '#createaccount' ).prop('checked', true);
           $( '.order-total' ).find( '.amount' ).html( '' ); 
           var data = {'action': 'fs_get_cart_total'};
	jQuery.post(ajaxurl, data, function(response) {
           $( '.order-total' ).find( '.amount' ).html( response ); 
           $( '#loader' ).hide();
           $( '#loading-back' ).hide();
           $( '#payment_method_paypal' ).prop('checked', true);
	});
	});
    }); 
$( '#checkout_link' ).on( 'click', function() {
        $('body').scrollTop(0);
        $( '#loading-back' ).show();
        $( '#loader' ).show();
var data = {
		'action': 'get_checkout'
	};
    jQuery.post(ajaxurl, data, function(response) {
           $( '#checkout_backer' ).html( '<div class ="col-md-10 col-md-offset-1" id ="checkout_backer">' + response + '</div>' );
            $( '.back-to-packages' ).on( 'click',function() {
                          $('body').scrollTop(0);
                          $( '#packages_link' ).click();
                      });
           $( '#createaccount' ).prop('checked', true);
           $( '#payment_method_paypal' ).prop('checked', true);
           $( '.order-total' ).find( '.amount' ).html( '' ); 
           var data = {'action': 'fs_get_cart_total'};
	jQuery.post(ajaxurl, data, function(response) {
           $( '.order-total' ).find( '.amount' ).html( response ); 
           $( '#loader' ).hide();
           $( '#loading-back' ).hide();
	});
	});
    }); 
$( '.back-to-packages' ).on( 'click',function() {
    $('body').scrollTop(0);
    $( '#packages_link' ).click();
    });
    /*----------------------handle coupons----------
---------------------------------------------------------*/
$(".woocommerce-message").detach().appendTo('.voucher_line');
$( ".woocommerce-error").detach().appendTo('.voucher_line');
                price = 0;
		if ( products > 0 ) {
		    price = price_1 + (( products - 1 ) * price_2 );
		}
var adjPrice = price;
var discounthtml = $( '.cart-discount' ).find( '.amount' ).html();
  if ( discounthtml !== undefined ) {
    var discount = parseInt(discounthtml.slice(1));
    adjPrice = price - discount;
    $( '.order-total' ).find( '.amount' ).html( '' ); 
           var data = {'action': 'fs_get_cart_total'};
	jQuery.post(ajaxurl, data, function(response) {
           $( '.order-total' ).find( '.amount' ).html( '$' + response ); 
	});
    $( '#savings' ).show().html( 'You saved $' + discount );
    $( '#old-price' ).html( 'Price: $' + ( price ) );
    $( '#cart_link' ).click();
    $( '.voucher' ).html( 'Discount Applied!' ).addClass( 'rightie' );
    $( '#voucher_form' ).hide();
}
else if ( $( '.woocommerce-error' ).html() !==undefined ) {
$( '.voucher' ).html( 'Discount Applied!' ).addClass( 'rightie' );
    $( '#voucher_form' ).hide();
    $( '#cart_link' ).click();
    }
    else {
 $( '#voucher_form' ).show();
 }
/*-----------------------Handle checkout-----------------
--------------------------------------------------------------*/
$( '#createaccount' ).prop('checked', true);
$( '.order-total' ).find( '.amount' ).html( '$' + adjPrice ); 
 $( '.order-total' ).find( '.amount' ).html( '' ); 
           var data = {'action': 'fs_get_cart_total'};
	jQuery.post(ajaxurl, data, function(response) {
           $( '.order-total' ).find( '.amount' ).html( response ); 
	});
$( '.next-steps-button' ).on( 'click', function() {
        $( '#loading-back' ).show();
        $( '#loader' ).show();
        });
});