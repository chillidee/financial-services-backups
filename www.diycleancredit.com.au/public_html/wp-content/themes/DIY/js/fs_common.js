jQuery( document ).ready(function($) {
function getParameterByName(name) {
    var match = RegExp('[?&]' + name + '=([^&]*)').exec(window.location.search);
    return match && decodeURIComponent(match[1].replace(/\+/g, ' '));
}
var doc_width = $( window ).width(),
    menu_open = 0;
if(  getParameterByName('first') == '0' && doc_width < 992 ) {
$( '.col-sidebar' ).animate({
    'margin-left': "+=200"
  }, 500, 'swing', function() {
       $( '.col-sidebar' ).animate({
           'margin-left': "-=200"
       }, 800, 'swing');
  });
window.history.pushState('',document.title,'https://www.diy.cleancredit.com.au/getting-started');
}
$( '#pull_out' ).on( 'click',function() {
   if ( menu_open == 0 ) {
       $( '.col-sidebar' ).animate({
          'margin-left': '0'
       },500, function() {
       menu_open = 1;
       }
        );
    }
    else {
     $( '.col-sidebar' ).animate({
          'margin-left': '-90%'
       },500, function() {
                 menu_open = 0;
                 }
         );
       }
});
   
    $( '#loading-back-2' ).fadeOut();
    $( '#loader' ).fadeOut();
open = 0;
$( '#hamburger' ).on( 'click', function() {
    if ( open == 0 ) {
        open = 1;
        $( '#menu-header-menu' ).css({ 
             height: '280px',
             overflow: 'visible'
 });
        $( '.login a' ).show();
}
else {
     open = 0;
     $( '#menu-header-menu' ).css({ 
             height: '0px',
             overflow: 'hidden'
 });
     $( '.login a' ).hide();
        }   
});
$( '.expired.account_image_div a' ).on( 'click', function(e) {
    e.preventDefault();
    $( '#loading-back-2' ).show();
    $( '#loader' ).show();
   location.assign("/packages");
});
    $( '.account_image' ).on( 'click', function() {
    $( '#loading-back-2' ).show();
    $( '#loader' ).show();
    });
$( '.renew' ).on( 'click', function(e) {
    e.preventDefault();
    $( '#loading-back-2' ).show();
    $( '#loader' ).show();
   location.assign("/packages");
});
    $( '.account_image' ).on( 'click', function() {
    $( '#loading-back-2' ).show();
    $( '#loader' ).show();
    });
//Simple sticky header
fading =0;
 function bindScroll(){
   if ( fading == 0 ){
       fading = 1;
       if($(window).scrollTop() > 100) {
           $( '#sticky-header' ).fadeIn();
       }
   if($(window).scrollTop() < 50) {
       $( '#sticky-header' ).fadeOut();
   }
   fading = 0
   }
}
$(window).bind('scroll', bindScroll);
        var open = 0,
        inner_open = 0;
$( '.side_package_header' ).on( 'click', function() {
    $( '.post_inner' ).hide();
    inner_open = 0;
    if ($(this).next().is(":visible")) {
          $( '.category_inner' ).hide();
      }
    else {
          $( '.category_inner' ).hide();
          $( this ).next().show();
      }
   });
$( '.post_parent' ).on( 'click', function() {
   if ( inner_open == 0 ) {
    $( '.post_inner' ).hide();
    $( this ).next().show();
    inner_open = 1;
    }
    else {
    $( '.post_inner' ).hide();
    inner_open = 0;
    }
        });
var package = '.' + getParameterByName('package');
$( '.current_link' ).parents(package).show();
/*----------------Back and prev buttons-
--------------------------------------------------*/
var current = $( package + '.current_link' ).attr("data-id");
current_el = $( package + '.current_link' );
   if ( current == undefined ) { //Get started page
      $( '#back' ).hide();
      $( '#next' ).on( 'click',function() {
              $( '#loading-back-2' ).show();
              $( '#loader' ).show();
          $( "a.post_link" ).first()[0].click();
          });
      }
   else if ( current == $( "a.post_link" ).first().attr("data-id" ) ){ //First page
   $( '#back' ).hide();
   $( '#next' ).on( 'click',function() {
              $( '#loading-back-2' ).show();
              $( '#loader' ).show();
         current_el.next()[0].click();
          });
       }
   else if ( current_el.attr("data-id") == $( "a.post_link" ).last().attr("data-id" ) ){ //Last page
   $( '#next' ).hide();
       };
   $( '#back' ).on( 'click',function() {
      $( '#loading-back-2' ).show();
      $( '#loader' ).show();
      if (  current_el.prev()[0] != undefined ) {
              current_el.prev()[0].click();
      }
      else {
         if ( current_el.parent().prev().prev().children().last()[0] != undefined ) {
              current_el.parent().prev().prev().children().last()[0].click();
         }
         else {
         current_el.parent().prev().parent().prev().prev().children('.post_inner').children().last()[0].click();
      }
      }
         
          });
      $( '#next' ).on( 'click',function() {
              $( '#loading-back-2' ).show();
              $( '#loader' ).show();
      if (  current_el.next()[0] != undefined ) {
         $( package + '.current_link' ).next()[0].click();
      }
      else {
          if ( current_el.parent().next().next().children().first()[0] != undefined ) {
              current_el.parent().next().next().children().first()[0].click();
         }
         else {
        current_el.parent().next().parent().next().next().children('.post_inner').children().first()[0].click();
      }
      }
         
          });
$( '.next-steps-button' ).on( 'click', function() {
     $( '#loading-back-2' ).show();
     $( '#loader' ).show();
  });
$( '.question' ).on( 'click',function() {
    if ( $(this).next().is(":visible") ) {
        $( '.answer' ).hide();
        $( '.question' ).css({ 'background-position': 'right -33px' });
        }
        else {
    $( '.answer' ).hide();
    $( '.question' ).css({ 'background-position': 'right -33px' });
    $( this ).css({ 'background-position': 'right 18px' });
    $( this ).next().show();
    }
    });
        

});