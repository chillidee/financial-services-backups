jQuery(function($) {
document.getElementById('fs_shadow_video').addEventListener('ended',myHandler,false);
    function myHandler(e) {
        $( '#shadow_box' ).fadeOut();
    }
$( '.grey_button' ).on( 'click',function() {
   $( '#shadow_box' ).fadeIn();
   ga('send','event','Video Link','play','Top video');
   setTimeout(function(){
   $('#fs_shadow_video').get(0).play();
    }, 500);
   });
$( '#shadow_box' ).on( 'click',function() {
    $('#fs_shadow_video').get(0).pause();
    $( '#shadow_box' ).fadeOut();
    });
//Play the home page video on click
var play = false;
$( '#diy_video' ).on( 'click',function() {
    $( this ).addClass( 'playing' );
    if ( play == false ) {
      $('#fs_diy_video').get(0).play();
      $( '.video_header' ).addClass('white');
      ga('send','event','Video Link','play','Content video');
      play = true;
    }
    else {
        $( this ).removeClass( 'playing' );
        $('#fs_diy_video').get(0).pause();
        $( '.video_header' ).removeClass('white');
        play = false;
    }
});
// Make the page scroll to the anchors
  $('a[href*=#]:not([href=#])').click(function() {
  if ( $( window ).width() < 880 ) {
             $( '#menu-header-menu' ).css({ 
             height: '0px',
             overflow: 'hidden'
 });
 $( '.login a' ).hide();
 open = 0
 }
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
      if (target.length) {
        $('html,body').animate({
          scrollTop: target.offset().top
        }, 1000);
        return false;
      }

    }
  });
});
jQuery( document ).ready(function($) {
//Simple sticky header
if ( $( window ).width() > 580 ) {
fading =0;
 function bindScroll(){
   if ( fading == 0 ){
       fading = 1;
       if($(window).scrollTop() > 100) {
           $( '#sticky-header' ).fadeIn();
       }
   if($(window).scrollTop() < 10) {
       $( '#sticky-header' ).fadeOut();
   }
   fading = 0
   }
}
$(window).bind('scroll', bindScroll);
}
});