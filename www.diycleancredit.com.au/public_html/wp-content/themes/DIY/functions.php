<?php
//Load files for our pdf custom post type
require_once ( get_template_directory() . '/extras/settings.php' );
require_once ( get_template_directory() . '/extras/custom-mailer.php' );
/*
 * Create class to handle attaching the logo in emails
 */
class logo {
        public function attachLogo() {
              $file = get_template_directory() .'/logo.png'; //phpmailer will load this file
              $uid = 'logo_attachment'; //will map it to this UID
              $name = 'logo.png'; //this will be the file name for the attachment  
              global $phpmailer; 
              if (is_file($file)) {  
        $phpmailer->AddEmbeddedImage($file, $uid, $name); 
         }
      }
}
$logo = new logo;
add_action('phpmailer_init', array( 'logo', 'attachLogo' ));

add_filter( 'woocommerce_email_attachments', 'attach_terms_conditions_pdf_to_email', 10, 3);

function attach_terms_conditions_pdf_to_email ( $attachments, $status , $order ) {
	$allowed_statuses = array( 'new_order', 'customer_invoice', 'customer_processing_order', 'customer_completed_order' );

	if( isset( $status ) && in_array ( $status, $allowed_statuses ) ) {
		$your_pdf_path = get_template_directory() . '/logo.png';
		$attachments[] = $pdf_path;
	}

	return $attachments;
}
/**
 *  Add our custom field for package length to the order on successful payment 
 *
 */
 function your_function($user_login, $user) {
  $user_id = $user->ID;
  global $woocommerce;
  date_default_timezone_set('Australia/Sydney');
  $today = strtotime ( date('d-m-Y'));
  $ordered_product_array = get_user_meta( $user_id, 'packages', true );
  foreach ( $ordered_product_array[0] as $product => $date ) {
       $object = get_page_by_title( $product, OBJECT, 'product' );
       $id = $object->ID;
       $ordered_products[$id] = $date;
  }
  foreach ( $ordered_products as $product => $date ) {
       if ( $date > $today ) {
		$cart = WC()->instance()->cart;
                $cart_id = $cart->generate_cart_id($product);
                $cart_item_id = $cart->find_product_in_cart($cart_id);

    if($cart_item_id){
       $cart->set_quantity($cart_item_id,0);
    }
		} 
}
}
add_action('wp_login', 'your_function', 10, 2);

function fs_add_time( $order_id ) {
  date_default_timezone_set('Australia/Sydney');
  $date = strtotime ( date('d-m-Y'));
  $days = get_option( 'fs_days', false );
  $expires = $date  + ( $days * 24 * 60 * 60 );
  add_post_meta($order_id, '_expires', $expires);
  $user = get_post_meta( $order_id, '_customer_user', false );
  global $wpdb;
  $items = $wpdb->get_results("SELECT * FROM " . $wpdb->prefix  . "woocommerce_order_items WHERE order_id = " . $order_id, ARRAY_A );
  $item_array = array();
  $item_array = get_user_meta( $user[0], 'packages', true ); 
  foreach ( $items as $item) {
     if ($item['order_item_name'] == 'Utility Defaults' || $item['order_item_name'] == 'Credit Defaults' ||$item['order_item_name'] == 'Phone Defaults') {
      $key = $item['order_item_name'];
      $item_array[0][$key] = $expires;
      }
   }
  update_user_meta( $user[0], 'packages', $item_array );   
}
//Change this hook once payment gateway is implemented
//Paypal payments procesed correctly, but returning as on-hold
 add_action( 'woocommerce_order_status_on-hold', 'fs_add_time');
 add_action( 'woocommerce_order_status_processing', 'fs_add_time');
 add_action( 'woocommerce_order_status_completed', 'fs_add_time');

/**
 * Register our sidebars and widgetized areas.
 *
 */
function arphabet_widgets_init() {

	register_sidebar( array(
		'name'          => 'Left Sidebar Posts',
		'id'            => 'left_sidebar_1',
		'before_widget' => '<div>',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="rounded">',
		'after_title'   => '</h2>',
	) );

}
add_action( 'widgets_init', 'arphabet_widgets_init' );
/**
 * Get all Products Successfully Ordered by the user
 * @return bool|array false if no products otherwise array of product ids
 */
function fs_get_all_products_ordered_by_user() {
    $orders = fs_get_all_user_orders(get_current_user_id(), 'completed');
    if(empty($orders)) {
        return false;
    }
    $order_list = '(' . join(',', $orders) . ')';//let us make a list for query
    //so, we have all the orders made by this user that were completed.
    //we need to find the products in these orders and make sure they are downloadable.
    global $wpdb;
    $query_select_order_items = "SELECT order_item_id as id FROM {$wpdb->prefix}woocommerce_order_items WHERE order_id IN {$order_list}";
    $query_select_product_ids = "SELECT meta_value as product_id FROM {$wpdb->prefix}woocommerce_order_itemmeta WHERE meta_key=%s AND order_item_id IN ($query_select_order_items)";
    $products = $wpdb->get_col($wpdb->prepare($query_select_product_ids, '_product_id'));
    return $products;
}

/**
 * Returns all the orders made by the user
 * @param int $user_id
 * @param string $status (completed|processing|canceled|on-hold etc)
 * @return array of order ids
 */
function fs_get_all_user_orders($user_id, $status = 'completed') {
    if(!$user_id) {
        return false;
    }
    $args = array(
        'numberposts' => -1,
        'meta_key' => '_customer_user',
        'meta_value' => $user_id,
        'post_type' => 'shop_order',
       // 'post_status' => 'publish'
    );
    $posts = get_posts($args);
    //get the post ids as order ids
    return wp_list_pluck($posts, 'ID');
}
function wpcustom_inspect_scripts_and_styles() {
    global $wp_scripts;
    global $wp_styles;

    // Runs through the queue scripts
    foreach( $wp_scripts->queue as $handle ) :
        $scripts_list .= $handle . ' | ';
    endforeach;

    // Runs through the queue styles
    foreach( $wp_styles->queue as $handle ) :
        $styles_list .= $handle . ' | ';
    endforeach;

    printf('Scripts: %1$s  Styles: %2$s', 
    $scripts_list, 
    $styles_list);
}

//add_action( 'wp_print_scripts', 'wpcustom_inspect_scripts_and_styles' );
add_action('after_setup_theme', 'remove_admin_bar');

function remove_admin_bar() {
if (!current_user_can('administrator') && !is_admin()) {
  show_admin_bar(false);
}
}
/*--------------Add products to cart with ajax---
----------------------------------------------------*/
add_action( 'wp_ajax_add_product_to_cart', 'add_product_to_cart' );
add_action( 'wp_ajax_nopriv_add_product_to_cart', 'add_product_to_cart' );
add_action( 'wp_ajax_remove_product_from_cart', 'remove_product_from_cart' );
add_action( 'wp_ajax_fs_change_product_price', 'fs_change_product_price' );
add_action( 'wp_ajax_fs_change_days', 'fs_change_days' );
add_action( 'wp_ajax_nopriv_remove_product_from_cart', 'remove_product_from_cart' );
add_action( 'woocommerce_before_calculate_totals', 'add_custom_price' );
add_action( 'wp_ajax_get_checkout', 'get_checkout' );
add_action( 'wp_ajax_nopriv_get_checkout', 'get_checkout' );
add_action( 'wp_ajax_nopriv_fs_get_cart_total', 'fs_get_cart_total' );
add_action( 'wp_ajax_fs_get_cart_total', 'fs_get_cart_total' );
/*--------------------Customize woocommerce checkout fields------
-----------------------------------------------------------------------*/
// Hook in
add_filter( 'woocommerce_default_address_fields' , 'custom_override_default_address_fields' );

// Our hooked in function - $address_fields is passed via the filter!
function custom_override_default_address_fields( $address_fields ) {
     $address_fields['address_1']['required'] = false;
     $address_fields['company']['required'] = false;
     $address_fields['state']['required'] = false;
     $address_fields['postcode']['required'] = false;
     $address_fields['city']['required'] = false;
     $address_fields['country']['required'] = false;

     return $address_fields;
}
add_filter( 'woocommerce_checkout_fields' , 'custom_override_checkout_fields' );
// Our hooked in function - $fields is passed via the filter!
function custom_override_checkout_fields( $fields ) {
     unset($fields['billing']['billing_address_1']);
     unset($fields['billing']['billing_address_2']);
     unset($fields['billing']['billing_city']);
     unset($fields['billing']['billing_postcode']);
     unset($fields['billing']['billing_country']);
     unset($fields['billing']['billing_state']);
     unset($fields['billing']['billing_company']);
     unset($fields['billing']['billing_phone']);
     unset($fields['shipping']['shipping_address_1']);
     unset($fields['shipping']['shipping_address_2']);
     unset($fields['shipping']['shipping_city']);
     unset($fields['shipping']['shipping_postcode']);
     unset($fields['shipping']['shipping_country']);
     unset($fields['shipping']['shipping_state']);
     unset($fields['shipping']['shipping_company']);
     unset($fields['shipping']['shipping_first_name']);
     unset($fields['shipping']['shipping_last_name']);
      unset($fields['order']['order_comments']);

     return $fields;
}
function get_checkout() {
 echo do_shortcode( '[woocommerce_checkout]' ); 
 echo "<button class ='grey_kit mob_only back-to-packages'>Back to Packages</button>";
 die();
}
function setup_theme_admin_menus() {
    add_menu_page( 'Customise', 'Customise', 'manage_options', 'customise', 'theme_front_page_settings', get_template_directory_uri() . '/images/favicon.ico', 13.2 );
}
function fs_change_product_price() {
  if ( wp_verify_nonce( $_POST['fs_noncename'], plugin_basename(__FILE__) )) {
       update_option( 'fs_price_0', $_POST['price-0'] );
       update_option( 'fs_price_1', $_POST['price-1'] );
       update_option( 'fs_price_2', $_POST['price-2'] );
       echo 'Prices';
       die();
 }
 }
function fs_change_days(){
  if ( wp_verify_nonce( $_POST['fs_noncename'], plugin_basename(__FILE__) )) {
       update_option( 'fs_days', $_POST['fs_days'] );
       update_option( 'fs_days_label', $_POST['fs_days_label'] );
       update_option( 'fs_expires_label', $_POST['fs_expires_label'] );
       update_option( 'fs_expired_package_price', $_POST['fs_expired_package_price'] );
       echo 'Days';
       die();
 }
 }
function theme_front_page_settings() {
    echo "<div style ='background-color: #fff; font-size: 18px; padding: 10px 100px 100px; margin-top: 60px;'><div class ='wrap'>
            <h1>Customise options for DIY clean credit Packages</h1>
            <p class ='fs_info'>
             The following overrides all prices set in the woocommerce product sections</p>
             <h2>Package Prices</h2>
             ";
             global $post;	
	     // Noncename needed to verify where the data originated
	     echo "<form class ='fs_update_price_form'>
	          <label class ='fs_label'>Full Price</label><input type ='text' value ='" . get_option( 'fs_price_0', false ) . "' name ='price-0'><br>
	          <label class ='fs_label'>2nd Package</label><input type ='text' value ='" . get_option( 'fs_price_1', false ) . "' name ='price-1'><br> 
	          <label class ='fs_label'>3rd Package</label><input type ='text' value ='" . get_option( 'fs_price_2', false ) . "' name ='price-2'><br>
	          <input type ='hidden' name ='action' value ='fs_change_product_price'> 
	     <input type='hidden' name='fs_noncename' id='fs_noncename' value='" . 
	wp_create_nonce( plugin_basename(__FILE__) ) . "' />
	<input type ='submit' value ='Change Price'>
	</form>
	<h2>Time that packages are valid for after payment</h2>
	<form class ='fs_update_price_form'>
	          <label class ='fs_label'>Days</label><input type ='text' value ='" . get_option( 'fs_days', false ) . "' name ='fs_days'><br>
	          <label class ='fs_label'>Text- Packages page</label><input type ='text' value ='" . get_option( 'fs_days_label', false ) . "' name ='fs_days_label'><br>
	          <label class ='fs_label'>Text- Accounts page</label><input type ='text' value ='" . get_option( 'fs_expires_label', false ) . "' name ='fs_expires_label'><br>
	          <label class ='fs_label'>Expired package price</label><input type ='text' value ='" . get_option( 'fs_expired_package_price', false ) . "' name ='fs_expired_package_price'><br>
	          <input type ='hidden' name ='action' value ='fs_change_days'> 
	     <input type='hidden' name='fs_noncename' id='fs_noncename' value='" . 
	wp_create_nonce( plugin_basename(__FILE__) ) . "' />
	<input type ='submit' value ='Change days'>
	</form>
            </div></div>
            <script>
            jQuery( document ).ready(function($) {
                 $( '.fs_update_price_form' ).on( 'submit', function(e){
                      var data = $( this ).serialize();
                      	$.post(ajaxurl, data, function(response) {
                      	  alert( response + ' have been changed.' );
                 })
                 e.preventDefault();
              });
          });
      </script>
      <style>
      .fs_info { font-size: 18px;
      }
      h1 {
       color: #555;
       border-bottom: 1px solid #2ca7be;
       padding-bottom: 15px;
       font-size: 24px!important;
       }
      h2 {
        margin-top: 45px!important;
        color: #2e74a7;
        margin-bottom:15px!important;
        font-size: 18px;
        }
      label.fs_label {
       display: block;
       float: left;
       width: 200px;
       }
       input[type=submit] {
         color: #fff;
         background-color: #2ca7be;
         padding: 4px 24px;
         border-radius: 3px;
         border: none;
         margin-top: 20px;
         float: none;
       }
       input.double-line  {
        margin: 5px 0 15px;
        }
       </style>
      ";
   } 
// This tells WordPress to call the function named "setup_theme_admin_menus"
// when it's time to create the menu pages.
add_action("admin_menu", "setup_theme_admin_menus");

function add_custom_price( $cart_object ) {
    $price_1 = get_option( 'fs_price_0', false ); // This will be your custome price  
    $price_2 = get_option( 'fs_price_1', false );
    $price_3 = get_option( 'fs_price_2', false );
    $expired_price = get_option( 'fs_expired_package_price', false );
    $i = 0;
    $packages = get_user_meta( get_current_user_id(), 'packages', true );
    foreach ( $cart_object->cart_contents as $key => $value ) {
     if ( array_key_exists( $value['data']->post->post_title, $packages[0] )) {
           $value['data']->price = $expired_price;
     }
     else {
    if ( $i == 0 ) {
        $value['data']->price = $price_1;
        $i++;
        }
        else if ( $i == 1 ) {
        $value['data']->price = $price_2;
        $i++;
        }
        else {
        $value['data']->price = $price_3;
        }
      }
    }
}
function fs_get_cart_total() {
    global $woocommerce;
    echo $woocommerce->cart->get_cart_total();
    die();
    }
function add_product_to_cart() {
                global $woocommerce;
		$product_id = $_POST['id'];
		$found = false;
		//check if product already in cart
			//check if product already in cart
			if ( sizeof( WC()->cart->get_cart() ) > 0 ) {
			foreach ( WC()->cart->get_cart() as $cart_item_key => $values ) {
				$_product = $values['data'];
				if ( $_product->id == $product_id )
					$found = true;
			}
			// if product not found, add it
			if ( ! $found ){
				WC()->cart->add_to_cart( $product_id );
                        }
		} else {
		       $woocommerce->session->set_customer_session_cookie(true);
			// if no products in cart, add it
			WC()->cart->add_to_cart( $product_id );
		}

 foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
			$_product     = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
			if ( $_product->id == $product_id ) { 
			$price = $_product->price;
			ob_start(); ?>
			<div id ='product-<?php echo $_product->id; ?>'> 
	                <div class ='col-md-3'>
	                <?php
 $args = array(
   'post_type' => 'attachment',
   'numberposts' => -1,
   'post_status' => null,
   'post_parent' => $_product->id
  );

  $attachments = get_posts( $args );
     if ( $attachments ) {
        foreach ( $attachments as $attachment ) {
           echo '<div class ="blue-image">';
           echo wp_get_attachment_image( $attachment->ID, 'thumbnail' );
           echo '</div>';
          }
     }
?>      </div>
	<div class = 'col-md-9'>
	                       <h3 class ='cart_blue'><?php echo $_product->post->post_title;?></h3>
	                       <span class="price cart_span">$<?php echo $_product->price;?></span>
	                       <div class ='product_content'><?php echo $_product->post->post_content;?></div>
	                       <a class ='remove_item' data-id ='<?php echo $_product->id; ?>'>Remove Item</a>
	 
	                       </div><div class ='clear'></div>
	                       <div class ='dark_line'></div>
	                       </div>
	                       <?php
			}
		}
	$pass = array();
       $pass['html'] = ob_get_clean();
       $pass['price'] = $price;
       echo json_encode($pass);

	die();
}
function remove_product_from_cart() {
    global $woocommerce;
    $id = $_POST['id'];
    $cart = WC()->instance()->cart;
    $cart_id = $cart->generate_cart_id($id);
    $cart_item_id = $cart->find_product_in_cart($cart_id);

    if($cart_item_id){
       $cart->set_quantity($cart_item_id,0);
    }
    echo $cart->cart_contents_total;
    die();
}
// Custom metaboxes for adding Faqs and testimonials for the front page
$post_id = $_GET['post'] ? $_GET['post'] : $_POST['post_ID'] ;
	if ($post_id == '5') {
	add_action( 'add_meta_boxes', 'fs_add_metaboxes' );
	}
add_action( 'after_setup_theme', 'woocommerce_support' );
function woocommerce_support() {
    add_theme_support( 'woocommerce' );
}
function fs_add_metaboxes() {
	add_meta_box('fs_faqs', 'Top Section', 'fs_faqs', 'page', 'normal', 'high');
	add_meta_box('fs_testimonials', 'Middle Section', 'fs_testimonials', 'page', 'normal', 'high');
	add_meta_box('fs_section_1', 'Bottom Section', 'fs_section_1', 'page', 'normal', 'high');
	add_meta_box('fs_videos', 'DIY Video', 'fs_videos', 'page', 'normal', 'high');
}
function fs_section_1() {
       global $post;	
	// Noncename needed to verify where the data originated
	echo '<input type="hidden" name="eventmeta_noncename" id="eventmeta_noncename" value="' . 
	wp_create_nonce( plugin_basename(__FILE__) ) . '" />';		
	// Get the location data if its already been entered
	$faqs = get_post_meta($post->ID, '_fs_faqs', true);	
	// Echo out the field
	echo ' 
	<textarea style ="width: 1000px; height:300px;" name ="fs_section1_content">' . get_option( 'fs_section1_content', false ) . '</textarea>
	';
	}
function fs_videos() {
       global $post;	
	// Get the location data if its already been entered
	// Echo out the field
	echo ' 
	 <span class ="fs-video span">MP4</span><input style ="width: 1000px;" name ="fs_video_url" value = "' . get_option( 'fs_video_url', false ) . '"><br>
         <span class ="fs-video span">Mkv</span><input style ="width: 1000px;" name ="fs_video_url_mkv" value = "' . get_option( 'fs_video_url_mkv', false ) . '"><br>
         <span class ="fs-video span">M4V</span><input style ="width: 1000px;" name ="fs_video_url_m4v" value = "' . get_option( 'fs_video_url_m4v', false ) . '"><br>
         <span class ="fs-video span">Webm</span><input style ="width: 1000px;" name ="fs_video_url_webm" value = "' . get_option( 'fs_video_url_webm', false ) . '"><br>
         <span class ="fs-video span">OGG</span><input style ="width: 1000px;" name ="fs_video_url_ogg" value = "' . get_option( 'fs_video_url_ogg', false ) . '">
	';
	}	
function fs_faqs() {
        global $post;	
	// Noncename needed to verify where the data originated
	echo '<input type="hidden" name="eventmeta_noncename" id="eventmeta_noncename" value="' . 
	wp_create_nonce( plugin_basename(__FILE__) ) . '" />';		
	// Get the location data if its already been entered
	$faqs = get_post_meta($post->ID, '_fs_faqs', true);	
	// Echo out the field
	echo '
	<Span>Heading</span><input type ="text" value ="' . get_option( 'faq_header', false ) . '" name ="faq_header"><br><br>
	<input type ="text" style ="margin-bottom: 30px; width: 300px;" name ="fs_faq_head0" value ="' . $faqs[0]['head'] . '">
	<input type ="text" style ="margin-bottom: 30px; width: 300px;" name ="fs_faq_head1" value ="' . $faqs[1]['head'] . '"><br>
	<input type ="text" style ="margin-bottom: 30px; width: 300px;" name ="fs_faq_head2" value ="' . $faqs[2]['head'] . '">
	<input type ="text" style ="margin-bottom: 30px; width: 300px;" name ="fs_faq_head3" value ="' . $faqs[3]['head'] . '"><br>
	<input type ="text" style ="margin-bottom: 30px; width: 300px;" name ="fs_faq_head4" value ="' . $faqs[4]['head'] . '">
	<input type ="text" style ="margin-bottom: 30px; width: 300px;" name ="fs_faq_head5" value ="' . $faqs[5]['head'] . '"><br>
	<div style ="clear: both; float: none;"></div>
	';
}
function fs_testimonials() {
	global $post;	
	// Noncename needed to verify where the data originated
	echo '<input type="hidden" name="fs_noncename" id="fs_noncename" value="' . 
	wp_create_nonce( plugin_basename(__FILE__) ) . '" />';

	$fs_testimonials = get_post_meta($post->ID, 'fs_testimonials', true);
	
	// Echo out the field
	echo '
	<Span>Heading</span><input type ="text" value ="' . $fs_testimonials['heading'] . '" name ="test_header"><br><br>
	<textarea name="fs_test" value="' . $fs_testimonials['content'] . '" class="fs_faq_textarea" style ="width: 1000px; height: 75px; margin-left: 20px;">' . $fs_testimonials['content'] . '</textarea><br>
	<img src = "' . $fs_testimonials['img'] . '" class = "upload_image" id ="img0" alt ="upload an image" style ="width:300pxpx; height:auto; background-color: #ececec; text-align: center;">
	<input type ="hidden" name ="fs_img" value ="' . $fs_testimonials['img'] . '">	
	<script>
jQuery( document ).ready(function($) {
function renderMediaUploader() {
    "use strict";
    var file_frame,
        img_number,
        json;
    var image_data;
    if ( file_frame != undefined ) {
        file_frame.open();
        return;
    } 
    file_frame = wp.media.frames.file_frame = wp.media({
        frame:    "post",
        state:    "insert",
        multiple: false
    });
        file_frame.on( "insert", function() {
    // Read the JSON data returned from the Media Uploader
    json = file_frame.state().get( "selection" ).first().toJSON();
    // First, make sure that we have the URL of an image to display
    if ( 0 > $.trim( json.url.length ) ) {
        return;
    }
    else {
       $( "#img" + window.img_number ).next().val( json.url );
       $( "#img" + window.img_number ).attr( "src", json.url );
       }
 })
 
    // Now display the actual file_frame
    file_frame.open();
}
    var file_frame,
        img_number,
        json;
    "use strict";
 
    $(function() {
        $( ".upload_image" ).on( "click", function( evt ) {
            // Stop the anchors default behavior
            evt.preventDefault();
            // Display the media uploader
            window.img_number = $( this ).attr( "id" ).slice(-1);
            renderMediaUploader();
        });
    });
 
})
</script>';
}
// Save the Metabox Data

function fs_save_meta($post_id, $post) {
	
	// verify this came from the our screen and with proper authorization,
	// because save_post can be triggered at other times
	if ( !wp_verify_nonce( $_POST['fs_noncename'], plugin_basename(__FILE__) )) {
	return $post->ID;
	}

	// Is the user allowed to edit the post or page?
	if ( !current_user_can( 'edit_post', $post->ID ))
		return $post->ID;

	// OK, we're authenticated: we need to find and save the data
	// Build an array to hold our data
	
        update_option( 'faq_header', $_POST['faq_header'] );
        update_option( 'fs_section1_content', $_POST['fs_section1_content'] );
        update_option( 'fs_test', $_POST['fs_test'] );
        update_option( 'fs_content', $_POST['fs_content'] );
        update_option( 'fs_video_url', $_POST['fs_video_url'] );
        update_option( 'fs_video_url_mkv', $_POST['fs_video_url_mkv'] );
        update_option( 'fs_video_url_m4v', $_POST['fs_video_url_m4v'] );
        update_option( 'fs_video_url_ogg', $_POST['fs_video_url_ogg'] );
        update_option( 'fs_video_url_webm', $_POST['fs_video_url_webm'] );
	
	
	$fs_array = array();
	$fs_array['img'] = $_POST['fs_img'];
	$fs_array['heading'] = $_POST['test_header'];
	$fs_array['content'] = $_POST['fs_test'];
	
	$faq_array = array();
	$faq_array[0]['head'] = $_POST['fs_faq_head0'];
	$faq_array[1]['head'] = $_POST['fs_faq_head1'];
	$faq_array[2]['head'] = $_POST['fs_faq_head2'];
	$faq_array[3]['head'] = $_POST['fs_faq_head3'];
	$faq_array[4]['head'] = $_POST['fs_faq_head4'];
	$faq_array[5]['head'] = $_POST['fs_faq_head5'];
	
	
	// Add values of $events_meta as custom fields
	
		if(get_post_meta($post->ID, 'fs_testimonials', FALSE)) { // If the custom field already has a value
			update_post_meta($post->ID, 'fs_testimonials', $fs_array);
		} else { // If the custom field doesn't have a value
			add_post_meta($post->ID, 'fs_testimonials', $fs_array);
		}
		if(get_post_meta($post->ID, '_fs_faqs', FALSE)) { // If the custom field already has a value
			update_post_meta($post->ID, '_fs_faqs', $faq_array);
		} else { // If the custom field doesn't have a value
			add_post_meta($post->ID, '_fs_faqs', $faq_array);
		}
	}

add_action('save_post', 'fs_save_meta', 1, 2); // save the custom fields

function enqueue_scripts_and_styles() {
if (is_admin()) {
wp_enqueue_media();
    }
	global $wp_styles;

	// Adds JavaScript for handling the navigation menu hide-and-show behavior.
	wp_enqueue_script( 'bootstrap', get_template_directory_uri() . '/js/bootstrap.min.js', array(), '1.0', true );
	if ( is_page( 26 ) || is_page( 'packages' )) {
        wp_enqueue_script( 'custom', get_template_directory_uri() . '/js/custom.js', array(), '1.0', true );
        }
        elseif ( is_page( 5 ) ) {
        wp_enqueue_script( 'home', get_template_directory_uri() . '/js/home.js', array(), '1.0', true );
        }
        //Load woocommerce cart scripts on the packages page, so we can implement our cutom setup
   if ( is_page( 'packages' )) {
        wp_enqueue_script( 'woocheck', get_template_directory_uri() . '/js/woo-check.min.js', array(), '1.0', true );
        wp_enqueue_script( 'select2');
        wp_enqueue_style( 'select2');
        }
        else {
        wp_enqueue_script( 'fs_common', get_template_directory_uri() . '/js/fs_common.js', array(), '1.0', true );
        }
	// Loads our main stylesheet.
	wp_enqueue_style( 'bootstrap-css', get_template_directory_uri() . '/css/bootstrap.min.css', array() );
	wp_enqueue_style( 'stylesheet', get_template_directory_uri() . '/style.css');
}
/*
Plugin Name: Remove query strings from static resources
Plugin URI: http://www.yourwpexpert.com/remove-query-strings-from-static-resources-wordpress-plugin/
Description: Remove query strings from static resources like CSS & JS files. This plugin will improve your scores in services like PageSpeed, YSlow, Pingdoom and GTmetrix.
Author: Your WP Expert
Version: 1.3
Author URI: http://www.yourwpexpert.com/
*/

add_action( 'wp_enqueue_scripts', 'enqueue_scripts_and_styles', '500000' );
function _remove_query_strings_1( $src ){	
	$rqs = explode( '?ver', $src );
        return $rqs[0];
}
		if ( is_admin() ) {
// Remove query strings from static resources disabled in admin
}
		else {
add_filter( 'script_loader_src', '_remove_query_strings_1', 15, 1 );
add_filter( 'style_loader_src', '_remove_query_strings_1', 15, 1 );
}
function _remove_query_strings_2( $src ){
	$rqs = explode( '&ver', $src );
        return $rqs[0];
}
		if ( is_admin() ) {
// Remove query strings from static resources disabled in admin
}
		else {
add_filter( 'script_loader_src', '_remove_query_strings_2', 15, 1 );
add_filter( 'style_loader_src', '_remove_query_strings_2', 15, 1 );
}
function register_my_menus() {
  register_nav_menus(
    array(
      'header-menu' => __( 'Header Menu' ),
      'footer-menu' => __( 'Footer Menu' )
    )
  );
}
add_action( 'init', 'register_my_menus' );
/**
 * Featured image
 */
add_theme_support( 'post-thumbnails' ); 
/*Security!*/
function explain_less_login_issues(){ return '<strong>ERROR</strong>: Entered credentials are incorrect.';}
add_filter( 'login_errors', 'explain_less_login_issues' );
define('DISALLOW_FILE_EDIT', true);/*!
/* Hook into the 'init' action so that the function
* Containing our post type registration is not 
* unnecessarily executed. 
*/
add_action( 'init', 'custom_post_type', 0 );
//flush_rewrite_rules( );
function low_prio() {
  return 'low';
  }
add_filter( 'wpseo_metabox_prio', 'low_prio');

//Store login times for all users
add_action('wp_login', 'record_login');
 
//function for setting the last login
function record_login($login) {
   $user = get_userdatabylogin($login);
 
   //add or update the last login value for logged in user
   add_user_meta( $user->ID, 'User_login', current_time('mysql') );
}
?>