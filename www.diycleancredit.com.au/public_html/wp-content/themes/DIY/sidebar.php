<?php
/**
 * The sidebar containing the main widget area
 *
 * If no active widgets are in the sidebar, hide it completely.
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */
?>
<div id ='sidebar'>
  <?php $current_id = get_the_ID(); ?>
  <h3>Packages</h3>
  <?php $ordered_product_array = get_user_meta( get_current_user_id(), 'packages', true );
    date_default_timezone_set('Australia/Sydney');
    $today = strtotime ( date('d-m-Y'));
    $i = 0;
  foreach ( $ordered_product_array[0] as $package => $date ) {
	    if ( $date > $today ) {
	    $class = str_replace ( ' ' , '-' , strtolower( $package ) );
	    $id = get_cat_ID( $package );
	    echo '<div class = "' . $class . ' side_package_header">' . $package . '</div>
	    <div class ="category_inner ' . $class . '">';
$args = array(
	'type'                     => 'post',
	'child_of'                 => $id,
	'parent'                   => '',
	'orderby'                  => 'name',
	'order'                    => 'ASC',
	'hide_empty'               => 0,
	'hierarchical'             => 1,
	'exclude'                  => '',
	'include'                  => '',
	'number'                   => '',
	'taxonomy'                 => 'category',
	'pad_counts'               => false 

);
$categories = get_categories( $args );
foreach( $categories as $category ) { 
$args = array(
     'cat'         => $category->term_id,
     'orderby'     => 'menu_order',
     'order'                    => 'ASC',
     'post_type'        => array('post','PDFs'),
     'posts_per_page'   => 30,
     );
$posts_array = get_posts( $args ); 
      echo "
      <span class ='post_parent " . $class . "'>" . $category->name . "</span>
      <div class ='post_inner " . $class . "'>";
      foreach( $posts_array as $post ) {
            $currentClass = '';
      if ( $post->post_type == 'pdfs' ) {
      if (  $current_id == $post->ID ) { $currentClass = " current_link"; }
          echo "<a data-id = '" . $post->ID . "' class = 'post_link " . $class . $currentClass . "' href ='" .  get_post_meta($post->ID, 'upload', true) . "'>" . $post->post_title . "</a>";
      }
      else {
      if (  $current_id == $post->ID ) { $currentClass = " current_link"; }
          echo "<a data-id = '" . $post->ID . "' class = 'post_link " . $class . $currentClass . "' href ='" .  site_url() . "/" . $post->post_name . "/?package=" . $class . "'>" . $post->post_title . "</a>";
          }}
          	    echo '</div>';
      wp_reset_postdata();
	    }
	       	    echo '</div>';
   } 

   }
 ?>
 </div>
	<?php if ( is_active_sidebar( 'left_sidebar_1' ) ) : ?>
		<div id="secondary" class="widget-area" role="complementary">
			<?php dynamic_sidebar( 'left_sidebar_1' ); ?>			
		</div><!-- #secondary -->
	<?php endif; ?>