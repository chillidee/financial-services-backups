<?php
/**
 * The template for displaying the footer
 *
 * Contains footer content and the closing of the #main and #page div elements.
 */
?>
<footer><div id ='footer'>
  <div id ='top_footer'>
            <div class ='container'>
                 <div class ='row'>
                     <div class ='col-md-3'>
                       <h5>Company</h5>
                       Clean Credit Pty Ltd<br>
                       &#169;Copyright Clean Credit 2017
                     </div>
                     <div class ='col-md-3'>
                       <h5>Address</h5>
                       Australia-wide<br>
                     </div>
                     <div class ='col-md-3'>
                       <h5>Contact Details</h5>
                       Phone: 1300 015 210 <br>
                       Email: info@diycleancredit.com.au<br>
                     </div>
                     <div class ='col-md-3'>
                       <h5>Disclaimer:</h5>
                         This information deals with consumer defaults, Clearouts and Serious Credit Infringements only and does not cover Court Judgments or Court Writs. Purchasing this DIY Credit repair kit is not a guarantee your credit listing/s will be removed from your credit file.    <br>
                         Our terms and conditions, and the information provided in these packs are subject to change at any time due to National credit reporting laws. These include the Privacy Act and the National Consumer Credit protection Act, along with position statements by the ombudsman which are all subject to change.  
                     </div>
                  </div>
             </div>
       </div>
       <div class ='bottom_footer'>
             <div class ='container'>
                 <div class ='row'>
                  <div class ='col-md-12'>
                       <?php wp_nav_menu( array( 'theme_location' => 'footer-menu' ) ); ?>
                  </div>
              </div>
          </div>
     </div>
         </div></footer>
         </div>
<?php wp_footer(); ?>
</body>
</html>