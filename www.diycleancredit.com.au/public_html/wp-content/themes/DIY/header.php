<?php require( 'head.php' );?>
<body>
<script type="text/javascript">
    var ajaxurl = "<?php echo site_url() .'/wp-admin/admin-ajax.php'; ?>";
</script>
<header><div id ='page_header'>
           <div class ='container'>
               <div class ='row'>
                  <div class ='col-md-3 col-md-offset-0 col-sm-8 col-xs-8'>
                      <a href ='/'>
                          <img src ='<?php echo get_template_directory_uri(); ?>/images/logo.png' alt ='Clean credit DIY' class ='full_width' id ='logo'>
                      </a>
                  </div>
                  <div class ='mob_only col-xs-3 col-xs-offset-1'>
                       <img src ='<?php echo get_template_directory_uri(); ?>/images/menu.png' alt ='Menu' id ='hamburger'>
                  </div>
                  <div class ='col-md-9 top-nav'>        
                     <div class ='login'>
                         <?php if ( is_user_logged_in() ) { 
                         global $current_user;
                                get_currentuserinfo(); ?>
                         <ul id ='hover_menu'>
                            <li>
                               <a id ='my-account' href ='/my-account'>My account</a>
                               <ul>
				<li><a href="/getting-started">Get started</a></li>
				<li><a href="/my-account/">My packages</a></li>
				<li><a href ="<?php echo wc_get_endpoint_url( 'customer-logout', '', wc_get_page_permalink( 'myaccount' ) ); ?>">Logout</a></li>
			       </ul>
			   </li>
			 </ul>
                            <?php }
                            else {?>
                            <a href ='/my-account'>Login</a>
                      
                            <?php }?>
                            </div>
                            <?php wp_nav_menu( array( 'theme_location' => 'header-menu' ) ); ?>
                  </div>
                </div>
            </div>
  </div><!--header--></header>
<div id ='sticky-header'>
           <div class ='container'>
               <div class ='row'>
                  <div class ='col-md-3 col-md-offset-0 col-sm-4 col-sm-offset-2 col-xs-8'>
                      <a href ='/'>
                          <img src ='<?php echo get_template_directory_uri(); ?>/images/logo_black.png' alt ='Clean credit DIY' class ='full_width' id ='logo_black'>
                      </a>
                  </div>
                  <div class ='mob_only col-xs-3 col-xs-offset-1'>
                       <img src ='<?php echo get_template_directory_uri(); ?>/images/menu_black.png' alt ='Menu' id ='hamburger_black'>
                  </div>
                  <div class ='col-md-9 top-nav'>
                    <div class ='login'>
                         <?php if ( is_user_logged_in() ) { 
                         global $current_user;
                                get_currentuserinfo(); ?>
                         <ul id ='hover_menu'>
                            <li>
                               <a id ='my-account' href ='/my-account'>My account</a>
                               <ul>
				<li><a href="/getting-started">Get started</a></li>
				<li><a href="/my-account/">My packages</a></li>
				<li><a href ="<?php echo wc_get_endpoint_url( 'customer-logout', '', wc_get_page_permalink( 'myaccount' ) ); ?>">Logout</a></li>
			       </ul>
			   </li>
			 </ul>
                            <?php }
                            else {?>
                            <a href ='/my-account'>Login</a>
                      
                            <?php }?>
                            </div>
                            <?php wp_nav_menu( array( 'theme_location' => 'header-menu' ) ); ?>
                  </div>
</div>
  </div></div><!--sticky-header-->
  <div id ='loading-back'></div><div id ='loading-back-2'></div>
  <div id ='loader'><img src ='<?php echo get_template_directory_uri(); ?>/images/tail-spin.svg' alt ='loading...'></div>
                  