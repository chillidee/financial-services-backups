<?php
add_action( 'admin_enqueue_scripts', 'wp_enqueue_media' );
/*-----------------------------AJAX---------------------------------------*/
// Same handler function...
add_action( 'wp_ajax_my_action', 'my_action_callback' );
function my_action_callback() {
	update_post_meta($_POST['id'], 'status', $_POST['sale_action']);
	echo $_POST['id'] . ' ' . $_POST['sale_action'];
	die();
}

/**
 * Group scripts (js & css)
 */
function je_settings_scripts(){
	wp_enqueue_style('je_theme_settings_css', get_template_directory_uri() . '/extras/css/je_theme_settings.css');
	wp_enqueue_script( 'je_theme_settings_js', get_template_directory_uri() . '/extras/js/je_theme_settings.js', array('jquery'), false, true ); 
	wp_localize_script( 'je_theme_settings_js', 'ajax_object',
            array( 'ajax_url' => admin_url( 'admin-ajax.php' )) );
            // in javascript, object properties are accessed as ajax_object.ajax_url
}
add_action( 'admin_menu', 'je_settings_scripts' );
/*------------------------------Create our pdf post type-----------------------------------------*/
 function create_pdf_upload_posttype() {
    register_post_type( 'PDFs',
        array(
            'labels' => array(
                'name' => 'Pdfs',
                'singular_name' => 'Pdf',
                'add_new' => 'Add New',
                'add_new_item' => 'New Pdf',
                'edit' => 'Edit',
                'edit_item' => 'Edit Pdf',
                'new_item' => 'New Pdf',
                'view' => 'View',
                'view_item' => 'View Pdf',
                'search_items' => 'Search Pdf',
                'not_found' => 'No Pdfs found',
                'not_found_in_trash' => 'No Pdfs found in Trash',
                'parent' => 'Parent Pdfs'
            ),
 
            'public' => true,
            'show_in_menu'       => true,
            'supports' => array( 'title', ),
            'taxonomies' => array('category'), 
            'menu_icon' => get_template_directory_uri() . '/images/pdf.png',
            'has_archive' => true
        )
    );
    flush_rewrite_rules( false );
    }
    add_action( 'init', 'my_cpt_init' );
function my_cpt_init() {
    create_pdf_upload_posttype();
}
// Add to admin_init function
add_filter('manage_edit-PDFs_columns', 'add_new_PDFs_columns');
function add_new_PDFs_columns($gallery_columns) {
    $new_columns['title'] = _x('Catalogue', 'column name');
    $new_columns['language_col'] = __('Language');
    $new_columns['date'] = _x('Date uploaded', 'column name');
 
    return $new_columns;
}
// Add to admin_init function
add_action('manage_PDFs_posts_custom_column', 'manage_PDFs_columns', 10, 2);
 
function manage_PDFs_columns($column_name, $id) {
    global $wpdb;
    if ( $column_name == 'language_col' ){
       echo get_post_meta($id, 'language', true);
    }
} 
/*------------------------------------------------Meta Boxes------------------------------------------------*/ 
add_action( 'add_meta_boxes', 'add_events_metaboxes' );

// Add the Events Meta Boxes
function add_events_metaboxes() {
    add_meta_box('catalogue_language_meta', 'language', 'catalogue_language_meta', 'PDFs', 'side', 'default');
    add_meta_box('catalogue_upload_meta', 'Upload Pdf', 'catalogue_upload_meta', 'PDFs', 'normal', 'default');
}

function catalogue_language_meta() {
    global $post;
    $language = get_post_meta($post->ID, 'language', true);
    echo '<select name= "language" class="widefat" id ="language"/>
                 <option value ="English"' . ($language == 'English' ? 'selected' : '') . '>English</option>
                 <option value ="Chinese"' . ($language == 'Chinese' ? 'selected' : '') . '>Chinese</option>
                 <option value ="Both"' . ($language == 'Both' ? 'selected' : '') . '>Both</option>
          </select>';

}

function catalogue_upload_meta() {
    global $post;
    $upload_value = get_post_meta($post->ID, 'upload', true);
    $src = ($upload_value ? get_template_directory_uri() . "/extras/css/images/uploaded.jpg" : get_template_directory_uri() . "/extras/css/images/upload_pdf.jpg");
    echo "<div id ='modal_back' class ='hidden'></div>
    <div id ='really_delete' class ='hidden'>
<span id ='really_delete_span'>Are you sure you want to delete this pdf?</span>
                   <button id ='cancel_delete' class ='button'>Cancel</button>
                   <button id ='delete_image' class ='button'>Delete</button>
                   <input type ='hidden' value ='' id ='pdf_to_be_deleted'>
          </div>
              <div id ='preview_image_holder'>
              <img src ='" . $src . "' id ='preview' class ='preview_image upload_image_button' alt ='preview your image'>                                             
              <input id='image_id' class='hidden media-upload' name='uploaded_image' type='text' value='image_value' />
              <div class ='meta_text float_right'>
                       <span>File:  </span><span id = 'upload_value'>" . $upload_value . "</span><input type ='hidden' id = 'hidden_field' name ='file_upload' value = '" . $file_upload . "'>
              <div class ='clear'></div>
                              </div>";

}

// Save the Metabox Data
function wpt_save_PDFs_meta($post_id, $post) {
    // verify this came from the our screen and with proper authorization,
    // because save_post can be triggered at other times
   /* if ( !wp_verify_nonce( $_POST['eventmeta_noncename'], plugin_basename(__FILE__) )) {
    return $post->ID;
    }*/
    // Is the user allowed to edit the post or page?
    if ( !current_user_can( 'edit_post', $post->ID ))
        return $post->ID;
       if (($_POST['file_upload'])) {
               update_post_meta($post->ID, 'upload', $_POST['file_upload']);
               };
         update_post_meta($post->ID, 'language', $_POST['language']);
        if($_POST['language'] == '') update_post_meta($post->ID, 'language', 'English'); // Delete if blank
    }
       
add_action('save_post', 'wpt_save_PDFs_meta', 1, 2); // save the custom fields
/*------------------------------------------Video Post type extras-------------------------------------------------------------------------------*/

/*------------------------------------------------Meta Boxes------------------------------------------------*/ 
add_action( 'add_meta_boxes', 'add_videos_metaboxes' );

// Add the videos Meta Boxes
function add_videos_metaboxes() {
    add_meta_box('video_upload_meta', 'Upload Video', 'video_upload_meta', 'videos', 'normal', 'default');
}

function video_upload_meta() {
    global $post;
    $upload_value = get_post_meta($post->ID, 'video_upload', true);
    echo "<input type ='textarea' value ='" . $upload_value . "' name ='video_upload'>";

}

// Save the Metabox Data
function wpt_save_videos_meta($post_id, $post) {
    // verify this came from the our screen and with proper authorization,
    // because save_post can be triggered at other times
   /* if ( !wp_verify_nonce( $_POST['eventmeta_noncename'], plugin_basename(__FILE__) )) {
    return $post->ID;
    }*/
    // Is the user allowed to edit the post or page?
    if ( !current_user_can( 'edit_post', $post->ID ))
        return $post->ID;
       if (($_POST['video_upload'])) {
               update_post_meta($post->ID, 'video_upload', $_POST['video_upload']);
               };
    }
       
add_action('save_post', 'wpt_save_videos_meta', 1, 2); // save the custom fields
/*------------------------------------------Cofigure post table page---------------------*/  
/**
 * Helper function for creating admin messages
 * src: http://www.wprecipes.com/how-to-show-an-urgent-message-in-the-wordpress-admin-area
 *
 * @param (string) $message The message to echo
 * @param (string) $msgclass The message class
 * @return echoes the message
 */
function je_show_msg($message, $msgclass = 'info') {
	echo "<div id='message' class='$msgclass'>$message</div>";
}
/**
 * Callback function for displaying admin messages
 *
 * @return calls je_show_msg()
 */
function je_admin_msgs() {
	
	// check for our settings page - need this in conditional further down
	$je_settings_pg = strpos($_GET['page'], JE_PAGE_BASENAME);
	// collect setting errors/notices: //http://codex.wordpress.org/Function_Reference/get_settings_errors
	$set_errors = get_settings_errors(); 
	
	//display admin message only for the admin to see, only on our settings page and only when setting errors/notices are returned!	
	if(current_user_can ('manage_options') && $je_settings_pg !== FALSE && !empty($set_errors)){

		// have our settings succesfully been updated? 
		if($set_errors[0]['code'] == 'settings_updated' && isset($_GET['settings-updated'])){
			je_show_msg("<p>" . $set_errors[0]['message'] . "</p>", 'updated');
		
		// have errors been found?
		}else{
			// there maybe more than one so run a foreach loop.
			foreach($set_errors as $set_error){
				// set the title attribute to match the error "setting title" - need this in js file
				je_show_msg("<p class='setting-error-message' title='" . $set_error['setting'] . "'>" . $set_error['message'] . "</p>", 'error');
			}
		}
	}
}

// admin messages hook!
add_action('admin_notices', 'je_admin_msgs');
?>