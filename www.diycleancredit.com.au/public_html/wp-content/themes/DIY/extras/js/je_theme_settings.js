jQuery(function($){
jQuery('.image_uploader').on('click', function( event ){

 
  //  event.preventDefault();
    this_el = $(event.target);
 
    // If the media frame already exists, reopen it.
    if ( file_frame ) {
      file_frame.open();
      return;
    }
 
    // Create the media frame.
    file_frame = wp.media.frames.file_frame = wp.media({
      title: jQuery( this ).data( 'uploader_title' ),
      button: {
        text: jQuery( this ).data( 'uploader_button_text' ),
      },
      multiple: false  // Set to true to allow multiple files to be selected
    });
 
    // When an image is selected, run a callback.
    file_frame.on( 'select', function() {
      // We set multiple to false so only get one image from the uploader
      attachment = file_frame.state().get('selection').first().toJSON();
 
      // Do something with attachment.id and/or attachment.url here
      this_el.attr("src", attachment.url); 
      this_el.next().val(attachment.url);
    });
 
    // Finally, open the modal
    file_frame.open();
  });


/*-----------------------------------------------------End media frames------------------------------------------*/

///-------------------------Easy to read error messages
	var error_msg = jQuery("#message p[class='setting-error-message']");
	// look for admin messages with the "setting-error-message" error class
	if (error_msg.length != 0) {
		// get the title
		var error_setting = error_msg.attr('title');
		
		// look for the label with the "for" attribute=setting title and give it an "error" class (style this in the css file!)
		jQuery("label[for='" + error_setting + "']").addClass('error');
		
		// look for the input with id=setting title and add a red border to it.
		jQuery("input[id='" + error_setting + "']").attr('style', 'border-color: red');
	}

//--------------------------Load the media uploader with a custom frame
var file_frame;

 jQuery('.upload_image_button').live('click', function( event ){
 
    event.preventDefault();
 
    // If the media frame already exists, reopen it.
    if ( file_frame ) {
      file_frame.open();
      return;
    }
 
    // Create the media frame.
    file_frame = wp.media.frames.file_frame = wp.media({
      title: jQuery( this ).data( 'uploader_title' ),
      button: {
        text: jQuery( this ).data( 'uploader_button_text' ),
      },
      multiple: false  // Set to true to allow multiple files to be selected
    });
 
    // When an image is selected, run a callback.
    file_frame.on( 'select', function() {
      // We set multiple to false so only get one image from the uploader
      attachment = file_frame.state().get('selection').first().toJSON();
 
      // Do something with attachment.id and/or attachment.url here
      jQuery( '#upload_value' ).text(attachment.url); 
      jQuery( '#hidden_field' ).val(attachment.url);
      jQuery("#preview").attr("src","/wp-content/themes/DIY/extras/css/images/uploaded.jpg");
    });
 
    // Finally, open the modal
    file_frame.open();
  });

/*-----------------------------------------------------End media frames------------------------------------------*/
  function ajaxSendQuoteEmail(e){
$.post( MyAjax.ajaxurl, $( '#email_quote_form' ).serialize(), function( response ) {
              alert(response);
              $( '.loader2' ).hide();
	} );

e.preventDefault();
};

});
  
  	