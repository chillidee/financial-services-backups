<?php require( 'head.php' );?>
<body id ='home'>
<div id ='shadow_box'>
     <video class ='container' id ='fs_shadow_video' width ='100%' height ='auto' controls>
  <source src="<?php echo get_option( 'fs_video_url_webm', false );?>">
  <source src="<?php echo get_option( 'fs_video_url', false );?>">
  <source src="<?php echo get_option( 'fs_video_url_m4v', false );?>" type= "video/mp4">
  <source src="<?php echo get_option( 'fs_video_url_mkv', false );?>">
  <source src="<?php echo get_option( 'fs_video_url_ogg', false );?>">
  
Your browser does not support the video tag.
</video>
</div>
   <div id ='banner'>
 <?php   /*$atts = array(
	'alt'   => 'Diy Clean Credit',
	'class'   => 'banner-img'
);
the_post_thumbnail( 'full', $atts );*/?>
<?php global $post;
      $image_data = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), "full" );?>
      <img class ='banner-img' src ='<?php echo $image_data[0];?>' alt ='DIY clean credit'>
<header><div id ='header'>
           <div class ='container'>
               <div class ='row'>
                  <div class ='col-md-3 col-md-offset-0 col-sm-8 col-xs-8'>
                      <a href ='/'>
                          <img src ='<?php echo get_template_directory_uri(); ?>/images/logo.png' alt ='Clean credit DIY' class ='full_width' id ='logo'>
                      </a>
                  </div>
                  <div class ='mob_only col-xs-3 col-xs-offset-1'>
                       <img src ='<?php echo get_template_directory_uri(); ?>/images/menu.png' alt ='Menu' id ='hamburger'>
                  </div>
                  <div class ='col-md-9 top-nav'>
                   <div class ='login'>
                         <?php if ( is_user_logged_in() ) { 
                         global $current_user;
                                get_currentuserinfo(); ?>
                         <ul id ='hover_menu'>
                            <li>
                               <a id ='my-account' href ='/my-account'>My account</a>
                               <ul>
				<li><a href="/getting-started">Get started</a></li>
				<li><a href="/my-account/">My packages</a></li>
				<li><a href ="<?php echo wc_get_endpoint_url( 'customer-logout', '', wc_get_page_permalink( 'myaccount' ) ); ?>">Logout</a></li>
			       </ul>
			   </li>
			 </ul>
                            <?php }
                            else {?>
                            <a href ='/my-account'>Login</a>
                      
                            <?php }?>
                            </div>
                            <?php wp_nav_menu( array( 'theme_location' => 'header-menu' ) ); ?>
                  </div>
</div>
  </div><!--header--></header>
<div id ='sticky-header'>
           <div class ='container'>
               <div class ='row'>
                  <div class ='col-md-3 col-md-offset-0 col-sm-4 col-sm-offset-2 col-xs-8'>
                      <a href ='/'>
                          <img src ='<?php echo get_template_directory_uri(); ?>/images/logo_black.png' alt ='Clean credit DIY' class ='full_width' id ='logo_black'>
                      </a>
                  </div>
                  <div class ='mob_only col-xs-3 col-xs-offset-1'>
                       <img src ='<?php echo get_template_directory_uri(); ?>/images/menu_black.png' alt ='Menu' id ='hamburger_black'>
                  </div>
                  <div class ='col-md-9 top-nav'>
                                      <div class ='login'>
                         <?php if ( is_user_logged_in() ) { 
                         global $current_user;
                                get_currentuserinfo(); ?>
                         <ul id ='hover_menu'>
                            <li>
                               <a id ='my-account' href ='/my-account'>My account</a>
                               <ul>
				<li><a href="/getting-started">Get started</a></li>
				<li><a href="/my-account/">My packages</a></li>
				<li><a href ="<?php echo wc_get_endpoint_url( 'customer-logout', '', wc_get_page_permalink( 'myaccount' ) ); ?>">Logout</a></li>
			       </ul>
			   </li>
			 </ul>
                            <?php }
                            else {?>
                            <a href ='/my-account'>Login</a>
                            <?php }?>
                            </div>
                            <?php wp_nav_menu( array( 'theme_location' => 'header-menu' ) ); ?>
                  </div>
</div>
  </div></div><!--sticky-header-->
  <div class ='container'>
      <div class ='row'>
  <div id ='banner-middle'>
 <?php if ( have_posts() ) {
	while ( have_posts() ) {
		the_post(); ?>
       <h1><?php echo the_title();?></h1>
       <?php echo the_content();?>
      <?php  }
  };?>
     <div id ='button_centerer'>
        <a href ='/packages'><button class = 'action-button front_page_button' id = '_find_out_more'>SEE OUR PACKAGES</button></a>
        <button class = 'action-button grey_button front_page_button' id = 'main_button2'>Find out more</button>
     </div>
  </div>
  </div></div>