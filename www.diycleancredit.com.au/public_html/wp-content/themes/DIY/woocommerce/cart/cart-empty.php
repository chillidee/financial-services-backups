<?php
/**
 * Empty cart page
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}?>
<div id ='package_nav'>
  <div class ='container'>
     <div id ='centeredmenu'>
        <ul class="nav_p">
           <li role="presentation" class="active"><a id ='packages_link' class ='tab_link' href="#">Packages</a></li>
           <li role="presentation"><a id ='cart_link' class ='tab_link unclick' href="#">Account Details</a></li>
           <li role="presentation"><a id ='checkout_link' class ='tab_link unclick' href="#">Checkout</a></li>
        </ul>
        <div id ='line'></div>
     </div>
</div>
</div>
<div class ='container'>
<?php /*---------------------------------Packages----------
-----------------------------------------------------------*/?>
