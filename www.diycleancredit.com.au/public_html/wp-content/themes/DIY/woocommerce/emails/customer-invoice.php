<?php
/**
 * Customer invoice email
 *
 * @author         WooThemes
 * @package     WooCommerce/Templates/Emails
 * @version     2.2.0
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}

?>

<?php do_action( 'woocommerce_email_header', $email_heading ); ?>

<?php if ( $order->has_status( 'pending' ) ) : ?>

    <p><?php printf( __( 'An order has been created for you on %s. To pay for this order please use the following link: %s', 'woocommerce' ), get_bloginfo( 'name', 'display' ), '<a href="' . esc_url( $order->get_checkout_payment_url() ) . '">' . __( 'pay', 'woocommerce' ) . '</a>' ); ?></p>

<?php endif; ?>
<p><?php echo $order->get_formatted_billing_address(); ?><br>
<?php echo $order->billing_email;?>
</p>
<h2 style ='text-align:center;'></strong>TAX INVOICE</h2>
<div style ='text-align:left; margin-top: 35px; margin-bottom: 35px;'>Clean Credit trading as Clean Credit DIY<br>
<?php printf( '<time datetime="%s">%s</time>', date_i18n( 'c', strtotime( $order->order_date ) ), date_i18n( wc_date_format(), strtotime( $order->order_date ) ) ); ?><br>
ABN: 97 150 106 933<br><br>
</div>
<h2><?php printf( __( 'Invoice #%s', 'woocommerce' ), $order->get_order_number() ); ?></h2>

<table cellspacing="0" cellpadding="6" style="width: 100%; border: 1px solid #eee;" border="1" bordercolor="#eee">
    <thead>
        <tr>
            <th scope="col" colspan="2"  style="text-align:left; border: 1px solid #eee;"><?php _e( 'Product', 'woocommerce' ); ?></th>
            <th scope="col" colspan="2"  style="text-align:left; border: 1px solid #eee;"><?php _e( 'Amount', 'woocommerce' ); ?></th>
        </tr>
    </thead>
    <tbody>
        <?php
            switch ( $order->get_status() ) {
                case "completed" :
                    echo $order->email_order_items_table( $order->is_download_permitted(), false, true );
                break;
                case "processing" :
                    echo $order->email_order_items_table( $order->is_download_permitted(), true, true );
                break;
                default :
                    echo $order->email_order_items_table( $order->is_download_permitted(), true, false );
                break;
            }
        ?>
    </tbody>
    <tfoot>
        <?php
            if ( $totals = $order->get_order_item_totals() ) {
                $i = 0;
                foreach ( $totals as $total ) {
                    $i++;
                    if ( $total['label'] == 'Total:' ) { $total_gst = $total['value']; }
                    else if ( $total['label'] == 'Payment Method:' ) {
                         $payment_method = $total['value'];
                    }
                    else {
                    ?><tr>
                        <th scope="row" colspan="2" style="text-align:left; border: 1px solid #eee; <?php if ( $i == 1 ) echo 'border-top-width: 4px;'; ?>"><?php echo $total['label'];?></th>
                        <td style="text-align:left; border: 1px solid #eee; <?php if ( $i == 1 ) echo 'border-top-width: 4px;'; ?>">
                              <?php 
                                    $number = str_replace ( '(ex. tax)' , '' , $total['value'] );
                                    echo $number; ?></td>
                    </tr><?php
                }
                }
                ?>
                <tr>
                        <th scope="row" colspan="2" style="text-align:left; border: 1px solid #eee;">GST</th>
                        <td style="text-align:left; border: 1px solid #eee; <?php if ( $i == 1 ) echo 'border-top-width: 4px;'; ?>">
                             <?php  
                                    $number = str_replace( '&#36;', '', $total_gst );
                                    $number = str_replace( '.00', '', $number );
                                    preg_match_all('!\d+!', $number, $matches);
                                    $out =  (int)implode('', $matches[0]);
                                    echo '$' . round( $out / 11, 2 );?></td>
                    </tr>
                    <tr>
                        <th scope="row" colspan="2" style="text-align:left; border: 1px solid #eee;">Total Paid:</th>
                        <td style="text-align:left; border: 1px solid #eee; <?php if ( $i == 1 ) echo 'border-top-width: 4px;'; ?>">
                             <?php  
                            echo $total_gst;?>
                    </tr>
                    <?php
            }
        ?>
    </tfoot>
</table>

<br>Amount paid in full by <?php echo $payment_method;?>, thankyou.<br><br>

<?php do_action( 'woocommerce_email_footer' ); ?>