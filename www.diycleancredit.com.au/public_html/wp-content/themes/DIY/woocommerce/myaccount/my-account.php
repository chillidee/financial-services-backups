<?php
/**
 * My Account page
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
wc_print_notices(); ?>
<div class ='row'>
   <div class ='col-md-12'>
      <p class="myaccount_user">
	  <h3 class ="account">Your account</h3><div id ="logout"><a href ="<?php echo wc_get_endpoint_url( 'customer-logout', '', wc_get_page_permalink( 'myaccount' ) ); ?>">Logout</a></div>
      </p>
   </div>
   <div class = 'col-md-12'>
      <p class ='account_notices'>
	<?php	     
	printf( __( 'Welcome to your account!  From here you can view your packages, manage your billing address and <a href="%s">edit your password and account details</a>.', 'woocommerce' ),
		wc_customer_edit_account_url()
	);
	?>
</p>
</div>
   <div class = 'col-md-12'>
      <p class ='account_notices'>
         <h3 class ="account" id ='your_packages_header'>Your packages</h3>
      </p>
   </div>
  <?php 
  date_default_timezone_set('Australia/Sydney');
  $today = strtotime ( date('d-m-Y'));
  $ordered_products = get_user_meta( get_current_user_id(), 'packages', true );
   foreach ( $ordered_products[0] as $product => $date ) {
      $expired = 0;
      $expiredClass = '';
      if ( $today > $date ) {
          $expired = 1;
          $expiredClass = ' expired';
      }
      $object = get_page_by_title( $product, OBJECT, 'product' );
      $id = $object->ID;
      $url = wp_get_attachment_url( get_post_thumbnail_id($id) );?>
         <div class ='col-md-4<?php echo $expiredClass; ?> account_image_div'>
            <a href ='/what-is-credit-repair/?package=<?php echo str_replace ( ' ', '-', strtolower( get_the_title( $id )));?>'>
                <img src="<?php echo $url; ?>" alt="<?php echo get_the_title( $product );?>" class ="account_image">
            </a>
            <h3 class ='account_package_header'><?php echo get_the_title( $id );?></h3>
       <?php if ( $expired != 1 ) { ?>
            <div class ='access'><?php echo get_option( 'fs_expires_label', false ) . ' ' . date( 'd-m-Y', $date );?></div>
       <?php }
       else { ?>
            <div class ='expired_text'>Expired on <?php echo date( 'd-m-Y', $date );?></div>
            <div class ='renew'>Renew Now</div>
       <?php } ?>
             
        </div>
 <?php     }
    if ( count ($ordered_products[0] ) < 3 ) {
        echo '<div id ="go"><a href ="/packages">Get more packages</a></div>';
        };?>

</div>
</div>
</div>
</div>
<?php //do_action( 'woocommerce_before_my_account' ); ?>

<?php //wc_get_template( 'myaccount/my-downloads.php' ); ?>

<?php //wc_get_template( 'myaccount/my-orders.php', array( 'order_count' => $order_count ) ); ?>

<?php //wc_get_template( 'myaccount/my-address.php' ); ?>

<?php do_action( 'woocommerce_after_my_account' ); ?>