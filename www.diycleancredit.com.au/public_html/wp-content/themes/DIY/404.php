<?php
/**
 * The template for displaying 404 pages (Not Found)
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */

get_header(); ?>

	<div class ='container'>
	    <div class ='row'>
					<h2 class="entry-title">Sorry, that isn't quite right</h2>

					<p>Unfortunately we haven't found what you're looking for this time.  Why not <a href ='packages'>Check out our packages instead?</a></p>
					<?php get_search_form(); ?>
					<div class ='spacer'></div>
            </div>
        </div>
        

<?php get_footer(); ?>