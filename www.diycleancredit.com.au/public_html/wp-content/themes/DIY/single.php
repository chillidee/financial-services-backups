<?php
/**
 * The Template for displaying all single posts
 *
 */
 /*-Redirect if user has no packages---*/
  $packages = 0;
  date_default_timezone_set('Australia/Sydney');
  $today = strtotime ( date('d-m-Y'));
  $ordered_product_array = get_user_meta( get_current_user_id(), 'packages', true );
  foreach ( $ordered_product_array[0] as $package => $date ) {
	    if ( $date > $today ) {
	       $packages++;
	       }
	   }
 $errors = array_filter($ordered_product_array);
if (empty($errors) || $packages == 0 ) {
header("Location: " . site_url() . "/my-account/");
die();
}
get_header(); ?>
<div class ='container page single-page'>
    <div class ='row row-same-height'>
        <div class ='col-md-4 col-sidebar'>
        <div id ='pull_out'></div>
         <?php get_sidebar(); ?>
        </div>
        <div class ='col-md-8'>
<?php 
if ( have_posts() ) {
	while ( have_posts() ) {
		the_post(); ?>
		<h1><?php the_title(); ?></h1>
		<?php the_content(); ?>
	<?php } // end while
} // end if
?>
<div id ='back'>Back</div><div id ='next'>Next</div>
</div>
</div>
</div>
<?php get_footer(); ?>