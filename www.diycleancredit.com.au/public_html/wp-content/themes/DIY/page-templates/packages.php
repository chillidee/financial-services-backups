<?php
/**
 * Template Name: Packages
 */
  //$ordered_products = fs_get_all_products_ordered_by_user();
  date_default_timezone_set('Australia/Sydney');
  $today = strtotime ( date('d-m-Y'));
  $ordered_product_array = get_user_meta( get_current_user_id(), 'packages', true );
  $ordered_products = array();
  foreach ( $ordered_product_array[0] as $product => $date ) {
       $object = get_page_by_title( $product, OBJECT, 'product' );
       $id = $object->ID;
       $ordered_products[$id] = $date;
  }
  foreach ( $ordered_products as $product => $date ) {
       if ( $date > $today ) {
		$cart = WC()->instance()->cart;
                $cart_id = $cart->generate_cart_id($product);
                $cart_item_id = $cart->find_product_in_cart($cart_id);

    if($cart_item_id){
       $cart->set_quantity($cart_item_id,0);
    }
  } 
}
get_header(); ?>
<?php if ( isset($_GET['play_video']) ) {?>
   <script>
       jQuery( document ).ready(function($) {
           $( '#shadow_box' ).fadeIn();
           ga('send','event','Video Link','play','Top video');
           setTimeout(function(){
                $('#fs_shadow_video').get(0).play();
           }, 500);
           document.getElementById('fs_shadow_video').addEventListener('ended',myHandler,false);
    function myHandler(e) {
        $( '#shadow_box' ).fadeOut();
    }
       });
    </script>
    <div id ='shadow_box'>
     <video class ='container' id ='fs_shadow_video' width ='100%' height ='auto' controls>
  <source src="<?php echo get_option( 'fs_video_url_webm', false );?>">
  <source src="<?php echo get_option( 'fs_video_url', false );?>">
  <source src="<?php echo get_option( 'fs_video_url_m4v', false );?>" type= "video/mp4">
  <source src="<?php echo get_option( 'fs_video_url_mkv', false );?>">
  <source src="<?php echo get_option( 'fs_video_url_ogg', false );?>">
  
Your browser does not support the video tag.
</video>
</div>
    <?php } ?> 
<div id ='please_purchase'>
</div>
<div id ='package_nav'>
  <div class ='container'>
     <div id ='centeredmenu'>
        <ul class="nav_p">
           <li role="presentation" class="active"><a id ='packages_link' class ='tab_link' href="#">Packages</a></li>
           <li role="presentation"><a id ='cart_link' class ='tab_link unclick' href="#">Account Details</a></li>
           <li role="presentation"><a id ='checkout_link' class ='tab_link unclick' href="#">Checkout</a></li>
        </ul>
        <div id ='line'></div>
     </div>
</div>
</div>
<div class ='container'>
<?php /*---------------------------------Packages----------
-----------------------------------------------------------*/?>
<div id ='packages' class ='tab active'>
    <?php 
if ( have_posts() ) {
	while ( have_posts() ) {
		the_post(); ?>
		<h2 class ='blue'><?php echo the_title();?></h2>
                <div class ='centered'><?php echo the_content();?></div>
                <?php 
	} // end while
} // end if*/
?>
<div class ='row'>
<?php
        $i = 0;
        $products = 0;
        $cart_array = array();
	foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
	    $_product     = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
	    $cart_array[] = $_product->id;
   }
   $args = array(
     'post_type' => 'product',
     'posts_per_page' => 12,
     'order' => 'ASC'
			);
   $loop = new WP_Query( $args );
	if ( $loop->have_posts() ) {
		while ( $loop->have_posts() ) : $loop->the_post();
			global $product, $woocommerce_loop;                  
          if ( empty( $woocommerce_loop['loop'] ) )
	$woocommerce_loop['loop'] = 0;
// Store column count for displaying the grid
if ( empty( $woocommerce_loop['columns'] ) )
	$woocommerce_loop['columns'] = apply_filters( 'loop_shop_columns', 4 );
// Increase loop count
$woocommerce_loop['loop']++;
// Extra post classes
	$in_cart_class = '';
	$kit_class ='';
	$expired = 0;
	$purchased_product ='';
	if ( in_array ( $product->id, $cart_array ) ) {
	    $in_cart_class = ' added_product';
	    $kit_class = ' in_cart_kit';
	    $products ++;
	}
	else {
	$in_cart_class = '';
	$kit_class ='';
	}
	if ( array_key_exists($product->id, $ordered_products )) {
	    if ( $ordered_products[$product->id] < $today ) {
	    $purchased_product = ' expired';
	    $expired = 1;
	    }
	    else {
	    $purchased_product = ' purchased_product';
	}
	}
?>
<div class ='col-md-4'>
      <div class ='product_inner<?php echo $in_cart_class . $purchased_product;?>' id = 'inner-<?php echo $product->id;?>'>
      <?php //do_action( 'woocommerce_before_shop_loop_item' ); ?>
		<?php
			/**
			 * woocommerce_before_shop_loop_item_title hook
			 *
			 * @hooked woocommerce_show_product_loop_sale_flash - 10
			 * @hooked woocommerce_template_loop_product_thumbnail - 10
			 */
			do_action( 'woocommerce_before_shop_loop_item_title' );
		?>
		<h3 class ='blue'><?php the_title(); ?></h3>
		<?php the_content(); 
		if ( $expired == 0 ) { ?>
		<?php if ( $purchased_product == '' ) { ?>
		<button class ='action-button orange_kit remove_item' data-id= '<?php echo $product->id;?>'>Remove from cart</button>
		<button class="action-button orange_kit fix_my mob_only_imp">Proceed to Checkout</button><button class="action-button product_button" id ="open-kit<?php echo $i; ?>">View Product</button>

		<?php } else { ?><span class ='tick'></span><?php } ?>
		<?php }
		else {?>
		<button class="action-button renew_button" id ="renew-Now<?php echo $i; ?>">Renew now</button>
		<?php } ?>		
      </div>
   </div>
   <?php /*-----------------------------------------------Kits------------------
--------------------------------------------------------------------------------------------*/
   $exp_price = '';
   $x = $i;
   if ( $expired == 1 ) { 
       $exp_price = "<span class ='expired_price'>Special renewal price- only $" .  get_option( 'fs_expired_package_price', false ) . "</span>";
        }
   ?>
<div id = 'kit_<?php echo $i;?>' class ='kit<?php echo $kit_class;?>' data-kit-number ='<?php echo $product->id;?>'>
   <span class ='close'></span>
   <div class ='col-md-3'>
         <?php //Get the image
         do_action( 'woocommerce_before_shop_loop_item_title' ); ?>
         <button class ='action-button orange_kit add-to-cart' data-id= '<?php echo $product->id;?>'>Add To Cart</button>
         <button class ='action-button orange_kit remove_item' data-id= '<?php echo $product->id;?>'>Remove from Cart</button>
         <button class ='action-button grey_kit back'>Back to Page</button>
   </div>
   <div class ='col-md-9'>
   <h2 class ='kit_header'><?php echo the_title();?></h2>
                <div><?php  the_meta(); ?></div>
      <div class ='kitmenu'>
          <ul class="nav_p">
              <?php  $atts = array();
             // $k = 0;
              $k = 0;
              foreach ( $product->get_attributes() as $attribrute => $value ) {?>
                    <li role="presentation"<?php if ( $k == 0 ) { echo "class = 'active_kittab'";}?>><a class ='kit_tab_link kit-tab-<?php echo $k;?>' href="#"><?php echo $value[name]; ?></a></li>
                    <?php $atts[$k] = $value;
                    $k++;
                     } ?>
                    <?php if ( $expired == 0 ) { ?> <li role="presentation"><a class ='kit_tab_link kit-tab-1' href="#">Price Options</a></li><?php } ?>
        </ul>
        </div>
        <div class ='kit-box-cont'>
        <?php for ($j = 0; $j <= 0; $j++) { ?>
        <div class ='kit-box<?php if ( $j == 0 ) { echo ' active_tab';}?> kit_box_<?php echo $j; ?>'>
             <?php if( $j == 0 ){ echo $exp_price . '<div class ="days_label">' . get_option( 'fs_days_label', false ) . '</div>'; } ?>
              <?php $atts_a = explode ( ', ', $atts[$j]['value'] );
                   foreach ( $atts_a as $att_v ) { ?>
                      <span class ='tick-span'><?php echo $att_v; ?></span>
                   <?php }?>
                   <?php if ( j ==0 ) { } ?>
        </div>
                <?php   }?>
                              <div class="kit-box kit_box_1">
                    <h4 class ='blue'>Buy more & save!</h4>
                    <span class='kit_price'>1st Package- just <span class ='kit_dollar_span'>$<?php echo get_option( 'fs_price_0', false );?></span></span>
                    <span class ='kit_price'>2nd Package- just <span class ='kit_dollar_span'>$<?php echo get_option( 'fs_price_1', false );?></span></span>
                    <span class ='kit_price'>3rd Package- just <span class ='kit_dollar_span'>$<?php echo get_option( 'fs_price_2', false );?></span></span>
                    <div class = 'spacer'></div>
              </div> 
              <button class ='action-button orange_kit add-to-cart mob_only' data-id= '<?php echo $product->id;?>'>Add To Cart</button>
              <button class ='action-button orange_kit remove_item mob_only' data-id= '<?php echo $product->id;?>'>Remove from Cart</button>
              <button class ='action-button grey_kit back mob_only' id ='mobile_back_button'>Back to Page</button>
     </div>
</div>
</div>
<?php $i++;
			endwhile;
		} else {
			echo __( 'No products found' );
		}
		wp_reset_postdata();
	?>
    </div>
    <button class ='action-button orange_kit fix_my' id ='bottom_checkout'>Proceed to Checkout</button>
</div>
</div>  
<?php /*---------------------------------------------------------------Cart------------------
----------------------------------------------------------------------------------------------------*/?>
<div id ='cart' class ='tab'>
   <div class ='container'>
     <div class ='row'>
      <div class ='col-md-12'>
          <div class ='grey_back'>
             <h2 class ='blue cart_page'>Confirm Details</h2>
                        
   <?php
 global $woocommerce;
 $price = 0;
 foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
			$_product     = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
			$product_id   = apply_filters( 'woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key );
			$price += $cart_item['line_total'];?>
			<div id ='product-<?php echo $_product->id; ?>'> 
	                <div class ='col-md-3'>
	                <?php
 $args = array(
   'post_type' => 'attachment',
   'numberposts' => -1,
   'post_status' => null,
   'post_parent' => $_product->id
  );

  $attachments = get_posts( $args );
     if ( $attachments ) {
        foreach ( $attachments as $attachment ) {
           echo '<div class ="blue-image">';
           echo wp_get_attachment_image( $attachment->ID, 'thumbnail' );
           echo '</div>';
          }
     }
?>      </div>
	<div class = 'col-md-9'>
	                       <h3 class ='cart_blue'><?php echo $_product->post->post_title;?></h3>
	                       <span class="price cart_span">$<?php echo $cart_item['line_subtotal']?></span>
	                        <div class ='product_content'><?php echo $_product->post->post_content;?></div>
	                       <a class ='remove_item' data-id ='<?php echo $_product->id; ?>'>Remove Item</a>
	 
	                       </div><div class ='clear'></div>
	                       <div class ='dark_line'></div>
	                       </div>
	                       <?php 
}
?>
<?php /* Uncomment for coupon line
 <div class ='voucher_line'>
      <span class ='voucher'>Get Cheaper Deals with Discount Codes</span><form class="checkout_coupon" id ='voucher_form' method="post" style="display:none">
<input type="text" name="coupon_code" class="input-text" placeholder="Coupon code" id="coupon_code" value="">
<input type="submit" class="button" name="apply_coupon" value="Apply Coupon">
?>
<div class="clear"></div>
</form>
      <div class ='dark_line'></div>
      </div>*/?>
      <div class ='cart_container'>
           <div class ='cart_total' id ='cart_total_cart'><span style='color:red;text-decoration:line-through; margin-right:40px'>
  <span style='color:white' id ='old-price'></span>
</span>
Total: $<?php echo $price;?> </div>
                 <div id ='savings'>You saved $100</div>   
      </div>
      <button class ='action-button orange_kit' id ='purchase'>Proceed to Checkout</button>
      <button class ='grey_kit mob_only back-to-packages'>Back to Packages</button>
      </div>
      <?php if ( $products != 0 ) {?>
<script>
jQuery( document ).ready(function($) {
   		$( '#fix_my' ).css("display", "block");
		$( '#cart_link' ).removeClass( 'unclick' );
		$( '#checkout_link' ).removeClass( 'unclick' );	
		});
		</script>
		<?php }
?>
      </div>
      </div>
      </div>
      </div>
      <?php /*-----------------------------------------------------------------Checkout--------------------------
      -----------------------------------------------------------------------------------------------------------------*/?>
<div id ='checkout' class ='tab'>
 <div class ='container'>
    <div class ='row'>
      <div class ='col-md-12'>
          <div class ='grey_back' id ='checkout_backer'>
             <h2 class ='blue cart_page'>Checkout</h2>
             <div class ='col-md-10 col-md-offset-1 checkout_backer'> 
                          <?php 
                          $args = array(
    'posts_per_page'   => -1,
    'orderby'          => 'title',
    'order'            => 'asc',
    'post_type'        => 'shop_coupon',
    'post_status'      => 'publish',
);
//Get all our coupons
$coupons = get_posts( $args );
$coupon_array = array();
//Get todays date
$today = date("Y-m-d");
//Loop through the coupons, and if they aren't expired, add them to our coupon array
foreach ( $coupons as $coupon ) {
    $coupon_data = ( get_post_meta ($coupon->ID ));
    //Get all coupons not expired
    if ( $coupon_data['expiry_date'][0] > $today || $coupon_data['expiry_date'][0] == ''){
         $coupon_array[] = $coupon->ID;
         }
        
    } 
//Build a new array that hold the coupon id and amount for each unexpired coupon
$fs_coupons = array();
    foreach ( $coupon_array as $coupon ) {
       $coupon_data = ( get_post_meta ($coupon ));
       $coupon_name = get_post( $coupon );
       $fs_coupons[$coupon_name->post_title] = $coupon_data['coupon_amount'][0];
       };?>
<?php echo do_shortcode( '[woocommerce_checkout]' );?> 
<button class ='grey_kit mob_only back-to-packages'>Back to Packages</button> 
<div class ='clear'></div>    
</div>
</div>
</div>
</div>
</div>
</div>
</div>
<script>
var price = parseInt(<?php echo $price; ?>),
		    products = parseInt(<?php echo $products;?>),
		    products_array = [<?php foreach ( $cart_array as $product ) {?>
		    "<?php echo $product;?>",
		    <?php }?>
		    ],
		    price_1 = <?php echo get_option( 'fs_price_0', false );?>,
		    price_2 = <?php echo get_option( 'fs_price_1', false );?>;
		    </script>
		    <div class ='package-footer'>
<?php get_footer(); ?>
</div>	