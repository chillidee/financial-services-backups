<?php
/**
 * Template Name: Home
 */
get_header('home'); ?>
<?php if ( isset($_GET['play_video']) ) {?>
   <script>
       jQuery( document ).ready(function($) {
           $( '#shadow_box' ).fadeIn();
           ga('send','event','Video Link','play','Top video');
           setTimeout(function(){
                $('#fs_shadow_video').get(0).play();
           }, 500);
       });
    </script>
    <?php } ?>  
<div id ='Packages'>
<div id ='_packages'>
     <div class ='container'>
     <h2><?php echo get_option( 'faq_header', false );?></h2>
        <div class ='row faq_clear_row'>   
        <?php $faqs = get_post_meta('5', '_fs_faqs', FALSE); 
        $i = 0;
        foreach ( $faqs[0] as $faq ) {  
          ?>
          <div class ='col-md-6 faq_box'>   
              <span class ='faq_header'><?php echo $faq['head'];?></span>
              <?php echo $faq['faq'];?> 
          </div>
          <?php
          $i++;
          }?>
          <div class ='clear'></div>
      </div>
   </div>
</div>
<div id ='diy_video'>
    <h2 class ='video_header'>Repair Your Credit- Step by Step Video</h2>
    <video class ='container' id ='fs_diy_video' width ='100%' height ='auto'>
  <source src="<?php echo get_option( 'fs_video_url_webm', false );?>">
  <source src="<?php echo get_option( 'fs_video_url', false );?>">
  <source src="<?php echo get_option( 'fs_video_url_m4v', false );?>" type= "video/mp4">
  <source src="<?php echo get_option( 'fs_video_url_mkv', false );?>">
  <source src="<?php echo get_option( 'fs_video_url_ogg', false );?>">
Your browser does not support the video tag.
</video>
</div>
  <div id ='who_we_are'></div>
     <div id ='middle'>
        <div class ='container'>
           <div class ='row'>
         <?php 
   $fs_testimonials = get_post_meta(5, 'fs_testimonials', true);?>
   <h2 class ='middle-section'><?php echo $fs_testimonials['heading'];?></h2>
   <p class ='middle_content'><?php echo $fs_testimonials['content'];?></p><br>
   <img src ='<?php echo $fs_testimonials['img'];?>' class ='middle_section_image'>
   </div>
  </div>
</div>
</div>
  <div id ='top_section'>
     <div class ='container'>
         <div class ='row'>
                 <div class ='col-md-12 content'><?php echo get_option( 'fs_section1_content', false );?></div>
                 <a href ='/packages'><button class = 'action-button' id = 'main_button2'>Find out more</button></a>
         </div>
     </div>
  </div>   
<?php get_footer(); ?>