<?php
/**
 * The template for displaying Archive pages
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * If you'd like to further customize these archive views, you may create a
 * new template file for each specific one. For example, Twenty Twelve already
 * has tag.php for Tag archives, category.php for Category archives, and
 * author.php for Author archives.
 */

get_header(); ?>
<h1>Portfolio</h1>
<div class ='po-page'>
   <div id ='po-inner'>

<?php
$type = 'photoshoot';
$args=array(
  'post_type' => $type,
  'post_status' => 'publish',
  'posts_per_page' => -1,
  'caller_get_posts'=> 1
);
$my_query = null;
$my_query = new WP_Query($args);
$i = 0;
if( $my_query->have_posts() ) {
  while ($my_query->have_posts()) : $my_query->the_post(); 
   $i++;?>
   <div class ='that_class' id ='something-<?php echo $i; ?>'>
   <a class = 'work-archive-thumb-link col-' href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_post_thumbnail('post-thumbnail', array( 'class'	=> "work-masonry-thumb")); ?>
    <div class ='po_hover_info'>
        <div class ='po_cover_title'>
             <?php echo strtoupper( get_the_title() ); ?>
        </div>
   </div></a>
</div>
            
    <?php
  endwhile;
}
wp_reset_query();  // Restore global post data stomped by the_post().
?>
            </div><!-- #content -->
        </div><!-- #container -->


<?php get_sidebar(); ?>
<?php get_footer(); ?>