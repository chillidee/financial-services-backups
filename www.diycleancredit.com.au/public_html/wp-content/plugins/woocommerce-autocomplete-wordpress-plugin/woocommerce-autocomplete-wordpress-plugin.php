<?php
/*
Plugin Name: Woocommerce Autocomplete WordPress Plugin
Plugin URI: http://www.wpfasthelp.com
Description: keeps track of orders, if any order goes to on-hold status, it will automatically converts it to completed
Author: Fakhri Alsadi
Version: 1.1
Author URI: http://www.wpfasthelp.com
*/

add_action( 'woocommerce_order_status_on-hold', 'wpfasthelp_force_completed' );
 
function wpfasthelp_force_completed( $order_id ) { 
	$order = new WC_Order( $order_id );
	$order->update_status('completed', __( 'Order Completed', 'woocommerce' ));  
}

?>