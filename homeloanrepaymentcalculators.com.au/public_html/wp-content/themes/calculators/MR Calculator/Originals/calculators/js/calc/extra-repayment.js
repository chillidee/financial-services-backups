(function ($) {
  var printMode = false;
  var $calcElement = $('#calc-extra-repayment');
  if (!$calcElement.length) {
    return;
  }
  var result = [];

  /**
   * @returns {{loan_amount: number, loan_term: number, interest_rate: number, repayment_frequency: number, extra_contribution: number, extra_contribution_start: number}}
   */
  function get_inputs() {
    var loan_amount = LoanCalc.removeCommas($("#loan_amount").val());

    var loan_term = parseFloat($("#loan_term").val());
    var interest_rate = parseFloat($("#interest_rate").val());

    var repayment_frequency = $("#repayment_frequency").val();

    var extra_contribution = parseFloat($("#extra_contribution").val());
    var extra_contribution_start = parseFloat($("#extra_contribution_start").val());

    if (isNaN(loan_amount))
      loan_amount = 0;

    if (isNaN(loan_term))
      loan_term = 0;

    if (isNaN(interest_rate))
      interest_rate = 0;

    if (isNaN(extra_contribution))
      extra_contribution = 0;

    if (isNaN(extra_contribution_start))
      extra_contribution_start = 0;

    return {
      loan_amount: loan_amount,
      loan_term: loan_term,
      interest_rate: interest_rate,
      repayment_frequency: repayment_frequency,
      extra_contribution: extra_contribution,
      extra_contribution_start: extra_contribution_start
    };
  }

  function drawChart(arr, scale) {
    var data = new google.visualization.DataTable();

    data.addColumn('number', 'years');
    data.addColumn('number', 'original');
    data.addColumn('number', 'extra');

    for (var i = 0; i < arr.length; i++)
    {
      if (arr[i].original < 0)
        arr[i].original = 0;

      if (arr[i].extra < 0)
        arr[i].extra = 0;

      data.addRow([i/scale, arr[i].original/1000, arr[i].extra/1000]);
    }

    var options = {
      //title: 'Amount Owing',
      //backgroundColor: "#eee",
      colors: ['#E4E4E4','#F7C244'],
      //colors:['#A2C180','#3D7930','#FFC6A5','#FFFF42','#DEF3BD','#00A5C6','#DEBDDE','#000000'],
      hAxis: {title: 'Years'},
      //vAxis: {title: 'Amount Owing', format:'$#K', minValue: 0,, titleTextStyle: { fontSize: 12 }},
      vAxis: {title: 'Amount Owing', format:'$#K'},
      legend: {position: 'bottom'},
      //animation: { duration: 1000, easing: 'out', }
    };

    var chartElement = document.getElementById('chart_div');

    if (printMode) {
      if ($(chartElement).attr('style')) {
        $(chartElement).removeAttr('style');
      }
      printMode = false;
    }

    var chart = new google.visualization.AreaChart(chartElement);
    chart.draw(data, options);
  }

  $("#loan_amount_slider").slider({ min: 0, max: 10000000, step: 140000, range: "min", animate: "true", value: 260000,
    slide: function (event, ui) {
      $("#loan_amount").val(LoanCalc.addCommas(ui.value));
    },
    stop: function (event, ui) {
      $("#loan_amount").val(LoanCalc.addCommas(ui.value));
      redraw();
    }
  });

  $("#interest_rate_slider").slider({ min: 0, max: 25.0, step: 0.25, range: "min", animate: "true", value: 6.58,
    slide: function (event, ui) {
      $("#interest_rate").val(ui.value);
    },
    stop: function (event, ui) {
      $("#interest_rate").val(ui.value);
      redraw();
    }
  });

  $("#loan_term_slider").slider({ min: 0.5, max: 30, step: 0.5, range: "min", animate: "true", value: 30,
    slide: function (event, ui) {
      $("#loan_term").val(ui.value);
    },
    stop: function (event, ui) {
      $("#loan_term").val(ui.value);
      redraw();
    }
  });

  $("#extra_contribution_slider").slider({ min: 0, max: 5000, step: 60, range: "min", animate: "true", value: 100,
    slide: function (event, ui) {
      $("#extra_contribution").val(ui.value);
    },
    stop: function (event, ui) {
      $("#extra_contribution").val(ui.value);
      redraw();
    }
  });

  $("#extra_contribution_start_slider").slider({ min: 0, max: 30, step: 0.5, range: "min", animate: "true", value: 5.0,
    slide: function (event, ui) {
      $("#extra_contribution_start").val(ui.value);
    },
    stop: function (event, ui) {
      $("#extra_contribution_start").val(ui.value);
      redraw();
    }
  });

  $("#loan_amount").change(function () {
    redraw();
    var value = $(this).val();
    $(this).val(LoanCalc.addCommas(value));
    $("#loan_amount_slider").slider({
      value: LoanCalc.removeCommas(value)
    });
  });

  $("#interest_rate").change(function () {
    redraw();
    var value = $(this).val();
    $("#interest_rate_slider").slider({
      value: parseFloat(value)
    });
  });

  $("#loan_term").change(function () {
    redraw();
    $("#loan_term_slider").slider({
      value: parseFloat($("#loan_term").val())
    });
  });

  $("#repayment_frequency").change(function () {
    redraw();
  });

  $("#extra_contribution").change(function () {
    redraw();
    var value = $(this).val();
    $(this).val(LoanCalc.addCommas(value));
    $("#extra_contribution_slider").slider({
      value: parseFloat(value)
    });
  });

  $("#extra_contribution_start").change(function () {
    redraw();
    $("#extra_contribution_start_slider").slider({
      value: parseFloat($("#extra_contribution_start").val())
    });
  });


  function redraw() {
    var inputs = get_inputs();
    var interest = inputs.interest_rate / 100.0;
    var s = 100000000;
    var n;

    if (inputs.repayment_frequency == "weekly") {
      interest = interest / WEEKS_PER_YEAR;
      n = inputs.loan_term * WEEKS_PER_YEAR;
      $("#label1").html("Minimum weekly repayments:");
      $("#label2").html("Increased weekly repayments:");
      s = 52 * inputs.extra_contribution_start;		// start after n weeks
    } else if (inputs.repayment_frequency == "fortnightly") {
      interest = interest / FORTNIGHTS_PER_YEAR;
      n = inputs.loan_term * FORTNIGHTS_PER_YEAR;
      $("#label1").html("Minimum fortnightly repayments:");
      $("#label2").html("Increased fortnightly repayments:");
      s = 26 * inputs.extra_contribution_start;	// start after n fortnights
    } else {
      interest = interest / MONTHS_PER_YEAR;
      n = inputs.loan_term * MONTHS_PER_YEAR;
      $("#label1").html("Minimum monthly repayments:");
      $("#label2").html("Increased monthly repayments:");
      s = 12 * inputs.extra_contribution_start;	// start after n months
    }

    var res = LoanCalc.calculate(inputs.loan_amount, interest, n);
    $("#res1").html("$" + LoanCalc.addCommas(res.repayment.toFixed(2)));


    var pmt1 = LoanCalc.PMT(interest, n, inputs.loan_amount, 0);
    var nper1 = LoanCalc.NPER(interest, pmt1, -inputs.loan_amount, 0);
    var total_interest1 = LoanCalc.totalInterest(interest, nper1, pmt1, inputs.loan_amount);

    var pmt2 = pmt1 + inputs.extra_contribution;
    var q = LoanCalc.total(interest, s, pmt1, inputs.loan_amount);
    var nper2 = LoanCalc.NPER(interest, pmt2, -(inputs.loan_amount - q.total_principal), 0);
    var total_interest2 = q.total_interest + LoanCalc.totalInterest(interest, nper2, pmt2, inputs.loan_amount - q.total_principal);

    var x = pmt1 + inputs.extra_contribution;
    $("#res2").html("$" + LoanCalc.addCommas(x.toFixed(2)));

    var arr = [];
    var p1 = inputs.loan_amount;
    var p2 = inputs.loan_amount;
    var a;
    var b;

    for (var i = 0; i <= nper1; i++) {
      arr[i] = {};
      a = LoanCalc.calculatePeriodRepayment(pmt1, interest, nper1, i);
      arr[i].original = p1;
      p1 -= a.principal_repayment;

      if (i >= s) {
        b = LoanCalc.calculatePeriodRepayment(pmt2, interest, nper2, i - s);
        arr[i].extra = p2;
        p2 -= b.principal_repayment;
      } else {
        arr[i].extra = p1;
        p2 -= a.principal_repayment;
      }
    }

    var original_years = inputs.loan_term;
    var years_saved = 0;

    if (inputs.repayment_frequency == "weekly") {
      years_saved = original_years - ((s + nper2) / WEEKS_PER_YEAR);
      drawChart(arr, WEEKS_PER_YEAR);
    } else if (inputs.repayment_frequency == "fortnightly") {
      years_saved = original_years - ((s + nper2) / FORTNIGHTS_PER_YEAR);
      drawChart(arr, FORTNIGHTS_PER_YEAR);
    } else {
      years_saved = original_years - ((s + nper2) / MONTHS_PER_YEAR);
      drawChart(arr, MONTHS_PER_YEAR);
    }

    var months_saved = Math.round((years_saved - Math.floor(years_saved)) * MONTHS_PER_YEAR);
    years_saved = Math.floor(years_saved);

    if (years_saved < 0) {
      years_saved = 0;
      months_saved = 0;
    }

    $("#res3").html(years_saved + " years, " + months_saved + " months");

    var interest_saved = total_interest1 - total_interest2;
    $("#res4").html("$" + LoanCalc.addCommas(interest_saved.toFixed(2)));

    result = [
      {key: "loan amount", value: "$" + LoanCalc.addCommas(inputs.loan_amount.toFixed(2))},
      {key: "interest rate", value: inputs.interest_rate},
      {key: "loan term", value: inputs.loan_term},
      {key: "repayment frequency", value: inputs.repayment_frequency},
      {key: "extra contribution", value: "$" + inputs.extra_contribution},
      {key: "extra contribution start", value: inputs.extra_contribution_start + " after years"},
      {key: "SEPARATOR"},
      {key: "Minumum monthly repayments", value: "$" + LoanCalc.addCommas(res.repayment.toFixed(2))},
      {key: "Increased monthly repayments", value: "$" + LoanCalc.addCommas(x.toFixed(2))},
      {key: "Time saved", value: years_saved + " years, " + months_saved + " months"},
      {key: "Interest saved", value: "$" + LoanCalc.addCommas(interest_saved.toFixed(2))}
    ];
  }

  var printModeReset;
  $(".calc .print").click(function () {
    $('#chart_div').width(400);
    $(window).trigger('resize.chart');
    setTimeout(function () {
      window.print();
      printMode = true;
      clearTimeout(printModeReset);
      printModeReset = setTimeout(function () {
        $(window).trigger('resize.chart');
      }, 1);
    }, 100);
  });

  $(".calc .save").click(function (e) {
    e.preventDefault();
    var data = encodeURIComponent(JSON.stringify(result));
    var win=window.open('save.php?data=' + data + '&title=Extra Repayment', '_blank');
    win.focus();
  });

  $(document).on('click', '.calc .email', function (e) {
    e.preventDefault();
    var $form = $('#email-form');
    var isVisible = $form.is(':visible');

    $form[isVisible ? 'hide' : 'show']();
    $(".calc #message").val() //.val(isVisible ? '' : LoanCalc.resultToString(result));
  });

  var resizeTimer;
  $(window).on('resize.chart', function () {
    clearTimeout(resizeTimer);
    resizeTimer = setTimeout(redraw, 100);
  });

  $(".calc #send_email").click(function (e) {
    e.preventDefault();
    LoanCalc.sendMail();
  });

  redraw();
})(jQuery);



