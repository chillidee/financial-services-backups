<?php

if (!defined('__DIR__')) {
  define('__DIR__', dirname(__FILE__));
}

require_once __DIR__.'/libs/fpdf/fpdf.php';

class PDF extends FPDF
{
  public $_calcTitle = null;

  public function Header()
  {
      $this->Image(__DIR__.'/img/logo.png', 85, 6, 30);
      $this->SetFont('Arial', 'B', 15);
      if ($this->_calcTitle) {
        $this->Cell(0, 70, $this->_calcTitle, 0, 0, 'C');
      }
      $this->Ln(45);
  }
}

function dump($vars) {
  echo '<pre>';
  print_r($vars);
  echo '</pre>';
  exit;
}


if (isset($_GET['data'])) {
  $data = json_decode($_GET['data']);
  $title = isset($_GET['title']) ? $_GET['title'] : 'Sample Title';

  if (count($data) > 0) {
    $pdf = new PDF();

    if ($title) {
      $pdf->_calcTitle = $title;
    }

    $pdf->AddPage();
    $pdf->SetFont('Arial', '', 12);

    foreach ($data as $row) {
      if ($row->key === 'SEPARATOR') {
        $pdf->Ln(7);
      } else {
        $pdf->Cell(40, 10, ucfirst(trim($row->key, ':')) . ': ' . $row->value);
        $pdf->Ln(7);
      }
    }
    $pdf->Output();
  } else {
    die('Ooops, there is no any data.');
  }
} else {
  die('Ooops, there is no any data.');
}