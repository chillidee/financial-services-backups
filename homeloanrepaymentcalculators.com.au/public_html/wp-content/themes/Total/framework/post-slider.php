<?php
/**
 * Display page slider based on meta option
 *
 * @package Total
 * @subpackage functions
 * @author Alexander Clarke
 * @copyright Copyright (c) 2014, Symple Workz LLC
 * @link http://www.wpexplorer.com
 * @since Total 1.0
 */

/**
 * Outputs page/post slider based on the wpex_post_slider_shortcode custom field
 *
 * @since Total 1.0
 * @return html
 */
if ( ! function_exists( 'wpex_post_slider' ) ) {
	function wpex_post_slider() {

		// Get post ID
		$post_id = wpex_get_the_id();

		// Return if no post ID
		if( ! $post_id ) {
			return;
		}

		// Disable on mobile
		if ( 'on' == get_post_meta( $post_id, 'wpex_disable_post_slider_mobile', true ) && wp_is_mobile() ) {
			return;
		}

		// Get Correct slider with fallback
		$slider = get_post_meta( $post_id, 'wpex_post_slider_shortcode', true );
		if ( '' != $slider ) {
			$slider = $slider;
		} elseif( get_post_meta( $post_id, 'wpex_page_slider_shortcode', true ) ) {
			$slider = get_post_meta( $post_id, 'wpex_page_slider_shortcode', true );
		}

		// Get slider alternative
		$slider_alt = get_post_meta( $post_id, 'wpex_post_slider_mobile_alt', true );
		if ( is_array( $slider_alt ) && !empty( $slider_alt['url'] ) ) {
			$slider_alt = $slider_alt['url'];
			$slider_alt_url = get_post_meta( $post_id, 'wpex_post_slider_mobile_alt_url', true );
		} else {
			$slider_alt = $slider_alt_url = '';
		}

		// Get post slider bottom margin
		$margin = get_post_meta( $post_id, 'wpex_post_slider_bottom_margin', true );
		
		// Display Slider
		if ( '' != $slider ) { ?>
			<div class="page-slider clr">
				<?php
				// Mobile slider
				if ( wp_is_mobile() && $slider_alt ) {
					if ( $slider_alt_url ) { ?>
						<a href="<?php echo esc_url( $slider_alt_url ); ?>" title="" target="_<?php echo get_post_meta( $post_id, 'wpex_post_slider_mobile_alt_url_target', true ); ?>">
							<img src="<?php echo $slider_alt; ?>" class="page-slider-mobile-alt" alt="<?php echo the_title(); ?>" />
						</a>
					<?php } else { ?>
						<img src="<?php echo $slider_alt; ?>" class="page-slider-mobile-alt" alt="<?php echo the_title(); ?>" />
					<?php } ?>
				<?php }
				// Desktop slider
				else {
					echo do_shortcode( $slider );
				} ?>
			</div><!-- .page-slider -->
			<?php if ( '' != $margin ) { ?>
				<div style="height:<?php echo intval( $margin ); ?>px;"></div>
			<?php } ?>
		<?php }

	}
}

/**
 * Gets slider position based on wpex_post_slider_shortcode_position custom field
 *
 * @since Total 1.51
 * @return string
 */
if ( ! function_exists( 'wpex_post_slider_position' ) ) {
	function wpex_post_slider_position( $slider_position = 'below_title' ) {
		// Get post id
		$post_id = wpex_get_the_id();
		// Bail when function isn't necessary
		if ( ! $post_id ) {
			return;
		}
		// Get position
		$slider_position = get_post_meta( $post_id, 'wpex_post_slider_shortcode_position', true );
		if ( $slider_position ) {
			$slider_position = $slider_position;
		} else {
			$slider_position = 'below_title';
		}
		// Return the slider position
		return $slider_position;
	}
}