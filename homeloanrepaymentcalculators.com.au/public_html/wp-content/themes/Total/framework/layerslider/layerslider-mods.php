<?php
/**
 * Functions that modify the LayerSlider plugin
 *
 * @package Total
 * @subpackage LayerSlider Functions
 * @author Alexander Clarke
 * @copyright Copyright (c) 2014, Symple Workz LLC
 * @link http://www.wpexplorer.com
 * @since Total 1.0
 */


// Only needed in the admin
if( ! is_admin() ) {
	return;
}

// Disable LayerSlider auto updates
if ( ! function_exists( 'wpex_layerslider_remove_auto_updates' ) ) {
	function wpex_layerslider_remove_auto_updates() {
		$GLOBALS['lsAutoUpdateBox'] = false;
	}
}
add_action( 'layerslider_ready', 'wpex_layerslider_remove_auto_updates' );