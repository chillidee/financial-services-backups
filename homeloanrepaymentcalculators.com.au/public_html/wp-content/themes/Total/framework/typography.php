<?php
/**
 * This file is used for all typography functions
 * Loads Google fonts and custom CSS in the front-end
 * Adds support for Google fonts into the WP editor
 *
 * @package Total
 * @subpackage functions
 * @author Alexander Clarke
 * @copyright Copyright (c) 2014, Symple Workz LLC
 * @link http://www.wpexplorer.com
 * @since Total 1.0
 */


/**
 * Loops through typography options and holds an array of Google Fonts to load
 * and the custom CSS to output for the front-end
 *
 * @since Total 1.0
 */
if ( !function_exists( 'wpex_typography' ) ) {
	function wpex_typography( $return ='' ) {
	
		$load_google_fonts = array();
		$css = $google_scripts = $google_fonts_css = '';
		
		/*------------------------------------------------*/
		/* Body
		/*------------------------------------------------*/
		$add_css = '';
		$font = wpex_option( 'body_font' );

		if ( ! empty( $font ) ) {
			
			// Vars
			$family = isset( $font['font-family'] ) ? $font['font-family'] : '';
			$size = isset( $font['font-size'] ) ? $font['font-size'] : '';
			$weight = isset( $font['font-weight'] ) ? $font['font-weight'] : '';
			$style = isset( $font['font-style'] ) ? $font['font-style'] : '';
			$color = isset( $font['color'] ) ? $font['color'] : '';
			$line_height = isset( $font['line-height'] ) ? $font['line-height'] : '';
			
			// Font family
			if ( ! empty( $family ) && 'inherit' != $family ) {
				$css .= '.button, input, textarea{font-family:'. $family .'}';
				$add_css .= 'font-family:'. $family .';';
				$load_google_fonts[] = $family;
			}
			
			// Font size
			if ( ! empty( $size ) && 'inherit' != $size ) {
				$add_css .= 'font-size:'. $size .';';
			}
			
			// Font weight
			if ( ! empty( $weight ) && 'inherit' != $weight ) {
				$add_css .= 'font-weight:'. $weight .';';
			}

			// Font Style
			if ( ! empty( $style ) && 'inherit' != $style ) {
				$add_css .= 'font-style:'. $style .';';
			}
			
			// Font color
			if ( ! empty( $color ) && 'inherit' != $color ) {
				$add_css .= 'color:'. $color .';';
			}

			// Line Height
			if ( ! empty( $line_height ) && 'inherit' != $line_height ) {
				$add_css .= 'line-height:'. $line_height .';';
			}
			
			
		}

		if( $add_css ) {
			$css .= 'body{'. $add_css .'}';
		}

		/*------------------------------------------------*/
		/* Headings
		/*------------------------------------------------*/
		$add_css = '';
		$font = wpex_option( 'headings_font' );

		if ( ! empty( $font ) ) {
			
			// Vars
			$family = isset( $font['font-family'] ) ? $font['font-family'] : '';
			$weight = isset( $font['font-weight'] ) ? $font['font-weight'] : '';
			$style = isset( $font['font-style'] ) ? $font['font-style'] : '';
			
			// Font family
			if ( $family && 'inherit' != $family ) {
				$css .= '.vc_text_separator{font-family:'. $family .'}';
				$add_css .= 'font-family:'. $family .'';
				$load_google_fonts[] = $family;
			}
			
			// Font weight
			if ( $weight ) {
				$add_css .= 'font-weight:'. $weight .';';
			}

			// Font Style
			if ( ! empty( $style ) && 'inherit' != $style ) {
				$add_css .= 'font-style:'. $style .';';
			}
			
		}

		if( $add_css ) {
			$css .= 'h1,h2,h3,h4,h5,h6,.theme-heading,.widget-title{'. $add_css .'}';
		}

		/*------------------------------------------------*/
		/* Logo
		/*------------------------------------------------*/
		$add_css = '';
		$font = wpex_option( 'logo_font' );

		if ( ! empty( $font ) ) {
			
			// Vars
			$family = isset( $font['font-family'] ) ? $font['font-family'] : '';
			$size = isset( $font['font-size'] ) ? $font['font-size'] : '';
			$weight = isset( $font['font-weight'] ) ? $font['font-weight'] : '';
			$color = isset( $font['color'] ) ? $font['color'] : '';
			$style = isset( $font['font-style'] ) ? $font['font-style'] : '';
			
			// Font family
			if ( ! empty( $family ) && 'inherit' != $family ) {
				$add_css .= 'font-family:'. $family .';';
				$load_google_fonts[] = $family;
			}
			
			// Font size
			if ( ! empty( $size ) ) {
				$add_css .= 'font-size:'. $size .';';
			}
			
			// Font weight
			if ( ! empty( $weight ) ) {
				$add_css .= 'font-weight:'. $weight .';';
			}
			
			// Font color
			if ( ! empty( $color ) ) {
				$add_css .= 'color:'. $color .';';
			}

			// Font Style
			if ( ! empty( $style ) && 'inherit' != $style ) {
				$add_css .= 'font-style:'. $style .';';
			}
			
		}

		if( $add_css ) {
			$css .= '#site-logo a{'. $add_css .'}';
		}
		
		/*------------------------------------------------*/
		/* Menu
		/*------------------------------------------------*/
		$add_css = '';
		$font = wpex_option( 'menu_font' );

		if ( ! empty( $font ) ) {
			
			// Vars
			$family = isset( $font['font-family'] ) ? $font['font-family'] : '';
			$size = isset( $font['font-size'] ) ? $font['font-size'] : '';
			$weight = isset( $font['font-weight'] ) ? $font['font-weight'] : '';
			$style = isset( $font['font-style'] ) ? $font['font-style'] : '';
			
			// Font family
			if ( ! empty( $family ) && 'inherit' != $family ) {
				$add_css .= 'font-family:'. $family .';';
				$load_google_fonts[] = $family;
			}
			
			// Font size
			if ( ! empty( $size ) ) {
				$add_css .= 'font-size:'. $size .';';
			}
			
			// Font weight
			if ( ! empty( $weight ) ) {
				$add_css .= 'font-weight:'. $weight .';';
			}

			// Font Style
			if ( ! empty( $style ) && 'inherit' != $style ) {
				$add_css .= 'font-style:'. $style .';';
			}

		}

		if( $add_css ) {
			$css .= '#site-navigation .sf-menu a{'. $add_css .'}';
		}
		
		/*------------------------------------------------*/
		/* Menu Dropdowns
		/*------------------------------------------------*/
		$add_css = '';
		$font = wpex_option( 'menu_dropdown_font' );

		if ( ! empty( $font ) ) {
			
			// Vars	
			$family = isset( $font['font-family'] ) ? $font['font-family'] : '';
			$size = isset( $font['font-size'] ) ? $font['font-size'] : '';
			$weight = isset( $font['font-weight'] ) ? $font['font-weight'] : '';
			$style = isset( $font['font-style'] ) ? $font['font-style'] : '';
			
			// Font family
			if ( ! empty( $family ) && 'inherit' != $family ) {
				$add_css .= 'font-family:'. $family .';';
				$load_google_fonts[] = $family;
			}
			
			// Font size
			if ( ! empty( $size ) ) {
				$add_css .= 'font-size: '. $size .';';
			}
			
			// Font weight
			if ( ! empty( $weight ) ) {
				$add_css .= 'font-weight: '. $weight .';';
			}

			// Font Style
			if ( ! empty( $style ) && 'inherit' != $style ) {
				$add_css .= 'font-style: '. $style .';';
			}

		}

		if( $add_css ) {
			$css .= '#site-navigation .sf-menu ul a{'. $add_css .'}';
		}
		
		/*------------------------------------------------*/
		/* Page Titles
		/*------------------------------------------------*/
		$add_css = '';
		$font = wpex_option( 'page_header_font' );

		if ( ! empty( $font ) ) {
			
			// Vars
			$family = isset( $font['font-family'] ) ? $font['font-family'] : '';
			$size = isset( $font['font-size'] ) ? $font['font-size'] : '';
			$weight = isset( $font['font-weight'] ) ? $font['font-weight'] : '';
			$color = isset( $font['color'] ) ? $font['color'] : '';
			$style = isset( $font['font-style'] ) ? $font['font-style'] : '';
			
			// Font family
			if ( ! empty( $family ) && 'inherit' != $family ) {
				$add_css .= 'font-family:"'. $family .'";';
				$load_google_fonts[] = $family;
			}
			
			// Font size
			if ( ! empty( $size ) ) {
				$add_css .= 'font-size:'. $size .';';
			}
			
			// Font weight
			if ( ! empty( $weight ) ) {
				$add_css .= 'font-weight:'. $weight .';';
			}
			
			// Font color
			if ( ! empty( $color ) ) {
				$add_css .= 'color:'. $color .';';
			}

			// Font Style
			if ( ! empty( $style ) && 'inherit' != $style ) {
				$add_css .= 'font-style:'. $style .';';
			}

		}

		if( $add_css ) {
			$css .= '.page-header-title{'. $add_css .'}';
		}


		/*------------------------------------------------*/
		/* Breadcrumbs
		/*------------------------------------------------*/
		$add_css = '';
		$font = wpex_option( 'breadcrumbs_typography' );

		if ( ! empty( $font ) ) {
			
			// Vars
			$family = isset( $font['font-family'] ) ? $font['font-family'] : '';
			$size = isset( $font['font-size'] ) ? $font['font-size'] : '';
			$weight = isset( $font['font-weight'] ) ? $font['font-weight'] : '';
			$color = isset( $font['color'] ) ? $font['color'] : '';
			$style = isset( $font['font-style'] ) ? $font['font-style'] : '';
			
			// Font family
			if ( ! empty( $family ) && 'inherit' != $family ) {
				$add_css .= 'font-family:"'. $family .'";';
				$load_google_fonts[] = $family;
			}
			
			// Font size
			if ( ! empty( $size ) ) {
				$add_css .= 'font-size:'. $size .';';
			}
			
			// Font weight
			if ( ! empty( $weight ) ) {
				$add_css .= 'font-weight:'. $weight .';';
			}
			
			// Font color
			if ( ! empty( $color ) ) {
				$add_css .= 'color:'. $color .';';
			}

			// Font Style
			if ( ! empty( $style ) && 'inherit' != $style ) {
				$add_css .= 'font-style:'. $style .';';
			}

		}

		if( $add_css ) {
			$css .= '.site-breadcrumbs{'. $add_css .'}';
		}

		/*------------------------------------------------*/
		/* Sidebar Widget Titles
		/*------------------------------------------------*/
		$add_css = '';
		$font = wpex_option( 'sidebar_widget_title_typography' );

		if ( ! empty( $font ) ) {
			
			// Vars
			$family = isset( $font['font-family'] ) ? $font['font-family'] : '';
			$size = isset( $font['font-size'] ) ? $font['font-size'] : '';
			$weight = isset( $font['font-weight'] ) ? $font['font-weight'] : '';
			$color = isset( $font['color'] ) ? $font['color'] : '';
			$style = isset( $font['font-style'] ) ? $font['font-style'] : '';
			$line_height = isset( $font['line-height'] ) ? $font['line-height'] : '';
			
			// Font family
			if ( ! empty( $family ) && 'inherit' != $family ) {
				$add_css .= 'font-family:"'. $family .'";';
				$load_google_fonts[] = $family;
			}
			
			// Font size
			if ( ! empty( $size ) ) {
				$add_css .= 'font-size:'. $size .';';
			}
			
			// Font weight
			if ( ! empty( $weight ) ) {
				$add_css .= 'font-weight:'. $weight .';';
			}
			
			// Font color
			if ( ! empty( $color ) ) {
				$add_css .= 'color:'. $color .';';
			}

			// Font Style
			if ( ! empty( $style ) && 'inherit' != $style ) {
				$add_css .= 'font-style:'. $style .';';
			}

			// Font Line Height
			if ( ! empty( $line_height ) && 'inherit' != $line_height ) {
				$add_css .= 'line-height:'. $line_height .';';
			}

		}

		if( $add_css ) {
			$css .= '#sidebar .widget-title{'. $add_css .'}';
		}

		/*------------------------------------------------*/
		/* Footer Widget Titles
		/*------------------------------------------------*/
		$add_css = '';
		$font = wpex_option( 'footer_widget_title_typography' );

		if ( ! empty( $font ) ) {
			
			// Vars
			$family = isset( $font['font-family'] ) ? $font['font-family'] : '';
			$size = isset( $font['font-size'] ) ? $font['font-size'] : '';
			$weight = isset( $font['font-weight'] ) ? $font['font-weight'] : '';
			$color = isset( $font['color'] ) ? $font['color'] : '';
			$style = isset( $font['font-style'] ) ? $font['font-style'] : '';
			$line_height = isset( $font['line-height'] ) ? $font['line-height'] : '';
			
			// Font family
			if ( ! empty( $family ) && 'inherit' != $family ) {
				$add_css .= 'font-family:"'. $family .'";';
				$load_google_fonts[] = $family;
			}
			
			// Font size
			if ( ! empty( $size ) ) {
				$add_css .= 'font-size:'. $size .';';
			}
			
			// Font weight
			if ( ! empty( $weight ) ) {
				$add_css .= 'font-weight:'. $weight .';';
			}
			
			// Font color
			if ( ! empty( $color ) ) {
				$add_css .= 'color:'. $color .';';
			}

			// Font Style
			if ( ! empty( $style ) && 'inherit' != $style ) {
				$add_css .= 'font-style:'. $style .';';
			}

			// Font Line Height
			if ( ! empty( $line_height ) && 'inherit' != $line_height ) {
				$add_css .= 'line-height:'. $line_height .';';
			}

		}

		if( $add_css ) {
			$css .= '#footer .widget-title{'. $add_css .'}';
		}
		
		// Return trimmed CSS
		if ( 'css' == $return && $css ) {
			return preg_replace( '/\s+/', ' ', $css );
		}

		// Return Google Fonts
		if ( 'google_fonts' == $return && $load_google_fonts ) {
			return array_unique( $load_google_fonts );
		}
		
	}
}


/**
 * List of standard fonts
 *
 * @since Total 1.0
 */
if ( !function_exists( 'wpex_standard_fonts' ) ) {
	function wpex_standard_fonts() {
		return array(
			"Arial, Helvetica, sans-serif",
			"'Arial Black', Gadget, sans-serif",
			"'Bookman Old Style', serif",
			"'Comic Sans MS', cursive",
			"Courier, monospace",
			"Garamond, serif",
			"Georgia, serif",
			"Impact, Charcoal, sans-serif",
			"'Lucida Console', Monaco, monospace",
			"'Lucida Sans Unicode', 'Lucida Grande', sans-serif",
			"'MS Sans Serif', Geneva, sans-serif",
			"'MS Serif', 'New York', sans-serif",
			"'Palatino Linotype', 'Book Antiqua', Palatino, serif",
			"Tahoma, Geneva, sans-serif",
			"Tahoma,Geneva, sans-serif",
			"'Times New Roman', Times, serif",
			"'Trebuchet MS', Helvetica, sans-serif",
			"Verdana, Geneva, sans-serif",
			"Tahoma,Geneva",
			"Garamond, serif",
			"'Bookman Old Style'",
			"Tahoma,Geneva",
			"Verdana",
			"Comic Sans",
			"Courier, monospace",
			"'Arial Black'",
			"Arial",
			"'Comic Sans MS'",
			"Courier",
			"Georgia",
			"Paratina Linotype",
			"Trebuchet MS",
			"Times New Roman",
			"'Times New Roman', Times,serif",
		);
	}
}

/**
 * Returns active Google font scripts - enabled by the admin
 *
 * @since Total 1.0
 */
if ( !function_exists( 'wpex_active_googlefont_scripts' ) ) {
	function wpex_active_googlefont_scripts() {
		$fonts = wpex_typography( 'google_fonts' );
		$std_fonts = wpex_standard_fonts();
		$scripts = array();
		if ( $fonts ) {
			foreach ( $fonts as $font ) {
				//echo $font .'<br />';
				if ( !in_array( $font, $std_fonts ) ) {
						$scripts[] = 'https://fonts.googleapis.com/css?family='.str_replace(' ', '%20', $font ).'';
				}
			}
			return $scripts;
		} else {
			return false;
		}
	}
}

/**
 * Loads CSS for Google fonts on the front-end hooked to wp_head
 *
 * @package WordPress
 */
if ( !function_exists( 'wpex_load_fonts' ) ) {
	function wpex_load_fonts() {
		$scripts = wpex_active_googlefont_scripts();
		if ( ! $scripts ) {
			return;
		}
		$output = '<!-- TOTAL - Google Fonts -->';
		foreach ( $scripts as $script ) {
			$output .= '<link href="'. $script .':300italic,400italic,600italic,700italic,800italic,400,300,600,700,800&amp;subset=latin,cyrillic-ext,greek-ext,greek,vietnamese,latin-ext,cyrillic" rel="stylesheet" type="text/css">';
		}
		echo $output;
	}
}
add_action( 'wp_head', 'wpex_load_fonts' );

/**
 * Adds loaded Google fonts to the WP editor font dropdown list
 *
 * @package WordPress
 */
if ( ! function_exists( 'wpex_mce_google_fonts_array' ) ) {
	function wpex_mce_google_fonts_array( $initArray ) {
		// Get Fonts List
		$fonts = wpex_typography( 'google_fonts' );
		$fonts_array = array();
		if ( is_array( $fonts ) && ! empty( $fonts ) ) {
			foreach ( $fonts as $font ) {
				$fonts_array[] = $font .'=' . $font;
			}
			$fonts = implode( ';', $fonts_array );
			// Add Fonts To MCE
			if ( $fonts ) {
				$initArray['font_formats'] = $fonts .';Andale Mono=andale mono,times;Arial=arial,helvetica,sans-serif;Arial Black=arial black,avant garde;Book Antiqua=book antiqua,palatino;Comic Sans MS=comic sans ms,sans-serif;Courier New=courier new,courier;Georgia=georgia,palatino;Helvetica=helvetica;Impact=impact,chicago;Symbol=symbol;Tahoma=tahoma,arial,helvetica,sans-serif;Terminal=terminal,monaco;Times New Roman=times new roman,times;Trebuchet MS=trebuchet ms,geneva;Verdana=verdana,geneva;Webdings=webdings;Wingdings=wingdings,zapf dingbats';
			}
		}
		return $initArray;
	}
}
add_filter( 'tiny_mce_before_init', 'wpex_mce_google_fonts_array' );

/**
 * Adds loaded Google fonts scripts to the backend for use in the editor
 *
 * @since Total 1.0
 */
if ( ! function_exists( 'wpex_mce_google_fonts_styles' ) ) {
	function wpex_mce_google_fonts_styles() {
		if ( ! wpex_option( 'load_google_fonts_admin', '1' ) ) {
			return;
		}
		$scripts = wpex_active_googlefont_scripts();
		if ( ! $scripts ) {
			return;
		}
		if ( is_array( $scripts ) && ! empty( $scripts ) ) {
			foreach ( $scripts as $script ) {
				add_editor_style( str_replace( ',', '%2C', $script .':300italic,400italic,600italic,700italic,800italic,400,300,600,700,800&amp;subset=latin,cyrillic-ext,greek-ext,greek,vietnamese,latin-ext,cyrillic' ) );
			}
		}
	}
}
add_action( 'init', 'wpex_mce_google_fonts_styles' );