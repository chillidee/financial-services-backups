<?php
/**
 * Returns the correct main layout class for the current post/page/archive/etc
 *
 * @package Total
 * @subpackage functions
 * @author Alexander Clarke
 * @copyright Copyright (c) 2014, Symple Workz LLC
 * @link http://www.wpexplorer.com
 * @since Total 1.0
 */

if ( ! function_exists( 'wpex_get_post_layout_class' ) ) {
	function wpex_get_post_layout_class() {

		// Define default class
		$class = 'right-sidebar';
		
		// Singular Page
		if ( is_singular( 'page' ) ) {
			if ( '' != get_post_meta( get_the_ID(), 'wpex_post_layout', true ) ) {
				$class = get_post_meta( get_the_ID(), 'wpex_post_layout', true );
			} else {
				// Blog template
				if ( is_page_template( 'templates/blog.php' ) ) {
					$class = wpex_option( 'blog_archives_layout', 'right-sidebar' );
				}
				// All other pages
				else {
					$class = wpex_option( 'page_single_layout', 'right-sidebar' );
				}
			}
		}

		// Singular Post
		elseif ( is_singular( 'post' ) ) {
			if ( '' != get_post_meta( get_the_ID(), 'wpex_post_layout', true ) ) {
				$class = get_post_meta( get_the_ID(), 'wpex_post_layout', true );
			} else {
				$class = wpex_option( 'blog_single_layout', 'right-sidebar' );
			}
		}

		// Singular Portfolio
		elseif ( is_singular( 'portfolio' ) ) {
			if ( '' != get_post_meta( get_the_ID(), 'wpex_post_layout', true ) ) {
				$class = get_post_meta( get_the_ID(), 'wpex_post_layout', true );
			} else {
				$class = wpex_option( 'portfolio_single_layout', 'right-sidebar' );
			}
		}

		// Singular Staff
		elseif ( is_singular( 'staff' ) ) {
			if ( '' != get_post_meta( get_the_ID(), 'wpex_post_layout', true ) ) {
				$class = get_post_meta( get_the_ID(), 'wpex_post_layout', true );
			} else {
				$class = wpex_option( 'staff_single_layout', 'right-sidebar' );
			}
		}

		// Singular Testimonials
		elseif ( is_singular( 'testimonials' ) ) {
			if ( '' != get_post_meta( get_the_ID(), 'wpex_post_layout', true ) ) {
				$class = get_post_meta( get_the_ID(), 'wpex_post_layout', true );
			} else {
				$class = wpex_option( 'testimonials_single_layout', 'right-sidebar' );
			}
		}

		// WooCoomerce shop
		if( class_exists( 'Woocommerce' ) && is_shop() ) {
			$meta = get_post_meta( wpex_get_the_id(), 'wpex_post_layout', true );
			if ( '' != $meta ) {
				$class = $meta;
			} else {
				$class = wpex_option( 'woo_shop_layout', 'full-width' );
			}
		}

		// WooCommerce single
		elseif ( class_exists( 'Woocommerce' ) && wpex_option( 'woo_custom_sidebar', '1' ) && is_woocommerce() ) {
			$meta = get_post_meta( wpex_get_the_id(), 'wpex_post_layout', true );
			if ( '' != $meta ) {
				$class = $meta;
			} else {
				$class = wpex_option( 'woo_product_layout', 'full-width' );
			}
		}

		// WooCommerce tax
		elseif ( class_exists( 'Woocommerce' ) && is_tax( 'product_cat' ) || is_tax( 'product_tag' ) ) {
			$class = wpex_option( 'woo_shop_layout', 'full-width' );
		}
		
		// Portfolio tax
		elseif ( is_tax( 'portfolio_category' ) || is_tax( 'portfolio_tag' ) ) {
			$class = wpex_option( 'portfolio_archive_layout', 'full-width' );
		}
		
		// Staff tax
		elseif ( is_tax( 'staff_category' ) || is_tax( 'staff_tag' ) ) {
			$class = wpex_option( 'staff_archive_layout', 'full-width' );
		}
		
		// Testimonials tax
		elseif ( is_tax( 'testimonials_category' ) || is_tax( 'testimonials_tag' ) ) {
			$class = wpex_option( 'testimonials_archive_layout', 'full-width' );
		}

		// Home
		elseif ( is_home() ) {
			$class = wpex_option( 'blog_archives_layout', 'right-sidebar' );
		}

		// Standard Categories
		elseif ( is_category() ) {
			$class = wpex_option( 'blog_archives_layout', 'right-sidebar' );
			$term = get_query_var( 'cat' );
			$term_data = get_option( "category_$term" );
			if ( $term_data ) {
				if( '' != $term_data['wpex_term_layout'] ){
					$class = $term_data['wpex_term_layout'];
				}
			}
		}

		// Author
		elseif ( is_author() ) {
			$class = wpex_option( 'blog_archives_layout', 'right-sidebar' );
		}

		// Archives
		elseif ( is_archive() ) {
			$class = wpex_option( 'blog_archives_layout', 'right-sidebar' );
		}

		// Tribe Events
		elseif ( function_exists( 'tribe_is_month' ) ) {
			if( tribe_is_month() ) {
				$class = 'full-width';
			} elseif ( function_exists( 'tribe_is_event' ) && function_exists( 'tribe_is_day' ) && tribe_is_event() && !tribe_is_day() && !is_single() ) {
				$class = 'full-width';
			} elseif ( function_exists( 'tribe_is_day' ) && tribe_is_day() ) {
				$class = 'full-width';
			}
			if ( is_singular( 'tribe_events' ) ) {
				$meta = get_post_meta( $post_id, 'wpex_post_layout', true );
				if ( '' != $meta ) {
					$class = $meta;
				} else {
					$class = 'full-width';
				}
			}
		}

		// Apply filters for child theme editing
		$class = apply_filters( 'wpex_post_layout_class', $class );
		
		// Return correct classname
		return $class;
		
	}
}