<?php
/**
 * Useful functions for the header overlay style
 *
 * @package Total
 * @subpackage Header Functions
 * @author Alexander Clarke
 * @copyright Copyright (c) 2014, Symple Workz LLC
 * @link http://www.wpexplorer.com
 * @since Total 1.1
 */

/**
 * Check if the header overlay style is enabled
 *
 * @since Total 1.53
 * @return bool
 */
if ( ! function_exists( 'wpex_is_overlay_header_enabled' ) ) { 
	function wpex_is_overlay_header_enabled() {
		$post_id = wpex_get_the_id();
		if ( $post_id && !wp_is_mobile() ) {
			if ( 'on' == get_post_meta( $post_id, 'wpex_overlay_header', true ) ) {
				return true;
			}	
		}
		return false;
	}
}

/**
 * WORK IN PROGRESS => not sure if I Will add this yet.
 *
 * Adds some custom styling for the header if needed
 * Hooks into the 'wpex_custom_css_filter' filter
 *
 * @since Total 1.53
 **/
/*
if ( ! function_exists( 'wpex_header_custom_css' ) ) {
	function wpex_header_custom_css( $output ) {

		// Check if it's a singular post and not mobile
		if ( is_singular() && !wp_is_mobile() ) {
			// Check if overlay header is enabled
			if ( '1' == get_post_meta( get_the_ID(), 'wpex_overlay_header', true ) ) {
				$overlay_header_css = '';
				// Add height for the overlay header
				if ( '' != get_post_meta( get_the_ID(), 'wpex_overlay_header_height', true ) ) {
					$overlay_header_css .= 'height: '. get_post_meta( get_the_ID(), 'wpex_overlay_header_height', true ) .';';
				}
			}
		}

		// If CSS is added append to the wpex_custom_css_filter $output
		if ( isset( $overlay_header_css ) && '' != $overlay_header_css ) {
			$output .= '@media only screen and (min-width: 960px) {#site-header.overlay-header{'. $overlay_header_css .'}}';
		}

		// Return CSS output
		return $output;

	}
}
add_filter( 'wpex_custom_css_filter', 'wpex_header_custom_css' );
*/