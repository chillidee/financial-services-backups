<?php
/**
 * Return the correct header style
 *
 * @package Total
 * @subpackage Header Functions
 * @author Alexander Clarke
 * @copyright Copyright (c) 2014, Symple Workz LLC
 * @link http://www.wpexplorer.com
 * @since Total 1.0
 */

if ( ! function_exists( 'wpex_get_header_style' ) ) {
	function wpex_get_header_style() {
		$style = wpex_option( 'header_style','one' );
		if ( is_singular() ) {
			global $post;
			if ( '' != get_post_meta( $post->ID, 'wpex_header_style', true ) ) {
				$style = $meta;
			}
		}
		return $style;
	}
}