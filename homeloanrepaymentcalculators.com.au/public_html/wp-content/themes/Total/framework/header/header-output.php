<?php
/**
 * Header Logo
 *
 * @package Total
 * @subpackage Header Functions
 * @author Alexander Clarke
 * @copyright Copyright (c) 2014, Symple Workz LLC
 * @link http://www.wpexplorer.com
 * @since Total 1.53
 */

/**
 * Whether the header should display or not
 *
 * @since Total 1.53
 * @return bool
 */
if ( ! function_exists( 'wpex_display_header' ) ) {
	function wpex_display_header( $return = true ) {
		if ( is_singular() ) {
			global $post;
			if ( 'on' == get_post_meta( $post->ID, 'wpex_disable_header', true ) ) {
				$return = false;
			}
		}
		return apply_filters( 'wpex_display_header', $return );
	}
}

/**
 * Outputs the header
 * Header elements are added via Hooks
 * See framework/hooks/ for more details
 * All header elements can be customized via your child theme, if you need some help just ask!
 *
 * @since Total 1.53
 */
if ( ! function_exists( 'wpex_header_output' ) ) {
	function wpex_header_output() {

		// Return nothing if header is disabled
		if ( !wpex_display_header() ) {
			return;
		}

		// Before header hook
		wpex_hook_header_before(); ?>

		<header id="site-header" class="<?php wpex_header_classes(); ?>" role="banner">
			<?php
			// Header top hook
			wpex_hook_header_top(); ?>
			<div id="site-header-inner" class="container clr">
				<?php
				// Header inner hook
				wpex_hook_header_inner(); ?>
			</div><!-- #site-header-inner -->
			<?php
			// Header bottom hook
			wpex_hook_header_bottom(); ?>
		</header><!-- #header -->

		<?php
		// Header after hook
		wpex_hook_header_after(); ?>

	<?php }
}