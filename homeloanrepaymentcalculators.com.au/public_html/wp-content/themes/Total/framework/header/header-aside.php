<?php
/**
 * Header Aside content used for header style two
 *
 * @package Total
 * @subpackage Header Functions
 * @author Alexander Clarke
 * @copyright Copyright (c) 2014, Symple Workz LLC
 * @link http://www.wpexplorer.com
 * @since Total 1.0
 */

if ( ! function_exists( 'wpex_header_aside' ) ) {
	function wpex_header_aside() {
		
		// If not headery style 2 return nothing
		if ( 'two' != wpex_get_header_style() ) {
			return;
		} ?>
		
		<aside id="header-aside" class=" header-two-aside clr">
			<?php
			// Header aside content based on theme option
			$content = wpex_option( 'header_aside' );
			if ( $content ) { ?>
				<div class="header-aside-content clr">
					<?php echo do_shortcode( $content ); ?>
				</div><!-- .header-aside-content -->
			<?php }
			// Show header search field if enabled in the theme options panel
			if ( wpex_option( 'main_search', '1' ) ) { ?>
				<div id="header-two-search" class="clr">
					<form method="get" class="header-two-searchform" action="<?php echo esc_url( home_url( '/' ) ); ?>" role="search">
						<input type="search" id="header-two-search-input" name="s" value="<?php _e( 'search', 'wpex' ); ?>" onfocus="if(this.value==this.defaultValue)this.value='';" onblur="if(this.value=='')this.value=this.defaultValue;"/>
						<button type="submit" value="" id="header-two-search-submit" />
							<span class="fa fa-search"></span>
						</button>
					</form><!-- #header-two-searchform -->
				</div><!-- #header-two-search -->
			<?php } ?>
		</aside><!-- #header-two-aside -->
		
	<?php
	}
}