<?php
/**
 * Add classes to the blog entries wrap
 *
 * @package Total
 * @subpackage Blog Functions
 * @author Alexander Clarke
 * @copyright Copyright (c) 2014, Symple Workz LLC
 * @link http://www.wpexplorer.com
 * @since Total 1.0
 */

/**
 * Adds main classes to blog post entries
 *
 * @since Total 1.16
 */
if ( ! function_exists( 'wpex_blog_wrap_classes' ) ) {
	function wpex_blog_wrap_classes( $classes=false ) {
		
		// Return custom class if set
		if ( $classes != false ) {
			return $class;
		}
		
		// Admin defaults
		$style = wpex_blog_style();
			
		// Isotope classes
		if ( $style == 'grid-entry-style' ) {
			$classes .= 'wpex-row ';
			if ( 'masonry' == wpex_blog_grid_style() ) {
				$classes .= 'blog-masonry-grid ';
			} else {
				if ( 'infinite_scroll' == wpex_blog_pagination_style() ) {
					$classes .= 'blog-masonry-grid ';
				} else {
					$classes .= 'blog-grid ';
				}
			}
		}
		
		// Add some margin when author is enabled
		if ( $style == 'grid-entry-style' && wpex_option( 'blog_entry_author_avatar' ) ) {
			$classes .= 'grid-w-avatars ';
		}
		
		// Infinite scroll classes
		if ( 'infinite_scroll' == wpex_blog_pagination_style() ) {
			$classes .= 'infinite-scroll-wrap ';
		}
		
		// Return correct classes
		echo apply_filters( 'wpex_blog_wrap_classes', $classes );
		
	}
}

/**
 * Animation classes that are added to the blog entry media
 *
 * @link framework/blog/blog-entry.php
 * @since Total 1.16
 */
if ( ! function_exists( 'wpex_img_animation_classes' ) ) {
	function wpex_img_animation_classes() {
		global $post;
		if ( 'post' != get_post_type( $post->ID ) ) {
			return;
		}
		if( wpex_option( 'blog_entry_image_hover_animation' ) ) {
			echo 'wpex-img-hover-parent wpex-img-hover-'. wpex_option( 'blog_entry_image_hover_animation' );
		}
	}
}