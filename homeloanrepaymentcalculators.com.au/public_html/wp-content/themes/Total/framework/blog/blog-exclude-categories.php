<?php
/**
 * Exclude blog categories from the main blog page / index
 *
 * @package Total
 * @subpackage Blog Functions
 * @author Alexander Clarke
 * @copyright Copyright (c) 2014, Symple Workz LLC
 * @link http://www.wpexplorer.com
 * @since Total 1.0
 */

if ( ! function_exists( 'wpex_blog_exclude_categories' ) ) {
	function wpex_blog_exclude_categories() {
		
		// Don't run in these places
		if ( is_admin() ) {
			return;
		} elseif ( is_search() ) {
			return;
		} elseif ( is_archive() ) {
			return;
		}
		
		// Categories to exclude
		$cats_to_exclude = wpex_option( 'blog_cats_exclude' );
		
		// Admin option is blank, so bail.
		if ( '' == $cats_to_exclude ) {
			return;
		}
		
		// Blog template
		if ( is_home() && !is_singular( 'page' ) ) {
			$exclude = $cats_to_exclude;
		} else {
			return;
		}

		// Alter query var
		if ( $cats_to_exclude ) {
			set_query_var( 'category__not_in', $cats_to_exclude );
		}
		
	}
}
add_action( 'pre_get_posts', 'wpex_blog_exclude_categories' );