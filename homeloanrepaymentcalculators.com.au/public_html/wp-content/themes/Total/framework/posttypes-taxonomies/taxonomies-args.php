<?php
/**
 * Used for renaming custom taxonomies
 *
 * @package Total
 * @subpackage functions
 * @author Alexander Clarke
 * @copyright Copyright (c) 2014, Symple Workz LLC
 * @link http://www.wpexplorer.com
 * @since Total 1.0
 */

/**
 * Portfolio Categories
 *
 * @since 1.0
 */
if ( ! function_exists( 'wpex_custom_portfolio_category_args' ) ) {
	function wpex_custom_portfolio_category_args( $args ) {
		// Labels
		$option = wpex_option( 'portfolio_cat_labels' );
		if ( !empty( $option ) && 'Portfolio Categories' != $option ) {
			$args['labels']['name'] = $option;
			$args['labels']['singular_name']				= $option;
			$args['labels']['search_items']					= __( 'Search','wpex');
			$args['labels']['popular_items']				= __( 'Popular','wpex');
			$args['labels']['all_items']					= __( 'All','wpex');
			$args['labels']['parent_item']					= __( 'Parent','wpex');
			$args['labels']['parent_item_colon']			= __( 'Parent','wpex');
			$args['labels']['edit_item']					= __( 'Edit','wpex');
			$args['labels']['update_item']					= __( 'Update','wpex');
			$args['labels']['add_new_item']					= __( 'Add New','wpex');
			$args['labels']['new_item_name']				= __( 'New Item Name','wpex');
			$args['labels']['separate_items_with_commas']	= __( 'Seperate with commas','wpex');
			$args['labels']['add_or_remove_items']			= __( 'Add or remove','wpex');
			$args['labels']['choose_from_most_used']		= __( 'Choose from the most used','wpex');
			$args['labels']['menu_name']					= $option;
		}
		// Custom Slug
		$option = wpex_option( 'portfolio_cat_slug', 'portfolio-category' );
		if ( isset( $option ) && $option ) {
			$args['rewrite'] = array( "slug" => $option );
		}
		// Return args
		return $args;
	}
}
add_filter( 'wpex_taxonomy_portfolio_category_args', 'wpex_custom_portfolio_category_args' );

/**
 * Portfolio Tags
 *
 * @since 1.0
 */
if ( ! function_exists( 'wpex_custom_portfolio_tag_args' ) ) {
	function wpex_custom_portfolio_tag_args( $args ) {
		// Labels
		$option = wpex_option( 'portfolio_tag_labels' );
		if ( !empty( $option ) && 'Portfolio Tags' != $option ) {
			$args['labels']['name'] = $option;
			$args['labels']['singular_name']				= $option;
			$args['labels']['search_items']					= __( 'Search','wpex');
			$args['labels']['popular_items']				= __( 'Popular','wpex');
			$args['labels']['all_items']					= __( 'All','wpex');
			$args['labels']['parent_item']					= __( 'Parent','wpex');
			$args['labels']['parent_item_colon']			= __( 'Parent','wpex');
			$args['labels']['edit_item']					= __( 'Edit','wpex');
			$args['labels']['update_item']					= __( 'Update','wpex');
			$args['labels']['add_new_item']					= __( 'Add New','wpex');
			$args['labels']['new_item_name']				= __( 'New Item Name','wpex');
			$args['labels']['separate_items_with_commas']	= __( 'Seperate with commas','wpex');
			$args['labels']['add_or_remove_items']			= __( 'Add or remove','wpex');
			$args['labels']['choose_from_most_used']		= __( 'Choose from the most used','wpex');
			$args['labels']['menu_name']					= $option;
		}
		// Custom Slug
		$option = wpex_option( 'portfolio_tag_slug', 'portfolio-tags' );
		if ( isset( $option ) && $option ) {
			$args['rewrite'] = array( "slug" => $option );
		}
		// Return args
		return $args;
	}
}
add_filter( 'wpex_taxonomy_portfolio_tag_args', 'wpex_custom_portfolio_tag_args' );

/**
 * Staff Categories
 *
 * @since 1.0
 */
if ( ! function_exists( 'wpex_custom_staff_category_args' ) ) {
	function wpex_custom_staff_category_args( $args ) {
		// Labels
		$option = wpex_option( 'staff_cat_labels' );
		if ( !empty( $option ) && 'Staff Categories' != $option ) {
			$args['labels']['name'] = $option;
			$args['labels']['singular_name']				= $option;
			$args['labels']['search_items']					= __( 'Search','wpex');
			$args['labels']['popular_items']				= __( 'Popular','wpex');
			$args['labels']['all_items']					= __( 'All','wpex');
			$args['labels']['parent_item']					= __( 'Parent','wpex');
			$args['labels']['parent_item_colon']			= __( 'Parent','wpex');
			$args['labels']['edit_item']					= __( 'Edit','wpex');
			$args['labels']['update_item']					= __( 'Update','wpex');
			$args['labels']['add_new_item']					= __( 'Add New','wpex');
			$args['labels']['new_item_name']				= __( 'New Item Name','wpex');
			$args['labels']['separate_items_with_commas']	= __( 'Seperate with commas','wpex');
			$args['labels']['add_or_remove_items']			= __( 'Add or remove','wpex');
			$args['labels']['choose_from_most_used']		= __( 'Choose from the most used','wpex');
			$args['labels']['menu_name']					= $option;
		}
		// Custom Slug
		$option = wpex_option( 'staff_cat_slug', 'staff-category' );
		if ( isset( $option ) && $option ) {
			$args['rewrite'] = array( "slug" => $option );
		}
		// Return args
		return $args;
	}
}
add_filter( 'wpex_taxonomy_staff_category_args', 'wpex_custom_staff_category_args' );

/**
 * Staff Tags
 *
 * @since 1.0
 */
if ( ! function_exists( 'wpex_custom_staff_tag_args' ) ) {
	function wpex_custom_staff_tag_args( $args ) {
		// Labels
		$option = wpex_option( 'staff_tag_labels' );
		if ( !empty( $option ) && 'Staff Tags' != $option ) {
			$args['labels']['name'] = $option;
			$args['labels']['singular_name']				= $option;
			$args['labels']['search_items']					= __( 'Search','wpex');
			$args['labels']['popular_items']				= __( 'Popular','wpex');
			$args['labels']['all_items']					= __( 'All','wpex');
			$args['labels']['parent_item']					= __( 'Parent','wpex');
			$args['labels']['parent_item_colon']			= __( 'Parent','wpex');
			$args['labels']['edit_item']					= __( 'Edit','wpex');
			$args['labels']['update_item']					= __( 'Update','wpex');
			$args['labels']['add_new_item']					= __( 'Add New','wpex');
			$args['labels']['new_item_name']				= __( 'New Item Name','wpex');
			$args['labels']['separate_items_with_commas']	= __( 'Seperate with commas','wpex');
			$args['labels']['add_or_remove_items']			= __( 'Add or remove','wpex');
			$args['labels']['choose_from_most_used']		= __( 'Choose from the most used','wpex');
			$args['labels']['menu_name']					= $option;
		}
		// Custom Slug
		$option = wpex_option( 'staff_tag_slug', 'staff-tags' );
		if ( isset( $option ) && $option ) {
			$args['rewrite'] = array( "slug" => $option );
		}
		// Return args
		return $args;
	}
}
add_filter( 'wpex_taxonomy_staff_tag_args', 'wpex_custom_staff_tag_args' );

/**
 * Testimonials Categories
 *
 * @since 1.0
 */
if ( ! function_exists( 'wpex_custom_testimonials_category_args' ) ) {
	function wpex_custom_testimonials_category_args( $args ) {
		// Labels
		$option = wpex_option( 'testimonials_cat_labels' );
		if ( !empty( $option ) && 'Testimonials Categories' != $option ) {
			$args['labels']['name'] = $option;
			$args['labels']['singular_name']				= $option;
			$args['labels']['search_items']					= __( 'Search','wpex');
			$args['labels']['popular_items']				= __( 'Popular','wpex');
			$args['labels']['all_items']					= __( 'All','wpex');
			$args['labels']['parent_item']					= __( 'Parent','wpex');
			$args['labels']['parent_item_colon']			= __( 'Parent','wpex');
			$args['labels']['edit_item']					= __( 'Edit','wpex');
			$args['labels']['update_item']					= __( 'Update','wpex');
			$args['labels']['add_new_item']					= __( 'Add New','wpex');
			$args['labels']['new_item_name']				= __( 'New Item Name','wpex');
			$args['labels']['separate_items_with_commas']	= __( 'Seperate with commas','wpex');
			$args['labels']['add_or_remove_items']			= __( 'Add or remove','wpex');
			$args['labels']['choose_from_most_used']		= __( 'Choose from the most used','wpex');
			$args['labels']['menu_name']					= $option;
		}
		// Custom Slug
		$option = wpex_option( 'testimonials_cat_slug', 'testimonials-category' );
		if ( isset( $option ) && $option ) {
			$args['rewrite'] = array( "slug" => $option );
		}
		// Return args
		return $args;
	}
}
add_filter( 'wpex_taxonomy_testimonials_category_args', 'wpex_custom_testimonials_category_args' );

/**
 * Post Series
 *
 * @since 1.0
 */
if ( ! function_exists( 'wpex_custom_post_series_args' ) ) {
	function wpex_custom_post_series_args( $args ) {
		// Labels
		$option = wpex_option( 'post_series_labels' );
		if ( !empty( $option ) && 'Post Series' != $option ) {
			$args['labels']['name'] = $option;
			$args['labels']['singular_name']				= $option;
			$args['labels']['search_items']					= __( 'Search','wpex');
			$args['labels']['popular_items']				= __( 'Popular','wpex');
			$args['labels']['all_items']					= __( 'All','wpex');
			$args['labels']['parent_item']					= __( 'Parent','wpex');
			$args['labels']['parent_item_colon']			= __( 'Parent','wpex');
			$args['labels']['edit_item']					= __( 'Edit','wpex');
			$args['labels']['update_item']					= __( 'Update','wpex');
			$args['labels']['add_new_item']					= __( 'Add New','wpex');
			$args['labels']['new_item_name']				= __( 'New Item Name','wpex');
			$args['labels']['separate_items_with_commas']	= __( 'Seperate with commas','wpex');
			$args['labels']['add_or_remove_items']			= __( 'Add or remove','wpex');
			$args['labels']['choose_from_most_used']		= __( 'Choose from the most used','wpex');
			$args['labels']['menu_name']					= $option;
		}
		// Custom Slug
		$option = wpex_option( 'post_series_slug', 'post-series' );
		if ( isset( $option ) && $option ) {
			$args['rewrite'] = array( "slug" => $option );
		}
		// Return args
		return $args;
	}
}
add_filter( 'wpex_taxonomy_post_series_args', 'wpex_custom_post_series_args' );