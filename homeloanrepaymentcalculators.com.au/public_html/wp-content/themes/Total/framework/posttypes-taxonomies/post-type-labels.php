<?php
/**
 * Used to re-brand built-in custom post types
 *
 * @package Total
 * @subpackage functions
 * @author Alexander Clarke
 * @copyright Copyright (c) 2014, Symple Workz LLC
 * @link http://www.wpexplorer.com
 * @since Total 1.0
 */

/**
* Portfolio Post Type
* @since 1.0
*/
if ( ! function_exists( 'wpex_custom_portfolio_args' ) ) {
	function wpex_custom_portfolio_args( $args ) {
		// Change Label
		$option = wpex_option( 'portfolio_labels', __( 'Portfolio', 'wpex' ) );
		if ( isset( $option  ) && $option  ) {
			$args['labels']['name']					= $option;
			$args['labels']['singular_name']		= $option;
			$args['labels']['add_new']				= __( 'Add New', 'wpex' );
			$args['labels']['add_new_item']			= __( 'Add New Item', 'wpex' );
			$args['labels']['edit_item']			= __( 'Edit Item', 'wpex' );
			$args['labels']['new_item']				= __( 'New Item', 'wpex' );
			$args['labels']['view_item']			= __( 'View Item', 'wpex' );
			$args['labels']['search_items']			= __( 'Search Items', 'wpex' );
			$args['labels']['not_found']			= __( 'No Items Found', 'wpex' );
			$args['labels']['not_found_in_trash']	= __( 'No Items Found In Trash', 'wpex' );
		}
		// Change slug
		$option = wpex_option( 'portfolio_slug', 'portfolio-item' );
		if ( isset( $option ) && $option ) {
			$args['rewrite'] = array( "slug" => $option );
		}
		return $args;
	}
}
add_filter( 'wpex_portfolio_args', 'wpex_custom_portfolio_args' );

/**
* Staff Post Type
* @since 1.0
*/
if ( ! function_exists( 'wpex_custom_staff_args' ) ) {
	function wpex_custom_staff_args( $args ) {
		// Change Label
		$option = wpex_option( 'staff_labels', __( 'Staff', 'wpex' ) );
		if ( isset( $option  ) && $option  ) {
			$args['labels']['name']					= $option;
			$args['labels']['singular_name']		= $option;
			$args['labels']['add_new']				= __( 'Add New', 'wpex' );
			$args['labels']['add_new_item']			= __( 'Add New Item', 'wpex' );
			$args['labels']['edit_item']			= __( 'Edit Item', 'wpex' );
			$args['labels']['new_item']				= __( 'New Item', 'wpex' );
			$args['labels']['view_item']			= __( 'View Item', 'wpex' );
			$args['labels']['search_items']			= __( 'Search Items', 'wpex' );
			$args['labels']['not_found']			= __( 'No Items Found', 'wpex' );
			$args['labels']['not_found_in_trash']	= __( 'No Items Found In Trash', 'wpex' );
		}
		// Change slug
		$option = wpex_option( 'staff_slug', 'staff-item' );
		if ( isset( $option ) && $option ) {
			$args['rewrite'] = array( "slug" => $option );
		}
		return $args;
	}
}
add_filter( 'wpex_staff_args', 'wpex_custom_staff_args' );

/**
* Testimonials Post Type
* @since 1.0
*/
if ( ! function_exists( 'wpex_custom_testimonials_args' ) ) {
	function wpex_custom_testimonials_args( $args ) {
		// Change Label
		$option = wpex_option( 'testimonials_labels', __( 'Staff', 'wpex' ) );
		if ( isset( $option  ) && $option  ) {
			$args['labels']['name']					= $option;
			$args['labels']['singular_name']		= $option;
			$args['labels']['add_new']				= __( 'Add New', 'wpex' );
			$args['labels']['add_new_item']			= __( 'Add New Item', 'wpex' );
			$args['labels']['edit_item']			= __( 'Edit Item', 'wpex' );
			$args['labels']['new_item']				= __( 'New Item', 'wpex' );
			$args['labels']['view_item']			= __( 'View Item', 'wpex' );
			$args['labels']['search_items']			= __( 'Search Items', 'wpex' );
			$args['labels']['not_found']			= __( 'No Items Found', 'wpex' );
			$args['labels']['not_found_in_trash']	= __( 'No Items Found In Trash', 'wpex' );
		}
		// Change slug
		$option = wpex_option( 'testimonials_slug', 'testimonials-item' );
		if ( isset( $option ) && $option ) {
			$args['rewrite'] = array( "slug" => $option );
		}
		return $args;
	}
}
add_filter( 'wpex_testimonials_args', 'wpex_custom_testimonials_args' );