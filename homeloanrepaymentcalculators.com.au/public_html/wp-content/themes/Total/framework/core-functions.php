<?php
/**
 * Core theme functions - muy importante!
 *
 * @package Total
 * @subpackage functions
 * @author Alexander Clarke
 * @copyright Copyright (c) 2014, Symple Workz LLC
 * @link http://www.wpexplorer.com
 * @since Total 1.33
 */

/**
 * Setup the core theme for easy adding/removing of functions
 *
 * @since Total 1.33
 * @return array $setup
 */
if ( !function_exists( 'wpex_global_config' ) ) {
	function wpex_global_config() {
		$setup = array(
			'primary'		=> array(
				'admin'					=> true,
				'post_types'			=> true,
				'breadcrumbs'			=> true,
				'visual_composer_edits'	=> true,
			),
			'post_types'	=> array(
				'portfolio'		=> true,
				'staff'			=> true,
				'testimonials'	=> true,
			),
			'mce'			=> array(
				'fontselect'		=> true,
				'fontsizeselect'	=> true,
				'formats'			=> true,
				'shortcodes'		=> true,
			),
			'helpers'		=> array (
				'display_queries_memory'	=> false,
			),
			'minify'		=> array(
				'js'	=> true,
				'css'	=> true
			),
		);
		return apply_filters( 'wpex_global_config', $setup );
	}
}

/**
 * Checks for core functions support
 *
 * @since Total 1.33
 * @return bool
 */
function wpex_supports( $group, $feature ) {
	$setup = wpex_global_config();
	if( isset( $setup[$group][$feature] ) && $setup[$group][$feature] ) {
		return true;
	} else {
		return false;
	}
}

/**
 * Returns theme custom post types
 *
 * @since Total 1.33
 * @return array $post_types
 */
function wpex_theme_post_types() {
	$config = wpex_global_config();
	if ( ! wpex_supports( 'primary', 'post_types' ) ) {
		return array();
	}
	$post_types = $config['post_types'];
	if ( ! $post_types ) {
		return;
	}
	$post_types = array_filter( $post_types );
	$return = '';
	foreach( $post_types as $key => $value ) {
		$return[$key] = $key;
	}
	return apply_filters( 'wpex_theme_post_types', $return );
}

/**
 * Returns the correct theme option value
 *
 * @since Total 1.33
 * @return array $post_types
 */
if ( ! function_exists( 'wpex_option' ) ) {
	function wpex_option( $id, $fallback = false, $param = false ) {
		// Return value based on $_GET value
		if ( isset( $_GET[ 'wpex_'.$id ] ) ) {
			if ( '-1' == $_GET[ 'wpex_'.$id ] ) {
				return false;
			} else {
				return $_GET[ 'wpex_'.$id ];
			}
		}
		// Return value based on theme panel option
		else {
			// Get options
			global $wpex_options;
			// Check if fallback is false set to empty
			if ( $fallback == false ) {
				$fallback = '';
			}
			// If option value exists and not empty return value
			if( isset( $wpex_options[ $id ] ) && '' != $wpex_options[ $id ] ) {
				$output = $wpex_options[ $id ];
			}
			// Otherwise return fallback
			else {
				$output = $fallback;
			}
			// If param defined return param
			if ( !empty( $wpex_options[ $id ] ) && $param ) {
				$output = $wpex_options[ $id ][ $param ];
			}
		}
		return $output;
	}
}

/**
 * Get's the current ID, this function is needed to properly support WooCommerce
 *
 * @since Total 1.5.4
 */
if ( ! function_exists( 'wpex_get_the_id' ) ) {
	function wpex_get_the_id() {
		// If singular get_the_ID
		if ( is_singular() ) {
			return get_the_ID();
		}
		// Get ID of WooCommerce product archive
		elseif ( is_post_type_archive( 'product' ) && class_exists( 'Woocommerce' ) && function_exists( 'wc_get_page_id' ) ) {
			$shop_id = wc_get_page_id( 'shop' );
			if ( isset( $shop_id ) ) {
				return wc_get_page_id( 'shop' );
			}
		}
		// Return nothing
		else {
			return NULL;
		}
	}
}

/**
 * Returns the correct main layout class
 *
 * @since Total 1.5
 * @return bool
 */
if ( !function_exists( 'wpex_main_layout' ) ) {
	function wpex_main_layout() {

		// Get default theme val
		$layout = wpex_option( 'main_layout_style', 'full-width' );

		// Check post meta
		$post_id = wpex_get_the_id();
		if ( $post_id ) {
			$meta = get_post_meta( $post_id, 'wpex_main_layout', true );
			if ( '' != $meta ) {
				$layout = $meta;
			}
		}

		// Return correct layout
		return apply_filters( 'wpex_main_layout', $layout );

	}
}

/**
 * Check if currently in front-end composer
 *
 * @since Total 1.5
 * @return bool
 */
if ( !function_exists( 'wpex_is_front_end_composer' ) ) {
	function wpex_is_front_end_composer() {
		if ( ! function_exists('vc_is_inline') ) {
			return false;
		} elseif ( vc_is_inline() ) {
			return true;
		} else{
			return false;
		}
	}
}

/**
 * The source for the sidr mobile menu
 *
 * @since Total 1.51
 * @return string
 */
if ( !function_exists( 'wpex_mobile_menu_source' ) ) {
	function wpex_mobile_menu_source() {
		$array = array();
		$array['sidrclose'] = '#sidr-close';
		if ( has_nav_menu( 'mobile_menu_alt' ) ) {
			$array['nav'] = '#mobile-menu-alternative';
		} else {
			$array['nav'] = '#site-navigation';
		}
		$array['search'] = '#mobile-menu-search';
		$array = apply_filters( 'wpex_mobile_menu_source', $array );
		return implode( ', ', $array );
	}
}

/**
 * Array of social profiles for staff members
 *
 * @since Total 1.54
 */
if ( ! function_exists( 'wpex_staff_social_array' ) ) {
	function wpex_staff_social_array() {
		$array = array(
			array (
				'key'			=> 'twitter',
				'meta'			=> 'wpex_staff_twitter',
				'icon_class'	=> 'fa fa-twitter',
				'label'			=> 'Twitter',
			),
			array (
				'key'			=> 'facebook',
				'meta'			=> 'wpex_staff_facebook',
				'icon_class'	=> 'fa fa-facebook',
				'label'			=> 'Facebook',
			),
			array (
				'key'			=> 'google_plus',
				'meta'			=> 'wpex_staff_google-plus',
				'icon_class'	=> 'fa fa-google-plus',
				'label'			=> 'Google Plus',
			),
			array (
				'key'			=> 'dribbble',
				'meta'			=> 'wpex_staff_dribbble',
				'icon_class'	=> 'fa fa-dribbble',
				'label'			=> 'Dribbble',
			),
			array (
				'key'			=> 'linkedin',
				'meta'			=> 'wpex_staff_linkedin',
				'icon_class'	=> 'fa fa-linkedin',
				'label'			=> 'Linkedin',
			),
			array (
				'key'			=> 'skype',
				'meta'			=> 'wpex_staff_skype',
				'icon_class'	=> 'fa fa-skype',
				'label'			=> 'Skype',
			),
			array (
				'key'			=> 'email',
				'meta'			=> 'wpex_staff_email',
				'icon_class'	=> 'fa fa-envelope',
				'label'			=> __( 'Email', 'wpex' ),
			),
			array (
				'key'			=> 'website',
				'meta'			=> 'wpex_staff_website',
				'icon_class'	=> 'fa fa-external-link-square',
				'label'			=> __( 'Website', 'wpex' ),
			),
		);
		return apply_filters( 'wpex_staff_social_array', $array );
	}
}

/**
 * Creates an array for adding the staff social options to the redux meta options
 * See framework/redux/meta-config.php
 *
 * @since Total 1.54
 */
if ( ! function_exists( 'wpex_staff_social_meta_array' ) ) {
	function wpex_staff_social_meta_array() {
		$profiles = wpex_staff_social_array();
		$array = array();
		foreach ( $profiles as $profile ) {
			$array[] = array(
					'title'		=> $profile['label'] .' '. __( 'URL', 'wpex' ),
					'id'		=> $profile['meta'],
					'type'		=> 'text',
					'std'		=> '',
			);
		}
		return $array;
	}
}

/**
 * Defines your default search results page style
 *
 * @since Total 1.54
 */
if ( ! function_exists( 'wpex_search_results_style' ) ) {
	function wpex_search_results_style() {
		$style = apply_filters( 'wpex_search_results_style', 'default' );
		return $style;
	}
}

/**
 * Returns the post URL
 *
 * @since Total 1.54
 * @return string
 */
if ( ! function_exists( 'wpex_permalink' ) ) {
	function wpex_permalink() {
		$permalink = get_permalink();
		$format = get_post_format();
		if( 'link' == $format ) {
			if ( '' != get_post_meta( get_the_ID(), 'wpex_post_link', true ) ) {
				$permalink = get_post_meta( get_the_ID(), 'wpex_post_link', true );
				$permalink = esc_url( $permalink );
			}
		}
		return $permalink;
	}
}

/**
 * Returns the 1st taxonomy of any taxonomy
 *
 * @since Total 1.33
 * @return string
 */
if ( !function_exists( 'wpex_get_first_term' ) ) {
	function wpex_get_first_term( $post_id, $taxonomy = 'category' ) {
		if ( ! $post_id ) {
			return;
		}
		if ( ! taxonomy_exists( $taxonomy ) ) {
			return;
		}
		$terms = wp_get_post_terms( $post_id, $taxonomy );
		if ( ! empty( $terms ) ) { ?>
			<span><?php echo $terms[0]->name; ?></span>
		<?php
		}
	}
}


/**
 * Returns the 1st taxonomy of any taxonomy
 *
 * @since Total 1.33
 * @return string
 */
if ( !function_exists( 'wpex_get_first_term' ) ) {
	function wpex_get_first_term( $post_id, $taxonomy = 'category' ) {
		if ( ! $post_id ) {
			return;
		}
		if ( ! taxonomy_exists( $taxonomy ) ) {
			return;
		}
		$terms = wp_get_post_terms( $post_id, $taxonomy );
		if ( ! empty( $terms ) ) { ?>
			<span><?php echo $terms[0]->name; ?></span>
		<?php
		}
	}
}