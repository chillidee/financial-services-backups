<?php
/**
 * Footer Bottom
 *
 * @package Total
 * @subpackage Footer Functions
 * @author Alexander Clarke
 * @copyright Copyright (c) 2014, Symple Workz LLC
 * @link http://www.wpexplorer.com
 * @since Total 1.1
 */

/**
 * Returns the footer copyright info
 *
 * @since Total 1.0
 */
if ( ! function_exists( 'wpex_footer_copyright' ) ) {
	function wpex_footer_copyright() {

		// Do nothing if the copyright text is empty
		if ( ! wpex_option( 'footer_copyright_text' ) ) {
			return;
		}
		
		// Return the copyright
		return apply_filters( 'wpex_copyright_info', do_shortcode( wpex_option( 'footer_copyright_text' ) ) );

	}
}

/**
 * Outputs the footer bottom area
 *
 * @since Total 1.0
 * @return bool
 */
if ( ! function_exists( 'wpex_footer_bottom' ) ) {
	function wpex_footer_bottom() { ?>
			
		<?php
		// Lets bail if this section is disabled
		if ( ! wpex_option( 'footer_copyright', '1' ) ) {
			return; 
		} ?>
		
		<div id="footer-bottom" class="clr">
			<div id="footer-bottom-inner" class="container clr">
				<div id="copyright" class="clr" role="contentinfo">
					<?php
					// Display copyright info
					echo wpex_footer_copyright(); ?>
				</div><!-- #copyright -->
				<div id="footer-bottom-menu" class="clr">
					<?php
					// Display footer menu
					wp_nav_menu( array(
						'theme_location'	=> 'footer_menu',
						'sort_column'		=> 'menu_order',
						'fallback_cb'		=> false,
					) ); ?>
				</div><!-- #footer-bottom-menu -->
			</div><!-- #footer-bottom-inner -->
		</div><!-- #footer-bottom -->
			
	<?php		
	}
}