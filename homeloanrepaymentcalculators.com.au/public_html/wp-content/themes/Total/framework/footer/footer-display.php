<?php
/**
 * Function used to show/hide the main footer depending on current post meta
 *
 * @package Total
 * @subpackage Footer Functions
 * @author Alexander Clarke
 * @copyright Copyright (c) 2014, Symple Workz LLC
 * @link http://www.wpexplorer.com
 * @since Total 1.0
 */


/**
 * Checks if the footer should display or not
 *
 * @since Total 1.0
 * @return bool
 */
if ( ! function_exists( 'wpex_display_footer' ) ) {
	function wpex_display_footer() {

		// Return true by default
		$return = true;

		// Check if disabled on a per-post basis
		$post_id = wpex_get_the_id();
		if ( $post_id && 'on' == get_post_meta( $post_id, 'wpex_disable_footer', true ) ) {
			$return = false;
		}

		// Return
		$return = apply_filters( 'wpex_display_footer', $return );
		return $return;
		
	}
}

/**
 * Checks if the footer widgets should display or not
 *
 * @since Total 1.54
 * @return bool
 */
if ( ! function_exists( 'wpex_display_footer_widgets' ) ) {
	function wpex_display_footer_widgets() {
		if( wpex_option( 'footer_widgets', '1' ) ) {
			$return = true;
		} else {
			$return = false;
		}
		return apply_filters( 'wpex_display_footer_widgets', $return );
	}
}

/**
 * Checks if the footer reveal option is enabled
 *
 * @since Total 1.0
 * @return bool
 */
if ( ! function_exists( 'wpex_footer_reveal_enabled' ) ) {
	function wpex_footer_reveal_enabled() {

		// Disable on mobile
		if ( wp_is_mobile() ) {
			return false;
		}

		// Disable on boxed style
		if ( 'boxed' == wpex_main_layout() ) {
			return false;
		}

		// Get post id
		if ( is_singular() ) {
			$post_id = get_the_ID();
		} elseif ( is_post_type_archive( 'product' )
			&& class_exists( 'Woocommerce' )
			&& function_exists( 'wc_get_page_id' ) ) {
			$shop_id = wc_get_page_id( 'shop' );
			if ( isset( $shop_id ) ) {
				$post_id = wc_get_page_id( 'shop' );
			}
		}

		// Meta check
		if ( isset( $post_id ) && '' != $post_id ) {
			if ( 'on' == get_post_meta( $post_id, 'wpex_footer_reveal', true ) ) {
				return true;
			} elseif ( 'off' == get_post_meta( $post_id, 'wpex_footer_reveal', true ) ) {
				return false;
			}
		}

		// Theme option check
		if ( wpex_option( 'footer_reveal' ) ) {
			return true;
		}

	}
}