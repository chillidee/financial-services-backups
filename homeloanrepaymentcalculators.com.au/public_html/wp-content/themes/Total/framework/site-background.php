<?php
/**
 * Custom backgrounds for your site
 *
 * @package Total
 * @subpackage functions
 * @author Alexander Clarke
 * @copyright Copyright (c) 2014, Symple Workz LLC
 * @link http://www.wpexplorer.com
 * @since Total 1.0
 */

if ( !function_exists( 'wpex_site_background' ) ) {
	function wpex_site_background() {
		
		// VARS
		$css = '';
		$color = wpex_option( 'background_color' );
		$image_toggle = wpex_option( 'background_image_toggle' );
		$image = wpex_option( 'background_image', '', 'url' );
		$style = wpex_option( 'background_style' );
		$pattern_toggle = wpex_option( 'background_pattern_toggle' );
		$pattern = wpex_option( 'background_pattern' );

		// Color
		if ( $color ) {
			$css .= 'body, .boxed-main-layout { background-color: '. $color .'; }';
		}
		
		// Image
		if ( $image && $image_toggle && ! $pattern_toggle  ) {
			$css .= 'body { background-image: url('. $image .'); }';
			if ( $style ) {
				if ( $style == 'stretched' ) {
					$css .= 'body { -webkit-background-size: cover; -moz-background-size: cover; -o-background-size: cover; background-size: cover; background-position: center center; background-attachment:fixed; background-repeat: no-repeat; }';
				}
				if ( $style == 'repeat' ) {
					$css .= 'body { background-repeat: repeat; }';
				}
				if ( $style == 'fixed' ) {
					$css .= 'body { background-repeat: no-repeat; background-position: center center; background-attachment:fixed; }';
				}
			}
		}
		
		// Pattern
		if ( $pattern && $pattern_toggle ) {
			$css .= 'body { background-image: url('. $pattern .'); background-repeat: repeat; }';
		}
		
		// Return trimmed CSS
		$css = preg_replace( '/\s+/', ' ', $css );
		// output css on front end
		if ( '' != $css ) {
			$css = '/*Admin Site Background CSS START*/'. $css .'/*Admin Site Background CSS END*/';
			return $css;
		} else {
			return '';
		}

	}
}
add_action( 'wp_head', 'wpex_site_background' );