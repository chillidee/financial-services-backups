<?php
/**
 * Function used to display the page subheading
 *
 * @package Total
 * @subpackage Functions
 * @author Alexander Clarke
 * @copyright Copyright (c) 2014, Symple Workz LLC
 * @link http://www.wpexplorer.com
 * @since Total 1.0
 */

/**
 * Returns the correct title to display
 *
 * @since Total 1.0
 */
if ( ! function_exists( 'wpex_page_title' ) ) {
	function wpex_page_title( $title = '' ) {
		
		// Get post ID
		$post_id = wpex_get_the_id();
		
		// Homepage - display blog description if not a static page
		if ( is_front_page() && !is_singular( 'page' ) ) {
			
			if ( get_bloginfo( 'description' ) ) {
				$title = get_bloginfo( 'description' );
			} else {
				return __( 'Recent Posts', 'wpex' );
			}

		// Homepage posts page
		} elseif ( is_home() && !is_singular( 'page' ) ) {

			$title = get_the_title( get_option( 'page_for_posts', true ) );
			
		// Archives
		} elseif ( is_archive() ) {
			
			// Daily archive title
			if ( is_day() ) {
				$title = sprintf( __( 'Daily Archives: %s', 'wpex' ), get_the_date() );
			
			// Monthly archive title
			} elseif ( is_month() ) {
				$title = sprintf( __( 'Monthly Archives: %s', 'wpex' ), get_the_date( _x( 'F Y', 'monthly archives date format', 'wpex' ) ) );
				
			// Yearly archive title
			} elseif ( is_year() ) {
				$title = sprintf( __( 'Yearly Archives: %s', 'wpex' ), get_the_date( _x( 'Y', 'yearly archives date format', 'wpex' ) ) );

			// Post Type archive title
			} elseif ( is_post_type_archive() ) {

				if ( is_post_type_archive( 'product' ) ) {
					if ( class_exists( 'Woocommerce' ) && function_exists( 'wc_get_page_id' ) ) {
						$title = get_the_title( wc_get_page_id( 'shop' ) );
					} else {
						$title = __( 'Shop', 'wpex' );
					}
				} else {
					$title = post_type_archive_title( '', false );
				}
			
			// Standard term title
			} else {
				$title = single_term_title( "", false );
				
				// Fix for bbPress and other plugins that are archives but use
				// Standard templates...zzz
				if ( ! $title ) {
					global $post;
					$title = get_the_title( $post->ID );
				}
			}
			
		// Search
		} elseif( is_search() ) {

			global $wp_query;
			
			$title = '<span id="search-results-count">'. $wp_query->found_posts .'</span> '. __( 'Search Results Found', 'wpex' );
			
		// 404 Page
		} elseif ( is_404() ) {
			
			$title = wpex_option( 'error_page_title', __( '404: Page Not Found', 'wpex') );
		
		// All else
		} elseif ( $post_id ) {
			// Singular products
			if ( is_singular( 'product' ) ) {
				$title = wpex_option( 'woo_shop_single_title', __( 'Store', 'wpex' ) );

			// Single posts
			} elseif( is_singular( 'post' ) ) {

				// Display custom text for blog post header
				if ( 'custom_text' == wpex_option( 'blog_single_header', 'custom_text' ) ) {
					$title = wpex_option( 'blog_single_header_custom_text', __( 'Blog', 'wpex' ) );
				}
				// Display post title for single posts
				else {
					$title = get_the_title( $post_id );
				}

			}

			// All other
			else {
				$title = get_the_title( $post_id );
			}
		}

		// Tribe Events Calendar Plugin title
		if ( function_exists( 'tribe_is_month' ) ) {
			if( tribe_is_month() ) {
				$title = __( 'Events Calendar', 'wpex' );
			} elseif ( function_exists( 'tribe_is_event' ) && function_exists( 'tribe_is_day' ) && tribe_is_event() && !tribe_is_day() && !is_single() ) {
				$title = __( 'Events List', 'wpex' );
			} elseif ( function_exists( 'tribe_is_event' ) && function_exists( 'tribe_is_day' ) && tribe_is_event() && !tribe_is_day() && is_single() ) {
				$title = __( 'Single Event', 'wpex' );
			} elseif ( function_exists( 'tribe_is_day' ) && tribe_is_day() ) {
				$title = __( 'Single Day', 'wpex' );
			}
		}

		// Backup
		$title = $title ? $title : get_the_title();

		// Return title and apply filters
		return apply_filters( 'wpex_title', $title );
		
	}
}

/**
 * Returns the page subheading
 *
 * @since Total 1.0
 */
if ( ! function_exists('wpex_post_subheading') ) {
	function wpex_post_subheading() {

		// Define output var
		$output='';

		// Get post ID
		$post_id = wpex_get_the_id();

		// Return if ID not defined
		if ( ! $post_id ) {
			return;
		}
		
		// Posts & Pages
		if ( $post_id ) {

			// Globals
			global $post;

			// Define vars
			$output = $subheading = '';

			// Display staff position if enabled instead of default subheading
			if( is_singular( 'staff' ) && wpex_option( 'staff_subheading_position', '1' ) ) {
				if( '' != get_post_meta( $post_id, 'wpex_staff_position', true ) ) {
					$subheading = get_post_meta( $post_id, 'wpex_staff_position', true );
				}
			}

			// Get subheading
			if( '' != get_post_meta( $post_id, 'wpex_post_subheading', true ) ) {
				$subheading = get_post_meta( $post_id, 'wpex_post_subheading', true );
			}

			// Apply filter for child theme altering
			$subheading = apply_filters( 'wpex_post_subheading', $subheading );
			
			// Output subheading
			if ( $subheading ) {
				$output .= '<div class="clr page-subheading">';
					$output .= do_shortcode( $subheading );
				$output .= '</div>';
			}

		}

		// Categories
		elseif ( is_category() && wpex_option( 'category_descriptions', '1' ) && 'under_title' == wpex_option( 'category_description_position', 'under_title' ) ) {
			if ( term_description() ){
				$output .= '<div class="clr page-subheading term-description">';
					$output .= term_description();
				$output .= '</div>';
			}
		}
		
		// Taxonomies
		elseif ( is_tax() ) {
			$obj = get_queried_object();
			$taxonomy = $obj->taxonomy;
			$term_id = $obj->term_id;
			$description = term_description($term_id,$taxonomy);
			if ( ! empty( $description ) ){
				$output .= '<div class="clr page-subheading term-description">';
					$output .= $description;
				$output .= '</div>';
			}
		}
		
		// Return content
		return apply_filters( 'wpex_post_subheading', $output );
		
	}
}

/**
 * Get current page header style
 *
 * @since Total 1.54
 */
if ( ! function_exists( 'wpex_page_header_style' ) ) {
	function wpex_page_header_style( $post_id = '' ) {
		if ( $post_id ) {
			$post_id = $post_id;
		} else {
			$post_id = wpex_get_the_id();
		}
		return get_post_meta( $post_id, 'wpex_post_title_style', true );

	}
}

/**
 * Get page header background image URL
 *
 * @since Total 1.54
 */
if ( ! function_exists( 'wpex_page_header_background_image' ) ) {
	function wpex_page_header_background_image( $post_id = '' ) {
		if ( $post_id ) {
			$post_id = $post_id;
		} else {
			$post_id = wpex_get_the_id();
		}
		if( !$post_id ) {
			return;
		}
		$bg_img = get_post_meta( $post_id, 'wpex_post_title_background_redux', true );
		if ( is_array( $bg_img ) && !empty( $bg_img['url'] ) ) {
			$bg_img = $bg_img['url'];
		} else {
			$bg_img = get_post_meta( $post_id, 'wpex_post_title_background', true );
		}
		if ( $bg_img ) {
			return $bg_img;
		} else {
			return;
		}
	}
}

/**
 * Checks whether or not the page header should display on a given page
 *
 * @since Total 1.52
 * @return bool
 */
if ( ! function_exists( 'wpex_display_page_header' ) ) {
	function wpex_display_page_header( $return = true ) {
		$return = apply_filters( 'wpex_display_page_header', $return );
		if ( $return == true ) {
			return wpex_page_header();
		}
	}
}

/**
 * Outputs Custom CSS for the page title
 *
 * @since Total 1.53
 */
if ( !function_exists( 'wpex_page_header_overlay' ) ) {
	function wpex_page_header_overlay() {

		// Get post ID
		$post_id = wpex_get_the_id();

		// Return if ID not defined
		if ( !$post_id ) {
			return;
		}

		// If a background image is defined
		if ( 'background-image' == get_post_meta( $post_id, 'wpex_post_title_style', true ) ) {
			// Check that overlay style isn't set to none
			if ( 'none' != get_post_meta( $post_id, 'wpex_post_title_background_overlay', true ) ) {
				// Add opacity style if opacity is defined
				if ( '' != get_post_meta( $post_id, 'wpex_post_title_background_overlay_opacity', true ) ) {
					$opacity = 'style="opacity:'. get_post_meta( $post_id, 'wpex_post_title_background_overlay_opacity', true ) .'"';
				} else {
					$opacity = '';
				}
				// Echo the span for the background overlay
				echo '<span class="background-image-page-header-overlay style-'. get_post_meta( $post_id, 'wpex_post_title_background_overlay', true ) .'" '. $opacity .'></span>';
			}
		}
		// No background image defined so return nothing
		else {
			return;
		}

	}
}
add_action( 'wpex_hook_page_header_bottom', 'wpex_page_header_overlay' );

/**
 * Outputs Custom CSS for the page title
 *
 * @since Total 1.53
 */
if ( ! function_exists( 'wpex_page_header_css' ) ) {
	function wpex_page_header_css( $output ) {

		// Get post ID
		$post_id = wpex_get_the_id();

		// Return if ID not defined
		if ( ! $post_id ) {
			return $output;
		}

		// Define var
		$css = $bg_img = '';
		$title_style = wpex_page_header_style( $post_id );

		// Background Color
		if ( $title_style == 'solid-color' || $title_style == 'background-image' ) {
			$bg_color = get_post_meta( $post_id, 'wpex_post_title_background_color', true );
			if ( $bg_color ) {
				$css .='background-color: '. $bg_color .';';
			}
		}

		// Background image
		if ( $title_style == 'background-image' ) {
			$bg_img = wpex_page_header_background_image( $post_id );
			if ( $bg_img ) {
				$css .= 'background-image: url('. $bg_img .');background-position:50% 0;-webkit-background-size: cover;-moz-background-size: cover;-o-background-size: cover;background-size: cover;';
			}
		}

		// Custom height
		$title_height = get_post_meta( $post_id, 'wpex_post_title_height', true );
		if( $title_height ) {
			$title_height = $title_height;
		} else {
			$title_height = '400';
		}
		if ( $title_height && $bg_img ) {
			$css .= 'height:'. intval( $title_height ) .'px;';
		}

		// Apply all css to the page-header class
		if ( '' != $css ) {
			$output .= '.page-header { '. $css .' }';
		}

		// Overlay Color
		$overlay_color = get_post_meta( $post_id, 'wpex_post_title_background_overlay', true );
		if( 'bg_color' == $overlay_color && $title_style == 'background-image' && isset( $bg_color ) ) {
			$output .= '.background-image-page-header-overlay { background-color: '. $bg_color .'; }';
		}

		// Return css
		return $output;
	}
}
add_filter( 'wpex_custom_css_filter', 'wpex_page_header_css' );

/**
 * Outputs the entire page header/title
 *
 * @since Total 1.0
 */
if ( ! function_exists( 'wpex_page_header' ) ) {
	function wpex_page_header() {

		// Get global $post var
		global $post;
		$post_id = $classes = '';

		// If title is empty return false
		if ( ! wpex_page_title() ) {
			return;
		}

		// Disabled for author archives
		if ( is_author() ) {
			return;
		}

		// Set defaults
		$title_enabled = true;
		$breadcrumbs = true;

		// Get currect post ID
		$post_id = wpex_get_the_id();

		// Return if disabled on the store via the admin panel
		if ( is_post_type_archive( 'product' ) && !wpex_option( 'woo_shop_title' ) ) {
			return;
		}

		// Return if page header is disabled via custom field
		if ( $post_id ) {

			// Get title style
			$title_style = wpex_page_header_style( $post_id );

			// Return if page header is disabled and there isn't a page header background defined
			if ( 'on' == get_post_meta( $post_id, 'wpex_disable_title', true ) && 'background-image' != $title_style ) {
				return;
			}
			
			// Get title style
			if ( empty( $title_style ) ) {
				$title_style = wpex_option( 'page_header_style' );
			}
			
			// Custom Classes
			if ( 'default' != $title_style && $title_style ) { 
				$classes .= ' '. $title_style .'-page-header';
			}
			
			// Disable breadcrumbs if background image set
			if ( 'background-image' == $title_style || 'centered' == $title_style || 'centered-minimal' == $title_style ) {
				$breadcrumbs = false;
			}

			// Disable title if the page header is disabled but the page header background is defined
			if ( 'on' == get_post_meta( $post_id, 'wpex_disable_title', true ) && 'background-image' == $title_style ) {
				$title_enabled = false;
			}
		}

		// Declare vars
		$output=$title_style='';

		// Enable breadcrumbs by default
		if ( is_front_page() ) {
			$breadcrumbs = false;
		}

		// Before Hook
		wpex_hook_page_header_before(); ?>
			<header class="page-header<?php echo $classes; ?>">
				<?php
				// Top Hook
				wpex_hook_page_header_top(); ?>
				<div class="container clr page-header-inner">
					<?php
					// Inner hook - IN PROGRESS!!!
					wpex_hook_page_header_inner();

					//  Display header and subheading if enabled
					if ( $title_enabled ) {

						// Display the main header title
						$heading = apply_filters( 'wpex_page_header_heading', 'h1');
						echo '<'. $heading .' class="page-header-title">'. wpex_page_title() .'</'. $heading .'>';
					
						// Function used to display the subheading defined in the meta options
						echo wpex_post_subheading();

					}
					
					// Display built-in breadcrumbs - see functions/breadcrumbs.php
					$breadcrumbs = apply_filters( 'wpex_page_header_breadcrumbs', $breadcrumbs );
					if ( $breadcrumbs ) {
						wpex_display_breadcrumbs();
					} ?>
				</div><!-- .page-header-inner -->
				<?php
				// Bottom Hook
				wpex_hook_page_header_bottom(); ?>
			</header><!-- .page-header -->
		<?php
		// After Hook
		wpex_hook_page_header_after(); ?>
		
	<?php
	}
}