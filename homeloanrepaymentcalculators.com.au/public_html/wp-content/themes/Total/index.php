<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme and one of the
 * two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * For example, it puts together the home page when no home.php file exists.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package	Total
 * @author Alexander Clarke
 * @copyright Copyright (c) 2014, Symple Workz LLC
 * @link http://www.wpexplorer.com
 * @since Total 1.0
 */


// Get header
get_header(); ?>

	<div id="content-wrap" class="container clr <?php echo wpex_get_post_layout_class(); ?>">
		<section id="primary" class="content-area clr">
			<div id="content" class="site-content" role="main">
				<?php
				// Display posts if there are some
				if ( have_posts() ) : ?>
					<div id="blog-entries" class="clr <?php wpex_blog_wrap_classes(); ?>">
						<?php
						// Start counters to create rows
						$wpex_total_posts=0;
						$wpex_count=0;
						// Loop through posts
						while ( have_posts() ) : the_post();
							// Add row for the fit rows style blog
							if ( 0 == $wpex_count && wpex_blog_fit_rows() ) { ?>
								<div class="blog-row clr">
							<?php }
							// Add to counters
							$wpex_count++;
							$wpex_total_posts++;
							 // Get correct entry template part
							get_template_part( 'content', get_post_format() );
							// Reset counter
							if ( wpex_blog_entry_columns() == $wpex_count ) {
								// Close row for the fit rows style blog
								if ( 'grid-entry-style' == wpex_blog_fit_rows() ) {
									echo '</div><!-- .row -->';
								}
								$wpex_count=0;
							}
						// End loop
						endwhile;
						// Make sure row is closed for the fit rows style blog
						if ( 'grid-entry-style' == wpex_blog_fit_rows() ) {
							if ( '4' == wpex_blog_entry_columns() && ( $wpex_total_posts % 4 != 0 ) ) {
								echo '</div><!-- .row -->';
							}
							if ( '3' == wpex_blog_entry_columns() && ( $wpex_total_posts % 3 != 0 ) ) {
								echo '</div><!-- .row -->';
							}
							if ( '2' == wpex_blog_entry_columns() && ( $wpex_total_posts % 2 != 0 ) ) {
								echo '</div><!-- .row -->';
							}
						} ?>
					</div><!-- #blog-entries -->
				<?php
				// Display pagination
				// See function/pagination.php
				wpex_blog_pagination(); ?>
				<?php endif; ?>
			</div><!-- #content -->
		</section><!-- #primary -->
		<?php
		// Get site sidebar
		get_sidebar(); ?>
    </div><!-- .container -->
    
<?php
// Get site footer
get_footer(); ?>