<?php
/**
 * The template for displaying Portfolio Tags
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package	Total
 * @author Alexander Clarke
 * @copyright Copyright (c) 2014, Symple Workz LLC
 * @link http://www.wpexplorer.com
 * @since Total 1.0
 */


// Get site header
get_header(); ?>

	<div id="content-wrap" class="container clr <?php echo wpex_get_post_layout_class(); ?>">
		<?php if ( have_posts( ) ) : ?>
			<section id="primary" class="content-area clr">
				<div id="content" class="site-content clr" role="main">
					<div id="portfolio-entries" class="<?php echo wpex_get_portfolio_wrap_classes(); ?>">
						<?php
						$wpex_count=0;
						if ( wpex_portfolio_match_height() ) {
							$wpex_total_posts=0;
						}
						// Loop through the posts
						while ( have_posts() ) : the_post();
							if ( wpex_portfolio_match_height() ) {
								if ( 0 == $wpex_count ) { ?>
									<div class="match-height-row clr">
								<?php }
								$wpex_total_posts++;
							}
							$wpex_count++;
							// Get the correct template file for this post type
							wpex_get_template_part();
							// Clear counter/floats
							if( $wpex_count == wpex_option( 'portfolio_entry_columns', '4' ) ) {
								// Close row for the fit rows style blog
								if ( wpex_portfolio_match_height() ) {
									echo '</div><!-- .match-height-row -->';
								}
								$wpex_count=0;
							}
						endwhile;
						// Make sure row is closed for the fit rows style blog
						if ( wpex_portfolio_match_height() ) {
							if ( '4' == wpex_option( 'portfolio_entry_columns', '4' ) && ( $wpex_total_posts % 4 != 0 ) ) {
								echo '</div><!-- .match-height-row -->';
							}
							if ( '3' == wpex_option( 'portfolio_entry_columns', '4' ) && ( $wpex_total_posts % 3 != 0 ) ) {
								echo '</div><!-- .match-height-row -->';
							}
							if ( '2' == wpex_option( 'portfolio_entry_columns', '4' ) && ( $wpex_total_posts % 2 != 0 ) ) {
								echo '</div><!-- .match-height-row -->';
							}
						} ?>
					</div><!-- #portfolio-entries -->
					<div class="clr"></div>
					<?php
					// Display per-page pagination
					wpex_pagination(); ?>
				</div><!-- #content -->
			</section><!-- #primary -->
		<?php endif; ?>
		<?php
		// Get sidebar if needed
		get_sidebar(); ?>
	</div><!-- #content-wrap -->

<?php
// Get Site footer
get_footer(); ?>