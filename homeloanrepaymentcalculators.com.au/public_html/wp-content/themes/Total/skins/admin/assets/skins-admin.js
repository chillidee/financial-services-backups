(function($) {
	"use strict";
	$('.wpex-skin').click( function() {
		$('.wpex-skin').removeClass('active');
		$(this).addClass('active');
		$(this).find('.wpex-skin-radio').prop("checked", true);
		event.preventDefault();
	});
})(jQuery);