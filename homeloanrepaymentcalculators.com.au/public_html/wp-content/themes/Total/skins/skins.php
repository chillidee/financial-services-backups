<?php
/**
 * Returns theme skins
 *
 * @package Total
 * @subpackage functions
 * @author Alexander Clarke
 * @copyright Copyright (c) 2014, Symple Workz LLC
 * @link http://www.wpexplorer.com
 * @since Total 1.54
 */
if ( !function_exists( 'wpex_skins' ) ) {
	function wpex_skins() {
		$skins = array(
			'base'	=> array (
				'core'			=> true,
				'name'			=> __( 'Base', 'wpex' ),
				'screenshot'	=> WPEX_SKIN_DIR_URI .'classes/base/screenshot.png',
			),
			'agent'	=> array(
				'core'			=> true,
				'name'			=> __( 'Agent', 'wpex' ),
				'class'			=> WPEX_SKIN_DIR .'classes/agent/agent-skin.php',
				'screenshot'	=> WPEX_SKIN_DIR_URI .'classes/agent/screenshot.png',
			),
			'neat'	=> array(
				'core'			=> true,
				'name'			=> __( 'Neat', 'wpex' ),
				'class'			=> WPEX_SKIN_DIR .'classes/neat/neat-skin.php',
				'screenshot'	=> WPEX_SKIN_DIR_URI .'classes/neat/screenshot.png',
			),
			'flat'	=> array(
				'core'			=> true,
				'name'			=> __( 'Flat', 'wpex' ),
				'class'			=> WPEX_SKIN_DIR .'classes/flat/flat-skin.php',
				'screenshot'	=> WPEX_SKIN_DIR_URI .'classes/flat/screenshot.png',
			),
			'gaps'	=> array(
				'core'			=> true,
				'name'			=> __( 'Gaps', 'wpex' ),
				'class'			=> WPEX_SKIN_DIR .'classes/gaps/gaps-skin.php',
				'screenshot'	=> WPEX_SKIN_DIR_URI .'classes/gaps/screenshot.png',
			),
			'minimal-graphical'	=> array(
				'core'			=> true,
				'name'			=> __( 'Minimal Graphical', 'wpex' ),
				'class'			=> WPEX_SKIN_DIR .'classes/minimal-graphical/minimal-graphical-skin.php',
				'screenshot'	=> WPEX_SKIN_DIR_URI .'classes/minimal-graphical/screenshot.png',
			),
		);
		$skins = apply_filters( 'wpex_skins', $skins );
		return $skins;
	}
}

/**
 * Setup Skins Admin Panel
 *
 * @since Total 1.54
 */
require_once( WPEX_SKIN_DIR . 'admin/skins-admin.php' );

/**
 * Get active skin
 *
 * @since Total 1.54
 */
if ( !function_exists( 'wpex_active_skin') ) {
	function wpex_active_skin() {
		$skin = get_option( 'theme_skin' );
		if( !$skin ) {
			$data = get_option( 'wpex_options' );
			if( isset( $data['site_theme'] ) ) {
				$skin = $data['site_theme'];
			}
		}
		if( isset( $skin ) && '' != $skin ) {
			$skin = $skin;
		} else {
			$skin = 'base';
		}
		return $skin;
	}
}

/**
 * Get active class file
 *
 * @since Total 1.54
 */
if ( !function_exists( 'wpex_active_skin_class_file') ) {
	function wpex_active_skin_class_file( $skin ) {
		$skins = wpex_skins();
		foreach ( $skins as $key => $value ) {
			if( $key == $skin ) {
				return $value['class'];
			}
		}
	}
}

/**
 * Include the active skin class file
 *
 * @since Total 1.54
 */
if ( 'base' != wpex_active_skin() && '' != wpex_active_skin() ) {
	$wpex_active_skin_class = wpex_active_skin_class_file( wpex_active_skin() );
	if ( file_exists( $wpex_active_skin_class ) ) {
		require_once( $wpex_active_skin_class );
	}
}