<?php
/**
 * Used for your standard post entry content and single post media
 *
 * @package	Total
 * @author Alexander Clarke
 * @copyright Copyright (c) 2014, Symple Workz LLC
 * @link http://www.wpexplorer.com
 * @since Total 1.0
 */


// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}


/******************************************************
 * Single Posts
******************************************************/
if ( is_singular() ) {
	
	// Do nothing on single posts because the post should be redirected

}
/******************************************************
 * Entries
 * @since 1.0
******************************************************/
else {
	/**
	 * Holy cow this is simple
	 * Does that mean it's easy to customize as well?
	 * Yes, you are correct!
	 * Visit the link below and you can copy any function to your child theme to override it.
	 *
	 * @link /framework/blog/blog-entry.php for functions
	 */
	wpex_blog_entry_display();
}